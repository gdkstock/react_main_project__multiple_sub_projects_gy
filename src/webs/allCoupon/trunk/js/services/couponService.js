/**
 * 优惠券相关接口
 */

import apiHelper from './apiHelper';

class CouponService {

  /**
   * 获取优惠券列表
   * @param {*object} data
   */
  getCouponList(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}coupon/list`,
      data: {
        method: 'post',
        body: data
      }
    };
    return apiHelper.fetch(requestParam);
  }

  /**
   * 获取优惠券详情
   * @param {*object} data
   */
  getCouponDetail(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}coupon/detail`,
      data: {
        method: 'post',
        body: data
      }
    };
    return apiHelper.fetch(requestParam);
  }

  /**
   * 兑换优惠券
   * @param {*object} data
   */
  addCoupon(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}coupon/distribute`,
      data: {
        method: 'post',
        body: data
      }
    };
    return apiHelper.fetch(requestParam);
  }

}

export default new CouponService()