/*
* 优惠券actions
*/

import {
    GET_COUPON_LIST,
    GET_COUPON_DETAIL,
    ADD_COUPON_ASYNC,
} from './actionsTypes'

//获取优惠券列表
export const getCouponList = (data, callback) => ({
    type: GET_COUPON_LIST,
    data,
    callback
})

//获取优惠券详情
export const getCouponDetail = (data, callback) => ({
    type: GET_COUPON_DETAIL,
    data,
    callback
})

//兑换优惠券
export const addCouponAsync = (data, callback) => ({
    type: ADD_COUPON_ASYNC,
    data,
    callback
})

