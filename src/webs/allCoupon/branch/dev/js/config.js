/**
 * 配置
 */
import common from './utils/common'

const isPre = window.location.host.indexOf('pre.cx580.com') > -1; //预发布环境
const production = window.location.host.indexOf('h5index.cx580.com') > -1 || isPre; //是否为生产环境或预发布环境
const cookiePre = production ? '' : 'test_'; //cookie前缀
const userType = sessionStorage.getItem('userType') || common.getUrlKeyValue('userType') || common.getCookie(cookiePre + 'userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getUrlKeyValue('authType') || common.getCookie(cookiePre + 'authType') || common.getJsApiUserType() || 'alipay'
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;

export default {
    //应用名
    projectName: "allCoupon",

    //是否为生产环境
    production: production,

    //接口地址
    baseApiUrl: production
        ? window.location.protocol + "//" + window.location.host + "/"
        : "http://webtest.cx580.com:9026/",

    //单点登录地址 回调地址需自行补全
    authUrl: authUrl,

    //debug的userId账号
    debugUsers: [],
}