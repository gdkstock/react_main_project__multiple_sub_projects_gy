import styles from './card.scss'

const Card = (props) => {
    let { onClick, couponTitle, couponAmount, invalidDate, couponExplain, des, type } = props;

    let className = styles.container;
    if (type === 2) {
        className += ' ' + styles.used;
    } else if (type === 3) {
        className += ' ' + styles.overdue;
    }
    
    return (
        <div className={className} onClick={onClick ? () => onClick() : false}>
            <div className={styles.card}>
                <div className={styles.main}>
                    {
                        des
                            ? <div className={styles.leftText}>
                                <pan className={styles.des}>{des}</pan>
                            </div>
                            : <div className={styles.leftText}>
                                <span className={styles.num}>{couponAmount}</span>
                                <span className={styles.unit}>元</span>
                            </div>
                    }
                    <div className={styles.rightText + " abs-v-center"}>
                        <h3>{couponTitle}</h3>
                        <p>有效期至 {invalidDate}</p>
                    </div>
                    <div className={styles.icon}></div>
                </div>
                <div className={couponExplain ? styles.msg : 'hide'}>
                    <div className={styles.leftCircle}></div>
                    <div className={styles.rightCircle}></div>
                    <div>{couponExplain}</div>
                </div>
            </div>
        </div>
    )
}

export default Card;