/**
 * 优惠券相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_COUPON_LIST,
    GET_COUPON_DETAIL,
    ADD_COUPON_LIST,
    UPDATE_COUPON_DETAIL,
    ADD_COUPON_ASYNC
} from '../actions/actionsTypes.js'

//antd
import { Toast } from 'antd-mobile'

//server
import couponService from '../services/couponService'

//常用工具类
import { ChMessage } from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库

/**
 * 获取优惠券列表
 */
function* getCouponList(action) {
    try {
        const { result, timeout } = yield race({
            result: call(couponService.getCouponList, action.data),
            timeout: call(delay, 3000) //超时时间设为3秒
        })

        if (timeout) {
            return;
        } else {
            if (result.code == 1000) {
                const coupon = new schema.Entity('coupon', {}, { idAttribute: 'couponId' });
                const coupons = new schema.Array(coupon);
                const normalizedData = normalize(result.data, coupons);
                console.log("normalizedData:", normalizedData)
                yield put({
                    type: ADD_COUPON_LIST,
                    data: normalizedData.entities.coupon,
                    result: normalizedData.result
                })
            }
            if (action.callback) {
                action.callback(result)
            }
        }
    } catch (error) {
        if (action.callback) {
            action.callback(error)
        }
    }
}
function* watchGetCouponList() {
    yield takeLatest(GET_COUPON_LIST, getCouponList)
}

/**
 * 获取优惠券详情
 */
function* getCouponDetail(action) {
    try {
        const { result, timeout } = yield race({
            result: call(couponService.getCouponDetail, action.data),
            timeout: call(delay, 3000) //超时时间设为3秒
        })

        if (timeout) {
            return;
        } else {
            if (result.code == 1000) {
                yield put({
                    type: UPDATE_COUPON_DETAIL,
                    data: result.data
                })
            }
            if (action.callback) {
                action.callback(result)
            }
        }
    } catch (error) {
        if (action.callback) {
            action.callback(error)
        }
    }
}

function* watchGetCouponDetail() {
    yield takeLatest(GET_COUPON_DETAIL, getCouponDetail)
}

/**
 * 兑换优惠券
 */
function* addCouponAsync(action) {
    Toast.loading('', 0);
    try {
        const { result, timeout } = yield race({
            result: call(couponService.addCoupon, action.data),
            timeout: call(delay, 15000)
        })
        Toast.hide();
        if (timeout) {
            return;
        } else {
            if (result.code == 1000) {
                //兑换成功 重新获取优惠券列表
                yield put({
                    type: GET_COUPON_LIST,
                    data: {}
                })
            }
            if (action.callback) {
                action.callback(result)
            }
        }
    } catch (error) {
        Toast.hide();
        if (action.callback) {
            action.callback(error)
        }
    }
}

function* watchAddCouponAsync() {
    yield takeLatest(ADD_COUPON_ASYNC, addCouponAsync)
}

export function* watchCoupon() {
    yield [
        fork(watchGetCouponList),
        fork(watchGetCouponDetail),
        fork(watchAddCouponAsync),
    ]
}