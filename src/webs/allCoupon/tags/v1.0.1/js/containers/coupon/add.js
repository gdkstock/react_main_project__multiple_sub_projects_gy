import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Toast } from 'antd-mobile';

//styles
import styles from './add.scss';

//actions
import * as couponActions from '../../actions/couponActions';

//工具类
import common from '../../utils/common';
import { ChMessage } from '../../utils/message.config';

class CouponAdd extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		//设置标题
		common.setViewTitle("兑换礼券")
	}

	send() {
		let code = this.refs.couponCode.value.toUpperCase();
		if (!code) {
			return Toast.info('请输入兑换码');
		}
		this.props.couponActions.addCouponAsync({ code }, res => {
			if (res.code == 1000) {
				Toast.info('兑换礼券成功', 1, () => {
					this.props.router.replace('/coupon/list'); //重定向到列表
				})
			} else {
				Toast.info(res.msg || ChMessage.FETCH_FAILED);
			}
		})
	}


	render() {
		return (
			<div className={styles.container}>
				<div><input ref='couponCode' type="url" name="coupon" placeholder="输入兑换码..." /></div>
				<div className={styles.btn} onClick={() => this.send()}>立即兑换</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
	couponActions: bindActionCreators(couponActions, dispatch),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CouponAdd);