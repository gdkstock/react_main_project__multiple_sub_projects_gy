import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//组件
import { Toast } from 'antd-mobile';

//样式
import styles from './detail.scss';

//工具类
import common from '../../utils/common';
import { ChMessage } from '../../utils/message.config';

class CouponDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            couponInfo: {}
        }
    }

    componentWillMount() {
        //设置标题
        common.setViewTitle("优惠券详情");

        let { coupon, params } = this.props;
        let { couponId } = params;
        let couponInfo = coupon.data[couponId];

        if (!couponInfo) {
            //非法来源 重定向到优惠券列表页面
            this.props.router.replace('/');
        } else {
            this.setState({
                couponInfo
            })
        }
    }

    toUrl(url) {
        if (url) {
            window.location.href = url;
        } else {
            Toast.info(ChMessage.FETCH_FAILED);
        }
    }

    render() {
        let { couponTitle, couponAmount, insertDate, invalidDate, couponExplain, des, type, targetUrl } = this.state.couponInfo;
        // des = '免单'
        return (
            <div className={styles.container}>
                <div className={styles.pannel}>
                    <div className={styles.main}>
                        {
                            des
                                ? <h2>{des}</h2>
                                : <h1>{couponAmount}<span>元</span></h1>
                        }
                        <h3>{couponTitle}</h3>
                        <p>有效期：{insertDate} 至 {invalidDate}</p>
                        {type === 1 && <div className={styles.btn} onClick={() => this.toUrl(targetUrl)}>立即使用</div>}
                    </div>
                    <div className={styles.illustrate}>
                        <div className={styles.leftCircle}></div>
                        <div className={styles.rightCircle}></div>
                        <div className={styles.title}>使用说明</div>
                        <div className={couponExplain ? styles.content : ''} dangerouslySetInnerHTML={{ __html: couponExplain }}></div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    coupon: state.coupon
})

const mapDispatchToProps = dispatch => ({

})

export default connect(
    mapStateToProps
)(CouponDetail);