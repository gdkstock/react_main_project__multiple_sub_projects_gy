import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//工具类
import common from '../../utils/common'

import * as couponActions from '../../actions/couponActions'

//组件
import { Toast, Tabs, Badge } from 'antd-mobile';
import Card from '../../components/coupon/card'
import styles from './list.scss'

const TabPane = Tabs.TabPane;


class CouponList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //选中的tab
            activeKey: ''
        }
    }

    componentWillMount() {
        //设置标题
        common.setViewTitle("优惠券")

        let { coupon } = this.props;

        //默认选中
        let index = this.props.params.index || 0;
        if ([0, 1, 2].indexOf(index * 1) === -1) {
            index = 0;
        }

        this.setState({
            activeKey: sessionStorage.getItem('Coupon_activeKey') || coupon['tab-' + index].key
        })
    }

    componentDidMount() {
        let { couponLength } = this.props;

        //请求数据
        if (couponLength === 0) {
            Toast.loading('', 0);
        }
        this.props.couponActions.getCouponList({}, () => {
            if (couponLength === 0) {
                Toast.hide();
            }
        });
    }

    //跳转到优惠券详情
    toDetail(id) {
        this.props.router.push(`/coupon/detail/${id}`);
    }

    //点击tab
    handleTabClick() {
        // 
    }

    //tab发生改变
    handleTabChange(key) {
        document.body.scrollTop = 0; //滚动到顶部
        sessionStorage.setItem('Coupon_activeKey', key);
        this.setState({
            activeKey: key
        })
    }

    exchangeBtn() {
        // Toast.info('接口开发中，请稍后再测试');
        this.props.router.push('/coupon/add');
    }

    render() {
        let { coupon } = this.props;
        let { activeKey } = this.state;

        return (
            <div className='box'>
                <div className='couponList'>
                    <Tabs defaultActiveKey={activeKey || 'tab-0'} onChange={(key) => this.handleTabChange(key)} onTabClick={() => this.handleTabClick()} swipeable={false}>
                        {
                            Object.keys(coupon).map(key =>
                                <TabPane tab={<Badge>{coupon[key].title}</Badge>} key={key}>
                                    {
                                        coupon[key].list.length > 0
                                            ? coupon[key].list.map((item) => <Card {...item} key={item.couponId} onClick={() => this.toDetail(item.couponId)} />)
                                            : <div className={styles.noCouponContainer}>
                                                <div className={styles.noCouponIcon}></div>
                                                <div>暂无{coupon[key].title}优惠券</div>
                                            </div>
                                    }
                                </TabPane>
                            )
                        }
                    </Tabs>
                    {
                        coupon[activeKey].list.length === 0 && <div className={styles.whiteBg}></div>
                    }
                    <div style={{ height: '1rem' }}></div>
                    <div className={styles.btn} onClick={() => this.exchangeBtn()}>兑换礼券</div>
                </div>
            </div>
        );
    }
}

const coupons = state => ({
    'tab-0': {
        key: 'tab-0',
        title: '可使用',
        list: getCouponList(state, [1])
    },
    'tab-1': {
        key: 'tab-1',
        title: '已使用',
        list: getCouponList(state, [2])
    },
    'tab-2': {
        key: 'tab-2',
        title: '已失效',
        list: getCouponList(state, [3])
    }
})

const getCouponList = (state, numArr) => {
    return state.result.filter(couponId => numArr.indexOf(state.data[couponId].type) > -1).map(couponId => state.data[couponId]);
}

const mapStateToProps = state => ({
    coupon: coupons(state.coupon),
    couponLength: state.coupon.result.length //优惠券数量
})

const mapDispatchToProps = dispatch => ({
    couponActions: bindActionCreators(couponActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CouponList);