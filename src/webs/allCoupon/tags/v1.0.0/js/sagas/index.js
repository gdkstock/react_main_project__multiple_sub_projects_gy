// saga 模块化引入
import { fork } from 'redux-saga/effects'

//优惠券相关
import { watchCoupon } from './coupon'

// 单一进入点，一次启动所有 Saga
export default function* rootSaga() {
  yield [
    fork(watchCoupon),
  ]
}