import { combineReducers } from 'redux'
import user from './users'
import coupon from './coupon'

const rootReducer = combineReducers({
    user, //用户信息表
    coupon, //优惠券
});

export default rootReducer;
