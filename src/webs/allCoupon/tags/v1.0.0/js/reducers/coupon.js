/**
 * 优惠券
 */
/**
 * 我的优惠券
 */

import {
    ADD_COUPON_LIST
} from '../actions/actionsTypes.js'

const init = {
    data: {},
    result: []
}

export default function coupon(state = init, action) {
    switch (action.type) {
        case ADD_COUPON_LIST:
            return {
                data: action.data,
                result: action.result
            };
        default:
            return state;
    }
}