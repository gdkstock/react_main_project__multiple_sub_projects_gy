/*
* 这里定义所有的action类型
* */

/**
 * fetch数据请求相关
 */
export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时

export const UPDATE_ORDER_REMINDER = 'UPDATE_ORDER_REMINDER' //更新催单后的订单信息
export const ORDER_REMINDER = 'ORDER_REMINDER' //催单

//优惠券
export const GET_COUPON_LIST = 'GET_COUPON_LIST' //获取优惠券列表
export const GET_COUPON_DETAIL = 'GET_COUPON_DETAIL' //获取优惠券详情
export const ADD_COUPON_LIST = 'ADD_COUPON_LIST' //添加优惠券列表
export const UPDATE_COUPON_DETAIL = 'UPDATE_COUPON_DETAIL' //更新优惠券信息
export const ADD_COUPON_ASYNC = 'ADD_COUPON_ASYNC' //添加优惠券 兑换优惠券
