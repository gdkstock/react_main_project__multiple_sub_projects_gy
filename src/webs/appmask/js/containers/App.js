import React, { Component } from 'react'
import common from '../utils/common'
import apiHelper from '../services/apiHelper'
import {Carousel} from 'antd-mobile'
import styles from './App.scss'

export default class App extends Component {
	constructor(props) {
        super(props);

        this.state = {
			bannerList:[
				// {
				// 	"eventId":"aaa",
				// 	"imgUrl":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1498708486927&di=3e663c1586ff587fbb55f84623b4960a&imgtype=0&src=http%3A%2F%2Fimg.sj33.cn%2Fuploads%2Fallimg%2F201302%2F1-130201105204.jpg",
				// 	"jumpUrl":"https://m.baidu.com"
				// },{
				// 	"eventId":"bbb",
				// 	"imgUrl":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1498708601524&di=532a6a5a3df33777525c6fab05cdece1&imgtype=0&src=http%3A%2F%2Fimg17.3lian.com%2Fd%2Ffile%2F201702%2F22%2F1005a2e0825ffe290b3f697404ee8038.jpg",
				// 	"jumpUrl":"https://m.baidu.com"
				// }
			]
        }
    }

	componentWillMount(){
		let searchObj = common.changeSearchToObject(window.location.search);
		let position = searchObj.position;
		let isLogin = searchObj.isLogin;
		let requestParam = {
			url:`${apiHelper.baseApiUrl}advert/getAdvert/${position}/${isLogin}`,//测试 http://192.168.1.164:8087 正式: http://mobile.cx580.com:14443
			data:{
				method:"post"
			}
		};
		apiHelper.fetch(requestParam).then(result=>{
			//将数据转换成我们需要的格式
			console.log(result)
			if(result.code=="1000"){
				this.setState({
					bannerList:result.data.AdList
				})
			}
		})
	}
  
  	jumpTo(item){
  		common.closeAppView();
  		common.openNewBrowserWithURL(item.jumpUrl);
  	}

	render() {
		let bannerList = this.state.bannerList || [];
    	let canPlay = bannerList.length>1?true:false;
    	let dots = bannerList.length>1
		return (
		  <div className={styles.wrap}>
		  	{bannerList.length>0 &&
                <Carousel 
                    dots={dots} 
                    autoplay={ false } 
                    infinite={ canPlay } 
                    swiping={ canPlay }
                >
                    {
                        bannerList.map((item)=>
                            <img 
                            	key={item.eventId}
                                src={item.imgUrl}  
                                className={styles.banner}
                                onClick={()=>this.jumpTo(item)}
                            />    
                        )
                    }
                </Carousel>
            } 
		  </div>
		)
	}
}