import jsApi from './cx580.jsApi';

class Common {

    /**
     * 获取当前网址的根路径 返回的数据为：http.../#/
     */
    getRootUrl() {
        let this_url = window.location.href; //当前网址
        let substrNum = this_url.indexOf('#') + 1; //获取到哈希路由当前的位置 + 1
        let url = this_url.substr(0, substrNum) + '/';
        return url;
    }

    /**
     * 将键值对转为URL参数
     */
    _toQueryPair(key, value) {
        ///<summary>将键值对转为URL参数</summary>
        if (typeof value == 'undefined') {
            return key;
        }
        return key + '=' + encodeURIComponent(value === null ? '' : String(value));
        //return key + '=' + (value == null ? '' : String(value));
    }

    /**
     * 将对象转为URL参数
     */
    toQueryString(obj) {
        var ret = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                key = encodeURIComponent(key);
                var values = obj[key];
                if (values && values.constructor === Array) { //数组 
                    var queryValues = [];
                    for (var i = 0, len = values.length, value; i < len; i++) {
                        value = values[i];
                        queryValues.push(this._toQueryPair(key + '[' + i + ']', value));
                    }
                    ret = ret.concat(queryValues);
                } else { //字符串 
                    ret.push(this._toQueryPair(key, values));
                }
            }
        }
        return ret.join('&');
    }

    /**
     * 设置浏览器标题 兼容IOS 后退title不修改的bug
     */
    setViewTitle(title) {
        let body = document.getElementsByTagName('body')[0];
        document.title = title;
        if (navigator.userAgent.indexOf("AlipayClient") !== -1) {
            AlipayJSBridge.call("setTitle", {
                title: title
            });
        }
        let iframe = document.createElement("iframe");
        iframe.setAttribute("src", "/favicon.ico");
		iframe.style.display = 'none';
        iframe.addEventListener('load', function () {
            setTimeout(() => {
                document.body.removeChild(iframe);
            }, 10);
        });
        document.body.appendChild(iframe);
    }

    /**
     * 检测是否为车行易app
     */
    isCXYApp() {
        let isCXYApp = navigator.userAgent.indexOf('appname_cxycwz') > -1 ? true : false;
        return isCXYApp;
    }

    /**
     * 获取安卓系统的版本号 非安卓手机则返回false
     */
    getAndroidVersion() {
        let re = /Android\s([^;]+)/ig;
        let _version = re.exec(navigator.userAgent);
        if (_version) {
            _version = _version[1];
        } else {
            _version = false;
        }
        return _version;
    }

    browser = function () {
        let u = navigator.userAgent;
        //app = navigator.appVersion;
        return {
            versions: { //移动终端浏览器版本信息 
                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端 
                iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器 
                iPad: u.indexOf('iPad') > -1, //是否iPad 
                android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1 //android终端或uc浏览器 
            }
        }
    }

    /**
     * 获取APP的版本号
     */
    getAppVersion = function () {
        let u = navigator.userAgent;
        if (u.indexOf('appname_cxycwz/') > -1) {
            return u.substr(u.indexOf('appname_cxycwz/') + 15);
        }
        return false;
    }

    /**
     * 检测当前浏览器是否为iPhone(Safari)
     */
    isIPhone = function () {
        let browser = this.browser();
        if (browser.versions.iPhone || browser.versions.iPad || browser.versions.ios) {
            return true;
        }
        return false;
    };

    /**
     * 检测当前浏览器是否为Android(Chrome)
     */
    isAndroid = function () {
        let browser = this.browser();
        if (browser.versions.android) {
            return true;
        }
        return false;
    };

    /**
     * 自定义title文案
     */
    setTitleText(text) {
        if (this.isCXYApp()) {
            jsApi.call({
                "commandId": "",
                "command": "customizeTitleText",
                "data": {
                    "titleText": text,
                }
            }, function (data) { });
        } else {
            document.querySelector("title").innerHTML = text;
        }
    }

    /**
     * 判断当前系统是否需要追加20px的头部像素，用于兼容APP的头部沉浸模式
     */
    needAddAppHeaderHeight() {
        if (!this.isCXYApp()) {
            return false; //非车行易APP不用增加头部高度
        }
        let version = this.getAndroidVersion();
        if (version) {
            version = version.split(".");
            if ((version[0] * 1) < 5 && (version[1] * 1) < 4) {
                return false; //4.4以下的系统不用添加头部高度
            }
        }
        return true;
    }

    /**
     * 打开APP新视图
     * @param url 打开的URL
     * @param showTitleLayout 是否需要显示顶部导航条 默认显示
     */
    openNewBrowserWithURL(url, showTitleLayout = 'true') {

        if (url.substr(0, 4) !== 'http') {
            url = this.getRootUrl() + url; //如果不是外网，则自动加载网址
        }

        console.log(this.isCXYApp())
        if(this.isCXYApp()){
            try {
                jsApi.call({
                    "commandId": "",
                    "command": "openNewBrowserWithURL",
                    "data": {
                        "url": url,
                        "umengId": "cfw_youkachongzhi",
                        "showTitleLayout": showTitleLayout //是否显示到导航栏
                    }
                }, function (data) {

                });
            } catch (error) {
                // alert("123")
            }
        }else{
            //执行到这里，说明不在 app 中运行
            window.location.href = url;
        }
    }

    /**
     * 关闭APP视图
     */
    closeAppView() {
        if (!this.isCXYApp()) {
            window.history.back();
            return;
        }
        try {
            jsApi.call(
                {
                    "commandId": "",
                    "command": "close"
                }
            )
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            window.history.back();
        }
    }

    /**
     * 拍照接口
     */
    picture(callback) {
        if (this.isCXYApp()) {
            if (!this.callbackPicture()) {
                return;
            }
            jsApi.call({
                "commandId": "",/*命令主键，每次调用，必须用全局唯一的字符串*/
                "command": "picture",/*执行的命令，原生APP会根据此值执行相应的逻辑*/
                "data": {
                    "quality": 75,/*图片质量, 取值1到100*/
                    "maxWidth": 1024,/*图片的最大宽度. 过大将被等比缩小*/
                    "maxHeight": 1024,/*图片的最大高度. 过大将被等比缩小*/
                    "maxSize": 300
                }
            }, function (result) {
                callback(result.data.dataBase64); //回调函数
            });
        }
    }

    /**
     * 判断是否需要调用APP的图片上传接口
     */
    callbackPicture() {
        if (this.isCXYApp()) {
            if (this.isIPhone()) {
                return true;
            } else if (this.isAndroid()) {
                let version = this.getAppVersion();
                if (version) {
                    version = version.split(".");
                    if (version.length > 2 && (version[0] * 1) > 5 && (version[2] * 1) > 1) {
                        return true; //安卓6.0.2及以上版本才调用APP图片上传接口
                    }
                }
            }
        }
        return false;
    }

    /**
     * 关闭安卓加载中
     */
    controlLoading() {
        if (this.isCXYApp()) {
            // jsApi.call({
            //     "commandId": "",/*命令主键，每次调用，必须用全局唯一的字符串*/
            //     "command": "controlLoading",/*执行的命令，原生APP会根据此值执行相应的逻辑*/
            //     "data": {
            //         "status": "close"
            //     }
            // }, function (data) { });
        }
    }

    /*
     * 将查询字符串转换为对象
     */
    changeSearchToObject(search){
        let arr = search.trim().slice(1).split("&")
        let outPutObj ={}
        arr.map(item=>{
            let tmp = item.split("=")
            outPutObj[tmp[0]] = tmp[1];
        }) 
        return outPutObj
    }

};

// 实例化后再导出
export default new Common()