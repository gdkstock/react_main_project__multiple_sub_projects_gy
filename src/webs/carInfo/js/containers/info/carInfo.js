import React, { Component } from 'react';

//styles
import styles from './carInfo.scss';

//常用工具类
import common from '../../utils/common';

class CarInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [
                {
                    id: 1,
                    title: '热门推荐',
                    leftBoxContent: [
                        {
                            title: '新车报价',
                            subTitle: '精准报价',
                            imgSrc: './images/图片尺寸2@2x.png',
                            imgAlt: '250*120'
                        }
                    ],
                    rightTopBoxContent: [
                        {
                            title: '低价车险',
                            subTitle: '超低价 超实惠',
                            imgSrc: './images/图片尺寸1@2x.png',
                            imgAlt: '250*100'
                        }
                    ],
                    rightBottomBoxContent: [
                        {
                            title: '油卡办理',
                            subTitle: '快速代办',
                            three: '手机流量',
                            four: '秒速到账'
                        }
                    ]
                },
                {
                    id: 2,
                    title: '车市行情',
                    leftBoxContent: [
                        {
                            title: '高价卖车',
                            subTitle: '安全可靠',
                            imgSrc: './images/图片尺寸2@2x.png',
                            imgAlt: '250*120'
                        }
                    ],
                    rightTopBoxContent: [
                        {
                            title: 'SUV大全',
                            subTitle: '销售排行报价',
                            imgSrc: './images/图片尺寸1@2x.png',
                            imgAlt: '250*100'
                        }
                    ],
                    rightBottomBoxContent: [
                        {
                            title: '二手好车',
                            subTitle: '享一年质保',
                            three: '新车报价',
                            four: '最快一天成交'
                        }
                    ]
                },
                {
                    id: 3,
                    title: '车服务',
                    leftBoxContent: [
                        {
                            title: '一嗨租车',
                            subTitle: '首租立减100元',
                            imgSrc: './images/图片尺寸2@2x.png',
                            imgAlt: '250*120'
                        }
                    ],
                    rightTopBoxContent: [
                        {
                            title: '驾照查分',
                            subTitle: '剩余积分查询',
                            imgSrc: './images/图片尺寸1@2x.png',
                            imgAlt: '250*100'
                        }
                    ],
                    rightBottomBoxContent: [
                        {
                            title: '预约代驾',
                            subTitle: 'E代驾',
                            three: '违章高发',
                            four: '（敬请期待）'
                        }
                    ]
                }
            ]
        }
    }

    componentDidMount() {


    }

    render() {

        return (
            <div className={styles.carInfo}>
                <div className={styles.banner}>
                    <img src='./images/图片尺寸1@2x.png' alt='广告位占位' />
                </div>
                {this.state.data.map((boxItem) =>
                    <div key={boxItem.id} className={styles.infoBox}>
                        {/* 顶部 */}
                        <div className={styles.title + ' ' + styles.lineB}>{boxItem.title}</div>
                        {/* 左边 */}
                        <div className={styles.leftBox + ' ' + styles.lineR}>
                            <div className={styles.itemBox}>
                                <span>{boxItem.leftBoxContent[0].title} </span>
                                <i>{boxItem.leftBoxContent[0].subTitle}</i>
                            </div>
                            <img src={boxItem.leftBoxContent[0].imgSrc} alt={boxItem.leftBoxContent[0].imgAlt} />
                        </div>
                        {/* 右边 */}
                        <div className={styles.rightTopBox + ' ' + styles.lineB}>
                            <div className={styles.itemBox}>
                                <span>{boxItem.rightTopBoxContent[0].title}</span>
                                <i className={boxItem.rightTopBoxContent[0].subTitle == '销售排行报价' ? styles.orange : styles.gray}>{boxItem.rightTopBoxContent[0].subTitle}</i>
                            </div>
                            <div className={styles.itemBox}>
                                <img src={boxItem.rightTopBoxContent[0].imgSrc} alt={boxItem.rightTopBoxContent[0].imgAlt} />
                            </div>
                        </div>
                        <div className={styles.rightBottomBox}>
                            <div className={styles.itemBox + ' ' + styles.lineR}>
                                <span>{boxItem.rightBottomBoxContent[0].title} </span>
                                <i>{boxItem.rightBottomBoxContent[0].subTitle}</i>
                            </div>
                            <div className={styles.itemBox}>
                                <span>{boxItem.rightBottomBoxContent[0].three}</span>
                                <i className={boxItem.rightBottomBoxContent[0].four == '秒速到账' ? styles.orange : styles.gray}>{boxItem.rightBottomBoxContent[0].four}</i>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

export default CarInfo