import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import CarInfo from './info/carInfo'

class Home extends Component {
	constructor(props) {
		super(props);

	}

	render() {
		return (
			<div className="box">
				<CarInfo/>
			</div>
		)
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({

})
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);