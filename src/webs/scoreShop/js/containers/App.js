import React, { Component } from 'react'
import common from '../utils/common'
import apiHelper from '../services/apiHelper'
import userHelper from '../utils/userHelper'
import { Toast } from 'antd-mobile'

window.queryUrl = () => {
	Toast.loading("", 0)
	let requestParam = {
		url: apiHelper.baseApiUrl + "point/buildUrl",
		data: {
			method: "post"
		}
	};
	apiHelper.fetch(requestParam).then(result => {
		Toast.hide()
		if (result.code == '1000') {
			window.location.replace(JSON.parse(result.data).url)
		}else if(result.code == '2222'){
			// userHelper.Login();
		}
	}).catch((e) => {
		Toast.hide()
		window.networkError('./images/networkError-icon.png')
	})
}

export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {

		}
	}

	componentWillMount() {
		document.querySelector("title").innerHTML = "车主积分商城"
		
	}

	componentDidMount(){
		window.queryUrl(); // 请求数据
	}

	render() {

		return (
			<div></div>
		)
	}
}