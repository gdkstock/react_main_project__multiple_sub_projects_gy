/*
* 发票详情
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as invoiceActions from '../../actions/invoiceActions'
import { Toast } from 'antd-mobile'
import Btn from '../../components/Btn'
import TxtGoup from '../../components/Form/txtGroup'
import { BtnGoup , ItemTitle , CommonTitle } from '../../components/invoiceInfo'
import apiHelper from '../../services/apiHelper';
import common from '../../utils/common'
import styles from './add.scss'
import jsApi from '../../utils/cx580.jsApi'

class InvoiceDetail extends React.Component{
	constructor(props){
		super(props);

        let initState = {
            invoiceStatus:2,//发票状态 1 处理中，2 已开票
            createTime:"2017-06-23 17:00", //开票时间
            invoiceType:2, //发票类型（个人 1，单位 2）
            invoiceName:"无", //发票抬头
            invoiceContent:"代理服务费", //发票内容
            invoiceAmount: 0,  //发票金额
            taxpayerId:'无', //纳税人识别码
            bankAcount:'无', //开户行及账号
            addressPhone:'无', //地址、电话
            remark:'无', //备注
            invoiceImgUrl:"./images/img-invoice.png",//发票图片
		}
        console.log("props:",this.props)
        let { invoiceList } = this.props;
        let invoiceId = this.props.params.invoiceId;
        let state = invoiceList.data[invoiceId] || {};
        console.log(invoiceId,state)
		//初始化状态
		this.state={...initState,...state}	
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('发票详情');

        //回到栈顶的回调
        window.cxyPageResumeCallBack = () => {      
            let isChangeUser = localStorage.getItem("isChangeUser");
            if(isChangeUser){
                window.history.back()//切换账号后退上一级页面
            }
        }

        //设置菜单栏右侧按钮
        try{
            jsApi.call({
                "commandId":"",
                "command":"customizeTopRightButton",
                "data":{
                    "reset":"false",
                    "list":[]
                }
            },
            function(data){
                
            })
        }catch(e){

        }
	}

    componentWillReceiveProps(nextProps){
		//组件跟新时，初始化状态
        console.log("nextProps",nextProps)
        let { invoiceList } = nextProps
        let invoiceId = nextProps.params.invoiceId;
        let state = invoiceList.data[invoiceId] || {};
		this.setState({ ...this.state ,...state })
	}

	componentDidMount(){
		//进入发票页面埋点
		// common.sendCxytj({
		// 	eventId:"Violation_EnterAddCar"
		// })
	}

	componentWillUnmount(){
		//添加车辆页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
	}

	render(){
        console.log(this.state)

        //发票抬头porps
        const invoiceNameProps = {
            groupId:"invoiceName" ,//唯一编码
            label:'发票抬头',//标签文本
            placeholder:'请输入您的姓名' ,//占位符文本
            type:"text" ,//输入框type
            hasIcon:false,//是否有提示icon
            value:this.state.invoiceName ,//输入框值
            disabled:true,//输入框是否可输入
            isNecessary:false,//是否必须
        }

        //纳税人识别号porps
        const taxpayerIdProps = {
            groupId:"taxpayerId" ,
            label:'纳税人识别号',
            placeholder:'请填写纳税人识别号' ,
            type:"text" ,
            hasIcon:false,
            value:this.state.taxpayerId || "无",
            disabled:true,
            isNecessary:false,
        }

        //发票内容porps
        const invoiceContentProps = {
            groupId:"invoiceContent" ,
            label:'发票内容',
            placeholder:'' ,
            type:"text" ,
            hasIcon:false,
            value:this.state.invoiceContent ,
            disabled:true,
            isNecessary:false,
        }

        //发票金额porps
        const invoiceAmountProps = {
            groupId:"invoiceAmount" ,
            label:'发票金额',
            placeholder:'' ,
            handleChange:()=>{} ,
            type:"text" ,
            hasIcon:false,
            value:"￥"+(this.state.invoiceAmount*1).toFixed(2) ,
            disabled:true,
            isNecessary:false,
        }

        //备注porps
        const remarkProps = {
            groupId:"remark" ,
            label:'备注说明',
            placeholder:'请填写备注说明' ,
            type:"text" ,
            hasIcon:false,
            value:this.state.remark || "无",
            disabled:true,
            isNecessary:false,
        }

        //地址电话props
        const addressPhoneProps = {
            groupId:"addressPhone" ,
            label:'地址、电话',
            placeholder:'请填写注册地址及电话' ,
            type:"text" ,
            hasIcon:false,
            value:this.state.addressPhone || "无",
            disabled:true,
            isNecessary:false,
        }

        //开户行及账号props
        const bankAcountProps = {
            groupId:"bankAcount" ,
            label:'开户行及账号',
            placeholder:'请填写开户行及账号' ,
            type:"text" ,
            hasIcon:false,
            value:this.state.bankAcount || "无",
            disabled:true,
            isNecessary:false,
        }

		return (
            <div className={styles.detailWrap}>
                <CommonTitle
                    invoiceStatus = {this.state.invoiceStatus}
                    createTime = {this.state.createTime}
                    invoiceImg={this.state.invoiceImgUrl}
                />
                <div className="fp_whiteSpace_24"></div>
                <div className="wrap">
                    <ItemTitle 
                        text="发票详情"
                        hasLine = {true}
                    />
                    <div className='wz_list'>
                        <TxtGoup {...invoiceNameProps}/>
                        { this.state.invoiceType == 2 && <TxtGoup {...taxpayerIdProps}/> }
                        <TxtGoup {...invoiceContentProps}/>
                        <TxtGoup {...invoiceAmountProps}/>
                    </div>
                </div>
                { this.state.invoiceType == 2 && <div className="fp_whiteSpace_24"></div> }
                {
                    this.state.invoiceType == 2 && <div className="wrap">
                        <ItemTitle 
                            text="更多信息（选填）"
                            hasLine = {true}
                        />
                        <div className='wz_list'>
                            <TxtGoup {...remarkProps}/>
                            <TxtGoup {...addressPhoneProps}/>
                            <TxtGoup {...bankAcountProps}/>
                        </div>
                    </div>
                }
			</div>
		)
	}
}

const mapStateToProps = state => ({
    invoiceList:state.invoiceList
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(invoiceActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(InvoiceDetail)