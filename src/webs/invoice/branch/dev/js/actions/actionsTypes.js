/*
* 这里定义所有的action类型
* */

/**
 * 异步请求
 */
export const QUERY_ORDER_LIST = 'QUERY_ORDER_LIST' // 请求可开票订单列表
export const ADD_ORDER_LIST = 'ADD_ORDER_LIST' // 添加到订单列表

export const QUERY_ADD_INVOICE = 'QUERY_ADD_INVOICE' //请求提交发票数据
export const QUERY_INVOICE_RECORD = 'QUERY_INVOICE_RECORD' //查询发票历史纪录
export const QUERY_INVOICE_DETAIL = 'QUERY_INVOICE_DETAIL' //请求发票详情

export const ADD_RECORD_LIST = 'ADD_RECORD_LIST' //添加列表数据到发票历史列表
export const ADD_RECORD = 'ADD_RECORD' //添加单条数据到发票历史列表

export const IS_LOADING_RECORD = 'IS_LOADING_RECORD' //正在请求历史纪录
export const FINISH_LOADING_RECORD = "FINISH_LOADING_RECORD" //请求历史纪录完成








