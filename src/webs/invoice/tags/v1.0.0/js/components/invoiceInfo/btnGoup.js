/*
    按钮列表
*/
import styles from './btnGoup.scss'

const defaultProps = {
    invoiceType:2, //发票类型
    invoiceTypeChange:()=>{},
}

const BtnGoup = (props)=>{
    // props = defaultProps;
    let leftBtnClass = props.invoiceType == 1 ? (styles.btn+" "+styles.btnActive) : (styles.btn+" "+styles.btnDefault)
    let rightBtnClass = props.invoiceType == 2 ? (styles.btn+" "+styles.btnActive) : (styles.btn+" "+styles.btnDefault)
    return (
        <div className={styles.btnGoup}>
            <div className={leftBtnClass} onClick={()=>props.invoiceTypeChange(1)}>个人</div>
            <div className={rightBtnClass} onClick={()=>props.invoiceTypeChange(2)}>单位</div>
        </div>
    )
}

export default BtnGoup