/*
* 开发票
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as invoiceActions from '../../actions/invoiceActions'
import { Toast } from 'antd-mobile'
import Btn from '../../components/Btn'
import TxtGoup from '../../components/Form/txtGroup'
import { BtnGoup , ItemTitle } from '../../components/invoiceInfo'
import { Modal } from '../../components'
import apiHelper from '../../services/apiHelper';
import common from '../../utils/common'
import styles from './add.scss'
import jsApi from '../../utils/cx580.jsApi'

class AddInvoice extends React.Component{
	constructor(props){
		super(props);

        console.log(this.props.params.fine)
		//初始化状态
        let sessionState = sessionStorage.getItem("addInvoiceState")
        if(sessionState){
            this.state = JSON.parse(sessionState)
        }else{
    		this.state={
                invoiceType:1, //发票类型（个人 1，单位 2）
                personName:"",//个人姓名
                companyName:"",//单位名字
                invoiceContent:"代理服务费", //发票内容
                invoiceAmount:this.props.params.fine || "",  //发票金额
                taxpayerId:'', //纳税人识别码
                bankAcount:'', //开户行及账号
                addressPhone:'', //地址、电话
                remark:'', //备注
    		}	
        }
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('按服务开票');

        window.cxyPageResumeCallBack = () => {      
            let isChangeUser = localStorage.getItem("isChangeUser");
            if(isChangeUser){
                window.history.back()//切换账号后退上一级页面
            }
        }

        //设置菜单栏右侧按钮
        try{
            jsApi.call({
                "commandId":"",
                "command":"customizeTopRightButton",
                "data":{
                    "reset":"false",
                    "list":[]
                }
            },
            function(data){
                
            })
        }catch(e){

        }
	}

	componentDidMount(){
		//进入发票页面埋点
		// common.sendCxytj({
		// 	eventId:"Violation_EnterAddCar"
		// })
	}

    componentDidUpdate(){
        sessionStorage.setItem("addInvoiceState",JSON.stringify(this.state))
    }

	componentWillUnmount(){
		//添加车辆页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
        
        let modal= document.getElementById("promptMsg");
        modal && document.body.removeChild(modal);
	}

    //发票类型改变
    invoiceTypeChange(invoiceType){
        this.setState({
            invoiceType
        })
    }

    //点击开票按钮
    invoiceBtnClick(){
        console.log(this.valiDate())
        if(this.valiDate()){
            let obj = {
                show: true, //显示对话框
                content: '确认信息并开取发票', //内容
                leftBtn: {
                    name: '取消', //左按钮文字
                    onClick: () => {}  //点击左按钮
                },
                rightBtn: {
                    name: '确认',//右按钮文字
                    onClick: () =>this.submitInvoiceDate() //点击右按钮
                },
                onClose: () =>{} //点击关闭按钮
            }
            Modal.confirm(obj);
        }
    }

    //提交发票数据
    submitInvoiceDate(){
        console.log(this.state)
        let param = {};
        let { invoiceType , invoiceContent , invoiceAmount , taxpayerId , bankAcount , addressPhone , remark , personName , companyName } = this.state
        if(this.state.invoiceType == 2){
            param = {invoiceType , invoiceContent , invoiceAmount, taxpayerId,bankAcount,addressPhone,remark , invoiceName :companyName }
        }else{
            param = { invoiceType , invoiceContent , invoiceAmount , invoiceName:personName }
        }
        param.orders = sessionStorage.getItem("selectedIdList")
        console.log("提交参数param：",param)
        this.props.actions.queryAddInvoice(param)
    }

    //输入改变
    preValiDate(e){
        console.log(e.target.value)
        let value = e.target.value,
            name = e.target.name;
            console.log(name,value)
        this.setState({
            [name]:value  
        })
    }

    //验证数据
    valiDate(){
        let {
            invoiceType,
            personName,
            companyName,
            invoiceContent,
            invoiceAmount,
            taxpayerId,
            bankAcount,
            addressPhone,
            remark,
        } = this.state

        console.log(this.state)

        if(invoiceType == 1){
            //判断发票抬头
            if(personName.length == 0){
                Toast.info("请输入您的姓名",2)
                return false
            }

        }else{
            //判断发票抬头
            if(companyName.length == 0){
                Toast.info("请输入单位名称",2)
                return false
            }

            //判断纳税人识别号
            if(taxpayerId.length == 0 ){
                Toast.info("请输入纳税人识别号",2)
                return false
            }else if(taxpayerId.length < 10){
                Toast.info("纳税人识别号应不小于10位",2)
                return false
            }
        }

        return true
    }

	render(){

        //发票抬头porps
        const invoiceNameProps = {
            groupId: this.state.invoiceType==1?"personName":"companyName" ,//唯一编码
            label:'发票抬头',//标签文本
            placeholder:this.state.invoiceType==1?'请输入您的姓名':"请输入单位名称" ,//占位符文本
            handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
            type:"text" ,//输入框type
            hasIcon:false,//是否有提示icon
            value:this.state.invoiceType==1?this.state.personName :this.state.companyName,//输入框值
            disabled:false,//输入框是否可输入
            isNecessary:true,//是否必须
            maxLen:20,
        }

        //纳税人识别号porps
        const taxpayerIdProps = {
            groupId:"taxpayerId" ,
            label:'纳税人识别号',
            placeholder:'请填写纳税人识别号' ,
            handleChange:(e)=>this.preValiDate(e) ,
            type:"text" ,
            hasIcon:false,
            value:this.state.taxpayerId ,
            disabled:false,
            isNecessary:true,
        }

        //发票内容porps
        const invoiceContentProps = {
            groupId:"invoiceContent" ,
            label:'发票内容',
            placeholder:'' ,
            handleChange:(e)=>this.preValiDate(e),
            type:"text" ,
            hasIcon:false,
            value:this.state.invoiceContent ,
            disabled:true,
            isNecessary:false,
        }

        //发票金额porps
        const invoiceAmountProps = {
            groupId:"invoiceAmount" ,
            label:'发票金额',
            placeholder:'' ,
            handleChange:(e)=>this.preValiDate(e) ,
            type:"text" ,
            hasIcon:false,
            value:"￥"+(this.state.invoiceAmount*1).toFixed(2) ,
            disabled:true,
            isNecessary:false,
        }

        //备注porps
        const remarkProps = {
            groupId:"remark" ,
            label:'备注说明',
            placeholder:'请填写备注说明' ,
            handleChange:(e)=>this.preValiDate(e),
            type:"text" ,
            hasIcon:false,
            value:this.state.remark,
            disabled:false,
            isNecessary:false,
            maxLen:20,
        }

        //地址电话props
        const addressPhoneProps = {
            groupId:"addressPhone" ,
            label:'地址、电话',
            placeholder:'请填写注册地址及电话' ,
            handleChange:(e)=>this.preValiDate(e) ,
            type:"text" ,
            hasIcon:false,
            value:this.state.addressPhone,
            disabled:false,
            isNecessary:false,
            maxLen:50,
        }

        //开户行及账号props
        const bankAcountProps = {
            groupId:"bankAcount" ,
            label:'开户行及账号',
            placeholder:'请填写开户行及账号' ,
            handleChange:(e)=>this.preValiDate(e) ,
            type:"text" ,
            hasIcon:false,
            value:this.state.bankAcount,
            disabled:false,
            isNecessary:false,
            maxLen:50,
        }

		return (
			<div>
                <div className={styles.fixedWindow}>
                    <div className="wrap">
                        <ItemTitle text="发票类型"/>
                        <BtnGoup 
                            invoiceType={this.state.invoiceType} 
                            invoiceTypeChange = {(invoiceType)=>this.invoiceTypeChange(invoiceType)}
                        />

                        <ItemTitle 
                            text="发票详情"
                            hasLine = {true}
                            hasPromptIcon = {this.state.invoiceType == 2 }
                            promptImg = "./images/img-invoice.png"
                        />
                        <div className='wz_list'>
                            <TxtGoup {...invoiceNameProps}/>
                            { this.state.invoiceType == 2 && <TxtGoup {...taxpayerIdProps}/> }
                            <TxtGoup {...invoiceContentProps}/>
                            <TxtGoup {...invoiceAmountProps}/>
                        </div>
                    </div>
                    { this.state.invoiceType == 2 && <div className="fp_whiteSpace_24"></div>}
                    {
                        this.state.invoiceType == 2 && <div className="wrap">
                            <ItemTitle 
                                text="更多信息（选填）"
                                hasLine = {true}
                                hasPromptIcon = {true}
                                promptImg = "./images/img-invoice.png"
                            />
                            <div className='wz_list'>
                                <TxtGoup {...remarkProps}/>
                                <TxtGoup {...addressPhoneProps}/>
                                <TxtGoup {...bankAcountProps}/>
                            </div>
                        </div>
                    }
                </div>
                <div className={styles.fixedBottomBtn}>
                    <Btn text="开发票" handleClick={()=>this.invoiceBtnClick()}/>
                </div>
			</div>
		)
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(invoiceActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddInvoice)