/*
* 发票首页
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as invoiceActions from '../../actions/invoiceActions'
import { Toast } from 'antd-mobile'
import apiHelper from '../../services/apiHelper';
import common from '../../utils/common'
import { TopHeader , List , FootBar } from '../../components/orderList'
import styles from './index.scss'
import jsApi from '../../utils/cx580.jsApi'
import userHelper from '../../utils/userHelper'
import LoadingMore from 'app/components/common/loadingMore'

class OrderList extends React.Component{
	constructor(props){
		super(props);

		console.log(this.props)
		//初始化状态
		let sessionState = sessionStorage.getItem("orderListState") // 获取缓存中的state
		if(sessionState){
			this.state = JSON.parse(sessionState)
		}else{
			this.state = this.getNextState({},this.props);
		}
	}

	//获取新状态
	getNextState(state,props){
		let orderListDate = props.orderList;
		let { data , result , totalInvoiceAmount , invoiceExplain } = orderListDate;
		return {
			...state,
			data,
			result,
			totalInvoiceAmount,
			invoiceExplain,
			selectedId:[],
			selectedInvoiceAmount:0,
			selectedNum:0,
			isSelectedAll:false,
			isFetching:true,
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('发票');

		let isChangeUser = localStorage.getItem("isChangeUser");
		if(isChangeUser){
	       	this.props.actions.queryOrderList();
	       	localStorage.removeItem("isChangeUser");
	    }

		window.cxyPageResumeCallBack = () => {
			let isChangeUser = localStorage.getItem("isChangeUser");
			if(isChangeUser){
		       	localStorage.removeItem("isChangeUser");
		    }		
			this.props.actions.queryOrderList();
		}

		//请求可开票订单列表
		let sessionState = sessionStorage.getItem("orderListState") // 获取缓存中的state
		if(sessionState){
			sessionStorage.removeItem("orderListState")
		}else{
			setTimeout(()=>{
				try {
					jsApi.call({
						"commandId": "",
						"command": "getSymbol",
						"data": {
							"city": "",
							"accountId": "",
							"token": "",
						}
					}, (data) => {
						if(data.data.accountId){
							sessionStorage.setItem("userId",data.data.accountId);
							sessionStorage.setItem("token",data.data.token);
							sessionStorage.setItem("city",data.data.city);
							sessionStorage.setItem("userType","App");
							sessionStorage.setItem("authType","App");
						}
						this.props.actions.queryOrderList()
					});
				} catch (error) {
					//执行到这里，说明不在 app 中运行
					// alert("调试信息：调用APP JS SDK出错了 获取APP信息出错了");		
				}
			},250)
		}

		//定义菜单栏右上角开票历史按钮点击回调
		window.openInvoiceRecord =  () => {
			sessionStorage.setItem("orderListState",JSON.stringify(this.state)) //缓存state
			this.props.router.push("invoiceRecord");
		}

		//设置菜单栏右侧按钮
		try{
			jsApi.call({
				"commandId":"",
				"command":"customizeTopRightButton",
				"data":{
					"reset":"false",
					"list":[
						{
							"iconUrl": "",
							"text": "发票历史",
							"callbackMethodName": "openInvoiceRecord"
						}
					]}
				},
				function(data){
					
				}
			)
		}catch(e){

		}

	}

	componentDidMount(){
		//进入发票页面埋点
		// common.sendCxytj({
		// 	eventId:"Violation_EnterAddCar"
		// })
	}

	componentWillReceiveProps(nextProps){
		//组件跟新时，初始化状态
		let nextState = this.getNextState(this.state,nextProps);
		this.setState({...nextState,isFetching:false})
	}

	componentWillUnmount(){
		//添加车辆页面信息
        sessionStorage.setItem("prevPageInfo", document.title);
        let modal= document.getElementById("modal");
        modal && document.body.removeChild(modal);
	}

	//选项改变时
	selectedChange(item){
		console.log(this.state)

		let { orderId } = item;

		//改变总订单数量和
		let { selectedNum , selectedInvoiceAmount , isSelectedAll ,selectedId } = this.state 
		if(selectedId.indexOf(item.orderId) >-1 ){//从选中切换到不选
			selectedInvoiceAmount -= item.orderInvoiceAmount*1
			selectedNum>0 ? selectedNum-- : ( selectedNum = 0 )
			let i = selectedId.indexOf(orderId)
			selectedId.splice(i,1);
		}else{
			selectedInvoiceAmount += item.orderInvoiceAmount*1
			selectedNum++;
			selectedId.push(item.orderId)
		}

		isSelectedAll = this.judgeSelectedAll(this.state.result,selectedId);

		this.setState({
			selectedNum,
			selectedInvoiceAmount,
			selectedId,
			isSelectedAll,
		})
	}

	//全选按钮改变时
	selectedAllChange(){

		let isSelectedAll = !this.state.isSelectedAll,
		    { result , data , totalInvoiceAmount } = this.state,
			selectedId = isSelectedAll ? [...result] :[],
			selectedNum = isSelectedAll ? result.length : 0 ,
			selectedInvoiceAmount = isSelectedAll ? totalInvoiceAmount : 0 ;
		this.setState({
			isSelectedAll:!this.state.isSelectedAll,
			selectedNum,
			selectedInvoiceAmount,
			selectedId,
		})	

	}

	//判断全选按钮是否选中
	judgeSelectedAll(allList,selectedList){
		if(allList.length == selectedList.length){
			return true
		}else{
			return false
		}
	}

	//下一步
	goNextStep(){
		let selectedId = this.state.selectedId;
		let selectedIdList = selectedId.map(item=>{
			return {
				orderId:item
			}
		})
		sessionStorage.setItem("selectedIdList",JSON.stringify(selectedIdList));//缓存选中的订单列表id
		sessionStorage.setItem("orderListState",JSON.stringify(this.state));//缓存state
		
		let fine = this.state.selectedInvoiceAmount //选中的订单总计开票金额
		this.props.router.push("invoiceInfo/add/"+fine) //跳转到开票页面
	}

	//生成顶部组件props
	createTopHeaderProps(state){
		return {
			totalInvoiceAmount:state.totalInvoiceAmount,
			invoiceExplain:state.invoiceExplain,
		}
	}

	//生成订单列表组件props
	createListProps(state){
		let { result , data , selectedId } = state
		return {
			list:result.map(item=>{
				//判断已选列表是否包含对应item的orderId
				console.log("selectedId",item,selectedId,selectedId.indexOf(item)>-1)
				if(selectedId.indexOf(item)>-1){//存在
					return {...data[item],isChecked:true }
				}else {
					return {...data[item],isChecked:false }
				}
			}),
			selectedChange:(orderId)=>this.selectedChange(orderId)
		}
	}

	//生成底部导航组件
	createFootBarProps(state){
		let { selectedNum,selectedInvoiceAmount,isSelectedAll } = state
		return {
			selectedNum,
			selectedInvoiceAmount,
			isSelectedAll,
			selectedAllChange:()=>this.selectedAllChange(),
			btnClick:()=>this.goNextStep()
		}
	}

	createLoadingMoreProps(state){
		let { result } = state,
			showMsg = result.length>6
		return {
			showMsg, //显示文字提醒
			msgType:2, // 对应msg字段的下标
			msg: ['点击加载更多', '加载中...', 'END'], //文字提醒
			height: 5, //触发加载数据的高度 单位px
			loadingMore: () => console.log("111"), //到达底部时，触发的事件
			line:true, //END时,是否添加贯穿文本的横线
		}
	}

	render(){
		console.log(this.state)

		//生成顶部组件props
		let TopHeaderProps = this.createTopHeaderProps(this.state)

		//生成订单列表组件props
		let ListProps = this.createListProps(this.state)

		//生成底部导航组件
		let FootBarProps = this.createFootBarProps(this.state)

		//加载跟多组件Props
		let loadingMoreProps = this.createLoadingMoreProps(this.state)

		return (
			<div className={styles.wrap}>
				{
					!this.state.isFetching && <div>
						{
							this.state.totalInvoiceAmount !=0 ? <div>
								<TopHeader {...TopHeaderProps}/>
								<div className="fp_whiteSpace_24"></div>
								<List {...ListProps}/>
								<LoadingMore {...loadingMoreProps}/>
								<FootBar {...FootBarProps}/>
							</div>:<div className={styles.noOrder}></div>
						}
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	orderList:state.orderList
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(invoiceActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(OrderList)