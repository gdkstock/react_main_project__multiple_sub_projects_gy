/*
    底部固定栏
*/

import styles from './footBar.scss'

const defaultProps = {
    selectedNum:2323, //选中订单数量
    selectedInvoiceAmount:"3000", //当前选中的可开票金额总计
    isSelectedAll:false,//全选按钮
    selectedAllChange:()=>{},//全选按钮改变
    btnClick:()=>{},//下一步按钮点击事件
}
const FootBar = (props)=>{
    // props = defaultProps;
    console.log(props)
    let leftIconClass = props.isSelectedAll ? styles.selected : styles.defalut;
    return (
        <div className={styles.wrap}>
            <div className={leftIconClass+" "+styles.leftIcon+" absolute-vertical-center"} onClick={props.selectedAllChange}></div>
            <div className={styles.selectedAllText}>全选</div>
            <div className={styles.content+" absolute-vertical-center"}>
                已选{props.selectedNum}个订单<br/>共计{(props.selectedInvoiceAmount*1).toFixed(2)}元
            </div>
            {
                props.selectedNum > 0 ? 
                <div className={ styles.nextBtn+" "+styles.btnActive } onClick={props.btnClick}>下一步</div>
                :<div className={ styles.nextBtn+" "+styles.btnDefault }>下一步</div>
            }
        </div>
    )
}

export default FootBar