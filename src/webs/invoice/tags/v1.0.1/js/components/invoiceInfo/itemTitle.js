/*
   项目标题
*/
import styles from './itemTitle.scss'

const defaultProps = {
    text:"",//文本
    hasLine:false,//存在下边框
    hasPromptIcon:false,//有提示按钮
    promptImg:"",//点击提示框弹出内容
}

const showMask = (imgSrc) =>{
    let mask = document.createElement("div")
    mask.id="promptMsg";
    mask.innerHTML = `<div  class="mask" onClick="document.body.removeChild(document.getElementById('promptMsg'))">
					<img src="${imgSrc}"/>
				</div>`
    document.body.appendChild(mask);
}

const ItemTitle = (props)=>{
    // props = defaultProps;
    let txtClass = props.hasLine ? (styles.hasLine+" text") : "text";
    return (
        <div>
            <div className="item-title">
                <div className={txtClass}>
                    { props.text }
                    { props.hasPromptIcon && <i className="icon-prompt" onClick={()=>showMask(props.promptImg)}></i>}
                </div>
            </div>
        </div>
    )
}

export default ItemTitle