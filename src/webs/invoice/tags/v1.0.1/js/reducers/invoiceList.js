/*
	全局状态record
*/
import {
	ADD_RECORD,
    ADD_RECORD_LIST,
	IS_LOADING_RECORD,
	FINISH_LOADING_RECORD,
} from '../actions/actionsTypes'

const initState = {
	data:{
	},
	result:[],
	hasMore:true,
	isLoadingRecordData:false,
}

// const initState = {
// 	data:{
// 		"123123":{
// 			invoiceId:"123123",//发票id
// 			invoiceStatus:1,//开票状态 1 处理中 2 已开票
// 			createTime:"2017-06-21 17:00",//开票时间
// 			invoiceAmount:300,//发票金额
// 		},
// 		"123124":{
// 			invoiceId:"123124",//发票id
// 			invoiceStatus:2,//开票状态 1 处理中 2 已开票
// 			createTime:"2017-06-21 17:00",//开票时间
// 			invoiceAmount:300,//发票金额
// 		}
// 	},
// 	result:["123123","123124"]
// }

export default function invoiceList( state = initState, action ){
	let obj = {};
	switch(action.type){
		case ADD_RECORD://暂未用到
			obj.data = {};
			obj.result = [...state.result];
			obj.hasMore = state.hasMore;
			obj.isLoadingRecordData = state.isLoadingRecordData;
			let invoiceId = action.data.invoiceId

			if( !invoiceId ){ return state}
			if( state.data[invoiceId] ){
				obj.data[invoiceId] = { ...state.data[invoiceId], ...action.data }
			}else{
				obj.data[invoiceId] = {...action.data};
				obj.result.unshift(invoiceId);
			}
			obj.data = { ...state.data, ...obj.data }
			return obj;
		case ADD_RECORD_LIST:
			obj.data = {};
			obj.result = [];
			obj.hasMore = state.hasMore;
			obj.isLoadingRecordData = state.isLoadingRecordData;
			
			let pageNum = action.data.pageNum;
			let recordList = action.data.invoiceList || [];

			//如果是第一次请求
			if(pageNum == 1){
				recordList.map(item=>{
					obj.data[item.invoiceId] = item;
					obj.result.push(item.invoiceId); 
				})
			}else{
				obj.result = [...state.result]
				recordList.map(item=>{
					let invoiceId = item.invoiceId
					if( !invoiceId ){ return }
					if( state.data[invoiceId] ){
						obj.data[invoiceId] = { ...state.data[invoiceId], ...item }
					}else{
						obj.data[invoiceId] = {...item}
						obj.result.push(invoiceId);
					}
				})
				obj.data = { ...state.data, ...obj.data }
			}	
			if(recordList.length<20){
				obj.hasMore = false
			}else{
				obj.hasMore = true
			}	
			return obj;
		case IS_LOADING_RECORD:
			return { ...state,isLoadingRecordData:true }
		case FINISH_LOADING_RECORD:
			return { ...state,isLoadingRecordData:false }
		default:
			return state;
	}
}