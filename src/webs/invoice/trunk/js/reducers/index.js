import { combineReducers } from 'redux'
import orderList from './orderList'
import invoiceList from './invoiceList'

const rootReducer = combineReducers({
    orderList,
    invoiceList
});

export default rootReducer;
