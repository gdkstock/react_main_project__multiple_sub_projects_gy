/*
	全局状态orderList
*/
import {
    ADD_ORDER_LIST,
} from '../actions/actionsTypes'

const initState = {
	data:{
	},
	result:[],
	totalInvoiceAmount:"",//总计开票金额
	invoiceExplain:"",//说明
}

//测试
// const initState = {
// 	data: {
// 		"1234":{
// 			orderId:"1234",//订单id
// 			carNumber:"粤123456",//车牌号码
// 			violationNumber:"2",//违章数量
// 			payTime:"2017-06-21 17:00",//下单时间
// 			orderInvoiceAmount:"30.00",//可开票金额
// 			isChecked:false,//是否选中
// 		},
// 		"1236":{
// 			orderId:"1236",//订单id
// 			carNumber:"粤123456",//车牌号码
// 			violationNumber:"2",//违章数量
// 			payTime:"2017-06-21 17:00",//下单时间
// 			orderInvoiceAmount:"30.00",//可开票金额
// 			isChecked:false,//是否选中
// 		},
// 	},
// 	result: [ '1234', '1236' ],
// 	totalInvoiceAmount:"2323",//总计开票金额
// 	invoiceExplain:"1. 你可以开具最大额度的发票，也可以分次开票；<br>2. 发票申请处理后，将于3个工作日之内完成；<br>3. 目前仅支持开电子普通发票；<br>4. 目前开票内容仅支持“代理服务费”；<br>5. 目前仅支持两年内的相关费用开发票；<br>6. 目前仅支持订单服务费、加急费开具发票。车务卡、加油卡等业务暂时不支持开具发票。代收费的罚金、滞纳金等费用，不支持开具发票。",//说明 
// }

export default function orderList( state = initState, action ){
	let obj = {};
	switch(action.type){
		case ADD_ORDER_LIST:
			obj.totalInvoiceAmount = action.data.totalInvoiceAmount;
			obj.invoiceExplain = action.data.invoiceExplain;
			obj.data = {};
			obj.result = [];
			if(action.data.orderList && action.data.orderList.length>0){
				let list = action.data.orderList;
				list.map(item=>{
					obj.data[item.orderId] = Object.assign({},item,{isChecked:false});
					obj.result.push(item.orderId);
				}) 
			}
			return obj;
		default:
			return state;
	}
}


