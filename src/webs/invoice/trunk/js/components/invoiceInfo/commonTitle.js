/*
    公用item组件
*/
import styles from './commonTitle.scss'

const defaultProps = {
    invoiceStatus: 2, //开票状态 1 处理中，2 已开票
    createTime:"2017-06-23 17:00" ,//开票时间
    invoiceImg:"./images/img-invoice.png",//发票图片
}

const showMask = (id) =>{
	let mask = document.getElementById(id);
	mask.style.display="block";
}

const hideMask = (e,id) =>{
    if(e.target.nodeName!="IMG"){
        let mask = document.getElementById(id);
        mask.style.display="none";
    }
}

const CommonTitle = (props)=>{
    // props = defaultProps;
    let textClass = props.invoiceStatus == 1 ? (styles.title+" "+styles.titleLight):(styles.title+" "+styles.titleDefault);
    let textContent = props.invoiceStatus == 1 ? "发票正在处理中":"电子发票已开";
    return (
        <div className={styles.wrap}>
            <div className='absolute-vertical-center'>
                <div className={textClass}>{textContent}</div>
                <div className={styles.time}>{props.createTime}</div>
            </div>
            { props.invoiceStatus == 2 && <div className={styles.rightText} onClick={()=>showMask("invoiceImg")}>查看</div> }
            {
				props.invoiceStatus == 2 &&
				<div id="invoiceImg" className={styles.mask} onClick={(e)=>hideMask(e,"invoiceImg")}>
					<img src={props.invoiceImg}/>
                    <div className={styles.text}>长按图片保存到相册</div>
				</div>
			}
        </div>
    )
}

export default CommonTitle