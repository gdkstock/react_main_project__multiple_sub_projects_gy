/*
    组件模板
*/
import styles from './topHeader.scss'

const defaultProps = {
    totalInvoiceAmount:2323, //可开票总金额
    invoiceExplain:`1. 你可以开具最大额度的发票，也可以分次开票；<br>
                2. 发票申请处理后，将于3个工作日之内完成；<br>
                3. 目前仅支持开电子普通发票；<br>
                4. 目前开票内容仅支持“代理服务费”；<br>
                5. 目前仅支持两年内的相关费用开发票；<br>
                6. 目前仅支持订单服务费、加急费开具发票。车务卡、加油卡等业务暂时不支持开具发票。代收费的罚金、滞纳金等费用，不支持开具发票。
                `
}

//蒙层
const showModal = str =>{
    var div = document.createElement("div");
    div.id = "modal";
    div.className = "modal_65 modalWrap";
    div.innerHTML = `<div class='modal-title'>开票说明</div><div class='modal-content'>${str}</div><i class="closeIcon" onClick="document.body.removeChild(document.getElementById('modal'))"></i>`;
    document.body.appendChild(div);
}

const TopHeader = (props)=>{
    // props = defaultProps;
    console.log(props)
    return (
        <div className={styles.wrap}>
            <div>可开票金额：</div>
            <div className={styles.totalFine}>￥{(props.totalInvoiceAmount*1).toFixed(2)}</div>
            <div className={styles.rightBlock} onClick={()=>showModal(props.invoiceExplain)}>
                <div>开票说明</div>
                <div className={styles.illustrateIcon}></div>
            </div>
        </div>
    )
}

export default TopHeader