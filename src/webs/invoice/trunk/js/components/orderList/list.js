/*
    可开票订单列表
*/
import styles from './list.scss'

const defaultProps = {
    list:[{
        orderId:"123123",//订单id
        carNumber:"粤123456",//车牌号码
        violationNumber:"2",//违章数量
        payTime:"2017-06-21 17:00",//下单时间
        orderInvoiceAmount:"30.00",//可开票金额
        isChecked:true,//是否选中
        orderTitle:"",//订单标题
    }],
    selectedChange:()=>{}
}

const Item = props =>{
    let leftIconClass = props.isChecked ? styles.selected : styles.defalut;
    console.log(leftIconClass)
    return(
        <div className={styles.item}>
            <div className={leftIconClass+" "+styles.leftIcon+" absolute-vertical-center"} onClick={()=>props.selectedChange(props)}></div>
            <div className={styles.content+" absolute-vertical-center"}>
                {props.orderTitle?
                    <div className={styles.title}>{props.orderTitle}</div>
                    : <div className={styles.title}>{props.carNumber}的{props.violationNumber}条违章办理</div>
                }

                <div className={styles.time}>下单时间：{props.payTime}</div>
            </div>
            <div className={styles.rightFine}>￥{(props.orderInvoiceAmount*1).toFixed(2)}</div>
        </div>
    )
}

const List = props =>{
    // props = defaultProps;
    console.log(props)
    return (
        <div className={styles.list}>
            {
               props.list.length>0 && props.list.map(item=><Item {...item} key={item.orderId} selectedChange={props.selectedChange}/>) 
            }
        </div>
    )
}

export default List