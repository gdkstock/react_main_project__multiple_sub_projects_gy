/*
    开票历史列表组件
*/
import styles from './list.scss'

const defaultProps = {
    list:[{
        invoiceId:"123123",//发票id
        invoiceStatus:1,//开票状态 1 处理中 2 已开票
        createTime:"2017-06-21 17:00",//开票时间
        invoiceAmount:300,//发票金额
    },
    {
        invoiceId:"123124",//发票id
        invoiceStatus:2,//开票状态 1 处理中 2 已开票
        createTime:"2017-06-21 17:00",//开票时间
        invoiceAmount:300,//发票金额
    }],
    handleClick:()=>{}
}

const Item = props =>{
    let rightTextClass = props.invoiceStatus == 1 ? (styles.rightText+" "+styles.textLight) : (styles.rightText+" "+styles.textDefault);
    let rightText = props.invoiceStatus == 1 ? "处理中" : "已开票";
    return(
        <div className={styles.item} onClick={()=>props.handleClick(props)}>
            <div className="absolute-vertical-center">
                <div className={styles.time}>{props.createTime}</div>
                <div className={styles.title}>￥{(props.invoiceAmount*1).toFixed(2)}&nbsp;电子发票&nbsp;|&nbsp;代理服务费</div>
            </div>
            <div className={rightTextClass}>{rightText}</div>
        </div>
    )
}
const RecordList = (props)=>{
    // props = defaultProps;
    return (
        <div className={styles.list}>
           { props.list && props.list.length>0 && props.list.map(item=><Item {...item} handleClick={(item)=>props.handleClick(item)}/>) }
        </div>
    )
}

export default RecordList