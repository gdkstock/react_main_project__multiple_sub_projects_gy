/**
 * 发票相关接口
 */

import apiHelper from './apiHelper';

class InvoiceService {

    /**
     * 查询可开票订单列表
     * @param {*object} data
     */
    orderList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}invoice/orders`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 提交开票数据
     * @param {*object} data
     */
    add(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}invoice/save`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 请求发票历史记录
     * @param {*object} data
     */
    record(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}invoice/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 请求发票详情
     * @param {*object} data
     */
    detail(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}invoice/detail`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


}

export default new InvoiceService()