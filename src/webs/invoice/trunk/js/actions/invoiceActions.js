/*
* 发票相关
*/
import {
  QUERY_ORDER_LIST,
  ADD_ORDER_LIST,
  QUERY_ADD_INVOICE,
  QUERY_INVOICE_RECORD,
  QUERY_INVOICE_DETAIL
} from './actionsTypes'

//请求可开票订单列表
export const queryOrderList = ( param ) => ({type:QUERY_ORDER_LIST,param})

//请求提交发票数据
export const queryAddInvoice = ( param ) => ({type:QUERY_ADD_INVOICE,param})

//请求发票历史纪录
export const queryInvoiceRecord = ( param ) => ({type:QUERY_INVOICE_RECORD,param})

//请求发票详情
export const queryInvoiceDetail = ( param ) => ({type:QUERY_INVOICE_DETAIL,param})


