/*
	车辆信息相关请求
*/
import {
  put,
  call,
  take,
  fork,
} from 'redux-saga/effects'

import moment from 'moment'
import apiHelper from '../services/apiHelper';
import fetch from 'isomorphic-fetch'
import { Toast } from 'antd-mobile'
import { ChMessage } from '../utils/message.config'
import invoiceService from '../services/invoiceService'
import common from '../utils/common'

import {
  QUERY_ORDER_LIST,
  ADD_ORDER_LIST,
  QUERY_ADD_INVOICE,
  QUERY_INVOICE_RECORD,
  QUERY_INVOICE_DETAIL,
  ADD_RECORD_LIST,
  ADD_RECORD,
  IS_LOADING_RECORD,
  FINISH_LOADING_RECORD,
} from '../actions/actionsTypes'


/*===========================查询可开票列表！===================================*/ 

  function* fetchOrderList(data) {
    Toast.loading("",0)
    try{
      const result = yield call(invoiceService.orderList, data)
      Toast.hide()
      if (result.code == "1000") {   
          yield put({ type: ADD_ORDER_LIST, data: result.data })
      } else {
        if(result.msg && result.code != "4222"){
          Toast.info(result.msg, 1);
        }
      }
    }catch(error){
      Toast.hide()
      Toast.info(ChMessage.FETCH_FAILED, 1);
    }
  }

  function* watchOrderList() {
  	while(true){
  		let { param } = yield take(QUERY_ORDER_LIST)
   		yield fork(fetchOrderList, param)
  	}
  }

/*===========================提交开票数据===================================*/ 
  function* fetchAddInvoice(data) {
    Toast.loading("",0)
    try{
      const result = yield call(invoiceService.add, data)
      Toast.hide()
      if (result.code == "1000") {   
          // yield put({ type: ADD_RECORD, data: result.data })
          //清除可开票订单列表的缓存state，保证回到订单列表页面时刷新数据
          sessionStorage.removeItem("orderListState");
          sessionStorage.removeItem("addInvoiceState");
          //跳转到发票详情
          window.location.replace(common.getRootUrl()+"invoiceRecord")
      } else {
        if(result.msg && result.code != "4222"){
          Toast.info(result.msg, 1);
        }
      }
    }catch(error){
      Toast.hide()
      Toast.info(ChMessage.FETCH_FAILED, 1);
    }
  }

  function* watchAddInvoice() {
  	while(true){
  		let { param } = yield take(QUERY_ADD_INVOICE)
   		yield fork(fetchAddInvoice, param)
  	}
  }

/*===========================请求发票历史纪录===================================*/ 
  function* fetchInvoiceRecord(data) {
    if(data.pageNum==1){
      Toast.loading("",0)
    }
    yield put({ type: IS_LOADING_RECORD })
    try{
      const result = yield call(invoiceService.record, data)
      Toast.hide()
      if (result.code == "1000") {   
          yield put({ type: ADD_RECORD_LIST, data: {...result.data,pageNum:data.pageNum} })
      } else {
        if(result.msg && result.code != "4222"){
          Toast.info(result.msg, 1);
        }
      }
      yield put({ type: FINISH_LOADING_RECORD })
    }catch(error){
      yield put({ type: FINISH_LOADING_RECORD })
      Toast.hide()
      Toast.info(ChMessage.FETCH_FAILED, 1);
    }
  }

  function* watchInvoiceRecord() {
  	while(true){
  		let { param } = yield take(QUERY_INVOICE_RECORD)
   		yield fork(fetchInvoiceRecord, param)
  	}
  }

/*===========================请求发票详情===================================*/ 
  function* fetchInvoiceDetail(data) {
    Toast.loading("",0)
    try{
      const result = yield call(invoiceService.detail, data)
      Toast.hide()
      if (result.code == "1000") {   
          yield put({ type: ADD_RECORD, data: result.data })
          //todo:跳转到发票详情
          let invoiceId = data.invoiceId
          window.location.href = common.getRootUrl()+"invoiceInfo/detail/"+invoiceId
      } else {
        if(result.msg && result.code != "4222"){
          Toast.info(result.msg, 1);
        }
      }
    }catch(error){
      Toast.hide()
      Toast.info(ChMessage.FETCH_FAILED, 1);
    }
  }

  function* watchInvoiceDetail() {
  	while(true){
  		let { param } = yield take(QUERY_INVOICE_DETAIL)
   		yield fork(fetchInvoiceDetail, param)
  	}
  }




/*********监听所有发票相关请求！*********/ 
export function* watchInvoice(){
  yield [
    fork(watchOrderList),
    fork(watchAddInvoice),
    fork(watchInvoiceRecord),
    fork(watchInvoiceDetail),
  ]
}