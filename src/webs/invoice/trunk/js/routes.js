import React from 'react'
import { Route, IndexRoute } from 'react-router'

//antd UI
import { Toast } from 'antd-mobile'

import {
  App,
  NotFoundPage,
} from './containers'

//显示加载中
const showLoading = () => {
  Toast.loading('', 30, () => {
    window.networkError('./images/networkError-icon.png')
  })
}

import OrderList from './containers/orderList'
import AddInvoice from './containers/invoiceInfo/add'
import InvoiceDetail from './containers/invoiceInfo/detail'
import InvoiceRecord from './containers/invoiceRecord'

export default (
  <Route path="/" component={App}>
    {/*<IndexRoute component={CarList} />*/}
    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}
{/*
    <IndexRoute getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/orderList').default)
      }, 'index')
    }} />
*/}

    <IndexRoute component={OrderList}/>

    <Route path="invoiceInfo/add/:fine" component={AddInvoice}/>

    <Route path="invoiceInfo/detail/:invoiceId" component={InvoiceDetail}/>

    <Route path="invoiceRecord" component={InvoiceRecord}/>
    
    <Route path="*" component={NotFoundPage}/>

  </Route>
);