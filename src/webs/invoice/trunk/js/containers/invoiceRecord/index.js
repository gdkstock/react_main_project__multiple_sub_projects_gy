/*
* 发票历史页
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as invoiceActions from '../../actions/invoiceActions'
import { Toast } from 'antd-mobile'
import RecordList from '../../components/invoiceRecord/list'
import apiHelper from '../../services/apiHelper';
import common from '../../utils/common'
import styles from './index.scss'
import jsApi from '../../utils/cx580.jsApi'
import LoadingMore from 'app/components/common/loadingMore'

class InvoiceRecord extends React.Component{
	constructor(props){
		super(props);

		if(sessionStorage.getItem("recordListState")){
			this.state = JSON.parse(sessionStorage.getItem("recordListState"))
			sessionStorage.removeItem("recordListState")
		}else{
			let { data , result, hasMore , isLoadingRecordData } = this.props.invoiceList
			console.log(this.props.invoiceList)
			//初始化状态
			this.state={
				data,
				result,
				pageNum:1,
				pageSize:20,
				hasMore,
				isLoadingRecordData,
			}	
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('开票历史');

		let isChangeUser = localStorage.getItem("isChangeUser");
		if(isChangeUser){
	       	window.history.back()//切换账号后退上一级页面
	    }

		window.cxyPageResumeCallBack = () => {
			let isChangeUser = localStorage.getItem("isChangeUser");
		    if(isChangeUser){
		       	window.history.back()//切换账号后退上一级页面
		    }else{ 
				this.props.actions.queryInvoiceRecord({
					pageNum:1,
					pageSize:20,
				});
			}
		}

		//请求历史纪录
		let prevpageInfo =  sessionStorage.getItem("prevPageInfo")
		
		if(!(prevpageInfo && prevpageInfo == "发票详情")){
			this.props.actions.queryInvoiceRecord({
				pageNum:this.state.pageNum,
				pageSize:this.state.pageSize,
			})
		}
		
		//设置菜单栏右侧按钮
		try{
			jsApi.call({
				"commandId":"",
				"command":"customizeTopRightButton",
				"data":{
					"reset":"false",
					"list":[]
				}
			},
			function(data){
				
			})
		}catch(e){

		}
	}

	componentDidMount(){
		//进入发票历史页面埋点
		// common.sendCxytj({
		// 	eventId:"Violation_EnterAddCar"
		// })
		document.body.style.backgroundColor = "#fff";
	}

	componentWillReceiveProps(nextProps){
		//组件跟新时，初始化状态
		let { data , result ,hasMore , isLoadingRecordData} = nextProps.invoiceList
	
		let pageNum = Math.ceil(result.length/20)+1;

		this.setState({ ...this.state , data , result ,pageNum,hasMore , isLoadingRecordData})
	}

	componentWillUnmount(){
		//发票历史页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
        document.body.style.backgroundColor = "#f1f1f1";
	}

	//生成开票历史列表
	createInvoiceRecordProps(state){
		let { data , result } = state;
		let list = result.map(item=>{
			return data[item]
		})
		return {
			list,
			handleClick:(item)=>this.handleChick(item)
		}
	}

	//历史列表item单击事件
	handleChick(item){
		let invoiceId = item.invoiceId;
		let invoiceList = this.props.invoiceList;
		if(invoiceList.data[invoiceId].invoiceName){
			this.props.router.push("invoiceInfo/detail/"+invoiceId)
		}else{
			this.props.actions.queryInvoiceDetail({invoiceId})
		}
		sessionStorage.setItem("recordListState",JSON.stringify(this.state))
		sessionStorage.setItem("recordListScrollTop",document.body.scrollTop)
	}

	//生成加载更多组件
	createLoadingMoreProps(state){
		let { result , hasMore , isLoadingRecordData } = state,
			showMsg = true,
			msgType = 0;
		msgType = hasMore ? (isLoadingRecordData ? 1 : 0) : 2;
		showMsg = result.length>8 ? true : false;
		return {
			showMsg, //显示文字提醒
			msgType, // 对应msg字段的下标
			msg: ['点击加载更多', '加载中...', 'END'], //文字提醒
			height: 50, //触发加载数据的高度 单位px
			loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
			line:true, //END时,是否添加贯穿文本的横线
		}
	}

	//上拉加载更多
	loadingMore(){
		this.props.actions.queryInvoiceRecord({
			pageNum:this.state.pageNum,
			pageSize:this.state.pageSize,
		})
	}

	render(){
		let RecordListProps  = this.createInvoiceRecordProps(this.state)
		let LoadingMoreProps = this.createLoadingMoreProps(this.state)
		console.log("state",this.state)
		return (
			<div>
				{
					this.state.result.length>0 ? <div>
						<RecordList { ...RecordListProps }/> 
						{ this.state.result.length > 8 && <LoadingMore {...LoadingMoreProps}/>}
					</div>
					: <div className={ styles.noRecord }></div>
				}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	invoiceList:state.invoiceList
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(invoiceActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(InvoiceRecord)