/**
 * 首页相关接口
 */

import apiHelper from './apiHelper';

class IndexService {

    /**
     * 获取重定向的URL
     * @param {*object} data
     */
    getUrl(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}gangao/getTargetUrl/${data.userId}`,
          data: {
            method: 'get'
          }
        };
        return apiHelper.fetch(requestParam);
    }
}

export default new IndexService()