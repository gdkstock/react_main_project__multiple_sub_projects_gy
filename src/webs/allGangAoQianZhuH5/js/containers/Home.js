import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import { Toast } from 'antd-mobile';

//services
import indexService from '../services/indexService';

//常用工具类
import common from '../utils/common';
import userHelper from '../utils/userHelper';
import { ChMessage } from '../utils/message.config';


class Home extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		common.setViewTitle(' ');
	}

	componentDidMount() {
		this.init();
	}

	//组件初始化
	init() {
		//APP环境
		if (common.isCXYApp()) {
			if (window.cxyJSBridgeIsReady) {
				//JSDK 以及初始化完毕
				this.appInit();
			} else {
				//JSDK还未初始化完毕
				window.cxyJSBridgeReady = this.appInit();;
			}
		}

		if (sessionStorage.getItem('userId')) { //判断userId是否存在
			this.toUrl(sessionStorage.getItem('userId')); //获取数据
		}
	}

	//APP环境初始化完毕
	appInit() {
		userHelper.getAccountId(userId => {
			if (userId) {
				//存在用户信息
				this.toUrl(userId);
			} else {
				//用户ID不存在 则跳转到登录页面
				userHelper.Login(userId => {
					//登录成功
					this.toUrl(userId);
				});
			}
		});
	}

	//跳转到指定的URL
	toUrl(userId = '') {
		Toast.loading('', 0);
		indexService.getUrl({ userId }).then(res => {
			Toast.hide();
			if (res.code == 1000) {
				window.location.replace(res.data.targetUrl);
			} else {
				Toast.info(res.msg || ChMessage.FETCH_FAILED)
			}
		}, error => {
			Toast.hide();
			Toast.info(ChMessage.FETCH_FAILED);
		})
	}


	render() {

		return (
			<div className='box whiteBg'>

			</div>
		);
	}
}

const mapStateToProps = state => ({

})

export default connect(
	mapStateToProps
)(Home);