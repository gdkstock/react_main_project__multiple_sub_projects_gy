/**
 * 个人信息
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//组件
import { Modal, Toast, Icon } from 'antd-mobile';

//styles
import styles from './index.scss';

//services
import imageService from '../../services/imageService';

//actions
import * as myActions from '../../actions/myActions';

//常用工具类
require("lrz");
import { ChMessage } from '../../utils/message.config';
import userHelper from '../../utils/userHelper';
import common from '../../utils/common';

//配置
import config from '../../config';

class MyInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }

    }

    componentWillMount() {
        let { user } = this.props;

        //用户信息不存在则去请求数据
        if (user.ifBind === undefined) {
            this.props.myActions.queryUserDetailAsync({});
        }
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * 显示昵称输入框
     */
    showNickInput() {
        let { user } = this.props;
        Modal.prompt('昵称', '', [
            { text: '取消' },
            {
                text: '提交', onPress: value => {
                    if (value) {
                        this.props.myActions.updateUserDetailAsync({
                            nickName: value.substr(0, 16)
                        })
                    }
                }
            },
        ], 'default', user.nickName);
    }

    /**
     * 显示性别选择
     */
    showSexSelete() {
        Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请选择性别</span>, [
            {
                text: '男', onPress: () => {
                    this.props.myActions.updateUserDetailAsync({
                        sex: '男'
                    })
                }, style: { color: '#2582ea' }
            },
            {
                text: '女', onPress: () => {
                    this.props.myActions.updateUserDetailAsync({
                        sex: '女'
                    })
                }, style: { color: '#2582ea' }
            }
        ])
    }

    /**
	 * 图片上传
	 */
    uploadImg(target, name) {
        this._lrz(target.files[0], name);
    }

    /**
	 * 图片压缩上传
	 * @param e 图片资源
	 */
    _lrz(files, name) {
        this.setState({
            [name + 'Loading']: true //显示加载中
        })
        try {
            let quality = 1;
            if (files.size > 1024 * 1024 * 5) {
                quality = .5;
            }
            else if (files.size > 1024 * 1024 * 2) {
                quality = .5;
            }
            else if (files.size > 1024 * 1024) {
                quality = .5;
            }
            else if (files.size > 1024 * 500) {
                quality = .4;
            }
            else if (files.size > 1024 * 100) {
                quality = .5;
            } else {
                quality = .7;
            }

            lrz(files, {
                width: 300,
                quality: quality
            }).then((rst) => {
                // 处理成功会执行
                this.postImgToUploadImg(rst.base64, name);

            }).catch((err) => {
                // 处理失败会执行
                Toast.info(ChMessage.UPLOAD_FAILED, 2);
            }).always(() => {
                // 不管是成功失败，都会执行
                this.refs[name].value = ''; //清空文件上传 避免上传同一张照片或是拍照时出现的图片无法展示的bug
            });
        } catch (e) {
            Toast.info(ChMessage.UPLOAD_FAILED, 2)
            this.setState({
                [name + 'Loading']: false //隐藏加载中
            })
        }

    }

    /**
	 * 上传图片到服务器
	 * @param base64 base64图片
	 */
    postImgToUploadImg(base64, name) {
        //上传图片到服务器
        let uploadimgData = {
            content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
            format: 'jpg',
            channel: 'app',
            thumbnail: '100x' //缩略图尺寸
            // watermark: true //加水印
        }
        imageService.uploadImg(uploadimgData).then(data => {
            if (data.code === 0) {
                //修改头像
                if (data.data && data.data.url) {
                    this.props.myActions.updateUserDetailAsync({
                        headImgUrl: data.data.url
                    })
                }
            } else {
                if (data.msg) {
                    Toast.info(data.msg, 2);
                }
            }
        }, () => {
            Toast.info(ChMessage.FETCH_FAILED);
        }).then(() => {
            this.setState({
                [name + 'Loading']: false //隐藏加载中
            })
        })
    }

    /**
     * 解除绑定
     */
    userUnBind() {
        //解除绑定
        this.props.myActions.userUnBindAsync({}, res => {
            if (res.code == 1000) {
                Toast.info('解除绑定成功');

                setTimeout(() => {
                    //更新userId和token
                    sessionStorage.setItem('userId', res.data.userId);
                    sessionStorage.setItem('token', res.data.token);

                    //页面后退
                    window.history.back();
                }, 1000);
                //解除成功后重新去单点登录
                // userHelper.Login(config.authCallbackUrl);
            } else {
                Toast.info(res.msg || ChMessage.FETCH_FAILED);

                //正常逻辑下，不应该后出现解绑失败。
                //导致解绑失败最大的可能就是 账号问题
                //延迟1秒跳转到单点登录 重新获取最新的账号信息
                setTimeout(() => {
                    userHelper.Login(config.authCallbackUrl);
                }, 1000);
            }
        });

        common.sendCxytj({
            eventId: 'h5_e_wz_info_logout'
        })

    }

    /**
     * 设置默认用户头像
     */
    setHeadImgUrl(e) {
        //避免出错死循环
        if (e.src !== './images/userAvatar.png') {
            e.src = './images/userAvatar.png';
        }
    }

    render() {
        let { user } = this.props;
        let { headImgUrl, nickName, telephone, sex } = user;
        let name = 'uploadImg';

        //过滤null字符串
        if (sex == 'null') {
            sex = '';
        }

        return (
            <div className='box'>
                <div className='whiteBg'></div>

                <ul className={styles.infoList}>
                    <li>
                        <label>头像</label>
                        <img src={headImgUrl ? decodeURIComponent(headImgUrl) : './images/userAvatar.png'} onError={e => this.setHeadImgUrl(e.target)} />
                        <div className={this.state[name + 'Loading'] ? styles.loadingBox : 'hide'}><Icon type='loading' /></div>
                        <input
                            ref={name}
                            className={styles.uploadImg}
                            type="file"
                            accept="image/*"
                            name={name}
                            onChange={(e) => this.uploadImg(e.target, name)}
                            style={{ opacity: '0' }}
                        />
                    </li>
                    <li onClick={() => this.showNickInput()}>
                        <label>昵称</label>
                        <span>{nickName || '未填写'}</span>
                    </li>
                    <li className={styles.noIcon}>
                        <label>手机</label>
                        <span>{telephone}</span>
                    </li>
                    <li onClick={() => this.showSexSelete()}>
                        <label>性别</label>
                        <span>{sex || <i>&nbsp;</i>}</span>
                    </li>
                </ul>

                <div className={styles.btnBox}>
                    <div className={styles.btn} onClick={() => this.userUnBind()}>解除绑定</div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
})
const mapDispatchToProps = dispatch => ({
    myActions: bindActionCreators(myActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyInfo);