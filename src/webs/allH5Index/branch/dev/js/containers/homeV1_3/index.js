import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//组件
import LoadingMore from 'app/components/common/LoadingMore';
import { DefaultCard, HotNewsCard, CommunityCard, AdCard } from '../../components/msgCard';
import { Toast, Icon } from 'antd-mobile';
import IconList from '../../components/iconList';
import CarList from '../../components/carList';
import AddCar from '../../components/addCar';

//styles
import styles from './index.scss';

//action
import * as carsActions from '../../actions/carsActions'
import * as indexPageActions from '../../actions/indexPageActions'

//常用工具类
import common from '../../utils/common'
import config from '../../config'

class HomeV1_3 extends Component {
    constructor(props) {
        super(props);

        let { cars } = this.props;
        let list = sessionStorage.getItem('H5Index_home_state_list'); //从缓存中获取信息流列表
        this.state = {
            //车辆列表
            carList: {
                data: cars.result.map(carId => cars.data[carId])
            },

            //显示添加车辆蒙层
            showAddCar: false,

            //车牌前缀列表 不传则使用默认的列表
            carPrefixList: undefined,

            //上滑加载更多
            loadingMoreProps: {
                showMsg: true, //显示文字提醒
                msgType: 0, // 对应msg字段的下标
                msg: [
                    '加载更多...',
                    <span className={styles.iconLoading}><Icon type='loading' />加载更多...</span>,
                    '喂，已经到底了',
                    ''
                ], //文字提醒
                height: 100, //触发加载数据的高度 单位px
                loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
                line: true, //END时,是否添加贯穿文本的横线
            },

            //消息流列表
            list: list ? JSON.parse(list) : [],
        }
    }

    componentWillMount() {
        let { list } = this.state;
        let { cars } = this.props;
        if (cars.result.length === 0) {
            //车辆列表没有数据时，显示加载中
            Toast.loading('', 0);
        }
        //获取车辆列表基础信息
        this.props.carsActions.queryCarsAsync({}, res => {
            if (cars.result.length === 0) {
                //车辆列表没有数据时，再回调中关闭加载中
                Toast.hide();
            }
        });

        //获取首页其他数据
        this.props.indexPageActions.getIndexDatas();

        //获取消息列表 首次进入或者列表为空的时候才请求数据
        if (list.length === 0 || !sessionStorage.getItem('H5Index_getMessages')) {
            sessionStorage.setItem('H5Index_getMessages', 1);
            this.getMessages();
        }
    }

    componentDidMount() {
        //滚动页面到指定位置
        document.body.scrollTop = sessionStorage.getItem('H5Index_home_scrollTop') || 0;
    }

    componentWillReceiveProps(nextProps) {
        let { cars } = nextProps
        this.setState({
            carList: {
                data: cars.result.map(carId => cars.data[carId])
            }
        })
    }

    componentWillUnmount() {
        //记录滚动条的位置
        sessionStorage.setItem('H5Index_home_scrollTop', document.body.scrollTop);
    }

    /**
     * 获取信息流列表
     */
    getMessages(params = {}) {
        let { list } = this.state;

        //改变加载文案
        this.setState({
            loadingMoreProps: this.getMsgIndexObject(1)
        });

        //获取消息列表
        this.props.indexPageActions.queryMessagesAsync(params, res => {

            //改版state
            let _state = {};

            //修改文案提示 默认值
            _state.loadingMoreProps = this.getMsgIndexObject(0);
            if (res.code == 1000) {

                if (res.data && res.data.length > 0) {
                    //返回的数据不满10条 则默认已经无新数据
                    if (res.data.length < 10) {
                        _state.loadingMoreProps = this.getMsgIndexObject(2);
                    }

                    //合并数据
                    _state.list = list.concat(res.data);

                    //获取社区类型的帖子点赞数和评论数
                    this.getPostsInfo(res.data.filter(item => item.type == 3));

                    //获取热门资讯的阅读数
                    this.getHotNewsViewCount(res.data.filter(item => item.type == 2));
                } else {
                    _state.loadingMoreProps = this.getMsgIndexObject(2);
                }


            }

            this.setState(_state);
        })
    }

    /**
     * 获取帖子的评论和点赞数
     * @param {array} posts 信息流对象数组
     */
    getPostsInfo(objs = []) {
        let ids_str = this.getMsgObjIds(objs);

        if (ids_str) {
            this.props.indexPageActions.queryPostInfoAsync({ id: ids_str }, res => {
                if (res.code == 1000) {
                    this.msgObjsUpdate(res.data);
                }
            })
        }
    }

    /**
     * 获取热门资讯阅读数
     */
    getHotNewsViewCount(objs = []) {
        let ids_str = this.getMsgObjIds(objs);

        if (ids_str) {
            this.props.indexPageActions.queryHotNewsViewCountAsync({ id: ids_str }, res => {
                if (res.code == 1000) {
                    this.msgObjsUpdate(res.data);
                }
            })
        }
    }

    /**
     * 根据objId更新信息流的信息
     */
    msgObjsUpdate(objs = []) {
        let { list } = this.state;

        let objsIds = objs.map(obj => obj.objId + ''); //接口返回的id数组

        let newList = list.map(item => {
            let index = objsIds.indexOf(item.objId + '');

            //信息流匹配
            if (index !== -1) {
                return Object.assign({}, item, objs[index])
            }
            return item;
        })

        this.setState({
            list: newList
        })
    }

    /**
     * 获取信息流对象Id字符串
     */
    getMsgObjIds(objs) {
        if (objs.length === 0) {
            return;
        }

        let ids = []; //id数组
        objs.map(obj => {
            if (ids.indexOf(obj.objId) === -1) {
                ids.push(obj.objId);
            }
        })
        let ids_str = ids.join(',');
        return ids_str;
    }

    /**
     * 加载更多
     */
    loadingMore() {
        let { list } = this.state;

        if (list.length === 0) {
            //改变加载文案
            this.setState({
                loadingMoreProps: this.getMsgIndexObject(2)
            });
            return;
        }

        //请求数据
        let { updateTime, top } = list[list.length - 1];
        this.getMessages({
            endTime: updateTime,
            top
        })
    }

    //获取msg下标的对象结构体
    getMsgIndexObject(index) {
        let { loadingMoreProps } = this.state;
        return Object.assign({}, loadingMoreProps, {
            msgType: index, // 对应msg字段的下标
        })
    }

    //添加车辆
    addCar() {
        //点击添加车辆埋点
        common.sendCxytj({
            eventId: "H5Index_AddCar"
        })

        //全渠道都使用引导注册的方式

        //获取车牌前缀
        this.props.carsActions.getCarConditionList({}, result => {
            let { carPrefixList } = this.state
            if (result && result.code == '1000' && result.result.length > 0) {
                carPrefixList = result.result
            }

            this.setState({
                showAddCar: true,
                carPrefixList: carPrefixList
            })
        })
    }

    /**
     * 检测输入的合法性
     */
    checkInputs(carNumber) {
        let checkAZ = /[A-Z]/g.exec(carNumber.substr(1, 1)); //检查车牌号码是否以字母开头

        if (carNumber.length < 2) {
            Toast.info("车牌号码不能为空", 1);
        } else if (carNumber.length < 7 || carNumber.length > 8 || !checkAZ) {
            Toast.info("您输入的车牌号码有误", 1);
        } else {
            return true
        }
        return false
    }

    //跳转到添加车辆验证页面
    toAddCarAuthPage(carNumber) {
        if (this.checkInputs(carNumber)) {
            this.props.carsActions.getCarInformation({ carNumber: carNumber }, result => {
                if (result && result.code == '1000') {
                    this.hideAddCar();

                    let { carId, count, degree, money, tel, flag } = result.data
                    switch (flag + '') {
                        case '0': //无车辆信息
                            if (carNumber.substr(0, 1) == '粤') {
                                //粤牌车可以进行身份验证
                                this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
                            } else {
                                this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
                            }
                            break;
                        case '1': //存在车辆信息
                            if (carNumber.substr(0, 1) != '粤' && count == -1 && !tel) {
                                //非粤牌车 && 无违章 && 无手机号码
                                this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
                            } else {
                                this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
                            }

                            break;
                        case '2': //车辆已经存在车辆列表中
                            this.toViolationListUrl(carId);
                            break;
                        default:
                            this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`);
                    }

                } else {
                    if (result.code == '4443') {
                        //满三辆车
                        Toast.info('违章查询最多支持绑定3辆车辆,请前往车辆列表删除后再继续查询', 3, () => this.hideAddCar())
                    } else {
                        Toast.info(result.msg || ChMessage.FETCH_FAILED)
                    }
                }
            })
        }
    }

    //跳转到添加车辆页面
    toAddCarUrl(url) {
        window.location.href = `${config.addCarUrl + '?' + common.getUserInfoString()}#${url}` //跳转到添加车辆的相关路由页面
    }

    //跳转到违章列表页面
    toViolationListUrl(carId) {
        window.location.href = `${config.violationUrl + '?' + common.getUserInfoString()}#violationList/${carId}`;
    }

    //隐藏添加车辆
    hideAddCar() {
        this.setState({
            showAddCar: false
        })

        //滚动页面返回顶部
        document.body.scrollTop = 0;
    }

    //获取信息流卡片dom元素
    getCardDom(data, index) {

        let { id, type, title, typeName, author, forwardUrl } = data;

        //右上角省略号
        data.ellipsisProps = {
            id: id,
            list: [
                {
                    id: '',
                    click: id => this.ignoreCard(id, index),
                    title: '忽略此消息'
                }
            ]
        }

        let Dom = '';
        switch (type + '') {
            case '1': //默认
                data.toUrlEventId = 'h5_e_symsg_msg'; //跳转URL的埋点ID
                Dom = <DefaultCard {...data} />;
                break;
            case '2': //热门资讯
                data.toUrlEventId = 'h5_e_symsg_news'; //跳转URL的埋点ID
                Dom = <HotNewsCard {...data} styleType={id % 3 === 0 ? 0 : 1} />;
                break;
            case '3': //社区
                data.toUrlEventId = 'h5_e_symsg_post'; //跳转URL的埋点ID
                data.clickSupports = () => this.postSupport(index); //点赞
                data.clickComments = () => {
                    common.sendCxytj({
                        eventId: 'h5_e_symsg_post'
                    })
                    this.toUrl(forwardUrl)
                }; //评论
                Dom = <CommunityCard {...data} />;
                break;
            case '4': //广告
                Dom =
                    <div onClick={() => this.toUrl(forwardUrl)}>
                        <AdCard
                            id={data.id}
                            img={data.imgUrl[0]} />
                    </div>;
                break;
            default:
                data.toUrlEventId = 'h5_e_symsg_msg'; //跳转URL的埋点ID
                Dom = <DefaultCard {...data} />;
        }

        return Dom;
    }

    //忽略某条信息流卡片
    ignoreCard(id, index) {
        let { list } = this.state;

        //父元素的dom节点Id
        let domId = `msg-dom-${index}-${id}`;
        let obj = document.getElementById(domId);
        if (obj) {
            //存在
            obj.style.overflow = 'hidden';
            obj.style.height = obj.offsetHeight + 'px';
            setTimeout(() => {
                obj.style.height = 0; //延迟触发动画效果
            }, 10);
        }

        //等待动画 延迟删除数据
        setTimeout(() => this.setState({
            list: list.filter((item, i) => i !== index)
        }), 500);

        common.sendCxytj({
            eventId: 'h5_e_symsg_delete'
        })

        //忽略消息 异步
        this.props.indexPageActions.messagesIgnoreAsync({ id: id });
    }

    /**
     * 帖子点赞
     * @param {int} i 索引
     */
    postSupport(i) {
        let { list } = this.state;
        list[i].is_praise = !list[i].is_praise; //点赞取反
        let num = list[i].is_praise ? 1 : -1; //点赞+1 取消点赞-1
        list[i].praise = list[i].praise ? list[i].praise * 1 + num : 1; //点赞数
        this.setState({
            list
        })

        //获取社区ID 用跳转的URL中裁剪出groupId
        let groupId = list[i].forwardUrl.substr(list[i].forwardUrl.indexOf('/post/') + 6);
        groupId = groupId.substr(0, groupId.indexOf('/'))

        //更新点赞状态  这里不等待接口返回 "ac_type":1//默认为空 4：帖子点赞 5：帖子取消点赞
        let postsId = list[i].objId;
        let ac_type = num > 0 ? 4 : 5;
        this.props.indexPageActions.postPraiseAsync({ groupId, postsId, ac_type });
    }

    /**
     * 跳转到指定URL
     */
    toUrl(url) {
        if (url) {
            window.location.href = url;
        }
    }

    render() {
        let { icons } = this.props.indexPage;
        let { loadingMoreProps, list, carList, showAddCar, carPrefixList } = this.state;

        //保存信息流到缓存中
        sessionStorage.setItem('H5Index_home_state_list', JSON.stringify(list));

        if (list.length === 0) {
            loadingMoreProps.msgType = 3; //没有消息时 不显示end状态
        }

        return (
            <div>
                <LoadingMore {...loadingMoreProps}>
                    <CarList {...carList} addCar={() => this.addCar()} />
                    <IconList data={icons} />
                    <div style={{ height: '.24rem' }}></div>
                    {/* 消息流 start */}
                    {
                        list.length > 0 && list.map((item, i) =>
                            <div key={`msg-${i}-${item.id}`} id={`msg-dom-${i}-${item.id}`} style={{ 'transition': 'all .3s' }}>
                                {
                                    this.getCardDom(item, i)
                                }
                                <div style={{ height: '.24rem' }}></div>
                            </div>)
                    }
                    {/* 消息流 end */}
                </LoadingMore>

                {/*添加车辆蒙层 start*/}
                {
                    showAddCar
                        ? <AddCar
                            carPrefixList={carPrefixList}
                            onClick={(carNumber) => this.toAddCarAuthPage(carNumber)}
                            close={() => this.hideAddCar()}
                        />
                        : ''
                }

                {/*添加车辆蒙层 end*/}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cars: state.cars,
    indexPage: state.indexPage
})

const mapDispatchToProps = dispatch => ({
    carsActions: bindActionCreators(carsActions, dispatch),
    indexPageActions: bindActionCreators(indexPageActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeV1_3)