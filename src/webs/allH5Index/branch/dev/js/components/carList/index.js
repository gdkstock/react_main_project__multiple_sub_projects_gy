/**
 * 车辆相关：车辆列表；车辆卡片
 */
import { Carousel, Icon } from 'antd-mobile'
import Style from './index.scss'

//常用工具类
import config from '../../config'
import common from '../../utils/common'

/**
 * 车辆列表
 * @param {*Object} props 
 */
const CarList = props => {
    let cars = props.data;

    if (cars.length < 3 && cars.indexOf('addCar') === -1) {
        cars.push('addCar'); //添加车辆的标识符
    }

    if (cars.length === 1) {
        //没有车辆
        return (
            <div className={Style.carListBox}>
                {/*车辆列表 start*/}
                <div style={{ width: '6.5rem', margin: '0 auto' }}>
                    <AddCar type='0' addCar={() => props.addCar()} />
                </div>
                {/*车辆列表 end*/}
            </div>
        )
    }

    return (
        <div className={Style.carListBox}>
            {/*车辆列表 start*/}
            <div style={{ width: '6.5rem', margin: '0 auto' }}>
                <Carousel
                    className="carListCarousel"
                    autoplay={false}
                    selectedIndex={0}
                    dots={false}
                >
                    {cars.map((item, i) => (
                        <div key={item} className={Style.cardBox}>
                            {item == 'addCar'
                                ? <AddCar type='1' addCar={() => props.addCar()} />
                                : <Car {...item} />
                            }
                        </div>
                    ))}
                </Carousel>
            </div>
            {/*车辆列表 end*/}
        </div>
    )
}

/**
 * 单个车辆卡片
 * @param {*Object} props 
 */
const Car = props => {
    let { carId, carNumber, isCorrect, carBrandLogoUrl, totalNumber, totalFine, totalDegree, inspectionState, inspectionDay } = props

    //信息有误
    if (isCorrect !== 1) {
        totalNumber = '?'
        totalFine = '?'
        totalDegree = '?'
    }

    //违章相关
    let btnStyle = Style.blueBtn; //按钮样式
    let canClickBtn = true; //按钮可点击

    if (totalNumber === undefined) {
        btnStyle = Style.blueBtnFalse;
        canClickBtn = false;
        //违章信息
        totalNumber = '?'
        totalFine = '?'
        totalDegree = '?'
    }

    //车辆logo
    carBrandLogoUrl = carBrandLogoUrl || './images/carIcon.png';

    //年检状态
    let content = '';
    switch (inspectionState) {
        case -1:
            content = <div><span>开通年检提醒</span><Icon type='right' /></div>;
            break;
        case 0:
            content = <div><i className={Style.icon_loud_org}></i><span>{`距年检预约还有${inspectionDay}天`}</span></div>;
            break;
        case 1:
            content = <div><i className={Style.icon_loud_org}></i><span>{`距年检截止还剩${inspectionDay}天`}</span></div>;
            break;
        case 2:
            content = <div><i className={Style.icon_loud_org}></i><span>{`年检已逾期${inspectionDay}天，仍可免检`}</span></div>;
            break;
        case 3:
            content = <div><i className={Style.icon_loud_org}></i><span>{`年检已逾期${inspectionDay}天，需上线检测`}</span></div>;
            break;
        case 4:
            content = <div><i className={Style.icon_loud_org}></i><span>{`年检已严重逾期，需上线检测`}</span></div>;
            break;
        case 5:
            content = <div><i className={Style.icon_loud_org}></i><span>{`车辆已报废，请勿驾驶`}</span></div>;
            break;
        case 6:
            content = <div><i className={Style.icon_loud_org}></i><span>{`年检办理中`}</span></div>;
            break;
        default:
            content = <div><span>开通年检提醒</span><Icon type='right' /></div>
    }

    return (
        <div>
            <div className={Style.cardTop} onClick={() => toCarInfo(carId, carNumber)}>

                <div className={Style.logo}>
                    <img
                        src={carBrandLogoUrl}
                        onError={(e) => e.target.src != './images/carIcon.png' ? e.target.src = './images/carIcon.png' : false}
                    />
                </div>
                <div className='absolute-vertical-center' style={{ width: '5.8rem' }}>
                    <div className={Style.carInfoText}>
                        <span className={Style.carNumber}>{carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}</span>
                    </div>
                    <div className={Style.nianjian} onClick={(e) => {
                        e.stopPropagation();
                        toInspection(carId, carNumber)
                    }}>
                        {content}
                    </div>
                </div>
            </div>
            {
                isCorrect !== 1
                    ? <div className={btnStyle} onClick={() => canClickBtn && toEditCar(carId, carNumber)}>修改</div>
                    : <div className={btnStyle} onClick={() => canClickBtn && toViolation(carId, carNumber)}>{totalNumber ? '办理' : '查新'}</div>
            }
            <div className={Style.carFooter}>
                <ul className={Style.footerBtn}>
                    <li>违章{totalNumber || '0'}</li>
                    <li>罚款{totalFine || '0'}</li>
                    <li>扣分{totalDegree || '0'}</li>
                </ul>
            </div>
        </div>
    )
}

/**
 * 添加车辆卡片
 * @param {*Object} props 
 * {
 * type: '0' //0:无车辆信息 1:有车辆信息
 * }
 */
const AddCar = props => {
    let imgSrc = './images/'
    let className = Style.addCar
    if (props.type == '0') {
        imgSrc += 'btn_addcar.png'
        className += ' ' + Style.noCar
    } else {
        imgSrc += 'btn_addcar_org.png'
    }

    return (
        <div className={className} onClick={() => props.addCar()}>
            <div className={Style.addCarImg}>
                <img src={imgSrc} />
            </div>
            <h3>添加车辆查违章</h3>
            <p>享受新违章通知、在线办理违章和年审等服务</p>
        </div>
    )
}

/**
 * 跳转到违章业务
 */
const toViolation = (carId, carNumber) => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_weizhang',
    })
    window.location.href = config.violationUrl + '?' + common.getUserInfoString() + `#/violationList/${carId}`
}

/**
 * 跳转到年检业务
 */
const toInspection = (carId, carNumber) => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_nianjian',
    })
    carNumber = encodeURIComponent(carNumber); //URL编码
    // window.location.href = config.inspectionReminderUrl + `/${carNumber}/${carId}`;
    window.open(config.inspectionReminderUrl + `/${carNumber}/${carId}`, "_self");
}

/**
 * 跳转到车辆详情
 */
const toCarInfo = (carId, carNumber) => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_cardetail',
    })
    window.location.href = config.violationUrl + '?' + common.getUserInfoString() + `#carDetail/${carId}/getDatas`
}

/**
 * 跳转到编辑车辆页面
 */
const toEditCar = (carId, carNumber) => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_editCar',
    })
    window.location.href = config.violationUrl + '?' + common.getUserInfoString() + `#editCar/${carId}`
}

/**
 * 跳转到添加车辆页面
 */
const toAddCar = () => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_addcar',
    })
    window.location.href = config.addCarUrl + '?' + common.getUserInfoString();
}


export default CarList