/**
 * 快捷入口Icon相关
 */
import { Carousel } from 'antd-mobile'

import Style from './index.scss'

import common from '../../utils/common'

/**
 * 快捷入口的ICON列表
 * @param {*Object} props 
 */
const IconList = props => {
    if (props.data.length < 6) {
        //不需要翻页
        return (
            <div className={Style.iconBox}>
                <ul className={Style.iconList}>
                    {props.data.map((item, i) => <li><Icon {...item} /></li>)}
                </ul>
            </div>
        )
    } else {
        //需要翻页
        var pages = []; //页面列表
        let pageNum = 0;
        props.data.map((item, i) => {
            pageNum = Math.floor(i / 5); //当前页码
            if (!pages[pageNum]) {
                pages[pageNum] = []; //不存在时，则定义一个数组
            }
            pages[pageNum].push(
                <li key={'icon-' + pageNum + '-' + i}><Icon {...item} /></li>
            ); //页面ICONS
        })

    }

    return (
        <div className={Style.iconBox}>
            <Carousel
                className="iconListCarousel"
                autoplay={false}
                selectedIndex={0}
            >
                {pages.map((item, i) => (
                    <ul key={'iconPage' + i} className={Style.iconList}>
                        {item}
                    </ul>
                ))}
            </Carousel>
        </div>
    )
}

/**
 * 单个icon
 * @param {*Object} props 
 */
const Icon = props => {
    let { name, imgUrl, url, sort, subName, eventId, isJs, funName } = props
    return (
        <div className={Style.icon} onClick={() => isJs ? toFun(funName, eventId) : toUrl(url, eventId)}>
            <img src={imgUrl} />
            <p className='text-overflow-1'>{name}</p>
            <span className={subName ? Style.subTitle : 'hide'}>{subName}</span>
        </div>
    )
}

/**
 * 跳转到指定URL
 * @param {*string} url 
 * @param {*string} eventId 
 */
const toUrl = (url, eventId = '') => {
    if (url) {
        if (eventId) {
            common.sendCxytj({
                eventId: eventId,
            })
        }
        setTimeout(() => {
            window.location.href = url; //URL跳转
        }, 50) //延迟跳转，避免埋点无法统计
    }
}

/**
 * 处理指定function
 * @param {*string} url 
 * @param {*string} eventId 
 */
const toFun = (funName, eventId = '') => {
    if (window[funName]) {
        if (eventId) {
            common.sendCxytj({
                eventId: eventId,
            })
        }
        window[funName]();
    }
}

export default IconList