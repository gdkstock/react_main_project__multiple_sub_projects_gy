/**
 * 车辆相关接口
 */

import apiHelper from './apiHelper';

class CarService {

	/**
	 * 获取车辆列表 包含违章信息 1.2版
	 * @param {*object} data
	 */
	carList(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}app/violations`,
			data: {
				method: 'post',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}

	/**
	 * 根据车牌号码获取相关信息
	 * @param {*object} data
	 * {
	 * carNumber:''//车牌号码
	 * }
	 */
	information(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}car/information`,
			data: {
				method: 'post',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}

	/**
	 * 获取查询违章条件列表
	 * @param {*object} data
	 */
	carCondition(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}car/getCarConditionList`,
			data: {
				method: 'get',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}

	/**
	 * 获取车辆列表 1.3
	 * @param {*object} data
	  carId	string	是	车辆id
	  carNumber	string	是	车牌号码
	  carBrandLogoUrl	string	否	车辆品牌logo url
	  isCorrect	int	是	0 需要补充资料，1 不需要
	 */
	cars(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}car/list?t=${new Date().getTime()}`,
			data: {
				method: 'post',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}

	/**
	 * 车辆违章信息接口 1.3
	 * @param {*object} data
	  carId	string	是	车辆id
	  count	int	是	未处理违章数量
	  fine	double	是	未处理违章的总罚款金额，单位：元
	  degree	int	是	未处理违章的总扣分
	  untreatedIds	array	是	未办理违章id json列表
	 */
	violation(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}violation/list`,
			data: {
				method: 'post',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}

	/**
	 * 查询车辆年检概要信息
	 * @param {*object} data
	 */
	inspectionBrief(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}car/inspection`,
			data: {
				method: 'post',
				body: data
			}
		};

		return apiHelper.fetch(requestParam);
	}

}

export default new CarService()