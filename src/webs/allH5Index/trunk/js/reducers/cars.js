/*
	全局状态cars
*/
import {
	ADD_CAR_LIST,
	DELETE_CAR_LIST,
	UPDATE_CAR
} from '../actions/actionsTypes'

const initState = {
	data: {},
	result: []
}

export default function cars(state = initState, action) {
	let _state = JSON.parse(JSON.stringify(state)), _data = {}, _result = [], actionData = {};
	switch (action.type) {
		case ADD_CAR_LIST:
			_data = action.data.data;
			_result = action.data.result;

			if (_result && _result.length > 0) {
				//遍历合并旧数据 避免UI出现闪烁
				_result.map(carId => {
					_data[carId] = Object.assign({}, _state.data[carId] || {}, _data[carId]);
				})
				return {
					data: _data,
					result: _result
				};
			}
			return {
				data: {},
				result: []
			};
		case UPDATE_CAR:
			//存在车辆信息
			if (action.data.carId && _state.data[action.data.carId]) {
				_state.data[action.data.carId] = Object.assign({}, _state.data[action.data.carId], action.data);

				return Object.assign({}, state, _state);
			}
		case DELETE_CAR_LIST: //清空车辆列表
			return {
				data: {},
				result: []
			};
		default:
			return state;
	}
}


