import { combineReducers } from 'redux'
import cars from './cars'
import indexPage from './indexPage'
import user from './users'

const rootReducer = combineReducers({
    cars,
    indexPage,
    user, //用户信息表
});

export default rootReducer;
