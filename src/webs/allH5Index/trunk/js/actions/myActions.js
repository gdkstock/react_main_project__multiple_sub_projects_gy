/*
* 我的页面相关action
*/
import {
    UPDATE_USER_DETAIL,
    UPDATE_USER_DETAIL_ASYNC,
    QUERY_USER_DETAIL_ASYNC,
    QUERY_USER_ICONS_ASYNC,
    USER_BIND_ASYNC,
    USER_UN_BIND_ASYNC,
    USER_BIND_CHECK_ASYNC,
    USER_SEND_SMS_ASYNC
} from './actionsTypes.js'


//更新用户信息
export const updateUserDetail = (data) => ({ type: UPDATE_USER_DETAIL, data })
export const updateUserDetailAsync = (data, callback) => ({ type: UPDATE_USER_DETAIL_ASYNC, data, callback })

//获取个人信息
export const queryUserDetailAsync = (data, callback) => ({ type: QUERY_USER_DETAIL_ASYNC, data, callback })

//ICON列表
export const queryUserIconsAsync = (data, callback) => ({ type: QUERY_USER_ICONS_ASYNC, data, callback })

//绑定用户
export const userBindAsync = (data, callback) => ({ type: USER_BIND_ASYNC, data, callback })

//解除绑定
export const userUnBindAsync = (data, callback) => ({ type: USER_UN_BIND_ASYNC, data, callback })

//查询用户是否绑定
export const userBindCheckAsync = (data, callback) => ({ type: USER_BIND_CHECK_ASYNC, data, callback })

//发送手机验证码
export const userSendSmsAsync = (data, callback) => ({ type: USER_SEND_SMS_ASYNC, data, callback })
