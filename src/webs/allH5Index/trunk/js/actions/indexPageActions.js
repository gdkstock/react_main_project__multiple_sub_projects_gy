/*
* 首页相关action
*/
import {
	GET_INDEX_DATAS,
	QUERY_MESSAGES_ASYNC,
	MESSAGES_IGNORE_ASYNC,
	QUERY_POST_INFO_ASYNC,
	QUERY_HOT_NEWS_VIEW_COUNT_ASYNC,
	POST_PRAISE_ASYNC
} from './actionsTypes'

//请求首页的所有数据
export const getIndexDatas = () => ({ type: GET_INDEX_DATAS })

//获取消息流列表
export const queryMessagesAsync = (data, callback) => ({ type: QUERY_MESSAGES_ASYNC, data, callback })

//忽略消息
export const messagesIgnoreAsync = (data, callback) => ({ type: MESSAGES_IGNORE_ASYNC, data, callback })

//查询帖子点赞数、评论数、是否已经点赞
export const queryPostInfoAsync = (data, callback) => ({ type: QUERY_POST_INFO_ASYNC, data, callback })

//获取热门资讯阅读数
export const queryHotNewsViewCountAsync = (data, callback) => ({ type: QUERY_HOT_NEWS_VIEW_COUNT_ASYNC, data, callback })

//帖子点赞
export const postPraiseAsync = (data, callback) => ({ type: POST_PRAISE_ASYNC, data, callback })
