/*
* 车辆及驾照相关action
*/
import {
	QUERY_CAR_LIST_ASYNC,
	GET_CAR_INFORMATION,
	GET_CAR_CONDITION_LIST,
	QUERY_VIOLATION_ASYNC,
	QUERY_CARS_ASYNC,
} from './actionsTypes'

//请求车辆列表 包含违章信息
export const queryCarListAsync = data => ({ type: QUERY_CAR_LIST_ASYNC, data })

//根据车牌号码获取相关信息
export const getCarInformation = (data, callback) => ({ type: GET_CAR_INFORMATION, data, callback })

//获取车牌前缀列表和查询违章条件列表 
export const getCarConditionList = (data, callback) => ({ type: GET_CAR_CONDITION_LIST, data, callback })

//根据carId获取违章信息 
export const queryViolationAsync = (data, callback) => ({ type: QUERY_VIOLATION_ASYNC, data, callback })

//获取车辆列表 基础信息 
export const queryCarsAsync = (data, callback) => ({ type: QUERY_CARS_ASYNC, data, callback })