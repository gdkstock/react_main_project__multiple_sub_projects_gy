
export const ChMessage = {
    FETCH_FAILED: '系统繁忙，请稍后再试',
    FETCH_TIMEOUT: '网络超时，请稍后再试',
    UPLOAD_FAILED: '上传失败！',
    ILLEGAL_REQUEST: '非法请求',
}