import React, { Component } from 'react'
//styles
import styles from './index.scss';
import { Toast } from 'antd-mobile';

//常用工具类
import common from '../../utils/common';

/**
 * 默认消息流卡片
 * @param {*object} props 
 */
export const DefaultCard = props => {
    let { forwardUrl, toUrlEventId } = props;

    return (
        <div className={styles.box}>
            <Ellipsis {...props.ellipsisProps} />
            <CardHeader {...props} />
            <CardContent {...props} />
        </div>
    )
}

/**
 * 热门资讯消息流卡片
 * @param {*object} props 
 */
export const HotNewsCard = props => {
    let { imgUrl, forwardUrl, toUrlEventId, styleType, specificType } = props;
    let showToast = specificType == 210 || specificType == 220; //不跳转只显示toast
    return (
        styleType === 1
            ?
            <div className={styles.box}>
                <Ellipsis {...props.ellipsisProps} />
                <CardHeader {...props} />
                <Imgs {...props} />
                <CardContent {...props} pClassName='text-overflow-2' />
                <HotNewsFooter {...props} className={styles.newsFooter2} />
                <div style={{ clear: 'both' }}></div>
            </div>
            :
            < div className={styles.box} >
                <Ellipsis {...props.ellipsisProps} />
                <CardHeader {...props} />
                <CardContent {...props} />
                <div className={styles.newsImg} onClick={() => showToast ? Toast.info('请下载APP查看此消息详情'): common.toUrlAndSendCxytj(forwardUrl, toUrlEventId)}>
                    <img className='img-cover' src={imgUrl ? imgUrl[0] : ''} />
                </div>
                <HotNewsFooter {...props} />
            </div >
    )
}

/**
 * 社区消息流卡片
 * @param {*object} props 
 */
export const CommunityCard = props => {

    return (
        <div className={styles.box}>
            <Ellipsis {...props.ellipsisProps} />
            <CardHeader {...props} />
            <Imgs {...props} />
            <CardContent {...props} pClassName='text-overflow-2' />
            <CommunityFooter {...props} />
            <div style={{ clear: 'both' }}></div>
        </div>
    )
}

/**
 * 广告图消息流卡片
 * @param {*object} props 
 */
export const AdCard = props => {
    let { img } = props;
    return (
        <div className={styles.ad}>
            <img src={img} />
        </div>
    )
}


//--------------------------通用UI start -----------------------------

//头部
const CardHeader = props => {
    let { iconUrl, typeName, time, onClick } = props;

    return (
        <div className={styles.header} onClick={() => onClick ? onClick() : ''}>
            <div className={styles.headerImg}>
                <img src={iconUrl || './images/icon_xiaoxi.png'} />
            </div>
            <div className={styles.headerText}>
                <p>
                    <span className={styles.typeName}>{typeName}</span>
                    <span className={styles.time}>{time}</span>
                </p>
            </div>
        </div>
    )
}

//内容  pClassName:P标签的类名
const CardContent = props => {

    let { title, forwardUrl, toUrlEventId, pClassName, specificType } = props;
    let showToast = specificType == 210 || specificType == 220; //不跳转只显示toast
    return (
        <div className={styles.content} onClick={() => showToast ? Toast.info('请下载APP查看此消息详情'): common.toUrlAndSendCxytj(forwardUrl, toUrlEventId)}>
            <p className={pClassName || ''}>{title}</p>
        </div>
    )
}

//省略号
const Ellipsis = props => {

    return (
        <div className={styles.ellipsis}>
            <div className={styles.ellipsisBtn} onClick={() => showFList(props)}></div>
            <div
                id={'ellipsis-f-list-' + props.id}
                className={styles.fList}
                onClick={() => hideFList(props)}
            >
                {
                    props.list && props.list.map((item, i) =>
                        <div
                            key={props.id}
                            className={styles.fItem}
                            onClick={() => clickItem(props.id, item)}
                        >
                            <i className={styles.icon_eyesee}></i>
                            <h3>{item.title}</h3>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

//热门资讯尾部
const HotNewsFooter = props => {
    let { view, author, updateTime, className } = props;

    let timeText = dateFormat(updateTime / 1000);

    return (
        <div className={className || styles.newsFooter}>
            <span className={styles.newsTime}>{view || 0}人浏览</span>
        </div>
    )
}

//社区尾部
const CommunityFooter = props => {

    let { comments, praise, is_praise, clickComments, clickSupports, imgUrl } = props;
    const img = imgUrl ? imgUrl[0] : ''; //用于区分有无图片的样式

    return (
        <div className={img ? styles.communityFooter : styles.communityFooterNoImg}>
            <div>
                <span
                    className={is_praise ? styles.communitySupportsTrue : styles.communitySupports}
                    onClick={() => clickSupports ? clickSupports(props) : false}
                >{praise > 0 ? praise : <i>&nbsp;</i>}</span>
            </div>
            <div>
                <i
                    className={styles.communityComments}
                    onClick={() => clickComments ? clickComments(props) : false}
                >{comments > 0 ? comments : <i>&nbsp;</i>}</i>
            </div>
        </div>
    )
}

class Imgs extends Component {
    constructor(props) {
        super(props)

        this.state = {
            imgError: 0, //0：正常 1：出错
        };
    }

    render() {
        let { imgUrl, forwardUrl, toUrlEventId, specificType } = this.props;
        let showToast = specificType == 210 || specificType == 220; //不跳转只显示toast
        let { imgError } = this.state;
        const img = imgUrl ? imgUrl[0] : '';
        return (
            <div className={img ? styles.imgs : 'hide'}>
                {
                    !imgError
                        ? <img
                            src={img}
                            className='img-cover'
                            onClick={() => showToast ? Toast.info('请下载APP查看此消息详情'): common.toUrlAndSendCxytj(forwardUrl, toUrlEventId)}
                            onError={() => this.setState({ imgError: 1 })}
                        />
                        : <div className={styles.noImgBox}>
                            <img
                                src='./images/icon_no.png'
                                onClick={() => showToast ? Toast.info('请下载APP查看此消息详情'): common.toUrlAndSendCxytj(forwardUrl, toUrlEventId)}
                            />
                        </div>
                }
            </div>
        )
    }
}


//--------------------------通用UI end -----------------------------


//--------------------------事件 start -----------------------------

//显示浮层列表
const showFList = props => {
    let obj = document.getElementById('ellipsis-f-list-' + props.id);
    if (obj) {
        //存在
        obj.style.display = 'inherit'; //显示
    }
}

//隐藏浮层列表
const hideFList = props => {
    let obj = document.getElementById('ellipsis-f-list-' + props.id);
    if (obj) {
        //存在
        obj.style.display = 'none'; //显示
    }
}

//点击item
const clickItem = (id, props) => {
    props.click(id); //等待动画延迟删除数据
}

//人性化时间 	示例：dateFormat((new Date().getTime()/1000)-60); //一分钟前
const dateFormat = (dateTimeStamp) => {
    Date.prototype.format = function (format) {
        var date = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
            "q+": Math.floor((this.getMonth() + 3) / 3),
            "S+": this.getMilliseconds()
        };
        if (/(y+)/i.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
        }
        for (var k in date) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1
                    ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
            }
        }
        return format;
    };

    let minute = 1000 * 60;
    let hour = minute * 60;
    let day = hour * 24;
    let halfamonth = day * 15;
    let month = day * 30;
    if (!dateTimeStamp || dateTimeStamp == 0) {
        return '--';
    }
    let now = new Date().getTime();
    let diffValue = now - dateTimeStamp * 1000;
    if (diffValue < 0) {
        //若日期不符则弹出窗口告之
        //alert("结束日期不能小于开始日期！");
    }
    let monthC = diffValue / month;
    let weekC = diffValue / (7 * day);
    let dayC = diffValue / day;
    let hourC = diffValue / hour;
    let minC = diffValue / minute;
    //计算今天已经过去多少小时
    let rtime = new Date(dateTimeStamp * 1000);
    //let rtime = rtime.format('yyyy-MM-dd h:m:s');
    now = new Date();
    let pastHour = parseInt(hourC);
    let hourPastToday = now.format('h');
    let hourPastWithoutToday = parseInt(hourC) - parseInt(hourPastToday);
    let result = '';
    if (pastHour <= hourPastToday) {
        if (minC < 1) {
            result = "刚刚";
        } else if (pastHour < 1) {
            result = parseInt(minC) + "分钟前";
        } else if (pastHour >= 1 && pastHour < 3) {
            result = pastHour + "小时前";
        } else {
            result = "今天" + rtime.format('hh:mm');
        }
    }
    // else if (pastHour >= hourPastToday && hourPastWithoutToday < 24) {
    //     result = "昨天" + rtime.format('hh:mm');
    // } else if (hourPastWithoutToday > 24 && hourPastWithoutToday <= 48) {
    //     result = "前天" + rtime.format('hh:mm');
    // } 
    else {
        if (monthC >= 2) {
            result = rtime.format('yyyy-MM-dd hh:mm');
        } else {
            result = rtime.format('MM-dd hh:mm');
        }
    }

    //		if (monthC >= 1) {
    //			result = parseInt(monthC) + "个月前";
    //		} else if (weekC >= 1) {
    //			result = parseInt(weekC) + "周前";
    //		} else if (dayC >= 1) {
    //			result = parseInt(dayC) + "天前";
    //		} else if (hourC >= 1) {
    //			result = parseInt(hourC) + "个小时前";
    //		} else if (minC >= 1) {
    //			result = parseInt(minC) + "分钟前";
    //		} else
    //			result = "刚刚";
    return result;
}

//--------------------------事件 end -----------------------------