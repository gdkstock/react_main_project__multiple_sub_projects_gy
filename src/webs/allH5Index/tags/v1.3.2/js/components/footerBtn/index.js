/**
 * H5首页底部按钮
 */

//styles
import styles from './index.scss'

const FooterBtn = props => {
    //模拟数据
    // let _props = {
    //     selectedIndex: 0,
    //     list: [
    //         {
    //             icon: ['./images/tab_home_selected.png', './images/tab_home_normal.png'],
    //             title: '查违章',
    //             onClick: (i) => console.log(`点击了第${i + 1}个按钮`)
    //         },
    //         {
    //             icon: ['./images/tab_me_selected.png', './images/tab_me_normal.png'],
    //             title: '我的',
    //             onClick: (i) => console.log(`点击了第${i + 1}个按钮`)
    //         }
    //     ]
    // }

    let _props;

    _props = Object.assign({}, _props, props);

    let { selectedIndex } = _props;

    return (
        <div className={_props.list ? '' : 'hide'}>
            <div style={{ height: '1.2rem' }}></div>
            <div className={styles.box}>
                <ul>
                    {
                        _props.list && _props.list.map((item, i) =>
                            <li key={`footer-item-${i}`} onClick={() => item.onClick ? item.onClick(i) : false}>
                                <img src={i === selectedIndex ? item.icon[0] : item.icon[1]} />
                                <p>{item.title}</p>
                            </li>
                        )
                    }
                </ul>
            </div>
        </div>
    )
}

export default FooterBtn;