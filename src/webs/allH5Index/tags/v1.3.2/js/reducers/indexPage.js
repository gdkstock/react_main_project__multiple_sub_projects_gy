import {
	ADD_BANNERS,
	ADD_ICONS,
	ADD_NEWS,
} from '../actions/actionsTypes'

const initState = {
	banners: [],
	icons: [],
	news: []
}

export default function banners(state = initState, action) {
	switch (action.type) {
		case ADD_BANNERS:
			return Object.assign({}, state, {
				banners: action.data
			})
		case ADD_ICONS:
			return Object.assign({}, state, {
				icons: action.data
			})
		case ADD_NEWS:
			if (action.page > 1) {
				//追加数据
				return Object.assign({}, state, {
					news: [...state.news, ...action.data]
				})
			} else {
				//只显示前10条
				return Object.assign({}, state, {
					news: action.data
				})
			}

		default:
			return state;
	}
}