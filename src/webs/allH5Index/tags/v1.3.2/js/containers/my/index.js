import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//组件
import { UserCard, MyOrder, IconList } from '../../components/my';
import { Toast } from 'antd-mobile';

//styles
import styles from './index.scss';

//actions
import * as myActions from '../../actions/myActions';

//常用工具类
import common from '../../utils/common';

//配置
import config from '../../config';

class My extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    componentDidMount() {
        this.getDatas(); //获取数据
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * 跳转
     */
    toUrl(url) {
        if (url) window.location.href = url;
    }

    /**
     * 获取数据
     */
    getDatas() {
        let { user } = this.props;

        //数据不存在
        if (user.ifBind === undefined) {
            Toast.loading('', 0)
        }

        //获取用户信息
        this.props.myActions.queryUserDetailAsync({}, res => {
            Toast.hide();
        });

        //获取icon列表
        this.props.myActions.queryUserIconsAsync({});
    }

    /**
     * 点击用户卡片
     */
    clickUserCard(ifBind) {

        // return; //屏蔽跳转 暂时不允许注册和绑定

        if (ifBind === 1) {
            common.sendCxytj({
                eventId: 'h5_e_wz_my_jifenh5_e_wz_my_head'
            })
            this.toUrl(common.getRootUrl() + 'myInfo');
        } else {
            common.sendCxytj({
                eventId: 'h5_e_wz_my_login'
            })
            this.toUrl(common.getRootUrl() + 'bindAccount');
        }
    }

    render() {
        let { user } = this.props;
        const { walletUrl, couponUrl, pointUrl } = user;
        //定义卡片的点击事件
        user = Object.assign(user, {
            onClick: () => this.clickUserCard(user.ifBind),//点击用户信息
            clickWallet: () => common.toUrlAndSendCxytj(walletUrl, 'h5_e_wz_my_wallet'), //点击资产
            clickCoupon: () => common.toUrlAndSendCxytj(couponUrl, 'h5_e_wz_my_coupon'), //点击优惠券
            clickJifen: () => common.toUrlAndSendCxytj(pointUrl, 'h5_e_wz_my_jifen'), //点击积分
        })

        return (
            <div className='box'>
                <div className='whiteBg'></div>
                <div className={styles.topBg + ' box'}>
                    <img src='./images/my-bg.png' />
                </div>
                <div className={styles.centerBox}>
                    <UserCard {...user} />
                    <div className='h24'></div>
                    <MyOrder />
                    <div className='h24'></div>
                    <IconList list={user.icons} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
})

const mapDispatchToProps = dispatch => ({
    myActions: bindActionCreators(myActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(My);