/**
 * 首页相关接口
 */

import apiHelper from './apiHelper';

class MyService {

    /**
     * 个人信息
     * @param {*object} data
     */
    userDetail(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/detail`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 绑定用户
     * @param {*object} data
     */
    userBind(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/bind`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam, { credentials: 'include' }); //允许设置cookie
    }

    /**
     * 取消绑定
     * @param {*object} data
     */
    userUnBind(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/unbind`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam, { credentials: 'include' }); //允许设置cookie
    }

    /**
     * 查看用户是否已经绑定
     * @param {*object} data
     */
    userBindCheck(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/bind/check`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * Icon列表
     * @param {*object} data
     */
    userIcons(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/icons`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 发送手机验证码
     * @param {*object} data
     */
    userSendSms(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/send/sms`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 更新用户信息
     * @param {*object} data
     */
    userUpdate(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/update`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
}

export default new MyService()