//styles
import styles from './index.scss'


/**
 * Logo
 * @param {*Object} props 
 */
export const Logo = props =>{

    return (
        <div className={styles.logo}>
            <img src='./images/logo.png' alt='车行易'/>
        </div>
    )
}