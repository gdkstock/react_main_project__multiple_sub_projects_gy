/**
 * 注册绑定账号信息
 */

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//组件
import { Logo } from '../../components/register';
import { Toast } from 'antd-mobile';

//styles
import styles from './index.scss';

//actions
import * as myActions from '../../actions/myActions';

//常用工具类
import common from '../../utils/common';
import userHelper from '../../utils/userHelper';
import { ChMessage } from '../../utils/message.config';

//配置
import config from '../../config'

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //验证码按钮不可点击
            phoneCodeBtnFalse: false,

            //手机号码
            phoneNumber: this.props.params.phoneNumber
        }

    }

    componentWillMount() {
        common.setViewTitle('注册绑定账号信息');
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * 获取验证码
     */
    getPhoneCode(e) {
        let { phoneCodeBtnFalse, phoneNumber } = this.state
        //可点击
        if (!phoneCodeBtnFalse) {
            e.innerHTML = `60秒后获取`;
            this.setState({
                phoneCodeBtnFalse: true //设为不可点击
            }, () => {
                //发送验证码
                this.sendPhoneCode(phoneNumber);

                //显示重新发送计时器
                let second = 60;
                let SEND = setInterval(() => {
                    second--;
                    //到过60秒时停止
                    if (second == 0) {
                        e.innerHTML = `重新发送`;
                        clearInterval(SEND);
                        this.setState({
                            phoneCodeBtnFalse: false //设为可点击
                        })
                    } else {
                        e.innerHTML = `${second}秒后获取`;
                    }

                }, 1000);
            })
        }
    }

    /**
     * 发送验证码
     */
    sendPhoneCode(phone) {
        this.props.myActions.userSendSmsAsync({ phone }, res => {
            //发送失败
            if (res.code != 1000) {
                Toast.info(res.msg || ChMessage.FETCH_FAILED);
            }
        })
    }

    /**
     * 注册
     */
    register() {
        let { phoneNumber } = this.state;
        let captcha = this.refs.phoneCode.value;
        let password = this.refs.password.value;

        //验证码校验
        if (!captcha) {
            Toast.info('请输入验证码', 1);
            return;
        }
        if (captcha.length !== 6) {
            Toast.info('请输入6位数验证码');
            return;
        }

        //密码校验
        if (!password) {
            Toast.info('请输入车行易APP登录密码');
            return;
        }
        if (password.length < 6 || password.length > 25) {
            Toast.info('请输入6~25位字符密码');
            return;
        }

        this.props.myActions.userBindAsync({
            phone: phoneNumber,
            captcha,
            password: md5(password).toLocaleUpperCase(), //需要MD5加密,
            type: 2 //1=密码的方式，2=验证码的方式
        }, res => {
            if (res.code == 1000) {
                //注册成功，这里重新去单点登录 获取绑定后的账号信息
                Toast.info('注册成功');
                //延迟登录
                setTimeout(() => {
                    userHelper.Login(config.authCallbackUrl);
                }, 1000);
            } else {
                Toast.info(res.msg || ChMessage.FETCH_FAILED);
            }
        })
    }

    /**
     * 验证码输入框内容发生改变
     */
    phoneCodeChange() {
        let phoneCode = this.refs.phoneCode.value;
        if (phoneCode.length > 6) {
            this.refs.phoneCode.value = phoneCode.substr(0, 6); //最长输入6位
        }
    }

    /**
     * 密码输入框内容发生改变
     */
    passwordChange() {
        let password = this.refs.password.value;
        if (password.length > 25) {
            this.refs.password.value = password.substr(0, 25); //最长输入15位
        }
    }

    /**
     * 跳转到协议
     */
    toRule() {
        window.location.href = config.ruleUrl;
    }

    render() {
        let { phoneCodeBtnFalse, phoneNumber } = this.state;

        return (
            <div className='box'>
                <div className='whiteBg'></div>
                <Logo />
                {/* 表单 start */}
                <div className={styles.list}>
                    <div className={styles.item}>
                        <span className={styles.spanInput}>{phoneNumber.substr(0, 3) + ' ' + phoneNumber.substr(3, 4) + ' ' + phoneNumber.substr(7)}</span>
                        <span className={styles.phoneCodeBtn + (phoneCodeBtnFalse ? ' ' + styles.phoneCodeBtnFalse : '')} onClick={(e) => this.getPhoneCode(e.target)}>获取验证码</span>
                    </div>
                    <div className={styles.item}>
                        <input ref='phoneCode' type='number' onChange={() => this.phoneCodeChange()} placeholder='请输入验证码' />
                    </div>
                    <div className={styles.item}>
                        <input ref='password' type='password' onChange={() => this.passwordChange()} placeholder='请输入6~25位字符密码' />
                    </div>
                </div>
                {/* 表单 end */}

                <div className={styles.btnBox}>
                    <div className={styles.btn} onClick={() => this.register()}>注册</div>
                </div>

                <div className={styles.xieyi}>
                    <p>点击“注册”，即表示同意<span onClick={() => this.toRule()}>《车行易用户服务协议》</span></p>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    myActions: bindActionCreators(myActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Register);