import React, { Component } from 'react'

//组件
import HomeV1_3 from './homeV1_3';
import My from './my';
import FooterBtn from '../components/footerBtn';

//常用工具类
import common from '../utils/common';

class Home extends Component {
	constructor(props) {
		super(props)

		this.state = {

			//底部按钮的props selectedIndex：默认选中的tab索引
			footerBtnProps: {
				selectedIndex: sessionStorage.getItem('H5Index_selectedIndex') !== null
					? sessionStorage.getItem('H5Index_selectedIndex') * 1 //优先从缓存读取
					: common.getUrlKeyValue('selectedIndex') ? common.getUrlKeyValue('selectedIndex') * 1 : 0,
				list: [
					{
						icon: ['./images/tab_home_selected.png', './images/tab_home_normal.png'],
						title: '查违章',
						onClick: (i) => this.setSelectIndex(i)
					},
					{
						icon: ['./images/tab_me_selected.png', './images/tab_me_normal.png'],
						title: '我的',
						onClick: (i) => this.setSelectIndex(i)
					}
				]
			}
		}

	}

	componentWillMount() {
		common.setViewTitle('车行易查违章');
	}

	componentDidMount() {
		common.sendCxytj({
			eventId: 'h5_p_sy'
		})
	}

	componentWillReceiveProps(nextProps) {

	}

	componentWillUnmount() {

	}

	/**
	 * 修改页面的显示
	 */
	setSelectIndex(i) {
		let { footerBtnProps } = this.state;

		if (footerBtnProps.selectedIndex === i) {
			//处于当前页面 所以不刷新了
			return;
		}

		sessionStorage.setItem('H5Index_selectedIndex', i); //用session记录当前的页面

		this.setState({
			footerBtnProps: Object.assign({}, footerBtnProps, {
				selectedIndex: i
			})
		})

		//提交埋点数据
		common.sendCxytj({
			eventId: i === 0 ? 'h5_e_sy_cwz' : 'h5_e_sy_my'
		})
	}

	render() {
		let { footerBtnProps } = this.state;

		return (
			<div>
				{
					footerBtnProps.selectedIndex === 0 ? <HomeV1_3 /> : <My />
				}
				<FooterBtn {...footerBtnProps} />
			</div>
		)
	}
}

export default Home