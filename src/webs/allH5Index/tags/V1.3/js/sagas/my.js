/**
 * 我的页面相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    QUERY_USER_DETAIL_ASYNC,
    QUERY_USER_ICONS_ASYNC,
    USER_BIND_ASYNC,
    USER_UN_BIND_ASYNC,
    USER_BIND_CHECK_ASYNC,
    USER_SEND_SMS_ASYNC,
    UPDATE_USER_DETAIL_ASYNC,
    DELETE_CAR_LIST,
    USER_BIND_OR_USER_UN_BIND
} from '../actions/actionsTypes.js'

//antd
import { Toast } from 'antd-mobile'

//actions
import * as myActions from '../actions/myActions'

//server
import myService from '../services/myService'

//常用工具类
import { ChMessage } from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库


//获取个人信息
function* queryUserDetailAsync(action) {
    let callbackData = { code: 'fail' };

    try {
        const { result, timeout } = yield race({
            result: call(myService.userDetail, action.data),
            timeout: call(delay, 30000)
        })

        if (timeout) {
            //超时
        } else {
            callbackData = result;
            if (result.code == 1000) {
                yield put(myActions.updateUserDetail(result.data)) //更新用户信息
            }
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchQueryUserDetailAsync() {
    yield takeLatest(QUERY_USER_DETAIL_ASYNC, queryUserDetailAsync)
}

//获取icon列表
function* queryUserIconsAsync(action) {
    let callbackData = { code: 'fail' };

    try {
        let timeout = false;
        let result = {};
        //存在缓存数据
        if (sessionStorage.getItem('fetch_queryUserIconsAsync')) {
            //从缓存中读取
            result = JSON.parse(sessionStorage.getItem('fetch_queryUserIconsAsync'));
        } else {
            //不存在缓存数据
            const { _result, _timeout } = yield race({
                _result: call(myService.userIcons, action.data),
                _timeout: call(delay, 30000)
            })
            timeout = _timeout;
            result = _result;
        }

        if (timeout) {
            //超时
        } else {
            callbackData = result;
            if (result.code == 1000) {
                //更新用户信息
                yield put(myActions.updateUserDetail({
                    icons: result.data
                }))

                //保存接口数据，避免频繁请求
                sessionStorage.setItem('fetch_queryUserIconsAsync', JSON.stringify(result));
            }
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchQueryUserIconsAsync() {
    yield takeLatest(QUERY_USER_ICONS_ASYNC, queryUserIconsAsync)
}

//绑定用户
function* userBindAsync(action) {
    let callbackData = { code: 'fail' };
    Toast.loading('', 0);
    try {
        const { result, timeout } = yield race({
            result: call(myService.userBind, action.data),
            timeout: call(delay, 30000)
        })

        Toast.hide();

        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
            if (result.code == 1000) {
                yield put({ type: USER_BIND_OR_USER_UN_BIND });
            }
        }
    } catch (error) {
        Toast.hide();
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchUserBindAsync() {
    yield takeLatest(USER_BIND_ASYNC, userBindAsync)
}

//解除绑定
function* userUnBindAsync(action) {
    let callbackData = { code: 'fail' };
    Toast.loading('', 0);
    try {
        const { result, timeout } = yield race({
            result: call(myService.userUnBind, action.data),
            timeout: call(delay, 30000)
        })

        Toast.hide();

        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
            if (result.code == 1000) {
                yield put({ type: USER_BIND_OR_USER_UN_BIND });
            }
        }
    } catch (error) {
        Toast.hide();
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchUserUnBindAsync() {
    yield takeLatest(USER_UN_BIND_ASYNC, userUnBindAsync)
}

//用户信息发生绑定或解绑事件
function* userUnBindOrUserUnBind() {
    console.log("用户信息发送变化,这里处理一堆需要处理的事情")
    yield put(myActions.updateUserDetail({ ifBind: undefined })) //更新用户信息为未知账号信息
    yield put({ type: DELETE_CAR_LIST }) //清空车辆列表
    sessionStorage.removeItem('H5Index_home_state_list'); //清空信息流
}
function* watchUserUnBindOrUserUnBind() {
    yield takeLatest(USER_BIND_OR_USER_UN_BIND, userUnBindOrUserUnBind)
}

//查询用户是否绑定
function* userBindCheckAsync(action) {
    let callbackData = { code: 'fail' };
    Toast.loading('', 0);
    try {
        const { result, timeout } = yield race({
            result: call(myService.userBindCheck, action.data),
            timeout: call(delay, 30000)
        })

        Toast.hide();

        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
        }
    } catch (error) {
        Toast.hide();
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchUserBindCheckAsync() {
    yield takeLatest(USER_BIND_CHECK_ASYNC, userBindCheckAsync)
}

//发送手机验证码
function* userSendSmsAsync(action) {
    let callbackData = { code: 'fail' };
    try {
        const { result, timeout } = yield race({
            result: call(myService.userSendSms, action.data),
            timeout: call(delay, 30000)
        })

        if (timeout) {
            //超时
        } else {
            callbackData = result;
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchUserSendSmsAsync() {
    yield takeLatest(USER_SEND_SMS_ASYNC, userSendSmsAsync)
}

//更新用户信息
function* updateUserDetailAsync(action) {

    //同步更新用户数据 不等待接口返回
    yield put(myActions.updateUserDetail(action.data));

    //异步更新数据
    let callbackData = { code: 'fail' };
    try {
        const { result, timeout } = yield race({
            result: call(myService.userUpdate, action.data),
            timeout: call(delay, 3000) //只等待3秒
        })

        if (timeout) {
            //超时
        } else {
            callbackData = result;
            if (result.code != 1000 || !result.data.flag) {
                //数据更新失败
                Toast.info(result.msg || ChMessage.FETCH_FAILED);
            }
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchUpdateUserDetailAsync() {
    while (true) {
        let action = yield take(UPDATE_USER_DETAIL_ASYNC);
        yield fork(updateUserDetailAsync, action);
    }
}


export function* watchMyFetch() {
    yield [
        fork(watchQueryUserDetailAsync),
        fork(watchQueryUserIconsAsync),
        fork(watchUserBindAsync),
        fork(watchUserUnBindAsync),
        fork(watchUserBindCheckAsync),
        fork(watchUserSendSmsAsync),
        fork(watchUpdateUserDetailAsync),
        fork(watchUserUnBindOrUserUnBind)
    ]
}