/**
 * 车辆相关
 */
import {
	takeEvery,
	delay,
	takeLatest,
	buffers,
	channel,
	eventChannel,
	END
} from 'redux-saga'
import {
	race,
	put,
	call,
	take,
	fork,
	select,
	actionChannel,
	cancel,
	cancelled
} from 'redux-saga/effects'

import {
	QUERY_CAR_LIST_ASYNC,
	ADD_CAR_LIST,
	DELETE_CAR_LIST,
	GET_CAR_INFORMATION,
	GET_CAR_CONDITION_LIST,
	QUERY_VIOLATION_ASYNC,
	QUERY_CARS_ASYNC,
	UPDATE_CAR,
} from '../actions/actionsTypes.js'

//antd
import { Toast } from 'antd-mobile'

//server
import carService from '../services/carService'

//常用工具类
import { normalize, schema } from 'normalizr'; //范式化库
import { ChMessage } from '../utils/message.config';

/*==========================获取违章查询条件信息-引导注册============================*/
function* fetchCarConditionList(action) {
	Toast.loading("", 0)
	try {
		let result;
		if (sessionStorage.getItem("carCondition")) {
			result = JSON.parse(sessionStorage.getItem("carCondition"))
		} else {
			const { _result, timeout } = yield race({
				_result: call(carService.carCondition, action.data),
				timeout: call(delay, 30000)
			})
			result = _result

			if (timeout) {
				window.networkError('./images/networkError-icon.png');
				return;
			}
		}
		Toast.hide()
		if (result.code == "1000") {
			sessionStorage.setItem("carCondition", JSON.stringify(result)) //成功的时候 才保存，避免网络错误时，每次都提示网络错误

			//提取城市
			const citi = new schema.Entity('citi', {}, { idAttribute: 'carNumberPrefix' });
			const citis = new schema.Array(citi)
			const province = { cities: citis }
			const provinces = new schema.Array(province)
			let normalizedData = normalize(result.data.carConditionList, provinces)

			//提取省份
			const newProvince = new schema.Entity('province', {}, { idAttribute: 'provincePrefix' })
			const newProvinces = new schema.Array(newProvince)
			const newNormalizedData = normalize(normalizedData.result, newProvinces)

			if (action.callback) {
				action.callback({
					code: '1000',
					data: normalizedData.entities.citi,
					result: newNormalizedData.result
				})
			}
		} else {
			Toast.info(result.msg || ChMessage.FETCH_FAILED, 1);
			if (action.callback) {
				action.callback({
					code: 'failed',
					data: {},
					result: []
				})
			}
		}

	} catch (error) {
		Toast.hide()
		Toast.info(ChMessage.FETCH_FAILED, 1);
		if (action.callback) {
			action.callback(error)
		}
	}

}

function* watchCarConditionList() {
	yield takeLatest(GET_CAR_CONDITION_LIST, fetchCarConditionList)
}

/*==========================根据车牌号码获取相关信息============================*/
function* getCarInformation(action) {
	Toast.loading('', )
	try {
		const { result, timeout } = yield race({
			result: call(carService.information, action.data),
			timeout: call(delay, 30000)
		})

		Toast.hide()
		if (timeout) {
			Toast.info(ChMessage.FETCH_FAILED)
			return;
		} else {
			if (result.code == '1000') {

			} else {
				Toast.info(result.msg || ChMessage.FETCH_FAILED)
			}
			if (action.callback) {
				action.callback(result)
			}
		}
	} catch (error) {
		Toast.hide()
		if (action.callback) {
			action.callback(error)
		}
	}
}
function* watchGetCarInformation() {
	yield takeLatest(GET_CAR_INFORMATION, getCarInformation)
}

/*==========================获取车辆列表 1.3============================*/
function* queryCarsAsync(action) {
	let callbackData = {}; //回调数据

	try {
		const { result, timeout } = yield race({
			result: call(carService.cars, action.data),
			timeout: call(delay, 30000)
		})

		if (timeout) {
			//超时
		} else {
			callbackData = result;

			if (result.code == '1000') {

				const car = new schema.Entity('car', {}, { idAttribute: 'carId' })
				const cars = new schema.Array(car)
				const normalizedData = normalize(result.data, cars)

				if (normalizedData.result.length > 0) {
					yield put({
						type: ADD_CAR_LIST,
						data: {
							data: normalizedData.entities.car,
							result: normalizedData.result
						}
					})

					for (let i = 0; i < normalizedData.result.length; i++) {
						//遍历查询车辆的违章信息
						yield put({
							type: QUERY_VIOLATION_ASYNC,
							data: {
								carId: normalizedData.result[i],
								isGet: sessionStorage.getItem('isGet') == '1' ? 0 : 1
							}
						})
					}


					sessionStorage.setItem('isGet', '1') //请求成功后 不再重复强制刷新
				} else {
					yield put({ type: DELETE_CAR_LIST }) //清空车辆列表
				}
			} else {
				if (result.code != '4222') {
					//非token过期
					yield put({ type: DELETE_CAR_LIST }) //清空车辆列表
				}
			}
		}
	} catch (error) {
		callbackData = error;
	}

	//回调
	if (action.callback) {
		action.callback(callbackData);
	}
}
function* watchQueryCarsAsync() {
	yield takeLatest(QUERY_CARS_ASYNC, queryCarsAsync)
}

/*==========================获取车辆违章信息 1.3============================*/
function* queryViolationAsync(action) {
	let callbackData = {}; //回调数据

	try {
		const { result, timeout } = yield race({
			result: call(carService.violation, action.data),
			timeout: call(delay, 30000)
		})

		if (timeout) {
			//超时
		} else {
			callbackData = result;

			if (result.code == '1000') {
				//更新车辆信息
				let { count, fine, degree, msg } = result.data;
				yield put({
					type: UPDATE_CAR,
					data: {
						carId: action.data.carId,
						totalDegree: degree,
						totalFine: fine,
						totalNumber: count,
						msg: msg
					}
				})

			} else {
				//获取违章信息失败
			}
		}
	} catch (error) {
		callbackData = error;
	}

	//回调
	if (action.callback) {
		action.callback(callbackData);
	}
}
function* watchQueryViolationAsync() {
	while (true) {
		let action = yield take(QUERY_VIOLATION_ASYNC)
		yield fork(queryViolationAsync, action)
	}
}

//-------------------------------------------------------------------------

export function* watchCarsFetch() {
	yield [
		fork(watchGetCarInformation),
		fork(watchCarConditionList),
		fork(watchQueryCarsAsync),
		fork(watchQueryViolationAsync),
	]
}