/**
 * 首页相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    QUERY_BANNERS_ASYNC,
    QUERY_ICONS_ASYNC,
    ADD_BANNERS,
    ADD_ICONS,
    ADD_NEWS,
    GET_INDEX_DATAS,
    QUERY_MESSAGES_ASYNC,
    MESSAGES_IGNORE_ASYNC,
    ADD_MESSAGES,
    FETCH_FAILED,
    QUERY_POST_INFO_ASYNC,
    QUERY_HOT_NEWS_VIEW_COUNT_ASYNC,
    POST_PRAISE_ASYNC
} from '../actions/actionsTypes.js'

//antd
import { Toast } from 'antd-mobile'

//server
import indexService from '../services/indexService'

//常用工具类
import { ChMessage } from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库


//获取banner
function* queryBanners(action) {
    try {
        const { result, timeout } = yield race({
            result: call(indexService.banners, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            console.error("获取banner超时")
        } else {
            if (result.code == '1000') {
                yield put({ type: ADD_BANNERS, data: result.data.adList })
            }
        }

    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }
}

function* watchQueryBanners() {
    yield takeLatest(QUERY_BANNERS_ASYNC, queryBanners)
}

//获取快捷入口ICONS
function* queryIcons(action) {
    try {
        //存在缓存
        if (sessionStorage.getItem('H5Index_indexService_icons')) {
            let res = JSON.parse(sessionStorage.getItem('H5Index_indexService_icons'));
            yield put({ type: ADD_ICONS, data: res.data })
            return;
        }

        const { result, timeout } = yield race({
            result: call(indexService.icons, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            if (result.code == '1000') {
                sessionStorage.setItem("H5Index_indexService_icons", JSON.stringify(result)); //标识已经请求过数据
                yield put({ type: ADD_ICONS, data: result.data })
            }
        }


    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }
}

function* watchQueryIcons() {
    yield takeLatest(QUERY_ICONS_ASYNC, queryIcons)
}

//首页数据的统一请求入口
function* indexDatas() {

    yield put({ type: QUERY_ICONS_ASYNC })

}

function* watchIndexDatas() {
    yield takeLatest(GET_INDEX_DATAS, indexDatas)
}

//获取信息列表
function* queryMessagesAsync(action) {
    let callbackData = { code: 'fail' };
    try {
        const { result, timeout } = yield race({
            result: call(indexService.messages, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
            if (result.code == '1000') {
                yield put({ type: ADD_MESSAGES, data: result.data })
            }
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchQueryMessagesAsync() {
    yield takeLatest(QUERY_MESSAGES_ASYNC, queryMessagesAsync)
}

//忽略指定消息
function* messagesIgnoreAsync(action) {
    let callbackData = { code: 'fail' };
    try {
        const { result, timeout } = yield race({
            result: call(indexService.messagesIgnore, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchMessagesIgnoreAsync() {
    yield takeLatest(MESSAGES_IGNORE_ASYNC, messagesIgnoreAsync)
}

//获取帖子评论数、点赞数和是否已经点赞
function* queryPostInfoAsync(action) {
    let callbackData = { code: 'fail' };
    try {
        const { result, timeout } = yield race({
            result: call(indexService.getPostInfo, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchQueryPostInfoAsync() {
    while (true) {
        let action = yield take(QUERY_POST_INFO_ASYNC)
        yield fork(queryPostInfoAsync, action)
    }
}

//帖子点赞
function* postPraiseAsync(action) {
    let callbackData = { code: 'fail' };
    try {
        const { result, timeout } = yield race({
            result: call(indexService.postPraise, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchPostPraiseAsync() {
    while (true) {
        let action = yield take(POST_PRAISE_ASYNC)
        yield fork(postPraiseAsync, action)
    }
}

//获取热门资讯阅读数
function* queryHotNewsViewCountAsync(action) {
    let callbackData = { code: 'fail' };
    try {
        const { result, timeout } = yield race({
            result: call(indexService.getHotNewsViewCount, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            Toast.info(ChMessage.FETCH_TIMEOUT);
        } else {
            callbackData = result;
        }
    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData);
    }
}

function* watchQueryHotNewsViewCountAsync() {
    while (true) {
        let action = yield take(QUERY_HOT_NEWS_VIEW_COUNT_ASYNC)
        yield fork(queryHotNewsViewCountAsync, action)
    }
}

export function* watchIndexPageFetch() {
    yield [
        fork(watchQueryBanners),
        fork(watchQueryIcons),
        fork(watchIndexDatas),
        fork(watchQueryMessagesAsync),
        fork(watchMessagesIgnoreAsync),
        fork(watchQueryPostInfoAsync),
        fork(watchPostPraiseAsync),
        fork(watchQueryHotNewsViewCountAsync),
    ]
}