/**
 * 配置
 */
import common from './utils/common'

const userType = sessionStorage.getItem('userType') || common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay'
const isPre = window.location.host.indexOf('pre.cx580.com') > -1; //预发布环境
const production = window.location.host.indexOf('h5index.cx580.com') > -1 || isPre; //是否为生产环境或预发布环境
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;
//年检提醒
const inspectionReminderUrl = production
    ? `https://annualcheck.cx580.com/user/bindInfo?authType=${authType}&userType=${userType}&clientId=CheWu&redirectUrl=https://annualcheck.cx580.com/inspection/index.html?detailUserType=${userType}/%23/remindHome`
    : `http://192.168.1.165:7083/user/bindInfo?authType=${authType}&userType=${userType}&clientId=CheWu&redirectUrl=http://192.168.1.165:7083/inspection/index.html?detailUserType=${userType}/%23/remindHome`;


export default {
    //是否为生产环境
    production: production,

    //接口地址
    baseApiUrl: production
        ? window.location.protocol + "//" + window.location.host + "/"
        : "http://webtest.cx580.com:9026/",

    //单点登录地址 回调地址需自行补全
    authUrl: authUrl,

    //年检提醒URL carId和carNumber需要拼接进来（示例：inspectionReminderUrl+'/粤A12345/carId=123456'）
    inspectionReminderUrl: inspectionReminderUrl,

    //违章项目路径
    violationUrl: production
        ? (isPre
            ? `http://pre.cx580.com:59021` // 预发布
            : `https://daiban.cx580.com` //正式
        ) + `/Violation1.3/index.html`
        : `http://webtest.cx580.com:9021/Violation/index.html`,

    //添加车辆（引导注册）
    addCarUrl: production
        ? (isPre
            ? `http://pre.cx580.com:59021` // 预发布
            : `https://daiban.cx580.com` //正式
        ) + `/AddCar_v1.3/index.html`
        : `http://webtest.cx580.com:9021/AddCar/index.html`,

    //社区项目路径
    commentsUrl: production ? `https://comments.cx580.com/dist/index.html` : `http://testshequ.cx580.com:18066/dist/index.html`,

    //协议URL
    ruleUrl: 'http://mobile.cx580.com:14443/H5/more/rule.html',

    //单点登录回调地址
    authCallbackUrl: window.location.protocol + "//" + window.location.host + window.location.pathname + '?selectedIndex=1',

    //我的订单地址
    myOrderUrl: production ? `https://banli.cx580.com/myorder/orderlist.aspx?userType=${userType}&model=1` : `http://webtest.cx580.com:5555/myorder/orderlist.aspx?userType=${userType}&model=1`,

    //我的优惠券地址
    myCouponUrl: production ? `https://banli.cx580.com/mycoupon/couponlist.aspx?userType=${userType}&model=1` : `http://webtest.cx580.com:5555/mycoupon/couponlist.aspx?userType=${userType}&model=1`,

    //调试的userId
    debugUsers: ['B406E4D73A704585AB64B35E2A7896BA', 'B4C02D709C9E41AAB3F7B40BD073B4B5'], //debug的userId账号
}