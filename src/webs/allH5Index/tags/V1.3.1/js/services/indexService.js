/**
 * 首页相关接口
 */

import apiHelper from './apiHelper';

class IndexService {

    /**
     * 获取banner
     * @param {*object} data
     */
    banners(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}violation/car/list`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
    * 获取快捷入口ICONS
    * @param {*object} data
    */
    icons(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}app/icons`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


    /**
     * 消息列表接口
     * @param {*object} data
     */
    messages(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}app/messages`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


    /**
     * 忽略消息接口
     * @param {*object} data
     */
    messagesIgnore(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}app/messages/ignore`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


    /**
     * 获取帖子点赞数和评论数接口
     * @param {*object} data
     */
    getPostInfo(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}post/posts`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


    /**
     * 帖子点赞
     * @param {*object} data
     */
    postPraise(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}post/praise`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


    /**
     * 获取热门资讯阅读数
     * @param {*object} data
     */
    getHotNewsViewCount(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}new/news`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
}

export default new IndexService()