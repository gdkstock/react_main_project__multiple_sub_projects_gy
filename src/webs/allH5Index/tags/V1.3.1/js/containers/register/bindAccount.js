/**
 * 绑定账号
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//组件
import { Logo } from '../../components/register';
import { Toast } from 'antd-mobile';

//styles
import styles from './index.scss';

//actions
import * as myActions from '../../actions/myActions';

//常用工具类
import common from '../../utils/common';
import { ChMessage } from '../../utils/message.config'
import userHelper from '../../utils/userHelper'

//配置
import config from '../../config'

class BindAccount extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //手机号码 如果存在手机号码 说明用户已经注册
            phoneNumber: this.props.params.phoneNumber || '',

            //登录类型 1=密码的方式 2=注册页面的验证码注册 3=手机已绑定，无密码（使用验证码）
            loginType: 1,

            //显示警告按钮
            showNotice: false,

            //显示删除按钮
            showDelete: false,
        }
    }

    componentWillMount() {
        common.setViewTitle('绑定账号信息');
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * 手机号码输入框发生改变
     */
    phoneNumberChange() {
        let { showNotice, showDelete } = this.state;
        let phoneNumber = this.refs.phoneNumber.value;

        //显示或隐藏 警告按钮
        if (/[^0-9]/.test(phoneNumber)) {
            //显示警告按钮
            if (!showNotice) {
                this.setState({
                    showNotice: true
                })
            }
        } else {
            //显示删除按钮
            if (showNotice) {
                this.setState({
                    showNotice: false
                })
            }
        }

        //显示或隐藏 删除按钮
        if (phoneNumber.length === 0) {
            //隐藏删除按钮
            if (showDelete) {
                this.setState({
                    showDelete: false
                })
            }
        } else {
            //显示删除按钮
            if (!showDelete) {
                this.setState({
                    showDelete: true
                })
            }
        }

        if (phoneNumber.length > 11) {
            this.refs.phoneNumber.value = phoneNumber.substr(0, 11); //最大允许长度为：11
        }
    }

    /**
     * 显示警告信息
     */
    showNoticeMsg() {
        Toast.info("请输入有效的11位手机号码");
    }

    /**
     * 清空输入框
     */
    clearInput() {
        this.refs.phoneNumber.value = '';
    }

    /**
     * 验证码输入框内容发生改变
     */
    phoneCodeChange() {
        let phoneCode = this.refs.phoneCode.value;
        if (phoneCode.length > 6) {
            this.refs.phoneCode.value = phoneCode.substr(0, 6); //最长输入6位
        }
    }

    /**
     * 密码输入框发生变化
     */
    passwordChange() {
        let password = this.refs.password.value;
        if (password.length > 25) {
            this.refs.password.value = password.substr(0, 25); //最长输入15位
        }
    }

    /**
     * 下一步
     */
    next() {
        let { showNotice } = this.state
        let phoneNumber = this.refs.phoneNumber.value;

        if (!phoneNumber) {
            Toast.info('请输入您的手机号码');
        }

        //判断是否含有非法字符
        if (showNotice) {
            this.showNoticeMsg();
        } else {
            //判断手机号码 是否为11位
            if (phoneNumber.length === 11) {
                this.props.myActions.userBindCheckAsync({ phone: phoneNumber }, res => {
                    if (res.code == 1000) {
                        if (res.data.flag) {
                            //存在账号信息
                            this.setState({
                                phoneNumber: phoneNumber //修改手机号码 重新渲染页面
                            })
                        } else {
                            //账号信息不存在则跳转到注册页面
                            window.location.href = common.getRootUrl() + `register/${phoneNumber}`;
                        }
                    } else {
                        Toast.info(res.msg || ChMessage.FETCH_FAILED)
                    }
                })
            } else {
                this.showNoticeMsg();
            }
        }
    }

    /**
     * 绑定信息
     */
    bindAccount() {
        let { phoneNumber, loginType } = this.state;

        let params = {}; //post提交的参数

        if (loginType === 1) {
            //密码登录
            let password = this.refs.password.value;
            if (!password) {
                Toast.info('请输入车行易APP登录密码');
                return;
            }
            if (password.length < 6 || password.length > 25) {
                Toast.info('请输入6~25位字符密码');
                return;
            }

            params = {
                phone: phoneNumber,
                password: md5(password).toLocaleUpperCase(), //需要MD5加密
                type: loginType //1=密码的方式，2=验证码的方式，3=手机已绑定，无密码
            }
        } else {
            //验证码登录
            let captcha = this.refs.phoneCode.value;
            if (!captcha) {
                Toast.info('请输入验证码', 1);
                return;
            }
            if (captcha.length !== 6) {
                Toast.info('请输入6位数验证码');
                return;
            }
            params = {
                phone: phoneNumber,
                captcha,
                type: loginType //1=密码的方式，2=验证码的方式，3=手机已绑定，无密码
            }
        }

        //发送数据请求
        this.props.myActions.userBindAsync(params, res => {
            if (res.code == 1000 && res.data.flag) {
                Toast.info('绑定信息成功');

                common.sendCxytj({
                    eventId: params.loginType === 1 ? 'h5_e_wz_loginbypwd' : 'h5_e_wz_loginbycode'
                })
                setTimeout(() => {
                    //更新userId和token
                    sessionStorage.setItem('userId', res.data.userId);
                    sessionStorage.setItem('token', res.data.token);
                    
                    //页面后退
                    window.history.back();
                    // userHelper.Login(config.authCallbackUrl);
                }, 1000);
            } else {
                Toast.info(res.msg || ChMessage.FETCH_FAILED);
            }
        })

    }

    /**
     * 获取验证码
     */
    getPhoneCode(e) {
        let { phoneCodeBtnFalse, phoneNumber } = this.state
        //可点击
        if (!phoneCodeBtnFalse) {
            e.innerHTML = `60秒后获取`;
            this.setState({
                phoneCodeBtnFalse: true //设为不可点击
            }, () => {
                //发送验证码
                this.sendPhoneCode(phoneNumber);

                //显示重新发送计时器
                let second = 60;
                let SEND = setInterval(() => {
                    second--;
                    //到过60秒时停止
                    if (second == 0) {
                        e.innerHTML = `重新发送`;
                        clearInterval(SEND);
                        this.setState({
                            phoneCodeBtnFalse: false //设为可点击
                        })
                    } else {
                        e.innerHTML = `${second}秒后获取`;
                    }

                }, 1000);
            })
        }
    }

    /**
     * 发送验证码
     */
    sendPhoneCode(phone) {
        this.props.myActions.userSendSmsAsync({ phone }, res => {
            //发送失败
            if (res.code != 1000) {
                Toast.info(res.msg || ChMessage.FETCH_FAILED);
            }
        })
    }

    /**
     * 切换登录方式
     */
    toggleLogin() {
        let { loginType } = this.state;

        this.setState({
            loginType: loginType === 1 ? 3 : 1
        })
    }

    render() {
        let { phoneNumber, loginType, showNotice, showDelete, phoneCodeBtnFalse } = this.state;

        return (
            <div className='box'>
                <div className='whiteBg'></div>
                <Logo />
                {
                    phoneNumber
                        ? //存在手机号码 说明用户已经注册 则让用户输入密码进行绑定
                        <div>
                            <div className={styles.list}>
                                <div className={styles.item}>
                                    <span className={styles.spanInput}>{phoneNumber.substr(0, 3) + ' ' + phoneNumber.substr(3, 4) + ' ' + phoneNumber.substr(7)}</span>
                                    {
                                        loginType === 1 ? ''
                                            : <span className={styles.phoneCodeBtn + (phoneCodeBtnFalse ? ' ' + styles.phoneCodeBtnFalse : '')} onClick={(e) => this.getPhoneCode(e.target)}>获取验证码</span>
                                    }
                                </div>
                                <div className={styles.item}>
                                    {
                                        loginType === 1
                                            ? <input ref='password' key='password' type='password' onChange={() => this.passwordChange()} placeholder='请输入车行易APP登录密码' />
                                            : <input ref='phoneCode' key='phoneCode' type='number' onChange={() => this.phoneCodeChange()} placeholder='请输入验证码' />
                                    }
                                </div>
                            </div>
                            <div className={styles.btnBox}>
                                <div className={styles.btn} onClick={() => this.bindAccount()}>绑定信息</div>
                                <div className={styles.btn2} onClick={() => this.toggleLogin()}>{loginType === 1 ? '手机验证码登录' : '密码登录'}</div>
                            </div>
                        </div>
                        :
                        <div>
                            <div className={styles.list}>
                                <div className={styles.item}>
                                    <input ref='phoneNumber' type='text' onChange={() => this.phoneNumberChange()} placeholder='请输入您的手机号码' />
                                    <i className={showNotice ? styles.notice : 'hide'} onClick={() => this.showNoticeMsg()}></i>
                                    <i className={!showNotice && showDelete ? styles.delete : 'hide'} onClick={() => this.clearInput()}></i>
                                </div>
                            </div>
                            <div className={styles.btnBox}>
                                <div className={styles.btn} onClick={() => this.next()}>下一步</div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    myActions: bindActionCreators(myActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BindAccount);