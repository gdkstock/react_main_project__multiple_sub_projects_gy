import React, { Component } from 'react';

import { Carousel } from 'antd-mobile';

//touch相关变量
let touch = {
    start: {
        x: 0,
        y: 0
    },
    move: {
        x: 0,
        y: 0
    },
    isL_R: true, //左右滑动
    isEnd: false, //滑动结束
    scrollTop: 0,
    transfrom: 'translate3d(0, 0, 0)',
}

class Test extends Component {
    constructor(props) {
        super(props)

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    shouldComponentUpdate(nextProps, nextState) {

    }

    componentWillUpdate(nextProps, nextState) {

    }

    componentDidUpdate(prevProps, prevState) {

    }

    componentWillUnmount() {

    }

    touchStart(e) {
        let touchs = e.targetTouches[0];
        touch.start.x = touchs.clientX;
        touch.start.y = touchs.clientY;
        touch.scrollTop = document.body.scrollTop;
        try {
            touch.transfrom = document.getElementsByClassName('slider-list')[0].style.transform;
        } catch (error) {
            //dom节点不存在
        }

    }

    touchMove(e) {
        //活动过程中 只判断一次
        if (!touch.isEnd) {
            let touchs = e.targetTouches[0];
            touch.move.x = touchs.clientX;
            touch.move.y = touchs.clientY;
            touch.isL_R = Math.abs(touch.move.x - touch.start.x) > Math.abs(touch.move.y - touch.start.y);
        }


        //左右滑动
        if (touch.isL_R) {
            //左右滑动时，禁止上下滑动
            document.body.scrollTop = touch.scrollTop;
            console.log("左右滑动时，禁止上下滑动")
        } else {
            //上下滑动时，禁止左右滑动
            try {
                document.getElementsByClassName('slider-list')[0].style.transform = touch.transfrom;
                console.log("上下滑动时，禁止左右滑动")
            } catch (error) {
                //dom节点不存在

            }
        }

        touch.isEnd = true;
        console.log("手指移动：", touch);
    }

    touchEnd() {
        //左右滑动
        if (touch.isL_R) {
            //左右滑动时，禁止上下滑动
            document.body.scrollTop = touch.scrollTop;
            console.log("左右滑动时，禁止上下滑动")
        } else {
            //上下滑动时，禁止左右滑动
            try {
                document.getElementsByClassName('slider-list')[0].style.transform = touch.transfrom;
                console.log("上下滑动时，禁止左右滑动")
            } catch (error) {
                //dom节点不存在

            }
        }
        touch.isEnd = false;
    }

    render() {
        return (
            <div>
                <Carousel className={"my-carousel "}
                    dots={true}
                    beforeChange={(from, to) => console.log('页面滑动：', from, to)}
                >
                    { //测试左右滑动和上下滑动
                        [1, 2, 3, 4, 5].map((img, i) =>
                            <div
                                onTouchStart={e => this.touchStart(e)}
                                onTouchMove={e => this.touchMove(e)}
                                onTouchEnd={e => this.touchEnd(e)}
                                key={i} className="item" style={{ height: '2000px', background: `rgba(0,0,0,.${img})` }}>
                                {img}
                            </div>
                        )
                    }
                </Carousel>
            </div>
        )
    }
}

export default Test