/*
* 这里定义所有的action类型
* */

/**
 * fetch数据请求相关
 */
export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时


/**
 * 同步请求
 */

//首页相关
export const GET_INDEX_DATAS = 'GET_INDEX_DATAS' //获取首页的数据
export const ADD_CAR_LIST = 'ADD_CAR_LIST' //添加车辆列表
export const DELETE_CAR_LIST = 'DELETE_CAR_LIST' //清空车辆列表
export const UPDATE_CAR = 'UPDATE_CAR' //修改车辆信息
export const ADD_BANNERS = 'ADD_BANNERS' //添加banner
export const ADD_ICONS = 'ADD_ICONS' //添加快捷入口的ICONS
export const ADD_NEWS = 'ADD_NEWS' //添加资讯内容列表
export const ADD_MESSAGES = 'ADD_MESSAGES' //添加消息列表

//我的页面相关
export const CREATE_USER = 'CREATE_USER' //创建用户信息
export const UPDATE_USER_DETAIL = 'UPDATE_USER_DETAIL' //更新用户信息
export const USER_BIND_OR_USER_UN_BIND = 'USER_BIND_OR_USER_UN_BIND' //用户发生绑定或解绑

/**
 * 异步请求
 */

//首页相关
export const QUERY_CAR_LIST_ASYNC = 'QUERY_CAR_LIST_ASYNC' //获取车辆列表
export const QUERY_BANNERS_ASYNC = 'QUERY_BANNERS_ASYNC' //获取banner
export const QUERY_ICONS_ASYNC = 'QUERY_ICONS_ASYNC' //获取快捷入口的ICON
export const QUERY_NEWS_ASYNC = 'QUERY_NEWS_ASYNC' //获取资讯内容列表
export const GET_CAR_INFORMATION = 'GET_CAR_INFORMATION' //根据车牌获取车辆的相关信息
export const GET_CAR_CONDITION_LIST = 'GET_CAR_CONDITION_LIST' //引导注册 - 请求车辆违章约束条件信息
export const QUERY_VIOLATION_ASYNC = 'QUERY_VIOLATION_ASYNC' //获取违章信息
export const QUERY_CARS_ASYNC = 'QUERY_CARS_ASYNC' //获取车辆列表 1.3版本
export const QUERY_MESSAGES_ASYNC = 'QUERY_MESSAGES_ASYNC' //获取消息列表接口
export const MESSAGES_IGNORE_ASYNC = 'MESSAGES_IGNORE_ASYNC' //忽略消息接口
export const QUERY_POST_INFO_ASYNC = 'QUERY_POST_INFO_ASYNC' //查询帖子点赞数、评论数、是否已经点赞
export const QUERY_HOT_NEWS_VIEW_COUNT_ASYNC = 'QUERY_HOT_NEWS_VIEW_COUNT_ASYNC' //获取热门资讯阅读数
export const POST_PRAISE_ASYNC = 'POST_PRAISE_ASYNC' //帖子点赞

//我的页面相关
export const UPDATE_USER_DETAIL_ASYNC = 'UPDATE_USER_DETAIL_ASYNC' //更新用户信息
export const QUERY_USER_DETAIL_ASYNC = 'QUERY_USER_DETAIL_ASYNC' //获取个人信息
export const QUERY_USER_ICONS_ASYNC = 'QUERY_USER_ICONS_ASYNC' //icon列表
export const USER_BIND_ASYNC = 'USER_BIND_ASYNC' //用户绑定
export const USER_UN_BIND_ASYNC = 'USER_UN_BIND_ASYNC' //取消绑定
export const USER_BIND_CHECK_ASYNC = 'USER_BIND_CHECK_ASYNC' //查询用户是否绑定
export const USER_SEND_SMS_ASYNC = 'USER_SEND_SMS_ASYNC' //发送手机验证码
