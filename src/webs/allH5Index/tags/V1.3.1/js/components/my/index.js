
//styles
import styles from './index.scss';

//常用工具类
import common from '../../utils/common';

//配置
import config from '../../config'

/**
 * 用户卡片
 * @param {*Object} props 
 */
export const UserCard = props => {
    let { ifBind, nickName, headImgUrl, vip, telephone, coupons, money, point, onClick, clickWallet, clickCoupon, clickJifen } = props;

    return (
        <div className={styles.userCard}>
            <div className={styles.userBox} onClick={() => onClick && onClick()}>
                <div className={styles.userAvatar}>
                    <img
                        src={headImgUrl ? decodeURIComponent(headImgUrl) : './images/userAvatar.png'}
                        onError={e => e.target.src !== './images/userAvatar.png' ? e.target.src = './images/userAvatar.png' : false}
                    />
                </div>
                <div className={styles.userText}>
                    <h3 className={styles.userName}>{nickName || telephone || <i>&nbsp;</i>}</h3>
                    <div className={styles.vip}>
                        <div className={styles.vipIcon}>
                            <img src={vip == '1' ? './images/vip_yes.png' : './images/vip_no.png'} />
                        </div>
                        <span>尽享更多优惠</span>
                    </div>
                </div>
            </div>
            <div className={styles.userFooter}>
                <ul>
                    <li onClick={() => clickWallet && clickWallet()}>
                        <h3>{money === undefined ? '?' : money}</h3>
                        <p>总资产(元)</p>
                    </li>
                    <li onClick={() => clickCoupon && clickCoupon()}>
                        <h3>{coupons === undefined ? '?' : coupons}</h3>
                        <p>优惠券(张)</p>
                    </li>
                    <li onClick={() => clickJifen && clickJifen()}>
                        <h3>{point === undefined ? '?' : point}</h3>
                        <p>积分</p>
                    </li>
                </ul>
            </div>
        </div>
    )
}


/**
 * 我的订单
 * @param {*Object} props 
 */
export const MyOrder = props => {

    return (
        <div className={styles.order}>
            <div className={styles.orderTitle}>
                <span>我的订单</span>
            </div>
            <div className={styles.orderTypes}>
                <ul>
                    <li onClick={() => common.toUrlAndSendCxytj(config.myOrderUrl + '&sType=0', 'h5_e_wz_my_orderworking')}>
                        <i className={styles.iconProcess}></i>
                        <p>处理中</p>
                    </li>
                    <li onClick={() => common.toUrlAndSendCxytj(config.myOrderUrl + '&sType=1', 'h5_e_wz_my_orderfinished')}>
                        <i className={styles.iconSuccess}></i>
                        <p>已成功</p>
                    </li>
                    <li onClick={() => common.toUrlAndSendCxytj(config.myOrderUrl + '&sType=2', 'h5_e_wz_my_ordercanceled')}>
                        <i className={styles.iconRevoke}></i>
                        <p>已撤销</p>
                    </li>
                </ul>
            </div>
        </div>
    )
}


/**
 * 功能ICON列表
 * @param {*Object} props 
 */
export const IconList = props => {
    let { list } = props;
    return (
        <div className={styles.iconList}>
            <ul>
                {
                    list && list.map((item, i) =>
                        <li key={'icon-' + i} onClick={() => item.iconTargetUrl ? window.location.href = item.iconTargetUrl : false}>
                            <img src={item.iconImgUrl} />
                            <p>{item.iconName}</p>
                            {item.iconSubName && <i>{item.iconSubName}</i>}
                        </li>
                    )
                }
            </ul>
        </div>
    )
}