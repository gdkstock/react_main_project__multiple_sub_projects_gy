function add(x, y) {
    return x + y;
}

var expect = require('chai').expect;

describe('加法函数测试：',function(){
    it('1加1等于2',function(){
        expect(add(1,1)).to.be.equal(2);
    })
})