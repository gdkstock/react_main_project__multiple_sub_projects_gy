function add(x, y) {
    return x + y;
}

var request = require('superagent');
var expect = require('chai').expect;

describe('dir文件夹中的加法函数测试：', function () {
    it('1加1等于2', function () {
        expect(add(1, 1)).to.be.equal(2);
    })
})

describe('异步请求测试：', function () {
    it('异步请求应该返回code=1000', function (done) {
        request
            .post('http://127.0.0.1:9999/test/index')
            .send({ key: 'key->Gvalue' })
            .end(function (err, res) {
                expect(res.body.code).to.be.equal(1000);
                done();
            });
    });
})

describe('timeout.test.js - 超时测试', function () {
    it('测试应该 5000 毫秒后结束', function (done) {
        var x = true;
        var f = function () {
            x = false;
            expect(x).to.be.not.ok;
            done();
        };
        setTimeout(f, 4000);
    });
});