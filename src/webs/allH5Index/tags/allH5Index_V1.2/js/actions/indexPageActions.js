/*
* 车辆及驾照相关action
*/
import {
	GET_INDEX_DATAS,
	QUERY_NEWS_ASYNC
} from './actionsTypes'

//请求首页的所有数据
export const getIndexDatas = () => ({ type: GET_INDEX_DATAS })
export const queryNewsAsync = (data, callback) => ({ type: QUERY_NEWS_ASYNC, data: data, callback: callback })
