/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    QUERY_BANNERS_ASYNC,
    QUERY_ICONS_ASYNC,
    QUERY_NEWS_ASYNC,
    ADD_BANNERS,
    ADD_ICONS,
    ADD_NEWS,
    GET_INDEX_DATAS,
    FETCH_FAILED
} from '../actions/actionsTypes.js'

//antd
import { Toast } from 'antd-mobile'

//server
import indexService from '../services/indexService'


import { normalize, schema } from 'normalizr'; //范式化库


//获取banner
function* queryBanners(action) {
    try {
        const { result, timeout } = yield race({
            result: call(indexService.banners, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            console.error("获取banner超时")
        } else {
            if (result.code == '1000') {
                yield put({ type: ADD_BANNERS, data: result.data.adList })
            }
        }

    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }
}

//获取快捷入口ICONS
function* queryIcons(action) {
    try {
        const { result, timeout } = yield race({
            result: call(indexService.icons, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            if (result.code == '1000') {
                yield put({ type: ADD_ICONS, data: result.data })
            }
        }

    } catch (error) {
        yield put({ type: FETCH_FAILED, error })
    }
}

//获取资讯列表
function* queryNews(action) {
    let callbackData = '' //回调内容
    try {
        const { result, timeout } = yield race({
            result: call(indexService.news, action.data),
            timeout: call(delay, 30000)
        })
        if (timeout) {
            callbackData = {
                msg: 'timeout'
            }
        } else {
            callbackData = result;
            if (result.code == '1000') {
                sessionStorage.setItem('queryNewsAsync_page', action.data.page); //保存当前的页码
                yield put({ type: ADD_NEWS, data: result.data, page: action.data.page })
            }
        }

    } catch (error) {
        callbackData = {
            msg: 'error'
        }
        yield put({ type: FETCH_FAILED, error })
    }

    if (action.callback) {
        action.callback(callbackData) //回调
    }

}

//首页数据的统一请求入口
function* indexDatas() {
    yield put({
        type: QUERY_BANNERS_ASYNC,
        data: {
            positionCode: 'WZ_CAR_H5_SY'
        }
    })
    yield put({ type: QUERY_ICONS_ASYNC })
    yield put({
        type: QUERY_NEWS_ASYNC,
        data: {
            page: 1,
            size: 10
        }
    })
}

function* watchQueryBanners() {
    yield takeLatest(QUERY_BANNERS_ASYNC, queryBanners)
}

function* watchQueryIcons() {
    yield takeLatest(QUERY_ICONS_ASYNC, queryIcons)
}

function* watchQueryNews() {
    yield takeLatest(QUERY_NEWS_ASYNC, queryNews)
}

function* watchIndexDatas() {
    yield takeLatest(GET_INDEX_DATAS, indexDatas)
}

export function* watchIndexPageFetch() {
    yield [
        fork(watchQueryBanners),
        fork(watchQueryIcons),
        fork(watchQueryNews),
        fork(watchIndexDatas),
    ]
}