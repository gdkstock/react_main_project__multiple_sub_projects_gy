/**
 * 车辆相关
 */
import {
  takeEvery,
  delay,
  takeLatest,
  buffers,
  channel,
  eventChannel,
  END
} from 'redux-saga'
import {
  race,
  put,
  call,
  take,
  fork,
  select,
  actionChannel,
  cancel,
  cancelled
} from 'redux-saga/effects'

import {
  QUERY_CAR_LIST_ASYNC,
  ADD_CAR_LIST,
  GET_CAR_INFORMATION,
  GET_CAR_CONDITION_LIST,
} from '../actions/actionsTypes.js'

//antd
import { Toast } from 'antd-mobile'

//server
import carService from '../services/carService'


import { normalize, schema } from 'normalizr'; //范式化库
import { ChMessage } from '../utils/message.config'


//获取车辆列表
function* queryCarList(action) {
  Toast.loading('', 0)
  try {
    const { result, timeout } = yield race({
      result: call(carService.carList, action.data),
      timeout: call(delay, 30000)
    })
    Toast.hide()
    if (timeout) {
      window.networkError('./images/networkError-icon.png');
    } else {
      if (result.code == '1000') {
        sessionStorage.setItem('isGet', '1') //请求成功后 不再重复强制刷新

        const car = new schema.Entity('car', {}, { idAttribute: 'carId' })
        const cars = new schema.Array(car)
        const normalizedData = normalize(result.data.carViolationInfoList, cars)

        if (normalizedData.result.length > 0) {
          yield put({
            type: ADD_CAR_LIST,
            data: {
              data: normalizedData.entities.car,
              result: normalizedData.result
            }
          })
        }

      } else {
        // if (result.msg) {
        //     Toast.info(result.msg)
        // }
        yield put({
          type: ADD_CAR_LIST,
          data: {
            data: {},
            result: []
          }
        })
      }
    }

  } catch (error) {
    Toast.hide()
    yield put({ type: FETCH_FAILED, error })
  }
}


function* watchQueryCarList() {
  yield takeLatest(QUERY_CAR_LIST_ASYNC, queryCarList)
}


/*==========================获取违章查询条件信息-引导注册============================*/
function* fetchCarConditionList(action) {
  Toast.loading("", 0)
  try {
    let result;
    if (sessionStorage.getItem("carCondition")) {
      result = JSON.parse(sessionStorage.getItem("carCondition"))
    } else {
      const { _result, timeout } = yield race({
        _result: call(carService.carCondition, action.data),
        timeout: call(delay, 30000)
      })
      result = _result

      if (timeout) {
        window.networkError('./images/networkError-icon.png');
        return;
      }
    }
    Toast.hide()
    if (result.code == "1000") {
      sessionStorage.setItem("carCondition", JSON.stringify(result)) //成功的时候 才保存，避免网络错误时，每次都提示网络错误

      //提取城市
      const citi = new schema.Entity('citi', {}, { idAttribute: 'carNumberPrefix' });
      const citis = new schema.Array(citi)
      const province = { cities: citis }
      const provinces = new schema.Array(province)
      let normalizedData = normalize(result.data.carConditionList, provinces)

      //提取省份
      const newProvince = new schema.Entity('province', {}, { idAttribute: 'provincePrefix' })
      const newProvinces = new schema.Array(newProvince)
      const newNormalizedData = normalize(normalizedData.result, newProvinces)

      //   yield put({
      //     type: ADD_CAR_CONDITION_LIST, data: {
      //       data: normalizedData.entities.citi,
      //       result: newNormalizedData.result
      //     }
      //   })

      if (action.callback) {
        action.callback({
          code: '1000',
          data: normalizedData.entities.citi,
          result: newNormalizedData.result
        })
      }
    } else {
      Toast.info(result.msg || ChMessage.FETCH_FAILED, 1);
      if (action.callback) {
        action.callback({
          code: 'failed',
          data: {},
          result: []
        })
      }
    }

  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
    if (action.callback) {
      action.callback(error)
    }
  }

}

function* watchCarConditionList() {
  yield takeLatest(GET_CAR_CONDITION_LIST, fetchCarConditionList)
}

/*==========================根据车牌号码获取相关信息============================*/
function* getCarInformation(action) {
  Toast.loading('', )
  try {
    const { result, timeout } = yield race({
      result: call(carService.information, action.data),
      timeout: call(delay, 30000)
    })

    Toast.hide()
    if (timeout) {
      Toast.info("系统繁忙，请稍后再试")
      return;
    } else {
      if (result.code == '1000') {

      } else {
        Toast.info(result.msg || "系统繁忙，请稍后再试")
      }
      if (action.callback) {
        action.callback(result)
      }
    }
  } catch (error) {
    Toast.hide()
    if (action.callback) {
      action.callback(error)
    }
  }
}
function* watchGetCarInformation() {
  yield takeLatest(GET_CAR_INFORMATION, getCarInformation)
}


export function* watchCarsFetch() {
  yield [
    fork(watchQueryCarList),
    fork(watchGetCarInformation),
    fork(watchCarConditionList),
  ]
}