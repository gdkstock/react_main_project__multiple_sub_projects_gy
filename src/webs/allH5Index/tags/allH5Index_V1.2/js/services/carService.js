/**
 * 车辆相关接口
 */

import apiHelper from './apiHelper';

class CarService {

    /**
     * 获取车辆列表
     * @param {*object} data
     */
    carList(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}app/violations`,
          data: {
            method: 'post',
            body: data
          }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 根据车牌号码获取相关信息
     * @param {*object} data
     * {
     * carNumber:''//车牌号码
     * }
     */
    information(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/information`,
          data: {
            method: 'post',
            body: data
          }
        };   
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取查询违章条件列表
     * @param {*object} data
     */
    carCondition(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/getCarConditionList`,
          data: {
            method: 'get',
            body: data
          }
        };  
        return apiHelper.fetch(requestParam);
    }

}

export default new CarService()