
//styles
import styles from './index.scss'
import { Icon } from 'antd-mobile'


/**
 * 默认消息流卡片
 * @param {*object} props 
 */
export const DefaultCard = props => {

    //模拟数据
    let _props = {

        //省略号模拟数据
        ellipsisProps: {
            id: 1,
            list: [
                {
                    id: 1,
                    click: id => console.log("点击了：", id),
                    title: '忽略此消息1'
                },
                {
                    id: 2,
                    click: id => console.log("点击了：", id),
                    title: '忽略此消息2'
                }
            ]
        },

        //头部模拟数据
        cardHeaderProps: {
            icon: './images/icon-gangaoqianzhu.png',
            title: '违章提醒',
            time: '2017-8-14 11:33:20'
        },

        //内容模拟数据
        content: '您的爱车有6条违章记录。车牌号码：粤A847为避免影响用车或产生滞纳金，请及时处理处理啊啊啊啊啊啊'
    }

    props = Object.assign({}, _props, props); //合并模拟数据

    return (
        <div className={styles.box}>
            <Ellipsis {...props.ellipsisProps} />
            <CardHeader {...props.cardHeaderProps} />
            <CardContent {...props} />
            <div className={styles.btn}>查看更多</div>
        </div>
    )
}

/**
 * 热门资讯消息流卡片
 * @param {*object} props 
 */
export const HotNewsCard = props => {

    //模拟数据
    let _props = {

        //省略号模拟数据
        ellipsisProps: {
            id: 2,
            list: [
                {
                    id: 3,
                    click: id => console.log("点击了：", id),
                    title: '忽略此消息1'
                },
                {
                    id: 4,
                    click: id => console.log("点击了：", id),
                    title: '忽略此消息2'
                }
            ]
        },

        //头部模拟数据
        cardHeaderProps: {
            icon: './images/icon-tucao.png',
            title: '热门资讯',
            time: '2017-8-14 11:57:44'
        },

        //内容模拟数据
        content: '您的爱车有6条违章记录。车牌号码：粤A847为避免影响用车或产生滞纳金，请及时处理处理啊啊啊啊啊啊'
    }

    props = Object.assign({}, _props, props); //合并模拟数据

    return (
        <div className={styles.box}>
            <Ellipsis {...props.ellipsisProps} />
            <CardHeader {...props.cardHeaderProps} />
            <CardContent {...props} />
            <div className={styles.newsImg}>
                <img className='img-cover' src='http://info.cx580.com/uploadfile/2017/0307/20170307092652769.jpg' />
            </div>
            <HotNewsFooter />
        </div>
    )
}

/**
 * 社区消息流卡片
 * @param {*object} props 
 */
export const CommunityCard = props => {

    //模拟数据
    let _props = {

        //省略号模拟数据
        ellipsisProps: {
            id: 3,
            list: [
                {
                    id: 3,
                    click: id => console.log("点击了：", id),
                    title: '忽略此消息1'
                },
                {
                    id: 4,
                    click: id => console.log("点击了：", id),
                    title: '忽略此消息2'
                }
            ]
        },

        //头部模拟数据
        cardHeaderProps: {
            icon: './images/icon-tucao.png',
            title: '社区',
            time: '2017-8-14 11:57:44',
        },

        //用户名称
        userName: '你管我叫啥',

        //内容模拟数据
        content: '您的爱车有6条违章记录。车牌号码：粤A847为避免影响用车或产生滞纳金，请及时处理处理啊啊啊啊啊啊'
    }

    props = Object.assign({}, _props, props); //合并模拟数据

    return (
        <div className={styles.box}>
            <Ellipsis {...props.ellipsisProps} />
            <CardHeader {...props.cardHeaderProps} />
            <UserName userName={props.userName} />
            <CardContent {...props} />
            <Imgs />
            <CommunityFooter />
        </div>
    )
}

/**
 * 广告图消息流卡片
 * @param {*object} props 
 */
export const AdCard = props => {
    let { img } = props;
    return (
        <div className={styles.ad}>
            <img src={img} />
        </div>
    )
}


//--------------------------通用UI start -----------------------------

//头部
const CardHeader = props => {
    let { icon, title, time, onClick } = props;

    return (
        <div className={styles.header} onClick={() => onClick ? onClick() : ''}>
            <div className={styles.headerImg}>
                <img src={icon} />
            </div>
            <div className={styles.headerText}>
                <p>{title}</p>
                <p>{time}</p>
            </div>
        </div>
    )
}

//用户名称
const UserName = props => {
    let { userName } = props;

    return (
        <div className={styles.userNameBox}><i className={styles.userName}>{userName || '匿名易友' + parseInt(Math.random() * 1000000)}</i></div>
    )
}

//内容 超过两行则显示缩略号
const CardContent = props => {

    let { content, onClick } = props;
    return (
        <div className={styles.content} onClick={() => onClick ? onClick() : ''}>
            <p className='text-overflow-2'>{content}</p>
        </div>
    )
}

//省略号
const Ellipsis = props => {

    return (
        <div className={styles.ellipsis}>
            <Icon type='ellipsis' onClick={() => showFList(props)} />
            <div
                id={'ellipsis-f-list-' + props.id}
                className={styles.fList}
                onClick={() => hideFList(props)}
            >
                {
                    props.list.map((item, i) =>
                        <div
                            className={styles.fItem}
                            onClick={() => clickItem(item)}
                        >
                            <img src='' />
                            <h3>{item.title}</h3>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

//热门资讯尾部
const HotNewsFooter = props => {

    return (
        <div className={styles.newsFooter}>
            <span className={styles.newsTime}>车行易 30分钟前</span>
            <i className={styles.newsViews}>13245</i>
        </div>
    )
}

//社区尾部
const CommunityFooter = props => {

    let { clickComments, clickSupports } = props;

    return (
        <div className={styles.communityFooter}>
            <div>
                <span
                    className={styles.communitySupports}
                    onClick={() => clickSupports ? clickSupports(props) : false}
                >12</span>
            </div>
            <div>
                <i
                    className={styles.communityComments}
                    onClick={() => clickComments ? clickComments(props) : false}
                >13245</i>
            </div>
        </div>
    )
}

//图片列表
const Imgs = props => {

    return (
        <div className={styles.imgs}>
            {
                [1, 2, 3, 4, 5, 6].map((item, i) =>
                    <img key={'img-' + i} src='' className='img-cover' />
                )
            }
        </div>
    )
}

//--------------------------通用UI end -----------------------------


//--------------------------事件 start -----------------------------

//显示浮层列表
const showFList = props => {
    let obj = document.getElementById('ellipsis-f-list-' + props.id);
    if (obj) {
        //存在
        obj.style.display = 'inherit'; //显示
    }
}

//隐藏浮层列表
const hideFList = props => {
    let obj = document.getElementById('ellipsis-f-list-' + props.id);
    if (obj) {
        //存在
        obj.style.display = 'none'; //显示
    }
}

//点击item
const clickItem = props => {
    props.click(props.id);
}

//--------------------------事件 end -----------------------------