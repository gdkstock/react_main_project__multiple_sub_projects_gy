/**
 * 资讯内容相关
 */
import Style from './index.scss'

const NewsList = props => {

    return (
        <div className={Style.newsList}>
            {
                props.data.map((item, i) => (
                    <News {...item} />
                ))
            }
        </div>
    )
}

const News = props => {
    let { title, imgUrl, newsDate, source, viewNum, url } = props
    return (
        <div className={Style.newsBox} onClick={() => toUrl(url)}>
            <div className={Style.newsImg}>
                <img src={imgUrl} />
            </div>
            <div className={Style.newsText}>
                <h3>{props.title}</h3>
                <p>
                    <span className={Style.footerText}>{source} {newsDate}</span>
                    <span className={Style.viewNum}>{viewNum}</span>
                </p>
            </div>
        </div>
    )
}

const toUrl = url => {
    if (url) {
        let search = 'userId=' + sessionStorage.getItem('userId') + '&token=' + sessionStorage.getItem('token')
        search = url.indexOf('?') > -1 ? '&' + search : '?' + search
        window.location.href = url + search
    }
}
export default NewsList