/**
 * 车辆相关：车辆列表；车辆卡片
 */
import { Carousel } from 'antd-mobile'
import Style from './index.scss'

//常用工具类
import config from '../../config'
import common from '../../utils/common'

/**
 * 车辆列表
 * @param {*Object} props 
 */
const CarList = props => {
    let cars = props.data;

    if (cars.length < 3 && cars.indexOf('addCar') === -1) {
        cars.push('addCar'); //添加车辆的标识符
    }

    if (cars.length === 1) {
        //没有车辆
        return (
            <div className={Style.carListBox}>
                {/*车辆列表 start*/}
                <div style={{ width: '6.5rem', margin: '0 auto' }}>
                    <AddCar type='0' addCar={() => props.addCar()} />
                </div>
                {/*车辆列表 end*/}
            </div>
        )
    }

    return (
        <div className={Style.carListBox}>
            {/*车辆列表 start*/}
            <div style={{ width: '6.5rem', margin: '0 auto' }}>
                <Carousel
                    className="carListCarousel"
                    autoplay={false}
                    selectedIndex={0}
                    dots={false}
                >
                    {cars.map((item, i) => (
                        <div key={item} className={Style.cardBox}>
                            {item == 'addCar'
                                ? <AddCar type='1' addCar={() => props.addCar()} />
                                : <Car {...item} />
                            }
                        </div>
                    ))}
                </Carousel>
            </div>
            {/*车辆列表 end*/}
        </div>
    )
}

/**
 * 单个车辆卡片
 * @param {*Object} props 
 */
const Car = props => {
    let { carId, carNumber, msg, brandUrl, untreatedIdList, untreatedNum, totalFine, totalDegree } = props
    if (msg) {
        //信息有误
        untreatedNum = '--'
        totalFine = '--'
        totalDegree = '--'
    }

    let isNew = true //new标识符
    if (untreatedIdList) {
        isNew = localStorage.getItem('readViolationIds-' + carId) !== untreatedIdList.toString(); //已读列表不等于待处理列表
    } else {
        isNew = false
    }

    return (
        <div>
            <div className={Style.cardTop} onClick={() => {
                if (untreatedIdList && untreatedIdList.length > 0) {
                    localStorage.setItem('readViolationIds-' + carId, untreatedIdList.toString())
                }
                toCarInfo(carId, carNumber)
            }}>
                <div className='absolute-vertical-center' style={{ width: '5.8rem' }}>
                    <div className={Style.carInfo}>
                        <div className={Style.logo}>
                            {
                                brandUrl
                                    ? <img src={brandUrl} onError={(e) => e.target.parentNode.innerHTML = '<div></div>'} />
                                    : <div></div>
                            }

                        </div>
                        <div className={Style.carInfoText}>
                            <span className={Style.carNumber}>{carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}</span>
                        </div>
                    </div>

                    <div className={Style.violation}>
                        {
                            msg
                                ? <div className={Style.top}>
                                    <img className={Style.errorImg} src='./images/car_error.png' />
                                </div>
                                : <div className={Style.top}>
                                    <span className={Style.topText}>未处理违章</span>
                                    <i className={Style.topNum}>{untreatedNum || '0'}</i>
                                    <i className={isNew ? Style.newIcon : 'hide'}></i>
                                </div>
                        }

                        <p className={Style.violationInfo}>
                            扣<span>{totalDegree || '0'}</span>分 罚款<span>{totalFine || '0'}</span>元
                        </p>
                    </div>
                </div>
            </div>
            <div className={Style.carFooter}>
                <ul className={Style.footerBtn}>
                    <li onClick={() => toViolation(carId, carNumber)}>办违章</li><li onClick={() => toInspection(carId, carNumber)}>年检</li>
                </ul>
            </div>
        </div>
    )
}

/**
 * 添加车辆卡片
 * @param {*Object} props 
 * {
 * type: '0' //0:无车辆信息 1:有车辆信息
 * }
 */
const AddCar = props => {
    let imgSrc = './images/'
    let className = Style.addCar
    if (props.type == '0') {
        imgSrc += 'btn_addcar.png'
        className += ' ' + Style.noCar
    } else {
        imgSrc += 'btn_addcar_org.png'
    }

    return (
        <div className={className} onClick={() => props.addCar()}>
            <div className={Style.addCarImg}>
                <img src={imgSrc} />
            </div>
            <h3>添加车辆查违章</h3>
            <p>享受新违章通知、在线办理违章和年审等服务</p>
        </div>
    )
}

/**
 * 跳转到违章业务
 */
const toViolation = (carId, carNumber) => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_weizhang',
    })
    window.location.href = config.violationUrl + '?' + common.getUserInfoString() + `#/violationList/${carId}`
}

/**
 * 跳转到年检业务
 */
const toInspection = (carId, carNumber) => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_nianjian',
    })
    carNumber = encodeURIComponent(carNumber); //URL编码
    // window.location.href = config.inspectionReminderUrl + `/${carNumber}/${carId}`;
    window.open(config.inspectionReminderUrl + `/${carNumber}/${carId}`, "_self");
}

/**
 * 跳转到车辆详情
 */
const toCarInfo = (carId, carNumber) => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_cardetail',
    })
    window.location.href = config.violationUrl + '?' + common.getUserInfoString()+ `#carDetail/${carId}/getDatas`
}

/**
 * 跳转到添加车辆页面
 */
const toAddCar = () => {
    common.sendCxytj({
        eventId: 'h5_e_sycarpage_addcar',
    })
    window.location.href = config.addCarUrl + '?' + common.getUserInfoString();
}


export default CarList