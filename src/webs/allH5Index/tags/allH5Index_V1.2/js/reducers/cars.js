/*
	全局状态cars
*/
import {
	ADD_CAR_LIST,
} from '../actions/actionsTypes'

const initState = {
	data: {},
	result: []
}

export default function cars(state = initState, action) {
	let _data = {}, _result = [], actionData = {}
	switch (action.type) {
		case ADD_CAR_LIST:
			if (action.data.result && action.data.result.length > 0) {
				return {
					data: action.data.data,
					result: action.data.result
				}
			}
			return {
				data: {},
				result: []
			}
		default:
			return state;
	}
}


