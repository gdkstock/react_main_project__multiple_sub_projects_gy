import React, { Component } from 'react';

//组件
import LoadingMore from 'app/components/common/LoadingMore';
import { DefaultCard, HotNewsCard, CommunityCard ,AdCard} from '../../components/msgCard';
import { Icon } from 'antd-mobile';
import IconList from '../../components/iconList'

//styles
import styles from './index.scss';

class HomeV1_3 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //上滑加载更多
            loadingMoreProps: {
                showMsg: true, //显示文字提醒
                msgType: 0, // 对应msg字段的下标
                msg: [
                    '点击加载更多',
                    <span className={styles.iconLoading}><Icon type='loading' />加载更多...</span>,
                    'END'
                ], //文字提醒
                height: 100, //触发加载数据的高度 单位px
                loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
                line: true, //END时,是否添加贯穿文本的横线
            },

            //消息流列表
            list: [],
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.setState({
            list: [1]
        })
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * 加载更多
     */
    loadingMore() {
        let { loadingMoreProps } = this.state;
        console.log("到底了");

        //改变加载文案
        this.setState({
            loadingMoreProps: Object.assign({}, loadingMoreProps, {
                msgType: 1, // 对应msg字段的下标
            })
        });

        //模拟加载完毕
        setTimeout(() => {
            this.setState({
                loadingMoreProps: Object.assign({}, loadingMoreProps, {
                    msgType: 0, // 对应msg字段的下标
                }),
                list: this.state.list.concat([1]) //模拟新增数据
            });
        }, 500);
    }

    render() {

        let { loadingMoreProps, list } = this.state;

        return (
            <div>
                <LoadingMore {...loadingMoreProps}>
                    <div style={{ height: '100px' }}></div>
                    {/* 消息流 start */}
                    {
                        list.map((item, i) =>
                            <div key={i}>
                                <DefaultCard />
                                <div style={{ height: '.24rem' }}></div>
                                <HotNewsCard />
                                <div style={{ height: '.24rem' }}></div>
                                <CommunityCard />
                                <div style={{ height: '.24rem' }}></div>
                                <AdCard img='http://info.cx580.com/uploadfile/2017/0307/20170307092652769.jpg'/>
                                <div style={{ height: '.24rem' }}></div>
                            </div>)
                    }
                    {/* 消息流 end */}
                </LoadingMore>
            </div>
        );
    }
}


export default HomeV1_3;