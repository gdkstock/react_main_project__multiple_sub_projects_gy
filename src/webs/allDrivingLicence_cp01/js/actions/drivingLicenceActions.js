/*
* 车辆及驾照相关action
*/
import {
    QUERY_DRIVINGLICENCE,
    QUERY_DRIVINGLICENCE_LIST,
    QUERY_ADD_DRIVINGLICENCE,
    QUERY_UPDATE_DRIVINGLICENCE,
    QUERY_DELETE_DRIVINGLICENCE,
} from './actionsTypes'

//请求单个驾照信息
export const queryDrivingLicence = ( param ) => ({
	type:QUERY_DRIVINGLICENCE,
	param
})

//请求驾照列表
export const queryDrivingLicenceList = ( param ,callback) => ({
	type:QUERY_DRIVINGLICENCE_LIST,
	param,
	callback
})

//添加驾照信息
export const queryAddDrivingLicence = ( param ) => ({
	type:QUERY_ADD_DRIVINGLICENCE,
	param
})

//修改驾照信息
export const queryUpdateDrivingLicence = ( param ) => ({
	type:QUERY_UPDATE_DRIVINGLICENCE,
	param
})

//删除驾照信息
export const queryDeleteDrivingLicence = ( param ) => ({
	type:QUERY_DELETE_DRIVINGLICENCE,
	param
})


