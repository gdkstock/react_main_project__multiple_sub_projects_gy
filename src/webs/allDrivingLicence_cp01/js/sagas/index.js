// saga 模块化引入
import { fork } from 'redux-saga/effects'

//车辆信息相关
import { watchDrivingLicence } from './drivingLicence'

// 单一进入点，一次启动所有 Saga
export default function* rootSaga() {
  yield [
    fork(watchDrivingLicence),
  ]
}