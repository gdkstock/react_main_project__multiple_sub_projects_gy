/**
 * 车辆相关接口
 */

import apiHelper from './apiHelper';

class OrtherService {

  /**
   * 查询单个驾照信息
   * @param {*object} data
   */
  getInfo(data) {
    let requestParam = {
      url: `https://bl.cx580.com/auth/alqc2017`, // 测试：http://webtest.cx580.com:20006/auth/alqc2017
      data: {
        method: 'post',
        body: data
      }
    };
    
    return apiHelper.fetch(requestParam);
  }
   

}

export default new OrtherService()