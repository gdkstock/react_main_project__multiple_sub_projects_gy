import config from '../config'
import jsApi from './cx580.jsApi';
import common from './common';
import CryptoJS from 'crypto-js' //加密
import OrtherService from '../services/ortherService'

let g_userContainer = undefined;

let g_userId = ''; //CXY_3FBA032214714C19BA4CCA267F86C550
let g_userToken = ''; //0850B086D355F263654698F097C0E1C8

let g_city = "";


/**
 * 用户帮助类
 */
class UserHelper {
    //userContainer = "";
    // userId = "";
    // userToken = "";
    //city = "";


    constructor() {
        this._initialize();
    }

    _initialize() {
        if (common.isCXYApp()) {
            //执行到这里，说明是在 app 中运行
            g_userContainer = "App";
            this._getUserInfoFromApp();
        } else {
            //执行到这里，说明不在 app 中运行
            g_userContainer = "";
        }
    }


    /**
     * 从 app 中获取用户 token
     */
    _getUserInfoFromApp() {
        try {
            jsApi.call({
                "commandId": "",
                "command": "getSymbol",
                "data": {
                    "city": "",
                    "accountId": "",
                    "token": "",
                }
            }, (data) => {
                if(data.data){
                    g_userId = data.data.accountId;
                    g_userToken = data.data.token;
                    g_city = data.data.city;
                    sessionStorage.setItem("userId",data.data.accountId);
                    sessionStorage.setItem("token",data.data.token);
                }
            });
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            // alert("调试信息：调用APP JS SDK出错了 获取APP信息出错了");
            // alert("getSymbol"+error)
        }
    }

    /**
     * app 登陆
     */
    _appLogin(callback) {
        try {
            jsApi.call({
                "commandId": "",
                "command": "login"
            }, (data) => {
                localStorage.setItem("upLoginState", "1"); //用户登录状态发生变化
                if (data.data.accountId) {
                    g_userId = data.data.accountId;
                    this._getUserInfoFromApp();
                    if (callback) {
                        callback();
                    }
                } else {
                    //登录失败 关闭APP视图
                    sessionStorage.setItem("closeWebView",1)
                    jsApi.call({
                        "commandId": "",
                        "command": "close",
                    }, function (data) { });

                }
            });
        } catch (error) {
            // alert("JsApi login错误："+error)
        }
    }

    /**
     * 跳转到单点登录
     */
    toAuthUrl() {
        sessionStorage.clear(); //清空sessionStorage缓存
        let url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.hash; //好像不用编码 编码反而出错？
        url = url.replace('#', '%23'); //替换#号
        window.location.replace(config.authUrl + url); //跳转到单点登录
    }

    /**
     * 获取淘宝用户信息
    */
    getTidaUserInfo(callback){
        //天猫渠道无用户信息，从sdk获取
        let options = {
            sellerNick: "车行易旗舰店"
        };

        let user_from = 'CXYTMALL';
        try{
            Tida && Tida.ready({
                sellerNick: "车行易旗舰店", // 商家名称
                shopId: 558702026, // 店铺ID 从url中取 可选
                console: 1
            }, () => {
                Tida.mixNick(options, (data) => {
                    if (data.mixnick) {
                        let text = `${data.mixnick + user_from}6D54AE366CA44A22AB1604B067594A3A`;
                        let sign = CryptoJS.MD5(text).toString();
                        let params = {
                            user_id: data.mixnick,
                            user_from: user_from,
                            sign: sign
                        };
                        OrtherService.getInfo(params).then((result)=>{
                            if(result.code==0){
                                let { userId , token , userType } = result.data
                                sessionStorage.setItem("userId",userId);
                                sessionStorage.setItem("token",token);
                                sessionStorage.setItem("userType",userType);
                                sessionStorage.setItem("authType",userType);
                                window.location.reload();
                            } 
                        });
                    }
                })
            });
        }catch(e){
            
        }

    }

    /**
     * 获取 userId 和 token
     */
    getUserIdAndToken() {
        if (g_userContainer == "App") {
            common.ready(()=>this._getUserInfoFromApp())
            // this._getUserInfoFromApp();

            return {
                userId: sessionStorage.getItem('userId'),
                token: sessionStorage.getItem('token'),
                city: g_city,
                userType: 'App',
                authType: 'App'
            }
        } else {
            //非APP
            return {
                userId: sessionStorage.getItem('userId'),
                token: sessionStorage.getItem('token'),
                userType: sessionStorage.getItem('userType'),
                authType: sessionStorage.getItem('authType')
            }
        }

    }

    /**
     * 登陆
     * @param callback function 登陆成功之后的回调
     */
    Login(callback) {
        //接天猫我的爱车
        let _channel = getKeyValue("_channel");
        let user_id = getKeyValue("user_id");
        let user_from = getKeyValue("user_from");
        let sign = getKeyValue("sign");
        if(user_id && user_from && sign){
            let params={
                user_id,
                user_from,
                sign,
            }
            OrtherService.getInfo(params).then((result)=>{
                if(result.code==0){
                    let { userId , token , userType } = result.data
                    sessionStorage.setItem("userId",userId);
                    sessionStorage.setItem("token",token);
                    sessionStorage.setItem("userType",userType);
                    sessionStorage.setItem("authType",userType);
                    window.location.reload();
                }          
            });
            return
        }
        //接天猫旗舰店
        if(_channel && _channel.toLowerCase()=="tidashop"){
            this.getTidaUserInfo(()=>{

            })
            return
        }

        if (g_userContainer == "App") {
            common.ready(()=>{this._appLogin(callback)})
            // this._appLogin(callback)
        } else {
            // alert("1111")
            this.toAuthUrl() //跳转到单点登录
        }
    }

};

// 实例化后再导出
export default new UserHelper()