/**
 * 登录授权
 */

import common from './common'
import config from '../config'
import CryptoJS from 'crypto-js' //加密
import OrtherService from '../services/ortherService'
import { Toast } from 'antd-mobile'

class Auth {

  /**
   * 构造函数
   * @param {*object} config 配置信息
   */
  constructor() {
    this.config = config;
    this.userType = common.getJsApiUserType();
    this.Login();
  }

  /**
   * 获取key的值
   */
  getKeyValue(key) {
    if (!key) {
      return key;
    }
    var keyName = key; //保存keyName
    var search = window.location.href; //由于hash值的原因 这里不用window.location.search用window.locationhref
    key = new RegExp('[\?|&]' + key + '=([^&]+)', 'g');
    key = key.exec(search);
    key = key ? key[1] : "";
    sessionStorage.setItem(keyName, key); //key
    return key.split("#")[0] //避免返回的数据带有hash值
  }

  /**
     * 获取淘宝用户信息
    */
  getTidaUserInfo(callback) {
    //天猫渠道无用户信息，从sdk获取
    let options = {
      sellerNick: "车行易旗舰店"
    };

    let user_from = 'CXYTMALL';
    try {
      Tida && Tida.ready({
        sellerNick: "车行易旗舰店", // 商家名称
        shopId: 558702026, // 店铺ID 从url中取 可选
        console: 1
      }, () => {
        Tida.mixNick(options, (data) => {
          if (data.mixnick) {
            let text = `${data.mixnick + user_from}6D54AE366CA44A22AB1604B067594A3A`;
            let sign = CryptoJS.MD5(text).toString();
            let params = {
              user_id: data.mixnick,
              user_from: user_from,
              sign: sign
            };
            Toast.loading("");
            OrtherService.getInfo(params).then((result) => {
              if (result.code == 0) {
                let { userId, token, userType } = result.data
                sessionStorage.setItem("userId", userId);
                sessionStorage.setItem("token", token);
                sessionStorage.setItem("userType", userType);
                sessionStorage.setItem("authType", userType);
                // 派发用户信息获取完成事件
                let ev = new Event('userInfoIsReady', {
                  bubbles: 'true',
                  cancelable: 'true'
                });
                document.dispatchEvent(ev);
              }
            }).catch((e) => {

            });
          }
        })
      });
    } catch (e) {
    }

  }

  /**
   * 登陆
   * @param callback function 登陆成功之后的回调
   */
  Login(callback) {
    //接天猫我的爱车
    let _channel = this.getKeyValue("_channel");
    let user_id = this.getKeyValue("user_id");
    let user_from = this.getKeyValue("user_from");
    let sign = this.getKeyValue("sign");
    let userId = sessionStorage.getItem("userId");
    let token = sessionStorage.getItem("token");
    let userType = sessionStorage.getItem("userType");
    if (userId && token) {
      return
    } else if (user_id && user_from && sign) {
      let params = {
        user_id,
        user_from,
        sign,
      }
      OrtherService.getInfo(params).then((result) => {
        if (result.code == 0) {
          let { userId, token, userType } = result.data
          sessionStorage.setItem("userId", userId);
          sessionStorage.setItem("token", token);
          sessionStorage.setItem("userType", userType);
          sessionStorage.setItem("authType", userType);
          // window.location.reload();
          // 派发用户信息获取完成事件
          let ev = new Event('userInfoIsReady', {
            bubbles: 'true',
            cancelable: 'true'
          });
          document.dispatchEvent(ev);
        }
      });
      return
    }
    //接天猫旗舰店
    if (_channel && _channel.toLowerCase() == "tidashop") {
      this.getTidaUserInfo(() => { })
      return
    }
  }

};

// 实例化后再导出
export default new Auth();