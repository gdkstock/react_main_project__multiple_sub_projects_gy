/**
 * 配置
 */
const production = window.location.host.indexOf('daiban.cx580.com') > -1; //是否为生产环境

export default {
    production: production, //是否为生产环境
    baseApiUrl: production ? window.location.protocol + "//" + window.location.host + "/" : "http://192.168.1.165:9021/", //接口地址
}