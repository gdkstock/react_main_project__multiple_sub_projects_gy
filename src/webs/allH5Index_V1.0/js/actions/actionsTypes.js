/*
* 这里定义所有的action类型
* */

/**
 * fetch数据请求相关
 */
export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时


/**
 * 首页相关
 */

//同步请求
export const GET_INDEX_DATAS = 'GET_INDEX_DATAS' //获取首页的数据
export const ADD_CAR_LIST = 'ADD_CAR_LIST' //添加车辆列表
export const ADD_BANNERS = 'ADD_BANNERS' //添加banner
export const ADD_ICONS = 'ADD_ICONS' //添加快捷入口的ICONS
export const ADD_NEWS = 'ADD_NEWS' //添加资讯内容列表

//异步请求
export const QUERY_CAR_LIST_ASYNC = 'QUERY_CAR_LIST_ASYNC' //获取车辆列表
export const QUERY_BANNERS_ASYNC = 'QUERY_BANNERS_ASYNC' //获取banner
export const QUERY_ICONS_ASYNC = 'QUERY_ICONS_ASYNC' //获取快捷入口的ICON
export const QUERY_NEWS_ASYNC = 'QUERY_NEWS_ASYNC' //获取资讯内容列表
export const GET_CAR_INFORMATION = 'GET_CAR_INFORMATION' //根据车牌获取车辆的相关信息
export const GET_CAR_CONDITION_LIST = 'GET_CAR_CONDITION_LIST' //引导注册 - 请求车辆违章约束条件信息
