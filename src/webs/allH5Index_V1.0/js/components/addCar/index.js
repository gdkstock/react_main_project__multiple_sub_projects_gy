/**
 * 引导注册 -- 添加车辆
 */

import React, { Component } from 'react';

//组件
import CarPrefixList from '../../../../../js/components/common/carPrefixList'

//style
import styles from './index.scss'

//常用工具列
import common from '../../utils/common'

class AddCar extends Component {
    constructor(props) {
        super(props);

        let { carPrefixList } = this.props
        this.state = {
            carPrefix: carPrefixList && carPrefixList.length > 0 ? carPrefixList[0] : '粤', //默认车牌前缀
            showCarPrefixList: false, //显示车牌前缀列表
        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * 点击按钮
     */
    clickBtn() {
        let carNumber = this.getCarNum()
        this.props.onClick(carNumber); //返回车牌号码给父元素
        common.sendCxytj({
            eventId: 'h5_p_violation_ClickNext'
        })
    }

    /**
     * 点击关闭
     */
    close() {
        common.sendCxytj({
            eventId: 'h5_p_violation_CloseLicencePopups'
        })
        this.props.close()
    }

    /**
     * 获取车牌前缀
     */
    getCarPrefix(name) {
        if (name) {
            this.setState({
                carPrefix: name,
                showCarPrefixList: false
            })
        } else {
            this.setState({
                showCarPrefixList: false
            })
        }
    }

    /**
     * 获取完整的车牌号码
     */
    getCarNum() {
        return this.state.carPrefix + this.refs.carNumber.value.toUpperCase()
    }

    /**
     * 车牌号码 输入框 用户输入事件
     */
    inputOnInput(e) {
        let value = e.target.value;
        if (value.length === 1) {
            //输入车牌
            common.sendCxytj({
                eventId: 'h5_e_violation_InputLicencenumber'
            })
        }
    }

    /**
     * 车牌号码 输入框 失去焦点
     */
    inputOnBlur(e) {
        this.refs.carNumber.value = e.target.value.toUpperCase().substr(0, 7) //最多只能输入七位
    }

    render() {
        let { carPrefix } = this.state
        let { title, btnText, carPrefixList } = this.props
        return (
            <div>
                <div className='absolute-center' style={{ marginTop: '-0.5rem' }}>
                    <div className={styles.whiteBox}>
                        <div className={styles.title}>{title}</div>
                        <div className={styles.carNumber}>
                            <span className={styles.carPre} onClick={() => this.setState({ showCarPrefixList: true })}>{carPrefix}</span>
                            <input
                                type='text'
                                ref='carNumber'
                                placeholder='请输入车牌号码'
                                onInput={e => this.inputOnInput(e)}
                                onBlur={e => this.inputOnBlur(e)}
                                maxLength={7}
                                autoFocus={true}
                            />
                        </div>
                        <div className={styles.btn} onClick={() => this.clickBtn()}>{btnText}</div>
                        <div className={styles.closeBtn} onClick={() => this.close()}></div>
                    </div>
                </div>
                {/*车牌前缀列表 start*/}
                <CarPrefixList list={carPrefixList} getName={(name) => this.getCarPrefix(name)} show={this.state.showCarPrefixList} />
                {/*车牌前缀列表 end*/}
            </div>
        );
    }
}

AddCar.defaultProps = {
    title: '添加车辆',
    btnText: '下一步',
    onClick: () => console.log("点击了添加车辆的按钮"),
    close: () => console.log("点击了关闭按钮"),
    carPrefixList: undefined, //车牌前缀列表 array 不传则使用默认的
};

export default AddCar;