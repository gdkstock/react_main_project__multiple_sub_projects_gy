// saga 模块化引入
import { fork } from 'redux-saga/effects'

//首页相关
import { watchIndexPageFetch } from './indexPage'

//车辆相关
import { watchCarsFetch } from './cars'

// 单一进入点，一次启动所有 Saga
export default function* rootSaga() {
  yield [
    fork(watchCarsFetch),
    fork(watchIndexPageFetch)
    // fork(watchFirstThreeTodosCreation)
  ]
}