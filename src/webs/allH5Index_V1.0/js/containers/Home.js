import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Toast, Carousel } from 'antd-mobile'

//组件
import IconList from '../components/iconList'
import CarList from '../components/carList'
import NewsList from '../components/newsList'
import LoadingMore from 'app/components/common/LoadingMore'
import AddCar from '../components/addCar'

//常用工具类
import common from '../utils/common'
import config from '../config'
import { ChMessage } from '../utils/message.config'

//action
import * as carsActions from '../actions/carsActions'
import * as indexPageActions from '../actions/indexPageActions'


//变量
var queryNewsAsync = true; //可请求资讯列表

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			carList: {
				data: [],
				isOk: false
			},
			loadingMoreProps: {
				showMsg: true, //显示文字提醒
				msgType: 0, // 对应msg字段的下标
				msg: ['点击加载更多', '加载中...', 'END'], //文字提醒
				height: 60, //触发加载数据的高度 单位px
				loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
				line: true, //END时,是否添加贯穿文本的横线
			},
			showAddCar: false, //显示添加车辆蒙层
			carPrefixList: undefined, //车牌前缀列表 不传则使用默认的列表
		}
	}

	componentWillMount() {
		common.setViewTitle('车行易查违章');
	}

	componentDidMount() {
		this.props.carsActions.queryCarListAsync({
			isGet: sessionStorage.getItem('isGet') == '1' ? '0' : '1'
		})
		this.props.indexPageActions.getIndexDatas() //获取首页其他数据
	}

	componentWillReceiveProps(nextProps) {
		let { cars } = nextProps
		this.setState({
			carList: {
				data: cars.result.map(carId => cars.data[carId]),
				isOk: true
			}
		})
	}

	componentWillUnmount() {
		sessionStorage.setItem("prevPageInfo", document.title)
	}

	/**
	 * 加载更多资讯内容
	 */
	loadingMore() {
		console.log("到底了，触发新的请求")

		let { news } = this.props.indexPage
		let { loadingMoreProps } = this.state

		let page = (news.length / 10) + 1 //页码

		if (queryNewsAsync && news.length % 10 === 0) {
			//请求数据
			queryNewsAsync = false;
			this.setState({
				loadingMoreProps: Object.assign({}, loadingMoreProps, {
					msgType: 1, // 对应msg字段的下标
				})
			})
			this.props.indexPageActions.queryNewsAsync({
				page: page,
				size: 10
			}, (result) => {
				queryNewsAsync = true //可请求
				if (result && result.code == '1000') {
					//请求成功
					if (result.data.length !== 10) {
						//已经把所有数据都请求完了
						this.setState({
							loadingMoreProps: Object.assign({}, loadingMoreProps, {
								msgType: 2, // 对应msg字段的下标
								loadingMore: () => false, //到达底部时，触发的事件
							})
						})
						return;
					}
				}
				this.setState({
					loadingMoreProps: Object.assign({}, loadingMoreProps, {
						msgType: 0, // 对应msg字段的下标
					})
				})
			})
		}
	}

	/**
	 * 跳转到指定的URL并且进行埋点统计
	 * @param {*string} url 跳转的目标URL
	 * @param {*string} eventId 埋点ID
	 */
	toUrlAndSendCxytj(url, eventId = '') {
		if (eventId) {
			common.sendCxytj({
				eventId: eventId,
			})
		}
		setTimeout(() => {
			window.location.href = url; //URL跳转
		}, 50) //延迟跳转，避免埋点无法统计
	}

	//添加车辆
	addCar() {
		//点击添加车辆埋点
		common.sendCxytj({
			eventId: "H5Index_AddCar"
		})

		//全渠道都使用引导注册的方式

		//获取车牌前缀
		this.props.carsActions.getCarConditionList({}, result => {
			let { carPrefixList } = this.state
			if (result && result.code == '1000' && result.result.length > 0) {
				carPrefixList = result.result
			}

			this.setState({
				showAddCar: true,
				carPrefixList: carPrefixList
			})
		})
	}

	/**
	 * 检测输入的合法性
	 */
	checkInputs(carNumber) {
		let checkAZ = /[A-Z]/g.exec(carNumber.substr(1, 1)); //检查车牌号码是否以字母开头

		if (carNumber.length < 2) {
			Toast.info("车牌号码不能为空", 1);
		} else if (carNumber.length < 7 || carNumber.length > 8 || !checkAZ) {
			Toast.info("您输入的车牌号码有误", 1);
		} else {
			return true
		}
		return false
	}

	//跳转到添加车辆验证页面
	toAddCarAuthPage(carNumber) {
		if (this.checkInputs(carNumber)) {
			this.props.carsActions.getCarInformation({ carNumber: carNumber }, result => {
				console.log("返回的结果：", result);
				if (result && result.code == '1000') {
					this.hideAddCar();

					let { carId, count, degree, money, tel, flag } = result.data
					switch (flag + '') {
						case '0': //无车辆信息
							if (carNumber.substr(0, 1) == '粤') {
								//粤牌车可以进行身份验证
								this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							} else {
								this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
							}
							break;
						case '1': //存在车辆信息
							if (carNumber.substr(0, 1) != '粤' && count == -1 && !tel) {
								//非粤牌车 && 无违章 && 无手机号码
								this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
							} else {
								this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							}

							break;
						case '2': //车辆已经存在车辆列表中
							this.toViolationListUrl(carId);
							break;
						default:
							this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`);
					}

				} else {
					if (result.code == '4443') {
						//满三辆车
						Toast.info('违章查询最多支持绑定3辆车辆,请前往车辆列表删除后再继续查询', 3, () => this.hideAddCar())
					} else {
						Toast.info(result.msg || ChMessage.FETCH_FAILED)
					}
				}
			})
			// 
			// carNumber = encodeURIComponent(carNumber);
			// window.location.href = `${config.addCarUrl}#authPage/${carNumber}(/:count/:degree/:money)(/:tel)` //跳转到添加车辆的身份验证页面
		}
	}

	//跳转到添加车辆页面
	toAddCarUrl(url) {
		window.location.href = `${config.addCarUrl+ '?' + common.getUserInfoString()}#${url}` //跳转到添加车辆的相关路由页面
	}

	//跳转到违章列表页面
	toViolationListUrl(carId) {
		window.location.href = `${config.violationUrl + '?' + common.getUserInfoString()}#violationList/${carId}`;
	}

	//隐藏添加车辆
	hideAddCar() {
		this.setState({
			showAddCar: false
		})
	}

	render() {
		let { banners, icons, news } = this.props.indexPage
		let { banner, iconList, carList, newsList, loadingMoreProps, showAddCar, carPrefixList } = this.state

		console.log("this.props", this.props)
		return (
			<div className="box">
				{/*banner start*/}
				{
					banners.length > 1
						? <Carousel
							className="bannerCarousel"
							dots={true}
							infinite={true}
							autoplay={true}
							selectedIndex={0}
						>
							{banners.map((item, i) => (
								<div key={item.adId} onClick={() => this.toUrlAndSendCxytj(item.targetUrl, item.umengEvent)}>
									<img
										src={item.imageUrl}
										onLoad={() => {
											window.dispatchEvent(new Event('resize'));
										}}
									/>
								</div>
							))}
						</Carousel>
						: banners.map((item, i) => (
							<div
								style={{ lineHeight: 0 }}
								key={item.adId}
								onClick={() => this.toUrlAndSendCxytj(item.targetUrl, item.umengEvent)}>
								<img
									src={item.imageUrl}
									onLoad={() => {
										window.dispatchEvent(new Event('resize'));
									}}
								/>
							</div>
						))
				}
				{/*banner end*/}
				{/*快捷入口ICON列表 start*/}
				<IconList data={icons} />
				{/*快捷入口ICON列表 end*/}

				<div className='h20'></div>
				{/*车辆列表 OR 添加车辆 start*/}
				<CarList {...carList} addCar={() => this.addCar()} />
				{/*车辆列表 OR 添加车辆 end*/}

				<div className='h20'></div>
				{/*资讯内容 start*/}
				<LoadingMore {...loadingMoreProps}>
					<NewsList data={news} />
				</LoadingMore>
				{/*资讯内容 end*/}

				{/*添加车辆蒙层 start*/}
				{
					showAddCar
						? <div style={{
							position: 'fixed',
							left: 0,
							top: 0,
							right: 0,
							bottom: 0,
							zIndex: 11,
							background: 'rgba(0,0,0,.5)',
						}}>
							<AddCar
								carPrefixList={carPrefixList}
								onClick={(carNumber) => this.toAddCarAuthPage(carNumber)}
								close={() => this.hideAddCar()}
							/>
						</div>
						: ''
				}

				{/*添加车辆蒙层 end*/}
			</div >
		)
	}
}

const mapStateToProps = state => ({
	cars: state.cars,
	indexPage: state.indexPage
})

const mapDispatchToProps = dispatch => ({
	carsActions: bindActionCreators(carsActions, dispatch),
	indexPageActions: bindActionCreators(indexPageActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home)