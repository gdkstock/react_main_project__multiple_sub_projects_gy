import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


//组件
import IndexPage from '../components/indexPage'

//通用工具类
import common from '../utils/common'
import config from '../config'

class Home extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		common.setViewTitle('MM和你一起去看车展');
	}

	componentDidMount() {

		//进入“直播宣传页面”的用户量
		common.sendCxytj({
			eventId: 'h5_HD_zhiboxc_UV'
		})
	}


	render() {

		return (
			<div className='box'>
				<IndexPage />
			</div>
		);
	}
}

const mapStateToProps = state => ({

})

export default connect(
	mapStateToProps
)(Home);