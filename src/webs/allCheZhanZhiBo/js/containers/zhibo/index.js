import React, { Component } from 'react';

//styles
import styles from './index.scss';

//组件
import { Toast, Tabs } from 'antd-mobile';
const TabPane = Tabs.TabPane;
import ZhiboPost, { FooterEdi, NewPostNumber } from '../../components/zhibo';
import SendZhiboPost from '../../components/zhibo/sendZhiboPost';
import LoadingMore from 'app/components/common/LoadingMore'

//servers
import PostService from '../../services/postService'
import userHelper from '../../utils/userHelper'
import { ChMessage } from '../../utils/message.config'
import config from '../../config'

//通用工具类
import common from '../../utils/common';
import { normalize, schema } from 'normalizr'; //范式化库

//组件变量
let timeId = 0; //滚动事件，渲染过快
let lastTime = 0; //上次请求数据的时间 单位：毫秒


class ZhiBo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //每半个小时增加43人
            peopleNumber: parseInt((new Date().getTime() - new Date(2017, 10, 10, 10).getTime()) / 1800000) * 47,

            //是否浮动tab
            tabsFixed: false,

            //是否滑动中
            isScroll: false,

            //当前选中的tab key
            zhiboTabKey: 'zhibojian',

            //当前选中的tab groupId 默认选中第一个
            groupId: '169',

            //显示发帖输入框
            showPostInput: false,

            //发帖输入框的提示文案
            placeholder: '',

            //帖子列表
            groups: {
                //直播间
                'zhibojian': {
                    key: 'zhibojian',
                    name: '直播间', //tab名称
                    class: ['zhiboTabTv', 'zhiboTabTvBlue'], //数组下标0为未选中样式；下标1为选中样式
                    groupId: '169', //社区栏目ID
                    is_digest: '', //非精选图传空
                    posts: {
                        isEnd: '0',
                        list: []
                    }
                },
                //精选图
                'jingxuantu': {
                    key: 'jingxuantu',
                    name: '精选图',
                    class: ['zhiboTabPic', 'zhiboTabPicBlue'],
                    groupId: '169',
                    is_digest: 1, //精选图必传参数
                    posts: {
                        isEnd: '0',
                        list: []
                    }
                }
            },

            //新消息的数量
            newPostsNumber: '0',

            //上滑加载更多
            loadingMoreProps: {
                showMsg: true, //显示文字提醒
                msgType: 0, // 对应msg字段的下标
                msg: ['点击加载更多', '加载中...', 'END'], //文字提醒
                height: 60, //触发加载数据的高度 单位px
                loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
                line: true, //END时,是否添加贯穿文本的横线
            },
        }
    }

    componentWillMount() {
        common.setViewTitle('广州车展直播间');
    }

    componentDidMount() {
        window.homeScroll = e => this.handleScroll(e)
        window.addEventListener('scroll', window.homeScroll);

        this.getData(); // 请求数据

        setTimeout(() => {
            this.setIntervalGetNewPostNumber();
        }, 3000);
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {
        window.removeEventListener('scroll', window.homeScroll);
    }

    getData() {
        // 如果存在缓存数据 则还原数据
        let _state = sessionStorage.getItem('zhibo_state');
        if (_state) {
            _state = JSON.parse(_state); // 转为对象
            _state.loadingMoreProps.loadingMore = () => this.loadingMore(); //字符串转对象时，方法丢失了
            const scrollTop = sessionStorage.getItem('zhibo_scrollTop') || 0;
            this.setState(Object.assign({}, this.state, _state), () => {
                // 还原滚动条位置
                document.body.scrollTop = scrollTop;
            });

            // 清除缓存
            sessionStorage.removeItem('zhibo_state');
            sessionStorage.removeItem('zhibo_scrollTop');
        } else {
            let { groupId, groups, zhiboTabKey } = this.state;
            let { is_digest } = groups[zhiboTabKey];

            //请求数据
            this.getPostsByGroupId({
                groupId: groupId,
                pageSize: 10,
                is_digest: is_digest
            });
        }
    }

    /**
     * 滚动条事件
     * @param {*event} e 
     */
    handleScroll(e) {
        // console.log("页面滚动了已超出指定的位置:", document.body.scrollTop ,this.refs.bannerImg.offsetHeight);
        try {
            if (this.state.tabsFixed && document.body.scrollTop < this.refs.bannerImg.offsetHeight) {
                //还原
                this.setState({
                    tabsFixed: false,
                    isScroll: true, //是否滚动中
                })
                if (document.body.scrollTop > this.refs.bannerImg.offsetHeight) {
                    document.body.scrollTop = this.refs.bannerImg.offsetHeight
                }
            } else if (!this.state.tabsFixed && document.body.scrollTop > this.refs.bannerImg.offsetHeight) {
                //悬浮
                this.setState({
                    tabsFixed: true,
                    isScroll: true, //是否滚动中
                })
                if (document.body.scrollTop < this.refs.bannerImg.offsetHeight) {
                    document.body.scrollTop = this.refs.bannerImg.offsetHeight //滚动高度改为图片的高度
                }
            }

            clearTimeout(timeId);
            timeId = setTimeout(() => this.setState({
                isScroll: false, //是否滚动中
            }), 100); //延迟更新加载状态
        } catch (error) {
            //单页应该没有及时卸载带来的bug提示
            console.log("单页应该没有及时卸载带来的bug提示")
        }
    }

    /**
     * 定时获取新帖子条数
     */
    setIntervalGetNewPostNumber() {
        setInterval(() => {
            let { zhiboTabKey, groups } = this.state
            if (lastTime < new Date().getTime()) {
                this.setLastTime();
                if (groups[zhiboTabKey].is_digest != '1') {
                    //非精选图
                    this.getNewPostNumber();
                }
            } else {
                // console.log("距离上次请求未超过5秒");
            }
        }, 2000);
    }

    /**
     * 设置最后一次请求的时间
     */
    setLastTime(s = 5) {
        lastTime = new Date().getTime() + (1000 * s);
    }

    /**
     * 请求数据
     */
    getPostsByGroupId(postData) {
        let { groupId, direction, postsId } = postData;
        let { loadingMoreProps, zhiboTabKey, groups } = this.state;

        this.setLastTime(10); //延迟10秒再获取新数据

        // 非下拉获取数据
        if (direction !== 'down') {
            //改变对应的加载文案
            if (postsId) {
                this.setState({
                    loadingMoreProps: Object.assign({}, loadingMoreProps, {
                        msgType: 1, // 对应msg字段的下标
                    })
                });
            }
        }

        //定义一个即将改变state的对象，用于一次性改变state，而非多次渲染
        let newState = {};

        Toast.loading('', 30);
        PostService.getPostsByGroupId(postData).then(data => {

            Toast.hide();

            this.setLastTime();

            let isEnd = '0';
            if (data.code == 1000) {

                //合并数据
                let groupDatas = groups[zhiboTabKey]; //栏目数据
                let oldList = groupDatas.posts.list;
                let oldPostsIds = oldList.map(item => item.postsId);
                // let newList = data.data.list.filter(item => oldPostsIds.indexOf(item.postsId) === -1); //过滤重复
                let newList = oldList.concat(data.data.list); //合并数据 包含重复的

                if (newList.length > 0) {
                    //范式化 用于过滤重复
                    const postEntity = new schema.Entity('post', {}, { idAttribute: 'postsId' });
                    const postArray = new schema.Array(postEntity);
                    const normalizedData = normalize(newList, postArray);

                    //去重
                    newList = [];
                    let postObj = normalizedData.entities.post;
                    let postObj_len = Object.keys(postObj).length; //长度
                    Object.keys(postObj).map((postsId, i) => newList[postObj_len - 1 - i] = postObj[postsId]);
                }

                //根据帖子ID去获取数据
                if (postsId) {
                    if (direction == 'down') {
                        //下拉
                        document.body.scrollTop = 0 //帮用户滚动页面到顶部

                        //改变未读消息数
                        newState.newPostsNumber = 0;
                    }
                }

                isEnd = direction == 'down' ? '0' : data.data.isEnd; //加载最新帖子时，不应该出现end
                groupDatas.posts = {
                    isEnd, //加载最新帖子时，不应该出现end
                    list: newList
                }
                //栏目帖子
                newState.groups = Object.assign({}, this.state.groups, {
                    [zhiboTabKey]: groupDatas
                })
            } else {
                if (data.code != "90015") {
                    Toast.info(data.msg || ChMessage.FETCH_FAILED);
                } else {
                    document.body.scrollTop = 0 //滚动到顶部，避免一直出现请用户登录
                }
            }

            // 非下拉获取数据
            if (direction !== 'down') {
                //判断是否已经没有更多数据可以获取
                if (data.data && isEnd == '1') {
                    newState.loadingMoreProps = Object.assign({}, loadingMoreProps, {
                        msgType: 2, // 对应msg字段的下标
                    });
                } else {
                    newState.loadingMoreProps = Object.assign({}, loadingMoreProps, {
                        msgType: 0, // 对应msg字段的下标
                    });
                }
            }

            //统一改变state 避免多次渲染
            this.setState(Object.assign({}, this.state, newState));
            newState = undefined; //销毁
        }, () => {
            Toast.hide(); //隐藏Toast
            Toast.info("系统繁忙，请稍后再试");
            this.setState({
                loadingMoreProps: Object.assign({}, loadingMoreProps, {
                    msgType: 0, // 对应msg字段的下标
                })
            })
        });
    }

    /**
     * 请求新信息的条数
     */
    getNewPostNumber() {
        let { groupId, zhiboTabKey, groups, newPostsNumber } = this.state
        let { list } = groups[zhiboTabKey].posts;
        let { is_digest } = groups[zhiboTabKey];

        let postsId = list.length > 0 ? list[0].postsId : '';

        let postData = {
            groupId: groupId,
            postsId: postsId, //第一个帖子的id
            is_digest: is_digest, //精选图
            direction: 'down'//up 上划 down下拉。
        }

        PostService.getCounts(postData).then(res => {
            if (res.code == '1000' && res.data) {
                if (newPostsNumber != res.data.count) {
                    //存在新消息数时才渲染页面 避免频繁渲染
                    this.setState({
                        newPostsNumber: res.data.count
                    })
                }
            }
        }).then(() => {
            this.setLastTime(); //修改下次获取时间为5秒后
        })
    }

    /**
     * 加载更多
     */
    loadingMore() {
        let { loadingMoreProps, groupId, groups, zhiboTabKey } = this.state;
        let { list } = groups[zhiboTabKey].posts;
        let { is_digest } = groups[zhiboTabKey];


        if (list.length === 0) {
            this.setState({
                loadingMoreProps: Object.assign({}, loadingMoreProps, {
                    msgType: 2, // 对应msg字段的下标
                })
            })
            return;
        }

        let { postsId } = list[list.length - 1];
        this.getPostsByGroupId({
            groupId: groupId,
            direction: 'up',//上滑
            postsId: postsId, //最后一个帖子的id
            pageSize: 10,
            is_digest: is_digest, //精选图
        })
    }

    /**
     * 获取新消息
     */
    getNewPosts() {
        let { loadingMoreProps, groupId, groups, zhiboTabKey, newPostsNumber } = this.state;
        let { list } = groups[zhiboTabKey].posts;
        let { is_digest } = groups[zhiboTabKey];
        let fristPostId = list.length > 0 ? list[0].postsId : '';

        this.getPostsByGroupId({
            groupId: groupId,
            direction: 'down',//下拉
            postsId: fristPostId, //第一个帖子的id
            pageSize: newPostsNumber * 1 + 10, //获取多10条数据，避免获取数据的过程中有新的帖子
            is_digest: is_digest,
        });
    }

    /**
     * tab触发了切换事件
     * @param {*string} key 
     */
    tabsCallback(zhiboTabKey) {
        console.log("tab触发了切换事件：", zhiboTabKey);
        let { groups, loadingMoreProps } = this.state;
        let { is_digest, groupId, posts } = groups[zhiboTabKey];

        //改变选中的key
        this.setState({
            groupId: groupId,
            zhiboTabKey: zhiboTabKey,

            //修改加载文案
            loadingMoreProps: Object.assign({}, loadingMoreProps, {
                msgType: posts.isEnd == '1' ? 2 : 0, // 对应msg字段的下标
            }),
        }, () => {
            //列表没有数据的时候 才去请求数据
            if (posts.list.length === 0) {
                this.getPostsByGroupId({
                    groupId: groupId,
                    is_digest: is_digest
                });
            }
        });

        document.body.scrollTop = 0; //回到顶部
    }

    /**
     * 显示发帖输入框
     */
    showPostInput() {
        let userInfo = userHelper.getUserIdAndToken();
        if (!userInfo.userId || !userInfo.token) {
            userHelper.Login();
            return false; //跳转到登录页面
        }

        this.setState({
            showPostInput: true
        }, () => {
            //弹出“输入框”的次数
            common.sendCxytj({
                eventId: 'h5_HD_shurukuang_UV'
            });
        })
    }

    /**
     * 隐藏发帖输入框
     */
    hidePostInput() {
        this.setState({
            showPostInput: false,
            placeholder: '' //还原提示文案
        })
    }

    /**
     * 发帖
     */
    send(postData) {
        //点击“发送”的次数
        common.sendCxytj({
            eventId: 'h5_HD_fasong_UV'
        });

        this.setLastTime();

        Toast.loading('', 30);
        PostService.createPost(postData).then(data => {
            Toast.hide(); //隐藏加载中

            if (data.code == 1000) {
                data = data.data;
                if (data.points && data.points > 0) {
                    Toast.info(`发布成功 +${data.points}分`, 1);
                } else {
                    Toast.info('发布成功', 1);
                }

                this.hidePostInput(); //隐藏输入框
                this.setLastTime();

                //切换tab
                this.tabsCallback('zhibojian');
                setTimeout(() => {
                    Toast.hide(); //隐藏加载中
                    this.getNewPosts(); //ajax加载新帖子
                    // window.location.reload(); //更好的方法是通过接口获取数据。
                }, 1000);

            } else {
                Toast.info(data.msg || ChMessage.FETCH_FAILED);
            }
        }, () => {
            Toast.hide(); //隐藏Toast
            Toast.info(ChMessage.FETCH_FAILED);
        });
    }

    /**
     * 点赞
     */
    clickSupports(data, key) {
        let userInfo = userHelper.getUserIdAndToken();
        if (!userInfo.userId || !userInfo.token) {
            userHelper.Login();
            return false; //跳转到登录页面
        }

        let { zhiboTabKey } = this.state
        let newState = this.state;

        let postData = {
            "groupId": data.groupId,//社区id
            "postsId": data.postsId,//帖子ID
        }
        if (data.is_praise == 1) {
            //取消点赞
            postData.ac_type = 5;

            //state新状态
            newState.groups[zhiboTabKey].posts.list[key] = Object.assign({}, data, {
                is_praise: '0',
                supports: data.supports * 1 - 1
            });
        } else {
            //点赞
            postData.ac_type = 4;

            //state新状态
            newState.groups[zhiboTabKey].posts.list[key] = Object.assign({}, data, {
                is_praise: '1',
                supports: data.supports * 1 + 1
            });
        }

        //直接显示点赞成功 不等待接口的返回
        this.setState(Object.assign({}, this.state, newState));

        PostService.praise(postData).then(data => {
            // console.log("点赞功能：", data)
        }, () => {
            // Toast.hide(); //隐藏Toast
            // Toast.info("系统繁忙，请稍后再试");
        });
    }

    /**
     * 评论
     */
    clickComments(data) {
        let userInfo = userHelper.getUserIdAndToken();
        if (!userInfo.userId || !userInfo.token) {
            userHelper.Login();
            return false; //跳转到登录页面
        }
        this.setState({
            placeholder: `回复${data.authorUsername} `,
            showPostInput: true,
        }, () => {
            //弹出“输入框”的次数
            common.sendCxytj({
                eventId: 'h5_HD_shurukuang_UV'
            });
        })
    }

    /**
     * 跳转到帖子详情
     */
    toPostsDetail({ groupId, postsId, catid }) {
        // 跳转前先保存当前的数据信息 以及滚动条位置
        sessionStorage.setItem('zhibo_state', JSON.stringify(this.state));
        sessionStorage.setItem('zhibo_scrollTop', document.body.scrollTop);
        const url = config.communityUrl + `?api_version=3#/post/${groupId}/${postsId}/${catid}`;

        if (common.isCXYApp()) {
            try {
                this.openNewWebView(url);
            } catch (e) {
                // 调用jsdk出错了
            }
        } else {
            window.location.href = config.communityUrl + `#/post/${groupId}/${postsId}/${catid}`;
        }

    }

    /**
     * jsdk打开新view
     */
    openNewWebView(url) {
        window.cx580.jsApi.call({
            "commandId": "",
            "command": "openNewBrowserWithURL",
            "data": {
                "url": url,
                "umengId": "cfw_youkachongzhi",
                "showTitleLayout": "false",
                "showLoading": "true"
            }
        }, function (data) { });
    }

    render() {
        let { peopleNumber, tabsFixed, zhiboTabKey, showPostInput, groups, loadingMoreProps, newPostsNumber, placeholder } = this.state
        let tabs = Object.keys(groups).map(key => groups[key]);
        let { groupId, is_digest } = groups[zhiboTabKey];

        //每次渲染的时候 都执行一次懒加载
        window.Echo.init({
            offset: 0,
            throttle: 0
        });

        return (
            <div>
                {/* 列表 start */}
                <LoadingMore {...loadingMoreProps}>
                    {/* banner start */}
                    <div ref='bannerImg' className={styles.banner}>
                        <img src='./images/banner.png' />
                        <div className={styles.bannerText}>
                            <p><span className={styles.peopleNumberBg}>{peopleNumber}人参与互动</span></p>
                        </div>
                    </div>
                    {/* banner end */}

                    <Tabs
                        activeKey={zhiboTabKey}
                        className={tabsFixed ? "wTabs" : ''}
                        animated={true}
                        swipeable={false} //是否可以滑动 tab 内容进行切换
                        onChange={key => this.tabsCallback(key)}
                    >
                        {
                            tabs.map((item, i) =>
                                <TabPane
                                    tab={<span className={zhiboTabKey == item.key ? item.class[1] : item.class[0]}>{item.name}</span>}
                                    key={item.key}>
                                    {/*渲染帖子列表 start*/}
                                    <div style={{ height: '.24rem' }}></div>
                                    {
                                        item.posts.list.map((item2, i2) =>
                                            <div key={'post-' + i2 + item2.postsId}>
                                                <ZhiboPost {...item2}
                                                    toDetail={() => this.toPostsDetail(item2)}
                                                    clickComments={(d) => this.clickComments(d, i2)}
                                                    clickSupports={(d) => this.clickSupports(d, i2)}
                                                />
                                                <div style={{ height: '.14rem' }}></div>
                                            </div>
                                        )
                                    }
                                    {/*渲染帖子列表 end*/}
                                </TabPane>
                            )
                        }
                    </Tabs>
                </LoadingMore>
                {/* 列表 end */}

                {/* 底部逼真按钮 start */}
                <FooterEdi onClick={() => this.showPostInput()} />
                {/* 底部逼真按钮 end */}

                {/* 返回顶部 start */}
                {
                    300 < document.body.scrollTop
                        ? <div className={styles.goTop} onClick={() => document.body.scrollTop = 0}>
                            <img src='./images/goTop.png' alt='返回顶部' />
                        </div>
                        : ''
                }
                {/* 返回顶部 start */}

                {/* 有新消息 start */}
                {
                    newPostsNumber > 0 && !is_digest
                        ? <NewPostNumber number={newPostsNumber} onClick={() => this.getNewPosts()} />
                        : ''
                }
                {/* 有新消息 end */}

                {/* 发帖 start */}
                {
                    showPostInput ?
                        <SendZhiboPost
                            groupId={groupId}
                            send={postData => this.send(postData)}
                            close={() => this.hidePostInput()}
                            placeholder={placeholder}
                        /> : ''
                }
                {/* 发帖 end */}
            </div>
        );
    }
}

export default ZhiBo;