import JsApiService from '../services/JsApiService'
import common from './common'
import config from '../config'

/**
 * Js文件加载、初始化操作类
 */
class JsApiInit {

    constructor() {
        this.debug = config.debugUsers.indexOf(sessionStorage.getItem('userId')) > -1;
        this.requireJs(); //引入JSDK
        this.cxytjInit(); //埋点JS初始化
    }

    requireJs() {
        let userType = common.getJsApiUserType();
        let jsSrc = '';
        if (userType == 'weixin') {
            jsSrc = '//res.wx.qq.com/open/js/jweixin-1.2.0.js';
        } else if (userType == 'qq') {
            jsSrc = '//qzonestyle.gtimg.cn/qzone/qzact/common/share/share.js';
        }
        if (jsSrc) {
            let hm = document.createElement("script");
            hm.src = jsSrc;
            hm.onload = () => {
                this.jsApiIsLoad(); //JsApi加载完成后执行
            }
            hm.onerror = () => {
                if(sessionStorage.getItem('userId')){
                    networkError('./images/networkError-icon.png');
                }
            }
            let s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        } else {
            this.jsApiIsLoad(); //JsApi加载完成后执行
        }
    }

    jsApiIsLoad() {
        this.runJsApi() //执行JsApi
    }

    runJsApi() {
        let userType = common.getJsApiUserType();
        if (userType == 'weixin') {
            this.weiXinConfig() //初始化微信配置
        }
        if (userType == 'qq') {
            this.qqConfig() //初始化QQ配置
        }
        if (userType == 'alipay') {
            common.alipayReady(() => AlipayJSBridge.call('hideOptionMenu')); //隐藏支付宝右上角
        }
    }

    /**
     * 微信配置
     */
    weiXinConfig() {
        let shareUrl = location.href.split('#')[0];
        let userType = common.getJsApiUserType();
        if (userType == 'weixin') {
            JsApiService.weiXinConfig({
                signUrl: encodeURIComponent(shareUrl)
            }).then(data => {
                try {
                    wx.config({
                        debug: this.debug, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                        appId: data.data.appId, // 必填，公众号的唯一标识
                        timestamp: data.data.timestamp, // 必填，生成签名的时间戳
                        nonceStr: data.data.nonceStr, // 必填，生成签名的随机串
                        signature: data.data.signature,// 必填，签名，见附录1
                        jsApiList: [
                            'checkJsApi',
                            'onMenuShareTimeline',
                            'onMenuShareAppMessage',
                            'onMenuShareQQ',
                            'onMenuShareWeibo',
                            'onMenuShareQZone',
                            'hideMenuItems',
                            'showMenuItems',
                            'hideAllNonBaseMenuItem',
                            'showAllNonBaseMenuItem',
                            'translateVoice',
                            'startRecord',
                            'stopRecord',
                            'onRecordEnd',
                            'playVoice',
                            'pauseVoice',
                            'stopVoice',
                            'uploadVoice',
                            'downloadVoice',
                            'chooseImage',
                            'previewImage',
                            'uploadImage',
                            'downloadImage',
                            'getNetworkType',
                            'openLocation',
                            'getLocation',
                            'hideOptionMenu',
                            'showOptionMenu',
                            'closeWindow',
                            'scanQRCode',
                            'chooseWXPay',
                            'openProductSpecificView',
                            'addCard',
                            'chooseCard',
                            'openCard'
                        ]
                    });

                    window.addEventListener("hashchange", () => {
                        //所有需要使用JS-SDK的页面必须先注入配置信息，否则将无法调用（同一个url仅需调用一次，对于变化url的SPA的web app可在每次url变化时进行调用,目前Android微信客户端不支持pushState的H5新特性，所以使用pushState来实现web app的页面会导致签名失败，此问题会在Android6.2中修复）。
                        wx.config({
                            debug: this.debug, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                            appId: data.data.appId, // 必填，公众号的唯一标识
                            timestamp: data.data.timestamp, // 必填，生成签名的时间戳
                            nonceStr: data.data.nonceStr, // 必填，生成签名的随机串
                            signature: data.data.signature,// 必填，签名，见附录1
                            jsApiList: [
                                'checkJsApi',
                                'onMenuShareTimeline',
                                'onMenuShareAppMessage',
                                'onMenuShareQQ',
                                'onMenuShareWeibo',
                                'onMenuShareQZone',
                                'hideMenuItems',
                                'showMenuItems',
                                'hideAllNonBaseMenuItem',
                                'showAllNonBaseMenuItem',
                                'translateVoice',
                                'startRecord',
                                'stopRecord',
                                'onRecordEnd',
                                'playVoice',
                                'pauseVoice',
                                'stopVoice',
                                'uploadVoice',
                                'downloadVoice',
                                'chooseImage',
                                'previewImage',
                                'uploadImage',
                                'downloadImage',
                                'getNetworkType',
                                'openLocation',
                                'getLocation',
                                'hideOptionMenu',
                                'showOptionMenu',
                                'closeWindow',
                                'scanQRCode',
                                'chooseWXPay',
                                'openProductSpecificView',
                                'addCard',
                                'chooseCard',
                                'openCard'
                            ]
                        });

                        wx.ready(function () {
                            this.wxReady();
                        }.bind(this));
                    });

                } catch (error) {

                }
                try {
                    wx.ready(function () {
                        this.wxReady();
                    }.bind(this));
                } catch (error) {

                }
            }, error => console.error(error))

        }
    }

    wxReady() {
        //屏蔽分享等功能
        // wx.hideAllNonBaseMenuItem(); // “基本类”按钮详见附录3

        //分享功能
        let title = 'MM和你一起去看车展';
        let link = window.location.href;
        let imgUrl = config.shareIcon;
        let shareDatas = {
            title,
            desc: title,
            link,
            imgUrl,
            success: function () {
                // 用户确认分享后执行的回调函数
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        }

        //分享到朋友圈
        wx.onMenuShareTimeline(shareDatas);

        //分享给朋友
        wx.onMenuShareAppMessage(shareDatas);

        //分享到QQ
        wx.onMenuShareQQ(shareDatas);

        //分享到腾讯微博
        wx.onMenuShareWeibo(shareDatas);

        //分享到QQ空间
        wx.onMenuShareQZone(shareDatas);
    }

    /**
     * QQ配置
     */
    qqConfig() {
        //分享功能
        let title = 'MM和你一起去看车展';
        let link = window.location.href;
        let imgUrl = config.shareIcon;

        window.setShareInfo({
            title: title,
            summary: title,
            pic: imgUrl,
            url: window.location.href,
            WXconfig: {
                swapTitleInWX: true,
                appId: '',
                timestamp: '',
                nonceStr: '',
                signature: ''
            }
        });
        window.addEventListener("hashchange", () => {
            window.setShareInfo({
                title: title,
                summary: title,
                pic: imgUrl,
                url: window.location.href,
                WXconfig: {
                    swapTitleInWX: true,
                    appId: '',
                    timestamp: '',
                    nonceStr: '',
                    signature: ''
                }
            });
        });
    }

    /**
     * 埋点JS
     */
    cxytjInit() {
        if (window.CXYTongji) {
            //埋点已加载完毕
            this.cxytjInitData();
        } else {
            //埋点JS还未加载完毕，这里监听加载完毕事件
            document.addEventListener('CXYTongjiReady', () => this.cxytjInitData(), false);
        }

        //监听hash路由的变化，自动保存title作为埋点的上一页信息
        window.addEventListener("hashchange", function (e) {
            sessionStorage.setItem("prevPageInfo", document.title);
        }, false);
    }

    /**
     * 埋点JS初始化的数据
     */
    cxytjInitData() {
        window.cxytj.init({ //以下为初始化示例，可新增或删减字段
            productId: 'h5_HD_zhibo', //产品ID
            productVersion: '1.0', //产品版本
            channel: sessionStorage.getItem('userType'), //推广渠道
            isProduction: config.production, //生产环境or测试环境 默认测试环境
            userId: sessionStorage.getItem('userId'), //用户ID
        });
    }
};

// 实例化后再导出
export default new JsApiInit()


window.onerror = function (msg, url, l) {
    var txt = '';
    txt = "There was an error on this page.\n\n";
    txt += "Error: " + msg + "\n";
    txt += "URL: " + url + "\n";
    txt += "Line: " + l + "\n\n";
    txt += "Click OK to continue.\n\n";
    if (config.debugUsers.indexOf(sessionStorage.getItem('userId')) > -1) {
        alert(txt);//测试环境
    } else {
        return true; //正式环境屏蔽错误
    }
}