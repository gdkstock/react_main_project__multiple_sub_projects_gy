/**
 * URL重定向
 */
import { Toast } from 'antd-mobile'

//各个平台的JSDK
import jsApi from './cx580.jsApi'

//常用工具类
import common from './common'
import { ChMessage } from './message.config'

class Upload {
    constructor() {

    }

    /**
     * 上传图片
     * @param {*function} callback 回调函数 上传图片成功后执行 callback(data)
     */
    uploadImg(callback) {
        if (common.getJsApiUserType() == 'app') {
            this.appUploadImg(callback);
        }
    }

    //APP图片上传
    appUploadImg(callback) {
        //默认
        let data = {
            "quality": 75,/*图片质量, 取值1到100*/
            "maxWidth": 760,/*图片的最大宽度. 过大将被等比缩小*/
            "maxSize": 300
        };
        let version = common.getAppVersion();

        if (version) {
            version = version.split(".");
            if ((version[0] * 1) > 6 || ((version[0] * 1) > 5 && (version[1] * 1) > 0)) {
                //APP 6.1.0以上
            } else {
                //APP 6.1.0以下
                data = {
                    "quality": 75,/*图片质量, 取值1到100*/
                    "maxWidth": 760,/*图片的最大宽度. 过大将被等比缩小*/
                    "maxHeight": 760,/*图片的最大高度. 过大将被等比缩小*/
                    "maxSize": 300
                };
            }
        }
        try {
            jsApi.call({
                "commandId": "",/*命令主键，每次调用，必须用全局唯一的字符串*/
                "command": "picture",/*执行的命令，原生APP会根据此值执行相应的逻辑*/
                "data": data,
                "quality": 75,/*图片质量, 取值1到100*/
                "maxWidth": 760,/*图片的最大宽度. 过大将被等比缩小*/
                "maxSize": 300
            }, function (result) {
                callback(result.data.dataBase64); //回调函数
            });
        } catch (error) {
            Toast.info(ChMessage.FETCH_FAILED);
        }

    }
}

// 实例化后再导出
export default new Upload()