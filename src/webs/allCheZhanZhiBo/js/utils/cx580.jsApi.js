﻿/**
 * App JS SDK
 */

//配置文件
import config from '../config'

let jsApi = {}
try {
    window.cx580.jsApi.config({
        version: window.location.href.indexOf('api_version=3') > -1 ? '3.0' : '2.0', //jsApi版本
        jsApiList: ['cxyPageResume'] //注册需要APP监听的jsApi列表
    }, function (data) {
        // alert(JSON.stringify(data));
        init();
    });

    jsApi = window.cx580.jsApi
} catch (error) {
    //非APP环境
}

const init = () => {
    // 设置分享按钮
    window.cx580.jsApi.call({
        "commandId": "",
        "command": "customizeTopRightButton",
        "data": {
            "reset": "false",
            "list": [
                {
                    "iconUrl": '',
                    "text": "分享",
                    "callbackMethodName": "shareZhiBo"
                }
            ]
        }
    }, function (data) { });
}

//默认分享
window.shareZhiBo = () => {
    let url = config.shareUrl + window.location.hash + '?t=' + new Date().getTime(); //加个问号，避免QQ无法正常显示
    window.cx580.jsApi.call({
        "commandId": "",
        "command": "share",
        "data": {
            "url": url,
            "title": "MM和你一起去看车展",
            "content": "MM和你一起去看车展",
            "icon": config.shareIcon,
            "shareChannel": "weiXin,wenXinCircle,alipay,qq",
            "transportMap": {}
        }/*封装需求请求的数据*/
    }, function (data) {

    });
}

// 回到栈顶事件
window.cxyPageResume = () => {
    // 显示title栏 避免其他项目隐藏了title栏
    window.cx580.jsApi.call({
        "commandId": "",
        "command": "controlTitleLayout",
        "data": {
            "showStatus": "show"
        }
    }, function (data) { });
}



export default jsApi