/**
 * 配置
 */
import common from './utils/common'

const userType = sessionStorage.getItem('userType') || common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay'
const production = window.location.host.indexOf('comments.cx580.com') > -1; //是否为生产环境
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;

export default {
    //是否为生产环境
    production: production,

    //接口地址
    baseApiUrl: production ? window.location.protocol + "//" + window.location.host + "/" : "http://testshequ.cx580.com:18066/",

    //单点登录地址 回调地址需自行补全
    authUrl: authUrl,

    //社区地址
    communityUrl: production ? 'http://comments.cx580.com/dist/index.html' : "http://testshequ.cx580.com:18066/dist/index.html",

    //分享地址
    shareUrl: production ? "https://comments.cx580.com/H5/CheZhanZhiBo/index.html" : "http://testshequ.cx580.com:18066/H5/CheZhanZhiBo/index.html",

    //分享小图标
    shareIcon: production ? "https://comments.cx580.com/H5/CheZhanZhiBo/images/share.png" : "http://testshequ.cx580.com:18066/H5/CheZhanZhiBo/images/share.png",

    //直播URL
    zhiboUrl: 'http://m.yizhibo.com/member/mpersonel/go_homepage?memberid=278886712',

    //调试的userId
    debugUsers: ['B406E4D73A704585AB64B35E2A7896BA', 'B4C02D709C9E41AAB3F7B40BD073B4B5', 'A917E6D2B8294B35ACD718E13F75D198'], //debug的userId账号
}