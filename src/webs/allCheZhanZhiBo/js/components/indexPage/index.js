//样式
import styles from './index.scss'

//组件
import { Toast } from 'antd-mobile'

//service
import IndexService from '../../services/indexService'

//通用工具类
import common from '../../utils/common'
import config from '../../config'

/**
 * 获取直播URL
 */
IndexService.getZhiBoUrl({}).then(res => {
    if (res.code == 1000 && res.data && res.data.domain) {
        window.zhiboUrl = res.data.domain;
    }
})


/**
 * 车展直播宣传页首页
 * @param {*object} props 
 */
const IndexPage = props => {

    return (
        <div className={styles.box}>
            <div className={styles.imgs}>
                <img src='./images/index/wuhan-01.png' onClick={() => toTuwenUrl()} />
                <img src='./images/index/wuhan-02.png' onClick={() => toTuwenUrl()} />
                <img src='./images/index/wuhan-03.png' />
                <img src='./images/index/wuhan-04.png' />
            </div>
        </div>
    )
}

export default IndexPage;


//跳转到直播url
const toZhiboUrl = () => {
    common.sendCxytj({
        eventId: 'h5_HD_yizhibo_PV'
    });

    let zhiboUrl = window.zhiboUrl || config.zhiboUrl;

    if (common.isCXYApp()) {
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "openNewBrowserWithURL",
                "data": {
                    "url": zhiboUrl,
                    "umengId": "cfw_youkachongzhi",
                    "showTitleLayout": "true",
                    "showLoading": "true"
                }
            }, function (data) { });
        } catch (error) {
            window.location.href = zhiboUrl;
        }

    } else {
        window.location.href = zhiboUrl;
    }
}

//跳转到图文直播URL
const toTuwenUrl = () => {
    common.sendCxytj({
        eventId: 'h5_HD_tuwenzhibo_PV'
    });

    document.body.scrollTop = 0; //避免单页应用滚动条没有停留在顶部
    window.location.href = common.getRootUrl() + 'zhibo';
}