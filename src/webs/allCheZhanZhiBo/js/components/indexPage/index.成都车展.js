//样式
import styles from './index.scss'

//组件
import { Toast } from 'antd-mobile'

//service
import IndexService from '../../services/indexService'

//通用工具类
import common from '../../utils/common'
import config from '../../config'

/**
 * 获取直播URL
 */
IndexService.getZhiBoUrl({}).then(res => {
    if (res.code == 1000 && res.data && res.data.domain) {
        window.zhiboUrl = res.data.domain;
    }
})


/**
 * 车展直播宣传页首页
 * @param {*object} props 
 */
const IndexPage = props => {

    return (
        <div>
            <div className={styles.box}>
                <div className={styles.imgs}>
                    <img src='./images/index/index-01.png' />
                    <img src='./images/index/index-02.png' />
                </div>
                <div className={styles.box1Text}>
                    <p>直播时间：详情见下方直播时段预告</p>
                </div>
                <img className={styles.box1Btn} src='./images/index/index-03.gif' onClick={() => toZhiboUrl()} />
            </div>
            <div className={styles.box}>
                <div className={styles.imgs}>
                    <img src='./images/index/index-04.png' />
                    <img src='./images/index/index-05.png' />
                </div>
                <div className={styles.tuwenBtn} onClick={() => toTuwenUrl()} ></div>
            </div>

            {/* 直播时段 start */}
            <div className={styles.time}>
                <div className={styles.imgs}>
                    <img src='./images/index/index-06.png' />
                </div>
                <div className={styles.table}>
                    <table>
                        <thead>
                            <tr className={styles.tableTitle}>
                                <td className={styles.td1Title}>栏目安排</td>
                                <td>日期</td>
                                <td>时间</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className={styles.td1}>探馆直播</td>
                                <td>8月23日-8月24日</td>
                                <td>
                                    <p>
                                        AM 10:00-11:00<br />
                                        PM 14:30-15:30
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td className={styles.td1}>车展直播</td>
                                <td>8月25日-8月26日</td>
                                <td>
                                    <p>
                                        AM 10:30-11:30<br />
                                        PM 12:30-13:30<br />
                                        <span>PM </span>14:30-15:30
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td className={styles.td1}>网红直播</td>
                                <td>8月27日-9月3日</td>
                                <td>
                                    <p>
                                        AM10:00-11:00<br />
                                        AM12:00-13:00
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td className={styles.td1}>图文直播</td>
                                <td>8月25日-9月3日</td>
                                <td>
                                    <p>
                                        全天候
                                </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {/* 直播时段 end */}

            <div className={styles.box}>
                <div className={styles.imgs}>
                    <img src='./images/index/index-07.png' />
                </div>
            </div>
        </div>
    )
}

export default IndexPage;


//跳转到直播url
const toZhiboUrl = () => {
    common.sendCxytj({
        eventId: 'h5_HD_yizhibo_PV'
    });

    let zhiboUrl = window.zhiboUrl || config.zhiboUrl;

    if (common.isCXYApp()) {
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "openNewBrowserWithURL",
                "data": {
                    "url": zhiboUrl,
                    "umengId": "cfw_youkachongzhi",
                    "showTitleLayout": "true",
                    "showLoading": "true"
                }
            }, function (data) { });
        } catch (error) {
            window.location.href = zhiboUrl;
        }

    } else {
        window.location.href = zhiboUrl;
    }
}

//跳转到图文直播URL
const toTuwenUrl = () => {
    common.sendCxytj({
        eventId: 'h5_HD_tuwenzhibo_PV'
    });

    document.body.scrollTop = 0; //避免单页应用滚动条没有停留在顶部
    window.location.href = common.getRootUrl() + 'zhibo';
}