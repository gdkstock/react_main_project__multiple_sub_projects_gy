
//styles
import styles from './index.scss'


/**
 * 直播帖子
 * @param {*object} props 
 */
const ZhiboPost = props => {
    let { isOwner } = props;

    return (
        <div className={isOwner == '1' ? styles.boxOwn : styles.box}>
            <PostContent {...props} />
            <PostImgs {...props} />
            <PostFooter {...props} />
        </div>
    )
}


/**
 * 帖子用户头像、名称和内容
 * @param {*object} props 
 */
const PostContent = props => {

    let { isOwner, headImgUrl, authorUsername, content, toDetail } = props;
    let rawHTML = {
        __html: content
    };
    return (
        <div className={isOwner == '1' ? styles.contentOwn : styles.content} onClick={() => toDetail && toDetail()}>
            <div className={styles.avatar}>
                <img src={headImgUrl || './images/head.png'} />
            </div>
            <div className={styles.name}>{authorUsername || '匿名易友' + parseInt(Math.random() * 1000000)}</div>
            <div className={styles.txt} dangerouslySetInnerHTML={rawHTML}></div>
        </div>
    )
}

//帖子图片列表
const PostImgs = props => {
    let { imgList } = props
    return (
        <div className={styles.imgs}>
            {
                imgList && imgList.map((img, i) =>
                    <img key={'img-' + img} className="lazy" src={img.substr(0, img.length - 4) + '_thumbnail.jpg'} data-echo={img} />
                )
            }
        </div>
    )
}

//帖子底部
const PostFooter = props => {

    let { is_praise, comments, supports, clickComments, clickSupports, toDetail } = props

    return (
        <div className={styles.postFooter}>
            <i
                className={styles.commentNum}
                /* onClick={() => clickComments ? clickComments(props) : false} */
                onClick={() => toDetail && toDetail()}
            >回复</i>
            <i
                className={is_praise == "1" ? styles.likeOrange : styles.like}
                onClick={() => clickSupports ? clickSupports(props) : false}
            >{supports || '0'}</i>
        </div>
    )
}

//底部逼真的输入框
export const FooterEdi = props => {
    let { onClick } = props
    return (
        <div onClick={() => onClick ? onClick() : false}>
            <div style={{ height: '1.3rem' }}></div>
            <div className={styles.footerEdi}>
                <i className={styles.iconEdi}></i>
                <p>我来说两句</p>
                <i className={styles.iconPic}></i>
            </div>
        </div>
    )
}

//有新消息
export const NewPostNumber = props => {
    let { number, onClick } = props
    return (
        <div className={styles.newPostNumber} onClick={() => onClick ? onClick() : false}>
            <span>有 {number} 条新消息</span>
        </div>
    )
}

export default ZhiboPost;