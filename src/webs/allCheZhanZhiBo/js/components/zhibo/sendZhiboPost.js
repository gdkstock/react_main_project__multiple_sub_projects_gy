/**
 * 发送直播帖子
 */
import React, { Component } from 'react';

//组件
import { Toast, Icon, Modal } from 'antd-mobile'

//styles
import styles from './sendZhiboPost.scss';

//servers
import UploadService from '../../services/uploadService'
import PostService from '../../services/postService'

//常用工具类
import common from '../../utils/common';
import userHelper from '../../utils/userHelper'
import upload from '../../utils/upload';
import { ChMessage } from '../../utils/message.config'

//图片上传工具
require("lrz");

class SendZhiboPost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imgs: [], //图片集合 @param src 服务器上的图片地址 @param tempSrc 本地的base64
            placeholder: this.props.placeholder,
            maxImgIndex: -1, //大图Index
        }
    }

    componentWillMount() {
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * jsdk上传图片
     */
    jsdkUploadImg() {
        try {
            upload.uploadImg(base64 => this.postImgToUploadImg(base64));
        } catch (error) {
            console.error(error);
        }

    }

    /**
     * H5上传图片
     * @param {*event} e 
     */
    uploadImgs(e) {
        let { imgs } = this.state
        let files = e.target.files;
        if (files.length + imgs.length > 9) {
            Toast.info("最多只能上传9张图片", 2);
            return;
        }
        for (let i = 0; i < files.length; i++) {
            this._lrz(files[i], i);
        }
    }

	/**
	 * 图片压缩上传
	 * @param e 图片资源
	 */
    _lrz(e) {
        try {
            let files = e;
            let quality = 1;
            console.log("图片大小：", files.size)
            if (files.size > 1024 * 1024 * 5) {
                quality = .2;
            }
            else if (files.size > 1024 * 1024 * 2) {
                quality = .4;
            }
            else if (files.size > 1024 * 1024) {
                quality = .5;
            }
            else if (files.size > 1024 * 500) {
                quality = .4;
            }
            else if (files.size > 1024 * 100) {
                quality = .5;
            } else {
                quality = .7;
            }
            lrz(files, {
                width: 760,
                quality: quality
            }).then((rst) => {
                // 处理成功会执行
                console.log('拿到的图片数据', rst);
                this.postImgToUploadImg(rst.base64);

            }).catch((err) => {
                // 处理失败会执行
                Toast.fail('上传失败!', 1);
            }).always(() => {
                // 不管是成功失败，都会执行
                this.refs.uploadImgInput.value = ""; //清空文件上传的内容 避免上传同一张照片或是拍照时出现的图片无法展示的bug
            });
        } catch (e) {
            Toast.info("图片上传失败", 2)
            console.log(e);
        }

    }

	/**
	 * 上传图片到服务器
	 * @param base64 base64图片
	 */
    postImgToUploadImg(base64) {
        //添加图片
        let imgs = this.state.imgs;
        let index = imgs.push({
            tempSrc: base64
        });
        this.setState({
            imgs: imgs
        })

        //上传图片到服务器
        let uploadimgData = {
            content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
            format: 'jpg',
            channel: sessionStorage.getItem('userType') || 'app',
            thumbnail: `200x`, //缩略图尺寸
            watermark: true //加水印
        }
        UploadService.uploadImg(uploadimgData).then(data => {
            if (data.code === 0) {
                imgs[index - 1].src = data.data.url;
                this.setState({
                    imgs: imgs
                })
            } else {
                Toast.info(data.msg || ChMessage.UPLOAD_ERROR, 2);
            }
        }, () => {
            Toast.info(ChMessage.FETCH_FAILED);
        })
    }

    /**
     * 发表
     */
    send() {
        let userInfo = userHelper.getUserIdAndToken();
        if (!userInfo.userId || !userInfo.token) {
            userHelper.Login();
            return false; //跳转到登录页面
        }

        let text = this.refs.postTextarea.value;

        if (text.replace(/\s*/g,'').length === 0) {
            Toast.info('内容不能为空', 1);
            return false;
        }

        let { imgs, placeholder } = this.state
        let imgList = imgs.map(img => img.src); //图片集

        for (let i = 0; i < imgList.length; i++) {
            if (!imgList[i]) {
                //图片未上传完毕
                Toast.info('图片正在上传中，请稍候再发布');
                return;
            }
        }

        let postData = {
            "groupId": this.props.groupId,
            "catid": 4, //话题
            "content": placeholder + text,
            "imgList": imgList,
        }

        if (this.props.send) {
            this.props.send(postData);
        }
    }

    /**
     * 关闭
     */
    close() {
        if (this.props.close) {
            this.props.close();
        }
    }

    /**
     * 删除图片
     */
    deleteImg(key) {
        let { imgs } = this.state;
        Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>要删除这张图片吗?</span>, [
            { text: '取消', onPress: () => this.setState({ isClickHeaderBtn: false }), style: { color: '#108ee9' } },
            {
                text: '确定', onPress: () => {
                    this.setState({
                        maxImgIndex: -1,
                        imgs: imgs.filter((img, i) => i != key) //删除图片
                    });
                }, style: { color: '#108ee9' }
            }
        ])
    }

    render() {
        let { imgs, placeholder, maxImgIndex } = this.state;
        return (
            <div className={styles.box}>
                <div className={styles.sendBox}>
                    <i className={styles.closeBtn} onClick={() => this.close()}></i>
                    <div className={styles.textarea}>
                        <textarea
                            ref='postTextarea'
                            autoFocus={true}
                            placeholder={placeholder || '请写下你想对车展说的话。'}></textarea>
                    </div>
                    <div className={styles.imgs}>
                        {/* 图片列表 start */}
                        {
                            imgs.map((img, i) =>
                                <div key={i} className={styles.imgBox} onClick={() => this.setState({ maxImgIndex: i })}>
                                    <img src={img.tempSrc} />
                                    {img.src ? '' : <Icon className={styles.loading} type="loading" />}
                                </div>
                            )
                        }
                        {/* 图片列表 end */}

                        {/* 图片上传 start */}
                        {
                            imgs.length >= 9 ? '' :
                                common.getJsApiUserType() == 'app'
                                    ? <img src='./images/icon-add-pic.png' alt="add" onClick={() => this.jsdkUploadImg()} />
                                    : <div className={styles.h5UploadImg}>
                                        <input ref="uploadImgInput" type="file" accept="image/*" onChange={(e) => this.uploadImgs(e)} multiple="multiple" />
                                        <img src='./images/icon-add-pic.png' alt="add" />
                                    </div>
                        }
                    </div>
                    <div className={styles.sendBtn} onClick={() => this.send()}>发送</div>
                </div>

                {/* 查看大图及删除图片 start */}
                {
                    maxImgIndex > -1
                        ? <div>
                            <div className={styles.maxImg}>
                                <img src={imgs[maxImgIndex].tempSrc} onClick={() => this.setState({ maxImgIndex: -1 })} />
                            </div>
                            <div className={styles.delImg} onClick={() => this.deleteImg(maxImgIndex)}></div>
                        </div>
                        : ''
                }
                {/* 查看大图及删除图片 end */}
            </div>
        );
    }
}

export default SendZhiboPost;