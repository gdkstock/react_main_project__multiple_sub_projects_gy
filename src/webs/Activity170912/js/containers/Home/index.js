import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// 加载样式
import Style from './index.scss'

// 加载组件
import auth from '../../utils/auth'
import common from '../../utils/common'
import config from '../../config'
import jsApi from '../../utils/cx580.jsApi'

class Home extends Component {
	constructor(props) {
		super(props);

	}

	/**
   * 生命周期
   */
	componentWillMount() {
		//执行一次，在初始化render之前执行，如果在这个方法内调用setState，render()知道state发生变化，并且只执行一次
		document.querySelector("title").innerHTML = '爱车年检';
	}
	componentDidMount() {
		//在初始化render之后只执行一次，在这个方法内，可以访问任何组件，componentDidMount()方法中的子组件在父组件之前执行
		this.sendCxytjFUN();
	}

	/**
* 事件
*/	//------------ 事件 ------------------

	sendCxytjFUN() {
		// 保存用户信息
		//	alert('sendCxytjFUN= ' + common.getJsApiUserType())
		if (common.getJsApiUserType() == 'app') {
			jsApi.call({
				"commandId": "",
				"command": "getSymbol",
				"data": {
					"deviceId": "",
					"userid": "",
					"lng": "",
					"version": "",
					"channel": "",
					"phone": "",
					"name": "",
					"type|orderNum": "",
					"city": "",
					"accountId": "",
					"token": "",
					"carId": "",
					"carNumber": ""
				}
			}, (data) => {

				sessionStorage.setItem('userId', data.data.accountId);
				sessionStorage.setItem('token', data.data.token);
				sessionStorage.setItem('userType', 'app');
				sessionStorage.setItem('authType', 'app');
				sessionStorage.setItem('productUserId', data.data.userid || data.data.deviceId);
				setTimeout(function () {
					common.sendCxytj({ eventId: 'h5_p_mot_EnterMotpage' })//  埋点
				}, 1000); // 活动请求：没有用户ID，也请求数据
			});
		} else {
			let keyPre = ''; //cookie key值的前缀
			if (!config.production) {
				//测试环境
				keyPre = 'test_';
			}
			sessionStorage.setItem('userId', common.getCookie(keyPre + 'userId') || common.getUrlKeyValue('userId') || '');
			sessionStorage.setItem('token', common.getCookie(keyPre + 'userToken') || common.getUrlKeyValue('token') || '');
			sessionStorage.setItem('userType', common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay');
			sessionStorage.setItem('authType', common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay');

			common.sendCxytj({ eventId: 'h5_p_mot_EnterMotpage' })//  埋点
		}

	}
	//
	// 判断是单点登录还是直接跳转
	toAUTH(url) {

		common.sendCxytj({ eventId: 'h5_e_mot_ClickQuery' })//  埋点

		if (!common.getUrlKeyValue('userId') || !common.getUrlKeyValue('token')) {
			auth.init(url)
		} else {
			this.toURL(url)
		}
	}
	// 页面跳转
	toURL(url) {
		this.context.router.push(url)
	}

	// --------
	render() {

		return (
			<div className={Style.home}>
				<div className={Style.mark} />
				<div className='relative'>
					<div className={Style.btnIcon} onClick={() => this.toURL('/news1/')} />
					<img src='./images/index/1_01.png' width='100%' height='auto' />
				</div>
				<img src='./images/index/1_02.png' width='100%' height='auto' />
				<img src='./images/index/1_03.png' width='100%' height='auto' />
				<img src='./images/index/1_04.png' width='100%' height='auto' />
				<div className='relative'>
					<div className={Style.btn} onClick={() => this.toAUTH(`annual/`)} />
					<img src='./images/index/1_05.png' width='100%' height='auto' />
				</div>
				<img src='./images/index/1_06.png' width='100%' height='auto' />
				<ul>
					<li>1、 活动时间：9月18日-9月24日</li>
					<li>2、 用户在活动期间通过查询年审时间有机会获取年审优惠券，可在车行易办理年审时下单直接抵扣现金</li>
					<li>3、 此活动新老用户均可参与，每个用户只能领取一次</li>
					<li>4、 优惠券有效期为领取后7天内有效</li>
					<li>5、 此优惠券不可与平台其他优惠券同时使用</li>
					<li>6、 车行易公司享有对上述规则的最终解释权</li>
				</ul>
				<br /><br />

			</div>
		);
	}
}

//使用context
Home.contextTypes = {
	router: React.PropTypes.object.isRequired
}

export default connect()(Home)