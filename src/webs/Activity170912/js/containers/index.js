export {default as App} from './App'

// 简单
export { default as Home } from './Home'
export { default as Annual } from './annual'

// 404
export { default as NotFoundPage } from './404.js'

