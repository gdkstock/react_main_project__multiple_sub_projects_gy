import JsApiService from '../services/JsApiService'
import common from './common'
import config from '../config'
import jsApi from '../utils/cx580.jsApi'

/**
 * Js文件加载、初始化操作类:本次活动单独使用
 */
class JsApiInit {

    constructor() {
        this.debug = config.debugUsers.indexOf(sessionStorage.getItem('userId')) > -1;
        this.requireJs(); //引入JSDK
        this.cxytjInit(); //埋点JS初始化
    }

    requireJs() {
        // 这里不分享
        let userType = common.getJsApiUserType();
        let jsSrc = '';
        if (userType == 'weixin') {
            jsSrc = '//res.wx.qq.com/open/js/jweixin-1.2.0.js';
        } else if (userType == 'qq') {
          //  jsSrc = '//mp.gtimg.cn/open/js/openApi.js';
        } else if (userType == 'app') {

            jsApi.call({
                "commandId": "",
                "command": "customizeTopRightButton",
                "data": {
                    "reset": "false",
                    "list": []
                }
            }, function (data) { });

        }
        if (jsSrc) {
            let hm = document.createElement("script");
            hm.src = jsSrc;
            hm.onload = () => {
                this.jsApiIsLoad(); //JsApi加载完成后执行
            }
            hm.onerror = () => {
                networkError('./images/networkError-icon.png');
            }
            let s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        } else {
            this.jsApiIsLoad(); //JsApi加载完成后执行
        }
    }

    jsApiIsLoad() {
        this.runJsApi() //执行JsApi
    }

    runJsApi() {
        let userType = common.getJsApiUserType();
      //  alert(userType)
        let jsSrc = '';
        if (userType == 'weixin') common.jsdkReady(() => WeixinJSBridge.call('hideOptionMenu')); //隐藏微信右上角
        else if (userType == 'alipay') common.jsdkReady(() => AlipayJSBridge.call('hideOptionMenu')); //隐藏支付宝右上角
    }
 



    /**
     * 埋点JS
     */
    cxytjInit() {
        if (window.CXYTongji) {
            //埋点已加载完毕
            this.cxytjInitData();
        } else {
            //埋点JS还未加载完毕，这里监听加载完毕事件
            document.addEventListener('CXYTongjiReady', () => this.cxytjInitData(), false);
        }

        //监听hash路由的变化，自动保存title作为埋点的上一页信息
        window.addEventListener("hashchange", function (e) {
            sessionStorage.setItem("prevPageInfo", document.title);
        }, false);
    }

    /**
     * 埋点JS初始化的数据
     */
    cxytjInitData() {
        window.cxytj.init({ //以下为初始化示例，可新增或删减字段
            productId: 'h5_HD_nianjian', //产品ID
            productVersion: '1.0', //产品版本
            channel: sessionStorage.getItem('userType')||common.getJsApiUserType(), //推广渠道
            isProduction: config.production, //生产环境or测试环境 默认测试环境
            userId: sessionStorage.getItem('userId'), //用户ID
        });
    }
};

// 实例化后再导出
export default new JsApiInit()


window.onerror = function (msg, url, l) {
    var txt = '';
    txt = "There was an error on this page.\n\n";
    txt += "Error: " + msg + "\n";
    txt += "URL: " + url + "\n";
    txt += "Line: " + l + "\n\n";
    txt += "Click OK to continue.\n\n";
    if (config.debugUsers.indexOf(sessionStorage.getItem('userId')) > -1) {
        alert(txt);//测试环境
    } else {
        return true; //正式环境屏蔽错误
    }
}