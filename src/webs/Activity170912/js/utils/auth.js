import common from './common'
import config from '../config'

/**
 * 登录授权:专门用于这个活动，指定跳转
 */
class Auth {

    constructor() {
        //   this.init()
    }

    init(tourl) {

        let userType = common.getJsApiUserType()
        
        let userInfo = this.getUserInfo();

        if (!userInfo.userId || !userInfo.token) {
            //userId或token不存在 则去单点登录
            this.toAuthUrl(tourl);
        } else {
            //保存用户信息
            this.setUserInfo(userInfo, tourl);
        }
        // alert(userInfo.userId?'不要登陆 ':'要去单点登录')
        //  alert(config.production)
    }

    /**
     * 获取用户信息（userId、token、userType、authType）
     * 先从cookie中获取，如果cookie中没有则从url上获取
     */
    getUserInfo() {
        let keyPre = ''; //cookie key值的前缀
        if (!config.production) {
            //测试环境
            keyPre = 'test_';
        }

        return {
            userId: common.getCookie(keyPre + 'userId') || common.getUrlKeyValue('userId') || '',
            token: common.getCookie(keyPre + 'userToken') || common.getUrlKeyValue('token') || '',
            userType: common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay',
            authType: common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay',
            productUserId: common.getUrlKeyValue('productUserId') || common.getJsApiUserType() || ''
        }
    }

    /**
     * 单点登录
     */
    toAuthUrl(tourl) {
        //走单点登录，获取用户信息
        //   alert(common.getUrlKeyValue('userId')?' 1 保存用户信息 ':'1 走单点登录，获取用户信息')
        if (!common.getUrlKeyValue('userId') || !common.getUrlKeyValue('token')) {
            //没有用户信息时跳转到单点登录
            sessionStorage.clear(); //清空sessionStorage缓存
            localStorage.clear();
            this.toUrl(tourl);
        } else {
            //保存用户信息
            this.setUserInfo({
                userId: common.getUrlKeyValue('userId'),
                token: common.getUrlKeyValue('token'),
                userType: common.getUrlKeyValue('userType'),
                authType: common.getUrlKeyValue('authType'),
                productUserId: common.getUrlKeyValue('productUserId') || ''
            })
        }

    }

    /**
     * 保存用户信息
     * @param {*object} user 
     * {
     * userId:'',
     * token:'',
     * userType:'',
     * authType:''
     * }
     */
    setUserInfo(user, tourl) {
        sessionStorage.setItem('userId', user.userId);
        sessionStorage.setItem('token', user.token);
        sessionStorage.setItem('userType', user.userType);
        sessionStorage.setItem('authType', user.authType);
        sessionStorage.setItem('productUserId', user.productUserId);

        this.toUrl(tourl);
        //清除URL上的参数
        //  common.deleteUrlSearch();
    }
    /**
     * 定向跳转
     * tourl
     */
    toUrl(tourl) {
        let url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.hash; //好像不用编码 编码反而出错？
        tourl = tourl || '';



        url = url.replace('#', '%23'); //替换#号

        url = url.split('?')[0]; //过滤？号

        url += tourl + '?api_version=' + config.jsdkV + '&_wv=1027&t=' + new Date().getTime(); //加一个时间戳，避免服务器挂后，一直读取缓存的数据 ,//这里强制跳转到具体网址
        //  window.location.replace(config.authUrl + url); //跳转到单点登录,前网址不保留

        window.location.href = config.authUrl + url; //跳转到单点登录
    }

};

// 实例化后再导出
export default new Auth()