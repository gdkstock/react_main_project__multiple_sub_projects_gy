/**
 * 配置
 */
import common from './utils/common'

const userType = sessionStorage.getItem('userType') || common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay'
const production = window.location.host.indexOf('webservice.cx580.com') > -1; //是否为生产环境 或预发布环境
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;

export default {
    //是否为生产环境
    production: production,

    //接口地址
    baseApiUrl: production ? 'https://weixin.cx580.com:3443/' : 'http://192.168.1.235:18066/',

    //单点登录地址 回调地址需自行补全
    authUrl: authUrl,

    //年检首页
    annualUrl: production ? `https://annualcheck.cx580.com/user/bindInfo` : `http://192.168.1.165:7083/user/bindInfo`, 

    //调试的userId
    debugUsers: [], //debug的userId账号

    jsdkV:3, // 

}