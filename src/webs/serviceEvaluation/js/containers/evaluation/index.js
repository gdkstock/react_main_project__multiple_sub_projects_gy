import React, { Component } from 'react'
import AddEvaluation from './AddEvaluation'
import EvaluationView from './EvaluationView'


export default class Evaluation extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { orderId, isView } = this.props.params

		return (
			<div className="box">
				{isView ? <EvaluationView orderId={orderId} /> : <AddEvaluation orderId={orderId} />}
			</div>
		)
	}
}