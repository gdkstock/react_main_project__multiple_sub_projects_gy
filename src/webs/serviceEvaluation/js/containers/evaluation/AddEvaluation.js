import React, { Component, PropTypes } from 'react';
import { Toast } from 'antd-mobile'
import Style from './index.scss'
import EvaluationService from '../../services/EvaluationService'

//userHelper
import common from '../../utils/common'
//userHelper
import userHelper from '../../utils/userHelper'

class AddEvaluation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            score: 0, //默认0分
            text: '', //评价内容
        }
    }

    componentWillMount() {
        common.setViewTitle('服务评价')
    }

    componentDidMount() {
    }

    componentWillUnmount() {

    }

    /**
     * 渲染分数
     */
    showScore() {
        let score = this.state.score; //分数
        let htm = [];
        for (let i = 0; i < 5; i++) {
            htm.push(<i key={'score' + i} className={i < score ? Style.blue : {}} onClick={() => this.setState({ score: i + 1 })}></i>)
        }
        return htm;
    }

    /**
     * 输入框
     */
    textareaOnInput(e) {
        let text = e.target.value;
        if (text.length > 200) {
            if (this.isIOS()) {
                setTimeout(() => document.body.scrollTop = 60, 10)
            }
            e.target.blur()
            Toast.info("内容不能超过200字")
            e.target.value = this.state.text;
        } else {
            this.setState({
                text: e.target.value
            })
        }
    }

    /**
     * 判断是否为IOS
     */
    isIOS() {
        let u = navigator.userAgent;
        let isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        return isiOS
    }

    /**
     * 提交评价
     */
    send() {
        if (this.state.score === 0) {
            Toast.info('您还没有为星级打分', 1)
            return
        }
        userHelper.getSymbol(data => {
            let { nickName, phone } = data
            if (!nickName && phone) {
                nickName = phone.substr(0, 2) + '**' + phone.substr(-1)
            }
            let { orderId } = this.props;
            let postData = {
                orderId: orderId,
                score: this.state.score.toString(),
                content: this.state.text.replace(/\s/g, '') ? this.state.text : '我来评价订单啦',
                telephone: phone,
                userName: nickName
            }

            Toast.loading('', 0)
            EvaluationService.add(postData).then(data => {
                Toast.hide();
                if (data.code == 1000) {
                    if (common.isCXYApp()) {
                        //APP环境
                        Toast.info('提交成功')
                        common.sendBroadcast('H5_addEvaluation', postData, data => {
                            setTimeout(() => {
                                common.closeAppView() //广播后 延迟500毫秒关闭view
                            }, 500)
                        })
                    } else {
                        //非APP环境
                        Toast.info('提交成功', 2, () => window.history.back());
                    }
                } else {
                    Toast.info(data.msg || '系统繁忙，请稍后再试');
                    if (common.isCXYApp()) {
                        if (data.msg == '亲，已经评价过哦') {
                            common.sendBroadcast('H5_addEvaluation', postData, data => {
                                setTimeout(() => {
                                    common.closeAppView() //广播后 延迟500毫秒关闭view
                                }, 500)
                            })
                        }
                    }
                }
            }, () => {
                Toast.hide()
                Toast.info('系统繁忙，请稍后再试')
            })
        })
    }

    render() {
        return (
            <div className='box'>
                <div className={Style.stars} style={{ padding: '.6rem .3rem' }}>
                    {this.showScore()}
                </div>
                <div className='plr30'>
                    <textarea ref='text' placeholder='快来对本次服务进行评价吧！' onInput={e => this.textareaOnInput(e)}></textarea>
                </div>
                <div style={{ height: '1.64rem' }}></div>
                <div className={Style.sendBtnBox + ' box'}>
                    <div className={Style.btn} onClick={() => this.send()}>提交评价</div>
                </div>
            </div>
        );
    }
}

AddEvaluation.propTypes = {

};

export default AddEvaluation;