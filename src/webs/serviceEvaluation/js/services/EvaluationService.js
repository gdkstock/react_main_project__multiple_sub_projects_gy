/**
 * 评价相关接口
 */

import apiHelper from './apiHelper';

class EvaluationService {
    /**
      * 提交评价
      * @param data object
      * {
      * orderId:'', //订单ID
      * score:'', //评分分数
      * content:'', //评价内容
      * telephone:'', //手机号码
      * userName:'', //用户名称
      * }
      */
    add(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}evaluate/add`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
      * 查看评价
      * @param data object
      * {
      * orderId:'', //订单ID
      * }
      */
    view(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}evaluate/view/${data.orderId}`,
            data: {
                method: 'get'
            }
        };
        return apiHelper.fetch(requestParam);
    }
}

export default new EvaluationService()