
import React, {Component} from 'react';
import jsApi from '../../utils/cx580.jsApi';
import style from './pageBar.scss';
class PageBar extends Component {
	constructor(props) {
		super(props);
		window.cxyControlTitleLayout = function () {
			jsApi.call({
				"commandId": "",
				"command": "controlTitleLayout",
				"data": {
					"showStatus": "hide",
				}
			}, (data) => {
			});
		};

		//返回webview时关闭原生导航栏
		window.cxyPageResume=function(){
			jsApi.call({
				"commandId": "",
				"command": "controlTitleLayout",
				"data": {
					"showStatus": "hide",
				}
			}, (data) => {
			});
		}

	}


	static propTypes = {
		back:React.PropTypes.func,//自定义后退方法
		title:React.PropTypes.string,//标题
		isHome:React.PropTypes.bool//是否首页，点击直接关闭webview
	};


	componentDidMount() {
		//页面卸载时恢复导航条
		document.addEventListener('onpagehide',(e)=>{
			jsApi.call({
				"commandId": "",
				"command": "controlTitleLayout",
				"data": {
					"showStatus": "show",
				}
			}, (data) => {
			});
		},false);

		if (navigator.userAgent.indexOf("appname_cxycwz") > -1) {
			setTimeout(() => {
				jsApi.call({
					"commandId": "",
					"command": "controlTitleLayout",
					"data": {
						"showStatus": "hide",
					}
				}, (data) => {
				});
			}, 500)

		}
	}

	//返回
	goBack() {
		if (this.props.back) {
			this.props.back();
		}
		else {
			if (this.props.isHome) {
				jsApi.call(
					{
						"commandId": "",
						"command": "close"
					});
			}
			else {
				history.go(-1);
			}
		}
	}

	render() {
		return <div className={style.container}>
			<div className={style.pageBar}>
				<img src="./images/back.png" className={style.pageButton} onClick={() => {
					this.goBack();
				}}/>
				<div>{this.props.title}</div>
			</div>
		</div>
	}
}
export default PageBar;