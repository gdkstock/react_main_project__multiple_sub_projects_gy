import React from 'react'
import styles from './index.scss'
import Demo from '../../../../../js/components/common/carPrefixList/demo'

export const BindCar = (props) => (
    <div className={styles.box}>
      <div className={styles.leftItem}>
        <p className={styles.plate}>{props.leftItem}</p>
        <span className={styles.start_s}>*</span>
      </div>
      <div className={`${styles.rightItem} f15`}>
        <span className={styles.brand} >
          <Demo getChooseProvince={(name) => props.getChooseProvince(name)} /><img src="./images/icon_rg_blue.png" alt=""/>
        </span>
        <input type="text"
               style={props.inputStyle}
               //maxLength="7"
               placeholder={props.placeholder}
               value={props.value}
               onInput={e =>props.changeHandler(e)}
        />
      </div>
    </div>
);







