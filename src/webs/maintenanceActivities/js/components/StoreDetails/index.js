import React, { Component } from 'react'
import { connect } from 'react-redux'

// 加载样式
import style from './index.scss'


export default class StoreDetailsList extends Component {
	constructor(props) {
		super(props);
	}

	/**
		 * 事件
		 */
	toUrl(url) {  //跳转详情
		this.context.router.push(url);
		console.log(url);
	}

	/**
	 * 组件
	 */

	render() {
		let data = this.props.data;
		console.log('++++  活动 组件 store ',data)
		return (
			<div className={style.list} onClick={() => { this.toUrl('eventDetails/' + data.actId) }}>
				<div className={style.explain_img}>
					<div className='imgs52 mb30'>
						{data.actPic.length? <img className='img' src={data.actPic} alt={data.actName} /> : <img className='img' src={data.actPic} alt={data.actName} />}
					</div>
				</div>
				<div className={style.explain_text}>
					<p >
						<span className="text-overflow-one">{data.actName}</span>
					</p>
					{data.shopType == '-1' || data.actDesc == ''?
						<div></div>
						:
					<p className={style.shopType1}>
						<img src="./images/benefit.png" alt=""/>
						<span className="text-overflow-one">{data.actDesc}</span>
					</p>}
				</div>
			</div>

		)
	}
}
StoreDetailsList.contextTypes = {
	router: React.PropTypes.object.isRequired
}

StoreDetailsList.defaultProps = {
} 