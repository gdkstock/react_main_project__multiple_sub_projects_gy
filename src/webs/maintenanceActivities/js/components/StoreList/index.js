import React, { Component } from 'react'
import { connect } from 'react-redux'

// 加载样式
import style from './index.scss'


export default class StoreList extends Component {
	constructor(props) {
		super(props);
	}

	/**
		 * 事件
		 */
	//返回的数据
	getData(data) {
		this.props.onClick('DOM', data); //传递给父组件
	}
	/**
	 * 组件
	 */

	render() {
		let data = this.props.data;
		let index = this.props.index;
		let type = this.props.type;
		let dataId, dataPic, dataName, dataType, dataDesc, dataDistance;
	//	console.log(" ******************** 在门店列表", data.shopType ? true : false, " ********************");
		switch (type) {
			case 'list': // 门店列表,这里表示没有右侧的距离提示
				dataId = data.shopId;
				dataPic = data.shopPic;
				dataName = data.shopName;
				dataType = data.shopType; // 这个变量记录优惠类型的图标
				dataDesc = data.shopDesc;
				dataDistance = data.shopDistance;
				break;
			case 'details': // 门店详情
				dataId = data.actId;
				dataPic = data.actPic;
				dataName = data.actName;
				dataType = data.actType; // 这个变量记录优惠类型的图标
				dataDesc = data.actDesc;
				dataDistance = 0; // 没有这个值，为空
				break;
			default:
				break;
		}


		let shopTypeIndex = dataType; // 这个变量记录优惠类型的图标,默认没有-1；1是紫色优惠券

		let imgDOM;

		switch (dataType) { // 优惠券
			case 0:
				imgDOM = <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAAAAAApWe5zwAAAAF0Uk5TAEDm2GYAAAAMSURBVHjaYmAACDAAAAIAAU9tWeEAAAAASUVORK5CYII=' alt={dataName} />;
				break;

			default:
				imgDOM = <img src={dataPic} alt={dataName} />;
				break;
		}

		console.log('++++  xiao组件 store ' + index, dataName, data)

		let distance;
		if(dataDistance < 1000)
			distance=dataDistance+"m"
		else if(dataDistance > 1000)
			distance=(Math.round(dataDistance/100)/10).toFixed(1) + "km"
		return (
			<div
				className={index ? style.list : style.list + ' ' + style.isTop}
				onClick={() => this.getData(dataId)}
			>
				<div className={style.explain_img}>
					{imgDOM}
				</div>
				<div className={type == 'list' ? style.explain_text : style.explain_text + ' ' + style.explain_text_noList}>
					<p >
						<span className="text-overflow-one">{dataName}</span>
					</p>
					{dataType == '-1' ?
						<p></p>
						:
						<p className={'shopType' + shopTypeIndex}>
							<span className="text-overflow-one">{dataDesc}</span>
						</p>}
				</div>
				<div className={type == 'list' ? style.distance : 'hide'}>
					<span>{distance}</span>
				</div>
			</div>

		)
	}
}
