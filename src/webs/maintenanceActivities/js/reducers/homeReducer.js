/**
 * karin 2017.04.12
 */
import {SERVICE_INFO} from "../actions/actionsTypes"

export default function homeReducer(state = {}, action) {
    switch (action.type) {
        case SERVICE_INFO:
            return Object.assign({}, state, {homeInfo: action.data});
        default:
            return state;
    }
}