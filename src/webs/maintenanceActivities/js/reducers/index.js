import { combineReducers } from 'redux'

import homeReducer from './homeReducer'

const rootReducer = combineReducers({
    homeInfo: homeReducer,
});

export default rootReducer;
