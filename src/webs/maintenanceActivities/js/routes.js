import React from 'react'
import { Route, IndexRoute } from 'react-router'

import {
  App,
  Home,
  StoreList,//商家列表
  StoreDetails, //门店详情
  EventDetails, //活动详情
	Meik, //填写资料
  MyAppointment, // 预约列表
  NotFoundPage,
	Gould, //高德地图
} from './containers'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="list/:actId" component={StoreList} />
    <Route path="details/:shopId" component={StoreDetails} />
    <Route path="eventDetails/:actId(/:shopId)" component={EventDetails} />
    <Route path="meik/:actId/:shopId" component={Meik} />
    <Route path="order/:id" component={MyAppointment} />
    <Route path="gould/:g/:t/:n" component={Gould} />
    <Route path="*" component={NotFoundPage} />
  </Route>
);