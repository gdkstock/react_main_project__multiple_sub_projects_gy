import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { Modal, Button, Toast } from 'antd-mobile';
import { connect } from 'react-redux';
import Common from '../../utils/common'
import { getServiceEventDetail } from '../../actions/eventDetails'
import StoreDetailsList from '../../components/StoreDetails' //门店列表公用组件
import jsApi from '../../utils/cx580.jsApi' //获取appUserId
import CancelkService from '../../services/cancelService';
import PageBar from '../../components/pageBer/pageBar'
import userHelper from '../../utils/userHelper'
import style from './index.scss';

class EventDetails extends Component {
	constructor(props) {
		super(props);

		this.state = {
			details: '',
			LngEnd: '',
			LatEnd: '',
			shopList: this.props.defaultShopList, //商家列表
		};
	}

	componentWillMount() {
		Common.setTitleText('活动详情');  // 改标题
		let Lng = parseFloat(localStorage.getItem('Lng'));
		let Lat = parseFloat(localStorage.getItem('Lat'));
		this.setState({ LngEnd: Lng, LatEnd: Lat });

		// let userInfo = userHelper.getUserIdAndToken();
		// if (!userInfo.userId || !userInfo.token) {
		// 	this.cancel();  ///如果已经没有登陆只要进入這个页面都要取消预约状态
		// }else {
		//
		// }
	}

	componentDidMount() {
		this.requestTypeList(); //请求商品品类列表
		// 登陆后返回，则重新拿数据
		//重新回到APP视图
		window.onShowReact = () => this.update();
	}
	// 是否从登陆回来
	update() {
		if (localStorage.getItem("upLoginState") && localStorage.getItem("ifDetailsGoblack")) {
			Toast.loading("", 2);
			//userHelper.getUserIdAndToken(); //先提前获取用户信息
			setTimeout(() => {
				this.requestTypeList();
			}, 100)
		}
	}
	requestTypeList() { //获取数据
		let params = {
			userId: userHelper.getUserIdAndToken().userId,
			token: userHelper.getUserIdAndToken().token,
			actId: this.props.params.actId,
			lng: this.state.LngEnd,
			lat: this.state.LatEnd,
			shopId: this.props.params.shopId,
		}

		this.props.dispatch(getServiceEventDetail(params, (result) => {
			// console.log(result)
			// alert(' 拿到预约数据='+result.actStatus);
			this.setState({
				details: result,
			})
		}));
	}

	showConfirmDPhone(p) {  //手机拨打电话
		let phone = p.replace(/\s+/g, "").split('/')[0];
		const alert = Modal.alert;
		const alertInstance = alert('联系商家', '是否联系商家?', [
			{ text: '取消', style: 'default' },
			{
				text: '确定', onPress: () => {
					window.location.href = "tel:"+phone;
				}, style: { fontWeight: 'bold' }
			},
		]);
	}

	toOrder() {
		let userInfo = userHelper.getUserIdAndToken();
		setTimeout(() => {
			userInfo = userHelper.getUserIdAndToken();
			if (this.state.details.actStatus == 0) {//要预约
				let userInfo = userHelper.getUserIdAndToken();
				if (!userInfo.userId || !userInfo.token) {
					const alert = Modal.alert;
					const alertInstance = alert('登陆预约', '需要登陆才能预约', [
						{ text: '取消', style: 'default' },
						{
							text: '确定', onPress: () => {
								setTimeout(()=>{
									userHelper.Login(); //先登录
									localStorage.setItem("ifDetailsGoblack", '1');
									return false;  // 没有登陆，返回
								},250)
							}, style: { fontWeight: 'bold' }
						},
					]);

				} else {
					localStorage.removeItem("ifDetailsGoblack");
					this.toUrl(); //如果已经登陆调到填写资料页
				}
			} else if (this.state.details.actStatus == 1) { //取消预约
				const alert = Modal.alert;
				const alertInstance = alert('取消预约', '确定取消预约吗?', [
					{ text: '取消', style: 'default' },
					{
						text: '确定', onPress: () => {
							this.cancel()
						}, style: { fontWeight: 'bold' }
					},
				]);
			}
		}, 150)
	}

	toBack() {  //商户中心图片返回到门店列表
		let shopId_to;
		if (this.props.params.shopId) {
			shopId_to = this.props.params.shopId;
		} else {
			shopId_to = this.state.details.shopId;
		}
		this.context.router.push('details/' + shopId_to);
	}

	toUrl() {//跳转
		// 'meik/'+this.props.params.actId+'/'+this.props.params.shopId
		let shopId;
		if (this.props.params.shopId) {
			shopId = this.props.params.shopId;
		} else {
			shopId = this.state.details.shopId;
		}
		this.context.router.push('meik/' + this.props.params.actId + '/' + shopId);
		localStorage.removeItem("ifDetailsGoblack");
	}

	cancel() {   //取消预约
		let consumptionId;
		if (this.props.params.shopId) {
			consumptionId = this.props.params.shopId;
		} else {
			consumptionId = this.state.details.shopId;
		}
		let getData = {
			userId: userHelper.getUserIdAndToken().userId,
			shopId: consumptionId,
			actId: this.props.params.actId,
		}
		CancelkService.attend(getData).then(data => {
			Toast.hide();
			Toast.loading("", 0)
			console.log(data)
			if (data.code == 1000) {
				Toast.hide();
				this.requestTypeList();
			} else {
				Toast.info(data.msg);
			}
		})
	}

	componentWillReceiveProps(nextProps) {

	}

	///解决打开高德地图返回为空的问题
	openURL(g, t, n) {
		this.context.router.push(`gould/${g}/${t}/${n}`);
	}

	render() {
		let data = this.state.details;
		let distance;
		if (data.shopDistance < 1000) {
			distance = data.shopDistance + "m"
		} else if (data.shopDistance > 1000) {
			distance = (Math.round(data.shopDistance / 100) / 10).toFixed(1) + "km"
		}

		return (
			<div className={style.eventDetails}>
				<PageBar title='活动详情'/>
				<div className='imgs36'>
					<img className='img' src={data.actPic} />
				</div>
				<div className={style.company}>
					<p>{data.actName}</p>
				</div>
				<div style={{ height: '.24rem' }}></div>

				<div className={style.eventDetails}>
					<div className={style.detailsBox}>
						<p className={style.title}><span>商户信息</span><img src="./images/icon-phone.png" alt="" onClick={() => { this.showConfirmDPhone(data.shopTel) }} /></p>
					</div>
				</div>

				<div className={style.list_box}>
					<div className={style.list}>
						<div className={style.explain_img}>
							<div className='imgs52 mb30'>
								<img className='img' src={data.shopPic} alt={data.actName} onClick={() => { this.toBack() }} />
							</div>
						</div>
						<div className={style.explain_text} onClick={() => { this.openURL(data.shopLng, data.shopLat, data.shopName) }}>
							{/*<a href={`https://m.amap.com/navi/?start=${parseFloat(localStorage.getItem('Lng'))},${parseFloat(localStorage.getItem('Lat'))}&dest=${parseFloat(data.shopLng)},${parseFloat(data.shopLat)}&zoom=13&destName=${data.shopName}&naviBy=car&key=68fcbb9b3fd45f9e9207b2106fd6839b`}*/}
							{/*>*/}
							<p ><span className="text-overflow-one">{data.shopName}</span></p>
							<p className="text-overflow-one"><img src="./images/map.png" alt="" /><span>{data.shopAddress}</span></p>
							<p>{distance}</p>
							{/*</a>*/}
						</div>
						<img className={style.icon_enter} src="./images/icon_enter.png" alt="" />
					</div>

				</div>

				<div className={style.eventDetails}>
					<div className={style.detailsBox}>
						<p className={style.title}><span>活动介绍</span></p>
						<div className={style.explain}>
							<p className='mb10'><span>活动内容：</span><span dangerouslySetInnerHTML={{ __html: data.actContent }}></span></p>
							<p><span>活动时间：</span><span dangerouslySetInnerHTML={{ __html: data.actTime }}></span></p>
							{
								data.actDateLimit ? <p><span>日期限制：</span><span>{data.actDateLimit}</span></p> : ''
							}

							{/*<p><span dangerouslySetInnerHTML={{__html: data.actDetail}}></span></p>*/}
							{/*<div dangerouslySetInnerHTML={{ __html: data.shopInfo }}></div>*/}
						</div>
					</div>
				</div>

				<div style={{ height: '.24rem' }}></div>

				{data.actNotice && data.actNotice.length ?
					<div>
						<div className={style.eventDetails}>
							<div className={style.detailsBox}>
								<p className={style.title}><span>预约须知</span></p>
								<ul>
									{
										data.actNotice.map((e, i) => {
											return <li key={i}>{e}</li>
										})
									}
								</ul>
							</div>
						</div>
						<div style={{ height: '72px', background: '#fff', }}></div>
					</div>
					:
					<div style={{ height: '72px', }}></div>
				}

				<div className={style.btn_box}>
					<Button className={style.btn} onClick={() => this.toOrder()}>
						{
							data.actStatus == 0 ? '立即预约' : data.actStatus == 1 ? '取消预约' : ''
						}
					</Button>
				</div>
			</div>
		)
	}
}

EventDetails.contextTypes = {
	router: React.PropTypes.object.isRequired
}

EventDetails.defaultProps = {
	defaultShopList: [{
		actId: '',
		actDesc: '...',
		actPic: '',
		actType: '-1',
	}]

};
export default connect()(EventDetails)