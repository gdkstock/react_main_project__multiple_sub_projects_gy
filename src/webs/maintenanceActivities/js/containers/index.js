export { default as App } from './App'

// 首页
export { default as Home } from './Home'
//export { default as List } from './List' // 门店列表页面

//门店列表
export { default as StoreList } from './storeList'

 // 门店详情
export { default as StoreDetails } from './storeDetails'

//活动详情
export { default as EventDetails } from './eventDetails'

//活动详情
export { default as Meik } from './meik'

export { default as MyAppointment } from './myAppointment' // 我的预约

export {default as Gould } from './gould' //高德地图

// 404
export { default as NotFoundPage } from './404.js'

