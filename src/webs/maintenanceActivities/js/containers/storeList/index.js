/**
 * zhangqiang 2017.04.13
 * APP-banner-保养：商家列表
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Common from '../../utils/common'

// 加载样式
//import style from './index.scss'

// 加载依赖
//import LoadingMore from '../../../../../js/components/common/LoadingMore' //公用组件 加载更多
import ListGroup from '../../components/StoreList'


// 加载actions
import { getStoreList } from '../../actions/storeList'
import PageBar from '../../components/pageBer/pageBar'

// 加载组件
import { Toast } from 'antd-mobile'

// 加载APP封装:实现标题，右上角预约
//import common from '../../utils/common'

class StoreList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			//page: 1, //翻页数

			pic: '', //顶部广告图,商家信息
			tlt: '',
			//oadingStatus: 'block', //分页是否到底
			lng: '', //经度
			lat: '', //纬度
			shopList: this.props.defaultShopList, //商家列表
		};
		this.addData = this.addData.bind(this)
	}
	/**
	 * 生命周期
	 */
	componentWillMount() {
		//在完成首次渲染之前调用，此时仍可以修改组件的state
		Common.setTitleText('门店列表');  // 改标题
	}

	componentDidMount() {
		//真实的DOM被渲染出来后调用
		Common.setTitleText('门店列表');  // 改标题
		// 获得定位并返回
		// window.cx580.jsApi.call({
		// 	"commandId": "",
		// 	"command": "getSymbol",
		// 	"data": {
		// 		"lng": "",
		// 		"lat": "",
		// 	}
		// },
		// 	function (result) {
		// 		localStorage.setItem('Lng', result.data.lng);
		// 		localStorage.setItem('Lat', result.data.lat);
		// 		that.addData(localStorage.getItem('Lng'), localStorage.getItem('Lat'));
		// 	});
	//	alert('test:' + localStorage.getItem('Lng') + ' ,' + localStorage.getItem('Lat'));

		//this.getLocation(); // 定位
		let Lng = parseFloat(localStorage.getItem('Lng'));
		let Lat = parseFloat(localStorage.getItem('Lat'));
		this.addData(Lng, Lat);
	}

	/**
	 * 事件
	 */
	// 请求：通过活动ID、经度、纬度数据，拿到门店列表数据
	addData(lng, lat) {
			//alert(this.props.params.actId + ' , ' + lng + ' , ' + lat)
		this.props.dispatch(getStoreList({
			actId: this.props.params.actId,
			lng: lng,  //经度
			lat: lat,  //纬度 
		}, (result) => {
			console.log('+++++++通过活动ID、经度、纬度数据,', result)
			this.setState({
				LngEnd: lng,
				LatEnd: lat,
				pic: result.actPic, //banner
				actId: result.actId, // id
				tlt: result.actName,
				shopList: result.shopList, // 门店列表
			})
		}));
		//	that.setState({ Lng: data.position.getLng(), Lat: data.position.getLat() });
	}
	//目前先注释 用做上拉加载更多
	/*addData(page){
		if (!page) {page = 1;}

		let newPage = ++this.state.page;
		this.setState({page: newPage});

		console.log(this.state.page)

		let dataList = this.state.dataList;

		///此处  ajax
		this.props.list.forEach((e,i)=>{
			dataList.push(e)
		})
	//	console.log(dataList.length)
		this.setState({
			dataList:dataList,
		})

		if(dataList.length == 'maxCount'){ //接口应该能拿到最大列表的长度
			this.setState({
				list: dataList,
				loadingStatus: 'none',
			})
		}
	}*/

	toUrl(url) {//跳转
		this.context.router.push(url);
	}


	//高德地图定位
	/*getLocation() {
		//Toast.loading('请稍等', 20);
		let that = this;
		let map, geolocation;
		//加载地图，调用浏览器定位服务
		map = new AMap.Map('container', {
			resizeEnable: true
		});
		map.plugin('AMap.Geolocation', function () {
			geolocation = new AMap.Geolocation({
				enableHighAccuracy: true,//是否使用高精度定位，默认:true
				timeout: 10000,          //超过10秒后停止定位，默认：无穷大
				buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
				zoomToAccuracy: true,      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
				buttonPosition: 'RB'
			});
			map.addControl(geolocation);
			geolocation.getCurrentPosition();
			AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
			AMap.event.addListener(geolocation, 'error', onError);      //返回定位出错信息
			// 解析定位结果

			function onComplete(data) {
				localStorage.setItem('Lng', data.position.getLng());
				localStorage.setItem('Lat', data.position.getLat());
				//	alert('+++onComplete='+data.position.getLng());
				console.log('++++++++++++', data.position.getLng());
				that.addData(data.position.getLng(), data.position.getLat());
			}

			//解析定位错误信息
			function onError(data) {
				Toast.info('定位失败', 2); // 实际要删除
				that.addData(localStorage.getItem('Lng'), localStorage.getItem('Lat'));
				console.log('定位失败')
			}
		}.bind(this));
	}
	*/

	/**
	 * 组件
	 */
	render() {

		return (
			<div>
				<PageBar title='门店列表'/>
				<div className='imgs36 mb30'>
					{this.state.pic ? <img className='img' src={this.state.pic} alt={this.state.tlt} onClick={() => { this.toUrl('eventDetails/' + this.state.actId) }} /> : ''}
					{/*轮播图没有shopId*/}
				</div>

				<div className='bg-white'>
					{
						this.state.shopList.map((item, i) => {
							return <ListGroup
								key={i}
								index={i}
								data={item}
								type='list'
								onClick={() => this.toUrl('details/' + item.shopId)}
							/>
						})
					}

				</div>
				{/*	<div>

					<LoadingMore loadingMore={()=> this.addData()}/>

				</div>*/}
			</div>
		)
	}
}

StoreList.contextTypes = {
	router: React.PropTypes.object.isRequired
}

StoreList.defaultProps = {
	// 门店列表默认值
	defaultShopList: [{
		shopDesc: '...',
		shopDistance: '?',
		shopId: '',
		shopName: '...',
		shopPic: '',
		shopType: '-1',
	}]
};

function mapStateToProps(state) {
	return Object.assign({}, state.homeInfo);
}

export default connect(mapStateToProps)(StoreList)