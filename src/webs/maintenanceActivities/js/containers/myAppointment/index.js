/**
 * zhangqiang 2017.04.13
 * APP-banner-保养：预约列表
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'

// 加载样式
import style from './index.scss'
import homestyle from '../Home/index.scss'


// 加载actions
import { getServiceInfo } from '../../actions/homeAction'
import { getOrderInfo } from '../../actions/orderAction'

// APP
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'
import PageBar from '../../components/pageBer/pageBar'


class MyAppointment extends Component {
	constructor(props) {
		super(props);

		this.state = {
			type: 0,
			storeList: this.props.storeList,
			orderList: [],
			actSeriesId:'',
		};
		this.codes = this.props.params.id // 用于无预约的下面商家列表
	}

	/**
	 * 生命周期
	 */
	componentWillMount() {
		//在完成首次渲染之前调用，此时仍可以修改组件的state
		setTimeout(() => {
			common.setTitleText('我的预约');  // 改标题
		}, 100);

	}

	componentDidMount() {
		//真实的DOM被渲染出来后调用
		//重新回到APP视图
		this.getByCode(this.codes); // 无预约页面的商家列表信息

		setTimeout(() => {
			this.getByUserId(localStorage.getItem('userId'), localStorage.getItem('token')); // 预约列表信息
		}, 100);
	}
	componentWillUnmount() {
		//	组件被移除之前被调用，可以用于做一些清理工作
		this.setState({
			type: 0,
			orderList: [],
			actSeriesId:'',
		});
	}

	/**
	 * 事件
	 */

	// 请求：通过系列编码，得到商家服务相关数据
	getByCode(codes) {
		// 发出请求
		this.props.dispatch(getServiceInfo({
			code: codes,
		}, (result) => {
			// 处理请求得到的数据
			console.log('---------通过系列编码，得到商家服务相关数据', result)
			// 需要判断数据，拿到数据，并且为空，才赋值
			let that = this;
			let timer = setTimeout(function () {
				if (that.state.type == 2) {
					that.setState({
						storeList: result.actList,
						orderList: [],
						actSeriesId:result.actSeriesId,
					});
					clearTimeout(timer);
				}
			}, 300);


		}));
	}
	// 请求：判断是否登陆，得到用户信息
	// ifLoad() {
	// 	//发出请求
	// 	let userInfo = userHelper.getUserIdAndToken();
	// 	if (!userInfo.userId || !userInfo.token) {
	// 		userHelper.Login(); //先登录
	// 	 	return false;  // 没有登陆，返回
	// 	}
	// 	this.getByUserId(localStorage.getItem('userId'), localStorage.getItem('token')); // 预约列表信息
	// }
	// 请求：登陆后，得到预约信息
	getByUserId(userId, token) {
		// 发出请求
		console.log('---------getByUserId', userId, token)
		this.props.dispatch(getOrderInfo({
			userId: userId,
			token: token,
			// userId: 'CXY_56E0A990245D4B1BA38AFD14A16EB014',
			// token: '3E191F5B20EEF6E0B4ECD51B6651A539',
		}, (result) => {
			// 处理请求得到的数据
			console.log('---------得到预约信息', result, result.length ? true : false)
			let type = result.length ? 1 : 2;
			// let type = result.length ? 0 : 1;
			this.setState({
				type: type,
				orderList: result,
			});
		}));
	}
	toUrl(url) {//跳转
		this.context.router.push(url);
	}

	/**
	 * 组件
	 */
	render() {
		return (
			<div>
				{/*无预约列表start*/}
				<PageBar title='我的预约'/>
				{
					this.state.type == 1 ?
						<div className={style.myAppointment_box}>
							{
								this.state.orderList.map((item, i) => {
									return <div className={style.activity_list} key={i} onClick={() => { this.toUrl('eventDetails/' + item.actId + '/' + item.shopId) }}>
										<p className='text-overflow-1'>
											{item.actName}
										</p>
										<p>{item.shopName}</p>
										<p>{item.attendTime}</p>
									</div>
								})
							}
						</div>
						:
						<div className={style.myAppointment}>
							<div className={style.activity_title + ' plr30'}>
								<div className={style.nocontent}>
									<p><img src='./images/icon-i.png' alt='车行易保养活动' /></p>
									<p>{this.state.actSeriesId!=''?'您还没有参与活动哦～':''}</p>
								</div>
								<div className={style.like}>
									<span>猜你喜欢</span>
								</div>
							</div>
							<div className={homestyle.activity_img}>
								{
									this.state.storeList.map(item => {
										return <div className={homestyle.box_img} onClick={() => { this.toUrl('list/' + item.actId) }} key={item.actId}>
											<div className='imgs75'>
												{item.actPic ? <img className='img' src={item.actPic} /> : ''}
												
												<p className={homestyle.title_text + ' font-30 color-white '}>{item.actName}</p>
											</div>
										</div>
									})
								}
							</div>
						</div>
				}
			</div>
		)
	}
}

MyAppointment.defaultProps = {
	storeList: [
		{ actId: '1', actName: '' },
	],
};
MyAppointment.contextTypes = {
	router: React.PropTypes.object.isRequired
}

function mapStateToProps(state) {
	return Object.assign({});
}

export default connect(mapStateToProps)(MyAppointment)