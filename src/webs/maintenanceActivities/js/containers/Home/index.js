/**
 * karin 2017/4/12
 * APP-banner-保养：首页
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal } from 'antd-mobile';

// 加载样式
import style from './index.scss'

// 加载actions
import { getServiceInfo } from '../../actions/homeAction'

// 加载APP封装:实现标题，右上角预约
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'
import PageBar from '../../components/pageBer/pageBar'


class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			actSeriesId: '1',
			actSPic: this.props.actSPic,
			tip: this.props.tip,
			actSPic: this.props.actSPic,
			data: this.props.data,
			btnShow: false,
		}
	}
	/**
	 * 生命周期
	 */
	componentWillMount() {
		//在完成首次渲染之前调用，此时仍可以修改组件的state
		common.setTitleText(' ');  // 改标题
	}
	componentDidMount() {
		//真实的DOM被渲染出来后调用
		common.setTitleText(' ');  // 改标题
		this.getByCode(this.props.codes);
		// 添加右上角的入口
		//common.closeTopRightButton();
		// 获得定位并返回
		setTimeout(()=>{
			window.cx580.jsApi.call({
				"commandId": "",
				"command": "getSymbol",
				"data": {
					"lng": "",
					"lat": "",
				}
			}, function (result) {
				localStorage.setItem('Lng', result.data.lng);
				localStorage.setItem('Lat', result.data.lat);
			});
		},500)

		// 如果是因为我的预约按钮登陆后返回，则跳转到我的预约
		//重新回到APP视图
		window.onShowReact = () => this.update();
	}

	/**
	 * 事件
	 */
	// 请求：通过系列编码，得到商家服务相关数据
	getByCode(codes) {
		// 发出请求
		this.props.dispatch(getServiceInfo({
			code: codes,
		}, (result) => {
			// 处理请求得到的数据
			console.log('---------通过系列编码，得到商家服务相关数据', result)
			this.setState({
				actSeriesId: result.actSeriesId,
				actSPic: result.actSeriesPic,
				tip: result.actSeriesIntro,
				data: result.actList,
				actName: result.actSeriesName,
				btnShow:true,
			});

			common.setTitleText(result.actSeriesName);  // 改标题
		}));
	}
	// 是否从登陆回来
	update() {
		let userInfo = userHelper.getUserIdAndToken();
		if (localStorage.getItem("upLoginState") && localStorage.getItem("ifHomeGotoMyAppointment")) {
			//userHelper.getUserIdAndToken(); //先提前获取用户信息
			//要有登陆信息，才能跳转到我的预约
			setTimeout(() => {
				if (userInfo.userId && userInfo.token) {
					this.toUrl('order/' + this.props.codes);
				}
			}, 100);

		}
	}
	//预约入口
	toOrder(url) {
		// 判断登陆
		let userInfo = userHelper.getUserIdAndToken();

		setTimeout(() => {
			userInfo = userHelper.getUserIdAndToken();
			if (!userInfo.userId || !userInfo.token) {
				const alert = Modal.alert;
				const alertInstance = alert('登陆预约', '需要登陆才能预约', [
					{ text: '取消', style: 'default' },
					{
						text: '确定', onPress: (e) => {
							setTimeout(() => {
								userHelper.Login(); //先登录
								localStorage.setItem("ifDetailsGoblack", '1');
								return false;  // 没有登陆，返回
							}, 250)
						}, style: { fontWeight: 'bold' }
					},
				]);
			} else {
				localStorage.removeItem("ifDetailsGoblack");
				this.toUrl(url); //如果已经登陆调到填写资料页
			}
		}, 150);

	}

	toUrl(url) {//跳转
		this.context.router.push(url);
		localStorage.removeItem("ifHomeGotoMyAppointment");
	}
	/**
	 * 顶部标题-右上角按钮，5.2.5以上版本
	 */

	// // 右上角点击事件
	// TopRightButton() {
	// 	alert('点击了右上角2-1');
	// }
	// news_fresh() {
	// 	document.location.reload(true);
	// }

	/**
	 * 组件
	 */
	render() {

		return (
			<div className={style.home_box}>
				<PageBar title={this.state.actName} isHome={true}/>
				<div className='imgs52'>
					<img className='img' src={this.state.actSPic} />
				</div>

				<div className={style.activity_img}>

					{
						this.state.data.map(item => {
							return <div className={style.box_img} onClick={() => { this.toUrl('list/' + item.actId) }} key={item.actId}>
								<div className='imgs75'>
									{item.actPic ? <img className='img' src={item.actPic} /> : ''}
									{/*<p className={style.title_introduce}>{item.actDesc}</p>*/}
									<p className={style.title_text + ' font-30 color-white '}>{item.actName}</p>
								</div>
							</div>
						})
					}
					<div className={style.activity_img_circle}></div>
				</div>
				<div className={this.state.btnShow ? '' : 'hide'}>
					<div className={style.btn + ' font-36'}>
						<div onClick={() => { this.toOrder('order/' + this.props.codes) }}><span>我的预约</span><img src='./images/icon-arrow.png' /></div>
					</div>

					<div className={style.foot_img}>
						<div className='imgs12'>
							<img className='img' src='./images/main.png' />
						</div>
						<div className={style.title_text + ' font-26 color-white '}>
							<div dangerouslySetInnerHTML={{ __html: this.state.tip }}></div>
						</div>
					</div>
				
				</div>
					<div className={style.home_bg}></div>
			</div>
		)
	}
}

//使用context


Home.defaultProps = {
	codes: 'baoyao2017',
	data: [
		{ actId: '1', actName: '' },
		{ actId: '2', actName: '' },
		{ actId: '3', actName: '' },
		{ actId: '4', actName: '' },
	],
};

//使用context
Home.contextTypes = {
	router: React.PropTypes.object.isRequired
}
function mapStateToProps(state) {
	return Object.assign({}, state.homeInfo);
}

export default connect(mapStateToProps)(Home)