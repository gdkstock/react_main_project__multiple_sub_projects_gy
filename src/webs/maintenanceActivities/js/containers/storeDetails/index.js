import React, { Component } from 'react'
import { Modal } from 'antd-mobile';
import { connect } from 'react-redux';
import Common from '../../utils/common'
import { getServiceDetail } from '../../actions/storeDetails'
//import StoreDetailsList from '../../components/StoreDetails' //门店列表公用组件
import style from './index.scss';
import PageBar from '../../components/pageBer/pageBar'
import jsApi from '../../utils/cx580.jsApi' //获取appUserId


// 加载依赖
//import LoadingMore from '../../../../../js/components/common/LoadingMore' //公用组件 加载更多
import ListGroup from '../../components/StoreList'


class StoreDetails extends Component {
	constructor(props) {
		super(props);

		this.state = {
			details: '',
			LngEnd: '',
			LatEnd: '',
			shopList: this.props.defaultShopList, //商家列表
		};
	}

	componentWillMount() {
		Common.setTitleText('门店详情');  // 改标题

		let Lng = parseFloat(localStorage.getItem('Lng'));
		let Lat = parseFloat(localStorage.getItem('Lat'));
		this.setState({ LngEnd: Lng, LatEnd: Lat });

     	this.getData()
	}

	componentDidMount() {
		Common.setTitleText('门店详情');  // 改标题

	}

	componentWillReceiveProps(nextProps) {

	}

	/**
	 * 事件
	 */

	getData() { //获取数据
		let getData = {
			shopId: this.props.params.shopId,
		}
		this.props.dispatch(getServiceDetail(getData, (result) => {
			console.log('*+++++++++++++ 具体门店，活动列表=', result)
			this.setState({
				details: result,
				shopList: result.actList,
			})
		}));
	}

	showConfirmDPhone(p) {  //手机拨打电话
		let phone = p.replace(/\s+/g, "").split('/')[0];
		const alert = Modal.alert;
		const alertInstance = alert('联系商家', '是否联系商家?', [
			{ text: '取消', style: 'default' },
			{
				text: '确定', onPress: () => {
					window.location.href = "tel://" + phone;
				}, style: { fontWeight: 'bold' }
			},
		]);
	}

	toUrl(url) {//跳转
		this.context.router.push(url);
	}
	/**
	 * 组件
	 */

	///解决打开高德地图返回为空的问题
	openURL(g,t,n){
		this.context.router.push(`gould/${g}/${t}/${n}`);
	}

	render() {
		let data = this.state.details;

		return (
			<div className={style.storeDetails}>
				<PageBar title='门店详情' />
				<div className='imgs36'>
					<img className='img' src={data.shopPic} />
				</div>
				<div className={style.company}>
					<p>{data.shopName}</p>
				</div>
				<div style={{ height: '.24rem' }}></div>

				<div className={style.facility}>

					<p className={style.use} onClick={()=>this.openURL(data.shopLng,data.shopLat,data.shopName)}>
						{/*<a className={style.item} href={`https://m.amap.com/navi/?start=${parseFloat(localStorage.getItem('Lng'))},${parseFloat(localStorage.getItem('Lat'))}&dest=${parseFloat(data.shopLng)},${parseFloat(data.shopLat)}&zoom=13&destName=${data.shopName}&naviBy=car&key=68fcbb9b3fd45f9e9207b2106fd6839b`}*/}
						{/*>*/}
							<img className={style.map_img} src='./images/map.png' alt="" />
							<span className="text-overflow-one">{data.shopAddress}</span>
							<img className={style.icon_rg} src="./images/icon_enter.png" alt="" />
						{/*</a>*/}
					</p>
					<p className={style.use} onClick={() => { this.showConfirmDPhone(data.shopTel) }}>
						<img className={style.map_img} src='./images/phone.png' alt="" />
						<span className="text-overflow-one">{data.shopTel}</span>
						<img className={style.icon_rg} src="./images/icon_enter.png" alt="" />
					</p>
				</div>
				<div style={{ height: '.24rem' }}></div>

				<div className={style.eventDetails}>
					<div className={style.detailsBox}>
						<p className={style.title}><span>商家活动</span></p>
						{
							this.state.shopList.map((item, i) => {
								return <ListGroup
									key={i}
									index={i}
									data={item}
									type='details'
									onClick={() => this.toUrl('eventDetails/'+item.actId+'/'+data.shopId)}

									//onClick={() => this.toUrl('eventDetails/?actId='+item.actId+'&shopId='+data.shopId)}
								/>
							})
						}
					</div>
				</div>
				<div style={{ height: '.24rem' }}></div>
				{
					data.shopInfo?	<div className={style.eventDetails}>
							<div className={style.detailsBox}>
								<p className={style.title}><span>门店信息</span></p>
								<div className={style.information}>
									<span>营业时间：</span><span dangerouslySetInnerHTML={{__html: data.shopInfo}}></span>
								</div>
							</div>
						</div>:
						''
				}

			</div>
		)
	}
}
StoreDetails.contextTypes = {
	router: React.PropTypes.object.isRequired
}
StoreDetails.defaultProps = {
	defaultShopList: [{
		actId: '',
		actDesc: '...',
		actPic: '',
		actType: '-1',
	}]
};
export default connect()(StoreDetails)