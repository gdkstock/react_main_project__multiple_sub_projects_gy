import React, { Component } from 'react'
import { connect } from 'react-redux';
import PageBar from '../../components/pageBer/pageBar'
import style from './index.scss';

class Gould extends Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	render() {

		return (
			<div className={style.gould}>
				<PageBar title='高德地图'/>
				<iframe height="100%" width="100%" src={`https://m.amap.com/navi/?start=${parseFloat(localStorage.getItem('Lng'))},${parseFloat(localStorage.getItem('Lat'))}&dest=${parseFloat(this.props.params.g)},${parseFloat(this.props.params.t)}&zoom=13&destName=${this.props.params.n}&naviBy=car&key=68fcbb9b3fd45f9e9207b2106fd6839b`}>
				</iframe>
			</div>
		)
	}
}

Gould.contextTypes = {
	router: React.PropTypes.object.isRequired
}

Gould.defaultProps = {

};

export default connect()(Gould)