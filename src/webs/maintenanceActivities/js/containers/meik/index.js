import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { Modal, Button, Toast, InputItem } from 'antd-mobile';
import { connect } from 'react-redux';
import Common from '../../utils/common'
import { createForm } from 'rc-form';
import StoreDetailsList from '../../components/StoreDetails' //门店列表公用组件
import jsApi from '../../utils/cx580.jsApi' //获取appUserId
import userHelper from '../../utils/userHelper'
import MeikService from '../../services/meikService';
import common from '../../utils/common'
import { BindCar, ChooseProvince } from '../../components'
import Demo from '../../../../../js/components/common/carPrefixList/demo'
import PageBar from '../../components/pageBer/pageBar'
import style from './index.scss';

class Meik extends Component {
	constructor(props) {
		super(props);

		this.state = {
			name: '', //姓名
			phone: '', //手机号
			modal1: false,
			modalTitle: '您已提交成功,请等待门店确认预约结果或拨打电话联系门店确认',
			close: () => console.log('点击关闭按钮'), //点击关闭按钮
			isAgree: false, //是否同意协议
			isShow: 'none', //是否显示选择省份
			chooseProvince: '粤', //车辆选择的省份
			back: true,
		};
		this.subMit = this.subMit.bind(this)
		this.isEmojiCharacter = this.isEmojiCharacter.bind(this)
	}

	getChooseProvince(val) {
		console.log(val)
		this.setState({ chooseProvince: val, isShow: 'none' });
	}

	handleCarNoInput(e) {
		var pattern = /([^A-Za-z0-9]|\s)*/g;
		e.target.value = e.target.value.replace(pattern, '').toUpperCase();
		let carNo = e.target.value.toUpperCase();
		this.setState({ carNo: carNo.substring(0,7) })
	}

	componentWillMount() {
		Common.setTitleText('预约到店');  // 改标题
		let Lng = parseFloat(localStorage.getItem('Lng'));
		let Lat = parseFloat(localStorage.getItem('Lat'));
		this.setState({
			LngEnd: Lng,
			LatEnd: Lat
		});
	}

	componentDidMount() {
		this.setState({
			phone: userHelper.getUserIdAndToken().phone,
		})
	}

	onClose = key => () => {
		this.setState({
			[key]: false,
			back: true,
		});
		setTimeout(history.go(-1), 1500)
	}

	/*
	* 不能输入表情验证
	* */
	 isEmojiCharacter(substring) {
		for ( var i = 0; i < substring.length; i++) {
			var hs = substring.charCodeAt(i);
			if (0xd800 <= hs && hs <= 0xdbff) {
				if (substring.length > 1) {
					var ls = substring.charCodeAt(i + 1);
					var uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
					if (0x1d000 <= uc && uc <= 0x1f77f) {
						return true;
					}
				}
			} else if (substring.length > 1) {
				var ls = substring.charCodeAt(i + 1);
				if (ls == 0x20e3) {
					return true;
				}
			} else {
				if (0x2100 <= hs && hs <= 0x27ff) {
					return true;
				} else if (0x2B05 <= hs && hs <= 0x2b07) {
					return true;
				} else if (0x2934 <= hs && hs <= 0x2935) {
					return true;
				} else if (0x3297 <= hs && hs <= 0x3299) {
					return true;
				} else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030
					|| hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b
					|| hs == 0x2b50) {
					return true;
				}
		}
	}
}
	subMit() {
		if (this.state.back) {
			this.props.form.validateFields((error, value) => {
				console.log(error, value);
				let telNum = this.state.phone.replace(/\s+/g, ""),
					reg = /^0?1[3|4|5|7丨8][0-9]\d{8}$/,
					reg_name =/^[a-zA-Z\u4e00-\u9fa5\s]{1,20}$/;

				if (!this.state.name && !this.state.phone) {
					Toast.info('请填写信息!',1)
					return false;
				} else {
					if (this.state.name == '' || !this.state.name) {
						Toast.info('姓名不能为空!',1);
						return false;
					}else if(!reg_name.test(this.state.name)){
						Toast.info('姓名格式错误',1)
						return false;
					}else if(this.isEmojiCharacter(this.state.name)){
						Toast.info('姓名不能包含表情!',1)
						return false;
					}else if (this.state.phone == '' || !this.state.phone) {
						Toast.info('手机号码不能为空!',1);
						return false;
					} else if (!reg.test(telNum)) {
						Toast.info('手机号码格式错误!',1)
						return false;
					} else if (!this.state.carNo) {
						Toast.info('请填写车牌号!',1)
						return false;
					} else if(this.state.carNo.toString().length<6){
						Toast.info('车牌号不能少于6位!',1)
						return false;
					}
				}

				let postData = {
					name: this.state.name.replace(/\s+/g, ""),
					tel: this.state.phone.replace(/\s+/g, ""),
					carnumber: this.state.chooseProvince + '' + this.state.carNo,
					actId: this.props.params.actId,
					shopId: this.props.params.shopId,
					userId: userHelper.getUserIdAndToken().userId,
					token: userHelper.getUserIdAndToken().token,
				}
				Toast.loading('', 2);
				MeikService.attend(postData).then(data => {
					Toast.hide();
					// Toast.loading("",0)
					if (data.code == 1000) {
						this.setState({
							modal1: true,
							back: false,
						});
					} else {
						Toast.info(data.msg);
					}
				})
			})
		}

	}

	componentWillReceiveProps(nextProps) {

	}

	getName(e) {
		this.setState({
			name: e,
		})
	}

	render() {

		const { getFieldProps } = this.props.form;
		let state = this.state;

		return (
			<div className={style.meik}>
				<PageBar title='预约到店'/>
				<div className={style.title}>
					填写预约信息
				</div>
				<div className={style.box}>
					<div className={style.material}>
						<div className={style.input_box}>
							<InputItem
								{...getFieldProps('name') }
							//	clear
								value={this.state.name}
								maxLength="12"
								placeholder="请填写姓名"
								autoFocus
								//onBlur ={(e)=>{this.getName(e)}}
								onChange={(name) => this.setState({name})}
							>姓名</InputItem>
							<span className={style.star}>*</span>
						</div>
						<div className={style.borderB}></div>
						<div className={style.input_box}>
							<InputItem
								//clear
								{...getFieldProps('phone') }
								type="phone"
								placeholder="请填写电话"
								value={this.state.phone}
								onChange={(phone) => this.setState({ phone })}
							//placeholder="186 1234 1234"
							// onBlur={(e)=>{this.getPhone(e)}}
							>联系电话</InputItem>
							<span className={style.star_s}>*</span>
						</div>
						<div className={style.borderB}></div>
					</div>
				</div>

				<BindCar ProVince={state.chooseProvince}
					placeholder="请输入"
					leftItem="车牌号码"
					inputStyle={{ textAlign: 'right', maxWidth: '2.1rem' }}
					changeHandler={::this.handleCarNoInput}
				         getChooseProvince = {::this.getChooseProvince}
				         value={state.carNo}
				/>

				<div className={style.btn_box}>
					<Button className={style.btn} onClick={e => this.subMit()}>
						提交预约
					</Button>
				</div>
				<Modal
					transparent
					maskClosable={false}
					closable={false}
					visible={this.state.modal1}
					title={this.state.modalTitle}
					footer={[{ text: '确定', onPress: () => { this.onClose('modal1')() } }]}
				/>
			</div>
		)
	}
}

Meik.contextTypes = {
	router: React.PropTypes.object.isRequired
}

Meik.defaultProps = {

};
export default createForm()(Meik);