/**
 * zhangqiang 2017.04.13
 */
import {  } from "./actionsTypes"
import storeListService from '../services/storeListService'
import { Toast } from 'antd-mobile'

//通过经纬度获取商家信息
export const getStoreList=(data,callBack)=>{
	return (dispatch, getState) => {
		Toast.hide();
		Toast.loading("", 30, () => Toast.info("网络错误", 2));
		storeListService.getData(data).then(resultData=>{
			Toast.hide();
			if (resultData.code == 1000) {
				if(callBack) {
					callBack(resultData.data);
				}
			}
			else {
				// Toast.info(resultData.msg, 2);
				Toast.info('网络异常', 2);
			}
		})
	}
};
