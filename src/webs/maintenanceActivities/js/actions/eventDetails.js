/**
 * zhangqiang 2017.04.13
 */
import {  } from "./actionsTypes"
import EventDetailsService from '../services/eventDetailsService'
import { Toast } from 'antd-mobile'


export const getServiceEventDetail=(data,callBack)=>{
	return (dispatch, getState) => {
		Toast.hide();
		Toast.loading("", 30, () => Toast.info("网络错误", 2));
		EventDetailsService.actDetail(data).then(resultData=>{
			Toast.hide();
			if (resultData.code == 1000) {
				if(callBack) {
					callBack(resultData.data);
				}
			}
			else {
				// Toast.info(resultData.msg, 2);
				Toast.info('网络异常', 2);
			}
		})
	}
};
