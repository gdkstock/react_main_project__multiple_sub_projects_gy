/**
 * karin 2017.04.14
 * 预约列表
 */
import orderInfoService from '../services/orderInfoService'
import { Toast } from 'antd-mobile'

// 请求 ：登陆后，得到预约列表相关数据
export const getOrderInfo=(data,callBack)=>{
    return (dispatch, getState) => {
        Toast.hide();
        Toast.loading("",30, () => Toast.info("网络错误", 2));
        orderInfoService.getInfo(data).then(resultData=>{
        console.log('+++++++ ：登陆后，得到预约列表相关数据 = ',data,resultData);
            Toast.hide();
            if (resultData.code == 1000) {
                if(callBack) {
                    callBack(resultData.data);
                }
            }
            else {
                // Toast.info(resultData.msg, 2);
                 Toast.info('网络异常', 2);
            }
        })
    }
};

