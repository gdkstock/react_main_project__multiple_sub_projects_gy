/**
 * karin 2017.04.12
 */
import { SERVICE_INFO } from "./actionsTypes"
import homeInfoService from '../services/homeInfoService'
import { Toast } from 'antd-mobile'

export const homeInfo = data => ({ type: SERVICE_INFO, data: data });//首页商家服务列表

// 请求 ：通过系列编码，得到首页商家服务列表相关数据
export const getServiceInfo=(data,callBack)=>{
    return (dispatch, getState) => {
        Toast.hide();
        Toast.loading("", 30, () => Toast.info("网络错误", 2));
        homeInfoService.getInfo(data).then(resultData=>{
        //  console.log('+++++++ ：通过系列编码，得到首页商家服务列表相关数据 = ',data,resultData);
            Toast.hide();
            if (resultData.code == 1000) {
                if(callBack) {
                    callBack(resultData.data);
                }
            }
            else {
                // Toast.info(resultData.msg, 2);
                 Toast.info('网络异常', 2);
            }
        })
    }
};

