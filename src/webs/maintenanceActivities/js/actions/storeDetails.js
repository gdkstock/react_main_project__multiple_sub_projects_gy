/**
 * zhangqiang 2017.04.13
 */
import {  } from "./actionsTypes"
import StoreDetailsService from '../services/storeDetailsService'
import { Toast } from 'antd-mobile'


export const getServiceDetail=(data,callBack)=>{
	return (dispatch, getState) => {
		Toast.hide();
		Toast.loading("", 30, () => Toast.info("网络错误", 2));
		StoreDetailsService.shopDetail(data).then(resultData=>{
			Toast.hide();
			if (resultData.code == 1000) {
				if(callBack) {
					callBack(resultData.data);
				}
			}
			else {
				// Toast.info(resultData.msg, 2);
				Toast.info('网络异常', 2);
			}
		})
	}
};
