import apiHelper from '../services/apiHelper';

/**
 * 商家列表
 */
class CancelkService {

	/**
	 * 取消预约接口
	 */
	attend(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}activity/attendCancel`, // test 测试:实际是 url: urls
			data: {
				method: 'get',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}
}

export default new CancelkService()