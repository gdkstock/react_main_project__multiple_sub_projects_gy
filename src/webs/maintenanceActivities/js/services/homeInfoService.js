/**
 * karin 2017.04.12
 */
import apiHelper from './apiHelper'
//import Violation from '../utils/violation' // 这个是跳转违章网址，判断环境的JS

class homeInfoService {
    //请求 ：通过订单号，得到车辆相关数据
    getInfo(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}activity/welcome`, // 
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new homeInfoService();