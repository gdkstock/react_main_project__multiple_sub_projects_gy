import apiHelper from '../services/apiHelper';

/**
 * 商家列表
 */
class storeListService {
    /**
     * 门店列表
     */
    getData(data) {
        let requestParam = {
	           url: `${apiHelper.baseApiUrl}activity/shopList`, // test 测试:实际是 url: urls
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new storeListService()