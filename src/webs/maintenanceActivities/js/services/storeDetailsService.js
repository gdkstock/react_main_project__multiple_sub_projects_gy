import apiHelper from '../services/apiHelper';

/**
 * 商家列表
 */
class StoreDetailsService {

	/**
	 * 门店详情
	 */
	shopDetail(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}activity/shopDetail`, // test 测试:实际是 url: urls
			data: {
				method: 'get',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}
}

export default new StoreDetailsService()