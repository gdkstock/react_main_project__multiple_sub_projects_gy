/**
 * karin 2017.04.13
 * 预约列表接口说明
 */
import apiHelper from './apiHelper'
//import Violation from '../utils/violation' // 这个是跳转违章网址，判断环境的JS

class orderInfoService {
    //请求 ：通过订单号，得到门店列表接口说明
    getInfo(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}activity/attendList`, //
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new orderInfoService();