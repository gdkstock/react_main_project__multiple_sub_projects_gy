import apiHelper from '../services/apiHelper';

/**
 * 商家列表
 */
class MeikService {

	/**
	 * 填写资料
	 */
	attend(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}activity/attend`, // test 测试:实际是 url: urls
			data: {
				method: 'post',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}
}

export default new MeikService()