import apiHelper from '../services/apiHelper';

/**
 * 商家列表
 */
class EventDetailsService {

	/**
	 * 门店详情
	 */
	actDetail(data) {
		let requestParam = {
			url: `${apiHelper.baseApiUrl}activity/actDetail`, // test 测试:实际是 url: urls
			data: {
				method: 'get',
				body: data
			}
		};
		return apiHelper.fetch(requestParam);
	}
}

export default new EventDetailsService()