import common from '../utils/common';
import userHelper from '../utils/userHelper';

require('isomorphic-fetch');

class ApiHelper {

    //production = window.location.href.indexOf('https://comments.cx580.com/') > -1 ? true : false; //是否为生产环境

    /**
     * 切换生产或测试环境
     * "http://192.168.1.235:18066/"; //测试
     * "https://comments.cx580.com/";//生产
     */
   // baseApiUrl = this.production ? "https://comments.cx580.com/" : "http://192.168.1.235:18066/";
   baseApiUrl="https://comments.cx580.com/" ;


    /**
     * 获取 HTTP 头
     */
    _getHeaders() {
        return {
            "Accept": "*/*",
            // "authorization": "Basic Y3h5aW06Y3g1ODBjeDU4MGN4NTgws",
        }
    }

    /**
     * 封装fetch
     */
    fetch(requestParam) {
        let setTimeNum = 250; //默认延迟250毫秒请求数据 兼容APP处理

        let UserIdAndToken = userHelper.getUserIdAndToken(); //先去获取用户数据，避免数据没有实时返回
        if (UserIdAndToken.alipayOpenId) {
            setTimeNum = 10; //支付宝平台 不需要延迟请求数据
        }

        let promise = new Promise((resolve, reject) => {
            //用于支付宝 不需要延迟请求
            setTimeout(() => {
                // console.log("requestParam", requestParam);
                requestParam.data.method = requestParam.data.method || "get";
                requestParam.data.headers = requestParam.data.headers || {};
                Object.assign(requestParam.data.headers, this._getHeaders());
                if (requestParam.data.method.trim().toLowerCase() == "post") {
                    requestParam.data.headers["Content-Type"] = "application/x-www-form-urlencoded";
                }
                requestParam.data.body = requestParam.data.body || {};
                //设置用户
                if (UserIdAndToken.alipayOpenId) {
                    //支付宝
                    if (!requestParam.data.body.openId) {
                        requestParam.data.body["openId"] = UserIdAndToken.alipayOpenId;
                    }
                } else {
                    //APP 平台
                    UserIdAndToken = userHelper.getUserIdAndToken();
                    if (!requestParam.data.body.userId) {
                        requestParam.data.body["userId"] = UserIdAndToken.userId;
                    }
                    if (!requestParam.data.body.token) {
                        requestParam.data.body["token"] = UserIdAndToken.token;
                    }
                    //保存用户信息及城市信息(用于判断是否需要刷新页面重新获取信息)
                    localStorage.setItem("g_userId", UserIdAndToken.userId);
                    localStorage.setItem("g_userToken", UserIdAndToken.token);
                    localStorage.setItem("g_city", UserIdAndToken.city);
                }

                requestParam.data.body = common.toQueryString(requestParam.data.body);


                requestParam.data.mode = "cors";
                if (requestParam.data.method.trim().toLowerCase() == "get") {
                    var request = new Request(requestParam.url + '?' + requestParam.data.body); //get请求不能有body,否则会报错
                } else {
                    var request = new Request(requestParam.url, requestParam.data);
                }
                // console.debug("request", request);
                let result = window.fetch(request, { headers: requestParam.data.headers })
                    .then(function (response) {
                        let resp = response.json();
                        resp.then(function (data) {
                            if (data.code == "90015") {
                                userHelper.Login();
                            }
                        });
                        return resp;
                    })
                    .catch(function (e) {
                        console.error("fetch 请求出错了");
                        console.dir(e);
                        throw e;
                    });
                console.log("result", result);
                resolve(result)
            }, setTimeNum);

            // 网络超时
            setTimeout(() => reject('网络错误'), 30000);
        });
        return promise;
    }
}

// 实例化后再导出
export default new ApiHelper()