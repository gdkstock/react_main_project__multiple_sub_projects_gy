import {
  put,
  call,
  take,
  fork
} from 'redux-saga/effects'

import apiHelper from '../services/apiHelper'

import common from '../utils/common'

import {
    REQUEST_DISCOUNT_BANNER,
    RECEIVE_DISCOUNT_BANNER,
    REQUEST_DISCOUNT_LISTTAB,
    RECEIVE_DISCOUNT_LISTTAB,
    REQUEST_DISCOUNT_TYPELIST,
    RECEIVE_DISCOUNT_TYPELIST,
    REQUEST_DISCOUNT_BARGAINSLIST,
    RECEIVE_DISCOUNT_BARGAINSLIST,
    IS_LOADING,
    HASE_MORE,
    NO_MORE,
    NETWORK_ERROR
} from '../actions/actionsTypes'

/**
 * banner部分的异步
 */

//获取banner数据的方法
function fetchDiscountBannerApi(param) {
    let requestParam = {
        url: `${apiHelper.baseApiUrl}cxy/advertisement/getBanner`,
        data: {
            method: 'post',
            body:param
        }
    };
    return apiHelper.fetch(requestParam);
}

//获取banner数据，成功则put一个action
function* fetchDiscountBanner(param) {
    const result = yield call(fetchDiscountBannerApi,param);
    if (result.code == "1000") {
        yield put({ type: RECEIVE_DISCOUNT_BANNER, data: result.data });
    } else {
        // alert(result.msg);
    }
}

//监听与banner的REQUEST并执行获取数据
export function* watchDiscountBanner() {
    while(true){
        let {param} = yield take(REQUEST_DISCOUNT_BANNER);
        yield fork(fetchDiscountBanner,param);
    }
}

/**
 * banner部分的异步结束
 */




/**
 * 商品列表tab的异步
 */

//获取tab数据的方法
function fetchDiscountListTabApi() {
    let requestParam = {
        url: `${apiHelper.baseApiUrl}vehicle/list`,
        data: {
            method: 'get'
        }
    };
    return apiHelper.fetch(requestParam);
}

//获取tab数据，成功则put一个action
function* fetchDiscountListTab() {
    const result = yield call(fetchDiscountListTabApi);
    if (result.code == "1000") {
        yield put({ type: RECEIVE_DISCOUNT_LISTTAB, data: result.data });
    } else {
        // alert(result.msg);
    }
}

//监听tab的REQUEST并执行获取数据
export function* watchDiscountListTab() {
    yield take(REQUEST_DISCOUNT_LISTTAB);
    yield fork(fetchDiscountListTab);
}

/**
 * 商品列表tab的异步结束
 */



/**
 * 商品类型list的异步
 */

//获取typelist的方法
function fetchDiscountTypeListApi(param) {
    let requestParam = {
        url: `${apiHelper.baseApiUrl}cxy/advertisement/getAdColumns`,
        data: {
            method: 'post',
            body:param
        }
    };
    return apiHelper.fetch(requestParam);
}

//获取typelist，成功则put一个action
function* fetchDiscountTypeList(param) {
    const result = yield call(fetchDiscountTypeListApi,param);
    if (result.code == "1000") {
        yield put({ type: RECEIVE_DISCOUNT_TYPELIST, data: result.data });
    } else {
        // alert(result.msg);
    }
}

//监听typelist的REQUEST并执行获取数据
export function* watchDiscountTypeList() {
    while(true){
        let {param} = yield take(REQUEST_DISCOUNT_TYPELIST);
        yield fork(fetchDiscountTypeList,param);
    }
}

/**
 * 商品类型list的异步结束
 */



/**
 * 商品list的异步
 */

function fetchDiscountBargainsListApi(param) {
    let requestParam = {
        url: `${apiHelper.baseApiUrl}cxy/taobaoke/getShopList`,
        data: {
            method: 'post',
            body: param
        }
    };
    return apiHelper.fetch(requestParam);
}

function* fetchDiscountBargainsList(param) {
    //alert("456")
    try{
        //alert("发送请求了")
        const result = yield call(fetchDiscountBargainsListApi, param);
        if (result.code == "1000") {
            yield put({ type: RECEIVE_DISCOUNT_BARGAINSLIST, data: result.data });
            //判断是否有更多的商品数据
            let count=result.data.total*1;
            if(count==0 || count < 10){
                yield put({ type: NO_MORE })
            }else {
                yield put({ type: HASE_MORE })
            }
        } else {
            console.log(result.msg);
        }
    }catch(e){
        yield put({ type: NETWORK_ERROR });
    }
}

export function* watchDiscountBargainsList() {
    while(true) {
        const {param} = yield take(REQUEST_DISCOUNT_BARGAINSLIST);
        yield put({ type:IS_LOADING });
        yield fork(fetchDiscountBargainsList, param);
    }
}

/**
 * 商品List的异步结束
 */