import {
    REQUEST_DISCOUNT_BANNER,
    RECEIVE_DISCOUNT_BANNER,
    REQUEST_DISCOUNT_LISTTAB,
    RECEIVE_DISCOUNT_LISTTAB,
    REQUEST_DISCOUNT_TYPELIST,
    RECEIVE_DISCOUNT_TYPELIST,
    REQUEST_DISCOUNT_BARGAINSLIST,
    RECEIVE_DISCOUNT_BARGAINSLIST,
    IS_LOADING,
    HASE_MORE,
    NO_MORE,
    NETWORK_ERROR
} from '../actions/actionsTypes'

//顶部banner的reducers
const initDiscountBanner = {
    data:[
        {
            "imgUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1491454333714&di=d62634f09b5e9766fee6996352907fb6&imgtype=0&src=http%3A%2F%2Fpic.58pic.com%2F58pic%2F13%2F18%2F01%2F50q58PICdIM_1024.jpg",
            "jumpUrl":"https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=%E9%A3%8E%E6%99%AF&step_word=&hs=0&pn=71&spn=0&di=149959825840&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=2&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=-1&cs=767135048%2C4151285646&os=218888356%2C2804910670&simid=4291113714%2C855594938&adpicid=0&lpn=0&ln=1988&fr=&fmq=1459502303089_R&fm=&ic=0&s=undefined&se=&sme=&tab=0&width=&height=&face=undefined&ist=&jit=&cg=&bdtype=0&oriquery=&objurl=http%3A%2F%2Fpic.58pic.com%2F58pic%2F13%2F18%2F01%2F50q58PICdIM_1024.jpg&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3Bcbrtv_z%26e3Bv54AzdH3Ff7vwtAzdH3F8n8ba8ca_z%26e3Bip4s&gsm=1000000001e&rpstart=0&rpnum=0",
            "eventId": "hhh"
        },
        {
            "imgUrl": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1491454333714&di=d62634f09b5e9766fee6996352907fb6&imgtype=0&src=http%3A%2F%2Fpic.58pic.com%2F58pic%2F13%2F18%2F01%2F50q58PICdIM_1024.jpg",
            "jumpUrl":"https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=%E9%A3%8E%E6%99%AF&step_word=&hs=0&pn=71&spn=0&di=149959825840&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=2&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=-1&cs=767135048%2C4151285646&os=218888356%2C2804910670&simid=4291113714%2C855594938&adpicid=0&lpn=0&ln=1988&fr=&fmq=1459502303089_R&fm=&ic=0&s=undefined&se=&sme=&tab=0&width=&height=&face=undefined&ist=&jit=&cg=&bdtype=0&oriquery=&objurl=http%3A%2F%2Fpic.58pic.com%2F58pic%2F13%2F18%2F01%2F50q58PICdIM_1024.jpg&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3Bcbrtv_z%26e3Bv54AzdH3Ff7vwtAzdH3F8n8ba8ca_z%26e3Bip4s&gsm=1000000001e&rpstart=0&rpnum=0",
            "eventId": "hhh"
        }
    ]   
}
export const discountBanner = (state = initDiscountBanner, action) => {
    switch(action.type) {
        case RECEIVE_DISCOUNT_BANNER:
            return Object.assign({},state,{
                data: action.data
            })
        default: 
            return state
    }
}

//列表tab的reducers
export const discountListTab = (state = {index:0}, action) => {
    switch(action.type) {
        case RECEIVE_DISCOUNT_LISTTAB:
            return Object.assign({},state,{
                data: action.data
            })
        default: 
            return state
    }
}

//商品类型list的reducers
export const discountTypeList = (state = {}, action) => {
    switch(action.type) {
        case RECEIVE_DISCOUNT_TYPELIST:
            return Object.assign({},state,{
                data: action.data
            })
        default:
            return state
    }
}

//商品list的初始值
let initBargainsList = {
    "HOT": {
        goodsList: []
    },
    "JIUJIU": {
        goodsList: []
    }
}
//商品list的reducers
export const discountBargainsList = (state = initBargainsList, action) => {
    switch(action.type) {
        case REQUEST_DISCOUNT_BARGAINSLIST:
            return Object.assign({},state,{
                param: action.param
            })
        case RECEIVE_DISCOUNT_BARGAINSLIST:
            return Object.assign({},state,{
                [state.param.labelCode]: action.data
            })
        default:
            return state
    }
}

//加载更多状态
const initLoadingMore={
    showMsg: true, //显示文字提醒
    msgType: 0, // 对应msg字段的下标
    msg: ['点击加载更多', '加载中...','END'], //文字提醒
    height: 10, //触发加载数据的高度 单位px
    loadingMore: () => false, //到达底部时，触发的事件
    line:true, //END时,是否添加贯穿文本的横线
}
export const loadingMore = (state = initLoadingMore ,action) => {
    switch(action.type){
        case IS_LOADING:
            return {...state,msgType:1}
        case HASE_MORE:
            return {...state,msgType:0}
        case NO_MORE:
            return {...state,msgType:2}
        default:
            return state
    }
}


export const networkState = ( state = { isNetworkError:false },action ) => {
   switch(action.type){
        case NETWORK_ERROR:
            return {...state,isNetworkError:true}
        default:
            return state
    } 
}