import { combineReducers } from 'redux'

import {discountBanner, discountListTab, discountTypeList, discountBargainsList ,loadingMore , networkState} from './discount'

const rootReducer = combineReducers({
  discountBanner,
  discountListTab,
  discountTypeList,
  discountBargainsList,
  loadingMore,
  networkState
});

export default rootReducer;
