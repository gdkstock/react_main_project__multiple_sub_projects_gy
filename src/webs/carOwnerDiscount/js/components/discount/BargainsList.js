import React from 'react'
import styles from './BargainsList.scss'
import common from '../../utils/common'

//商品的信息
const BargainInfo = (props) => (
    <div className={styles.bargainInfo}>
        <div className={styles.bargainName}>{props.name}</div>
        <div className={styles.bargainDescription}>{props.description}</div>
        <BargainPrice
            originalCost={props.originalCost}
            discountPrice={props.discountPrice}
            seller={props.seller} 
            isPost={props.isPost}
        />
    </div>
)

//商品图片
const BargainImg = (props) => (
    <div className={styles.bargainImg}>
        <img src={props.img} />
    </div>
)

//商品价格与卖家
const BargainPrice = (props) => (
    <div className={styles.bargainPrice}>
        <span className={styles.bargainDiscountPrice}>¥{props.discountPrice}</span>
        <span className={styles.bargainOriginalCost}> 原价 <span>¥{props.originalCost}</span></span>
        <span className={styles.seller}>{props.seller}{props.isPost == 1&&" | 包邮"}</span>
    </div>
)

//一条商品信息
const Bargain = (props) => (
    <li className={styles.bargain} onClick={()=>jumpToUrl(props.clickUrl)}>
        <BargainImg img={props.img}/>
        <BargainInfo
            name={props.name}
            description={props.description}
            originalCost={props.originalCost}
            discountPrice={props.discountPrice}
            seller={props.seller}
            isPost={props.isPost}
        />
        <hr />
    </li>
)

//跳转链接
const jumpToUrl=(url)=>{
    //alert(url)
    common.openNewBrowserWithURL(url);
}

//商品列表
class BargainsList extends React.Component {
    render() {
        const {goodsList} = this.props;
        // let goodsList = discountBargainsList.data.goodsList;
        return(
            <div>
                <ul>
                    {goodsList.map((item) =>
                        <Bargain
                            key={item.numIid}
                            img={item.pictUrl}
                            name={item.customTitle.length!=0?item.customTitle:item.title}
                            description={item.describe}
                            originalCost={item.reservePrice}
                            discountPrice={item.finalPrice}
                            seller={item.categoryName}
                            isPost={item.isPost}
                            clickUrl={item.clickUrl}
                        />
                    )}
                </ul>
            </div>
        )
    }
}

export default BargainsList