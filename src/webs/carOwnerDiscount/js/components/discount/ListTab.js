import React from'react'
import {connect} from 'react-redux'
import {requestDiscountListTab} from '../../actions/discountActions'
import styles from './ListTab.scss'


class ListTab extends React.Component {
    constructor() {
        super();
        this.state = {
            "tabName": ["热门推荐","9.9元包邮"]
        }
    }

    render() {
        let {selectedIndex, handleIndexChange} = this.props;
        return(
            <div className={styles.listTab}>
                <div className={selectedIndex==0?(styles.tab + " " + styles.tabChoice):styles.tab}>
                    <button onClick={() => {handleIndexChange("0","HOT")}}>{this.state.tabName[0]}</button>
                </div>
                <div className={selectedIndex==0?styles.tab:(styles.tab + " " + styles.tabChoice)}>
                    <button onClick={() => {handleIndexChange("1","JIUJIU")}}>{this.state.tabName[1]}</button>
                </div>
                <div className={selectedIndex==0?styles.slideBorder:(styles.slideBorder + " " + styles.slideBorderRight)}></div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        discountListTab: state.discountListTab
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        requestAction: () => {
            dispatch(requestDiscountListTab())
        }
    }
}

const DiscountListTab = connect(
    mapStateToProps,
    mapDispatchToProps
)(ListTab)

export default ListTab