import React from 'react'
import {Carousel} from 'antd-mobile'
import styles from './banner.scss'

const DiscountBanner=(props)=>{
    const discountBanner = props.discountBanner || {};
    const bannerList = discountBanner.bannerList || [];
    const canPlay = bannerList.length>1?true:false;
    return (
        <div>
            {bannerList.length>0 &&
                <Carousel 
                    dots={false} 
                    autoplay={canPlay} 
                    infinite={canPlay} 
                    swiping={canPlay}
                >
                    {
                        discountBanner.bannerList.map((item)=>
                            <div key={item.eventId} onClick={()=>props.handleClick(item)}>
                                <img 
                                    src={item.imgUrl}  
                                    className={styles.banner}
                                />
                            </div>
                        )
                    }
                </Carousel>
            }   
        </div>
    );
};

export default DiscountBanner