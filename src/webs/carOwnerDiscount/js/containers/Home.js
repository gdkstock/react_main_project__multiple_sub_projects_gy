import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FontIcon from 'material-ui/FontIcon';
import { BottomNavigation, BottomNavigationItem } from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import IconLocationOn from 'material-ui/svg-icons/communication/location-on';
import FlatButton from 'material-ui/FlatButton';
import ArrowRightIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-right'


const recentsIcon = <FontIcon className="material-icons">restore</FontIcon>;
const favoritesIcon = <FontIcon className="material-icons">favorite</FontIcon>;
const nearbyIcon = <IconLocationOn />;

/**
 * A simple example of `BottomNavigation`, with three labels and icons
 * provided. The selected `BottomNavigationItem` is determined by application
 * state (for instance, by the URL).
 */
class Home extends Component {
    state = {
        selectedIndex: 0,
    };

    select = (index) => this.setState({ selectedIndex: index });

    render() {

        return (
            <MuiThemeProvider>
                <div>
                    <h1>什么都木有的首页</h1>
                    <ArrowRightIcon/>
                    <Paper zDepth={1}>
                        <BottomNavigation selectedIndex={this.state.selectedIndex}>
                            <BottomNavigationItem
                                label="Recents"
                                icon={recentsIcon}
                                onTouchTap={() => this.select(0)}
                            />
                            <BottomNavigationItem
                                label="Favorites"
                                icon={favoritesIcon}
                                onTouchTap={() => this.select(1)}
                            />
                            <BottomNavigationItem
                                label="Nearby"
                                icon={nearbyIcon}
                                onTouchTap={() => this.select(2)}
                            />
                        </BottomNavigation>
                    </Paper>
                    <div>
                        <FlatButton label="Default" />
                        <FlatButton label="Primary" primary={true} />
                        <FlatButton label="Secondary" secondary={true} />
                        <FlatButton label="Disabled" disabled={true} />
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

const mapStateToProps = state => ({
    //state: state.timer
})

const mapDispatchToProps = dispatch => ({
    //actions: bindActionCreators(TimerActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)