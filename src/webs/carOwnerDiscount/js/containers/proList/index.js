import React,{ Component } from 'react'
import BargainsList from '../../components/discount/BargainsList'
import {connect} from 'react-redux'
import {requestDiscountBargainsList} from '../../actions/discountActions'
import LoadingMore from 'app/components/common/loadingMore'
import styles from './index.scss'
import common from '../../utils/common'

class Pro extends React.Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            "selectedIndex": 0,
            "list": {
                "0": {
                    "labelCode": props.params.id,
                    "page": 0,
                    "pageSize": 10,
                    "goodsList": []
                }
            }
        }
    }

    //请求商品数据
    handleLoadMore(page) {
        let nextPage = page?page:this.state.list[this.state.selectedIndex].page + 1
        try {
            setTimeout(function(){},300);
            // 触发请求数据的action
            this.props.requestAction({
                "page": nextPage,
                "pageSize": this.state.list[this.state.selectedIndex].pageSize,
                "labelCode": this.state.list[this.state.selectedIndex].labelCode
            })
        } catch (error) {
           alert(error) 
        }
    }

    componentWillMount() {
        //设置头部标题
        //console.log(this.props.location.query.title)
        let title = this.props.location.query.title
        document.querySelector('title').innerHTML=title;
        common.setTitleText(title)
        this.handleLoadMore(1);
    }
    
    //处理新商品数据
    handleGoodsList(index,page,goodsList) {
        let prePage = this.state.list[index].page;
        let preGoodsList = this.state.list[index].goodsList;
        console.log(index,page,goodsList,prePage)
        if(typeof(page)!=undefined &&  page-prePage==1) {
            let newItem = Object.assign({},this.state.list[index],{
                page: prePage + 1,
                goodsList: [...preGoodsList,...goodsList]
            })
            let newList =  Object.assign({},this.state.list,{
                [index]: newItem
            })
            this.setState({
                "list": newList
            })
            console.log(this.state.list)
        }
    }

    //处理新属性
    handleNewProps(nextProps) {
        const {discountBargainsList} = nextProps;
        let labelCode = discountBargainsList.param.labelCode;
        let page = discountBargainsList[labelCode].page;
        let goodsList = discountBargainsList[labelCode].goodsList;

        this.handleGoodsList(0,page,goodsList);
    }

    componentWillReceiveProps(nextProps) {
        this.handleNewProps(nextProps);
    }

    render() {
        console.log("组件2state",this.state)
    	const selectedIndex = this.state.selectedIndex;
        const goodsList = this.state.list[selectedIndex].goodsList;
        const loadingMore = {...this.props.loadingMore,loadingMore:()=>this.handleLoadMore()}
        return(
            <div>
                <div className={styles.whiteSpace}></div>
                <BargainsList goodsList={goodsList} />
                <LoadingMore {...loadingMore}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return ({
        discountBargainsList: state.discountBargainsList,
        loadingMore:state.loadingMore
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        requestAction: (param) => {
            dispatch(requestDiscountBargainsList(param))
        }
    }
}

const ProList = connect(
    mapStateToProps,
    mapDispatchToProps
)(Pro)

export default ProList