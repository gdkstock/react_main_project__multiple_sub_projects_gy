/**
 * 测试   Test
 */

import React, { Component } from 'react';
import jsApi from '../utils/cx580.jsApi'
import common from '../utils/common'
import apiHelper from '../services/apiHelper'

class Test extends Component{

	// getDefaultProps(){
	// 	console.log("1.getDefaultProps")
	// }

	// getInitialState(){
	// 	console.log("2.getInitialState")
	// 	return{

	// 	}
	// }

	constructor(props){
		console.log("0")
		let list = [];
		for (var i = 0; i < 100; i++) {
			list.push(i)
		}
		super(props);
		this.state={
			list
		}
		console.log(props)
	}

	componentWillMount(){
		console.log("3.componentWillMount")
		if(common.isCXYApp()){
			jsApi.call({
				"commandId":"",
				"command":"controlTitleLayout",
				"data":{
				"showStatus":"hide",
				}
			},function(data){});
		}
	}

	componentDidMount(){
		console.log("5.componentDidMount")
	}

	componentWillReceiveProps(){
		console.log("6.componentWillReceiveProps")
	}

	shouldComponentUpdate(){
		console.log("7.shouldComponentUpdate")
		return true;
	}

	componentWillUpdate(){
		console.log("8.componentWillUpdate")
	}

	componentDidUpdate(){
		console.log("9.componentWillUpdate")
	}

	componentWillUnmount(){
		console.log("10.componentWillUnmount")
	}

	changeState(){
		let list=[]
		for (var i = 100; i >0; i--) {
			list.push(i)
		}
		this.setState({
			list
		})
	}

	render(){
		console.log("4.render")
		return(
			<div>
				<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493198736137&di=ed0b1964a41dc8fa0377b270f8fc65bd&imgtype=0&src=http%3A%2F%2Fimg.tuku.cn%2Ffile_big%2F201502%2F0e93d8ab02314174a933b5f00438d357.jpg"/>
				<div onClick={this.changeState.bind(this)}>改变状态</div>
				<a href="http://192.168.2.239:8081/#/discount">跳转</a>
				{
					this.state.list.map((item,index)=>
						<div key={index}>
							{item}
						</div>
					)
				}
			</div>
		)
	}
}

// const Test = React.createClass({

// 	getDefaultProps(props){
// 		console.log("1.getDefaultProps",props)
// 		return{name:"lijun"}
// 	},

// 	getInitialState(){
// 		console.log("2.getInitialState")
// 		console.log("props",this.props)
// 		let list = []
// 		for(var i = 0 ;i<100;i++){
// 			list.push(i)
// 		}
// 		return{
// 			list
// 		}
// 	},

// 	componentWillMount(){
// 		console.log("3.componentWillMount")
// 		console.log(this.state)
// 		if(common.isCXYApp()){
// 			jsApi.call({
// 				"commandId":"",
// 				"command":"controlTitleLayout",
// 				"data":{
// 				"showStatus":"hide",
// 				}
// 			},function(data){});
// 		}
// 		let requestParam = { 
// 			url:"http://192.168.1.167:9391/shopping/cxy/taobaoke/getShopList",
// 			data:{
// 				method:"post",
// 			}
// 		}
// 		apiHelper.fetch(requestParam).then(data=>{}).catch(data=>{common.networkError();})
// 	},

// 	componentDidMount(){
// 		console.log("5.componentDidMount")
// 	},

// 	componentWillReceiveProps(){
// 		console.log("6.componentWillReceiveProps")
// 	},

// 	shouldComponentUpdate(e){
// 		console.log("7.shouldComponentUpdate")
// 		return true;
// 	},

// 	componentWillUpdate(e){
// 		console.log("8.componentWillUpdate")
// 		console.log(e)
// 	},

// 	componentDidUpdate(){
// 		console.log("9.componentWillUpdate")
// 	},

// 	componentWillUnmount(){
// 		console.log("10.componentWillUnmount")
// 	},

// 	changeState(){
// 		let list = []
// 		for(var i = 100 ;i>0;i--){
// 			list.push(i)
// 		}
// 		this.setState({
// 			list
// 		})
// 	},

// 	render(){
// 		console.log("4.render")
// 		return(
// 			<div>
// 				<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493198736137&di=ed0b1964a41dc8fa0377b270f8fc65bd&imgtype=0&src=http%3A%2F%2Fimg.tuku.cn%2Ffile_big%2F201502%2F0e93d8ab02314174a933b5f00438d357.jpg"/>
// 				<div onClick={this.changeState}>改变状态</div>
// 				{
// 					this.state.list.map(item=>
// 						<div key={item}>
// 							{item}
// 						</div>
// 					)
// 				}
// 			</div>
// 		)
// 	}
// })

//
export default Test