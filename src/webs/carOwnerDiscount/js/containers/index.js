export {default as App} from './App'

// 简单
export { default as Home } from './Home'


// 404
export { default as NotFoundPage } from './404.js'


// 测试
// export { default as Test } from './Test.js'