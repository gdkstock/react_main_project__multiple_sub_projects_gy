import React from 'react'
import common from '../../utils/common'
import {Banner} from '../../components/discount'
import TypeList from 'app/components/common/typeList'
import DiscountTabs from "./Tabs"
import styles from './index.scss'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as discountActions from '../../actions/discountActions'
import jsApi from '../../utils/cx580.jsApi'
import NetworkError from 'app/components/common/networkError'

let timer=null;

class DiscountChild extends React.Component {

    componentWillMount() {
        document.querySelector("title").innerHTML = "车主特惠";
        common.setTitleText("车主特惠");

    }

    componentDidMount(){

        let requestBanner = this.props.actions.requestDiscountBanner
        let requestTypeList = this.props.actions.requestDiscountTypeList

        //获取app相关参数
        if(common.isCXYApp()){
            if(!timer){
                timer=setTimeout(()=>{
                    jsApi.call({
                        "commandId":"",
                        "command":"getSymbol",
                        "data": {
                            "userid":"",
                            "lat":"",
                            "lng":"",
                            "city":"",
                            "channel":"",
                            "version":""
                        }
                    },function(data){
                        // alert("调用成功"+JSON.stringify(data))
                        timer=null;
                        let param = {
                            userId:data.data.userid,
                            latitude:data.data.lat,
                            longitude:data.data.lng,
                            cityName:data.data.city,
                            channel:data.data.channel,
                            appServierVersion:data.data.version
                        }
                        // alert("param"+JSON.stringify(param))
                        requestBanner(data); //请求头部广告
                        requestTypeList(data); //请求商品品类列表
                    })
                },500)
            }
        }else {
            requestBanner({
                "userId":"",
                "latitude":"116.23234",
                "longitude":"23.3334",
                "cityName":"广州市",
                "channel":"",
                "appServierVersion":""
            });
            requestTypeList({
                "userId":"",
                "latitude":"116.23234",
                "longitude":"23.3334",
                "cityName":"广州市",
                "channel":"",
                "appServierVersion":""
            })
        }

    }
    componentWillUnmount() {
        
    }
    handleClick(item){
        console.log(item);
        let url=item.targetUrl || item.jumpUrl
        common.openNewBrowserWithURL(url)
    }

    render() {

        const { discountBanner,discountTypeList,networkState } = this.props
        let list=[];
        const handleClick=this.handleClick;

        if(discountTypeList.data!=undefined){

            let obj=discountTypeList.data
            alert(obj)
            let dataType = Object.prototype.toString.call(obj)
            dataType=="[object Array]"?list=obj:Object.keys(obj).length>0?list.push(obj):list=[];

            //给list每个item添加单击操作
            list=list.map((item)=>{
                let adList=item.adList.map((item)=>{
                    return {...item,handleClick}
                })
                return {...item,adList}
            })

        }
        return(
            <div className={styles.discount}>
                {networkState.isNetworkError ? <NetworkError/>:<div>
                <Banner discountBanner={discountBanner.data} handleClick={this.handleClick}/>
                {list.length>0 && <div className={styles.whiteSpace}></div>}
                <TypeList list={list}/>
                <div className={styles.whiteSpace}></div>
        
                <DiscountTabs selectedIndex={this.props.location.query.labelCode}/>
                </div>
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({...state})

const mapDispatchToProps = (dispatch) => ({
    actions:bindActionCreators(discountActions, dispatch)
})

const Discount = connect(
    mapStateToProps,
    mapDispatchToProps
)(DiscountChild)

export default Discount