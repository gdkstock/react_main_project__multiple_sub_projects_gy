import React from'react'
import {connect} from 'react-redux'
import {ListTab,BargainsList,LoadMoreBtn} from '../../components/discount'
import {requestDiscountBargainsList} from '../../actions/discountActions'
import LoadingMore from 'app/components/common/loadingMore'

class Tabs extends React.Component {
    constructor(props) {
        super(props);
        console.log(props)
        let selectedIndex = props.selectedIndex=="JIUJIU"?1:0;
        this.state = {
            "selectedIndex": selectedIndex,
            "list": {
                "0": {
                    "labelCode": "HOT",
                    "page": 0,
                    "pageSize": 10,
                    "goodsList": []
                },
                "1": {
                    "labelCode": "JIUJIU",
                    "page": 0,
                    "pageSize": 10,
                    "goodsList": []
                }
            }
        }
        this.handleIndexChange = this.handleIndexChange.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
    }

    //请求商品数据
    handleLoadMore(page) {
        let nextPage = page?page:this.state.list[this.state.selectedIndex].page + 1
        try {
            //alert("123")
            setTimeout(function(){},300);
            // 触发请求数据的action
            this.props.requestAction({
                "page": nextPage,
                "pageSize": this.state.list[this.state.selectedIndex].pageSize,
                "labelCode": this.state.list[this.state.selectedIndex].labelCode
            })
        } catch (error) {
            //alert("chucuo")
        }
    }

    componentWillMount() {
        // alert("zujianjiazaile")
        this.handleLoadMore(1);
    }

    //处理Tab标签变化
    handleIndexChange(index,labelCode) {
        this.setState({
            "selectedIndex": index
        })
        // if(this.state.list[index].page!==0){
        //     return;
        // }
        try {
            setTimeout(function(){},300);
            // 触发请求数据的action
            this.props.requestAction({
                "page": 1,
                "pageSize": this.state.list[index].pageSize,
                "labelCode": this.state.list[index].labelCode
            })
        } catch (error) {
           
        }
    }
    
    //处理新商品数据
    handleGoodsList(index,page,goodsList) {
        let prePage = this.state.list[index].page;
        let preGoodsList = this.state.list[index].goodsList;
        if(typeof(page)!=undefined && page-prePage==1) {
            let newItem = Object.assign({},this.state.list[index],{
                page: prePage + 1,
                goodsList: [...preGoodsList,...goodsList]
            })
            let newList =  Object.assign({},this.state.list,{
                [index]: newItem
            })
            this.setState({
                "list": newList
            })
            console.log(this.state.list)
        }
    }

    //处理新属性
    handleNewProps(nextProps) {
        const {discountBargainsList} = nextProps;
        // alert(nextProps)
        let labelCode = discountBargainsList.param.labelCode;
        let page = discountBargainsList[labelCode].page;
        let goodsList = discountBargainsList[labelCode].goodsList;

        labelCode === "HOT" && this.handleGoodsList(0,page,goodsList);
        labelCode === "JIUJIU" && this.handleGoodsList(1,page,goodsList);
    }

    componentWillReceiveProps(nextProps) {
        this.handleNewProps(nextProps);
    }

    render() {
        console.log("组件1state",this.state)
        const selectedIndex = this.state.selectedIndex;
        const goodsList = this.state.list[selectedIndex].goodsList;
        const loadingMore = {...this.props.loadingMore,loadingMore:this.handleLoadMore}
        return(
            <div>
                <ListTab
                    handleIndexChange={this.handleIndexChange}
                    selectedIndex={this.state.selectedIndex}
                />
                <BargainsList goodsList={goodsList} />
                <LoadingMore {...loadingMore}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log(state)
    return ({
        discountBargainsList: state.discountBargainsList,
        loadingMore:state.loadingMore
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        requestAction: (param) => {
            dispatch(requestDiscountBargainsList(param))
        }
    }
}

const DiscountTabs = connect(
    mapStateToProps,
    mapDispatchToProps
)(Tabs)

export default DiscountTabs