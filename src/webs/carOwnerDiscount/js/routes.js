import React from 'react'
import { Route, IndexRoute } from 'react-router'

import {
  App,
  Home,
  NotFoundPage,
  // Test, // 测试
} from './containers'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="discount" getComponent={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/Discount').default)
        }, 'discount')
    }} />
    <Route path="proList/:id" getComponent={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/proList').default)
        }, 'proList')
    }} />
    <Route path="test" getComponent={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/Test').default)
        }, 'test')
    }} />
    <Route path="*" component={NotFoundPage} />
  </Route>
);