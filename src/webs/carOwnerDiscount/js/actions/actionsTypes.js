/*
* 这里定义所有的action类型
* */

//车主特惠顶部banner
export const REQUEST_DISCOUNT_BANNER = 'REQUEST_DISCOUNT_BANNER'//请求特惠banner链接
export const RECEIVE_DISCOUNT_BANNER = 'RECEIVE_DISCOUNT_BANNER'//获取特惠banner链接

//车主特惠Tab标签
export const REQUEST_DISCOUNT_LISTTAB = 'REQUEST_DISCOUNT_LISTTAB'//请求Tab标签
export const RECEIVE_DISCOUNT_LISTTAB = 'RECEIVE_DISCOUNT_LISTTAB'//获取Tab标签

//车主特惠广告list
export const REQUEST_DISCOUNT_TYPELIST = 'REQUEST_DISCOUNT_TYPELIST'//请求广告list
export const RECEIVE_DISCOUNT_TYPELIST = 'RECEIVE_DISCOUNT_TYPELIST'//获取广告list

//车主特惠商品list
export const REQUEST_DISCOUNT_BARGAINSLIST = 'REQUEST_DISCOUNT_BARGAINSLIST'//请求商品list
export const RECEIVE_DISCOUNT_BARGAINSLIST = 'RECEIVE_DISCOUNT_BARGAINSLIST'//获取商品list
export const IS_LOADING = 'IS_LOADING'//加载中
export const HASE_MORE = 'HASE_MORE' //有商品数据
export const NO_MORE = 'NO_MORE' //没有更多数据

//网络状况
export const NETWORK_ERROR = 'NETWORK_ERROR' //网络错误