import {
    REQUEST_DISCOUNT_BANNER,
    RECEIVE_DISCOUNT_BANNER,
    REQUEST_DISCOUNT_LISTTAB,
    RECEIVE_DISCOUNT_LISTTAB,
    SET_TAB_INDEX,
    REQUEST_DISCOUNT_TYPELIST,
    RECEIVE_DISCOUNT_TYPELIST,
    REQUEST_DISCOUNT_BARGAINSLIST,
    RECEIVE_DISCOUNT_BARGAINSLIST,
} from './actionsTypes'

//请求Banner
export const requestDiscountBanner = (param) => ({
    type: REQUEST_DISCOUNT_BANNER,
    param
})

//获取Banner
export const receiveDiscountBanner = (data) => ({
    type: RECEIVE_DISCOUNT_BANNER,
    data
})

//请求商品Tab
export const requestDiscountListTab = () => ({
    type: REQUEST_DISCOUNT_LISTTAB
})

//获取商品Tab
export const receiveDiscountListTab = (data) => ({
    type: RECEIVE_DISCOUNT_LISTTAB,
    data
})

//请求商品类型list
export const requestDiscountTypeList = (param) => ({
    type: REQUEST_DISCOUNT_TYPELIST,
    param
})

//获取类型list
export const receiveDiscountTypeList = (data) => ({
    type: RECEIVE_DISCOUNT_TYPELIST,
    data
})

//请求商品list
export const requestDiscountBargainsList = (param) => ({
    type: REQUEST_DISCOUNT_BARGAINSLIST,
    param
})

//获取商品list
export const receiveDiscountBargainsList = (data) => ({
    type: RECEIVE_DISCOUNT_BARGAINSLIST,
    data
})