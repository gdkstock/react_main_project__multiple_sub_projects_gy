import baseJsApi from 'app/utils/jsApi';
import common from '../utils/common';

class JsApi extends baseJsApi {
    constructor() {
        super();

    }

    ready(callback) {
        if (window.jsApiIsLoad) {
            callback();
        } else {
            document.addEventListener('jsApiIsReady', callback);
        }
    }

    // 设置APPwebView的高度
    setPageRate(rate) {
        if (common.isCXYApp()) {
            try {
                this.ready(() => {
                    window.cx580.jsApi.call(
                        {
                            "commandId": "",
                            "command": "setPageRate",
                            "data": {
                                "rate": rate + ''
                            }
                        }, res => {
                            // alert('调用成功：' + JSON.stringify(res));
                        }
                    );
                })
            } catch (e) {
                // alert('调用JSDK出错：' + JSON.stringify(e));
            }
        }
    }

    /**
     * getSymbol
     * @callback callback 回调函数
     */
    getSymbol(callback) {
        try {
            this.ready(() => {
                window.cx580.jsApi.call({
                    "commandId": "",
                    "command": "getSymbol",
                    "data": {
                        "userid": "", //2.0版本
                        "deviceId": "",
                        "lng": "",
                        "lat": "",
                        "version": "",
                        "channel": "",
                        "headImage": "",
                        "phone": "",
                        "nickName": "",
                        "city": "",
                        "cityCode": "",
                        "accountId": "",
                        "token": "",
                        "productId": "",
                        "sessionId": "",
                        "statusBarHeight": "",
                    }
                }, res => {
                    const { accountId, token, deviceId, userid, version, productId,
                        channel, city, cityCode, phone, sessionId, lng, lat
                    } = res.data;

                    // 保存数据到session中
                    sessionStorage.setItem('userId', accountId);
                    sessionStorage.setItem('token', token);
                    sessionStorage.setItem('deviceId', deviceId || userid);
                    sessionStorage.setItem('appVersion', version);
                    sessionStorage.setItem('productId', productId);
                    sessionStorage.setItem('channel', channel);
                    sessionStorage.setItem('city', city);
                    sessionStorage.setItem('cityCode', cityCode);
                    sessionStorage.setItem('sessionId', sessionId);
                    sessionStorage.setItem('lng', lng);
                    sessionStorage.setItem('lat', lat);
                    sessionStorage.setItem('phone', phone);
                    callback(res);
                });
            });
        } catch (e) {
            callback(e);
        }
    }
}

// 实例化后再导出
export default new JsApi()