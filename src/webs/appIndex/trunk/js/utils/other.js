/**
 * URL重定向
 */
import { Toast } from 'antd-mobile'

//配置文件
import config from '../config'

class Other {
    constructor() {
        this.init();
    }

    init() {
        this.cxytjInit(); // 埋点JS初始化
        this.watchBody(); // 监听body的变化
        setInterval(() => this.historyInit(), 100) //避免微信后退不刷新，所以这里定时循环
    }

    /**
     * history相关初始化
     */
    historyInit() {
        if (window.history.state) {

            let { urlReplace, historyGo } = window.history.state;

            //重定向到指定页面
            if (urlReplace) {
                document.getElementById('app').style.display = 'none' //隐藏界面，提升交互体验
                Toast.loading('', 0)
                let url = urlReplace
                window.history.replaceState({}, '', ''); //清空需要跳转的URL
                window.location.replace(url) //重定向
                return;
            }

            //页面后退
            if (historyGo) {
                window.history.go(historyGo);
                return;
            }
        }
    }

    /**
     * 埋点JS初始化
     */
    cxytjInit() {
        if (window.cxytjIsReady) {
            //初始化通用数据
            this.cxytjInitData();
        } else {
            window.cxytjReady = () => {
                this.cxytjInitData();
            }
        }
    }

    /**
     * 埋点JS初始化的数据
     */
    cxytjInitData() {
        window.cxytj.init({ //以下为初始化示例，可新增或删减字段
            productId: 'AppIndexH5', //产品ID
            productVersion: '1.0', //产品版本
            channel: sessionStorage.getItem('userType'), //推广渠道
            isProduction: config.production, //生产环境or测试环境 默认测试环境
            userId: sessionStorage.getItem('userId'), //用户ID
        });
    }

    /**
     * 监听body的变化
     */
    watchBody() {
        // Firefox和Chrome早期版本中带有前缀
        const MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver

        // 选择目标节点
        const target = document.body;

        // 创建观察者对象
        const observer = new MutationObserver(mutations => {
            // 节点发送了变化时分派body发送变化事件
            return this.dispatchEvent('bodyMutation');
        });

        // 配置观察选项:
        const config = { attributes: true, childList: true, characterData: true, subtree: true };

        // 传入目标节点和观察选项
        observer.observe(target, config);

        // body加载完成后分派body发送变化事件
        window.bodyLoad = () => this.dispatchEvent('bodyMutation');
    }

    dispatchEvent(eventName) {
        const ev = new Event(eventName, {
            bubbles: 'true',
            cancelable: 'true'
        });
        document.dispatchEvent(ev);
    }

}

// 实例化后再导出
export default new Other()


window.onerror = function (msg, url, l) {
    var txt = '';
    txt = "There was an error on this page.\n\n";
    txt += "Error: " + msg + "\n";
    txt += "URL: " + url + "\n";
    txt += "Line: " + l + "\n\n";
    txt += "Click OK to continue.\n\n";
    console.log('全局异常捕获：', txt);
    return true; //正式环境屏蔽错误
}