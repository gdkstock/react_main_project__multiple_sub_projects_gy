/**
 * 自媒体相关
 */

import {
    CREATE_ZIMEITI,
} from '../actions/actionsTypes';

export default function zimeiti(state = {}, action) {
    switch (action.type) {
        // 创建自媒体信息
        case CREATE_ZIMEITI:
            return action.data;
        default:
            return state;
    }
}