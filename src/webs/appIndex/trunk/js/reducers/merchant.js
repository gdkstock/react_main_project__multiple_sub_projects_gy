/**
 * 附近商家相关
 */

import {
	CREATE_MERCHANT_SERVICES,
	CREATE_SERVICE_TYPES,
	CREATE_MARCHANT_INFO,
	CREATE_MERCHANT_MORE_URL,
} from '../actions/actionsTypes'

const init = {
	// 无附近商家时的服务列表
	merchantServices: [],

	// 有商家时的服务栏目
	serviceTypes: [],

	// 商家信息
	merchantInfo: {},

	// 跳转URL
	moreUrl: '',
}

export default function merchant(state = init, action) {
	switch (action.type) {
		case CREATE_MERCHANT_SERVICES:
			return Object.assign({}, state, {
				merchantServices: action.data
			})

		case CREATE_SERVICE_TYPES:
			return Object.assign({}, state, {
				serviceTypes: action.data
			})

		case CREATE_MARCHANT_INFO:
			return Object.assign({}, state, {
				merchantInfo: action.data
			})

		case CREATE_MERCHANT_MORE_URL:
			return Object.assign({}, state, {
				moreUrl: action.data
			})
		
		default:
			return state
	}
}