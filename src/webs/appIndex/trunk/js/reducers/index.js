import { combineReducers } from 'redux'
import user from './users'

import merchant from './merchant';

import zimeiti from './zimeiti';

import adver from './adver';

const rootReducer = combineReducers({
    user, //用户信息表
    merchant, // 附近商家相关
    zimeiti, // 自媒体相关
    adver, // 广告相关
});

export default rootReducer;
