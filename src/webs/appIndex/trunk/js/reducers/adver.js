/**
 * 广告相关
 */

import {
    CREATE_ADVER,
} from '../actions/actionsTypes';

export default function adver(state = {}, action) {
    switch (action.type) {
        // 创建自媒体信息
        case CREATE_ADVER:
            return action.data;
        default:
            return state;
    }
}