/*
* 这里定义所有的action类型
* */

/**
 * fetch数据请求相关
 */
export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时


//同步请求
export const CREATE_MARCHANT_INFO = 'CREATE_MARCHANT_INFO' // 创建首页附近商家详情
export const CREATE_MERCHANT_SERVICES = 'CREATE_MERCHANT_SERVICES' // 创建无商家时的服务列表
export const CREATE_SERVICE_TYPES = 'CREATE_SERVICE_TYPES' // 创建有商家时的服务列表
export const CREATE_MERCHANT_MORE_URL = 'CREATE_MERCHANT_MORE_URL' // 附近商家title的跳转URL
export const CREATE_ZIMEITI = 'CREATE_ZIMEITI' // 创建自媒体相关的信息
export const CREATE_ADVER = 'CREATE_ADVER' // 创建广告

//异步请求
export const GET_MAIN_TYPES_ASYNC = 'GET_MAIN_TYPES_ASYNC' // 获取首页附近商家的服务列表
export const GET_SHOP_ASYNC = 'GET_SHOP_ASYNC' // 获取附件商家主页服务列表
export const GET_ZIMEITI_ASYNC = 'GET_ZIMEITI_ASYNC' // 获取自媒体相关
export const GET_ADVER_ASYNC = 'GET_ADVER_ASYNC' // 获取广告