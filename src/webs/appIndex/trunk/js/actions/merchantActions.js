/*
* 附近商家相关的actions
*/

import {
    GET_MAIN_TYPES_ASYNC,
    GET_SHOP_ASYNC,
} from './actionsTypes'

//获取首页附近商家的服务列表
export const getMainTypesAsync = (data, callback) => ({
    type: GET_MAIN_TYPES_ASYNC,
    data,
    callback
})

//获取附件商家主页服务列表
export const getShopAsync = (data, callback) => ({
    type: GET_SHOP_ASYNC,
    data,
    callback
})
