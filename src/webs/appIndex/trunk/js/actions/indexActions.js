/*
* 首页其他接口相关的actions
*/

import {
    GET_ZIMEITI_ASYNC,
    GET_ADVER_ASYNC,
} from './actionsTypes'


// 获取自媒体的信息
export const getZimeitiAsync = (data, callback) => ({
    type: GET_ZIMEITI_ASYNC,
    data,
    callback
})

// 获取广告
export const getAdverAsync = (data, callback) => ({
    type: GET_ADVER_ASYNC,
    data,
    callback
})
