/**
 * 自媒体相关
 */

import Title from '../../components/title';

import { Carousel } from 'antd-mobile';

import styles from './index.scss';

import jsApi from '../../utils/jsApi';

import common from '../../utils/common';

export const ZiMeiTi = props => {
    const { common, hot, main } = props;
    const showRecommend = main && Object.keys(main).length > 0;
    const showHotList = hot && hot.length > 0;
    const showList = common && common.length > 0;

    return (
        <div className={'box indexBox'}>
            <Title title='头条推荐'></Title>
            {showRecommend && <Recommend {...main} />}
            {showHotList && <HotList list={hot} />}
            {showList && <List list={common} />}
        </div>
    )
}

/**
 * 推荐
 * @param {object} props 
 */
const Recommend = props => {
    const { title, readNumber, thumbUrl, jumpUrl, id } = props;
    return (
        <div className={styles.recommend} onClick={() => jumpUrl && toUrl(jumpUrl, 'APP_INDEX_H5_ZIMEITI_MAIN', id)}>
            <img src={thumbUrl} />
            <div className={styles.text}>
                {readNumber != '0' && <span className={styles.views}>{readNumber}</span>}
                <p className={styles.title + ' text-overflow-1'}>{title}</p>
            </div>
        </div>
    )
}

/**
 * 热读
 * @param {object} props 
 */
const HotList = props => {
    const { list = [] } = props;
    return (
        <div className={styles.hotList}>
            {
                list.length > 1 ?
                    <Carousel
                        infinite={true}
                        autoplay={true}
                        dots={false}
                        vertical={true}
                    >
                        {list.map((item, i) => <Hot key={'hot-' + item.id + '-' + i} {...item} />)}
                    </Carousel>
                    : <Hot {...list[0]} />
            }
        </div>
    )
}
const Hot = props => {
    const { title, jumpUrl, id } = props;
    return (
        <div className={styles.hot} onClick={() => jumpUrl && toUrl(jumpUrl, 'APP_INDEX_H5_ZIMEITI_HOT', id)}>
            <p className={styles.title + ' text-overflow-1'}>{title}</p>
        </div>
    )
}

/**
 * 资讯列表
 * @param {object} props 
 */
const List = props => {
    const { list } = props;
    return (
        <div className={styles.list}>
            {
                list.map((item, i) =>
                    <div
                        key={'zimeiti-item-' + i}
                        className={styles.item + ' topLine'}
                        onClick={() => item.jumpUrl && toUrl(item.jumpUrl, 'APP_INDEX_H5_ZIMEITI_COMMON')}
                    >
                        <img className={styles.itemImg} src={item.thumbUrl} />
                        <div className={styles.text}>
                            <p className={styles.title + ' text-overflow-2'}>{item.title}</p>
                        </div>
                        <div className={styles.author}>
                            <img src={item.authorHeaderUrl} />
                            <span>{item.authorName}</span>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

/**
 * 跳转到指定页面
 * @param {string} url 跳转的URL
 * @param {string} eventId 埋点ID
 * @param {string} merchantId 商家ID
 */
const toUrl = (url, eventId, merchantId) => {
    // 跳转
    common.sendCxytj({ eventId, attr1: merchantId }); // 提交埋点
    console.log("跳转:", url, eventId, merchantId)
    jsApi.pushWindow(url); // 通过新view打开
}