import React, { Component } from 'react';

import Title from '../../components/title';

//styles
import styles from './carInfo.scss';

// jsApi
import jsApi from '../../utils/jsApi';

//常用工具类
import common from '../../utils/common';

class CarInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // 列表
            list: [
                {
                    id: 1,
                    title: '热门推荐',
                    body: [
                        {
                            title: '车品商城',
                            subTitle: '优质热销车品',
                            imgSrc: './images/chepinshangcheng.png',
                            jumpUrl: 'https://h5.youzan.com/v2/showcase/homepage?alias=11t0aaqs5',
                        },
                        {
                            title: '油卡办理',
                            subTitle: '快速代办',
                            imgSrc: './images/youkabanli.png',
                            jumpUrl: `https://chewu.cx580.com/OrderSite/page/OilNewCard/NewCardIndex.aspx?userType=${sessionStorage.getItem('userType')}`,
                        },
                        {
                            title: '入深申报',
                            subTitle: '非深圳车牌',
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: 'https://discovery.cx580.com/gatepass/index.html',
                        },
                        {
                            title: '车主招募',
                            subTitle: <i className={styles.orange}>滴滴顺风车</i>,
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: 'https://wap.didialift.com/pinche/register/login?regfrom=501852',
                        }
                    ]
                },
                {
                    id: 2,
                    title: '车市行情',
                    body: [
                        {
                            title: '新车报价',
                            subTitle: '低价买车',
                            imgSrc: './images/xinchebaojia.png',
                            jumpUrl: 'https://car.pcauto.com.cn/cxy/auto/?ad=12027',
                        },
                        {
                            title: '二手好车',
                            subTitle: <i className={styles.orange}>享一年质保</i>,
                            imgSrc: './images/ershouhaoche.png',
                            jumpUrl: 'https://m.renrenche.com/ershouche/?fr=1425',
                        },
                        {
                            title: '爱车估值',
                            subTitle: '你的爱车还值多少',
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: 'http://mobile.cx580.com:9080/appmsg/FW?F=445',
                        },
                        {
                            title: '高价卖车',
                            subTitle: '享一年质保',
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: 'https://m.renrenche.com/ershouche/?fr=1425',
                        }
                    ]
                },
                {
                    id: 3,
                    title: '金融服务',
                    body: [
                        {
                            title: '信用卡办理',
                            subTitle: '加油返现',
                            imgSrc: './images/xinyongkabanli.png',
                            jumpUrl: 'https://discovery.cx580.com/carservice/index.html?api_version=3#/card',
                        },
                        {
                            title: '底价车险',
                            subTitle: '投保即送',
                            imgSrc: './images/dijiachexian.png',
                            jumpUrl: `https://chewu.cx580.com/orderSite/page/picch5/index.aspx?userType=${sessionStorage.getItem('userType')}`,
                        },
                        {
                            title: '车主贷款',
                            subTitle: '快速放贷 最高50万',
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: 'https://static1.weidai.com.cn/static/common/weiApp/webH5/thirdPromote/index.html?clmg_tg=Z015',
                        },
                        {
                            title: '保险超市',
                            subTitle: '省心省力省钱',
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: `https://discovery.cx580.com/content/index.html?userType=${sessionStorage.getItem('userType')}`,
                        }
                    ]
                },
                {
                    id: 4,
                    title: '服务大全',
                    body: [
                        {
                            title: '一嗨租车',
                            subTitle: '首租立减100元',
                            imgSrc: './images/yihaizuche.png',
                            jumpUrl: 'http://mobile.cx580.com:9080/appmsg/FW?F=453',
                        },
                        {
                            title: '手机流量',
                            subTitle: '秒速到账',
                            imgSrc: './images/shoujiliulang.png',
                            jumpUrl: `https://merorder.cx580.com/authorization?userType=${sessionStorage.getItem('userType')}&redirect_uri=http://flowrc.19ego.cn/cxyapp/flowRecharge`,
                        },
                        {
                            title: '违章高发',
                            subTitle: '剩余积分查询',
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: 'https://daiban.cx580.com/drivingLicence/index.html?api_version=3',
                        },
                        {
                            title: '一键应急',
                            subTitle: '车主必备',
                            // imgSrc: './images/图片尺寸2@2x.png',
                            jumpUrl: 'https://discovery.cx580.com/carservice/index.html?api_version=3#/emergency',
                        }
                    ]
                }
            ],

            // 橙色字体
            orangeText: ['滴滴顺风车', '享一年质保'],
        }
    }

    componentDidMount() {

    }

    render() {
        const { list, orangeText } = this.state;

        return (
            <div className={styles.carInfo}>
                {list.map((item) =>
                    <div key={item.id} className={styles.infoBox}>
                        {/* 顶部 */}
                        <div className={styles.title + ' bottomLine'}>{item.title}</div>

                        {/* 左边 */}
                        <div className={styles.leftBox + ' ' + styles.lineR} onClick={() => jsApi.pushWindow(item.body[0].jumpUrl)}>
                            <div className={styles.itemBox}>
                                <span>{item.body[0].title} </span>
                                <i>{item.body[0].subTitle}</i>
                            </div>
                            <img src={item.body[0].imgSrc} />
                        </div>

                        {/* 右边 start */}
                        <div className={styles.rightTopBox + ' bottomLine'} onClick={() => jsApi.pushWindow(item.body[1].jumpUrl)}>
                            <div className={styles.itemBox}>
                                <span>{item.body[1].title}</span>
                                <i>{item.body[1].subTitle}</i>
                            </div>
                            <div className={styles.itemBox}>
                                <img src={item.body[1].imgSrc} />
                            </div>
                        </div>
                        <div className={styles.rightBottomBox}>
                            <div className={styles.itemBox + ' ' + styles.lineR} onClick={() => jsApi.pushWindow(item.body[2].jumpUrl)}>
                                <span>{item.body[2].title} </span>
                                <i>{item.body[2].subTitle}</i>
                            </div>
                            <div className={styles.itemBox} onClick={() => jsApi.pushWindow(item.body[3].jumpUrl)}>
                                <span>{item.body[3].title}</span>
                                <i>{item.body[3].subTitle}</i>
                            </div>
                        </div>
                        {/* 右边 end */}
                    </div>
                )
                }
            </div>
        )
    }
}

export default CarInfo