/**
 * 广告相关
 */

import styles from './index.scss';

export const Ad = props => {
    const { onClick, imgUrl } = props;
    return (
        <div className={styles.ad} onClick={() => onClick && onClick()}>
            <img src={imgUrl} />
        </div>
    )
}