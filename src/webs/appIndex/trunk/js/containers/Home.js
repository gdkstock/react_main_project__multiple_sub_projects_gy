import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// 组件
import { Merchant } from '../containers/merchant'; // 附近商家
import { ZiMeiTi } from '../containers/zimeiti'; // 自媒体
import { Ad } from '../containers/ad'; // 广告
import CarInfo from '../containers/carInfo/carInfo'; // 车辆相关

// actions
import * as merchantActions from '../actions/merchantActions';
import * as indexActions from '../actions/indexActions';

// jsdk
import jsApi from '../utils/jsApi';

import common from '../utils/common';

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {}
	}

	componentWillMount() {
		// 监听事件
		document.addEventListener('bodyMutation', () => this.authSetHeight()); // 自动设置webView的高度
	}

	componentDidMount() {
		this.getData(); // 获取数据

		this.watchAppEvent() // 监听App事件
	}

	/**
	 * 请求数据
	 */
	getData() {
		const cityName = sessionStorage.getItem('city') || '广州';
		const cityCode = sessionStorage.getItem('cityCode') || '4401';
		const longitude = sessionStorage.getItem('lng') || '113.338027';
		const latitude = sessionStorage.getItem('lat') || '23.107678';
		const appVersion = sessionStorage.getItem('appVersion');
		const channel = sessionStorage.getItem('channel');
		const deviceType = common.isAndroid() ? 'Android' : 'iPhone';

		// 共用参数
		const param = {
			longitude,
			latitude,
			cityName,
			cityCode,
		}
		this.props.merchantActions.getMainTypesAsync(param); // 获取首页附近商家的服务列表
		this.props.merchantActions.getShopAsync(param); // 获取首页附近商家的服务列表
		this.props.indexActions.getZimeitiAsync(); // 获取自媒体相关的信息
		this.props.indexActions.getAdverAsync({
			...param,
			appVersion,
			deviceType,
			channel,
		}); // 获取广告
	}

	/**
	 * 更新数据
	 */
	updateData() {
		jsApi.getSymbol(() => this.getData()); // 先getSymbor获取到最新的数据 再重新请求接口
	}

	/**
	 * 监听APP事件
	 */
	watchAppEvent() {
		// App地图定位发生了更新，生效版本：v6.4.0
		window.addEventListener('cxyBroadcast_App_LocationChanged', () => this.updateData());

		// App用户选择的城市更新，生效版本：v6.4.0
		window.addEventListener('cxyBroadcast_App_UserCityChanged', () => this.updateData());

		// App的主页发生了下拉事件，生效版本：v6.4.0
		window.addEventListener('cxyBroadcast_App_OnHomePullDown', () => this.updateData());

		// App登录状态发生更改了，生效版本：v6.4.0
		window.addEventListener('cxyBroadcast_App_LoginStateChanged', () => this.updateData());
	}

	// 设置高度
	setHeight(rate) {
		jsApi.setPageRate(rate);
	}

	// 自动设置webView的高度
	authSetHeight() {
		const w = document.body.offsetWidth;
		const h = document.body.offsetHeight;
		const rate = (h / w).toFixed(2); // 保留两位小数

		// 判断当前比例和上一个比例是否一致 不一致则调用jsdk 避免频繁调用的性能问题以及可能出现的bug问题
		if (window.preBodyRate !== rate) {
			window.preBodyRate = rate; // 保存当前比例
			this.setHeight(rate); // 设置高度
		}
	}

	toUrl(url, eventId, attr1) {
		// 跳转
		common.sendCxytj({ eventId, attr1 }); // 提交埋点
		jsApi.pushWindow(url); // 通过新view打开
	}

	render() {
		const { merchant, zimeiti,adver } = this.props;

		return (
			<div className='box'>
				{/* 附近商家 */}
				<Merchant {...merchant} />

				{/* 广告 */}
				{adver && Object.keys(adver).length > 0 && <Ad {...adver} onClick={() => this.toUrl(adver.jumpUrl, adver.eventId)} />}

				{/* 自媒体 */}
				<ZiMeiTi {...zimeiti} />

				{/* 车辆相关 */}
				<CarInfo />
			</div>
		);
	}
}

const getAdver = adver => {
	if (adver && adver.adList && adver.adList.length > 0) {
		return adver.adList[0];
	}
	return {}; // 默认返回空对象
}

const mapStateToProps = state => ({
	// 附近商家
	merchant: state.merchant,

	// 自媒体
	zimeiti: state.zimeiti,

	// 广告
	adver: getAdver(state.adver),
})

const mapDispatchToProps = dispatch => ({
	merchantActions: bindActionCreators(merchantActions, dispatch),
	indexActions: bindActionCreators(indexActions, dispatch),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);