/**
 * 首页附近商家相关
 */

// 标题
import Title from '../../components/title';

import styles from './index.scss';

// JSDK
import jsApi from '../../utils/jsApi';

// 常用工具类
import common from '../../utils/common';


/**
 * 附近商家详情
 * @param {object} props 
 */
export const Merchant = props => {
    const { merchantServices, serviceTypes, merchantInfo, moreUrl } = props;
    const showServiceList = Object.keys(merchantInfo).length === 0; // 商家详情不存在
    const { service } = merchantInfo;

    return (
        <div className={'box ' + styles.box} >
            <Title title='附近门店服务' moreUrl={moreUrl} onClick={() => moreUrl && toUrl(moreUrl, 'cxyapp_merchant_title_more')} />
            {
                showServiceList ? <ServiceList list={merchantServices} />
                    : <div>
                        <ServiceTypes list={serviceTypes} />
                        <MerchantInfo {...merchantInfo} />
                        {service && <ActivityList {...merchantInfo} />}
                    </div>
            }
        </div>
    )
}

/**
 * 无附近商家时的服务列表
 */
const ServiceList = ({ list = [] }) => {
    return (
        <div className={styles.serviceList}>
            {
                list && list.map((item, i) =>
                    <div key={'serviceItem-' + i} className={styles.serviceItem} onClick={() => toUrl(item.jumpUrl, item.eventId)}>
                        <img className={styles.serviceImg} src={item.imgUrl} />
                        <div className={styles.serviceText}>
                            <h3 className='text-overflow-1'>{item.name}</h3>
                            <p className='text-overflow-1'>{item.serviceDesc}</p>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

/**
 * 有附近商家时的服务类型
 */
const ServiceTypes = ({ list = [] }) => {
    if (list.length > 3) {
        list = list.slice(0, 3); // 只保存前三个
    }
    return (
        <div className={styles.serviceTypes + ' topLine'}>
            {
                list && list.map((item, i) =>
                    <div key={'serviceTypes-' + item.code}
                        className={styles.serviceType + ' leftLine'}
                        onClick={() => toUrl(item.jumpUrl, item.eventId)}
                    >
                        <h3 className={styles.serviceTypeName}>{item.name}</h3>
                        <img src={item.imgUrl} />
                    </div>
                )
            }
        </div>
    )
}

/**
 * 商家信息
 */
const MerchantInfo = props => {
    const { shopName, smallImageUrl, servicesList, distance, jumpUrl, shopAddress, eventId, merchantId } = props;
    return (
        <div className={styles.info + ' topLine'} onClick={() => toUrl(jumpUrl, eventId, merchantId)}>
            <img className={styles.infoImg} src={smallImageUrl} />
            <div className={styles.text}>
                <p className={styles.infoTitle + ' text-overflow-1'}>{shopName}</p>
                <ul className={styles.infoServices}>
                    {servicesList && servicesList.map((item, i) =>
                        <li className={'infoServices-' + i}><span>{item}</span></li>
                    )}
                </ul>
                <p className={styles.address + ' text-overflow-1'}>
                    <span>{shopAddress}</span>
                    {distance && <span className={styles.km}>{distance}km</span>}
                </p>
            </div>
        </div >
    )
}

const ActivityList = ({ service, merchantId }) => {
    const { shopPrice, jumpUrl, btnText, serviceName, serviceDesc, price, eventId } = service;
    return (
        <div className={styles.activity + ' topLine'} onClick={() => toUrl(jumpUrl, eventId, merchantId)}>
            <div className={styles.activityPrice}>
                <h3 className='text-overflow-1'>¥{price == 0 ? '0' : price}</h3>
                <p className='text-overflow-1'>{shopPrice}</p>
            </div>
            <div className={styles.activityText}>
                <h3 className={styles.activityTitle + ' text-overflow-1'}>{serviceName}</h3>
                <p className={styles.activityDesc + ' text-overflow-1'}>{serviceDesc}</p>
            </div>
            {btnText && <div className={styles.activityBtn}>{btnText}</div>}
        </div>
    )
}

/**
 * 跳转到指定页面
 * @param {string} url 跳转的URL
 * @param {string} eventId 埋点ID
 * @param {string} merchantId 商家ID
 */
const toUrl = (url, eventId, merchantId) => {
    // 跳转
    common.sendCxytj({ eventId, attr1: merchantId }); // 提交埋点
    jsApi.pushWindow(url); // 通过新view打开
}