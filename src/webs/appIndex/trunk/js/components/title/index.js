/**
 * APP 首页的title相关
 */


const Title = ({ title, onClick, moreUrl }) => {
    return (
        <div className='title' onClick={() => onClick && onClick()}>
            <span className={moreUrl ? 'titleText' : 'titleText noMoreUrl'}>{title}</span>
        </div>
    )
}

export default Title;