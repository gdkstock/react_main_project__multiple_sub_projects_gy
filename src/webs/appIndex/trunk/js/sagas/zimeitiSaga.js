/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_ZIMEITI_ASYNC,
    CREATE_ZIMEITI,
} from '../actions/actionsTypes.js'

import { baseFunc } from './baseSaga';

//server
import indexService from '../services/indexService'

/**
 * 获取首页附近商家的服务列表
 */
function* getZimeitiAsync(action) {
    yield baseFunc(indexService.getMedia, action, function* cb(res) {
        if (res.code == 1000) {
            yield put({
                type: CREATE_ZIMEITI,
                data: res.data && Object.keys(res.data).length > 0 ? res.data : {}
            })
        }
    });
}

function* watchGetZimeitiAsync() {
    yield takeEvery(GET_ZIMEITI_ASYNC, getZimeitiAsync)
}


export function* watchZimeiti() {
    yield [
        fork(watchGetZimeitiAsync),
    ]
}