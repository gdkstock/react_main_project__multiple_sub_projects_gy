/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_MAIN_TYPES_ASYNC,
    GET_SHOP_ASYNC,
    CREATE_MERCHANT_SERVICES,
    CREATE_MERCHANT_MORE_URL,
    CREATE_SERVICE_TYPES,
    CREATE_MARCHANT_INFO,
} from '../actions/actionsTypes.js'

import { baseFunc } from './baseSaga';

//server
import merchantService from '../services/merchantService'

//常用工具类
import ChMessage from '../utils/message.config'

/**
 * 获取首页附近商家的服务列表
 */
function* getMainTypesAsync(action) {
    yield baseFunc(merchantService.getMainTypes, action, function* cb(res) {
        if (res.code == 1000) {
            const { merchantServices, serviceTypes, moreUrl } = res.data;

            // 保存无商家时的服务列表
            if (merchantServices && merchantServices.length > 0) {
                yield put({
                    type: CREATE_MERCHANT_SERVICES,
                    data: merchantServices
                })
            }

            // 保存有商家时的服务器列表
            if (serviceTypes && serviceTypes.length > 0) {
                yield put({
                    type: CREATE_SERVICE_TYPES,
                    data: serviceTypes
                })
            }

            // 保存跳转URL
            yield put({
                type: CREATE_MERCHANT_MORE_URL,
                data: moreUrl
            })
        }
    });
}

function* watchGetMainTypesAsync() {
    yield takeEvery(GET_MAIN_TYPES_ASYNC, getMainTypesAsync)
}

/**
 * 获取首页附近商家的服务列表
 */
function* getShopAsync(action) {
    yield baseFunc(merchantService.getShop, action, function* cb(res) {
        if (res.code == 1000) {
            yield put({
                type: CREATE_MARCHANT_INFO,
                data: res.data && Object.keys(res.data).length > 0 ? res.data : {}
            })
        } else {
            // 请求失败则清空附近商家的信息
            yield put({
                type: CREATE_MARCHANT_INFO,
                data: {}
            })
        }
    });
}

function* watchGetShopAsync() {
    yield takeEvery(GET_SHOP_ASYNC, getShopAsync)
}


export function* watchMerchant() {
    yield [
        fork(watchGetMainTypesAsync),
        fork(watchGetShopAsync),
    ]
}