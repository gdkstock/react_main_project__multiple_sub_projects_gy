/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_ADVER_ASYNC,
    CREATE_ADVER,
} from '../actions/actionsTypes.js'

import { baseFunc } from './baseSaga';

//server
import indexService from '../services/indexService'

/**
 * 获取广告
 */
function* getAdverAsync(action) {
    yield baseFunc(indexService.getAppH5Advert, action, function* cb(res) {
        if (res.code == 1000) {
            yield put({
                type: CREATE_ADVER,
                data: res.data && Object.keys(res.data).length > 0 ? res.data : {}
            })
        }
    });
}

function* watchGetAdverAsync() {
    yield takeEvery(GET_ADVER_ASYNC, getAdverAsync)
}


export function* watchAdver() {
    yield [
        fork(watchGetAdverAsync),
    ]
}