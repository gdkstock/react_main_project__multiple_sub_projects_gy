/**
 * 附近商家相关接口
 */

import apiHelper from './apiHelper';
import { baseService } from './baseService';

class MerchantService {
  /**
   * 获取首页附近商家的服务列表
   * @param data data
   */
  getMainTypes(data) {
    return baseService(`${apiHelper.baseApiUrl}merchant/getMainTypes`, data);
  }

  /**
   * 获取附件商家主页服务列表
   * @param {object} data 
   */
  getShop(data) {
    return baseService(`${apiHelper.baseApiUrl}merchant/getShop`, data);
  }
}

export default new MerchantService()