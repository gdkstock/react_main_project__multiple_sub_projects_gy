/**
 * 首页相关接口
 */

import apiHelper from './apiHelper';

class IndexService {

    /**
     * 自媒体相关信息
     * @param {object} data
     */
    getMedia(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}getMedia`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取广告
     * @param {object} data
     */
    getAppH5Advert(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}getAppH5Advert`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
}

export default new IndexService()