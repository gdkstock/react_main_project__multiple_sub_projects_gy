// APP首页的H5页面

reducers的结构：{
    // 附近商家相关
    merchant:{
        // 没有附近商家时的服务列表
        merchantServices: [{
            eventId:"cxyapp_e_nearby_shopIndex_4s", // 埋点Id 提交时，需要在埋点Id前面加上“app_h5_”
            imgUrl:"https://pic.cx580.com:2443/gallery/20171129/violation/f5872405afa44633961e4f8addb95485.jpg",
            jumpUrl:"https://m.lechebang.com/duijie/cxy/index",
            name:"4s店铺保养",
            serviceDesc:"超值折扣 匠心品质",
        }],

        // 有商家是的服务 存在多个时，前端页面只显示前三个
        serviceTypes:[{
            code:"21001",
            eventId:"cxyapp_e_nearby_shopIndex_helpdo", // 埋点Id 提交时，需要在埋点Id前面加上“app_h5_”
            imgUrl:"https://pic.cx580.com:2443/gallery/20171129/violation/bb5c9159ea9b44f58827be641d79f7e4.jpg",
            name:"车务代办",
        }],
    }
}