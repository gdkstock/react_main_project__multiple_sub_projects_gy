/**
 * 社区相关接口
 */
import apiHelper from './apiHelper';
import { baseService } from './baseService';

class MemberService {

    /**
     * 用户个人信息接口
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * }
     */
    index(data) {
        return baseService(`${apiHelper.baseApiUrl}member/index`, data);
    }

    /**
     * 用户关注帖子列表
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * }
     */
    getFollowList(data) {
        return baseService(`${apiHelper.baseApiUrl}member/get_follow_list`, data);
    }

}
// 实例化后再导出
export default new MemberService()