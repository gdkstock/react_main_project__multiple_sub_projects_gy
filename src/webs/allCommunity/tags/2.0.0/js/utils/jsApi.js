import baseJsApi from 'app/utils/jsApi';
import common from './common'
import { setTimeout, clearTimeout } from 'timers';
import Broadcast from './broadcast'; // 广播类

import config from '../config';

class JsApi extends baseJsApi {
    constructor() {
        super();

    }

    ready(callback) {
        if (window.jsApiIsLoad) {
            callback();
        } else {
            document.addEventListener('jsApiIsReady', callback);
        }
    }

    /**
     * 拍照接口
     */
    picture(callback) {
        if (common.isCXYApp()) {
            if (!common.callbackPicture()) {
                return;
            }
            //默认
            let data = {
                "quality": 75,/*图片质量, 取值1到100*/
                "maxWidth": 768,/*图片的最大宽度. 过大将被等比缩小*/
                "maxSize": 500
            };
            let version = common.getAppVersion();

            if (version) {
                version = version.split(".");
                if ((version[0] * 1) > 6 || ((version[0] * 1) > 5 && (version[1] * 1) > 0)) {
                    //APP 6.1.0以上
                } else {
                    //APP 6.1.0以下
                    data = {
                        "quality": 75,/*图片质量, 取值1到100*/
                        "maxWidth": 512,/*图片的最大宽度. 过大将被等比缩小*/
                        "maxHeight": 512,/*图片的最大高度. 过大将被等比缩小*/
                        "maxSize": 300
                    };
                }
            }
            window.cx580.jsApi.call({
                "commandId": "",/*命令主键，每次调用，必须用全局唯一的字符串*/
                "command": "picture",/*执行的命令，原生APP会根据此值执行相应的逻辑*/
                "data": data
            }, function (result) {
                callback(result.data.dataBase64); //回调函数
            });
        }
    }


    /**
     * 图片预览查看
     * @param {array} imgs 图片url集合 {src:大图地址,msrc:缩略图地址}
     * @param {integer} index 开始幻灯片索引
     */
    openPhotoSwipe(imgs, index) {
        // 隐藏title栏
        this.hideTitleLayout();

        // 在history中添加一条新的记录，避免页面后退时直接关闭当前webView
        history.replaceState(Object.assign({}, history.state, { eventId: 'openPhotoSwipe' }), '', '');
        history.pushState({}, '', '');

        // 获取图片预览的容器节点
        const pswpElement = document.querySelectorAll('.pswp')[0];

        // 临时图片，避免网络差时 无法获取到图片的大小
        const tempImgs = imgs.map(item => ({
            src: item.src,
            w: 500,
            h: 500,
            msrc: item.msrc,
        }));

        // 参数社区
        const options = {
            history: false,
            focus: false,
            closeEl: false,
            shareEl: false,
            tapToClose: true,
            showAnimationDuration: 0,
            hideAnimationDuration: 0,
            index,
        };

        // 显示缩略图
        const tempGallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, tempImgs, options);

        // 延迟显示，存在缓存时不显示缩略图
        const tempGalleryHandle = setTimeout(() => tempGallery.init(), 100);

        const promiseImgs = imgs.map((item, i) => {
            return new Promise((resolve, reject) => {
                const img = new Image();
                img.src = item.src;
                img.onload = () => resolve({
                    src: item.src,
                    w: img.width || 500,
                    h: img.height || 500,
                    msrc: item.msrc,
                });
                img.onerror = () => resolve({
                    src: item.src,
                    w: 500,
                    h: 500,
                });
            });
        })

        Promise.all(promiseImgs)
            .then(res => {
                tempGallery.close();
                clearTimeout(tempGalleryHandle);
                const gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, res, options);
                gallery.init();

                // 监听关闭事件
                gallery.listen('close', () => {
                    if (window.location.hash !== '#/') {
                        this.showTitleLayout(); // 非首页则还原刚刚隐藏的title栏
                        history.back(); // 后退回刚刚pushState的页面，避免需要点击两次后退按钮才能后退回来源页面
                    }

                    // 移除返回键监听事件
                    document.removeEventListener('H5_COMMUNITY_POPSTATE', window.photoSwipeEvent);
                });

                // 监听返回键
                window.photoSwipeEvent = e => {
                    const { eventId } = e.msgObj;
                    if (eventId === 'openPhotoSwipe') {
                        gallery.close(); // 图片预览查看
                    }
                };
                document.addEventListener('H5_COMMUNITY_POPSTATE', window.photoSwipeEvent);
            });
    }

    /**
     * 显示title栏
     */
    showTitleLayout() {
        if (common.isCXYApp()) {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "controlTitleLayout",
                "data": {
                    "showStatus": "show",
                }
            }, function (data) { });
        }
    }

    /**
     * 隐藏title栏
     */
    hideTitleLayout() {
        if (common.isCXYApp()) {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "controlTitleLayout",
                "data": {
                    "showStatus": "hide",
                }
            }, function (data) { });
        }
    }

    /**
     * 发送广播
     * @param {string} eventId 广播id
     * @param {object} msgObj 广播id携带的数据（传递数据，必须符合Json格式）
     */
    sendBroadcast(eventId, msgObj) {
        this.ready(() => {
            if (common.isCXYApp() && false) {
                // app环境 因为IOS存在bug 这里这里不使用APP的广播事件
                window.cx580.jsApi.call({
                    "commandId": "",
                    "command": "sendBroadcast",
                    "data": {
                        eventId,
                        msgObj,
                    }
                }, data => {
                    // 广播成功
                    // alert(JSON.stringify(data))
                });
            } else {
                if (config.broadcastEventId.indexOf(eventId) === -1) {
                    alert('在配置文件中找不到需要发送的广播eventId：' + eventId);
                } else {
                    const broadcast = new Broadcast(); // 广播实例
                    broadcast.send(eventId, msgObj); // 发送广播
                }
            }
        });
    }

    /**
     * 监听广播
     * @param {string} eventId 广播id
     * @param {function} callback 存在广播时的回调函数；回调函数的第一个参数是发送广播时的msgObj参数
     */
    watchBroadcast(eventId, callback) {
        if (common.isCXYApp() && false) {
            // app环境 因为IOS存在bug 这里这里不使用APP的广播事件
            window['mybroadcast_' + eventId] = (msgObj) => callback(msgObj);
        } else {
            if (config.broadcastEventId.indexOf(eventId) === -1) {
                alert('在配置文件中找不到需要监听的广播eventId：' + eventId);
            } else {
                const broadcast = new Broadcast(); // 广播实例
                broadcast.watch(eventId, (msgObj) => callback(msgObj));
            }
        }
    }

    /**
     * APP 关闭webView
     */
    appClose() {
        this.ready(() => {
            if (common.isCXYApp()) {
                window.cx580.jsApi.call({
                    "commandId": "",
                    "command": "close"
                }, function (data) {

                });
            }
        });
    }

    /**
     * 自定义分享菜单栏
     * @param {object} data 
     */
    setShareMenu(data) {
        if (common.isCXYApp()) {
            this.ready(() => {
                // APP 分享jsdk
                window.appShare = () => {
                    const title = common.delHtmlTag(window.shareInfo ? window.shareInfo.title : document.title);
                    const shareIconId = document.getElementById('shareIcon');
                    const icon = shareIconId ? shareIconId.src : config.shareIcon; // 分享图片
                    window.cx580.jsApi.call({
                        "commandId": "",
                        "command": "share",
                        "data": {
                            "url": window.location.href,
                            "title":title,
                            "content":title,
                            "icon": icon,
                            "shareChannel": "weiXin,wenXinCircle",
                        }/*封装需求请求的数据*/
                    }, res => { });
                }

                window.cx580.jsApi.call({
                    "commandId": "",
                    "command": "customizeTopRightButton",
                    "data": {
                        "reset": "false",
                        "list": [
                            {
                                "text": "分享",
                                "callbackMethodName": "appShare"
                            },
                        ]
                    }
                }, res => { });
            })
        }
    }
}

// 实例化后再导出
export default new JsApi()