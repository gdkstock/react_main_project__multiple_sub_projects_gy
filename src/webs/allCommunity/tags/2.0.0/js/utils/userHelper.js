/**
 * 用户帮助类
 */

import config from '../config';
import common from '../utils/common';
import jsApi from '../utils/jsApi';
import { Toast, Modal } from 'antd-mobile';

class UserHelper {

    constructor() {

    }

    /**
     * 跳转到单点登录
     */
    toAuthUrl(url) {
        sessionStorage.clear(); //清空sessionStorage缓存
        url = url || window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.hash; //好像不用编码 编码反而出错？
        url = url.replace('#', '%23'); //替换#号
        url = url.split('?')[0]; //过滤？号
        window.location.replace(config.authUrl + url); //跳转到单点登录
    }

    /**
     * 获取单点登录的完整授权地址
     * @param {string} url 授权成功后的回调地址 
     */
    getAuthUrl(url) {
        sessionStorage.clear(); //清空sessionStorage缓存
        url = url || window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.hash; //好像不用编码 编码反而出错？
        url = url.replace('#', '%23'); //替换#号
        url = url.split('?')[0]; //过滤？号
        return config.authUrl + url; //跳转到单点登录
    }

    /**
     * 获取 userId 和 token
     */
    getUserIdAndToken() {
        return {
            userId: sessionStorage.getItem('userId'),
            token: sessionStorage.getItem('token'),
            userType: sessionStorage.getItem('userType'),
            authType: sessionStorage.getItem('authType'),
            deviceId: sessionStorage.getItem('deviceId')
        }
    }

    /**
     * 登陆
     * @param {string} url 登陆成功之后的回调地址
     */
    Login(url, newWebView = false) {
        //先判断是否已经存在userType 和 authType 避免页面接口返回4222时触发多个授权登录
        if (sessionStorage.getItem("userType") && sessionStorage.getItem("authType")) {

            // 根据是否需要打开新webView来执行不同的登录方法
            const func = () => {
                if (newWebView) {
                    jsApi.pushWindow(this.getAuthUrl(url));
                } else {
                    Toast.loading('登录中...', 0); // 显示加载中动画
                    this.toAuthUrl(url) //跳转到单点登录
                }
            };

            if (common.isCXYApp()) {
                // APP环境 未登录用户需要先提示用户登录
                return Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>此操作需要登录才能完成</span>, [
                    { text: '取消', onPress: () => false, style: { color: '#108ee9' } },
                    {
                        text: '登录', onPress: () => func(), style: { color: '#108ee9' }
                    }
                ]);
            } else {
                func();
            }
        }
    }

    /**
     * 已经登录
     */
    isLogin() {
        return sessionStorage.getItem('userId') && sessionStorage.getItem('token');
    }
};

// 实例化后再导出
export default new UserHelper()