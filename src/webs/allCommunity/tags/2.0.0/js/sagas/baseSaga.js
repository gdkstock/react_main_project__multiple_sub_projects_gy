/**
 * 基础saga
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'


//antd
import { Toast } from 'antd-mobile'

//常用工具类
import ChMessage from '../utils/message.config'


/**
 * 基础sage函数
 * @param {server} server 接口地址
 * @param {int} time 接口超时设置 单位毫秒
 * @param {Boolean} showLoading 显示加载中 默认：false
 * @param {function} cb 回调函数
 */
export function* baseFunc(server, action, cb, showLoading = false, time = 30000) {
    // 显示加载中
    if (showLoading) Toast.loading('', 0);

    try {
        const { result, timeout } = yield race({
            result: call(server, action.data),
            timeout: call(delay, time)
        })

        if (showLoading) Toast.hide();

        if (timeout) {
            showToastInfo();
            return;
        } else {
            // 先触发回调
            if (cb) yield cb(result);
            if (action.callback) action.callback(result);

            const { code } = result;
            if (code == '1000') {
                // 接口操作成功
            } else {
                showToastInfo(result.msg);
            }
        }
    } catch (error) {
        // 先触发回调
        if (cb) yield cb(error);
        if (action.callback) action.callback(error);

        showToastInfo();
    }
}

/**
 * 显示toast.info提示
 * @param {string} msg 提示信息
 */
const showToastInfo = msg => {
    Toast.hide(); // 显示toast之前先隐藏上一个toast 避免无法关闭
    return Toast.info(msg || ChMessage.FETCH_FAILED);
}