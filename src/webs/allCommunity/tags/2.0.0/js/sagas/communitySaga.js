/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    ADD_MEMBER_FOLLOW_ASYNC,
    GET_MEMBER_FOLLOW_ASYNC,
} from '../actions/actionsTypes.js'

import { baseFunc } from './baseSaga';

//antd
import { Toast } from 'antd-mobile'

//server
import communityService from '../services/communityService'

//常用工具类
import ChMessage from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库

/**
 * 获取已关注的社区
 */
function* getMemberFollowAsync(action) {
    yield baseFunc(communityService.getMemberFollow, action, yield function* cb(result) {
        if (result.code == '1000') {
            const group = new schema.Entity('data', {}, { idAttribute: 'groupId' });
            const groups = new schema.Array(group)
            const normalizedData = normalize(result.data.groups, groups);

            yield put({
                type: ADD_MEMBER_FOLLOW_ASYNC,
                data: normalizedData.entities.data,
                result: normalizedData.result
            })
        }
    }, true)
}

function* watchGetMemberFollowAsync() {
    yield takeLatest(GET_MEMBER_FOLLOW_ASYNC, getMemberFollowAsync)
}

export function* watchCommunity() {
    yield [
        fork(watchGetMemberFollowAsync),
    ]
}