/**
 * 城市列表、车型列表公共组件
*/
import React, { Component } from 'react'
import styles from './list.scss'

import OrtherService from '../../services/ortherService'
import UploadService from '../../services/imageService'

import { Toast } from 'antd-mobile'
import ChMessage from '../../utils/message.config'
import common from '../../utils/common'
import jsApi from '../../utils/jsApi'

const Item = props => {
	let { list, getName } = props

	return (
		<div className={styles.listCar}>
			{
				list.map((item, i) =>
					<div key={i} className={styles.car} onClick={() => getName(item)}>
						{item.img &&
							<div className={styles.img}>
								<img src={item.img} />
							</div>
						}
						<div className={styles.text}>{item.name}</div>
					</div>
				)
			}
		</div>
	)
}

const Group = props => {
	let { data, keyList, getName } = props
	return (
		<div>
			{
				keyList.map((item, i) =>
					<div className={styles.carBox} key={i}>
						{data[item] && data[item].length > 0 ?
							<div>
								<div className={styles.header} id={"scroll-" + item}>{item}</div>
								<Item list={data[item]} getName={(item) => getName(item)} />
							</div>
							: ''
						}
					</div>
				)
			}
		</div>
	)
}

const KeyList = props => {
	let { keyList, indexClick } = props
	return (
		<div className={styles.keyList}>
			{keyList.map((item, i) =>
				<div key={i} onClick={(keyName) => indexClick(item)}>{item}</div>
			)}
		</div>
	)
}

class List extends Component {
	constructor(props) {
		super(props)
		this.state = {
			list: {
				data: {},
				keyList: [],
			},
			scrollTopData: {

			},
			scrollInit: true
		}

	}

	componentWillMount() {
		let { typeId } = this.props.params;
		let title = "";
		switch (typeId) {
			case "carModel":
				title = "车型选择";
				break;
			case "city":
				title = "城市选择";
				break;
		}
		common.setViewTitle(title);
	}

	componentDidMount() {
		let { list } = this.state
		let { typeId } = this.props.params;
		if (typeId == "carModel") {
			//无缓存时请求车型数据
			if (!localStorage.getItem("carModelList")) {
				if (list.keyList.length == 0) {
					Toast.loading("", 0)
					OrtherService.getCarModelList().then((result) => {
						Toast.hide();
						if (result.code == "1000") {
							result.data.map(item => {
								let key = item.sortLetter;
								if (!list.data[key]) list.data[key] = [];
								list.data[key].push({
									id: item.id,
									img: item.logoFileName,
									name: item.brand,
								});
								if (list.keyList.indexOf(key) == -1) list.keyList.push(key);
							})
							//缓存车型数据
							localStorage.setItem("carModelList", JSON.stringify(list));
							this.setState({
								list
							})
						} else {
							if (result.msg) {
								Toast.info(result.msg, 2);
							}
						}
					}).catch((e) => {
						Toast.hide();
						Toast.info(ChMessage.FETCH_FAILED, 2);
					})
				}
			} else {
				this.setState({
					list: JSON.parse(localStorage.getItem("carModelList"))
				})
			}
		} else if (typeId == "city") {
			//无缓存时请求城市列表数据
			if (!localStorage.getItem("cityList")) {
				if (list.keyList.length == 0) {
					Toast.loading("", 0)
					OrtherService.getCityList().then((result) => {
						Toast.hide();
						if (result.code == "1000") {
							result.data.map(item => {
								let key = item.cityEname[0].toUpperCase();
								if (!list.data[key]) list.data[key] = [];
								list.data[key].push({
									id: item.cityId,
									name: item.cityName,
								});
								if (list.keyList.indexOf(key) == -1) list.keyList.push(key);
							})
							//索引排序
							list.keyList = list.keyList.sort()
							//缓存车型数据
							localStorage.setItem("cityList", JSON.stringify(list));
							this.setState({
								list
							})
						} else {
							if (result.msg) {
								Toast.info(result.msg, 2);
							}
						}
					}).catch((e) => {
						Toast.hide();
						Toast.info(ChMessage.FETCH_FAILED, 2);
					})
				}
			} else {
				this.setState({
					list: JSON.parse(localStorage.getItem("cityList"))
				})
			}
		}
	}

	componentDidUpdate() {
		let { scrollInit, list } = this.state
		if (list.keyList.length > 0) {
			if (scrollInit) {
				let scrollTopData = {};
				list.keyList.map(item => {
					let scrollTop = document.getElementById("scroll-" + item).offsetTop;
					scrollTopData[item] = scrollTop
				})
				this.setState({
					scrollTopData,
					scrollInit: false
				})
			}
		}
	}

	//选择车辆型号或城市名
	getName(item) {
		let { typeId } = this.props.params
		if (sessionStorage.getItem("questionAddState")) {
			let state = JSON.parse(sessionStorage.getItem("questionAddState"));
			state[typeId] = item.name;
			sessionStorage.setItem("questionAddState", JSON.stringify(state))
			jsApi.back();
		}
	}

	//索引点击
	indexClick(index) {
		Toast.hide();
		Toast.info(index, 1)
		let { scrollTopData } = this.state
		let scrollBody = document.getElementById("scroll-body");
		if (scrollTopData[index]) {
			scrollBody.scrollTop = scrollTopData[index];
		}
	}

	render() {
		let { list } = this.state
		let { keyList, data } = list
		return (
			<div className={styles.container} id="scroll-body">
				<Group {...list} getName={(item) => this.getName(item)} />
				<div className={styles.listIndexContainer}>
					<KeyList {...list} indexClick={(item) => this.indexClick(item)} />
				</div>
			</div>
		)
	}
}

export default List

