//评论组件
import styles from './comment.scss';

import common from '../../utils/common';

import jsApi from '../../utils/jsApi';

/**
 * 跳转到个人（他人）主页
 * @param {string} userId 
 */
const toHomePage = (userId) => {
    jsApi.pushWindow(common.getRootUrl() + `homePage/${userId}`);
}

//评论
const Item = props => {
    let { isWenda, isResolve, showCainaBtn, gid, tid, id, headImgUrl, author_username, content, thumbImgList, imgList, author_uid,
        datetime, replies, supports, is_owner, isPraise, reply_list, is_best,
        addReply, delReply, delComment, likeComment, bestComment } = props;
    let praiseIconClass = styles.icon + " " + (isPraise == "1" ? styles.useful : styles.agree);
    replies = reply_list ? reply_list.length : 0;

    // 图片阅览的数组格式
    const imgs = imgList && imgList.map((img, i) => ({
        src: img,
        msrc: thumbImgList[i]
    }));

    return (
        <div className={styles.item}>
            <div className={styles.author}>
                <img src={headImgUrl} onClick={() => toHomePage(author_uid)} />
                <p className={styles.userName + ' text-overflow-1'} onClick={() => toHomePage(author_uid)}>{author_username}</p>
                {is_best == 1 && <span className={styles.isBest}>采纳</span>}
                {
                    showCainaBtn && is_owner != 1 && <span className={styles.bestBox} onClick={() => bestComment && bestComment({
                        groupId: gid,
                        postsId: tid,
                        comId: id
                    })}>采纳</span>
                }
            </div>
            <div className={styles.content}>
                <h3 dangerouslySetInnerHTML={{ __html: content }}></h3>
                {thumbImgList && thumbImgList.length > 0 &&
                    <div className={styles.imgList}>
                        {
                            thumbImgList.map((item, index) =>
                                <img key={index} src={item} onClick={() => jsApi.openPhotoSwipe(imgs, index)} />)
                        }
                    </div>
                }
                {reply_list && reply_list.length > 0 &&
                    <div className={styles.replyList}>
                        {
                            reply_list.map(item =>
                                <p
                                    key={item.com_id + '-' + item.id}
                                    dangerouslySetInnerHTML={{ __html: item.reply_content }}
                                    onClick={() => item.is_owner != '1' ?
                                        addReply && addReply({
                                            groupId: gid,
                                            postsId: tid,
                                            com_id: id,
                                            pid: item.id,
                                            author_username: item.author_username,
                                        })
                                        :
                                        delReply && delReply({
                                            groupId: gid,
                                            postsId: tid,
                                            com_id: id,
                                            reply_id: item.id,
                                        })
                                    }
                                ></p>
                            )
                        }
                    </div>
                }
                <div className={styles.actionGroup}>
                    <span>{datetime}</span>
                    {is_owner != '1' ? '' : <span className={styles.del} onClick={() => delComment && delComment({
                        groupId: gid,
                        postsId: tid,
                        com_id: id,
                    })}>删除</span>}
                    <span className={styles.icon + " " + styles.reply} onClick={() => addReply && addReply({
                        groupId: gid,
                        postsId: tid,
                        com_id: id,
                        pid: '0',
                        author_username,
                    })}>回复({replies})</span>
                    <span className={praiseIconClass} onClick={() => likeComment && likeComment({
                        groupId: gid,
                        postsId: tid,
                        comId: id,
                        acType: isPraise != '1' ? '10' : '11',
                    })}>有用({supports})</span>
                </div>
            </div>
        </div>
    )
}

const Comment = props => {
    let { count, total, data, result } = props;

    return (
        <div>
            <div className={styles.head}>评论({count || total})</div>
            {result && result.map(item =>
                <Item
                    key={item}
                    {...data[item]}
                    {...props}
                />
            )}
            {
                total == 0 && count == 0 && <div className={styles.NOCommentList}>您的意见很重要，留下些建议吧。</div>
            }
        </div>
    )
}

export default Comment