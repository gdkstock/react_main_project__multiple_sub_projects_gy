/**
 * 用户信息相关组件
 */

import styles from './userInfo.scss';

const UserInfo = props => {
    let { username, headImgUrl, sex, signature } = props;
    return (
        <div className={styles.box} id='userInfoComponent'>
            <div className={styles.bg}>
                <img src='./images/homePageBg.png' />
            </div>
            <div className={styles.userAvatar}>
                <img
                    src={headImgUrl || './images/head.png'}
                    onError={e => setErrorImg(e.target, './images/head.png')} />
                <i className={sex == '女' ? styles.iconGril : styles.iconBoy}></i>
            </div>
            <div className={styles.userName}>
                <p className='text-overflow-1'>{username || '匿名易友'}</p>
            </div>
            <div className={styles.intro}>
                <p className='text-overflow-2'>{signature}</p>
            </div>
        </div>
    )
}

const setErrorImg = (e, img) => {
    if (e.src !== img) {
        e.src = img;
    }
}

export default UserInfo;