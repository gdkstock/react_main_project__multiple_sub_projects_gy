/**
 * 首页相关组件
 */

import React, { Component } from 'react';

//styles
import styles from './top.scss';

// 常用工具类
import common from '../../utils/common';

class Top extends Component {
    constructor(props) {
        super(props)

        const { defaultKey } = this.props;
        this.state = {
            //按钮的key
            keys: {
                '0': styles.left,
                '1': styles.right
            },

            //按钮初始样式
            btnsStyle: defaultKey == '0' ? styles.left : styles.right,

            //顶部tab的违章
            tabStyle: common.isCXYApp() ? common.isIPhone() ? 'fiexdTop20' : 'fiexdTop24' : '',
        }
    }

    btnsStyle(key) {
        let { keys } = this.state;

        this.setState({
            btnsStyle: keys[key]
        })

        let { onChange } = this.props;
        if (onChange) {
            onChange(key);
        }
    }

    render() {
        let { btnsStyle, tabStyle } = this.state;

        return (
            <div style={{ paddingBottom: '.88rem' }}>
                <div className={styles.top + ' ' + tabStyle}>
                    <div className={styles.btns + ' ' + btnsStyle}>
                        <span className={styles.wenda} onClick={() => this.btnsStyle('0')}>问答</span>
                        <span className={styles.huati} onClick={() => this.btnsStyle('1')}>话题</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Top
