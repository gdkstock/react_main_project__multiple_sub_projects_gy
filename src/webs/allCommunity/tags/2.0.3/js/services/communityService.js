/**
 * 社区相关接口
 */
import apiHelper from './apiHelper';
import { baseService } from './baseService';

class CommunityService {

    /**
     * 获取所有社区
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * }
     */
    getGroupList(data) {
        return baseService(`${apiHelper.baseApiUrl}groups/get_group_list`, data);
    }

    /**
     * 获取已关注的社区
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * }
     */
    getMemberFollow(data) {
        return baseService(`${apiHelper.baseApiUrl}groups/get_member_follow`, data);
    }


    /**
     * 更新关注
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * ,"groupIds":["1", "2"]//社区id数组
     * }
     */
    updateMemberFollow(data) {
        return baseService(`${apiHelper.baseApiUrl}groups/add_member_follow`, data);
    }

    /**
     * 获取社区广告图
     * @param data object
     * {
     * url:'', //图片Url
     * targetUrl:'' //跳转目标的URL
     * }
     */
    getAd(data) {
        return baseService(`${apiHelper.baseApiUrl}slide/index`, data);
    }

}
// 实例化后再导出
export default new CommunityService()