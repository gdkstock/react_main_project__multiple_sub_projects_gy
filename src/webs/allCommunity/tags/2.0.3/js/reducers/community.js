import {
    ADD_MEMBER_FOLLOW_ASYNC,
} from '../actions/actionsTypes'

const initState = {
    data: {},
    result: []
}

const community = (state = initState, action) => {
    switch (action.type) {
        case ADD_MEMBER_FOLLOW_ASYNC:
            //合并缓存中的数据，并且删除多余的groups
            action.result && action.result.map(groupId => action.data[groupId] = Object.assign({}, state.data[groupId], action.data[groupId]))
            return {
                data: action.data,
                result: action.result
            }

        default:
            return state;
    }
}

export default community