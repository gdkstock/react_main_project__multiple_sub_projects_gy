/**
 * 个人主页
 */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import { Tabs, Icon } from 'antd-mobile';
const TabPane = Tabs.TabPane;
import UserInfo from '../../components/homePage/userInfo';
import { PostsList, CommentList } from '../../components/homePage/posts';
import { GroupPostsList } from '../../components/group';
import LoadingMore from 'app/components/common/LoadingMore'; //上滑加载更多

//styles
import styles from './index.scss';

//actions
import * as memberActions from '../../actions/memberActions';
import * as postsActions from '../../actions/postsActions';

//常用工具类
import common from '../../utils/common';
import userHelper from '../../utils/userHelper';

class HomePage extends Component {
    constructor(props) {
        super(props)

        const { userId } = this.props.params;
        this.state = {

            //用户信息
            userInfo: {},

            //目标用户ID
            authorId: userId || sessionStorage.getItem('userId'),

            //当前页卡的key  可在titles中查看所有key以及其他信息
            homePageKey: 'myPosts',

            //个人主页的栏目
            titles: {
                myPosts: {
                    key: 'myPosts',
                    title: '我的发帖',
                    isEnd: 0,
                    list: [] //列表内容
                },
                myReply: {
                    key: 'myReply',
                    title: '我的回复',
                    isEnd: 0,
                    list: []
                },
                myFollow: {
                    key: 'myFollow',
                    title: '已关注',
                    isEnd: 0,
                    list: []
                }
            },

            //上滑加载更多
            loadingMoreProps: {
                showMsg: true, //显示文字提醒
                msgType: 0, // 对应msg字段的下标
                msg: [
                    '点击加载更多...',
                    <span className={styles.iconLoading}><Icon type='loading' />加载更多...</span>,
                    '已经到底啦',
                    '',
                    '还没有内容呢！'
                ], //文字提醒
                height: 100, //触发加载数据的高度 单位px
                loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
                line: true, //END时,是否添加贯穿文本的横线
            },

            //分页数量
            pageSize: 10,
        }
    }

    componentWillMount() {
        common.setViewTitle('主页');
        const { authorId } = this.state;

        // 游客模式下需要先登录
        if (authorId === sessionStorage.getItem('userId') && !sessionStorage.getItem('token')) {
            return userHelper.Login(undefined, false, false);
        } else {
            //获取用户个人信息
            this.props.memberActions.getMemberIndexAsync({ authorId }, res => {
                if (res.code == 1000) {
                    this.setState({
                        userInfo: res.data,
                    })
                }
            })

            //请求数据
            this.getDatas();

            // 访问自己的主页时清除小红点
            if (authorId === sessionStorage.getItem('userId')) {
                this.props.memberActions.updatePostsUnreadAsync({}); // 清除小红点
            }
        }
    }

    componentDidMount() {
        window.homePageScroll = () => this.homePageScroll();
        window.addEventListener('scroll', window.homePageScroll); //绑定页面滚动事件

        // 监听事件
        document.addEventListener('H5_COMMUNITY_UPDATA_POSTS', e => {
            setTimeout(() => this.props.postsActions.updataPosts(e.msgObj), 10);
        }); // 异步更新帖子信息

        document.addEventListener('H5_COMMUNITY_DELETE_POSTS', e => this.deletePosts(e.msgObj)); // 删除帖子
        document.addEventListener('H5_COMMUNITY_DELETE_COMMENT', e => this.getDatas(true)); // 删除评论事件
        document.addEventListener('H5_COMMUNITY_DELETE_REPLY', e => this.getDatas(true)); // 删除回复事件
        document.addEventListener('H5_COMMUNITY_UPDATA_FOLLOW', e => this.getDatas(true)); // 更新关注
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {
        window.removeEventListener('scroll', window.homePageScroll); //取消绑定页面滚动事件
    }


    /**
     * 页面滚动事件
     */
    homePageScroll() {
        const tabs = document.querySelector('.am-tabs-bar');
        if (tabs) {
            const obj = document.getElementById('userInfoComponent'); // 用户信息组件的dom节点
            // 超出指定高度后 则改变tab的展示方式
            if (document.body.scrollTop > obj.offsetHeight) {
                this.tabsStyles(1);
            } else {
                // 还原tab的展示方式
                this.tabsStyles(0);
            }
        }
    }

    /**
     * tab的样式
     * @param {number} type 0:无样式 1：悬浮在顶部
     */
    tabsStyles(type) {
        const tabs = document.querySelector('.am-tabs-bar');
        const tabsCon = document.querySelector('.am-tabs-content'); // tab内容的节点
        if (type) {
            tabs.className = 'am-tabs-bar fixedTabs';
            tabsCon.style.marginTop = tabs.offsetHeight + 'px';
        } else {
            tabs.className = 'am-tabs-bar';
            tabsCon.style.marginTop = '';
        }
    }

    /**
     * 加载更多
     */
    loadingMore() {
        let { homePageKey, titles } = this.state;

        if (titles[homePageKey].isEnd == '1') {
            return; //不再需要请求数据
        }

        //请求数据
        this.getDatas();
    }

    /**
     * 请求数据
     * @param {boolean} create 创建数据
     */
    getDatas(create = false) {
        const { authorId, pageSize, homePageKey, titles } = this.state;

        //改变加载文案
        this.setState({
            loadingMoreProps: this.getMsgIndexObject(1)
        });

        //请求下一页的数据 如果当前页数不足于分页，则重新获取第一页
        const pageNo = create ? 1 : Math.floor(titles[homePageKey].list.length / pageSize) + 1;

        if (homePageKey === 'myPosts') {
            //获取我的发帖
            this.props.postsActions.getMyPostsAsync({ pageSize, pageNo, authorId }, res => this.getDataCallBack(res, homePageKey, pageNo !== 1))
        } else if (homePageKey === 'myReply') {
            //获取我的评论
            this.props.postsActions.getMyCommentsAsync({ pageSize, pageNo }, res => this.getDataCallBack(res, homePageKey, pageNo !== 1))
        } else if (homePageKey === 'myFollow') {
            //获取关注帖子列表
            this.props.memberActions.getFollowListAsync({ pageSize, pageNo }, res => this.getDataCallBack(res, homePageKey, pageNo !== 1))
        }
    }

    /**
     * 请求数据的回调函数
     * @param {object} res 接口返回的数据
     * @param {object} homePageKey 页卡的key
     * @param {boolean} isAdd 追加内容
     */
    getDataCallBack(res, homePageKey, isAdd = true) {
        let { titles } = this.state;
        let msgType = 0; //默认点击加载更多
        if (res.code == 1000) {

            titles[homePageKey].list = isAdd ? [...titles[homePageKey].list, ...res.data.list] : res.data.list;
            titles[homePageKey].isEnd = res.data.isEnd * 1; //转为整数

            if (titles[homePageKey].isEnd == '1') { //已无新数据
                if (titles[homePageKey].list.length > 10) {
                    msgType = 2; // 超过10条
                } else if (titles[homePageKey].list.length === 0) {
                    // 列表为空
                    msgType = 4;
                } else {
                    msgType = 3; //不显示end
                }
            }
            this.setState({
                titles,
                loadingMoreProps: this.getMsgIndexObject(msgType)
            })
        } else {
            //改变加载文案
            this.setState({
                loadingMoreProps: this.getMsgIndexObject(msgType)
            });
        }
    }

    /**
     * 获取msg下标的对象结构体
     * @param {} index 索引
     */
    getMsgIndexObject(index) {
        let { loadingMoreProps } = this.state;
        return Object.assign({}, loadingMoreProps, {
            msgType: index, // 对应msg字段的下标
        })
    }

    /**
     * 栏目切换
     * @param {string} key 
     */
    tabsChange(key) {
        document.body.scrollTop = 0; //滚动到顶部

        this.setState({ homePageKey: key }, () => {
            let { homePageKey, titles } = this.state;
            let { isEnd, list } = titles[homePageKey]

            //数据不为空
            if (!isEnd) {

                //改变加载文案
                this.setState({
                    loadingMoreProps: this.getMsgIndexObject(0)
                })

                //如果没有数据 则请求数据
                if (list.length === 0) {
                    this.getDatas();
                }
            } else {
                //没有更多数据了 列表数据少于10条 则不显示end文案 一条都没有则显示“还没有内容呢”
                this.setState({
                    loadingMoreProps: this.getMsgIndexObject(list.length === 0 ? 4 : list.length < 10 ? 3 : 2)
                })
            }
        });
    }

    /**
     * 跳转到个人（他人）主页
     * @param {string} userId 
     */
    toHomePage(userId) {
        window.location.href = common.getRootUrl() + `homePage/${userId}`;
    }

    /**
     * 删除帖子
     * @param {object} msgObj { groupId, postsId } 
     */
    deletePosts({ groupId, postsId }) {
        const { titles } = this.state
        let { list } = titles.myPosts;
        list = list.filter(item => !(item.groupId == groupId && item.postsId == postsId));
        this.setState({
            titles: Object.assign({}, titles, {
                myPosts: Object.assign({}, titles.myPosts, {
                    list
                })
            })
        });

        // 重新请求数据
        this.getDatas();
    }

    render() {
        let { userInfo, homePageKey, titles, loadingMoreProps } = this.state;
        let { community, posts } = this.props;

        return (
            <div className='box' ref='userInfo'>
                <UserInfo {...userInfo} />
                <LoadingMore {...loadingMoreProps}>
                    {
                        //我的主页（isOwner=="1"） 或 他人主页
                        userInfo.isOwner == '1'
                            ? <Tabs className={'homePage ' + (homePageKey === 'myFollow' ? 'homePageFollow' : '')} animated={true} swipeable={false} onChange={(key) => this.tabsChange(key)}>
                                {
                                    Object.keys(titles).map((key, i) =>
                                        <TabPane tab={titles[key].title} key={titles[key].key}>
                                            {key === 'myPosts' && <PostsList myHomePage={userInfo.isOwner == '1'} posts={posts} list={titles[key].list} community={community} />}
                                            {key === 'myReply' && <CommentList list={titles[key].list} community={community} />}
                                            {key === 'myFollow' &&
                                                <GroupPostsList
                                                    posts={posts}
                                                    list={titles[key].list}
                                                    clickAvatar={posts => this.toHomePage(posts.authorId)}
                                                    clickUserName={posts => this.toHomePage(posts.authorId)}
                                                />
                                            }
                                        </TabPane>
                                    )
                                }
                            </Tabs>
                            : <PostsList myHomePage={userInfo.isOwner == '1'} posts={posts} list={titles['myPosts'].list} community={community} />
                    }
                </LoadingMore>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    community: state.community,
    posts: state.posts,
})

const mapDispatchToProps = dispatch => ({
    memberActions: bindActionCreators(memberActions, dispatch),
    postsActions: bindActionCreators(postsActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePage)