import React from 'react'
import { Route, IndexRoute } from 'react-router'

//antd UI
import { Toast } from 'antd-mobile'

import {
  App,
  Home,
  NotFoundPage,
  NewAppWebView,
} from './containers'

import HomePage from './containers/homePage' // 个人主页

import TopicAdd from './containers/topic/add' //新增话题
import QuestionAdd from './containers/question/add' //新增问题
import List from './containers/question/list' // 车型选择||城市选择
import PostDetail from './containers/detail' // 帖子详情

//显示加载中
const showLoading = () => {
  Toast.loading('', 30, () => {
    window.networkError('./images/networkError-icon.png')
  })
}

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />

    {/* 个人主页 */}
    <Route path="homePage(/:userId)" component={HomePage} />

    {/* 发帖 */}
    <Route path="topic">
      <Route path="add" component={TopicAdd}/>
    </Route>
    
    <Route path="question">
      <Route path="add" component={QuestionAdd}/>
      <Route path="list/:typeId" component={List}/> 
    </Route>

    {/* 帖子详情 */}
    <Route path="postDetail/:groupId/:postsId/:catid" component={PostDetail}/>

    {/* 用户跳转到单点登录之后，取消时不要关闭当前webView */}
    <Route path="newAppWebView" component={NewAppWebView}/>

    <Route path="*" component={NotFoundPage} />
  </Route>
);