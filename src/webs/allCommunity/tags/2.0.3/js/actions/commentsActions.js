/*
* 评论相关的actions
*/

import {
    GET_POST_COMMENT_LIST_ASYNC,
    ADD_COMMENT_ASYNC,
    DELETE_COMMENT_ASYNC,
    GET_POST_REPLY_LIST_ASYNC,
    ADD_REPLY_ASYNC,
    DELETE_REPLY_ASYNC,
    COMMENT_PRAISE_ASYNC,
    BEST_COMMENT_ASYNC,
} from './actionsTypes'

//获取帖子评论列表
export const getPostsCommentListAsync = (data,callback) => ({ type: GET_POST_COMMENT_LIST_ASYNC, data, callback })

//新增帖子评论
export const addCommentAsync = (data,callback) => ({ type: ADD_COMMENT_ASYNC, data, callback })

//删除帖子评论
export const deleteCommentAsync = (data,callback) => ({ type: DELETE_COMMENT_ASYNC, data, callback })

//获取回复列表
export const getPostsReplyListAsync = (data,callback) => ({ type: GET_POST_REPLY_LIST_ASYNC, data, callback })

//新增回复
export const addReplyAsync = (data,callback) => ({ type: ADD_REPLY_ASYNC, data, callback })

//删除回复
export const deleteReplayAsync = (data,callback) => ({ type: DELETE_REPLY_ASYNC, data, callback })

//评论点赞/取消点赞
export const commentPraiseAsync = (data, callback) => ({ type: COMMENT_PRAISE_ASYNC, data, callback })

//采纳评论
export const bestCommentAsync = (data, callback) => ({ type: BEST_COMMENT_ASYNC, data, callback })