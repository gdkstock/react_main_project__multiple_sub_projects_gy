/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    ADD_GROUP_LIST,
    ADD_POSTS_INDEX,
    GET_POSTS_INDEX_ASYNC,
    ADD_POST_ASYNC,
    GET_MY_POSTS_ASYNC,
    DELETE_MY_POSTS_ASYNC,
    DELETE_MY_POSTS,
    GET_MY_COMMENTS_ASYNC,
    POST_FOLLOW_ASYNC,
    POST_FOLLOW,
    GET_POST_DETAIL_ASYNC,
    POST_PRAISE_ASYNC,
    POST_PRAISE,
    POST_ADD_VOTE_ASYNC,
    POST_ADD_VOTE,
} from '../actions/actionsTypes.js'

import { baseFunc } from './baseSaga';

//antd
import { Toast } from 'antd-mobile'

//server
import postService from '../services/postService'
import commentService from '../services/commentService'

//常用工具类
import ChMessage from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库


/**
 * 根据栏目获取帖子
 */
function* getPostsIndexAsync(action) {
    yield baseFunc(postService.getPostsByGroupId, action, yield function* cb(result) {
        if (result.code == '1000') {
            const posts = new schema.Entity('data', {}, { idAttribute: 'postsId' });
            const postsList = new schema.Array(posts)
            const normalizedData = normalize(result.data.list, postsList);

            if (normalizedData.result.length > 0) {
                yield put({
                    type: ADD_POSTS_INDEX,
                    data: normalizedData.entities.data,
                    result: normalizedData.result
                })
            }
        }
    });
}

function* watchGetPostsIndexAsync() {
    yield takeEvery(GET_POSTS_INDEX_ASYNC, getPostsIndexAsync)
}

/**
 * 新增帖子
 */
function* addPostAsync(action) {
    yield baseFunc(postService.createPost, action, undefined, true);
}

function* watchAddPostAsync() {
    yield takeEvery(ADD_POST_ASYNC, addPostAsync)
}

/**
 * 我的发帖
 */
function* getMyPostsAsync(action) {
    yield baseFunc(postService.myPost, action, yield function* cb(result) {
        if (result.code == '1000') {
            const posts = new schema.Entity('data', {}, { idAttribute: 'postsId' });
            const postsList = new schema.Array(posts)
            const normalizedData = normalize(result.data.list, postsList);

            if (normalizedData.result.length > 0) {
                yield put({
                    type: ADD_POSTS_INDEX,
                    data: normalizedData.entities.data,
                    result: normalizedData.result
                })
            }
        }
    });
}

function* watchGetMyPostsAsync() {
    yield takeLatest(GET_MY_POSTS_ASYNC, getMyPostsAsync)
}

/**
 * 我的评论
 */
function* getMyCommentsAsync(action) {
    yield baseFunc(commentService.myComments, action, yield function* cb(result) {
        if (result.code == '1000') {
            const posts = new schema.Entity('data', {}, { idAttribute: 'postsId' });
            const postsList = new schema.Array(posts)
            const normalizedData = normalize(result.data.list, postsList);

            if (normalizedData.result.length > 0) {
                yield put({
                    type: ADD_POSTS_INDEX,
                    data: normalizedData.entities.data,
                    result: normalizedData.result
                })
            }
        }
    });
}

function* watchGetMyCommentsAsync() {
    yield takeLatest(GET_MY_COMMENTS_ASYNC, getMyCommentsAsync)
}

/**
 * 获取帖子详情
 */
function* getPostDetailAsync(action) {
    yield baseFunc(postService.getPostDetail, action);
}

function* watchGetPostDetailAsync() {
    yield takeLatest(GET_POST_DETAIL_ASYNC, getPostDetailAsync)
}

/**
 * 删除帖子
 */
function* deleteMyPostsAsync(action) {
    yield baseFunc(postService.deletePostById, action, yield function* cb(result) {
        if (result.code == '1000') {
            const { groupId, postsId } = action.data;
            yield put({
                type: DELETE_MY_POSTS,
                data: {
                    groupId,
                    postsId
                }
            })
        }
    }, true);
}

function* watchDeleteMyPostsAsync() {
    yield takeLatest(DELETE_MY_POSTS_ASYNC, deleteMyPostsAsync)
}

/**
 * 帖子关注/取消关注
 */
function* postFollowAsync(action) {
    yield baseFunc(postService.postFollow, action, yield function* cb(result) {
        if (result.code == '1000') {
            const { groupId, postsId, ac_type } = action.data;
            yield put({
                type: POST_FOLLOW,
                data: {
                    groupId,
                    postsId,
                    ac_type,
                }
            })
        }
    });
}

function* watchPostFollowAsync() {
    yield takeLatest(POST_FOLLOW_ASYNC, postFollowAsync)
}

/**
 * 帖子点赞/取消点赞
 */
function* postPraiseAsync(action) {
    //直接触发点赞相关 不等待接口返回
    yield put({
        type: POST_PRAISE,
        data: action.data
    });

    yield baseFunc(postService.praise, action);
}

function* watchPostPraiseAsync() {
    yield takeLatest(POST_PRAISE_ASYNC, postPraiseAsync)
}

/**
 * 投票
 */
function* postAddVoteAsync(action) {
    //直接触发投票相关 不等待接口返回
    yield put({
        type: POST_ADD_VOTE,
        data: action.data
    })

    yield baseFunc(postService.vote, action);
}

function* watchPostAddVoteAsync() {
    yield takeLatest(POST_ADD_VOTE_ASYNC, postAddVoteAsync)
}

export function* watchPosts() {
    yield [
        fork(watchGetPostsIndexAsync),
        fork(watchAddPostAsync),
        fork(watchGetMyPostsAsync),
        fork(watchDeleteMyPostsAsync),
        fork(watchGetMyCommentsAsync),
        fork(watchGetPostDetailAsync),
        fork(watchPostFollowAsync),
        fork(watchPostPraiseAsync),
        fork(watchPostAddVoteAsync),
    ]
}