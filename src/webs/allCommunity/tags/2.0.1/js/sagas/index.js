// saga 模块化引入
import { fork } from 'redux-saga/effects'

//社区
import { watchCommunity } from './communitySaga'

//帖子相关
import { watchPosts } from './postsSaga'

//评论相关
import { watchComments } from './commentSaga'

//个人主页相关
import { watchMember } from './memberSaga'

// 单一进入点，一次启动所有 Saga
export default function* rootSaga() {
  yield [
    fork(watchCommunity),
    fork(watchPosts),
    fork(watchMember),
    fork(watchComments),
  ]
}