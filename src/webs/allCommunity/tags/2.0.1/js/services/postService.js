/**
 * 帖子相关接口
 */
import apiHelper from './apiHelper';
import { baseService } from './baseService';

class PostService {


    /**
     * 根据社区获取帖子列表
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * ,"groupId":"问答Id"//社区id
     * ,"extends":[10,11]//1、默认是0   2、groupId = 问答Id, 跟我相关："10";未解决:"11";已解决:"12"   3、groupId = 吐槽Id,我的吐槽："20"; 易友吐槽: "21"
     * ,"pageNo":1//第一页  默认是1
     * ,"pageSize":10//分页数量 默认是10
     * ,"cityName":""//城市名称，易友吐槽的时候需要
     * ,direction:''//up 上划 down下拉。
     * ,postsId:''//根据最后一个帖子的ID来拉取数据
     * }
     */
    getPostsByGroupId(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/index`, data);
    }


    /**
     * 获取帖子详情
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * ,"groupId":"82345678"//社区id
     * ,"postsId":1//帖子ID
     * ,"Step":1//默认是1，可不传当是查看投票结果的时候，传2
     * }
     */
    getPostById(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/show`, data);
    }

    /**
     * 获取帖子的信息（点赞、评论数、投票数、是否已点赞等）
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * ,"groupId":"82345678"//社区id
     * ,"postsId":1//帖子ID
     * }
     */
    getPostsInfo(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/get_posts_info`, data);
    }

    /**
    * 帖子点赞
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":"82345678"//社区id
    * ,"postsId":1//帖子ID
    * ,"ac_type":1//默认为空 4：帖子点赞 5：帖子取消点赞
    * }
    */
    praise(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/praise`, data);
    }


    /**
    * 删除帖子
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":"82345678"//社区id
    * ,"postsId":1//帖子ID
    * }
    */
    deletePostById(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/del`, data);
    }


    /**
    * 发帖（话题，问答，投票）
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":"82345678"//社区id
    * ,"catid":""//分类ID
    * ,"content":""//内容
    * ,"imgList":["1.jpg", "2.jpg""]//图片
    * ,"vote_options":[]//投票选项
    * ,"points":0 //0：积分未增加或增加失败，> 0 是增加的积分值
    * }
    */
    createPost(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/add`, data);
    }


    /**
    * 投票
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":"82345678"//社区id
    * ,"postsId":""//帖子ID
    * ,"voteId":"",//选项ID
    * }
    */
    vote(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/add_vote`, data);
    }


    /**
    * 图片上传(暂不可用)
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"image":""//图片文件
    * }
    */
    upload(data) {
        return baseService(`${apiHelper.baseApiUrl}index/upload`, data);
    }

    /**
     * 我的帖子
     * @param data Object
     * {
     *  "userId": "", //用户id
     *  "pageNo": "1", //第一页  默认是1
     *  "pageSize": "10", //分页数量 默认是10
     *  "token": "" //token
     * }
     */
    myPost(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/mypost`, data);
    }

    /**
     * 获取帖子详情
     * @param data Object
     * {
     *  "userId": "", //用户id
     *  "groupId": "1", //社区
     *  "postsId": "10", //帖子id
     *  "token": "" ,//token
     * }
    */
    getPostDetail(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/show`, data);
    }

    /**
    * 帖子关注/取消关注
    * @param data Object
    * {
    *  "userId": "", //用户id
    *  "groupId": "1", //社区
    *  "postsId": "10", //帖子id
    *  "token": "" ,//token
    *  "ac_type":"31" //31 :关注；32：取消关注
    * }
    */
    postFollow(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/follow`, data);
    }

}
// 实例化后再导出
export default new PostService()