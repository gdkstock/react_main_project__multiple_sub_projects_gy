/**
 * 过滤组件
 */

import styles from './filter.scss';

const Filter = props => {

    let { list, index, show, onChange } = props;
    if (!index) {
        index = 0;
    }

    return (
        <div className={styles.box + ' ' + (show ? styles.show : styles.hide)}>
            <div className={styles.btns}>
                {
                    list.map((text, i) => <span
                        className={i === index ? styles.curr : styles.btn}
                        onClick={() => onChange && onChange(i)}
                    > {text}</span>)
                }
            </div>
            <div className={styles.bg} onClick={() => onChange && onChange()}></div>
        </div>
    )
}

export default Filter;