import {
    ADD_POSTS_INDEX,
    POST_PRAISE,
    POST_FOLLOW,
    DELETE_MY_POSTS,
    UPDATA_POSTS,
} from '../actions/actionsTypes'

const initState = {
    data: {},
    result: []
}

const posts = (state = initState, action) => {
    switch (action.type) {
        case ADD_POSTS_INDEX:
            // 合并缓存中的数据
            {
                const result = Array.from(new Set([...action.result, ...state.result])); // 合并数据并去重
                let data = {};

                result.map(id => {
                    // 判断数据是否存在
                    if (id) {
                        data[id] = Object.assign({}, state.data[id], action.data[id])
                    }
                })
                return {
                    data: data,
                    result: result
                }
            }
        // 删除帖子
        case DELETE_MY_POSTS:
            {
                const { postsId } = action.data;
                const result = state.result.filter(item => item !== postsId);
                let data = {};
                result.map(id => {
                    //判断数据是否存在
                    if (id) {
                        data[id] = state.data[id]
                    }
                })
                return {
                    data: data,
                    result: result
                }
            }
        // 更新帖子内容
        case UPDATA_POSTS:
            {
                const { postsId } = action.data;
                return {
                    data: Object.assign({}, state.data, { [postsId]: action.data }),
                    return: Array.from(new Set([...state.result, postsId])), // 合并去重
                }
            }
        // 点赞
        case POST_PRAISE:
            {
                let { postsId, ac_type } = action.data;
                let _state = Object.assign({}, state);
                //帖子存在
                if (_state.data[postsId]) {
                    _state.data[postsId].is_praise = ac_type == 4 ? "1" : "0";
                    _state.data[postsId].supports = _state.data[postsId].supports * 1 + (ac_type == 4 ? 1 : -1)
                    return _state
                }
            }
        // 关注
        case POST_FOLLOW:
            {
                let { postsId, ac_type } = action.data;
                let _state = Object.assign({}, state);
                //帖子存在
                if (_state.data[postsId]) {
                    _state.data[postsId].isFollow = ac_type == '31' ? '1' : '0';
                    return _state
                }
            }
        default:
            return state;
    }
}

export default posts