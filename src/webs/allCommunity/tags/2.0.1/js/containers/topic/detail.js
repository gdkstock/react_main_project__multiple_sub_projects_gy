/**
 * 话题详情页
 */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import { Comment } from '../../components/common'

class TopicDetail extends Component {
	constructor(props) {
		super(props);

		this.state = {
			
		}
	}

	render() {
		
		return (
			<div className='box whiteBg'>
				<Comment/>
			</div>
		);
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
	// actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
    mapDispatchToProps
)(TopicDetail);