/**
 * 新增话题页面
 */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import common from '../../utils/common'

import styles from './add.scss'

import * as postsActions from '../../actions/postsActions'
import UploadService from '../../services/imageService'
import jsApi from '../../utils/jsApi'

import { Toast, Modal } from 'antd-mobile'

class TopicAdd extends Component {
	constructor(props) {
		super(props);

		this.state = {
			title: "",//话题标题
			content: "",//问题内容
			imgs: [],//图片列表
			groupId: "",//选中栏目
			carModel: "",//车型
			city: "",//城市
			catid: "4",//分类id
			vote_options: [ //投票
				{
					vote_desc: "",
					vote_img_url: "",
				},
				{
					vote_desc: "",
					vote_img_url: "",
				}
			]
		}
	}

	componentWillMount() {
		common.setViewTitle("新话题");
	}

	componentDidMount() {

	}

	//标题改变
	titleChange(e) {
		let value = e.target.value
		let { title } = this.state
		if (e.target.value != title) {
			this.setState({
				title: value,
			})
		}
	}

	//内容改变
	contentChange(e) {
		let value = e.target.value
		let { content } = this.state
		if (e.target.value != content) {
			this.setState({
				content: value,
			})
		}
	}

	/**
	 * 图片上传
	 */
	picture(e) {
		if (this.state.imgs.length == 9) {
			Toast.info("最多只能上传9张图片", 2);
			return;
		}
		//在app时调用jsdk拍照接口（主要采用h5方式ios调用后title功能按钮为白色与背景同色）
		if (common.isCXYApp()) {
			jsApi.picture((base64) => {
				this.postImgToUploadImg(base64);
			})
		} else {
			this.uploadImgs(e, (base64) => {
				this.postImgToUploadImg(base64);
			});
		}
	}

	/**
	 * 投票选项图片上传
	*/
	voteItemPicture(e, index) {
		//在app时调用jsdk拍照接口（主要采用h5方式ios调用后title功能按钮为白色与背景同色）
		if (common.isCXYApp()) {
			jsApi.picture((base64) => {
				this.postVoteImgToUploadImg(base64, index);
			})
		} else {
			this.uploadImgs(e, (base64) => {
				this.postVoteImgToUploadImg(base64, index);
			});
		}
	}

	uploadImgs(e, callback) {
		let files = e.target.files;
		for (let i = 0; i < files.length; i++) {
			this._lrz(files[i], callback);
			e.target.value = ""; //清空文件上传的内容 避免上传同一张照片或是拍照时出现的图片无法展示的bug
		}
	}

	/**
	 * 图片压缩上传
	 * @param e 图片资源
	 */
	_lrz(e, callback) {
		try {
			let files = e;
			let quality = 1;
			if (files.size > 1024 * 1024 * 5) {
				quality = .5;
			}
			else if (files.size > 1024 * 1024 * 2) {
				quality = .5;
			}
			else if (files.size > 1024 * 1024) {
				quality = .5;
			}
			else if (files.size > 1024 * 500) {
				quality = .4;
			}
			else if (files.size > 1024 * 100) {
				quality = .5;
			} else {
				quality = .7;
			}
			return lrz(files, {
				width: 1024,
				quality: quality
			}).then((rst) => {
				// 处理成功会执行
				if (callback) {
					callback(rst.base64)
				}

			}).catch((err) => {
				// 处理失败会执行
				Toast.fail('上传失败!', 1);
			}).always(() => {
				// 不管是成功失败，都会执行
				// this.refs.uploadImgInput.value = ""; //清空文件上传的内容 避免上传同一张照片或是拍照时出现的图片无法展示的bug
			});
		} catch (e) {
			Toast.info("图片上传失败", 2)
			console.log("图片上传失败", e);
		}

	}

	/**
	 * 上传图片到服务器
	 * @param base64 base64图片
	 */
	postImgToUploadImg(base64) {
		//添加图片
		let imgs = this.state.imgs;
		let index = imgs.push({
			tempSrc: base64
		});
		this.setState({
			imgs: imgs
		})

		//上传图片到服务器
		let uploadimgData = {
			content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
			format: 'jpg',
			channel: sessionStorage.getItem("userType"),
			thumbnail: '200x200', //缩略图尺寸
			watermark: true //加水印
		}
		UploadService.uploadImg(uploadimgData).then(data => {
			if (data.code === 0) {
				imgs[index - 1].src = data.data.url;
				this.setState({
					imgs: imgs
				})
			} else {
				Toast.info(data.msg, 2);
			}
		}, () => {
			Toast.info("系统繁忙，请稍后再试");
		})
	}

	/**
	 * 上传投票选项到图片到服务器
	 * @param base64 base64图片
	 * index 选项下标
	 */
	postVoteImgToUploadImg(base64, index) {
		//添加图片
		let { vote_options } = this.state;

		//上传图片到服务器
		let uploadimgData = {
			content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
			format: 'jpg',
			channel: sessionStorage.getItem("userType"),
			thumbnail: '200x200', //缩略图尺寸
			watermark: true //加水印
		}
		UploadService.uploadImg(uploadimgData).then(data => {
			if (data.code === 0) {
				vote_options[index].vote_img_url = data.data.url;
				vote_options[index].tempSrc = base64
				this.setState({
					vote_options,
				})
			} else {
				Toast.info(data.msg, 2);
			}
		}, () => {
			Toast.info("系统繁忙，请稍后再试");
		})
	}

	// 删除图片
	deleteImg(index) {
		Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请确认是否删除</span>, [
			{
				text: '取消', onPress: () => false, style: { color: '#108ee9' }
			},
			{
				text: '删除', onPress: () => {
					const { imgs } = this.state;
					this.setState({
						imgs: imgs.filter((item, i) => i !== index)
					})
				}, style: { color: '#108ee9' }
			}
		])
	}

	//显示投票
	addVote() {
		this.setState({
			catid: "6",
			vote_options: [ //投票
				{
					vote_desc: "",
					vote_img_url: "",
				},
				{
					vote_desc: "",
					vote_img_url: "",
				}
			]
		})
	}

	//添加投票选项
	addVoteItem() {
		let { vote_options } = this.state
		vote_options.push({
			vote_desc: "",
			vote_img_url: "",
		})
		this.setState({
			vote_options,
		})
	}

	//删除投票选项
	munisVoteIcon(index) {
		let { vote_options } = this.state;
		vote_options.splice(index, 1);
		this.setState({
			vote_options,
			catid: vote_options.length > 0 ? '6' : '4'
		})
	}

	//删除投票选项图片
	deleteVoteItemImg(index) {
		Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请确认是否删除</span>, [
			{
				text: '取消', onPress: () => false, style: { color: '#108ee9' }
			},
			{
				text: '删除', onPress: () => {
					let { vote_options } = this.state;
					vote_options[index].tempSrc = "";
					vote_options[index].vote_img_url = "";
					this.setState({
						vote_options
					})
				}, style: { color: '#108ee9' }
			}
		])
	}

	//投票选项内容变化
	voteItemChange(e, index) {
		let value = e.target.value;
		let { vote_options } = this.state
		if (value != vote_options[index].vote_desc) {
			vote_options[index].vote_desc = value
			this.setState({
				vote_options
			})
		}
	}

	//发表问题
	submit() {
		let { groupId, content, imgs, catid, title, vote_options } = this.state
		let imgList = imgs.map(item => item.src)
		if (this.valiDate()) {
			//数据校验通过提交问题
			let params = {
				title,
				groupId,
				catid,
				content,
				imgList,
			}
			if (catid == "6") {
				params.vote_options = vote_options.map(item => {
					const { vote_desc, vote_img_url } = item;
					return JSON.stringify({
						vote_desc,
						vote_img_url
					})
				});
			}
			this.props.postsActions.addPostAsync(params, (result) => {
				if (result.code == "1000") {
					jsApi.sendBroadcast('H5_COMMUNITY_ADD_POSTS', params); // 广播发帖成功

					let data = result.data
					let text = data.points && data.points > 0 ? `发布成功 +${data.points}分` : '发布成功';
					Toast.info(text, 1, () => {
						jsApi.back();
					});
				}
			})
		}
	}

	//校验数据
	valiDate() {
		let { content, title, catid, vote_options } = this.state;

		//校验话题标题
		if (!title) {
			Toast.info("标题不能为空", 2)
			return false
		}

		//校验话题内容
		if (!content) {
			Toast.info("话题内容不能为空", 2)
			return false
		}

		//校验投票选项
		if (catid == "6") {
			if (vote_options.length < 2) {
				Toast.info('投票选项至少需要两项', 2);
				return false;
			}
			for (let i = 0; i < vote_options.length; i++) {
				let item = vote_options[i];
				if (!item.vote_desc.trim()) {
					Toast.info(`投票选项${i + 1}内容不能为空`, 2)
					return false
				}
			}
		}

		return true;

	}

	//自动适应高度变化
	textareaAutoHight(o) {
		o = o.target;
		// o.style.height = "auto";
		o.style.height = o.scrollTop + o.scrollHeight + "px";
	}

	render() {
		let { title, content, imgs, catid, vote_options } = this.state

		// 图片阅览的数组格式
		const psImgs = imgs && imgs.map((item, i) => ({
			src: item.tempSrc,
		}))

		// 投票图片阅览的数组格式
		const voteImgs = vote_options && vote_options.map((item, i) => ({
			src: item.tempSrc || item.src,
		}))

		return (
			<div className='box' style={{ paddingBottom: "1rem" }}>
				<div className='whiteBg'></div>
				<div className={styles.topic}>
					<input placeholder="起个响亮的标题" value={title} onChange={(e) => this.titleChange(e)} />
				</div>
				<div className={styles.content}>
					<textarea style={{ width: "100%", height: "2.7rem" }} placeholder="在这里输入正文" value={content} onChange={(e) => this.contentChange(e)}></textarea>
				</div>
				<div className={styles.imgList}>
					{/*图片列表*/}
					{
						imgs.map((item, index) =>
							<div className={styles.imgBox} key={`topicImg-${index}`}>
								<img src={item.tempSrc || item.src} key={index} onClick={() => jsApi.openPhotoSwipe(psImgs, index)} />
								<div className={styles.deleteImgIcon} onClick={() => this.deleteImg(index)}></div>
							</div>
						)
					}
					{imgs.length < 9 ? common.isCXYApp() ? <div className={styles.picAddBtn} onClick={() => this.picture()}></div> :
						<label className={styles.picAddBtn} htmlFor="imgAddBtn">
							<input id="imgAddBtn" type="file" style={{ display: "none" }} accept="image/*" onChange={(e) => this.picture(e)} />
						</label> : ""
					}
				</div>
				<div className="h16"></div>
				{catid == "4" && <div className={styles.voteBtn} onClick={() => this.addVote()}>
					<span>发起投票</span>
				</div>
				}
				{catid == "6" &&
					<div className={styles.voteContainer}>
						<h3>添加投票</h3>
						<div className={styles.voteList}>
							{vote_options.map((item, index) =>
								<div key={'voteBox-' + index} className={styles.voteItem}>
									<div className={styles.minusIcon} onClick={() => this.munisVoteIcon(index)}></div>
									<textarea
										key={'vote-textarea-' + index}
										style={{ width: "100%", height: "0.6rem", border: "1px solid transparent" }}
										placeholder={`选项${index + 1}: 在此输入文字…`}
										onInput={(e) => this.textareaAutoHight(e)}
										onChange={(e) => this.voteItemChange(e, index)}
										maxLength="20"
										value={item.vote_desc}
										ref={"voteItem" + index}
									></textarea>
									{
										item.tempSrc ?
											<div className={styles.selectImg}>
												<img src={item.tempSrc} onClick={() => jsApi.openPhotoSwipe(voteImgs, index)} />
												<div className={styles.deleteImgIcon} onClick={() => this.deleteVoteItemImg(index)}></div>
											</div>
											: <label id={"voteItemImg" + index} className={styles.defaultImg} onClick={common.isCXYApp() ? (e) => { this.voteItemPicture(e, index) } : () => { }}>
												{!common.isCXYApp() &&
													<input id={"voteItemImg" + index} type="file" accept="image/*" style={{ display: "none" }} onChange={(e) => this.voteItemPicture(e, index)} />
												}
											</label>
									}
								</div>
							)
							}
							<div onClick={() => this.addVoteItem()}><div className={styles.addIcon}></div>添加选项</div>
						</div>
					</div>
				}
				<div className="h80"></div>
				<div className="fixed_b_container">
					<div className="sq_btn" onClick={() => this.submit()}>发表话题</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
	postsActions: bindActionCreators(postsActions, dispatch),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TopicAdd);