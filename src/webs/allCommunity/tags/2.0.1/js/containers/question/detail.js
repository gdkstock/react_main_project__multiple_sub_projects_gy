/**
 * 问题详情页
 */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import { Comment } from '../../components/common'

import styles from './detail.scss'

const imgSrc = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1507805892057&di=fa52aa5124e975c5b709837e07dd5a7a&imgtype=0&src=http%3A%2F%2Fwww4.qqjay.com%2Fu%2Ffiles%2F2012%2F0420%2Faf94a4ffc82b24f2e612504931889649.jpg"

class QuestionDetail extends Component {
	constructor(props) {
		super(props);

		this.state = {
			
		}
	}

	render() {
		
		return (
			<div className='box whiteBg'>
                <div className={styles.questionContent}>
                    <div className={styles.head}>
                        <img src={imgSrc}/>
                        <div className={styles.author}>
                            <p>自然而燃</p>
                            <p>08-18 09:00 提问</p>
                        </div>
                        <div className={styles.label}>违章 · 广州</div>
                    </div>
                    <div className={styles.title}>个人认为开车的天赋所有人都有，只不过是女人开车的总体比例比较少，一旦出事故便放大了来说。</div>
                    <div className={styles.imgList}>
                        <img src={imgSrc}/>
                        <img src={imgSrc}/>
                        <img src={imgSrc}/>
                        <img src={imgSrc}/>
                        <img src={imgSrc}/>
                        <img src={imgSrc}/>
                        <img src={imgSrc}/>
                    </div>
                    <div>
                        
                    </div>
                </div>
                <div className="h16"></div>
				<Comment/>
			</div>
		);
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
	// actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
    mapDispatchToProps
)(QuestionDetail);