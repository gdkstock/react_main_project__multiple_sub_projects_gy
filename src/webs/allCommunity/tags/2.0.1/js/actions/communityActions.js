/*
* 社区相关的actions
*/

import {
    GET_MEMBER_FOLLOW_ASYNC,
} from './actionsTypes'

//获取用户已关注的社区
export const getMemberFollowAsync = (data, callback) => ({ type: GET_MEMBER_FOLLOW_ASYNC, data, callback })

