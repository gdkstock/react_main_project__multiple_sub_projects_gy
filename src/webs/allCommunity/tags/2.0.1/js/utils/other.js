/**
 * URL重定向
 */
import { Toast } from 'antd-mobile'

// 配置文件
import config from '../config'

// 常用工具类
import common from '../utils/common';

// jsApi
import jsApi from '../utils/jsApi';

class Other {
    constructor() {
        this.init();
    }

    init() {
        this.cxytjInit() // 埋点JS初始化
        setInterval(() => this.historyInit(), 100) // 避免微信后退不刷新，所以这里定时循环
        this.watchDocumentClick(); // 监听全局点击事件

        // 回到栈顶事件
        document.addEventListener('visibilitychange', () => {
            if (!document.hidden) {
                this.myCxyPageResume();
            }
        })

        // 每当处于激活状态的历史记录条目发生变化时,popstate事件就会在对应window对象上触发
        window.onpopstate = e => common.dispatchEvent('H5_COMMUNITY_POPSTATE', e.state); // 分派更新帖子事件
    }

    /**
     * history相关初始化
     */
    historyInit() {
        if (window.history.state) {

            let { urlReplace, historyGo } = window.history.state;

            //重定向到指定页面
            if (urlReplace) {
                document.getElementById('app').style.display = 'none' //隐藏界面，提升交互体验
                Toast.loading('', 0)
                let url = urlReplace
                window.history.replaceState({}, '', ''); //清空需要跳转的URL
                window.location.replace(url) //重定向
                return;
            }

            //页面后退
            if (historyGo) {
                window.history.go(historyGo);
                return;
            }
        }
    }

    /**
     * 埋点JS初始化
     */
    cxytjInit() {
        if (window.cxytjIsReady) {
            //初始化通用数据
            this.cxytjInitData();
        } else {
            window.cxytjReady = () => {
                this.cxytjInitData();
            }
        }
    }

    /**
     * 埋点JS初始化的数据
     */
    cxytjInitData() {
        window.cxytj.init({ //以下为初始化示例，可新增或删减字段
            productId: 'allViolation', //产品ID
            productVersion: '1.0', //产品版本
            channel: sessionStorage.getItem('userType'), //推广渠道
            isProduction: config.production, //生产环境or测试环境 默认测试环境
            userId: sessionStorage.getItem('userId'), //用户ID
        });
    }

    /**
     * 监听全局点击事件
     */
    watchDocumentClick() {
        document.addEventListener('click', (e) => {
            const xpath = this.getXpath(e.target);
            if (this.isToast(xpath)) {
                // 隐藏toast 轻提示
                if (xpath.indexOf('am-toast-text-icon') !== -1 || e.target.innerHTML.indexOf('am-toast-text-icon') !== -1) {
                    // 点击了加载中、错误、正确等带有图标的toast提示时不执行关闭toast
                } else {
                    Toast.hide(); // 关闭toast
                }
            }
        })
    }

    isToast(xpath) {
        return xpath.includes('div.am-toast>div>body>html');
    }

    /**
     * 获取点击元素的dom路径
     * @param {object} obj 点击的节点对象
     */
    getXpath(obj) {
        let nodeName, id, className, xpath = '';
        while (nodeName != 'html') {
            nodeName = obj.nodeName.toLocaleLowerCase()
            id = obj.id ? '#' + obj.id : '';
            className = obj.className ? '.' + obj.className.replace(' ', ',') : '';
            xpath += nodeName + id + className + '>';
            obj = obj.parentNode;
        }

        return xpath.substr(0, xpath.length - 1);
    }

	/**
	 * 回到栈顶事件
	 */
    myCxyPageResume() {
        // 监听广播
        jsApi.watchBroadcast('H5_COMMUNITY_UPDATA_POSTS', msgObj => {
            return common.dispatchEvent('H5_COMMUNITY_UPDATA_POSTS', msgObj); // 分派更新帖子事件
        });

        jsApi.watchBroadcast('H5_COMMUNITY_DELETE_POSTS', msgObj => {
            return common.dispatchEvent('H5_COMMUNITY_DELETE_POSTS', msgObj); // 分派删除帖子事件
        });
        jsApi.watchBroadcast('H5_COMMUNITY_ADD_POSTS', msgObj => {
            return common.dispatchEvent('H5_COMMUNITY_ADD_POSTS', msgObj); // 分派新增帖子事件
        })

        // 非首页时默认显示title栏
        if (window.location.hash !== '#/') {
            jsApi.showTitleLayout();
        } else {
            jsApi.hideTitleLayout();
        }
    }

}

// 实例化后再导出
export default new Other()


/**
 * 全局错误异常捕捉
 */
window.onerror = (msg, url, l) => {
    var txt = '';
    txt = "There was an error on this page.\n\n";
    txt += "Error: " + msg + "\n";
    txt += "URL: " + url + "\n";
    txt += "Line: " + l + "\n\n";
    txt += "Click OK to continue.\n\n";
    console.log('全局错误异常捕捉：', txt);
    if (config.debugUsers.indexOf(sessionStorage.getItem('userId')) > -1) {
        alert(txt);//测试环境
    } else {
        return true; //正式环境屏蔽错误
    }
}