import baseAuth from 'app/utils/auth';
import config from '../config'
import Broadcast from '../utils/broadcast';

/**
 * 登录授权
 */
class Auth extends baseAuth {

    constructor() {
        super(config);

        // 回到栈顶事件
        document.addEventListener('visibilitychange', () => {
            if (!document.hidden) {
                this.autoSetUserInfo(); // 每次回到栈顶 会自动获取一次用户信息
            }
        })
    }

    /**
     * 自动获取用户信息 并且 保存到sessionStorage中
     */
    autoSetUserInfo() {
        const userInfo = this.getUserInfo();
        this.setUserInfo(userInfo);
    }
};

// 实例化后再导出
export default new Auth()

