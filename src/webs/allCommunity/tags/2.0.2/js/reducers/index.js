import { combineReducers } from 'redux'
import user from './users'

import community from './community'

import posts from './posts'

const rootReducer = combineReducers({
    community,
    posts,
    user, //用户信息表
});

export default rootReducer;
