/**
 * 帖子相关
 */

import styles from './posts.scss';

import jsApi from '../../utils/jsApi';

import common from '../../utils/common';

/**
 * 帖子列表
 * @param {object} props 
 */
export const PostsList = props => {
    let { myHomePage, posts, list, community } = props;
    return (
        <div className={styles.list}>
            {
                list && list.map((item, i) =>
                    <div>
                        {/* 日期标题  */}
                        {(i === 0 || list[i - 1].addtime !== item.addtime) && <DateTitle title={item.addtime.replace(/-/g, '/')} />}

                        <Posts
                            key={item.groupId + '-' + item.postsId}
                            myHomePage={myHomePage}
                            {...(Object.assign({},
                                item,
                                posts && posts.data && posts.data[item.postsId]
                                    ? posts.data[item.postsId]
                                    : {}
                            )
                            ) } //这里使用全局的state覆盖当前state
                            groupName={community && community.data && community.data[item.groupId] ? community.data[item.groupId].groupName : ''}
                        />

                        {/* 如果下一条还是在同一个日期标题分组内 则用间距组件将其分隔 */}
                        {list[i + 1] && (list[i + 1].addtime === item.addtime) && <div className='h16'></div>}
                    </div>
                )
            }
        </div>
    )
}

/**
 * 帖子
 * @param {object} props 
 */
const Posts = props => {
    let { myHomePage, groupId, postsId, catid, qa, groupName, city, title, content, comments, supports, is_praise } = props;

    const isHuati = catid != '5'; //话题类型
    props.isHuati = isHuati;

    //tags
    let tags = [];

    if (!isHuati) {
        //问答
        if (groupName) {
            tags.push(groupName);
        }
        if (city) {
            tags.push(city);
        }
    }

    tags = tags.join('·'); //转为字符串

    return (
        <div
            className={isHuati ? styles.huati : styles.wenda}
            onClick={() => jsApi.pushWindow(common.getRootUrl() + `postDetail/${groupId}/${postsId}/${catid}`)}>
            {
                !isHuati && myHomePage &&
                <Reply content={qa.bestComment && qa.bestComment.commContent ? qa.bestComment.commContent : '暂无评论'} />
            }
            <PostsTop tags={tags} isHuati={isHuati} />
            <Title isHuati={isHuati} content={title || content} />
            <PostsFooter is_praise={is_praise} isHuati={isHuati} comments={comments} supports={supports} />
        </div>
    )
}


/**
 * 评论列表
 * @param {object} props 
 */
export const CommentList = props => {
    let { list, community } = props;

    return (
        <div className={styles.list}>
            {
                list && list.map((item, i) =>
                    <div>
                        {/* 日期标题  */}
                        {(i === 0 || list[i - 1].addtime !== item.addtime) && <DateTitle {...item}  title={item.addtime.replace(/-/g, '/')} />}

                        <Comment
                            key={item.gid + '-' + item.tid + '-' + item.id}
                            {...item}
                            groupName={community && community.data && community.data[item.groupId] ? community.data[item.groupId].groupName : ''}
                        />

                        {/* 如果下一条还是在同一个日期标题分组内 则用间距组件将其分隔 */}
                        {list[i + 1] && (list[i + 1].addtime === item.addtime) && <div className='h16'></div>}
                    </div>
                )
            }
        </div>
    )
}

/**
 * 评论
 * @param {object} props 
 */
const Comment = props => {
    let { groupId, postsId, catid, catId, content, title } = props;
    catid = catid || catId;
    const isHuati = catid != '5'; //话题类型
    props.isHuati = isHuati;

    //tags
    let tags = [];
    tags = tags.join('·'); //转为字符串

    return (
        <div
            className={styles.wenda}
            onClick={() => jsApi.pushWindow(common.getRootUrl() + `postDetail/${groupId}/${postsId}/${catid}`)}
        >
            <Reply content={content || '暂无评论'} />
            <PostsTop tags={tags} isHuati={isHuati} />
            <Title isHuati={isHuati} content={title} />
            <div style={{ height: '.38rem' }}></div>
        </div>
    )
}


/**
 * 帖子顶部类型及相关标签
 * @param {object} props 
 */
const PostsTop = props => {
    let { isHuati, tags } = props;

    return (
        <div className={styles.postsTop}>
            {
                isHuati
                    ? <span className={styles.iconHuati}>话题</span>
                    : <span className={styles.iconWenda}>问题</span>
            }
            <span className={styles.tags}>{tags}</span>
        </div>
    )
}

/**
 * 标题
 * @param {object} props 
 */
const Title = props => {
    let { content, isHuati } = props;
    var rawHTML = {
        __html: content
    };
    return (
        <div className={styles.title} style={isHuati ? { color: '#1a1a1a' } : { color: '#6a6a6a' }}>
            <p dangerouslySetInnerHTML={rawHTML}></p>
        </div>
    )
}

/**
 * 回复
 * @param {object} props 
 */
const Reply = props => {
    let { content } = props;
    return (
        <div className={content ? styles.reply : 'hide'}>
            {
                content && <p className='text-overflow-2' dangerouslySetInnerHTML={{
                    __html: content
                }}></p>
            }
        </div>
    )
}

//图片
const Imgs = props => {
    let { thumbImgList, imgList, postsId } = props;
    let className = '';
    thumbImgList = thumbImgList || [];
    if (thumbImgList.length === 1) {
        className = styles.img;
    } else if (thumbImgList.length > 1) {
        className = styles.imgs;
    } else {
        className = 'hide';
    }


    return (
        <div className={styles.img}>
            {
                thumbImgList.map((img, i) =>
                    <img
                        key={img + postsId}
                        src={img}
                    />
                )
            }
        </div>
    )
}

/**
 * 帖子底部 点赞 评论数
 * @param {object} props 
 */
const PostsFooter = props => {
    let { comments, supports, isHuati, is_praise } = props;
    const likeBtn = !isHuati ? styles.likeBtn : styles.likeBtn2; // 按钮风格
    const isLike = is_praise == '1' ? ' ' + styles.isLike : ''; // 是否点赞
    return (
        <div className={styles.footer}>
            <span className={styles.replyBtn}>{!isHuati ? `回答(${comments})` : comments}</span>
            <span className={likeBtn + ' ' + isLike}>{!isHuati ? `有用(${supports})` : supports}</span>
        </div>
    )
}

/**
 * 日期标题
 * @param {object} props 
 */
export const DateTitle = props => {
    let { title } = props;
    let t = new Date().getTime();
    let today = common.formatDate(t, '/').substr(0, 10);
    let yesterday = common.formatDate(t - 3600 * 24 * 1000, '/').substr(0, 10);

    if (title == today) {
        title = '今天';
    } else if (title == yesterday) {
        title = '昨天';
    }

    return (
        <div className={styles.dateTitle}>
            <span>{title}</span>
        </div>
    )
}