/**
 * 其他接口
 */
import apiHelper from './apiHelper';
import { baseService } from './baseService';

class OrtherService {

    /**
     * 获取车辆模型数据
     * }
     */
    getCarModelList(data) {
        return baseService(`${apiHelper.baseApiUrl}ajax/get_brandList`, data, 'get');
    }

    /**
     * 获取城市列表数据
     * }
     */
    getCityList(data) {
        return baseService(`${apiHelper.baseApiUrl}ajax/get_cityList`, data, 'get');
    }

}
// 实例化后再导出
export default new OrtherService()