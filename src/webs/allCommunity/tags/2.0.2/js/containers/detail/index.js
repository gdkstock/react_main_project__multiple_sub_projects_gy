/**
 * 帖子详情页
 */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import { Toast, Modal, Icon } from 'antd-mobile'
import { Comment, InputGroup } from '../../components/common'
import LoadingMore from 'app/components/common/LoadingMore'; //上滑加载更多

import * as postsActions from '../../actions/postsActions'
import * as commentsActions from '../../actions/commentsActions'
import postService from '../../services/postService'

import common from '../../utils/common'
import userHelper from '../../utils/userHelper';
import ChMessage from '../../utils/message.config'

import styles from './index.scss'

// jsApi
import jsApi from '../../utils/jsApi';

class PostDetail extends Component {
    constructor(props) {
        super(props);

        const { catid } = this.props.params;
        this.state = {
            isWenda: catid != '4' && catid != '6', //问答类型 catid=4 || catid=6时为话题类型；所有未知类型都默认展示问答类型
            isInputShow: false,//是否显示输入框
            isReply: false, // true:回复评论输入框 false:评论输入框
            placeholder: '', // 输入框提示信息
            post: {},
            comments: {
                count: 0,
                isEnd: '0',
                data: {},
                result: [],
            },
            pageNo: 1, // 评论列表页码
            pageSize: 10, // 每页数量

            //回复评论的数据
            replyInfo: {

            },
            showReplyInput: false, // 显示回复评论输入框
            checkedVote: -1, //选中的投票索引 默认不选中

            //上滑加载更多
            loadingMoreProps: {
                showMsg: true, //显示文字提醒
                msgType: 0, // 对应msg字段的下标
                msg: [
                    '加载更多...',
                    <span className={styles.iconLoading}><Icon type='loading' />加载更多...</span>,
                    '已经到底啦',
                    ''
                ], //文字提醒
                height: 100, //触发加载数据的高度 单位px
                loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
                line: true, //END时,是否添加贯穿文本的横线
            },
        }
    }

    componentWillMount() {
        //设置标题
        const { catid } = this.props.params;
        let title = catid == "5" ? "问答详情" : (catid == "4" || catid == "6") ? "话题详情" : "帖子详情";
        if (!common.isCXYApp()) {
            title = '车行易社区' + title;
        }
        common.setViewTitle(title);
    }

    componentDidMount() {
        let { posts } = this.props;
        let { data, result } = posts;
        let { postsId, groupId } = this.props.params;

        //不存在缓存时直接请求帖子数据
        if (!data[postsId]) {
            this.getData();
        } else {
            //  先展示缓存数据再静默请求接口
            this.setState({
                post: data[postsId]
            });
            this.getData();
        }

        // 自定义分享菜单栏
        jsApi.setShareMenu();

        // 回到栈顶事件
        document.addEventListener('visibilitychange', () => {
            if (!document.hidden) {
                this.myCxyPageResume();
            }
        })
    }

    /**
     * 回到栈顶事件
     */
    myCxyPageResume() {
        // 监听广播
        jsApi.watchBroadcast('H5_COMMUNITY_UPDATA_POSTS', msgObj => {
            if (msgObj) {
                this.props.postsActions.updataPosts(msgObj);
            }
        })
    }

    /**
     * 请求数据
     * @param {boolean} noGetComments 不请求评论列表数据
     */
    getData(noGetComments = false) {
        let { postsId, groupId } = this.props.params;
        let params = {
            postsId,
            groupId,
        }
        this.props.postsActions.getPostDetailAsync(params, (result) => {
            this.setState({
                post: result.data
            }, () => {
                // 请求评论列表数据
                if (!noGetComments) this.getComments();
            });
        })
    }

    /**
     * 加载更多
     */
    loadingMore() {
        this.getComments();
    }

    /**
     * 设置msg下标的对象结构体
     * @param {} index 索引
     */
    setMsgIndexObject(index = 0) {
        //改变加载文案
        this.setState({
            loadingMoreProps: this.getMsgIndexObject(index)
        });
    }

    /**
     * 获取msg下标的对象结构体
     * @param {} index 索引
     */
    getMsgIndexObject(index) {
        let { loadingMoreProps } = this.state;
        return Object.assign({}, loadingMoreProps, {
            msgType: index, // 对应msg字段的下标
        })
    }

    /**
     * 发送广播
     * @param {string} type 更新数据：updata；删除帖子：delete
     */
    sendBroadcast(type = 'updata') {
        if (type === 'updata') {
            jsApi.sendBroadcast('H5_COMMUNITY_UPDATA_POSTS', this.state.post);
        } else if (type === 'delete') {
            let { postsId, groupId } = this.props.params;
            jsApi.sendBroadcast('H5_COMMUNITY_UPDATA_POSTS', ''); // 不需要更新帖子内容
            jsApi.sendBroadcast('H5_COMMUNITY_DELETE_POSTS', { postsId, groupId }); // 广播删除帖子的事件
        }
    }

    // 获取帖子评论列表
    getComments() {
        let { postsId, groupId } = this.props.params;
        let { pageNo, pageSize, comments, post } = this.state;

        //改变加载文案
        this.setMsgIndexObject(1);

        this.props.commentsActions.getPostsCommentListAsync({
            groupId,
            postsId,
            pageNo,
            pageSize,
        }, (result) => {
            if (result.code = "1000") {
                const { count, list, isEnd } = result.data;
                comments.count = count;
                comments.isEnd = isEnd;
                list.map(item => {
                    comments.data[item.id] = { ...comments.data[item], ...item };
                    comments.result.push(item.id);
                })
                comments.result = Array.from(new Set(comments.result));
                this.setState({
                    comments,
                    post: Object.assign({}, post, {
                        comments: count, // 评论数
                    }),
                    pageNo: Math.floor(comments.result.length / pageSize) + 1, // 分页数
                }, () => {
                    list.map(item => {
                        this.getReply(item.id)
                    })
                })

                // 改变加载中文案
                if (isEnd == '1') {
                    if (comments.result.length > 10) {
                        this.setMsgIndexObject(2);
                    } else {
                        this.setMsgIndexObject(3);
                    }
                } else {
                    this.setMsgIndexObject(0);
                }
            } else {
                this.setMsgIndexObject(0);
            }
        })
    }

    // 获取每条评论的回复列表
    getReply(com_id) {
        let { postsId, groupId } = this.props.params;
        let { comments } = this.state;
        let { data } = comments;
        this.props.commentsActions.getPostsReplyListAsync({
            groupId,
            postsId,
            com_id
        }, res => {
            if (res.code == '1000' && comments.data[com_id]) {
                comments.data[com_id].reply_list = res.data.list;
                this.setState({
                    comments,
                })
            }
        })
    }

    // 删除帖子
    delPosts(params) {
        Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请确认是否删除</span>, [
            {
                text: '取消', onPress: () => false, style: { color: '#108ee9' }
            },
            {
                text: '删除', onPress: () => {
                    this.props.postsActions.deleteMyPostsAsync(params, res => {
                        if (res.code == 1000) {
                            // 删除帖子成功
                            let { postsId, groupId } = this.props.params;
                            this.sendBroadcast('delete');
                            jsApi.back();
                        }
                    });
                }, style: { color: '#108ee9' }
            }
        ])
    }

    //帖子关注 取消关注
    postFollow() {
        if (!userHelper.isLogin()) {
            // 用户还未登录
            return this.login();
        }
        let { post } = this.state;
        let { isFollow } = post;
        let { groupId, postsId } = this.props.params;
        let ac_type = isFollow == "0" ? "31" : "32";//31:关注  32：取消关注
        let params = {
            groupId,
            postsId,
            ac_type,
        }

        // 直接改变UI 不等待接口返回
        this.setState({
            post: {
                ...post,
                isFollow: isFollow == "0" ? "1" : "0"
            }
        })

        this.props.postsActions.postFollowAsync(params, (result) => {
            if (result.code = "1000") {
                // 操作成功
            }
        })
    }

    //帖子点赞 取消点赞
    postPraise() {
        if (!userHelper.isLogin()) {
            // 用户还未登录
            return this.login();
        }
        let { post } = this.state;
        let { is_praise, supports } = post;
        let { groupId, postsId } = this.props.params;
        let ac_type = is_praise == "0" ? "4" : "5";//4:点赞  5：取消点赞
        let params = {
            groupId,
            postsId,
            ac_type,
        }

        // 直接改变UI 不等待接口返回
        this.setState({
            post: {
                ...post,
                is_praise: is_praise == "0" ? "1" : "0",
                supports: is_praise == "0" ? ++supports : --supports,
            }
        })

        this.props.postsActions.postPraiseAsync(params, (result) => {
            if (result.code == "1000") {
                // 操作成功
            }
        })
    }

    // 删除评论
    delComment(params) {
        const { com_id } = params;
        Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请确认是否删除</span>, [
            {
                text: '取消', onPress: () => false, style: { color: '#108ee9' }
            },
            {
                text: '删除', onPress: () => {
                    Toast.loading('', 0);
                    this.props.commentsActions.deleteCommentAsync(params, res => {
                        Toast.hide();
                        if (res.code == 1000) {
                            // 删除成功
                            const { comments, post } = this.state;
                            const newComments = {};
                            newComments.result = comments.result.filter(item => item !== com_id);
                            newComments.count = comments.result.length - 1;
                            newComments.data = comments.data;
                            delete (newComments.data[com_id]); // 删除指定的评论

                            this.setState({
                                comments: newComments,
                                post: Object.assign({}, post, {
                                    comments: newComments.count, // 评论数
                                })
                            });
                        }
                        if (res.msg) Toast.info(res.msg, 2);
                    });
                }, style: { color: '#108ee9' }
            }
        ])
    }

    // 点赞评论
    likeComment(params) {
        if (!userHelper.isLogin()) {
            // 用户还未登录
            return this.login();
        }

        // 异步点赞，不等待接口的返回
        const { comments } = this.state;
        const { comId, acType } = params;
        const isLike = acType == '10'; // 点赞
        comments.data[comId].isPraise = isLike ? '1' : '0';
        comments.data[comId].supports = (comments.data[comId].supports * 1) + (isLike ? 1 : -1);
        this.setState({
            comments
        })

        // 请求接口
        this.props.commentsActions.commentPraiseAsync(params);
    }

    // 采纳
    bestComment(params) {
        Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请确认是否设为最佳回答</span>, [
            { text: <span style={{ fontSize: '16px', color: '#2582EA' }}>取消</span>, onPress: () => false },
            {
                text: <span style={{ fontSize: '16px', color: '#2582EA' }}>确认</span>, onPress: () => {
                    // 异步设为最佳，不等待接口的返回
                    const { post, comments } = this.state;
                    const { comId } = params;
                    const { content, author_username, author_uid } = comments.data[comId];
                    post.qa = {
                        isResolve: "1",
                        bestComment: {
                            commId: comId,
                            commContent: content,
                            authorId: author_uid,
                            authorUsername: author_username,
                        }
                    }; // 修改最佳的内容
                    comments.data[comId].is_best = 1; // 采纳标识符
                    const newComId = [comId, ...(comments.result.filter(item => item != comId))]; // 将采纳的评论放到最前
                    comments.result = newComId;
                    this.setState({
                        comments
                    })

                    // 请求接口
                    this.props.commentsActions.bestCommentAsync(params);
                }
            }
        ])
    }

    //获取社区
    getcommunity() {
        let { community } = this.props;
        let { data, result } = community;
        let { groupId } = this.props.params;

        if (data[groupId]) {
            return data[groupId]
        } else {
            return {}
        }
    }

    //获取标签组合
    getTags(groupName, city) {
        let tag = [];
        if (groupName) tag.push(groupName);
        if (city) tag.push(city);
        return tag.join("·");
    }

    //显示输入框
    showInput() {
        if (!userHelper.isLogin()) {
            // 用户还未登录
            return this.login();
        }
        this.setState({
            isInputShow: true
        })
    }

    //隐藏输入框
    hideInput() {
        this.setState({
            isInputShow: false,
            isReply: false,
            placeholder: ''
        })
    }

    //提交评论
    submitComment(data, callback) {
        let { groupId, postsId } = this.props.params
        data = {
            ...data,
            groupId,
            postsId,
        }
        Toast.loading('', 0);
        this.props.commentsActions.addCommentAsync(data, (result) => {
            Toast.hide();
            if (result.code == "1000") {
                callback();
                this.setState({
                    isInputShow: false
                });
                this.getComments(); // 请求评论列表数据
                if (result.msg) Toast.info(result.msg, 1);
            } else {
                Toast.info(result.msg || ChMessage.FETCH_FAILED);
            }
        })
    }

    // 回复评论
    addReply(data) {
        if (!userHelper.isLogin()) {
            // 用户还未登录
            return this.login();
        }
        this.setState({
            replyInfo: data,
            isInputShow: true,
            isReply: true,
            placeholder: `回复${data.author_username}:`,
        })

    }

    // 删除回复
    delReply(params) {
        const { com_id, reply_id } = params;
        Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请确认是否删除</span>, [
            {
                text: '取消', onPress: () => false, style: { color: '#108ee9' }
            },
            {
                text: '删除', onPress: () => {
                    Toast.loading('', 0);
                    this.props.commentsActions.deleteReplayAsync(params, res => {
                        Toast.hide();
                        if (res.code == 1000) {
                            // 删除成功
                            const { comments } = this.state;
                            comments.data[com_id].reply_list = comments.data[com_id].reply_list.filter(item => item.id !== reply_id)

                            this.setState({
                                comments
                            });
                        }
                        if (res.msg) Toast.info(res.msg, 2);
                    });
                }, style: { color: '#108ee9' }
            }
        ])
    }

    // 提交回复
    submitReply(data, callback) {
        const { groupId, postsId, com_id, pid } = this.state.replyInfo;
        const { content } = data;
        if (content.replace(/\s*/g, '').length === 0) return Toast.info('回复内容不能为空', 2);
        Toast.loading('', 0);
        this.props.commentsActions.addReplyAsync({ groupId, postsId, com_id, content, pid }, res => {
            Toast.hide();
            if (res.msg) Toast.info(res.msg);
            if (res.code == '1000') {
                callback();
                this.hideInput();
                this.getReply(com_id);
            }
        })
    }

    // 选择投票
    checkedVote(i) {
        this.setState({
            checkedVote: i
        })
    }

    // 提交投票
    submitVote() {
        if (!userHelper.isLogin()) {
            // 用户还未登录
            return this.login();
        }
        let { groupId, postsId } = this.props.params
        const { checkedVote, post } = this.state;
        const { voteId } = post.vote.itemList[checkedVote];
        Toast.loading('', 0);
        this.props.postsActions.postAddVoteAsync({ groupId, postsId, voteId }, res => {
            Toast.hide();
            if (res.code == 1000) {
                post.vote.hasVote = '1'; //已经评论
                post.vote.itemList[checkedVote].voteResult.cnt = post.vote.itemList[checkedVote].voteResult.cnt * 1 + 1; //投票数加1;
                this.setState({
                    post
                })
                this.getData(true); // 请求帖子数据
            }
            if (res.msg) Toast.info(res.msg, 1);
        })
    }

    // 登录
    login() {
        if (common.isCXYApp()) {
            const url = window.location.protocol + '//' + window.location.host + window.location.pathname + '#/newAppWebView';
            userHelper.Login(url, true, false);
        } else {
            userHelper.Login();
        }
    }

    render() {
        // 页面每次渲染的时候都发送一次广播，(暂时未在离开栈顶事件中处理，所以暂时先这样处理)
        this.sendBroadcast();

        let { isWenda, post, isReply, placeholder, checkedVote, isInputShow, loadingMoreProps } = this.state;
        let community = this.getcommunity();
        let { groupId, postsId, headImgUrl, authorUsername, addtime, city, vote,
            priseMemberList, title, content, authorId, supports, qa,
            imgList, thumbImgList, isOwner, is_praise, isFollow, is } = post;
        let { groupName } = community;
        let tags = this.getTags(groupName, city);

        // 分享参数
        window.shareInfo = {
            title: common.delHtmlTag(title || content),
            desc: common.delHtmlTag(title || content),
        };

        // 关注按钮动画
        let followBtnClass = isFollow == "1" ? (styles.followBtn + " " + styles.stepA) : (styles.followBtn + " " + styles.stepB);

        // 点赞按钮
        let praiseBtnClass = is_praise == "1" ? (styles.praiseBtn + " " + styles.stateA) : (styles.praiseBtn + " " + styles.stateB);

        let { comments } = this.state

        // 图片阅览的数组格式
        const imgs = imgList && imgList.map((img, i) => ({
            src: img,
            msrc: thumbImgList[i]
        }))

        // 投票图片阅览的数组格式
        const voteImgs = vote && vote.itemList.map((item, i) => ({
            src: item.voteImgList,
            msrc: item.voteThumbImgList
        }))

        return (
            <div className='box' style={{ paddingBottom: "1.16rem" }}>
                <div className="whiteBg"></div>
                {
                    isWenda
                        ? // 问答详情
                        <div className={styles.questionContent}>
                            <div className={styles.head}>
                                <img src={headImgUrl || "./images/head.png"} onClick={() => jsApi.pushWindow(common.getRootUrl() + `homePage/${authorId}`)} />
                                <div className={styles.author} onClick={() => jsApi.pushWindow(common.getRootUrl() + `homePage/${authorId}`)} >
                                    <p>{authorUsername}</p>
                                    <p>{addtime} 提问</p>
                                </div>
                                <div className={styles.label}>{tags}</div>
                            </div>
                            <div className={styles.title} dangerouslySetInnerHTML={{ __html: content }}></div>
                            <div className={styles.imgList}>
                                {
                                    thumbImgList && thumbImgList.map((item, index) =>
                                        <img id={index === 0 ? 'shareIcon' : ''} src={item} key={index} onClick={() => jsApi.openPhotoSwipe(imgs, index)} />)
                                }
                            </div>
                            <div className={styles.footBox}>
                                {
                                    isOwner == '1'
                                        ? <span className={styles.delIcon + ' fr'} onClick={() => this.delPosts({
                                            groupId,
                                            postsId
                                        })}>删除</span>
                                        :
                                        <div>
                                            <span className={praiseBtnClass} onClick={() => this.postPraise()}>有用({supports})</span>
                                            <span className={styles.line}></span>
                                            <span className={followBtnClass} onClick={() => this.postFollow()}></span>
                                        </div>
                                }
                            </div>
                        </div>
                        : // 话题详情
                        <div className={styles.huatiContent}>
                            <div style={{ padding: '0 .3rem .6rem' }}>
                                {/* 标题 */}
                                <div className={styles.title} dangerouslySetInnerHTML={{ __html: title }}></div>
                                {/* 关注、点赞、删除按钮 */}
                                <div className={styles.btns + ' ' + styles.bottomLine}>
                                    {
                                        isOwner == '1'
                                            ?
                                            <span className={styles.delIcon + ' fr'} onClick={() => this.delPosts({
                                                groupId,
                                                postsId
                                            })}>删除</span>
                                            :
                                            <div>
                                                <span className={styles.huatiLike + ' likeBtn2' + ' ' + (is_praise == '1' ? 'isLike' : 'noLikeAnimation')} onClick={() => this.postPraise()}>有用({supports})</span>
                                                <span className={styles.line}></span>
                                                <span className={followBtnClass} onClick={() => this.postFollow()}></span>
                                            </div>
                                    }
                                </div>
                                {/* 内容 */}
                                <div className={styles.content} dangerouslySetInnerHTML={{ __html: content }}></div>
                                {/* 图片列表 */}
                                <div className={thumbImgList && thumbImgList.length > 0 ? styles.imgList : 'hide'}>
                                    {
                                        thumbImgList && thumbImgList.map((item, index) =>
                                            <img id={index === 0 ? 'shareIcon' : ''} src={item} key={index} onClick={() => jsApi.openPhotoSwipe(imgs, index)} />)
                                    }
                                </div>
                                {/* 用户头像 */}
                                <div className={styles.head + ' ' + styles.topLine + ' ' + styles.bottomLine}>
                                    <img src={headImgUrl || "./images/head.png"} onClick={() => jsApi.pushWindow(common.getRootUrl() + `homePage/${authorId}`)} />
                                    <div className={styles.author} onClick={() => jsApi.pushWindow(common.getRootUrl() + `homePage/${authorId}`)} >
                                        <p>{authorUsername}</p>
                                    </div>
                                </div>
                                {/* 投票 */}
                                {vote && vote.itemList && vote.itemList.length > 0 &&
                                    <div className={vote.hasVote === '1' ? styles.isCheckedVote : styles.vote}>
                                        <div className={styles.voteTop}>
                                            <span className={styles.voteTitle}>投票</span>
                                        </div>
                                        <div className={styles.votes}>
                                            {
                                                vote.itemList.map((item, index) =>
                                                    <div className={styles.voteItem} key={`vote-${item.voteId}`}>
                                                        <div className={styles.checkedBox} onClick={() => this.checkedVote(index)}>
                                                            <img className='abs-center' src={checkedVote === index ? './images/checkBox-true.png' : './images/checkBox-false.png'} />
                                                        </div>
                                                        <div className={styles.voteContent} onClick={() => this.checkedVote(index)}>
                                                            <p className={styles.voteDesc} dangerouslySetInnerHTML={{ __html: item.voteDesc }}></p>
                                                            <p className={styles.voteResult}>{item.voteResult.cnt || 0}票</p>
                                                        </div>
                                                        {item.voteThumbImgList &&
                                                            <div className={styles.voteImg} onClick={() => jsApi.openPhotoSwipe(voteImgs, index)}>
                                                                <img src={item.voteThumbImgList} />
                                                            </div>
                                                        }
                                                    </div>
                                                )
                                            }
                                            {
                                                vote.hasVote == '0' &&
                                                <div className={styles.voteBtnBox}>
                                                    {
                                                        checkedVote !== -1
                                                            ? <div className='btn' onClick={() => this.submitVote()}>提交</div>
                                                            : <div className='btn notClickBtn'>提交</div>
                                                    }
                                                </div>
                                            }
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                }

                <div className="h16"></div>

                <LoadingMore {...loadingMoreProps}>
                    {/* 评论列表 */}
                    <Comment
                        {...comments}
                        total={post.comments} // 评论总数
                        isResolve={qa && qa.isResolve == '1' ? 1 : 0} // 是否设置过最佳
                        showCainaBtn={isOwner == '1' && isWenda && qa.isResolve != '1'} // 显示采纳按钮
                        addReply={params => this.addReply(params)} // 添加回复
                        delReply={params => this.delReply(params)} // 删除回复
                        delComment={params => this.delComment(params)} // 删除评论
                        likeComment={params => this.likeComment(params)} // 评论点赞||取消点赞
                        bestComment={(params) => this.bestComment(params)} // 采纳
                    />
                </LoadingMore>

                {/* 输入框 */}
                <InputGroup
                    handleSubmitComment={(data, callback) => isReply ? this.submitReply(data, callback) : this.submitComment(data, callback)}
                    handleShowInput={() => this.showInput()}
                    handleHideInput={() => this.hideInput()}
                    isShow={isInputShow}
                    isReply={isReply}
                    placeholder={placeholder}
                />
            </div >
        );
    }
}

const mapStateToProps = state => ({
    community: state.community,
    posts: state.posts
})

const mapDispatchToProps = dispatch => ({
    postsActions: bindActionCreators(postsActions, dispatch),
    commentsActions: bindActionCreators(commentsActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostDetail);