/**
 * 新增提问页面
 */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import styles from './add.scss'
import common from '../../utils/common'
import jsApi from '../../utils/jsApi'

import * as communityActions from '../../actions/communityActions'
import * as postsActions from '../../actions/postsActions'
import OrtherService from '../../services/ortherService'
import UploadService from '../../services/imageService'
import List from './list'

import { Toast, Modal } from 'antd-mobile'
import ChMessage from '../../utils/message.config'

require("lrz")

class QuestionAdd extends Component {
	constructor(props) {
		super(props);

		this.state = {
			content: "",//问题内容
			imgs: [],//图片列表
			groupId: "135",//选中栏目
			carModel: "",//车型
			city: "",//城市
			catid: "5",//分类id
		}
	}

	componentWillMount() {
		common.setViewTitle("新提问");

		if (!sessionStorage.getItem("questionAddState")) {
			let { community } = this.props
			let { carModelList } = this.state
			//无缓存时请求社区数据
			if (community.result.length == 0) {
				this.props.communityActions.getMemberFollowAsync({ type: "qa" });
			}
		} else {
			let state = JSON.parse(sessionStorage.getItem("questionAddState"))
			sessionStorage.removeItem('questionAddState'); // 删除缓存 避免缓存一直都存在
			this.setState({
				...state
			})
		}
	}

	componentDidMount() {
	}

	//提问内容改变
	contentChange(e) {
		let value = e.target.value
		let { content } = this.state
		if (e.target.value != content) {
			this.setState({
				content: value,
			})
		}
	}

	//提问业务选择
	groupChange(item) {
		let { groupId } = this.state
		if (groupId != item) {
			this.setState({
				groupId: item
			})
		}
	}

	//生成社区栏目组件
	createGroup(community) {
		let { data, result } = community;
		let { groupId } = this.state;
		return (
			<div>
				{result.length > 0 &&
					<div className={styles.serviceList}>
						<h3>提问业务</h3>
						<div className={styles.list}>
							{
								result.map(item => {
									let output = (item != "94" && item != "95") ? <div key={item} className={groupId == item ? styles.selected : ""} onClick={() => this.groupChange(item)}>{data[item].groupName}</div> : "";
									return output
								})
							}
						</div>
					</div>
				}
			</div>
		)
	}

	//发表问题
	submit() {
		let { groupId, content, city, carModel, imgs, catid } = this.state
		let imgList = imgs.map(item => item.src)
		if (this.valiDate()) {
			//数据校验通过提交问题
			let params = {
				groupId,
				catid,
				content,
				city,
				carModel,
				imgList,
			}
			this.props.postsActions.addPostAsync(params, (result) => {
				if (result.code == "1000") {
					jsApi.sendBroadcast('H5_COMMUNITY_ADD_POSTS', params); // 广播发帖成功

					let data = result.data
					let text = data.points && data.points > 0 ? `发布成功 +${data.points}分` : '发布成功';
					Toast.info(text, 1, () => {
						jsApi.back();
					});
				}
			})
		}
	}

	//校验数据
	valiDate() {
		let { content, groupId } = this.state;

		//校验问题内容
		if (!content) {
			Toast.info("问题内容不能为空", 2)
			return false
		}

		//校验业务类型
		if (!groupId) {
			Toast.info("请选择提问业务")
			return false
		}

		return true;

	}

	/**
	 * 图片上传
	 */
	picture(e) {
		//在app时调用jsdk拍照接口（主要采用h5方式ios调用后title功能按钮为白色与背景同色）
		if (common.isCXYApp()) {
			jsApi.picture((base64) => {
				this.postImgToUploadImg(base64);
			})
		} else {
			this.uploadImgs(e, (base64) => {
				this.postImgToUploadImg(base64)
			});
		}
	}
	uploadImgs(e, callback) {
		let files = e.target.files;
		for (let i = 0; i < files.length; i++) {
			this._lrz(files[i], callback);
			e.target.value = "";
		}
	}

	/**
	 * 图片压缩上传
	 * @param e 图片资源
	 */
	_lrz(e, callback) {
		try {
			let files = e;
			let quality = 1;
			if (files.size > 1024 * 1024 * 5) {
				quality = .5;
			}
			else if (files.size > 1024 * 1024 * 2) {
				quality = .5;
			}
			else if (files.size > 1024 * 1024) {
				quality = .5;
			}
			else if (files.size > 1024 * 500) {
				quality = .4;
			}
			else if (files.size > 1024 * 100) {
				quality = .5;
			} else {
				quality = .7;
			}
			lrz(files, {
				width: 1024,
				quality: quality
			}).then((rst) => {
				// 处理成功会执行
				if (callback) {
					callback(rst.base64);
				}

			}).catch((err) => {
				// 处理失败会执行
				console.log('上传失败：', err);
				Toast.fail('上传失败!', 1);
			}).always(() => {
				// 不管是成功失败，都会执行
				// this.refs.uploadImgInput.value = ""; //清空文件上传的内容 避免上传同一张照片或是拍照时出现的图片无法展示的bug
			});
		} catch (e) {
			Toast.info("图片上传失败", 2);
		}

	}

	/**
	 * 上传图片到服务器
	 * @param base64 base64图片
	 */
	postImgToUploadImg(base64) {
		//添加图片
		let imgs = this.state.imgs;
		let index = imgs.push({
			tempSrc: base64
		});
		this.setState({
			imgs: imgs
		})

		//上传图片到服务器
		let uploadimgData = {
			content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
			format: 'jpg',
			channel: 'app',
			thumbnail: '300x300', //缩略图尺寸
			watermark: true //加水印
		}
		UploadService.uploadImg(uploadimgData).then(data => {
			if (data.code === 0) {
				imgs[index - 1].src = data.data.url;
				this.setState({
					imgs: imgs
				})
			} else {
				Toast.info(data.msg, 2);
			}
		}, () => {
			Toast.info("系统繁忙，请稍后再试");
		})
	}

	/**
	 * 删除图片
	 * @param {int} index 
	 */
	deleteImg(index) {
		Modal.alert('', <span style={{ fontSize: '16px', padding: '15px 0', color: '#1a1a1a', display: 'block' }}>请确认是否删除</span>, [
			{
				text: '取消', onPress: () => false, style: { color: '#108ee9' }
			},
			{
				text: '删除', onPress: () => {
					const { imgs } = this.state;
					this.setState({
						imgs: imgs.filter((item, i) => i !== index)
					})
				}, style: { color: '#108ee9' }
			}
		])
	}

	//显示车型数据
	showList(typeId) {
		let stateStr = JSON.stringify(this.state); // 将当前state转为字符串 便于储存到session中
		try {
			sessionStorage.setItem("questionAddState", stateStr);
		} catch (e) {
			// 保存的内容过大，导致保存到session失败
			let stateObj = JSON.parse(stateStr);
			stateObj.imgs = stateObj.imgs.map(item => ({ src: item.src })); // 只保存图片的URL 不保存base64
			stateStr = JSON.stringify(stateObj); //转为字符串
			sessionStorage.setItem("questionAddState", stateStr);
		}
		this.props.router.push("question/list/" + typeId);
	}

	render() {
		let { community } = this.props
		let { groupId, content, city, carModel, imgs, showCarModel, carModelList } = this.state

		// 图片阅览的数组格式
		const psImgs = imgs && imgs.map((item, i) => ({
			src: item.tempSrc || item.src,
		}))

		console.log('addPosts.state', this.state)
		return (
			<div className='box' style={{ paddingBottom: "1rem" }}>
				<div className='whiteBg'></div>
				<div className={styles.content}>
					<textarea
						style={{ width: "100%", height: "2.7rem", wordBreak: "break-all" }}
						placeholder="请输入你的问题，以问号结尾"
						value={content}
						onChange={(e) => this.contentChange(e)}
					></textarea>
				</div>
				<div className={styles.imgList}>
					{/*图片列表*/}
					{
						imgs.map((item, index) =>
							<div className={styles.imgBox} key={`question-img-${index}`}>
								<img src={item.tempSrc || item.src} key={index} onClick={() => jsApi.openPhotoSwipe(psImgs, index)} />
								<div className={styles.deleteImgIcon} onClick={() => this.deleteImg(index)}></div>
							</div>
						)
					}
					{imgs.length < 9 ? common.isCXYApp() ? <div className={styles.picAddBtn} onClick={() => this.picture()}></div> :
						<label className={styles.picAddBtn} htmlFor="imgAddBtn">
							<input id="imgAddBtn" type="file" style={{ display: "none" }} accept="image/*" onChange={(e) => this.picture(e)} />
						</label> : ""
					}
				</div>

				{/*提问业务*/}
				{this.createGroup(community)}

				<div className={styles.selectGroup} onClick={() => this.showList("carModel")}>
					选择车型
					<div className={styles.textGray}>{carModel || '请选择'}</div>
				</div>
				<div className={styles.selectGroup} onClick={() => this.showList("city")}>
					选择城市
					<div className={styles.textGray}>{city || '请选择'}</div>
				</div>
				<div className="h80"></div>

				<div className="fixed_b_container">
					<div className="sq_btn" onClick={() => this.submit()}>发表问题</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	community: state.community
})

const mapDispatchToProps = dispatch => ({
	// actions: bindActionCreators(carListActions, dispatch)
	communityActions: bindActionCreators(communityActions, dispatch),
	postsActions: bindActionCreators(postsActions, dispatch),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(QuestionAdd);