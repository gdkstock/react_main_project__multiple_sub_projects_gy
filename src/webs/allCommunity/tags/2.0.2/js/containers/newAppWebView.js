import React, { Component } from 'react'

// 组件
import { Toast } from 'antd-mobile';

// 用户类
import userHelper from '../utils/userHelper';

// jsApi
import jsApi from '../utils/jsApi';

class CheckUserInfo extends Component {
	constructor(props) {
		super(props)

	}

	componentWillMount() {

	}

	componentDidMount() {
		Toast.loading('', 0);
		jsApi.appClose(); // 登录成功则关闭webView
	}

	componentWillReceiveProps(nextProps) {

	}

	shouldComponentUpdate(nextProps, nextState) {

	}

	componentWillUpdate(nextProps, nextState) {

	}

	componentDidUpdate(prevProps, prevState) {

	}

	componentWillUnmount() {

	}

	render() {
		return (
			<div>

			</div>
		)
	}
}

export default CheckUserInfo