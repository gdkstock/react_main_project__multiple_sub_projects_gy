/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_POST_COMMENT_LIST_ASYNC,
    ADD_COMMENT_ASYNC,
    ADD_COMMENT,
    DELETE_COMMENT_ASYNC,
    GET_POST_REPLY_LIST_ASYNC,
    ADD_REPLY_ASYNC,
    DELETE_REPLY_ASYNC,
    COMMENT_PRAISE_ASYNC,
    BEST_COMMENT_ASYNC,
} from '../actions/actionsTypes.js'

import { baseFunc } from './baseSaga';

//antd
import { Toast } from 'antd-mobile'

//server
import postService from '../services/postService'
import commentService from '../services/commentService'

//常用工具类
import ChMessage from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库

/**
 * 获取帖子评论列表
 */
function* getPostCommentListAsync(action) {
    yield baseFunc(commentService.getComments, action);
}

function* watchGetPostCommentListAsync() {
    yield takeLatest(GET_POST_COMMENT_LIST_ASYNC, getPostCommentListAsync)
}

/**
 * 新增评论
 */
function* addCommentAsync(action) {
    yield baseFunc(commentService.createComment, action, function* cb(result) {
        if (result.code == '1000') {
            // 评论成功
            yield put({
                type: ADD_COMMENT,
                data: action.data,
            });
        }
    });
}

function* watchAddCommentAsync() {
    yield takeLatest(ADD_COMMENT_ASYNC, addCommentAsync)
}

/**
 * 删除评论
 */
function* deleteCommentAsync(action) {
    try {
        const { result, timeout } = yield race({
            result: call(commentService.deleteComment, action.data),
            timeout: call(delay, 30000)
        })

        if (timeout) {
            Toast.info(ChMessage.FETCH_FAILED)
            return;
        } else {
            if (result.code == '1000') {
                // 删除评论成功
            } else {
                Toast.info(result.msg || ChMessage.FETCH_FAILED)
            }
            if (action.callback) {
                action.callback(result)
            }
        }
    } catch (error) {
        Toast.info(ChMessage.FETCH_FAILED)
        if (action.callback) {
            action.callback(error)
        }
    }
}

function* watchDeleteCommentAsync() {
    yield takeEvery(DELETE_COMMENT_ASYNC, deleteCommentAsync)
}

/**
 * 获取回复列表
 */
function* getPostReplyAsync(action) {
    yield baseFunc(commentService.getReply, action);
}

function* watchGetPostReplyListAsync() {
    yield takeEvery(GET_POST_REPLY_LIST_ASYNC, getPostReplyAsync)
}

/**
 * 新增回复
 */
function* addReplyAsync(action) {
    yield baseFunc(commentService.createReply, action);
}

function* watchAddReplyAsync() {
    yield takeLatest(ADD_REPLY_ASYNC, addReplyAsync)
}

/**
 * 删除回复
 */
function* deleteReplyAsync(action) {
    yield baseFunc(commentService.deleteReply, action);
}

function* watchDeleteReplyAsync() {
    yield takeEvery(DELETE_REPLY_ASYNC, deleteReplyAsync)
}

/**
 * 评论点赞||取消点赞
 */
function* commentPraiseAsync(action) {
    yield baseFunc(commentService.praise, action);
}

function* watchCommentPraiseAsync() {
    yield takeEvery(COMMENT_PRAISE_ASYNC, commentPraiseAsync)
}

/**
 * 采纳评论
 */
function* bestCommentAsync(action) {
    yield baseFunc(commentService.best, action);
}

function* watchBestCommentAsync() {
    yield takeEvery(BEST_COMMENT_ASYNC, bestCommentAsync)
}


export function* watchComments() {
    yield [
        fork(watchGetPostCommentListAsync),
        fork(watchAddCommentAsync),
        fork(watchDeleteCommentAsync),
        fork(watchGetPostReplyListAsync),
        fork(watchAddReplyAsync),
        fork(watchDeleteReplyAsync),
        fork(watchCommentPraiseAsync),
        fork(watchBestCommentAsync),
    ]
}