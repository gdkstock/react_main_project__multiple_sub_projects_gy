/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_FOLLOW_LIST_ASYNC,
    GET_MEMBER_INDEX_ASYNC,
    ADD_POSTS_INDEX,
} from '../actions/actionsTypes.js'

import { baseFunc } from './baseSaga';

//antd
import { Toast } from 'antd-mobile'

//server
import memberService from '../services/memberService'

//常用工具类
import ChMessage from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库

/**
 * 获取用户个人信息
 */
function* getMemberIndexAsync(action) {
    yield baseFunc(memberService.index, action, undefined, true);
}

function* watchGetMemberIndexAsync() {
    yield takeLatest(GET_MEMBER_INDEX_ASYNC, getMemberIndexAsync)
}

/**
 * 获取已关注的帖子
 */
function* getFollowListAsync(action) {
    yield baseFunc(memberService.getFollowList, action, yield function* cb(result) {
        if (result.code == '1000') {
            const posts = new schema.Entity('data', {}, { idAttribute: 'postsId' });
            const postsList = new schema.Array(posts)
            const normalizedData = normalize(result.data.list, postsList);

            if (normalizedData.result.length > 0) {
                yield put({
                    type: ADD_POSTS_INDEX,
                    data: normalizedData.entities.data,
                    result: normalizedData.result
                })
            }
        }
    });
}

function* watchGetFollowListAsync() {
    yield takeLatest(GET_FOLLOW_LIST_ASYNC, getFollowListAsync)
}



export function* watchMember() {
    yield [
        fork(watchGetFollowListAsync),
        fork(watchGetMemberIndexAsync),
    ]
}