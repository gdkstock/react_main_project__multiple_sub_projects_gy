/**
 * 社区分类相关
 */

//styles
import styles from './index.scss';

//jsApi
import jsApi from '../../utils/jsApi';

//常用工具类
import common from '../../utils/common';

//列表
export const GroupPostsList = props => {
    let { posts, list, community } = props;

    return (
        <div>
            {
                list && list.map((item, i) =>
                    <div key={'posts-' + item.postsId} className={styles.box}>
                        <div className='h16'></div>
                        <GroupPosts
                            {...(Object.assign({},
                                item,
                                posts && posts.data && posts.data[item.postsId]
                                    ? posts.data[item.postsId]
                                    : {}
                            )
                            ) } //这里使用全局的state覆盖当前state
                            groupName={community && community.data && community.data[item.groupId] ? community.data[item.groupId].groupName : ''}
                        />
                    </div>
                )
            }
        </div>
    )
}

//帖子
export const GroupPosts = props => {
    //分类ID catid（1：违章热点/吐槽 2：资讯 3：违章详情 4:话题 5：问答 6：投票，7：社区资讯）
    const { groupId, postsId, catid, supports } = props;

    return (
        <div className={styles.whiteBg}>
            <GroupPostsHeader {...props} />
            <GroupPostsContent {...props} />
            <Imgs {...props} />
            <Adopt {...props} />
            <GroupPostsFooter {...props} supports={supports} />
        </div>
    )
}

//帖子头部
const GroupPostsHeader = props => {
    let { groupId, postsId, catid, authorUsername, headImgUrl, city, groupName } = props;

    let tags = [];
    //问答
    if (groupName) {
        tags.push(groupName);
    }
    if (city) {
        tags.push(city);
    }
    tags = tags.join('·'); //转为字符串

    return (
        <div className={styles.header} onClick={() => jsApi.pushWindow(common.getRootUrl() + `postDetail/${groupId}/${postsId}/${catid}`)}>
            <img src={headImgUrl || './images/head.png'} className={styles.avatar} />
            <span className={styles.name}>{authorUsername || '匿名易友'}</span>
            <span className={styles.tags}>{tags}</span>
        </div>
    )
}

//帖子标题或内容
const GroupPostsContent = props => {
    let { title, content, isDigest, groupId, postsId, catid } = props;
    content = title || content;
    if (isDigest == '1') {
        //精华帖
        content = `<i class='${styles.jinghuaIcon}'></i>` + (title || content);
    }
    return (
        <div
            className={styles.content + ' text-overflow-2'}
            onClick={() => jsApi.pushWindow(common.getRootUrl() + `postDetail/${groupId}/${postsId}/${catid}`)}
            dangerouslySetInnerHTML={{
                __html: content
            }}>
        </div>
    )
}

//图片
const Imgs = props => {
    let { thumbImgList, imgList, groupId, postsId, catid } = props;
    let className = '';

    if (thumbImgList.length === 1) {
        className = styles.img;
    } else if (thumbImgList.length > 1) {
        className = styles.imgs;
    } else {
        className = 'hide';
    }

    // 图片阅览的数组格式
    // const imgs = imgList.map((img, i) => ({
    //     src: img,
    //     msrc: thumbImgList[i]
    // }))

    return (
        <div className={className} onClick={() => jsApi.pushWindow(common.getRootUrl() + `postDetail/${groupId}/${postsId}/${catid}`)}>
            {
                thumbImgList.map((img, i) =>
                    <img
                        key={img + postsId}
                        src={img}
                        // onClick={() => jsApi.openPhotoSwipe(imgs, i)}
                    />
                )
            }
        </div>
    )
}

//采纳的答案
const Adopt = props => {
    let { qa, groupId, postsId, catid } = props;

    return (
        <div className={styles.adopt} onClick={() => jsApi.pushWindow(common.getRootUrl() + (`postDetail/${groupId}/${postsId}/${catid}`))}>
            {
                qa && qa.bestComment && qa.bestComment.commContent &&
                <p>
                    {qa.isResolve == '1' && <i className={styles.adoptIcon}>采纳</i>}
                    <span className={styles.adoptUserName} style={qa.isResolve == '0' ? { paddingLeft: 0 } : {}}>{qa.bestComment.authorUsername || '匿名易友'}:</span>
                    {qa.bestComment.commContent}
                </p>
            }
        </div>
    )
}

//帖子底部 点赞 评论数
const GroupPostsFooter = props => {
    //分类ID catid（1：违章热点/吐槽 2：资讯 3：违章详情 4:话题 5：问答 6：投票，7：社区资讯）
    let { groupId, postsId, comments, supports, catid, is_praise, praise } = props;

    return (
        <div className={styles.footer} >
            <span className={styles.replyBtn} onClick={() => jsApi.pushWindow(common.getRootUrl() + `postDetail/${groupId}/${postsId}/${catid}`)}>
                {catid == 5 ? `回答(${comments})` : comments}
            </span>
            <span
                className={(catid == 5 ? 'likeBtn' : 'likeBtn2') + ' ' + (is_praise == '1' ? 'isLike' : 'noLikeAnimation')}
                onClick={() => praise()}>
                {catid == 5 ? `有用(${supports})` : supports}
            </span>
        </div>
    )
}
