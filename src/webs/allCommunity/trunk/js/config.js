/**
 * 配置
 */
import common from './utils/common'

const userType = sessionStorage.getItem('userType') || common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay'
const production = window.location.host === 'shequ.cx580.com' || window.location.host.indexOf('comments.cx580.com') > -1; //是否为生产环境 或预发布环境
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?api_version=3&forceLogin=true&userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;

export default {
    //是否为生产环境
    production: production,

    //接口地址
    baseApiUrl: production ? window.location.protocol + '//' + window.location.host + '/' : 'http://testshequ.cx580.com:18066/',
    // baseApiUrl: 'http://comments.cx580.com/',

    //单点登录地址 回调地址需自行补全
    authUrl: authUrl,

    //违章首页路径
    violationUrl: production ? `https://daiban.cx580.com/Violation/index.html` : `http://webtest.cx580.com:9021/Violation/index.html`,

    //H5首页路径
    h5IndexUrl: production ? `https://daiban.cx580.com/H5Index/index.html` : `http://webtest.cx580.com:9021/H5Index/index.html`,

    //APP渠道的广播前缀
    broadcastPre: 'cxyBroadcast_',

    //需要监听的广播事件Id列表
    broadcastEventId: [
        'H5_COMMUNITY_UPDATA_POSTS',
        'H5_COMMUNITY_DELETE_POSTS',
        'H5_COMMUNITY_ADD_POSTS',
        'H5_COMMUNITY_DELETE_COMMENT',
        'H5_COMMUNITY_DELETE_REPLY',
        'H5_COMMUNITY_UPDATA_FOLLOW',
    ],

    //分享图片
    shareIcon: window.location.protocol + '//' + window.location.host + window.location.pathname + '/../images/logo.png',

    //调试的userId
    debugUsers: ['B406E4D73A704585AB64B35E2A7896BA', 'B4C02D709C9E41AAB3F7B40BD073B4B5'], //debug的userId账号
}