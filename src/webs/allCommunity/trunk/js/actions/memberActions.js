/*
* 社区相关的actions
*/

import {
    GET_FOLLOW_LIST_ASYNC,
    GET_MEMBER_INDEX_ASYNC,
    UPDATA_POSTS_UNREAD_ASYNC,
} from './actionsTypes'

//获取关注的帖子
export const getFollowListAsync = (data, callback) => ({ type: GET_FOLLOW_LIST_ASYNC, data, callback })

//获取用户个人信息
export const getMemberIndexAsync = (data, callback) => ({ type: GET_MEMBER_INDEX_ASYNC, data, callback })

// 清除小红点
export const updatePostsUnreadAsync = (data, callback) => ({ type: UPDATA_POSTS_UNREAD_ASYNC, data, callback })