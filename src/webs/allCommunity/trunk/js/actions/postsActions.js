/*
* 帖子相关的actions
*/

import {
    UPDATA_POSTS,
    DELETE_MY_POSTS,
    GET_POSTS_INDEX_ASYNC,
    ADD_POST_ASYNC,
    GET_MY_POSTS_ASYNC,
    DELETE_MY_POSTS_ASYNC,
    GET_MY_COMMENTS_ASYNC,
    POST_FOLLOW_ASYNC,
    GET_POST_DETAIL_ASYNC,
    POST_PRAISE_ASYNC,
    GET_POST_COMMENT_LIST_ASYNC,
    POST_ADD_VOTE_ASYNC,
} from './actionsTypes'

/**
 * 同步操作 
 */

export const updataPosts = (data, callback) => ({ type: UPDATA_POSTS, data, callback })
export const deleteMyPosts = (data, callback) => ({ type: DELETE_MY_POSTS, data, callback })



/**
 * 异步请求 
 */

//根据栏目获取帖子
export const getPostsIndexAsync = (data, callback) => ({ type: GET_POSTS_INDEX_ASYNC, data, callback })

//新增帖子
export const addPostAsync = (data, callback) => ({ type: ADD_POST_ASYNC, data, callback })

//获取我的发帖
export const getMyPostsAsync = (data, callback) => ({ type: GET_MY_POSTS_ASYNC, data, callback })

//删除我的发帖
export const deleteMyPostsAsync = (data, callback) => ({ type: DELETE_MY_POSTS_ASYNC, data, callback })

//获取我的评论
export const getMyCommentsAsync = (data, callback) => ({ type: GET_MY_COMMENTS_ASYNC, data, callback })

//获取帖子详情
export const getPostDetailAsync = (data, callback) => ({ type: GET_POST_DETAIL_ASYNC, data, callback })

//帖子关注/取消关注
export const postFollowAsync = (data, callback) => ({ type: POST_FOLLOW_ASYNC, data, callback })

//帖子点赞/取消点赞
export const postPraiseAsync = (data, callback) => ({ type: POST_PRAISE_ASYNC, data, callback })

//投票
export const postAddVoteAsync =  (data, callback) => ({ type: POST_ADD_VOTE_ASYNC, data, callback })
