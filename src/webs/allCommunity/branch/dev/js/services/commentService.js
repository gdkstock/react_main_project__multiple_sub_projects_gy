/**
 * 评论相关接口
 */
import apiHelper from './apiHelper';
import { baseService } from './baseService';

class CommentService {

    /**
     * 获取所有评论
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * ,"groupId":""//社区ID
     * ,"postsId":""//帖子ID
     * ,"pageNo":""//第一页，默认1
     * ,"pagesize":""//分页数量，默认10
     * }
     */
    getComments(data) {
        return baseService(`${apiHelper.baseApiUrl}comments`, data);
    }


    /**
     * 删除评论
     * @param data Object
     *  {"userId": "13800138000"//用户id
     * ,"token": "ADJKSKJNKJASLKSKLS"//token
     * ,"groupId":""//社区ID
     * ,"postsId":""//帖子ID
     * ,"com_id":""//评论ID
     * }
     */
    deleteComment(data) {
        return baseService(`${apiHelper.baseApiUrl}comments/del`, data);
    }


    /**
    * 评论
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":""//社区ID
    * ,"postsId":""//帖子ID
    * ,"content":""//评论内容
    * }
    */
    createComment(data) {
        return baseService(`${apiHelper.baseApiUrl}comments/add`, data);
    }


    /**
    * 获取评论的回复
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":""//社区ID
    * ,"postsId":""//帖子ID
    * ,"comId":""//评论ID
    * }
    */
    getReply(data) {
        return baseService(`${apiHelper.baseApiUrl}reply/index/`, data);
    }


    /**
    * 删除评论的回复
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":""//社区ID
    * ,"postsId":""//帖子ID
    * ,"com_id":""//评论ID
    * ,"reply_id":""//回复ID
    * }
    */
    deleteReply(data) {
        return baseService(`${apiHelper.baseApiUrl}reply/del`, data);
    }


    /**
    * 添加评论的回复
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":""//社区ID
    * ,"postsId":""//帖子ID
    * ,"com_id":""//评论ID
    * ,"content":""//评论内容
    * ,"pid":"0"	//上一级回应ID
    * }
    */
    createReply(data) {
        return baseService(`${apiHelper.baseApiUrl}reply/add`, data);
    }

    /**
    * 帖子问答设置最佳答案接口
    * @param data Object
    *  {"userId": "13800138000"//用户id
    * ,"token": "ADJKSKJNKJASLKSKLS"//token
    * ,"groupId":""//社区ID
    * ,"postsId":""//帖子ID
    * ,"com_id":""//评论ID
    * }
    */
    setSolution(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/set_solution`, data);
    }

    /**
    * 我的评论
    * @param data Object
    */
    myComments(data) {
        return baseService(`${apiHelper.baseApiUrl}comments/mycomments`, data);
    }

    /**
    * 评论点赞||取消点赞
    * @param data Object
    */
    praise(data) {
        return baseService(`${apiHelper.baseApiUrl}comments/praise`, data);
    }

    /**
    * 采纳评论
    * @param data Object
    */
    best(data) {
        return baseService(`${apiHelper.baseApiUrl}posts/set_solution`, data);
    }

}
// 实例化后再导出
export default new CommentService()