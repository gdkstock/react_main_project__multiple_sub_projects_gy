import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import { Icon } from 'antd-mobile';
import { GroupPostsList, GroupPosts } from '../../components/group/index'; //帖子分类
import LoadingMore from 'app/components/common/LoadingMore'; //上滑加载更多

//styles
import styles from './index.scss';

//actions
import * as postsActions from '../../actions/postsActions';

//jsApi
import jsApi from '../../utils/jsApi';

//common
import common from '../../utils/common';

class Group extends Component {
    constructor(props) {
        super(props)

        let { groupId, groupName } = this.props;
        this.state = {
            //上滑加载更多
            loadingMoreProps: {
                showMsg: true, //显示文字提醒
                msgType: 0, // 对应msg字段的下标
                msg: [
                    '点击加载更多...',
                    <span className={styles.iconLoading}><Icon type='loading' />加载更多...</span>,
                    '已经到底啦',
                    ''
                ], //文字提醒
                height: 100, //触发加载数据的高度 单位px
                loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
                line: true, //END时,是否添加贯穿文本的横线
            },

            //栏目id
            groupId,

            //栏目名称
            groupName,

            //上一个过滤的key
            preExtendsKey: '',

            //列表
            list: [],

            //没有数据了 '1'：表示没有数据了
            isEnd: '0',

            // 一条数据也没有时的提示信息
            endMsg: '',
        }

    }

    componentWillMount() {

    }

    componentDidMount() {
        // 监听事件
        document.addEventListener('H5_COMMUNITY_DELETE_POSTS', e => this.deletePosts(e.msgObj)); // 删除帖子

        document.addEventListener('H5_COMMUNITY_ADD_POSTS', e => this.addPosts(e.msgObj)); // 新增帖子

        document.addEventListener('H5_COMMUNITY_PULL_DOWN_REFRESH', e => this.pullDownRefresh(e.msgObj)); // 下拉刷新

        document.addEventListener('H5_COMMUNITY_GROUP_RELOAD', e => this.reLoadDatas(e.msgObj)); // 重新加载

        document.addEventListener('H5_COMMUNITY_GROUP_LOAD', e => this.loadDatas(e.msgObj)); // 加载数据
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
     * 下拉刷新
     * @param {object} msgObj {done:加载完成的回调事件,...home容器组件的state}
     */
    pullDownRefresh(msgObj) {
        const { active } = this.props;

        //非活跃栏目不需要请求数据
        if (!active) {
            return;
        }

        this.getDatas('down', 1, msgObj.done);
    }

    /**
     * 重新加载数据
     * @param {object} msgObj {done:加载完成的回调事件,...home容器组件的state}
     */
    loadDatas(msgObj) {
        const { list } = this.state;
        const { active } = this.props;

        //非活跃栏目不需要请求数据
        if (!active) {
            return;
        }

        // 存在数据时不需要加载数据
        if (list.length > 0) {
            return;
        }

        this.getDatas('down', 1, msgObj.done);
    }

    /**
     * 重新加载数据
     * @param {object} msgObj {done:加载完成的回调事件,...home容器组件的state}
     */
    reLoadDatas(msgObj) {
        const { active } = this.props;

        //非活跃栏目不需要请求数据
        if (!active) {
            return;
        }

        this.getDatas('down', 1, msgObj.done);
    }

    /**
     * 请求数据
     * @param {string} direction 默认值：up; 可选值： up：上划 down：下拉。
     * @param {number} create 默认值：0； 可选值： 0（追加数据）; 1（创建数据）
     * @param {function} callback 回调函数
     */
    getDatas(direction = 'up', create = 0, callback) {
        let { groupId, list, isEnd, preExtendsKey } = this.state;
        let { extendsKey } = this.props;

        //改变加载文案
        this.setState({
            loadingMoreProps: this.getMsgIndexObject(1)
        });


        //区分话题和问答
        let catId = '5';
        if (groupId === 'huati') {
            groupId = undefined;
            catId = '4,6'
        }

        let params = {
            groupId,
            extends: extendsKey,//未解决:"11";已解决:"12"
            direction,
            catId, //4:话题 5：问答 6：投票  如果是获取话题和投票的列表数据，传4,6
            pageSize: 30,
        }

        //非创建新数据
        if (!create) {
            if (direction === 'up') {
                //上滑
                if (list[list.length - 1]) {
                    params.postsId = list[list.length - 1].postsId;
                }
            } else {
                //下拉
                if (list[0]) {
                    params.postsId = list[0].postsId;
                }
            }
        }

        this.props.postsActions.getPostsIndexAsync(params, res => {
            if (res.code == 1000) {
                if (create) {
                    //新建数据
                    list = res.data.list;
                } else {
                    //合并数据以及更新isEnd的状态
                    if (preExtendsKey == extendsKey) {
                        list = direction === 'up' ? [...list, ...res.data.list] : [...res.data.list, ...list];
                    } else {
                        list = res.data.list; // 过滤参数不一致时，直接覆盖原列表数据
                    }
                }

                isEnd = res.data.isEnd;
                let msgType = 0; //上滑加载更多
                if (isEnd == '1') { //无数据
                    if (list.length > 10) {
                        msgType = 2;
                    } else {
                        msgType = 3; //不显示end
                    }
                }

                this.setState({
                    list,
                    isEnd,
                    loadingMoreProps: this.getMsgIndexObject(msgType),
                    endMsg: list.length === 0 ? '查无结果' : '',
                    preExtendsKey: extendsKey,
                })
            } else {
                //改变加载文案
                this.setState({
                    loadingMoreProps: this.getMsgIndexObject(0),
                    endMsg: list.length === 0 ? '服务器繁忙，请稍后再试' : '',
                });
            }

            // 执行回调函数
            if (callback) callback();
        })
    }

    /**
     * 加载更多
     */
    loadingMore() {
        const { active } = this.props;
        let { list, isEnd } = this.state;

        //非活跃栏目不需要请求数据
        if (!active) {
            return;
        }

        if (isEnd == '1') {
            return; //不再需要请求数据
        }

        //请求数据
        this.getDatas();
    }

    /**
     * 获取msg下标的对象结构体
     * @param {} index 索引
     */
    getMsgIndexObject(index) {
        let { loadingMoreProps } = this.state;
        return Object.assign({}, loadingMoreProps, {
            msgType: index, // 对应msg字段的下标
        })
    }

    /**
     * 删除帖子
     * @param {object} msgObj { groupId, postsId } 
     */
    deletePosts({ groupId, postsId }) {
        let { list } = this.state;
        list = list.filter(item => !(item.groupId == groupId && item.postsId == postsId));
        this.setState({
            list
        })
    }

    /**
     * 新增帖子
     * @param {string} msgObj {groupId}
     */
    addPosts(msgObj) {
        this.getDatas('down');
    }

    render() {
        let { loadingMoreProps, list, isEnd, endMsg } = this.state;
        let { community, posts, praise } = this.props

        return (
            <div className='box'>
                <LoadingMore {...loadingMoreProps}>
                    {
                        list && list.map((item, i) =>
                            <div key={'posts-' + item.postsId} className={styles.box}>
                                <GroupPosts
                                    {...(Object.assign({}, item, posts.data[item.postsId])) } //这里使用全局的state覆盖当前state
                                    groupName={community && community.data && community.data[item.groupId] ? community.data[item.groupId].groupName : ''}
                                    praise={() => praise && praise(item)} //点赞
                                />
                            </div>
                        )
                    }
                    {
                        isEnd && endMsg && list.length === 0 && <div className={styles.endMsg}>{endMsg}</div>
                    }
                </LoadingMore>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    community: state.community,
    posts: state.posts,
})

const mapDispatchToProps = dispatch => ({
    postsActions: bindActionCreators(postsActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Group);