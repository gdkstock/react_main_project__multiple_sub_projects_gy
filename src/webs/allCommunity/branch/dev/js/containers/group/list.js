/**
 * 社区栏目列表
 */

import React, { Component } from 'react';

//组件
import { Tabs } from 'antd-mobile';
const TabPane = Tabs.TabPane;

import Group from './index';

class GroupList extends Component {
    constructor(props) {
        super(props)

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    render() {
        let { activeGroupId, groups, extendsKey, active, praise } = this.props;
        activeGroupId = activeGroupId || groups[0].groupId;

        return (
            <div className='homeTabs'>
                <Tabs activeKey={activeGroupId || groups[0].groupId} animated={true} swipeable={false}>
                    {
                        groups.map((item, i) =>
                            <TabPane tab={item.groupName} key={item.groupId} >
                                <Group
                                    key={'Group-' + item.groupId}
                                    {...item}
                                    activeGroupId={activeGroupId}
                                    extendsKey={extendsKey}
                                    active={active && item.groupId === activeGroupId}
                                    praise={(posts) => praise && praise(posts)}
                                />
                            </TabPane>
                        )
                    }
                </Tabs>
            </div>
        )
    }
}

export default GroupList