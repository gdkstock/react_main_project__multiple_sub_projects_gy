import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import { Modal, Toast } from 'antd-mobile';
import Top from '../components/home/top';
import TabBtns from '../components/home/TabBtns';
import GroupList from './group/list';
import Group from './group';
import PullDownRefresh from 'app/components/common/PullDownRefresh'; //上滑加载更多

//actions
import * as communityActions from '../actions/communityActions';
import * as postsActions from '../actions/postsActions';

//JSDK
import jsApi from '../utils/jsApi';

//常用工具类
import common from '../utils/common';
import userHelper from '../utils/userHelper';

//过滤按钮和对应extens的key值
const tabBtns = {
	list: ['全部', '待解决', '已采纳'],
	key: [['11', '12'], ['11'], ['12']],
	index: 0, // 当前选中索引
};

//自定义话题的数据
const huatiProps = {
	groupId: 'huati',
	groupName: '话题'
}

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			//0：问答；1：话题
			key: '0',

			//当前页卡的滚动位置
			scrollTop: [0, 0],

			//问答话题下的活动页卡
			activeKey: '',

			//问答话题下的活动页卡的groupId
			activeGroupId: '',

			//过滤类型所对应的key值（对应extends字段）
			extendsKey: '',

			//过滤的索引下标
			extendsIndex: 0,

			//顶部tab的样式
			topTabStyles: '',

			//下拉刷新
			pullDownRefreshProps: {
				onRefresh: () => this.pullDownRefresh(), //刷新回调函数
				loadingDone: false, //加载完成
				refreshInfo: {
					delay: 0,
				}, //下拉刷新 配置
			},
		}
	}

	componentWillMount() {
		common.setViewTitle('易友社区');
	}

	componentDidMount() {
		// 请求数据
		this.getDatas();

		// 设置title栏
		this.controlTitleLayout();

		// 监听事件
		document.addEventListener('H5_COMMUNITY_UPDATA_POSTS', e => {
			setTimeout(() => this.props.postsActions.updataPosts(e.msgObj), 10);
		}); // 异步更新帖子信息
	}

	componentWillReceiveProps(nextProps) {

	}

	componentWillUnmount() {

	}

	/**
	 * 设置title栏
	 */
	controlTitleLayout() {
		if (common.isCXYApp()) {
			this.showOrHideTitleLayout('hide');
		}
	}

	/**
	 * 隐藏App title栏
	 */
	showOrHideTitleLayout(showStatus = 'show') {
		try {
			jsApi.ready(() => {
				window.cx580.jsApi.call({
					"commandId": "",
					"command": "controlTitleLayout",
					"data": {
						showStatus: showStatus
					}
				}, data => {
					this.setState({
						topTabStyles: common.isAndroid() ? 'appTop24' : 'appTop20',
					})
				});
			})
		} catch (e) {
		}
	}

	/**
	 * 分派让页面重新请求数据的事件
	 */
	groupReloadDatas() {
		// 分派让页面重新请求数据的事件
		common.dispatchEvent('H5_COMMUNITY_GROUP_RELOAD', {
			done: () => false, // 请求成功的回调
			...this.state, // 首页的state
		});
	}

	/**
	 * 分派让页面请求数据的事件
	 */
	groupLoadDatas() {
		// 分派让页面重新请求数据的事件
		common.dispatchEvent('H5_COMMUNITY_GROUP_LOAD', {
			done: () => false, // 请求成功的回调
			...this.state, // 首页的state
		});
	}

	/**
	 * 下拉刷新
	 */
	pullDownRefresh() {
		const { pullDownRefreshProps } = this.state;
		this.setState({
			pullDownRefreshProps: Object.assign({}, pullDownRefreshProps, {
				onRefresh: () => false, //避免重复触发
				loadingDone: false, //加载完成
			}),
		}, () => {
			// 分派下拉刷新事件
			common.dispatchEvent('H5_COMMUNITY_PULL_DOWN_REFRESH', {
				done: () => this.pullDownRefreshDone(), // 下拉刷新完成事件
				...this.state, // 首页的state
			});
		})

	}

	/**
	 * 下拉刷新完成
	 */
	pullDownRefreshDone() {
		const { pullDownRefreshProps } = this.state;
		this.setState({
			pullDownRefreshProps: Object.assign({}, pullDownRefreshProps, {
				onRefresh: () => this.pullDownRefresh(), //刷新回调函数
				loadingDone: true, //加载完成
			}),
		})
	}

	/**
	 * 请求数据
	 */
	getDatas() {
		let { communityActions } = this.props;

		//获取用户关注的社区
		communityActions.getMemberFollowAsync({ type: 'qa' }, res => {
			if (res.code == 1000) this.groupReloadDatas(); // 请求栏目数据
		})
	}

	//切换0：问答；1：话题
	setKey(newkey) {
		let { key, scrollTop } = this.state;

		scrollTop[key] = document.body.scrollTop; //保存当前页卡的滚动位置
		document.body.scrollTop = scrollTop[newkey]; //还原目标页卡的滚动位置

		this.setState({
			key: newkey,
			scrollTop,
		}, () => {
			// 重新请求数据
			this.groupLoadDatas(); // 请求栏目数据
		})
	}

	/**
	 * 切换问答话题下的活动页卡
	 * @param {string} activeKey 活动页卡的key
	 * @param {*} activeGroupId 当前groupId
	 * @param {*} extendsKey 过滤的key
	 * @param {*} extendsIndex 当前选中的过滤索引下标
	 */
	setActiveKey(activeKey, activeGroupId, extendsKey, extendsIndex) {
		console.log(activeKey, activeGroupId, extendsKey, extendsIndex)
		this.setState({
			activeKey,
			activeGroupId,
			extendsKey,
			extendsIndex,
		}, () => {
			this.groupReloadDatas();
		})
	}

	//发帖
	addPosts() {
		let { key } = this.state;
		const hash = key === '0' ? 'question/add' : 'topic/add';
		if (!userHelper.isLogin()) {
			// 用户还未登录
			const path = window.location.protocol + '//' + window.location.host + window.location.pathname + '#/';
			return userHelper.Login(path + hash, true, false); // 通过新webView打开登录页面
		} else {
			jsApi.pushWindow(common.getRootUrl() + hash); // 保留URL上的参数
		}
	}

	//点赞
	praise(data) {
		let { groupId, postsId, catid } = data;
		let { posts } = this.props;

		const url = window.location.protocol + '//' + window.location.host + window.location.pathname + '#/newAppWebView';
		if (!userHelper.isLogin()) {
			// 用户还未登录
			return userHelper.Login(url, true, false); // 通过新webView打开登录页面
		}

		if (posts.data[postsId]) {
			data = Object.assign({}, data, posts.data[postsId]); //优先使用全局state中的数据
		}

		let ac_type = data.is_praise == '0' ? 4 : 5; //点赞或取消点赞
		this.props.postsActions.postPraiseAsync({ groupId, postsId, ac_type }, res => { })
	}

	render() {
		const { key, activeKey, activeGroupId, extendsKey, extendsIndex, topTabStyles, pullDownRefreshProps } = this.state;
		const { community } = this.props;
		const groups = community.result.map(groupId => community.data[groupId]);

		return (
			<div className='box'>
				{/* 填充APP顶部的高度 */}
				<div className={topTabStyles}></div>

				<Top defaultKey={key} onChange={key => this.setKey(key)} />

				{/* 下拉刷新组件 start */}
				<PullDownRefresh {...pullDownRefreshProps}>
					{/* 问答 */}

					{ // 这里需要重新渲染组件来计算滚动条位置
						key === '0' && groups.length > 0 && <TabBtns
							groups={groups}
							list={tabBtns.list}
							defaultKey={activeKey}
							index={extendsIndex}
							onChange={(i, i2) => this.setActiveKey(i, groups[i].groupId, tabBtns.key[i2], i2)} />
					}
					{
						groups.length > 0 &&
						<div className={key === '0' ? '' : 'hide'}>
							<GroupList
								groups={groups}
								extendsKey={extendsKey}
								active={key === '0'}
								activeGroupId={activeGroupId}
								praise={posts => this.praise(posts)} //点赞
							/>
						</div>
					}

					{/* 话题 */}
					<div className={key === '1' ? '' : 'hide'}>
						{
							<Group
								{...huatiProps}
								active={key === '1'}
								praise={posts => this.praise(posts)} //点赞
							/>
						}
					</div>
				</PullDownRefresh>
				{/* 下拉刷新组件 end */}

				{/* 发布按钮 */}
				<div className="homeEditBtn">
					<img src='./images/icon-edit.png' alt='发布' onClick={() => this.addPosts()} />
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	community: state.community,
	posts: state.posts,
})

const mapDispatchToProps = dispatch => ({
	communityActions: bindActionCreators(communityActions, dispatch),
	postsActions: bindActionCreators(postsActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);