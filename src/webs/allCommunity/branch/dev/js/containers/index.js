export { default as App } from './App'

// 简单
export { default as Home } from './Home'

// 404
export { default as NotFoundPage } from './404.js'

// 兼容用户信息 如果存在则关闭webView
export { default as NewAppWebView } from './newAppWebView'