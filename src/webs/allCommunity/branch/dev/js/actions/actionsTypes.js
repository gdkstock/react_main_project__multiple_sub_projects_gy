/*
* 这里定义所有的action类型
* */

/**
 * fetch数据请求相关
 */
export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时


//同步请求
export const ADD_MEMBER_FOLLOW_ASYNC = 'ADD_MEMBER_FOLLOW_ASYNC' //新增用户已关注的社区
export const ADD_POSTS_INDEX = 'ADD_POSTS_INDEX' //新增栏目帖子
export const ADD_FOLLOW_LIST = 'ADD_FOLLOW_LIST' //新增关注帖子列表
export const POST_PRAISE = 'POST_PRAISE' //帖子点赞/取消点赞
export const POST_FOLLOW = 'POST_FOLLOW' //帖子关注/取消关注
export const REPLY_PRAISE = 'REPLY_PRAISE' //回复点赞/取消点赞
export const DELETE_COMMENT = 'DELETE_COMMENT' //删除评论
export const DELETE_MY_POSTS = 'DELETE_MY_POSTS' //删除我的发帖
export const ADD_COMMENT = 'ADD_COMMENT' //添加评论
export const UPDATA_POSTS = 'UPDATA_POSTS' //更新帖子
export const POST_ADD_VOTE = 'POST_ADD_VOTE' //添加投票


//异步请求(为了更加直观的区分异步action和同步action；所有异步action 都需要在后面加上“_ASYNC”)
export const GET_MEMBER_FOLLOW_ASYNC = 'GET_MEMBER_FOLLOW_ASYNC' //获取用户已关注的社区
export const GET_POSTS_INDEX_ASYNC = 'GET_POSTS_INDEX_ASYNC' //根据栏目获取帖子
export const GET_FOLLOW_LIST_ASYNC = 'GET_FOLLOW_LIST_ASYNC' //获取关注的帖子
export const ADD_POST_ASYNC = 'ADD_POST_ASYNC' //新增帖子
export const GET_MEMBER_INDEX_ASYNC = 'GET_MEMBER_INDEX_ASYNC' //获取用户个人信息
export const UPDATA_POSTS_UNREAD_ASYNC = 'UPDATA_POSTS_UNREAD_ASYNC' // 清除小红点
export const GET_MY_POSTS_ASYNC = 'GET_MY_POSTS_ASYNC' //获取我的发帖
export const DELETE_MY_POSTS_ASYNC = 'DELETE_MY_POSTS_ASYNC' //删除我的发帖
export const GET_MY_COMMENTS_ASYNC = 'GET_MY_COMMENTS_ASYNC' //获取我的评论
export const GET_POST_DETAIL_ASYNC = 'GET_POST_DETAIL_ASYNC' //获取帖子详情
export const POST_FOLLOW_ASYNC = 'POST_FOLLOW_ASYNC' // 帖子关注/取消关注
export const POST_PRAISE_ASYNC = 'POST_PRAISE_ASYNC' // 帖子点赞/取消点赞
export const REPLY_PRAISE_ASYNC = 'REPLY_PRAISE_ASYNC' //回复点赞/取消点赞
export const GET_POST_COMMENT_LIST_ASYNC = 'GET_POST_COMMENT_LIST_ASYNC' //获取帖子评论列表
export const ADD_COMMENT_ASYNC = 'ADD_COMMENT_ASYNC' //添加评论
export const DELETE_COMMENT_ASYNC = 'DELETE_COMMENT_ASYNC' //删除评论
export const GET_POST_REPLY_LIST_ASYNC = 'GET_POST_REPLY_LIST_ASYNC' //获取帖子回复列表
export const ADD_REPLY_ASYNC = 'ADD_REPLY_ASYNC' //添加回复
export const DELETE_REPLY_ASYNC = 'DELETE_REPLY_ASYNC' //删除评论
export const POST_ADD_VOTE_ASYNC = 'POST_ADD_VOTE_ASYNC' //添加投票
export const COMMENT_PRAISE_ASYNC = 'COMMENT_PRAISE_ASYNC' //评论点赞||取消点赞
export const BEST_COMMENT_ASYNC = 'BEST_COMMENT_ASYNC' //采纳评论
