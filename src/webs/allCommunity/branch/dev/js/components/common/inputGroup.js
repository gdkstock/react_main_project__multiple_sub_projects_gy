/**
 * 城市列表、车型列表公共组件
*/
import React, { Component } from 'react'
import styles from './inputGroup.scss'

import UploadService from '../../services/imageService'

import { Toast } from 'antd-mobile'
import ChMessage from '../../utils/message.config'
import common from '../../utils/common'
import jsApi from '../../utils/jsApi'

const imgSrc = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1507805892057&di=fa52aa5124e975c5b709837e07dd5a7a&imgtype=0&src=http%3A%2F%2Fwww4.qqjay.com%2Fu%2Ffiles%2F2012%2F0420%2Faf94a4ffc82b24f2e612504931889649.jpg"

class InputGroup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			content: "",
			imgs: [],
		}
	}

    /**
	 * 图片上传
	 */
	picture(e) {
		if (this.state.imgs.length == 9) {
			Toast.info("最多只能上传9张图片", 2);
			return;
		}
		//在app时调用jsdk拍照接口（主要采用h5方式ios调用后title功能按钮为白色与背景同色）
		if (common.isCXYApp()) {
			jsApi.picture((base64) => {
				this.postImgToUploadImg(base64);
			})
		} else {
			this.uploadImgs(e, (base64) => {
				this.postImgToUploadImg(base64);
			});
		}
	}

	uploadImgs(e, callback) {
		let files = e.target.files;
		for (let i = 0; i < files.length; i++) {
			this._lrz(files[i], callback);
			e.target.value = ""; //清空文件上传的内容 避免上传同一张照片或是拍照时出现的图片无法展示的bug
		}
	}

	/**
	 * 图片压缩上传
	 * @param e 图片资源
	 */
	_lrz(e, callback) {
		try {
			let files = e;
			let quality = 1;
			if (files.size > 1024 * 1024 * 5) {
				quality = .5;
			}
			else if (files.size > 1024 * 1024 * 2) {
				quality = .5;
			}
			else if (files.size > 1024 * 1024) {
				quality = .5;
			}
			else if (files.size > 1024 * 500) {
				quality = .4;
			}
			else if (files.size > 1024 * 100) {
				quality = .5;
			} else {
				quality = .7;
			}
			return lrz(files, {
				width: 1024,
				quality: quality
			}).then((rst) => {
				// 处理成功会执行
				if (callback) {
					callback(rst.base64)
				}

			}).catch((err) => {
				// 处理失败会执行
				Toast.fail('上传失败!', 1);
			}).always(() => {
				// 不管是成功失败，都会执行
				// this.refs.uploadImgInput.value = ""; //清空文件上传的内容 避免上传同一张照片或是拍照时出现的图片无法展示的bug
			});
		} catch (e) {
			Toast.info("图片上传失败", 2);
		}

	}

	/**
	 * 上传图片到服务器
	 * @param base64 base64图片
	 */
	postImgToUploadImg(base64) {
		//添加图片
		let imgs = this.state.imgs;
		let index = imgs.push({
			tempSrc: base64
		});
		this.setState({
			imgs: imgs
		})

		//上传图片到服务器
		let uploadimgData = {
			content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
			format: 'jpg',
			channel: sessionStorage.getItem("userType"),
			thumbnail: '200x200', //缩略图尺寸
			watermark: true //加水印
		}
		UploadService.uploadImg(uploadimgData).then(data => {
			if (data.code === 0) {
				imgs[index - 1].src = data.data.url;
				this.setState({
					imgs: imgs
				})
			} else {
				Toast.info(data.msg, 2);
			}
		}, () => {
			Toast.info("系统繁忙，请稍后再试");
		})
	}

	contentChange(e) {
		let value = e.target.value;
		let { content } = this.state;
		if (value != content) {
			this.setState({
				content: value,
			})
		}
	}

	// 删除图片
	deleteImg(index) {
		let { imgs } = this.state;
		imgs.splice(index, 1);
		this.setState({
			imgs,
		})
	}

	//提交数据
	submitData() {
		let { content, imgs } = this.state
		let imgList = imgs.map(item => {
			return item.src;
		});

		// 返回给父组件
		this.props.handleSubmitComment({
			content,
			imgList,
		}, res => {
			// 发布成功后 清空内容
			this.setState({
				content: "",
				imgs: [],
			})
		})
	}

	//showInput
	showInput() {
		this.props.handleShowInput()
	}

	//handleHideInput
	handleHideInput(e) {
		e.preventDefault();
		this.props.handleHideInput();
	}

	render() {
		let { content, imgs } = this.state;
		let { isShow, isReply, placeholder } = this.props;
		let submitBtnClass = content ? styles.submitBtn : (styles.submitBtn + " " + styles.submitBtnDisabled)

		return (
			<div className={styles.container}>
				<div className={isShow ? styles.mask : "hide"} onTouchStart={(e) => this.handleHideInput(e)}></div>
				<div className={isShow ? styles.inputContainer : "hide"}>
					{isShow &&
						<textarea
							className={styles.textarea}
							id="questionInputGroup"
							placeholder={placeholder || "有何高见，展开讲讲…"}
							onChange={(e) => this.contentChange(e)}
							autoFocus={true}
						>
						</textarea>
					}
					{imgs.length > 0 &&
						<div className={styles.imgList}>
							{imgs.map((item, index) => <div key={index} >
								<img src={item.tempSrc} />
								<i onClick={() => this.deleteImg(index)}></i>
							</div>
							)}
						</div>
					}
					<div className={styles.btnGroup}>
						{
							isReply ? '' :
								<label className={styles.imgIcon} onClick={common.isCXYApp() ? (e) => this.picture(e) : () => { }}>
									{!common.isCXYApp() && <input type="file" className="hide" accept="image/*" onChange={(e) => { this.picture(e) }} />}
								</label>
						}
						<div className={submitBtnClass} onClick={content ? () => { this.submitData() } : () => { }}>发布</div>
					</div>
				</div>
				<div className={!isShow ? styles.fixedFootBox : "hide"}>
					<label htmlFor="questionInputGroup" onClick={() => this.showInput()}>有何高见，展开说说...</label>
				</div>
			</div>
		)
	}
}

export default InputGroup