/**
 * 首页相关组件
 */

import React, { Component } from 'react';

//组件
import Filter from './filter';

//styles
import styles from './tabBtns.scss';

//屏幕宽度
const window_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

class TabBtns extends Component {
    constructor(props) {
        super(props)

        let { activeKey, list, index } = this.props;
        this.state = {

            //默认选中的key
            curr: activeKey || 0,

            //过滤组件的props
            filterProps: {
                //显示过滤列表
                show: false,

                //过滤列表的索引值
                index,

                //过滤列表
                list,

                //内容发送改变
                onChange: index => this.updateFilter({ show: false, index })
            }
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        let { defaultKey, groups } = this.props;
        this.initScroll(); //设置滚动条的宽度
        if (defaultKey) {
            this.btnsStyle(defaultKey, this.refs['tabBtns-' + groups[defaultKey].groupId]); //设置滚动条的位置
        }
    }

    btnsStyle(key, e) {
        let { curr } = this.state;

        if (curr === key) {
            return; //避免重复渲染
        }

        this.setBtnsScrollLeft(e);

        this.setState({
            curr: key
        }, () => this.onChange());
    }

    //设置按钮滚动条位置
    setBtnsScrollLeft(e) {
        let { btnsBox, btns } = this.refs;
        let btn_l = e.offsetLeft; //按钮位置X坐标
        let btn_w = e.clientWidth; //按钮长度
        let btns_s = btnsBox.scrollLeft; //当前滚动位置

        //当前按钮位置大于总长度的一半
        if (btn_l + (btn_w / 2) > (window_w / 2)) {
            btnsBox.scrollLeft = btn_l + (btn_w / 2) - (window_w / 2); //居中显示
        } else {
            btnsBox.scrollLeft = 0;
        }
    }

    //初始化按钮滚动样式
    initScroll(e) {
        let { btnsBox, btns } = this.refs;
        let w = btns.clientWidth;
        if (btns.style.width != w + 'px') {
            btns.style.width = (w + 100) + 'px'; //避免出现换行的奇异现象
            btnsBox.style.width = '100%';
        }
    }

    //显示
    showOrHideFilter(show = true) {
        let { filterProps } = this.state;

        this.setState({
            filterProps: Object.assign({}, filterProps, {
                show
            })
        });
    }

    //更新filter的props
    updateFilter(data) {
        let { filterProps } = this.state;

        this.setState({
            filterProps: Object.assign({}, filterProps, data)
        }, () => data.index !== undefined && this.onChange());
    }

    //内容发送改变时，返回数据给父组件
    onChange() {
        let { onChange } = this.props;
        let { curr, filterProps } = this.state;
        if (onChange) {
            onChange(curr, filterProps.index);
        }
    }

    render() {
        let { curr, filterProps } = this.state;
        let { groups } = this.props;

        return (
            <div className={styles.box}>
                <div ref='btnsBox' className={styles.btnsBox} onTouchStart={() => this.initScroll()}>
                    <div ref='btns' className={styles.btns}>
                        {
                            groups && groups.map((item, i) =>
                                <span
                                    ref={'tabBtns-' + item.groupId}
                                    key={'tabBtns-' + item.groupId}
                                    className={curr === i ? styles.btn + ' ' + styles.curr : styles.btn}
                                    onClick={(e) => this.btnsStyle(i, e.target)}
                                >{item.groupName}</span>)
                        }
                    </div>
                </div>
                <div className={styles.filter} onClick={() => this.showOrHideFilter(true)}>
                    {
                        filterProps.index ? <span>{filterProps.list[filterProps.index]}</span> : <i></i>
                    }
                </div>
                <Filter {...filterProps} />
            </div>
        )
    }
}

export default TabBtns