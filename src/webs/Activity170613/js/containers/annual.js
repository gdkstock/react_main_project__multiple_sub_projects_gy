import React, { Component } from 'react'
import { connect } from 'react-redux'

// 样式
import Style from '../components/calculatorOfYearlyCheck/index.scss'


// 加载组件
import AlertDemo from '../components/alertDemo'; //Alert弹窗说明
import CalculatorOfYearlyCheck from '../components/calculatorOfYearlyCheck'
import YearlyCheckDetail from '../components/calculatorOfYearlyCheck/YearlyCheckDetail'

import { Popup, Toast, Tabs, Modal, List, InputItem } from 'antd-mobile'

// 加载actions
import carInfoService from '../services/carInfoService'

import SendCxytjMessage from '../utils/sendCxytjMessage'

/**
 * 组件
 */
class Annual extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tabIndex: '',// tab,被点中的生活，传的key;默认显示的KEY，要在请求车辆列表后才生效
            provinceS: {
                provinceList: [],  // 车牌前缀列表
                province: '',  // 车牌前缀
            },
            carList: {
                list: [],
                html: ''
            },
            data: { //''年检计算机的props数据
                carNum: '',
                registerDate: '',// 输出注册时间
                inspectDate: '',// 输出有效期
                onClick: (r, i) => this.showYearlyCheckDetailFUN(r, i, carNum), // 子组件：send
            },
            showYearlyCheckDetail: false, //显示年检状态
            yearlyCheckDetailProps: { //年检状态props数据
                carNum: '',
                state: '0', //年检状态；0：正常;1：可预约；2：逾期（仍可免检）；3：逾期；4：严重逾期；5：报废
                registerDate: '', //注册时间
                inspectDate: '', //检验有效期
                days: '', //文案提醒中的天数 (有效天数、逾期天数等)
                bizFlag: 0, // 当前状态
                clickLeftBtn: (d) => this.gotoBackFUN(d),
                clickRightBtn: () => console.log("父组件：点击了右边按钮（立即办理）"),
            },
            AlertDemo: { //弹窗demo
                show: false, //显示或隐藏弹窗
                props: { //弹窗demo 的props
                    demo: '暂无demo', //需要展示的demo
                    close: () => console.log('点击关闭按钮'), //点击关闭按钮
                }
            },
        }
    }
    /**
     * 生命周期
     */
    componentDidMount() {
        // 开始请求用户车辆信息
        if (!this.state.showYearlyCheckDetail) {
            this.showCarList(); // 请求车辆列表
            this.getProvinceList();  // 请求车牌前缀
        }
        document.querySelector("title").innerHTML = '年检查询';
        SendCxytjMessage.sendMessage('h5_p_mot_EnterMotQuery')//  埋点
    }
    /**
     * gotoBackFUN 是结果页面，返回事件
     * @param {*} type ='top'表示顶部的重新计算；''表示页面里面的返回按钮
     */
    gotoBackFUN(type) {
        let id = ''
        if (this.state.yearlyCheckDetailProps.state == 2) {
            id = 'h5_e_motcheck'
        } else if (this.state.yearlyCheckDetailProps.state == 1) {
            id = 'h5_e_motordet'
        }
        if (id) {
            switch (type) {
                case 'top':
                    SendCxytjMessage.sendMessage(id + '_ClickRecalculate')//  埋点
                    break;
                default:
                    SendCxytjMessage.sendMessage(id + '_ClickBack')//  埋点
                    break;
            }
        }


        this.setState({ showYearlyCheckDetail: false })
        setTimeout(() => {
            this.showCarList() //请求
        }, 300)
    }
    /**
    * 显示 车辆列表
     */
    showCarList() {
        // 发出请求
        let postData = {}
        let noCAR = {
            carNumber: '',
            registerDate: "",
            checkDate: "",
        }
        Toast.hide();
        Toast.loading("", 30, () => Toast.info("网络错误,稍后再试", 2));
        carInfoService.getCarList(postData).then(resultData => {
            //   sessionStorage.setItem('thisChange', false)
            //   console.log('+++++++ userCar/list 车辆列表请求 = ', resultData);

            if (resultData.code == 1000) {
                let result = resultData.data;
                // let newCars=result.cars.unshift(noCAR);
                let newCars = [];


                //  if (typeof result.carId) this.getBycarID(result.carId);
                switch (result.cars.length) {
                    case 4:
                    case 3:
                        newCars = result.cars
                        break;
                    case 0:
                        newCars = [noCAR]
                        break;
                    default:
                        // 如果车辆列表长度小于3
                        if (result.cars.length < 3) {
                            for (let i = 0; i < result.cars.length; i++) {
                                newCars.push(result.cars[i])
                            }
                            newCars.push(noCAR)
                        }
                        break;
                }
                sessionStorage.setItem('isChange', false)
                this.setState({
                    carList: Object.assign(this.state.carList, {
                        list: newCars,
                    }),
                    data: Object.assign(this.state.data, { //''年检计算机的props数据
                        carNum: '',
                    }),
                })
                setTimeout(() => {
                    Toast.hide();
                }, 500)
            }
            else {

                //  Toast.info(result.msg, 2);
            }

        }).then(() => this.setState({ tabIndex: sessionStorage.getItem('thisIndex') ? sessionStorage.getItem('thisIndex') : '1' }));
    }

    /**
   * 显示 车辆列表，点击顶部删除按钮效果，和新增加车辆
   */
    CarListClose(carnum, type, data) {
        // this.showCarList('BTN');
        switch (type) {
            case 'add': // 添加车辆
                this.getAddCar(carnum) //返回车牌号给父组件
                break;
            case 'del': // 删除车辆
                this.setState({
                    carNum: '',
                })
                this.getDelCar(carnum) //传id，返回车牌号给父组件
                break;
            case 'change': // 改变车牌

                break;
        }

    }
    /**
     * 获得 车牌所在省列表
     */
    getProvinceList() {
        // 已经有前缀的，不用再请求
        if (this.state.provinceS.provinceList.length) {
            return false;
        }

        // 发出请求
        let postData = {
            cityName: '广州',
            longitude: 23,
            latitude: 113,
        };
        //  alert(JSON.stringify(postData))
        carInfoService.getProvinceList(postData).then(resultData => {
            //  console.log('+++++++ inspection/cityList 车牌所在省列表请求 = ', resultData);

            if (resultData.code == 1000) {
                let result = resultData.data;
                this.setState({
                    provinceS: Object.assign(this.state.provinceS, {
                        provinceList: result.provinceList[0],
                        province: result.provinceList[0][0].provinceName
                    })
                });
            }
            else if (resultData.code == 0) {
                this.setState({
                    provinceS: Object.assign(this.state.provinceS, {
                        province: '粤'
                    })
                });
                //  Toast.info(resultData.msg, 2);
            }
            else {
                Toast.info(resultData.msg, 2);
            }
        });// test /////////////
    }
    /**
     * 获取 经纬度，去请求车牌前缀列表
     */
    getLocation() {
        sessionStorage.setItem("latitude", 23);
        sessionStorage.setItem("longitude", 113);
        sessionStorage.setItem("cityName", '粤');
    }
    /**
        * 弹窗显示 车牌所在省列表
        */
    showProvinceList() {
        let sfValue = this.state.provinceS.province

        if (!this.state.provinceS.provinceList.length) {
            Toast.info("请稍等", 1)
            // this.getProvinceList()
            return
        }

        let dateList = this.state.provinceS.provinceList //

        //alert(sfValue)

        let list = dateList.map((item, i) =>
            <div key={'provinceList' + i} className={sfValue == item.provinceName ? Style.active : ''} onClick={(date) => this.showProvince(item)}>{item.provinceName}</div>
        );

        let JSX = <div className={Style.provinceList}>{list}</div>
        Popup.show(JSX, { animationType: 'slide-up' });
    }
    /**
      * 页面显示 车牌前缀
      */
    showProvince(data) {
        // alert(data.provinceName);
        Popup.hide()
        this.setState({
            provinceS: Object.assign(this.state.provinceS, {
                province: data.provinceName
            })
        });
    }
    /**
   * 车牌号码 输入框 用户输入事件
   */
    inputOnInput(e) {
        let value = e.target.value.replace(/[^A-Za-z0-9]*/g, '').toUpperCase().substr(0, 7);
        // 需要判断首字母是否字母

        let province = this.state.provinceS.province;
        setTimeout(() => {
            this.setState({
                data: Object.assign(this.state.data, {
                    carNum: province + value
                })
            })
        }, 200)
        setTimeout(() => {
            if (this.refs.carNumInput) {
                this.refs.carNumInput.value = value;
            } //只能输入六位的字母和数字
        }, 0) //异步 避免中文输入法下的bug
    }


    /**
        * 删除 车辆
        */
    getDelCar(carId) {
        let postData = {
            carId: carId, //车牌号码
        }
        Toast.hide();
        Toast.loading("", 10, () => Toast.info("网络错误,稍后再试", 2));
        carInfoService.getDelCar(postData).then(resultData => {
            //   console.log('+++++++ cars/delete 删除车辆请求 = ', resultData);
            if (resultData.code == 1000) {
                //添加车辆后，请求车辆年检信息
                if (this.refs.carNumInput) this.refs.carNumInput.value = ''
                setTimeout(() => {
                    this.showCarList(0); //请求车辆列表
                }, 100)
            } else {
                Toast.info(resultData.msg, 2);
            }
        })
    }
    /**
     * 获得 子组件给出的车牌,注册日期，有效期
     */
    getCarNum(carNum, registerDate, inspectDate) {
        this.setState({
            data: Object.assign({}, this.state.data, {
                carNum: carNum,
                registerDate: registerDate,
                inspectDate: inspectDate
            })
        })
    }



    /**
     * 跳转到年检状态页面
     */
    showYearlyCheckDetailFUN(r, i) {
        let index = this.state.tabIndex - 1
        //    let carInfor =this.state.carList.list[index].carNumber
        let carInfor = this.state.carList.list[index]
        let thisCarNum = carInfor.carNumber ? carInfor.carNumber : this.state.data.carNum

        //   console.log("父组件：点击了保存按钮!", '车牌：'+thisCarnum,"注册时间：" + r, "检验有效期：" + i)

        let c = /^[a-zA-Z]/;
        if (!c.test(thisCarNum.substring(1, 2))) {
            Toast.info("请输入正确车牌", 1)
            return false
        }

        if (thisCarNum.length <= 6) {
            Toast.info("请输入正确车牌", 1)
        }
        else if (!r) {
            Toast.info("请选择车辆注册日期", 1)
        } else if (!i) {
            Toast.info("请选择检验有效期", 1)
        } else {
            Toast.hide();
            Toast.loading("", 10, () => Toast.info("网络错误", 2));
            // alert(carnum)

            let postData = {
                carId: carInfor.carId, //车牌号码
                carNumber: thisCarNum, //	string	是	车牌
                registerDate: r, //	string	是	车辆注册日期（yyyy-MM-dd）
                checkDate: i, //	string	是	车辆检验日期（yyyy-MM-dd）
                carCode: carInfor.carCode, //	string	是	车身架号
                engineNumber: carInfor.engineNumber, //	string	是	发动机号
            }
            SendCxytjMessage.sendMessage('h5_e_mot_ClickSaveAndQuery')//  埋点
            sessionStorage.setItem('thisIndex', this.state.tabIndex)
            if (carInfor.carNumber) {

                carInfoService.getUpdate(postData).then(resultData => {
                    Toast.hide();
                    //    console.log('+++++++ cars/delete 修改车辆请求 = ', resultData);
                    if (resultData.code == 1000) {
                        this.setState({
                            showYearlyCheckDetail: true,
                            yearlyCheckDetailProps: Object.assign({}, this.state.yearlyCheckDetailProps, {
                                carNum: resultData.data.carNumber,
                                state: resultData.data.state, //随机状态 模拟数据
                                registerDate: r, //注册时间
                                inspectDate: i, //检验有效期
                                days: resultData.data.days, //文案提醒中的天数 (有效天数、逾期天数等)
                                agentFlag: resultData.data.agentFlag ? true : false,
                                bizFlag: resultData.data.bizFlag
                            })
                        })



                    } else {
                        Toast.info(resultData.msg, 2);
                    }
                })
            } else {
                carInfoService.getAddCar(postData).then(resultData => {
                    Toast.hide();
                    //    console.log('+++++++ cars/delete 修改车辆请求 = ', resultData);
                    if (resultData.code == 1000) {
                        sessionStorage.setItem('thisIndex', 1)
                        this.setState({
                            tabIndex: 1,
                            showYearlyCheckDetail: true,
                            yearlyCheckDetailProps: Object.assign({}, this.state.yearlyCheckDetailProps, {
                                carNum: resultData.data.carNumber,
                                state: resultData.data.state, //随机状态 模拟数据
                                registerDate: r, //注册时间
                                inspectDate: i, //检验有效期
                                days: resultData.data.days, //文案提醒中的天数 (有效天数、逾期天数等)
                                agentFlag: resultData.data.agentFlag ? true : false,
                                bizFlag: resultData.data.bizFlag
                            })
                        })
                    } else {
                        Toast.info(resultData.msg, 2);
                    }
                })
            }



        }
    }
    /**
      * 显示弹窗说明
      * @param type 内容的类型
      */
    showAlertBox(type) {
        //  console.log(type);
        let demo = '';
        if (type === 'registerTime') { //注册时间
            demo = <img src='./images/demo-registration-time.png' />
        } else if (type === 'inspectTime') { //检验有效期
            demo = <img src='./images/demo-inspect-item.png' />
        }

        this.setState({
            AlertDemo: { //弹窗demo
                show: true, //显示
                props: {
                    demo: demo, //需要展示的demo
                    close: () => this.hideAlertBox(), //点击关闭按钮
                }
            }
        })
    }

    /**
     * 隐藏弹窗说明
     */
    hideAlertBox() {
        this.setState({
            AlertDemo: Object.assign({}, this.state.AlertDemo, {
                show: false //隐藏
            })
        })
    }

    /**
 * 顶部tab被选中，切换的时候,记录下KEY的值
 * @param {*} key 
 */
    callback(key) {
        sessionStorage.setItem('thisIndex', key)
        this.setState({
            tabIndex: key
        })
    }

    /**
     * 
     */
    /**
     * 组件
     */
    render() {
        let yearlyCheckDetailProps = this.state.yearlyCheckDetailProps
        let carlist = this.state.carList.list



        return (
            <div className='box'>

                {this.state.showYearlyCheckDetail ?
                    <YearlyCheckDetail {...yearlyCheckDetailProps} />

                    :

                    <div>
                        <div className={Style.subTitle}>以下年检所需信息为车管所的规定，你的信息将严格保密。</div>

                        <Tabs className='List_tagS' activeKey={this.state.tabIndex} animated={false} swipeable={false} onChange={(e) => this.callback(e)}>
                            {
                                carlist.map((item, i) =>
                                    <Tabs.TabPane
                                        key={i + 1}
                                        tab={carlist.length == 1 && item.carNumber == '' ?
                                            <div className='relative List_tag_hide'>nocar</div>
                                            :
                                            <div className='relative List_tag'>
                                                {item.carNumber == '' ?
                                                    <div className='colorBlue Item_tag' onChange={() => this.CarListClose('', 'change')}>添加车辆</div>
                                                    :
                                                    <div className='relative'>
                                                        <div className='my-tag-close' onClick={() => Modal.alert('确定删除车辆 [' + item.carNumber + ']', '', [
                                                            { text: '取消', onPress: () => console.log('no'), style: { color: '#999' } },
                                                            { text: '确定', onPress: () => this.CarListClose(item.carId, 'del') },
                                                        ])}></div>
                                                        <div className='colorBlue Item_tag' onChange={() => this.CarListClose(item.carNumber, 'change', item)}>{item.carNumber}</div>
                                                    </div>

                                                }
                                            </div>
                                        }
                                    >
                                        {/*** TabPane --end */}

                                        <div>
                                            {
                                                item.carNumber == '' ?
                                                    <List className='bg-white bottomLine' >

                                                        <List.Item
                                                            className='cf Item_InputItem'
                                                            extra={<input ref='carNumInput' className={'fl ' + Style.inputBox} maxLength='7' type='url' placeholder="请输入车牌" onInput={e => this.inputOnInput(e)} style={{ textTransform: 'uppercase' }} />}
                                                        >
                                                            <span className={Style.listItem}>车牌号码</span>
                                                            <span className='colorBlue fr' onClick={() => { this.showProvinceList() }}>{this.state.provinceS.province}</span>
                                                        </List.Item>

                                                    </List>
                                                    :
                                                    <List className='bg-white bottomLine' >

                                                        <List.Item extra={
                                                            <span className={Style.listItemExtra} style={{ color: '#333' }}>{item.carNumber}&nbsp;</span>
                                                        }>
                                                            <span className={Style.listItem}>车牌号码</span>
                                                        </List.Item>

                                                    </List>

                                            }

                                            <CalculatorOfYearlyCheck
                                                carNumber={item.carNumber}
                                                registerDate={item.registerDate}
                                                inspectDate={item.checkDate}
                                                onClick={(r, i) => this.showYearlyCheckDetailFUN(r, i)}
                                            />
                                        </div>


                                    </Tabs.TabPane>
                                )}
                        </Tabs>
                        <div className={Style.desBox}>
                            <div className={Style.desTitle}>
                                <span>年检时间计算说明</span>
                            </div>
                            <div className={Style.desText}>
                                <p><span>1、</span>输入准确的车辆信息，系统会智能计算出车辆的年检状态，以及下一次年检的类型。如果进入预约周期，系统还能自动提醒您需要准备哪些资料，该如何办理。如果一不小心逾期，系统还能协助您如何尽量避免损失。</p>
                                <p><span>2、</span>本计算方式只适用于7座以下的蓝牌小轿车。</p>
                            </div>
                        </div>

                        {/*弹窗说明 start*/}
                        {this.state.AlertDemo.show ? <AlertDemo {...this.state.AlertDemo.props} /> : ''}
                        {/*弹窗说明 end*/}

                    </div>

                }


            </div>
        );
    }
}

//使用context
Annual.contextTypes = {
    router: React.PropTypes.object.isRequired
}

Annual.propTypes = {
};

export default Annual;