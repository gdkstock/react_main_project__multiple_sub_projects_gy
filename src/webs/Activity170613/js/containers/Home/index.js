import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
// 加载样式
import Style from './index.scss'

import SendCxytjMessage  from '../../utils/sendCxytjMessage'

/**
 * 组件
 */
class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {

		}
	}

	/**
   * 生命周期
   */

	componentWillMount() {
		//执行一次，在初始化render之前执行，如果在这个方法内调用setState，render()知道state发生变化，并且只执行一次
		sessionStorage.setItem('userId', '');
		sessionStorage.setItem('token', '');
		document.querySelector("title").innerHTML = '爱车年检';
	}

	componentDidMount() {
		//在初始化render之后只执行一次，在这个方法内，可以访问任何组件，componentDidMount()方法中的子组件在父组件之前执行
		// 缓存信息
		//	console.log('home/index', this.getParameters())
		sessionStorage.setItem('userId', this.getParameters().userId);
		sessionStorage.setItem('token', this.getParameters().token);
		sessionStorage.setItem('userOpenId', this.getParameters().userOpenId);
		sessionStorage.setItem('userType', this.getParameters().userType);

		SendCxytjMessage.sendMessage('h5_p_mot_EnterMotpage')//  埋点
	}


	/**
    * 事件
    */
	//获取链接参数，返回对象
	getParameters() {
		let parameterResult = {};
		if (window.location.search) {
			let parameters = window.location.search.replace('?', '');
			let parameterArray = parameters.split('&');
			for (let i = 0; i < parameterArray.length; i++) {
				let result = parameterArray[i].split('=');
				parameterResult[result[0]] = result[1].replace('/', '');//过滤斜杠符号
			}
		}
		return parameterResult;
	}
	// 页面跳转
	toURL(url) {
		this.context.router.push(url)
		SendCxytjMessage.sendMessage('h5_e_mot_ClickQuery')//  埋点
	}

	/**
	 * 组件
	 */
	render() {
		return (
			<div className={Style.home}>
				<div className={Style.mark} />
				<div className='relative'>
					<div className={Style.btnIcon} onClick={() => this.toURL('/news1/')} />
					<img src='./images/index/1_01.png' width='100%' height='auto' />
				</div>
				<img src='./images/index/1_02.png' width='100%' height='auto' />
				<img src='./images/index/1_03.png' width='100%' height='auto' />
				<img src='./images/index/1_04.png' width='100%' height='auto' />
				<div className='relative'>
					<div className={Style.btn} onClick={() => this.toURL(`/annual/`)} />
					<img src='./images/index/1_05.png' width='100%' height='auto' />
				</div>
				<img src='./images/index/1_06.png' width='100%' height='auto' />
				
			</div>
		)
	}
}


//使用context
Home.contextTypes = {
	router: React.PropTypes.object.isRequired
}

export default connect()(Home)