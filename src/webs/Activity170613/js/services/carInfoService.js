/**
 * karin
 * on 2017/6/19.
 */

import apiHelper from './apiHelper';

class CarInfoService {

    //获得 车辆列表
    getCarList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/index`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    //获得 车牌所在省列表
    getProvinceList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}inspection/cityList`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    //添加 车辆
    getAddCar(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/save`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

 //通过 车辆ID，请求对应的车辆年检信息
    getCarInfo(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/view`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
 //通过 车辆ID，删除车辆
    getDelCar(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/delete`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
//通过 车牌，判断车牌是否支持年检验证
    getUpdate(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/update`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


}

export default new CarInfoService();