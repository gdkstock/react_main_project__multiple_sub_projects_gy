/**
 * karin
 * on 2017/6/19.
 */

import apiHelper from './apiHelper';

class Yearly {

    //获取 优惠券
    getCoupon(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}activity/get_user_coupons`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    //去领取 优惠券
    hasCoupon(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}activity/get_activity_status`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    
    // 联动获取检验有效时间列表
     getCheckDateList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}inspection/getCheckDateList`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


}

export default new Yearly();