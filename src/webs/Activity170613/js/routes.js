import React from 'react'
import { Route, IndexRoute } from 'react-router'

import {
  App,
  Home,
  NotFoundPage,
  Annual,
} from './containers'

import { News1 } from './components' //年检类型说明、法规说明

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home}/>
    <Route path="annual" component={Annual}/>
    <Route path="news1" component={News1}/>
    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}
    <Route path="*" component={NotFoundPage}/>
  </Route>
);