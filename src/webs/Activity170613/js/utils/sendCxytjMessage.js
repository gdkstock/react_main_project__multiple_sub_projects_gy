/**
 * Created by 小敏哥 on 2017/7/10.
 * karin on 2017/7/12修改
 */
class SendCxytjMessage {
    //统一封装的发送埋点操作
    sendMessage(type) {
        if (window.cxytj.cxytjReady) {
            window.cxytj.recordUserBehavior({
                eventId: type,
                eventType: '2', //事件类型
                eventTime: '',  //触发时间
                channel: sessionStorage.getItem('userType')
            });
        }
        else {
            //当未完成初始化时监听初始化事件再发送埋点
            document.addEventListener('cxytjReady', function (e) {
                window.cxytj.recordUserBehavior({
                    eventId: type,
                    eventType: '2', //事件类型
                    eventTime: '',  //触发时间
                    channel: sessionStorage.getItem('userType')
                });
            }, false);
        }
    }
}

export default new SendCxytjMessage();