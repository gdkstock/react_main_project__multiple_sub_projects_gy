import jsApi from './cx580.jsApi';
import common from './common';

let g_userContainer = undefined;

let g_userId = ''; 
let g_userToken = ''; 

let g_city = "";


/**
 * 用户帮助类
 */
class UserHelper {
    //userContainer = "";
    // userId = "";
    // userToken = "";
    //city = "";


    constructor() {
        this._initialize();
    }

    _initialize() {
        // setTimeout(() => {
        //     this._getContainer();
        // }, 500);
        if (common.isCXYApp()) {
            //执行到这里，说明是在 app 中运行
            g_userContainer = "App";
            this._getUserInfoFromApp();
            // setTimeout(() => {
            //     this._getUserInfoFromApp();
            // }, 150);
        } else {
            //执行到这里，说明不在 app 中运行
            g_userContainer = "";
            //alert("请使用车行易APP访问！");
        }
    }

    _getContainer() {
        try {
            jsApi.call({
                "commandId": "",
                "command": "netstat"
            }, (data) => {
                //执行到这里，说明是在 app 中运行
                g_userContainer = "App";
                this._getUserInfoFromApp();
            });
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            g_userContainer = "";
        }
    }


    /**
     * 从 app 中获取用户 token
     */
    _getUserInfoFromApp() {
        try {
            jsApi.call({
                "commandId": "",
                "command": "getSymbol",
                "data": {
                    "userid": "",
                    "lng": "",
                    "version": "",
                    "channel": "",
                    "cars": "",
                    "phone": "",
                    "name": "",
                    "type|orderNum": "",
                    "city": "",
                    "accountId": "",
                    "token": "",
                    "carId": "",
                    "carNumber": ""
                }
            }, (data) => {
                g_userId = data.data.accountId;
                g_userToken = data.data.token;
                g_city = data.data.city;
            });
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            // alert("调试信息：调用APP JS SDK出错了 获取APP信息出错了");
        }
    }

    /**
     * app 登陆
     */
    _appLogin(callback) {
        jsApi.call({
            "commandId": "",
            "command": "login"
        }, (data) => {
            localStorage.setItem("upLoginState", "1"); //用户登录状态发生变化
            if (data.data.accountId) {
                g_userId = data.data.accountId;
                this._getUserInfoFromApp();
                if (callback) {
                    callback();
                }
            } else {
                //登录失败
                common.closeAppView(); //关闭APP视图
            }
        });
    }

    /**
     * 获取 userId 和 token
     */
    getUserIdAndToken() {
        if (g_userContainer == "App") {
            this._getUserInfoFromApp();
        } else {
            //拿不到openId则页面后退或关闭
            if (!sessionStorage.getItem("alipayOpenId")) {
             //   window.history.go(-1); //页面后退
                if (navigator.userAgent.indexOf("AlipayClient") !== -1) {
                    setTimeout(() => {
                        //可后退时不执行，执行时表示不能后退了
                        AlipayJSBridge.call('popTo', {
                            index: -1, //返回上一页
                        }, function (result) {
                            if (error) {
                                AlipayJSBridge.call('closeWebview'); //返回上一页失败 则调用关闭JSDK
                            }
                        });
                    }, 30);
                }
            }
            return {
                alipayOpenId: sessionStorage.getItem("alipayOpenId"),
                userId: sessionStorage.getItem("userId"),
                token: sessionStorage.getItem("token"),
                userType: sessionStorage.getItem("userType")
            }
        }
        return {
            userId: g_userId,
            token: g_userToken,
            city: g_city
        }
    }

    /**
     * 登陆
     * @param callback function 登陆成功之后的回调
     */
    Login(callback) {
        if (g_userContainer == "App") {
            this._appLogin(callback)
        } else {
            alert("Login no app")
        }
    }
};

// 实例化后再导出
export default new UserHelper()