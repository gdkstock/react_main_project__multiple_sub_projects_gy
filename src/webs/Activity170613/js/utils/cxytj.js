/**
 * Created by 小敏哥 on 2017/4/14.
 *
 * 车行易统计
 */
class Cxytj {
    //初始化操作
    initToWindow() {
        //此处必须加上异常捕获，防止出错之后停止运行导致页面没有渲染
        try {
            //异步初始化
            function tj() { //空统计
                this.cxytjReady = false;
                this.init = function (a) {
                }
                this.recordUserBehavior = function (a) {
                }
            }
            window.cxytj = new tj; //创建对象
            var hm = document.createElement("script");
            hm.src = "https://tongji.cx580.com/userBehavior/static/js/cxytongji_v3.0.js"; //统计JS的路径
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);

            //全局初始化参数
            window.cxytjReady = () => {
                //初始化通用数据
                window.cxytj.init({ //以下为初始化示例，可新增或删减字段
                    productId: 'insepection', //产品ID
                    productVersion: '1.0', //产品版本
                    productUserId: '', //APP端 （设备标记ID）
                    channel: 'AliPay', //推广渠道
                    isProduction: true, //生产环境or测试环境 默认测试环境
                    userId: sessionStorage.getItem('userId'), //用户ID
                    sessionId: '', //会话ID
                    ip: '', //IP地址
                    longitude: '', //经度
                    latitude: '' //纬度
                });
                window.cxytj.cxytjReady = true;
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("cxytjReady", false, false);
                document.dispatchEvent(evt);
            }
        }
        catch (e) {

        }
    }
}

export default new Cxytj();