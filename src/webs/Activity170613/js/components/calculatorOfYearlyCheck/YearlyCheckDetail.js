import React, { Component, PropTypes } from 'react'
import { Toast, Modal, Button } from 'antd-mobile'

// css
import Style from './index.scss'
// 加载actions
import yearly from '../../services/yearly'


import SendCxytjMessage from '../../utils/sendCxytjMessage'

// 弹窗蒙层
const isIPhone = new RegExp('\\biPhone\\b|\\biPod\\b', 'i').test(window.navigator.userAgent);
let maskProps;
if (isIPhone) {
    // Note: the popup content will not scroll.
    maskProps = {
        onTouchStart: e => e.preventDefault(),
    };
}

/*
* 组件
*/
class YearlyCheckDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            couponShow: false,
            text: '',
            getCouponBool: 0,
            used: '', //优惠券是否被使用
        }

    }

    componentWillMount() {
        document.querySelector("title").innerHTML = '年检详情'
        if (this.props.state == '1')
            SendCxytjMessage.sendMessage('h5_p_mot_EnterOrderMotpage')//  埋点
        else if (this.props.state == '2')
            SendCxytjMessage.sendMessage('h5_p_mot_EnterCheckMotpage')//  埋点
    }

    componentDidMount() {
        this.getCoupon();

    }

    componentWillUnmount() {
        //     console.log('----------------销毁-2---------------')
        this.setState({
            couponShow: false
        })

    }
    /**
     *  获取领取优惠券入口
     */
    getCoupon() {
        if (this.props.state != '1' && this.props.state != '2') {
            console.log('不在办理期')
            return false
        }
        let postData = {}
        yearly.getCoupon(postData).then(resultData => {
            //    console.log('+++++++ get_user_coupons 优惠券请求 = ', resultData);
            let data = resultData.data;
            if (resultData.code == 1000) {
                //flag 0:未领 
                //  1：领取了礼包
                ////2：优惠券未使用 未过期
                //3：优惠券未使用 已过期
                //4：优惠券已使用 
                let text = ''
                let getCQ = 0
                let used = ''
                let couponShowBool = true
                switch (data.flag) {
                    case '0':
                        text = '领券办理'
                        break;
                    case '1':
                    case '2':
                        text = '去使用'
                        getCQ = 1
                        used = 'end'
                        break;
                    case '3':
                    case '4':
                        couponShowBool = false
                        getCQ = 2
                        break;
                }
                this.setState({
                    couponShow: couponShowBool,
                    text: text,
                    getCouponBool: getCQ,
                    used: used
                })
            } else {
                Toast.info(resultData.msg, 2);
            }
        })
    }
    /**
     * 优惠券领取成功
     */
    clickCouponBtn() {

        Modal.alert('领取成功', <p className='color666 font-30'>20元年检优惠券已放入您的账户<br />可在“个人中心”—“我的优惠券”查看</p>, [
            { text: '暂不使用', onPress: () => console.log('cancel'), style: { color: '#666' } },
            { text: '去使用', onPress: () => this.clickCoupon(1), style: { color: '#E95454' } },
        ]);
    }

    /**
     * 点击了优惠领取按钮
     * * go-立即使用 ：2 表示 点击页面里面的按钮 立即使用；1 表示 点击弹窗里面的按钮  
     */
    clickCoupon(goto) {
        
        Toast.loading("", 20, () => Toast.info("网络错误,稍后再试", 2));
        if (this.state.getCouponBool || goto) {
            // 优惠券是： 去使用 
           // alert('埋点 = ' + goto + ' , ' + this.props.state)
            if (goto == 2) {
                if (this.props.state == '1') {
               //     alert('可预约年检，点击立即使用 = ' + goto + ' , ' + this.props.state)
                    SendCxytjMessage.sendMessage('h5_e_motordet_ClickUse')//  可预约年检状态页 : 埋点 点击立即使用
                }
                else if (this.props.state == '2')
               //     alert('逾期可办理，点击立即使用 = ' + goto + ' , ' + this.props.state)
                SendCxytjMessage.sendMessage('h5_e_motcheck_ClickUse')//  逾期可办理年检状态页: 埋点 点击立即使用
            } else if (goto == 1) {
                if (this.props.state == '1')
                    SendCxytjMessage.sendMessage('h5_e_motorder_ClickUse_ popups')//  可预约年检状态页 : 埋点
                else if (this.props.state == '2')
                    SendCxytjMessage.sendMessage('h5_e_motcheck_ClickUse_ popups')//  逾期可办理年检状态页: 埋点
            } else {
                if (this.props.state == '1')
                    SendCxytjMessage.sendMessage('h5_e_motordet_ClickUse_ Coupon')//  埋点
                else if (this.props.state == '2')
                    SendCxytjMessage.sendMessage('h5_e_motcheck_ClickUse_ Coupon')//  埋点
            }


            // 跳转到 测试-支付宝
            let userType = sessionStorage.getItem("userType")
            window.location.href = 'https://annualcheck.cx580.com/user/bindInfo?authType=' + userType + '&userType=' + userType + '&clientId=CheWu&redirectFlag=index'
            setTimeout(() => {
                Toast.hide();
            }, 1000)

        } else {
            // 领取优惠券
            if (this.props.state == '1')
                SendCxytjMessage.sendMessage('h5_e_motorder_ClickReceiveCoupon')//  埋点
            else if (this.props.state == '2')
                SendCxytjMessage.sendMessage('h5_e_motcheck_ClickReceiveCoupon')//  埋点

            let postData = {}

            yearly.hasCoupon(postData).then(resultData => {
                //    console.log('+++++++ get_activity_status 优惠券请求 = ', resultData);
                let data = resultData.data;
                Toast.hide();
                if (resultData.code == 1000) {
                    //flag 0:未领 1：领取成功 2：领取失败 3：已经领取过
                    let text = ''
                    switch (data.flag) {
                        case '1':
                            this.clickCouponBtn()
                            //    show=true
                            text = '去使用'
                            break;
                        default:
                            break;
                    }
                    this.setState({
                        couponShow: true,
                        text: text,
                        getCouponBool: 1
                    })
                } else {
                    Toast.info(resultData.msg, 2);
                }
            })
        }
    }
    /**
     * 获取年检状态的文字
     * @param {*string} state 年检状态 
     */
    getStateText(state) {
        let { agentFlag } = this.props
        state = state + ''
        let text = '';
        switch (state) { //年检状态；0：正常;1：可预约；2：逾期（仍可免检）；3：逾期；4：严重逾期；5：报废
            case '0': text = '正常'; break
            case '1': text = '可预约'; break
            case '2': text = `逾期${agentFlag ? '(仍可免检)' : ''}`; break
            case '3': text = '逾期'; break
            case '4': text = '严重逾期'; break
            case '5': text = '报废'; break
            default: text = '正常';
        }
        return <span className={Style['stateText' + state]}>{text}</span>;
    }

    /**
     * 获取年检状态的提示信息
     * @param {*string} state 年检状态 
     */
    getStateMsg(state) {
        let { days, agentFlag, bizFlag } = this.props
        state = state + ''
        let text = ''
        let htis = bizFlag == '1' ? '六年内新车年检' : '上线年检'
        switch (state) { //年检状态；0：正常;1：可预约；2：逾期（仍可免检）；3：逾期；4：严重逾期；5：报废
            case '0':
                text = <div>
                    <p>车辆距离年检预约还有{days}天，请放心驾驶。</p>
                    {bizFlag == '3' ? '' : <p>下一次年检类型：{htis}</p>}
                </div>;
                break
            case '1':
                text = <div>
                    <p>车辆已进入年检办理周期，请您在年检有效期内办完年检（还剩余{days}天），否则车辆将面临扣分和有事故不能出险的风险！</p>
                    <p>下一次年检类型：{htis}</p>
                </div>;
                break
            case '2':
                text = <div>
                    {agentFlag == '1' ?
                        <p>车辆未按规定在检验有效期内完成年检， 目前已逾期{days}天。请您尽快处理，否则逾期超过一年，将需要车主本人开车去车管所亲自办理。</p>
                        :
                        <p>车辆未按规定在检验有效期内完成年检，目前已逾期{days}天。按相关法律规定，需要车主本人开车去车管所亲自办理年检。</p>
                    }
                    <p>下一次年检类型：{htis}</p>
                </div>;
                break
            case '3':
                text = <div>
                    <p>车辆未按规定在检验有效期内完成年检，目前已逾期{days}天。按相关法律规定，需要车主本人开车去车管所亲自办理年检。</p>
                    <p>下一次年检类型：{htis}</p>
                </div>;
                break
            case '4':
                text = <div>
                    <p>车辆已连续两次年检逾期，请您在“下一次理论上的年检有效期”之内完成车辆上线检测，否则车辆将直接报废。</p>
                    <p>下一次年检类型：{htis}</p>
                </div>;
                break
            case '5':
                text = <div>
                    <p>车车辆未按规定在检验有效期内完成年检，并且逾期次数已连续超过3次。按我国机动车相关法规《机动车强制报废标准》的规定，车辆已达强制报废条件。请勿再驾驶此车上路，否则将被收缴，并面临罚款和吊销驾驶证的处罚。</p>
                </div>;
                break
            default:
                text = <div>
                    <p>车辆距离年检有效期还有{days}天，请放心驾驶。</p>
                    <p>下一次年检类型：{htis}</p>
                </div>;
        }
        return text;
    }



    render() {
        let { carNum, state, registerDate, inspectDate, clickLeftBtn, clickCouponBtn } = this.props
        return (
            <div className={Style.detailBox}>
                <div className={Style.topRIconbox} onClick={() => clickLeftBtn('top')}>重新计算</div>
                <div className={Style.carNum}>{carNum.substr(0, 2) + ' ' + carNum.substr(2)}</div>
                <div className={Style.stateBox}>
                    <i className={Style['stateIcon' + state]}></i>
                    {this.getStateText(state)}
                </div>
                <div className={Style.datesBox}>
                    <span>注册日期：{registerDate}</span><span>检验有效期：{inspectDate}</span>
                </div>
                <div className={Style.stateMsgBox}>
                    {this.getStateMsg(state)}
                </div>

                {this.state.getCouponBool != 2 ? '' : <div className={Style.stateBtnBox}>
                    <div className={Style.stateBtns}>
                        <span onClick={() => clickLeftBtn()}>返回</span><span onClick={() => this.clickCoupon(2)}>立即办理</span>
                    </div>
                </div>}

                {this.state.couponShow ?
                    <div className={Style.bottomList}>
                        <div className={Style.bottomBtn} onClick={() => this.clickCoupon()}>{this.state.text}</div>
                        <img className={Style.bottomImg} src={'./images/qp' + this.state.used + '.png'} />
                    </div> :
                    ''}

            </div>
        );
    }
}

YearlyCheckDetail.defaultProps = {
    carNum: '',
    state: '0', //年检状态；0：正常;1：可预约；2：逾期（仍可免检）；3：逾期；4：严重逾期；5：报废
    agentFlag: true, //年检类型：true为6年新车年检；false为上线年检
    bizFlag: 0,
    registerDate: '', //注册时间
    inspectDate: '', //检验有效期
    days: '', //文案提醒中的天数 (有效天数、逾期天数等)
    clickTopBtn: () => console.log("点击了顶部按钮（重新计算按钮）"),
    clickLeftBtn: () => console.log("点击了左边按钮（返回）"),
    clickRightBtn: () => console.log("点击了右边按钮（立即办理）")
};

export default YearlyCheckDetail;