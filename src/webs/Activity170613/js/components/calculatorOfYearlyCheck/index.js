import React, { Component, PropTypes } from 'react'
import { Toast, DatePicker, List, Popup } from 'antd-mobile'
import { createForm } from 'rc-form'

// 样式
import 'style/calculatorOfYearlyCheck/antd.scss'
import Style from './index.scss'

// 加载actions
import yearly from '../../services/yearly'

// 时间换格式
import moment from 'moment'
const zhNow = moment().locale('zh-cn').utcOffset(8);
const maxDate = moment().utcOffset(8);
const minDate = moment('1998-01-01 +0800', 'YYYY-MM-DD Z').utcOffset(8);


class CalculatorOfYearlyCheck extends Component {
    constructor(props) {
        super(props);

        let { carNumber, registerDate, inspectDate } = this.props
        this.state = {
            dpValue: registerDate ? moment(registerDate + ' +0800', 'YYYY-MM-DD Z').utcOffset(8) : '', //车辆注册时间
            dateList: {
                date: '', //这个列表对应的时间
                list: [], //列表
                show: false, //显示
            },
            carNum: '',
            inspectDate: inspectDate,

        }
    }

    componentWillMount() {
    }

    componentDidMount() {
        // console.log('aaaaaaaaaaaaaaaa' + this.props.carNumber)
    }

    componentWillReceiveProps(props) {
        let { carNumber, registerDate, inspectDate } = props

        this.setState({
            dpValue: registerDate ? moment(registerDate + ' +0800', 'YYYY-MM-DD Z').utcOffset(8) : '', //车辆注册时间
            carNum: '',
            inspectDate: inspectDate,
        })

    }

    componentWillUnmount() {

        //   console.log('bbbbbbbbb' + this.props.carNumber)
    }



    /**
     * 联动获取检验有效时间列表
     * @param registerDate 注册时间，用于得到有效期列表
     * @param data 注册时间，用于赋值
     */
    getRegisterDate(registerDate, clickFun) {
        if (registerDate == this.state.dateList.date) {
            this.showPopDate(this.state.dateList.list);
            return false;
        }
        //请求成功
        Toast.hide();
        Toast.loading("", 10, () => Toast.info("网络错误", 2));
        let dateList = []
        let postData = {
            userId: sessionStorage.getItem("userId"),
            registerDate: registerDate, //	string	是	车辆注册日期（yyyy-MM-dd）
        }
        yearly.getCheckDateList(postData).then(resultData => {
            Toast.hide();
            //   console.log('+++++++ inspection/getCheckDateList 检验有效期请求 = ', resultData);
            if (resultData.code == 1000) {

                let data = resultData.data.dateList
                let inspectDate = ''

                for (let j = 0; j < data.length; j++) {
                    dateList.push(data[j].checkDate)
                    if (data[j].default == '1') {
                        this.setState({
                            inspectDate: data[j].checkDate
                        })
                    }
                }

                //修改检验有效期列表
                this.setState({
                    dateList: Object.assign({}, this.state.dateList, {
                        date: registerDate,
                        list: dateList
                    })
                })
                return dateList
            } else {
                Toast.info(resultData.msg, 2);
            }
        }).then((dateList) => clickFun && this.showPopDate(dateList));

    }


    /**
  * 页面点击了 ：选择有效期
  */
    showDateList() {
        let dpValue = this.state.dpValue
        let registerDate = dpValue ? dpValue.format("YYYY-MM-DD") : ''

        if (!registerDate) {
            Toast.info("请先选择车辆注册日期", 1)
            return;
        }

        this.getRegisterDate(registerDate, true) // 请求

    }

    /**
     * 页面弹窗显示 检验有效期
     */
    showPopDate(dateList) {
        if (!dateList.length
        ) { Toast.info('无可选有效期', 2); return false; }

        let list = dateList.map((item, i) =>
            <div key={'dateList' + i} onClick={(date) => this.showDate(item)}>{item}</div>
        )
        let JSX = <div className={Style.dateList}>{list}</div>
        Popup.show(JSX, { animationType: 'slide-up' });
    }

    showDate(date) {
        Popup.hide()
        this.setState({
            inspectDate: date
        })
    }

    /**
     * 车辆注册日期发送改变
     */
    datePickerOnChange(dpValue) {
        //日期格式化
        let date = dpValue.format("YYYY-MM-DD"); //选择的注册日期

        this.setState({
            dpValue: dpValue
        })
        this.showDate('') //清空检验有效期
        this.getRegisterDate(date, false) //返回注册时间给父组件
    }

    /**
 * 保存
 */
    send() {
        let { dpValue, inspectDate } = this.state

        let registerDate = dpValue ? dpValue.format("YYYY-MM-DD") : ''

        this.props.onClick(registerDate, inspectDate || '') //返回车辆注册日期和检验有效期
    }

    /**
     * 
     */
    render() {
        let { carNumber, inspectDate } = this.props
        const { getFieldProps } = this.props.form

        return (
            <div className='box'>

                <List className="date-picker-list">
                    <DatePicker
                        mode="date"
                        title="选择日期"
                        extra={<span className={Style.listItemExtra}>{this.props.spanDom}</span>}
                        {...getFieldProps('date1', {}) }
                        minDate={minDate}
                        maxDate={maxDate}
                        value={this.state.dpValue}
                        onChange={v => this.datePickerOnChange(v)}
                    >
                        <List.Item arrow="horizontal"><span className={Style.listItem}>车辆注册日期</span></List.Item>
                    </DatePicker>

                    <List.Item extra={
                        <span className={Style.listItemExtra} style={{ color: '#6a6a6a' }}>
                            {
                                this.state.inspectDate ?
                                    this.state.inspectDate : this.props.spanDom
                            }
                        </span>
                    }
                        arrow="horizontal"
                        onClick={() => this.showDateList()}>
                        <span className={Style.listItem}>检验有效期至</span>
                    </List.Item>

                </List>

                <div className={Style.btn} onClick={() => this.send()}>保存并查询</div>

            </div >
        );
    }
}

CalculatorOfYearlyCheck.defaultProps = {
    carNumber: '',
    dateList: [], //检验有效期列表
    registerDate: '', //车辆注册日期
    inspectDate: '', //检验有效期
    onClick: (r, i) => console.log("点击了保存按钮!", "注册时间：" + r, "检验有效期：" + i), //点击保存按钮
    btnText: '保存并查询',
    spanDom: <span className='color999 font-30'>请选择</span>,
};

export default createForm()(CalculatorOfYearlyCheck);
