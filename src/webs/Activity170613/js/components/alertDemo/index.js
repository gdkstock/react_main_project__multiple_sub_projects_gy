import React, { Component, PropTypes } from 'react';
import Style from './index.scss'

class AlertDemo extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div className={Style.alertBox} onClick={() => this.props.close()}>
                <div className={Style.demo + ' center-center-column'}>
                    {this.props.demo}
                </div>
            </div>
        );
    }
}

AlertDemo.propTypes = {

};

AlertDemo.defaultProps = {
    demo: '展示的内容，支持HTML标签', //需要展示的demo
    close: () => console.log('点击关闭按钮'), //点击关闭按钮
}

export default AlertDemo;
