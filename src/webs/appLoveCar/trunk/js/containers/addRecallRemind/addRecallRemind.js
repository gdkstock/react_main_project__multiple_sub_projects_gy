import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast } from 'antd-mobile'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './addRecallRemind.scss'




class addRecallRemind extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    render() {

        return (
            <div className={Style.addRRemind}>
                <div className={Style.topGroups}>
                    <div className={Style.chepai_icon}>
                        <img src="./images/icon_brand@2x.png" alt=""/>
                    </div>
                    <div className={Style.carNum}>粤A12345</div>
                    <p className={Style.content}>精准估值，随时掌握爱车市场动态</p>
                </div>
                <div className={Style.selectGroups}>
                    <div className={Style.label}>选择车型</div>
                    <div className={Style.selected_info}>请选择</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div>
                    {/* <div className={Style.border_bottom_line}></div> */}
                </div>
                <p className={Style.tip}>召回结果来源召回官方网站，查询结果仅供参考</p>
                <button className={Style.btn}>保存</button>
                <div className={Style.recallMsgBox}>
                    <div className={Style.top}>
                        <span>国家召回公告</span>
                    </div>
                    <div className={Style.list}>
                        <div className={Style.itemBox}>
                            <span className={Style.title}>北京奔驰汽车有限公司召回部分GLA级和E级汽车</span>
                            <span className={Style.time}>2017-11-01</span>
                        </div>
                        <div className={Style.itemBox}>
                            <span className={Style.title}>北京奔驰汽车有限公司召回部分GLA级和E级汽车北京奔驰汽车有限公司召回部分GLA级和E级汽车</span>
                            <span className={Style.time}>2017-11-01</span>
                        </div>
                        <div className={Style.itemBox}>
                            <span className={Style.title}>北京奔驰汽车有限公司召回部分GLA级和E级汽车</span>
                            <span className={Style.time}>2017-11-01</span>
                        </div>
                        <div className={Style.itemBox}>
                            <span className={Style.title}>北京奔驰汽车有限公司召回部分GLA级和E级汽车</span>
                            <span className={Style.time}>2017-11-01</span>
                        </div>
                        <div className={Style.itemBox}>
                            <span className={Style.title}>北京奔驰汽车有限公司召回部分GLA级和E级汽车</span>
                            <span className={Style.time}>2017-11-01</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default addRecallRemind
