import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast} from 'antd-mobile'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './valuationDetail.scss'


class valuationDetail extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <div className={Style.vDetail}>
                <div className={Style.carInfoBox}>
                    <div className={Style.img}>
                        <img src="./images/car.png" alt=""/>
                    </div>
                    <span className={Style.carNum}>粤A12345</span>
                    <span className={Style.carType}>奥迪a4L  2018款 30周年年型 45 TFSI  进取版动感车型 TFSI  进取版 动感车型</span>
                </div>
                <div className={Style.nowValuation}>
                    <div className={Style.title}>
                        <span>当前估价</span>
                    </div>
                    <div className={Style.box}>
                        <div className={Style.left}>
                            <span>商家报价</span>
                            <span>50万元</span>
                            <span>建议售价</span>
                        </div>
                        <div className={Style.line_mid_short}></div>
                        <div className={Style.right}>
                            <span>个人报价</span>
                            <span>60万元</span>
                            <span>建议售价</span>
                        </div>
                        <div className={Style.line_mid_long}></div>
                        <div className={Style.bottom}>
                            <span>上次报价时间：2017/11/20</span>
                            <span>估值结果由精真估技术提供</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default valuationDetail
