import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast, List } from 'antd-mobile'
import { DatePicker } from 'ys-antd-mobile2'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './addInsuranceRemind.scss'


/**
 * 获取最大和最小时间（日期格式）
 */
const getDates = () => {
    const date = new Date();
    let maxDate = date

    return {
        maxDate
    }
}

const t = ''

class addInsuranceRemind extends Component {

    constructor(props) {
        super(props)
        const { minDate, maxDate } = getDates();
        this.state = {
            showPicker: false,
            choseObj:'请选择',
            insuranceCompanyData:[
                {
                    src: './images/dazhong.png',
                    company: '大众汽车保险'
                },
                {
                    src: './images/minan.png',
                    company: '民安车险'
                },
                {
                    src: './images/huaan.png',
                    company: '华安车险'
                },
                {
                    src: './images/ancheng.png',
                    company: '安诚车险'
                },
                {
                    src: './images/renshou.png',
                    company: '中国人寿财险'
                },
                {
                    src: './images/huakang.png',
                    company: '华康保险'
                },
                {
                    src: './images/meiya.png',
                    company: '美亚保险'
                },
            ],
            // 最小可选择的日期
            minDate,
            
            // 最大可选择的日期
            maxDate,
        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    _showPicker(){
        this.setState({
            showPicker: true,
        })
    }

    _closePicker(){
        this.setState({
            showPicker: false,
        })
    }

    _choseObj(e){
        e.stopPropagation();
        console.log(e.target.innerText)
        this.t = e.target.innerText
    }

    _confirm(){
        this.setState({
            choseObj: this.t,
            showPicker: false,
        })
    }

    render() {

        const {showPicker, choseObj, insuranceCompanyData, minDate, maxDate} = this.state

        return (
            <div className={Style.addIRemind}>
                <div className={Style.topGroups}>
                    <div className={Style.chepai_icon}>
                        <img src="./images/icon_brand@2x.png" alt=""/>
                    </div>
                    <div className={Style.carNum}>粤A12345</div>
                    <p>开通保险提醒，一次设置，永不脱险</p>
                </div>
                <div className={Style.selectGroups}>
                    <div className={Style.label}>上次投保公司</div>
                    <div className={Style.selected_info} onClick={() => this._showPicker()}>{choseObj}</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div>
                </div>
                <div className={Style.selectGroups}>
                    {/* <div className={Style.label}>上次投保时间</div>
                    <div className={Style.selected_info}>请选择</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div> */}
                    <DatePicker
                        mode="date"
                        title="选择时间"
                        maxDate={maxDate}
                        value={this.state.date}
                        onChange={date => this.setState({ date })}
                    >
                        <List.Item arrow="horizontal">上次投保时间</List.Item>
                    </DatePicker>
                </div>
                <button className={Style.btn}>保存</button>

                {/* 选择保险公司 */}
                {/* Picker ---开始--- */}
                <div className={showPicker ? Style.pickerBox : 'hide'}>
                    <div className={Style.popup}>
                        <div className={Style.top}>
                            <span className={Style.close} onClick={() => this._closePicker()}>取消</span>
                            <span className={Style.title}>选择保险公司</span>
                            <span className={Style.ok} onClick={() => this._confirm()}>完成</span>
                        </div>
                        <div className={Style.list}>
                            <ul>
                                {
                                    insuranceCompanyData.map((insuranceCompanyItem, insuranceCompanyIndex) => 
                                    <li key={insuranceCompanyIndex}>
                                        <div className={Style.logo}>
                                            <img src={insuranceCompanyItem.src} alt=""/>
                                        </div>
                                        <div className={Style.name} onClick={(e) => this._choseObj(e)}>{insuranceCompanyItem.company}</div>
                                    </li>
                                    )
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default addInsuranceRemind
