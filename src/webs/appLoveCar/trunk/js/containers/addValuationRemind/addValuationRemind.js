import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast, List } from 'antd-mobile'
import { DatePicker } from 'ys-antd-mobile2'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './addValuationRemind.scss'


/**
 * 获取最大和最小时间（日期格式）
 */
const getDates = () => {
    const date = new Date();
    let maxDate = date

    return {
        maxDate
    }
}

class addValuationRemind extends Component {

    constructor(props) {
        super(props)
        const { minDate, maxDate } = getDates();
        this.state = {
            distance:'',
            // 最小可选择的日期
            minDate,
            
            // 最大可选择的日期
            maxDate,
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        // 键盘初始化
        this.keyboardInit();
    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }


    /**
     * 键盘实例初始化
     */
    keyboardInit() {
        const { distance } = this.state;
        const inputs = [
            {
                selectors: '#distance',
                type: 'digit',
                maxLength: 5,
                placeholder: '请填写',
                value: distance || '',
            }
        ];
        cxyKeyboard.init({ inputs });

        // 内容发生改变
        cxyKeyboard.onChange = (value, keyboardId) => {
            console.log(value)
            this.setState({
                distance: value
            })
        };
    }

    /**
     * 显示键盘
     * @param {string} id 
     */
    showKeyboard(id) {
        const param = {
            selectors: id,
            animation: !cxyKeyboard.isShow, // 键盘不存在时则显示动画
        }

        // 键盘如果存在，则切换键盘。不存在则显示键盘
        cxyKeyboard.show(param, cxyKeyboard.isShow);
    }

    render() {
        const {distance, minDate, maxDate} = this.state
        return (
            <div className={Style.addVRemind}>
                <div className={Style.topGroups}>
                    <div className={Style.chepai_icon}>
                        <img src="./images/icon_brand@2x.png" alt=""/>
                    </div>
                    <div className={Style.carNum}>粤A12345</div>
                    <p>精准估值，随时掌握爱车市场动态</p>
                </div>
                <div className={Style.selectGroups_car}>
                    <div className={Style.label_car}>选择车型</div>
                    <div className={Style.selected_info_car}>宝马5系</div>
                    <span className={Style.car_msg}>2018款 528Li 上市特别版</span>
                    <div className={Style.arrow_icon_car}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div>
                </div>
                
                <div className={Style.selectGroups}>
                    {/* <div className={Style.label}>上牌时间</div>
                    <div className={Style.selected_info}>2017-12-12</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div> */}
                    <DatePicker
                        mode="date"
                        title="选择时间"
                        maxDate={maxDate}
                        value={this.state.date}
                        onChange={date => this.setState({ date })}
                    >
                        <List.Item arrow="horizontal">上牌时间</List.Item>
                    </DatePicker>
                </div> 
               
                {/* <DatePicker
                    mode="date"
                    title="选择时间"
                    minDate={minDate}
                    maxDate={maxDate}
                    value={this.state.date}
                    onChange={date => this.setState({ date })}
                >
                    <List.Item arrow="horizontal">上牌时间</List.Item>
                </DatePicker> */}
                <div className={Style.selectGroups} onClick={() => this.showKeyboard('#distance')}>
                    <div className={Style.label}>行驶里程</div>
                    <div id='distance' className={Style.selected_info_count}></div>
                    <span className={Style.count_unit}>万公里</span>
                </div>
                <div className={Style.selectGroups}>
                    <div className={Style.label}>选择城市</div>
                    <div className={Style.selected_info}>广州</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div>
                </div>
                <button className={Style.btn}>保存</button>
            </div>
        )
    }
}

export default addValuationRemind
