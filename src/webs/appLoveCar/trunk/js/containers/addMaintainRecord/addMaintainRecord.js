import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast, List } from 'antd-mobile'
import { DatePicker } from 'ys-antd-mobile2'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './addMaintainRecord.scss'


/**
 * 获取最大和最小时间（日期格式）
 */
const getDates = () => {
    const date = new Date();
    let maxDate = date

    return {
        maxDate
    }
}

class addMaintainRecord extends Component {

    constructor(props) {
        super(props)
        const { minDate, maxDate } = getDates();
        this.state = {
            distance:'',
            maintainProject:[
                '小保养服务',
                '大保养服务',
                '空调滤清器',
                '雨刷',
                '空调制冷剂',
                '更换防冻冷却液',
                '刹车油',
                '火花塞',
                '内置燃油滤',
                '刹车盘',
                '刹车片',
                '变速箱油',
                '大灯',
                '雾灯',
                '发动机清洗',
                '节气门清洗',
                '空调管路杀菌',
                '蒸发箱清洗',
                '燃油系统养护',
                '刹车系统养护',
                '水箱防锈保护',
                '水箱清洗',
                '进气系统清洗',
                '三元催化清洗',
                '喷油嘴清洗',
                '发动机养护',
                '助力转向油',
                '正时皮带套装',
                '发动机舱清洗',
                '前减振器',
                '后减振器',
            ],

            // 最小可选择的日期
            minDate,
            
            // 最大可选择的日期
            maxDate,
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.keyboardInit();
    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

     /**
     * 键盘实例初始化
     */
    keyboardInit() {
        const { distance } = this.state;
        const inputs = [
            {
                selectors: '#distance',
                type: 'digit',
                maxLength: 5,
                placeholder: '请填写',
                value: distance || '',
            }
        ];
        cxyKeyboard.init({ inputs });

        // 内容发生改变
        cxyKeyboard.onChange = (value, keyboardId) => {
            console.log(value)
            this.setState({
                distance: value
            })
        };
    }

    /**
     * 显示键盘
     * @param {string} id 
     */
    showKeyboard(id) {
        const param = {
            selectors: id,
            animation: !cxyKeyboard.isShow, // 键盘不存在时则显示动画
        }

        // 键盘如果存在，则切换键盘。不存在则显示键盘
        cxyKeyboard.show(param, cxyKeyboard.isShow);
    }

    render() {
        
        const {maintainProject, distance, minDate, maxDate} = this.state
        return (
            <div className={Style.addMRecord}>
                <div className={Style.topGroups}>
                    <div className={Style.chepai_icon}>
                        <img src="./images/icon_brand@2x.png" alt=""/>
                    </div>
                    <div className={Style.carNum}>粤A12345</div>
                    <p>添加保养记录，定期保养，爱车常新</p>
                </div>
                <div className={Style.selectGroups}>
                    {/* <div className={Style.label}>上次保养时间</div>
                    <div className={Style.selected_info}>请选择</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div> */}
                    <DatePicker
                        mode="date"
                        title="选择时间"
                        maxDate={maxDate}
                        value={this.state.date}
                        onChange={date => this.setState({ date })}
                    >
                        <List.Item arrow="horizontal">上次保养时间</List.Item>
                    </DatePicker>
                </div>
                <div className={Style.selectGroups} onClick={() => this.showKeyboard('#distance')}>
                    <div className={Style.label}>行驶里程</div>
                    <div id='distance' className={Style.selected_info_car}> </div>
                    <span className={Style.unit}>km</span>
                </div>
                <div className={Style.title}>请选择保养项目（以便为您推荐下次保养项目）</div>
                <div className={Style.projectBox}>
                    <div className={Style.projectList}>
                        <ul>
                            {
                                maintainProject.map((maintainProjectItem) => 
                                    <li key={maintainProjectItem}>{maintainProjectItem}</li>
                                )
                            }
                        </ul>
                    </div>
                </div>
                <button className={Style.btn}>保存</button>
            </div>
        )
    }
}

export default addMaintainRecord
