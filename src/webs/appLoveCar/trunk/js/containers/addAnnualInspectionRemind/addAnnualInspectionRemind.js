import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast, List } from 'antd-mobile'
import { DatePicker } from 'ys-antd-mobile2'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './addAnnualInspectionRemind.scss'


/**
 * 获取最大和最小时间（日期格式）
 */
const getDates = () => {
    const date = new Date();
    let maxDate = date

    return {
        maxDate
    }
}

class addAnnualInspectionRemind extends Component {

    constructor(props) {
        super(props)
        const { minDate, maxDate } = getDates();
        this.state = {
            // 最小可选择的日期
            minDate,
            
            // 最大可选择的日期
            maxDate,
        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        const { minDate, maxDate } = this.state
        return (
            <div className={Style.addAIRemind}>
                <div className={Style.topGroups}>
                    <div className={Style.chepai_icon}>
                        <img src="./images/icon_brand@2x.png" alt=""/>
                    </div>
                    <div className={Style.carNum}>粤A12345</div>
                    <p>开通年检提醒，年检及时办，不扣分不罚款</p>
                </div>
                <div className={Style.selectGroups}>
                    {/* <div className={Style.label}>注册时间（上牌时间）</div>
                    <div className={Style.reg_prompt_icon}>
                        <img src="./images/icon-prompt.png" alt=""/>
                    </div>
                    <div className={Style.selected_info}>请选择</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div> */}
                    <DatePicker
                        mode="date"
                        title="选择时间"
                        maxDate={maxDate}
                        value={this.state.date}
                        onChange={date => this.setState({ date })}
                    >
                        <List.Item arrow="horizontal">注册时间（上牌时间）</List.Item>
                    </DatePicker>
                </div>
                <div className={Style.selectGroups}>
                    {/* <div className={Style.label}>校验有效期至</div>
                    <div className={Style.end_prompt_icon}>
                        <img src="./images/icon-prompt.png" alt=""/>
                    </div>
                    <div className={Style.selected_info}>请选择</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div> */}
                    <DatePicker
                        mode="date"
                        title="选择时间"
                        maxDate={maxDate}
                        value={this.state.date}
                        onChange={date => this.setState({ date })}
                    >
                        <List.Item arrow="horizontal">校验有效期至</List.Item>
                    </DatePicker>
                </div>
                <button className={Style.btn}>提交审核</button>
            </div>
        )
    }
}

export default addAnnualInspectionRemind
