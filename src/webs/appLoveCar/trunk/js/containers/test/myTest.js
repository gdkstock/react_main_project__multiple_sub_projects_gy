import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast, List } from 'antd-mobile'
import { DatePicker } from 'ys-antd-mobile2'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './myTest.scss'


const t = ''

const nowTimeStamp = Date.now();
const now = new Date(nowTimeStamp);
let minDate = new Date(nowTimeStamp - 1e7);
const maxDate = new Date(nowTimeStamp + 1e7);

class myTest extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showPicker: false,
            showDatePicker: false,
            choseObj: '请选择',
            insuranceCompanyData: [
                {
                    src: './images/dazhong.png',
                    company: '大众汽车保险'
                },
                {
                    src: './images/minan.png',
                    company: '民安车险'
                },
                {
                    src: './images/huaan.png',
                    company: '华安车险'
                },
                {
                    src: './images/ancheng.png',
                    company: '安诚车险'
                },
                {
                    src: './images/renshou.png',
                    company: '中国人寿财险'
                },
                {
                    src: './images/huakang.png',
                    company: '华康保险'
                },
                {
                    src: './images/meiya.png',
                    company: '美亚保险'
                },
            ],

            date: now,
        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    _showPicker() {
        this.setState({
            showPicker: true,
        })
    }

    _closePicker() {
        this.setState({
            showPicker: false,
        })
    }

    _choseObj(e) {
        e.stopPropagation();
        console.log(e.target.innerText)
        this.t = e.target.innerText
    }

    _confirm() {
        this.setState({
            choseObj: this.t,
            showPicker: false,
        })
    }

    _showDatePicker() {
        this.setState({
            showDatePicker: true,
        })
    }


    render() {
        const { showPicker, choseObj, insuranceCompanyData } = this.state
        return (
            <div className={Style.addIRemind}>
                <div className={Style.selectGroups}>
                    <div className={Style.label}>上次投保公司</div>
                    <div className={Style.selected_info} onClick={() => this._showPicker()}>{choseObj}</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt="" />
                    </div>
                </div>
                <div className={Style.selectGroups}>
                    <div className={Style.label}>上牌时间</div>
                    <div className={Style.selected_info}>2017-12-12</div>
                    <div className={Style.arrow_icon}>
                        <img src="./images/arrow_copy.png" alt=""/>
                    </div>
                </div>
                <DatePicker
                    mode="date"
                    title="选择时间"
                    minDate={minDate}
                    maxDate={maxDate}
                    value={this.state.date}
                    onChange={date => this.setState({ date })}
                >
                    <List.Item arrow="horizontal">上牌时间</List.Item>
                </DatePicker>
                {/* Picker ---开始--- */}
                <div className={showPicker ? Style.pickerBox : 'hide'}>
                    <div className={Style.popup}>
                        <div className={Style.top}>
                            <span className={Style.close} onClick={() => this._closePicker()}>取消</span>
                            <span className={Style.title}>选择保险公司</span>
                            <span className={Style.ok} onClick={() => this._confirm()}>完成</span>
                        </div>
                        <div className={Style.list}>
                            <ul>
                                {
                                    insuranceCompanyData.map((insuranceCompanyItem, insuranceCompanyIndex) =>
                                        <li key={insuranceCompanyIndex}>
                                            <div className={Style.logo}>
                                                <img src={insuranceCompanyItem.src} alt="" />
                                            </div>
                                            <div className={Style.name} onClick={(e) => this._choseObj(e)}>{insuranceCompanyItem.company}</div>
                                        </li>
                                    )
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default myTest
