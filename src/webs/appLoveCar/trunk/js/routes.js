import React from 'react'
import { Route, IndexRoute } from 'react-router'

//antd UI
import { Toast } from 'antd-mobile'

import {
  App,
  Home,
  NotFoundPage,
} from './containers'

//显示加载中
const showLoading = () => {
  Toast.loading('', 30, () => {
    window.networkError('./images/networkError-icon.png')
  })
}

export default (
  <Route path="/" component={App}>

    <IndexRoute component={Home} />

    {/* 添加年检提醒 */}
    <Route path="addAnnualInspectionRemind" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/addAnnualInspectionRemind/addAnnualInspectionRemind').default)
      }, 'car')
    }} />

    {/* 添加保险提醒 */}
    <Route path="addInsuranceRemind" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/addInsuranceRemind/addInsuranceRemind').default)
      }, 'car')
    }} />

    {/* 添加估值提醒 */}
    <Route path="addValuationRemind" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/addValuationRemind/addValuationRemind').default)
      }, 'car')
    }} />

    {/* 添加召回提醒 */}
    <Route path="addRecallRemind" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/addRecallRemind/addRecallRemind').default)
      }, 'car')
    }} />

    {/* 添加保养记录 */}
    <Route path="addMaintainRecord" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/addMaintainRecord/addMaintainRecord').default)
      }, 'car')
    }} />

    {/* 估值详情页 */}
    <Route path="valuationDetail" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/valuationDetail/valuationDetail').default)
      }, 'car')
    }} />

    {/* 测试页面 */}
    <Route path="myTest" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/test/myTest').default)
      }, 'car')
    }} />
    {/*按需demo start*/}
    {/*<IndexRoute getComponents={(nextState, cb) => {
      require.ensure([], (require) => {
        cb(null, require('./containers/carList/index').default)
      }, 'index')
    }} />
    <Route path="addCar" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/addCar').default)
      }, 'car')
    }} />*/}
    {/*按需demo end*/}

    <Route path="*" component={NotFoundPage} />
  </Route>
);