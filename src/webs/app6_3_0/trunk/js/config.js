/**
 * 配置
 */
const production = window.location.host.indexOf('helpdo.cx580.com') > -1 || window.location.host.indexOf('pre.cx580.com') > -1 //是否为生产环境

export default {
    projectName:'app6_3_0', //项目名称
    production: production, //是否为生产环境
    baseApiUrl: production ? window.location.protocol + "//" + window.location.host + "/" : "http://192.168.1.168:9025/", //接口地址
}