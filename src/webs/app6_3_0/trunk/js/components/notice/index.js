import React, { Component } from 'react';

//style
import styles from './index.scss'

class Notice extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: true, //显示
        }

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    render() {
        let { style, className, close , text } = this.props
        return (
            <div className={styles.noticeBox + ' ' + className} style={style}>
                <div className={styles.closeBtn} onClick={() => close()}></div>
                <p>{ text }</p>
            </div>
        );
    }
}

Notice.defaultProps = {
    style: {}, //样式
    className: '', //类名
    close: () => console.log("关闭"),
    text:"",//内容
}

export default Notice;