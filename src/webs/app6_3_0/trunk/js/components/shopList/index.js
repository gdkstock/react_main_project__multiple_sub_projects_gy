/**
 * 帮忙办-商家列表
 */

//style
import styles from './index.scss'


/**
 * 商家
 */

// const services = [{name:"违章代办"},{name:"洗车"},{name:"违章代办"}]
export const Shop = props => {
    let { services } = props
    return (
        <div className={styles.shop} onClick={() => props.handleClick(props)}>
            <div className={styles.img}>
                <img src={props.shopImgUrl[0].smallImageUrl} />
            </div>
            <div className={styles.fr+" abs-v-center"}>
                <div className={styles.name + ' text-overflow-1'}>{props.shopName}</div>
                <div className={styles.servers}>
                    {
                        services.map((item, i) => <span>{item.name}</span>)
                    }
                </div>
                <div className={styles.footer}>
                    <div className={styles.distance}>{props.distance}km</div>
                    <div className={styles.address + ' text-overflow-1'}>{props.shopAddress}</div>
                </div>

            </div>
        </div>
    )
}

/**
 * 头部导航
 */
export const HeaderNav = props => {
    let defaultProps = {
        address: {
            key: "address",
            name: '广州',
            click: name => console.log("点击了地区", name),
            checked: true
        },
        server: {
            key: "server",
            name: '保养',
            click: name => console.log("点击了服务", name),
            checked: false
        }
    }

    props = Object.assign({}, defaultProps, props)
    let { address, server } = props

    return (
        <div className={styles.header}>
            <div
                onClick={() => address.click(address)}
                className={styles.navTitle + ' ' + (address.checked ? styles.curr : '')}>
                <span>{address.name}</span>
            </div>
            <div
                onClick={() => server.click(server)}
                className={styles.navTitle + ' ' + (server.checked ? styles.curr : '')}>
                <span>{server.name}</span>
            </div>
        </div>
    )
}


/**
 * 字母索引
 */
const Indexes = props => {
    let defaultProps = {
        indexes: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
        click: key => console.log("点击了：", key)
    }
    props = Object.assign({}, defaultProps, props)
    let { indexes, click } = props
    return (
        <div className={styles.indexesBox}>
            <ul>
                <li>当前</li>
                {
                    indexes.map(key =>
                        <li onTouchStart={() => click(key)}>{key}</li>
                    )
                }
            </ul>
        </div>
    )
}

/**
 * 城市列表
 */
export const AddressList = props => {
    let defaultProps = {
        current: '广州',
        data: {},
        click: () => console.log("点击了"),
        clickIndexes: (key) => console.log("点击的索引：", key),
        clickMask: () => console.log("点击了蒙层")
    }
    props = Object.assign({}, defaultProps, props)
    let { current, data, click, clickIndexes, clickMask} = props
    return (
        <div className={styles.fixedTopBox} onClick={() => clickMask({ name: "" })}>
            {
                Object.keys(data).length > 0
                    ? <div className={styles.addrList} id='addressList'>
                        <div className={styles.addrItem}>
                            <div className={styles.addrItemTitle}>当前</div>
                            <div className={styles.addrItemName + ' ' + styles.curr}>{current}</div>
                        </div>
                        {
                            Object.keys(data).map(key =>
                                <div key={key} className={styles.addrItem}>
                                    <div className={styles.addrItemTitle + ' indexesKey-' + key}>{key}</div>
                                    {
                                        data[key].map(item =>
                                            <div
                                                onClick={() => click(item)}
                                                key={item.cityId}
                                                className={styles.addrItemName}>{item.cityName}</div>
                                        )
                                    }
                                </div>
                            )
                        }
                    </div>
                    : <div className={styles.addrList}>
                        <div className={styles.addrItem}>
                            <div className={styles.addrItemTitle}>当前</div>
                            <div className={styles.addrItemName + ' ' + styles.curr}>广州</div>
                        </div>
                    </div>
            }
            <Indexes click={key => clickIndexes(key)} indexes={Object.keys(data)} />
        </div>
    )
}

/**
 * 服务列表
 */
export const ServerList = props => {
    let defaultProps = {
        current: '广州',
        data: [],
        click: () => console.log("点击了"),
        clickMask: () => console.log("点击了蒙层"),
    }
    props = Object.assign({}, defaultProps, props)
    let { current, data, click, clickMask} = props

    let defaultClass = styles.serverItem;
    let activeClass = styles.serverItem + " " + styles.curr

    return (
        <div className={styles.fixedTopBox} onClick={() => clickMask({ name: "" })}>
            {
                data.length > 0
                    ? <div className={styles.serverList}>
                        <div className={current == "全部" ? activeClass : defaultClass} onClick={() => click({ name: "全部", code: "" })}>全部</div>
                        {
                            data.map((item, i) =>
                                <div
                                    key={i}
                                    onClick={() => click(item)}
                                    className={item.name == current ? activeClass : defaultClass}
                                    >
                                    {item.name}
                                </div>
                            )
                        }

                    </div>
                    : <div className={styles.serverList}>
                        <div className={styles.serverItem + ' ' + styles.curr} onClick={() => click({ name: "全部", code: "" })}>全部</div>
                    </div>
            }
        </div>
    )
}