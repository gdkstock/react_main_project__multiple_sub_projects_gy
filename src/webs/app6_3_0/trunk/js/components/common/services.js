/**
 * 商家服务列表
 */

import styles from './services.scss'

import { Service } from './index'

const Services = props => {
    let defaultProps = {
        title: '商家服务',
        list: []
    }
    props = Object.assign({}, defaultProps, props)

    let { title, list } = props

    return (
        <div>
            <div className={styles.title}>{title}</div>
            <div className={styles.list}>
                {
                    list.map((item, i) => <Service key={`service-${i}`} {...item} />)
                }
            </div>
        </div>
    )
}

export default Services