/*
 *  商家咨询信息
 */

import styles from './procedure.scss'

//默认属性
const defaultProps = {
    status:"1",//订单状态state
    isPannelShow:true,
    descriptions:[],
    title:"商家已报价",
    orderAmount:"10900",
    timer:{},
	beforeOrderStatus:"-1",
	delay:()=>{},//延迟验收
	surplusDay:"7",//延迟验收天数
	isDelay:"1",//是否可以延迟验收 1 可以 ； 0 不可以
	check:()=>{this.check()},//确认验收
	checkViolation:()=>{},
}

const Pannel = props =>{
    // props = defaultProps
    let text = "";
    let { status , title , descriptions , orderAmount , timer , beforeOrderStatus } = props;
    let iconClass = "";
    let pannelClass = "";
    // console.log(status)
    let condition = (status=="3" || status=="4")?(status+""+beforeOrderStatus):status;
    console.log(condition)
    switch(condition*1){
        case 0:
            text =`商家报价中...`;
            iconClass = styles.IconWaiting;
            pannelClass = styles.pannel+" "+styles.merchantStep;
            break;
        case 1:
            text =`${title}，总费用:${orderAmount}元(含服务费和罚金)`;
            iconClass = styles.IconPriced;
            pannelClass = styles.pannel+" "+styles.merchantStep;
            break;
        case 2:
            text =`商家正在处理中（2-7个工作日）...`;
            iconClass = styles.IconWaiting;
            pannelClass = styles.pannel+" "+styles.orderDeal;
            break;
        case 71:
            text =`您的违章已处理完！`;
            iconClass = styles.IconOk;
            pannelClass = styles.pannel+" "+styles.orderOk;
            break;
        case 8:
            text =`您的违章处理已确认完毕，订单完结！`;
            iconClass = styles.IconOk;
            pannelClass = styles.pannel+" "+styles.orderOk;
            break;
        case 30:
            text =`商家未在规定时间内报价，您的订单已取消！`;
            iconClass = styles.IconCancel;
            pannelClass = styles.pannel+" "+styles.orderCancel0;
            break;
        case 31:
            text =`未在规定时间内完成支付，您的订单已取消！`;
            iconClass = styles.IconCancel;
            pannelClass = styles.pannel+" "+styles.orderCancel1;
            break;
        case 42:
        case 32:
            text =`您的订单已取消！`;
            iconClass = styles.IconCancel;
            pannelClass = styles.pannel+" "+styles.orderCancel2;
            break;
        case 471:
        case 48:
            text =`您的订单已取消！`;
            iconClass = styles.IconCancel;
            pannelClass = styles.pannel+" "+styles.orderCancel7;
            break;
    }
    return (
        <div className={pannelClass}>
            <div className={styles.triangle}></div>
            <div className={styles.head}>
              <i className={iconClass}></i>
              <span>{text}</span> 
              { status == "0" && timer &&
                <span className={styles.time}>
                    <span className={styles.label}>报价剩余时间：</span>
                    {timer.d}天{timer.h}小时{timer.m}分{timer.s}秒
                </span>
              }
            </div>
            { (status == "1" || status=="71") && <div className={styles.divider}></div>}
            { (status == "1" || status=="71") && 
                <div className={styles.body}>
                {props.descriptions && props.descriptions.map((item,i)=>{
                    if(item.trim()){
                        return <div className={styles.item} key={i}>
                            <i></i>      
                            <div>{item}</div>
                        </div>
                    }
                })}
                {
                    status=="71" && <div>
                        <div className={styles.notice}>您的费用将在{props.surplusDay}天后自动支付给商家。如有异议，请联系商家客服，或是车行易客服。</div>
                        <div className={styles.btnGroup}>
                            <div className={styles.btn+" "+styles.btnActive} onClick={()=>props.check()}>确认验收</div>
                            <div className={props.isDelay=="1"?styles.btn:(styles.btn+" "+styles.btnDisabled)} onClick={props.isDelay=="1"?()=>props.delay():()=>{}}>延迟验收</div>
                            <div className={styles.btn} onClick={()=>props.checkViolation()}>检查违章情况</div>
                        </div>
                    </div>
                }
                </div>
            }
        </div>
    )
}

const Procedure = props => {
    // props = defaultProps
    console.log("++++",props)
    let { status , beforeOrderStatus } = props

    //订单流程图片列表
    let imgKey = (status=="3" || status=="4")?(status+""+beforeOrderStatus):status;
    let imgList = {
        "-1":"procedure-consult.png",//询价
        "0":"procedure-quote.png",//商家报价中
        "1":"procedure-quote.png",//报价完成
        "2":"procedure-treated.png",//支付完成
        "71":"procedure-complete.png",//已消费
        "8":"procedure-complete.png",//已完成
        "30":"procedure-cancel0.png",//取消（报价超时）
        "31":"procedure-cancel1.png",//取消（支付超时）
        "32":"procedure-cancel2.png",//取消（支付完成取消）
        "42":"procedure-cancel2.png",//取消（支付完成取消）
        "471":"procedure-cancel7.png",//取消（处理中取消）
        "48":"procedure-cancel7.png",//取消（处理完取消）
        "pay":"procedure-pay.png",//支付
    }

    return (
        <div className={styles.wrap}>
            <div className={styles.topIcon}>
                <img src={"./images/"+imgList[imgKey]}/>
            </div>
            { status!=-1 && status!="pay" && <Pannel {...props}/> }
        </div>
    )
}

export default Procedure