/*
 *  选择违章
 */

import styles from './violationBrief.scss'

//默认属性
const defaultProps = {
    onClick: () => console.log("触发点击事件"),
    title: '请选择需要办理的违章',
    detail: '违章代办省心、省时、省力'
}

const ViolationBrief = props => {
    props = Object.assign({}, defaultProps, props)
    let { onClick, title, detail } = props
    return (
        <div className={styles.wrap} onClick={() => onClick(props)}>
            <div className={styles.leftContent + " abs-v-center"}>
                <div className={styles.title + " text-overflow-1"}>{title}</div>
                <div className={styles.detail + " text-overflow-1"}>{detail}</div>
            </div>
            <div className={styles.rightIcon + " abs-v-center"}></div>
        </div>
    )
}

export default ViolationBrief