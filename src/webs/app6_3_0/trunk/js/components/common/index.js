export { default as ServiceNotice } from './serviceNotice'
export { default as ServiceProcess } from './serviceProcess'
export { default as ServiceList } from './serviceList'
export { default as Consult } from './consult'
export { default as ConsultViolation } from './consultViolation'
export { default as ViolationBrief } from './violationBrief'
export { default as Procedure } from './procedure'
export { default as Banner } from './Banner'
export { default as FullBtn } from './fullBtn'
export { default as TopBar } from './TopBar'
export { default as ShopName } from './shopName' //店铺名称 - 电话和营业时间
export { default as ShopAddress } from './shopAddress' //店铺地址
export { default as ShopNameAndAddress } from './shopNameAndAddress' //店铺名称和店铺地址
export { default as Service } from './service' //商家服务
export { default as Services } from './services' //商家服务列表