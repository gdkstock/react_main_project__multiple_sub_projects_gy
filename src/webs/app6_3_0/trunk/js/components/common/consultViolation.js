/*
 *  商家咨询信息
 */

import styles from './consultViolation.scss'

//默认属性
const defaultProps = {
    name:"sdfsdf了深刻的房间里说的克己复礼路上看到房价了深刻的",
    shopName:'商家打发打发打发',
    consult: p => console.log("点击了咨询按钮", p),
    phone: p => console.log("点击了拨打电话按钮", p),
    phoneNumber:"123123123131",
}

const ConsultViolation = props => {
    // props = defaultProps
    return (
        <div className={styles.wrap}>
            <div className={styles.content + " abs-v-center"}>
                <p className={styles.title + " text-overflow-1"}>{props.name}</p>
                <p className={styles.shopName + " text-overflow-1"}>{props.shopName}</p>
            </div>
            <div className={styles.consult + " abs-v-center"} onClick={() => props.consult(props)}></div>
            <a className={styles.phone + " abs-v-center"} onClick={() => props.phone(props)}></a>
        </div>
    )
}

export default ConsultViolation