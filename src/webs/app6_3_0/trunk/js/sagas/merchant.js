/**
 * 商家相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_SERVICE_DETAIL,
    GET_SHOP_LIST,
    GET_SHOP_DETAIL,
    GET_SHOP_NUM,
    ADD_SERVICE_DETAIL,
    GET_SERVICE_LIST,
    ADD_SHOP_NUM,
    ADD_SHOP_LIST,
    ADD_SHOP_DETAIL,
    GET_CITY_LIST,
    GET_TYPE_LIST,
} from '../actions/actionsTypes'

//service
import MerchantService from '../services/merchantSerivce'

import { ChMessage } from '../utils/message.config' //提示信息

import { Toast } from 'antd-mobile' //UI

import { normalize, schema } from 'normalizr'; //范式化库


//获取店铺详情
function* getShopDetail(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(MerchantService.getShopDetail, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            console.log(result)
            if (action.callback) {
                action.callback(result)
            }
        }
    } catch (error) {
        Toast.hide()
        window.networkError('./images/networkError-icon.png');
    }
}

function* watchGetShopDetail() {
    yield takeLatest(GET_SHOP_DETAIL, getShopDetail)
}

/***************获取商家服务详情***************/
    function* watchGetServiceDetail() {
        yield takeLatest(GET_SERVICE_DETAIL, getServiceDetail)
    }

    function* getServiceDetail(action) {
        try {
            let { result, timeout } = yield race({
                result: call(MerchantService.getDetail, action.param),
                timeout: call(delay, 30000)
            })
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == "1000") {
                    yield put({type:ADD_SERVICE_DETAIL,data:result.data})
                }
            }
        } catch (error) {
            if (action.callback) {
                action.callback(error)
            }
        }
    }

/***************获取商家数量***************/
    function* watchGetShopNum() {
        yield takeLatest(GET_SHOP_NUM, getShopNum)
    }

    function* getShopNum(action) {
        try {
            let { result, timeout } = yield race({
                result: call(MerchantService.getShopNum, action.param),
                timeout: call(delay, 30000)
            })
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == "1000") {
                    console.log("商家数量", result.data)
                    yield put({type:ADD_SHOP_NUM,count:result.data.count})
                }else{
                    yield put({type:ADD_SHOP_NUM,count:0})
                }
            }
        } catch (error) {
            window.networkError('./images/networkError-icon.png');
        }
    }

/***************获取店铺列表***************/
    function* watchGetShopList() {
        yield takeLatest(GET_SHOP_LIST, getShopList)
    }

    function* getShopList(action) {
        let { pageNo } = action.param
        if(pageNo=="1"){
            Toast.loading("",0);
        }
        try {
            let { result, timeout } = yield race({
                result: call(MerchantService.getShopList, action.param),
                timeout: call(delay, 30000)
            })
            Toast.hide()
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == "1000") {
                    // console.log("商家列表", result.data)
                    yield put({type:ADD_SHOP_LIST,data:{...result.data,pageNo}})
                    if(action.callback){
                        action.callback({...result,pageNo});
                    }
                }
            }
        } catch (error) {
            Toast.hide()
            window.networkError('./images/networkError-icon.png');
        }
    }


/***************获取店铺服务列表***************/
    function* watchGetServiceList() {
        yield takeLatest(GET_SERVICE_LIST, getServiceList)
    }

    function* getServiceList(action) {
        try {
            let { result, timeout } = yield race({
                result: call(MerchantService.getServiceList, action.param),
                timeout: call(delay, 30000)
            })
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == "1000") {
                    console.log("服务列表", action.callback)
                     if (action.callback) {
                        action.callback(result)
                    }
                }
            }
        } catch (error) {
            window.networkError('./images/networkError-icon.png');
        }
    }

/***************获取店铺详情***************/
    function* watchGetShopDetail() {
        yield takeLatest(GET_SHOP_DETAIL, getShopDetail)
    }

    function* getShopDetail(action) {
        Toast.loading("",0)
        try {
            let { result, timeout } = yield race({
                result: call(MerchantService.getShopDetail, action.param),
                timeout: call(delay, 30000)
            })
            Toast.hide();
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == "1000") {
                    console.log("店铺详情", result.data)
                    yield put({type:ADD_SHOP_DETAIL,data:result.data})
                }
            }
        } catch (error) {
            Toast.hide();
            window.networkError('./images/networkError-icon.png');
        }
    }

/***************获取城市列表***************/
    function* watchGetCityList() {
        yield takeLatest(GET_CITY_LIST, getCityList)
    }

    function* getCityList(action) {
        try {
            if(!sessionStorage.getItem("cityList")){
                let { result, timeout } = yield race({
                    result: call(MerchantService.getCityList, action.param),
                    timeout: call(delay, 30000)
                })
                if (timeout) {
                    window.networkError('./images/networkError-icon.png');
                } else {
                    if (result.code == "1000") {
                        sessionStorage.setItem("cityList",JSON.stringify(result))
                        if (action.callback) {
                            action.callback(result)
                        }
                    }
                }
            }else{
                let result = JSON.parse(sessionStorage.getItem("cityList"))
                if (action.callback) {
                    action.callback(result)
                }
            }
        } catch (error) {
            window.networkError('./images/networkError-icon.png');
        }
    }

/***************获取类型列表***************/
    function* watchGetTypeList() {
        yield takeLatest(GET_TYPE_LIST, getTypeLsit)
    }

    function* getTypeLsit(action) {
        try {
            if(!sessionStorage.getItem("typeList")){
                let { result, timeout } = yield race({
                    result: call(MerchantService.getTypeLsit, action.param),
                    timeout: call(delay, 30000)
                })
                if (timeout) {
                    window.networkError('./images/networkError-icon.png');
                } else {
                    if (result.code == "1000") {
                        sessionStorage.setItem("typeList",JSON.stringify(result));
                        if (action.callback) {
                            action.callback(result)
                        }
                    }
                }
            }else{
                let result = JSON.parse(sessionStorage.getItem("typeList")) 
                 if (action.callback) {
                    action.callback(result)
                }
            }
        } catch (error) {
            window.networkError('./images/networkError-icon.png');
        }
    }

export function* watchMerchantFetch() {
    yield [
        fork(watchGetServiceDetail),
        fork(watchGetShopDetail),
        fork(watchGetShopNum),
        fork(watchGetShopList),
        fork(watchGetServiceList),
        fork(watchGetCityList),
        fork(watchGetTypeList),
    ]
}