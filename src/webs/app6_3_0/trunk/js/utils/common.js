/**
 * PS：此类只支持纯方法
 */
import config from '../config'
class Common {

    /**
     * 获取当前网址的根路径 返回的数据为：http.../#/
     */
    getRootUrl() {
        let this_url = window.location.href; //当前网址
        let substrNum = this_url.indexOf('#') + 1; //获取到哈希路由当前的位置 + 1
        let url = this_url.substr(0, substrNum) + '/';
        return url;
    }

    /**
     * 将键值对转为URL参数
     */
    _toQueryPair(key, value) {
        ///<summary>将键值对转为URL参数</summary>
        if (typeof value == 'undefined') {
            return key;
        }
        return key + '=' + encodeURIComponent(value === null ? '' : String(value));
        //return key + '=' + (value == null ? '' : String(value));
    }

    /**
     * 将对象转为URL参数
     */
    toQueryString(obj) {
        var ret = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                key = encodeURIComponent(key);
                var values = obj[key];
                if (values && values.constructor === Array) { //数组 
                    var queryValues = [];
                    for (var i = 0, len = values.length, value; i < len; i++) {
                        value = values[i];
                        queryValues.push(this._toQueryPair(key + '[' + i + ']', value));
                    }
                    ret = ret.concat(queryValues);
                } else { //字符串 
                    ret.push(this._toQueryPair(key, values));
                }
            }
        }
        return ret.join('&');
    }

    /**
     * 设置浏览器标题 兼容IOS 后退title不修改的bug
     */
    setViewTitle(title) {
        let body = document.getElementsByTagName('body')[0];
        document.title = title;
        if (navigator.userAgent.indexOf("AlipayClient") !== -1) {
            AlipayJSBridge.call("setTitle", {
                title: title
            });
        }
        try {
            let iframe = document.createElement("iframe");
            iframe.setAttribute("src", "./favicon.ico");
            iframe.style.display = 'none';
            iframe.addEventListener('load', function () {
                setTimeout(() => {
                    document.body.removeChild(iframe);
                }, 10);
            });
            document.body.appendChild(iframe);
        } catch (error) {
            console.log("setViewTitle错误：", error)
        }
    }

    /**
     * 检测是否为车行易app
     */
    isCXYApp() {
        let isCXYApp = navigator.userAgent.indexOf('appname_cxycwz') > -1 || navigator.userAgent.indexOf('appname_cx580merchant') > -1? true : false;
        return isCXYApp;
    }

    /**
     * 获取安卓系统的版本号 非安卓手机则返回false
     */
    getAndroidVersion() {
        let re = /Android\s([^;]+)/ig;
        let _version = re.exec(navigator.userAgent);
        if (_version) {
            _version = _version[1];
        } else {
            _version = false;
        }
        return _version;
    }

    browser = function () {
        let u = navigator.userAgent;
        //app = navigator.appVersion;
        return {
            versions: { //移动终端浏览器版本信息 
                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端 
                iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器 
                iPad: u.indexOf('iPad') > -1, //是否iPad 
                android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1 //android终端或uc浏览器 
            }
        }
    }

    /**
     * 获取APP的版本号
     */
    getAppVersion = function () {
        let u = navigator.userAgent;
        if (u.indexOf('appname_cxycwz/') > -1) {
            return u.substr(u.indexOf('appname_cxycwz/') + 15);
        }
        return false;
    }

    /**
     * 检测当前浏览器是否为iPhone(Safari)
     */
    isIPhone = function () {
        let browser = this.browser();
        if (browser.versions.iPhone || browser.versions.iPad || browser.versions.ios) {
            return true;
        }
        return false;
    };

    /**
     * 检测当前浏览器是否为Android(Chrome)
     */
    isAndroid = function () {
        let browser = this.browser();
        if (browser.versions.android) {
            return true;
        }
        return false;
    };

    /**
     * 获取JsApi的userType
     */
    getJsApiUserType() {
        var userType = '';
        var ua = navigator.userAgent;
        if (ua.toLowerCase().match(/MicroMessenger/i) == "micromessenger") {
            userType = 'weixin';
        } else if (ua.indexOf("appname_cxycwz") !== -1) {
            userType = 'app';
        } else if (ua.indexOf("AlipayClient") !== -1) {
            userType = 'alipay';
        } else if (ua.indexOf("QQ") !== -1) {
            userType = 'qq';
        }
        return userType;
    }

    /**
     * 获取URL上的keyValue
     * @param {*string} key
     */
    getUrlKeyValue(key) {
      if (!key) {
        return key;
      }
      var keyName = key; //保存keyName
      var search = window.location.href; //由于hash值的原因 这里不用window.location.search用window.locationhref
      key = new RegExp('[\?|&]' + key + '=([^&]+)', 'g');
      key = key.exec(search);
      key = key ? key[1] : "";
      sessionStorage.setItem(keyName, key); //key
      return key.split("#")[0] //避免返回的数据带有hash值
    }

    /**
     * 后退时跳转到指定的URL，并且在指定URL后退时关闭webView 仅支持微信和支付宝
     */
    backToUrlAndCloseWebView(url = '') {
        var userType = this.getJsApiUserType();

        if (userType == 'weixin' || userType == 'alipay') {
            history.replaceState({ toUrl: url }, "", "");
            history.pushState({}, "", "");
            history.forward(); //前进
        }
    }

    /**
     * 当前地址和URL中的地址不相同时，关闭webView
     */
    noCloseWebViewUrl(url = '') {
        var userType = this.getJsApiUserType();

        if (userType == 'weixin' || userType == 'alipay') {
            sessionStorage.setItem("noCloseWebViewUrl", window.location.href); //后退到订单确认页面时关闭webView
        }
    }

    /**
     * 后退时显示确认对话框
     * @param {*string} text 
     * @param {*function} callback 
     */
    backShowConfirm(callback) {
        var userType = this.getJsApiUserType();
        if (userType == 'weixin' || userType == 'alipay' || userType == "app") {
            if (callback) {
                window['stateConfirmCallback'] = callback
                history.replaceState({ confirm: 1 }, "", "");
                history.pushState({}, "", "");
                history.forward(); //前进
            }
        }
    }

    /**
     * 支付宝JSDK加载完成
     */
    alipayReady(callback) {
        var userType = this.getJsApiUserType();
        if (userType == 'alipay') {
            if (window.AlipayJSBridge) {
                callback && callback();
            } else {
                // 如果没有注入则监听注入的事件
                document.addEventListener('AlipayJSBridgeReady', callback, false);
            }
        }
    }

    /**
     * 提交埋点
     * @param {*Object} data json格式
     */
    sendCxytj(data) {
        try {
            window.cxytj.init({ //以下为初始化示例，可新增或删减字段
                productId: 'bangmangban', //产品ID
                productVersion: '1.0', //产品版本
                channel:"app", //推广渠道
                isProduction: config.production, //生产环境or测试环境 默认测试环境
                userId: sessionStorage.getItem('userId'), //用户ID
            });
            window.cxytj.recordUserBehavior(Object.assign({}, {
                eventType: '2', //1.时长 2.点击数
                productUserId: sessionStorage.getItem("productUserId"), //APP端 （设备标记ID） //暂时不在APP端上线
                eventTime: window.cxytj.getNowFormatDate(),  //触发时间
                curPageInfo: document.title,
                prevPageInfo: sessionStorage.getItem("prevPageInfo") //上一页信息
            }, data));
        } catch (error) {
            console.error(error)
        }
    }

    /**
     *  生成二唯码
     *  @param {*Object} data json格式
     */
    createQRCode(str){
        str=this.utf16to8(str);
        $("#code").html("").qrcode({
            render: "canvas", //table方式 
            width:110, //宽度 
            height:110, //高度 
            text: str ,//任意内容 
            // correctLevel:1,//容错率 L , M , Q , H
        })
    }

    utf16to8(str) {  
        var out, i, len, c;  
        out = "";  
        len = str.length;  
        for(i = 0; i < len; i++) {  
        c = str.charCodeAt(i);  
        if ((c >= 0x0001) && (c <= 0x007F)) {  
            out += str.charAt(i);  
        } else if (c > 0x07FF) {  
            out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));  
            out += String.fromCharCode(0x80 | ((c >>  6) & 0x3F));  
            out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));  
        } else {  
            out += String.fromCharCode(0xC0 | ((c >>  6) & 0x1F));  
            out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));  
        }  
        }  
        return out;  
    } 

    ready(callback){
      if(callback){
        if (window.cxyJSBridgeIsReady) {
          //JSDK 以及初始化完毕
          callback();
        } else {
          //JSDK还未初始化完毕
          window.cxyJSBridgeReady = callback();
        }
      }
    }

    //判断是否返回
    isBack(){
        let isChangeUser = sessionStorage.getItem("isChangeUser")?true:false//是否切换用户
        let indexHash = sessionStorage.getItem("indexHash")//首页hash
        let curHash = window.location.hash //当前hash
        if(isChangeUser){
            if(curHash!=indexHash){
                return true
            }else{
                // sessionStorage.removeItem("isChangeUser")
                return false
            }
        }else{
            return false
        }
    }
};

// 实例化后再导出
export default new Common()