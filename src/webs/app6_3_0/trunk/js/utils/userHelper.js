import jsApi from './cx580.jsApi';
import common from './common';

let g_userContainer = undefined;

let g_userId = ''; //CXY_3FBA032214714C19BA4CCA267F86C550
let g_userToken = ''; //0850B086D355F263654698F097C0E1C8

let g_city = "";


/**
 * 用户帮助类
 */
class UserHelper {
    //userContainer = "";
    // userId = "";
    // userToken = "";
    //city = "";


    constructor() {
        this._initialize();
    }

    _initialize() {
        // setTimeout(() => {
        //     this._getContainer();
        // }, 500);
        if (common.isCXYApp()) {
            //执行到这里，说明是在 app 中运行
            g_userContainer = "App";
            this._getUserInfoFromApp();
            // setTimeout(() => {
            //     this._getUserInfoFromApp();
            // }, 150);
        } else {
            //执行到这里，说明不在 app 中运行
            g_userContainer = "";
            //alert("请使用车行易APP访问！");
        }
    }

    _getContainer() {
        try {
            jsApi.call({
                "commandId": "",
                "command": "netstat"
            }, (data) => {
                //执行到这里，说明是在 app 中运行
                g_userContainer = "App";
                this._getUserInfoFromApp();
            });
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            g_userContainer = "";
        }
    }


    /**
     * 从 app 中获取用户 token
     */
    _getUserInfoFromApp() {
        try {
            jsApi.call({
                "commandId": "",
                "command": "getSymbol",
                "data": {
                    "userid": "",
                    "lng": "",
                    "version": "",
                    "channel": "",
                    "cars": "",
                    "phone": "",
                    "name": "",
                    "type|orderNum": "",
                    "city": "",
                    "accountId": "",
                    "token": "",
                    "carId": "",
                    "carNumber": ""
                }
            }, (data) => {
                g_userId = data.data.accountId;
                g_userToken = data.data.token;
                g_city = data.data.city;
            });
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            // alert("调试信息：调用APP JS SDK出错了 获取APP信息出错了");
        }
    }

    /**
     * app 登陆
     */
    _appLogin(callback) {
        jsApi.call({
            "commandId": "",
            "command": "login"
        }, (data) => {
            localStorage.setItem("upLoginState", "1"); //用户登录状态发生变化
            let userId = sessionStorage.getItem("userId")
            if ( data.data.accountId ) {
                sessionStorage.setItem("isFromLogin","1")
                if(data.data.accountId!=userId){
                    sessionStorage.setItem("isChangeUser","1")
                }
                g_userId = data.data.accountId;
                this._getUserInfoFromApp();
                if (callback) {
                    callback();
                }
            } else {
                //登录失败
                // alert("22222"+new Date().getTime())
                setTimeout(()=>{this.closeAppView()},100)
                // this.closeAppView(); //关闭APP视图
            }
        });
    }

    /**
     * 获取 userId 和 token
     */
    getUserIdAndToken() {
        if (g_userContainer == "App") {
            this._getUserInfoFromApp();
        }
        return {
            userId: g_userId,
            token: g_userToken,
            userType:'App',
            authType:'App',
            city: g_city
        }
    }

    /**
     * 登陆
     * @param callback function 登陆成功之后的回调
     */
    Login(callback) {
        if (g_userContainer == "App") {
            this._appLogin(callback)
        } else {
            alert("Login no app")
        }
    }

    /*
     * 调用IM
     */
    openIM(merchantMsg){
        let { merchantUserId, merchantUserName, merchantUserImage , topic} = merchantMsg
        // alert(JSON.stringify(merchantMsg))
        try{
            jsApi.call({
                "commandId": "",
                "command": "openIM",
                "data": {
                    "userName": merchantUserName,
                    "userId": merchantUserId,
                    "userHeadImage": merchantUserImage,
                    "topic":topic,
                }
            }, function (data) {
                // alert(JSON.stringify(data))
            });
        }catch(e){
            alert("openIM出错了")
        }
    }

    /*
     * 订单支付
     */
    orderPay(params,callback){
        let { orderId , orderType , orderAmt , payType , token } = params
        // alert("params"+JSON.stringify(params))
        try{
            jsApi.call({
                "commandId":"",
                "command":"payOrder",
                "data":{
                    orderId,
                    orderType,
                    orderAmt,
                    payType,
                    token,
                }
            },function(data){
                // alert(JSON.stringify(data))
                callback(data)
            });
        }catch(e){
            
        }
    }

    /**
     * 隐藏标题
     */
    controlTitle(status){
        let preStatus = sessionStorage.getItem("preStatus")
        if(common.isIPhone() || preStatus!=status){
            common.ready(()=>{
                try{
                    jsApi.call({
                        "commandId":"",
                        "command":"controlTitleLayout",
                        "data":{
                            "showStatus":status,
                            "titleBarBgColor":"#ffffff",
                            "statusBarBgColor":"#00ffffff",
                            "reset":status=="show"?"true":"false"
                        }
                    },function(data){
                        sessionStorage.setItem("preStatus",status)
                    });
                }catch(e){
                    // alert("controlTitleLayout出错了")
                }
            })
        }
        if(status=="show"){
            this.hideRightBtn()
        }
    }

    /**
     * 检查违章情况
     */
    checkViolation(msg){
        let { carId , carNumber } = msg
        common.ready(()=>{
            try{
                jsApi.call({
                    "commandId":"",
                    "command":"jump",
                    "data": {
                        "jumpData": {
                            "target": "violationList",
                            "carId":carId,
                            "carNumber": carNumber,
                            "tabName": "违章"
                        }, //封装需要跳转的数据，地址、参数等
                        "closeH5WebView": "false"
                    }
                },function(data){
                });
            }catch(e){
                // alert("444")
            }
        })
    }

    /**
     * 打开新view
     */
    openNewBrowser(url){
        jsApi.call({
            "commandId":"",
            "command":"openNewBrowserWithURL",
            "data":{
            "url":url,
            "umengId":"cfw_youkachongzhi",
            "showTitleLayout":"true",
            "showLoading":"false"
        }
        },function(data){})
    }

     /**
     * 关闭view
     * @param callback function 登陆成功之后的回调
     */
    closeAppView(){
        jsApi.call(
            {
                "commandId":"",
                "command":"close",
            },function(data){}
        );
    }

    /**
     * 隐藏右侧按钮
     */
    hideRightBtn(){
        jsApi.call({
            "commandId":"",
            "command":"customizeTopRightButton",
            "data":{
                "reset":"false",
                "list":[
                        // {
                        //     "iconUrl": "http: //www.iconpng.com/png/elegant_font/icon_close_alt2.png",
                        //     "text": "关闭",
                        //     "callbackMethodName": "close"
                        // },
                        // {
                        //     "iconUrl": "http: //www.iconpng.com/png/computer-and-media-1/home82.png",
                        //     "text": "刷新",
                        //     "callbackMethodName": "fresh"
                        // }
                ]
            }
            },function(data){});
}


};

// 实例化后再导出
export default new UserHelper()