/**
 * 帮忙办业务代办发起询价
 * Created by lijun on 2017/7/10.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Toast } from 'antd-mobile'

//常用工具类
import common from '../../utils/common'
import config from '../../config'
import userHelper from '../../utils/userHelper'

//基础组件
import { Consult, ViolationBrief, ServiceNotice, Procedure , FullBtn} from '../../components/common'
import FootBar from './footBar'

//server
import orderService from '../../services/orderService'

//actions
import * as merchantActions from '../../actions/merchantActions'
import * as orderActions from '../../actions/orderActions'
import * as carActions from '../../actions/carActions'


class ViolationConsult extends Component {
	constructor(props) {
		super(props);
		//设置初始状态
		this.state = {
			shopDetail: {
				merchantId:this.props.params.merchantId,
				productId:this.props.params.productId,
			}, //商家详情，接口返回的数据
			violation: {
				carNumber: '', //车牌
				checkedNum: 0, //勾选违章总数
				checkedDegree: 0, //勾选总扣分
				checkedFine: 0, //勾选总罚款
				checkeds: '' //违章ID字符串，多个用;隔开
			}
		}
	}

	componentWillMount() {
		//设置标题
		common.setViewTitle('发起询价');

		//判断是否需要返回
        if(common.isBack()){
            window.history.go(-1)
        }else{
			this.getData()
			//在app的时候隐藏头部标题
			if(common.isCXYApp()){
				userHelper.controlTitle("show")
			}
		}

		window.violationConsultResume = ()=>{
			if(sessionStorage.getItem("isFromLogin")){
				sessionStorage.removeItem("isFromLogin")
				if(common.isBack()){
					window.history.back()
				}else{
					this.getData()
				}
			}
		}

	}

	getData(){
		let { merchantId , shopId , productId} = this.props.params;
		let { shops , serviceList , cars} = this.props;

		if (sessionStorage.getItem('checkedViolations')) {
			//存在已勾选的违章
			let violation = JSON.parse(sessionStorage.getItem('checkedViolations'));
			// sessionStorage.removeItem('checkedViolations') //删除缓存
			this.setState({
				violation: violation
			})
		}else if(cars.result.length>0){
			let carId = cars.result[0];
			let car = cars.data[carId];
			let carNumber = car.carNumber;
			let checkedNum = car.count;
			let checkedDegree = car.degree;
			let checkedFine = car.money;
			let violations = car.violations || [];
			let checkeds = "";
			if(violations.length>0){
				checkeds = violations.map(item=>item.violationId).join(";");
			}
			let violation = {
				carId,
				carNumber, //车牌
				totalNum:checkedNum,
				totalDegree:checkedDegree,
				totalFine:checkedFine,
				checkedNum, //勾选违章总数
				checkedDegree, //勾选总扣分
				checkedFine, //勾选总罚款
				checkeds, //违章ID字符串，多个用;隔开
			}
			this.setState({
				violation,
			})	
			sessionStorage.setItem("checkedViolations",JSON.stringify(violation));
		}else{
			this.props.carActions.getCarList()
		}

		//获取商家信息
		if(!shops.data[shopId]){
			this.props.merchantActions.getShopDetail({shopId})
		}else{
			this.setState({
				shopDetail:{...this.state.shopDetail,...shops.data[shopId]},
			})
		}

		//获取服务信息
		if(!serviceList.data[productId]){
			this.props.merchantActions.getServiceDetail({productId})
		}else{
			this.setState({
				serviceDetail:serviceList.data[productId]
			})
		}
	}

	componentWillReceiveProps(nextProps) {
		//组件props更新
		let { merchantId , shopId ,productId} = this.props.params;
		let { shops , serviceList , cars} = nextProps;
		let shopDetail = shops.data[shopId]?{...this.state.shopDetail,...shops.data[shopId]}:this.state.shopDetail;
		let serviceDetail = serviceList.data[productId]?{...this.state.serviceDetail,...serviceList.data[productId]}:this.state.serviceDetail;
		let violation = this.state.violation;
		if(cars.result.length>0){
			// alert(JSON.stringify(cars))
			let carId = cars.result[0];
			let car = cars.data[carId];
			let carNumber = car.carNumber;
			let checkedNum = car.count;
			let checkedDegree = car.degree;
			let checkedFine = car.money;
			let violations = car.violations || [];
			let checkeds = "";
			if(violations.length>0){
				checkeds = violations.map(item=>item.violationId).join(";");
			}
			violation = {
				...violation,
				carId,
				carNumber, //车牌
				totalNum:checkedNum,
				totalDegree:checkedDegree,
				totalFine:checkedFine,
				checkedNum, //勾选违章总数
				checkedDegree, //勾选总扣分
				checkedFine, //勾选总罚款
				checkeds, //违章ID字符串，多个用;隔开
			}
			sessionStorage.setItem("checkedViolations",JSON.stringify(violation));
		}
		this.setState({
			shopDetail,
			serviceDetail,
			violation,
		})
	}

	componentDidMount() {
		//进入组件埋点
		common.sendCxytj({
			eventId:"cxyapp_p_nearby_askprice"
		})

	}

	componentWillUnmount() {
		//离开记录页面信息
		sessionStorage.setItem("prevPageInfo", document.title)
		window.violationConsultResume = null;
	}

	/**
	 * 调用IM
	 */
	openIM() {
		let { shopDetail } = this.state,
			merchantMsg={};
		if(shopDetail){
			let user = shopDetail.users[0]
			merchantMsg = {
				merchantUserId : user.userId, 
				merchantUserName : user.name, 
				merchantUserImage : user.photo, 
				topic:"wzbl",
			}
			userHelper.openIM(merchantMsg)
		}
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_askprice_chat"
		})
	}

	/**
	 * 点击询价按钮
	 */
	handleFooterBtn() {
		let { violation } = this.state
		if(violation.carNumber){
			Toast.loading('', 0)

			let { shopDetail, violation } = this.state
			let { shopId, merchantId } = shopDetail
			let { carId, carNumber, checkeds } = violation

			console.log("11111",this.state)

			//下单请求参数
			let params = {
				shopId,
				violations: checkeds,
				typeId: '210',
				merchantId,
				carId,
				carNumber,
				productId:this.props.params.productId,
			}

			//下单
			this.props.orderActions.createOrder(params,(result)=>{
				sessionStorage.removeItem('checkedViolations')
				let orderId = result.data.orderId
				window.location.replace(common.getRootUrl()+'violation/orderDetail/'+orderId)
			})
		}else{
			Toast.info("请选择需要办理的违章",2)
		}
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_askprice_askprice"
		})
	}

	telephoneCall(telephone){
		telephone ? window.location.href = `tel:${telephone}` : Toast.info('请选择其他联系方式', 1)
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_askprice_call"
		})
	}

	//选择违章
	jumptoViolationSelect(){
		window.location.href = common.getRootUrl() + 'violation/list'
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_askprice_choosewz"
		})
	}

	render() {
		console.log("_________",this.state)
		let { shopDetail, violation ,serviceDetail} = this.state
		let { attention, shopName, telephone } = shopDetail
		let ConsultProps = {
			shopName: shopName,
			consult: p => this.openIM(),
			phone: p => this.telephoneCall(telephone)
		}
		let ViolationBriefProps = {
			onClick: () => this.jumptoViolationSelect(),
			title: violation.checkedNum == 0 ? '请选择需要办理的违章' : `${violation.carNumber.substr(0, 2)} ${violation.carNumber.substr(2)}车辆违章代办服务`,
			detail: violation.checkedNum == 0 ? '违章代办省心、省时、省力' : `共办理${violation.checkedNum}条，扣${violation.checkedDegree}分，罚款${violation.checkedFine}元`
		}

		return (
			<div className="has-footBar">
				<Procedure 
					status = "-1"
				/>
				<Consult {...ConsultProps} />
				<div className='hr'></div>
				<ViolationBrief {...ViolationBriefProps} />
				<div className='whitespace-24'></div>
				<ServiceNotice
					title="注意事项"
					content={attention}
				/>
				<FullBtn
                    text="立即询价"
                    handleClick={() => this.handleFooterBtn()}
                    isDisabled = {false}
                    type="1"
                />
			</div>
		)
	}
}

const mapStateToProps = state => ({
	shops:state.shops,
	serviceList:state.serviceList,
	cars:state.cars,
})

const mapDispatchToProps = dispatch => ({
	merchantActions: bindActionCreators(merchantActions, dispatch) ,//容器组件action
	orderActions:bindActionCreators(orderActions, dispatch),
	carActions:bindActionCreators(carActions, dispatch),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ViolationConsult)