/**
 * 帮忙办业务养护类服务订单详情
 * Created by lijun on 2017/7/5.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as orderActions from '../../actions/orderActions'
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'
import config from '../../config'

//基础组件
import { ServiceNotice , ServiceList } from '../../components/common'
import { ServiceBrief , ServiceState , MerchantBrief , OrderMsg } from '../../components/carServiceOrderDetail'

// import { Toast } from 'antd-mobile'

class CarServiceOrderDetail extends Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('订单详情');

		//进入页面缓存hash路由
        if(!sessionStorage.getItem("indexHash")){
            let hash = window.location.hash
            sessionStorage.setItem("orderCenter_carService",hash) //用来判断切换用户登录时关闭webview
        }

		//获取订单详情数据
		if(common.isBack()){
			let hash = sessionStorage.getItem("orderCenter_carService")
			if(hash.indexOf("#/carService/orderDetail")>-1){
				userHelper.closeAppView()
			}
        }else{
			this.getOrderDetail()
		}

		//回到栈顶时触发
        window.carServiceOrderResume=()=>{
            if(sessionStorage.getItem("isFromLogin")){
				sessionStorage.removeItem("isFromLogin")
                if(common.isBack()){
					let hash = sessionStorage.getItem("orderCenter_carService")
					if(hash.indexOf("#/carService/orderDetail")>-1){
						userHelper.closeAppView()
					}
				}else{
					this.getOrderDetail()
				}
            }
        }

		//在app的时候隐藏头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("show")
        }

	}

	//获取订单详情数据
	getOrderDetail(){
		let { orderId } = this.props.params
		this.props.orderActions.getOrderDetail({orderId},(result)=>{
			this.setState({
				...this.state,
				...result.data,
			})
		})
	}

	componentWillReceiveProps(nextProps){
		//组件props更新
	}

	componentDidMount(){
		//进入组件埋点
		common.sendCxytj({
            eventId: "cxyapp_p_nearby_maintainserver_order",
        })

		//生成二维码
		if(this.state.qrcode){
			let qrcode={...this.state.qrcode};
			let QRCodeDate = {//二维码数据结构
				type:qrcode.type,
				data:{
					code:qrcode.data.couponCode,
					orderId:qrcode.data.orderId,
				}
			}
			let str = JSON.stringify(QRCodeDate)
			common.createQRCode(str)
		}
	}

	componentDidUpdate(){
		console.log(JSON.stringify(this.state.qrcode));

		//生成二维码
		if(this.state.qrcode){
			let qrcode={...this.state.qrcode};

			let QRCodeDate = {//二维码数据结构
				type:qrcode.type,
				data:{
					code:qrcode.data.couponCode,
					orderId:qrcode.data.orderId,
				}
			}

			let str = JSON.stringify(QRCodeDate)
			common.createQRCode(str)
		}
	}

	componentWillUnmount() {
		//离开车辆列表页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
		sessionStorage.setItem("merchantMsg",JSON.stringify(this.state.shop.users))
		window.carServiceOrderResume = null
    }

	//申请退款
	refund(params,callback){
		console.log(params)
		let { orderId , remark } = params
		this.props.orderActions.refund({
			orderId,
			remark,
		},(result)=>{
			callback()
			this.props.orderActions.getOrderDetail({orderId},(result)=>{
				this.setState({
					...this.state,
					...result.data,
				})
			})
		})
		common.sendCxytj({
			eventId:"cxyapp_e_maintainorder_drawback_submit"
		})
	}

	//跳转到服务详情
	jumptoDetail(){
		let { merchantId , shop , product } = this.state,
			shopId = shop.shopId,
			productId = product.productId;
		this.props.router.push(`carServiceDetail/${merchantId}/${shopId}/${productId}`);
		common.sendCxytj({
			eventId:"cxyapp_e_maintainorder_maintainsever"
		})
	}

	//点击再次购买按钮
	buyService(){
		let { merchantId , shop , product } = this.state
		if(product){
			let orderMsg = {
				name:product.name,
				productDesc:product.productDesc,
				price:product.price,
			}
			sessionStorage.setItem("carServiceOrder",JSON.stringify(orderMsg))
			let shopId = shop.shopId;
			let productId = product.productId;
			let typeId = "220"
			this.props.router.push("orderPay/"+merchantId+"/"+shopId+"/"+productId+"/"+typeId);
		}
		common.sendCxytj({
			eventId:"cxyapp_e_maintainorder_buyagain"
		})
	}

	//定位
	location(props){
		let { shopAddress , longitude , latitude } = props;
		if(common.isCXYApp()){
			let url = common.getRootUrl()+`map?address=${shopAddress}&longitude=${longitude}&latitude=${latitude}`
            userHelper.openNewBrowser(encodeURI(url))  
            // userHelper.openNewBrowser(common.getRootUrl()+`map?address=${shopAddress}&longitude=${longitude}&latitude=${latitude}`)  
        }else{
            this.props.router.push(`map?address=${shopAddress}&longitude=${longitude}&latitude=${latitude}`);
        }
		common.sendCxytj({
			eventId:"cxyapp_e_maintainorder_map"
		})
	}

	//咨询
	consult(){
		let { shop } = this.state,
			merchantMsg = {};
		if(shop){
			let user = shop.users[0]
			merchantMsg = {
				merchantUserId : user.userId, 
				merchantUserName : user.name, 
				merchantUserImage : user.photo, 
				topic:"qcyh",
			}
			userHelper.openIM(merchantMsg)
		}
		common.sendCxytj({
			eventId:"cxyapp_e_maintainorder_chat"
		})
	}

	//i图标点击事件
	promptIconClick(item){
		if(item.serviceDescUrl){
			if(common.isCXYApp()){
				userHelper.openNewBrowser(item.serviceDescUrl)
			}else{
				window.location.href=item.serviceDescUrl
			}
		}
		common.sendCxytj({
			eventId:"cxyapp_e_maintainorder_tips"
		})
	}

	telephoneCall(phone){
		window.location.href=`tel:${phone}`
		common.sendCxytj({
			eventId:"cxyapp_e_maintainorder_call"
		})
	}

	render(){
		console.log(this.state)
		let { product , service } = this.state
		let serviceStateProps={
			orderId:this.state.orderId,
			payStatus:this.state.payStatus,
			code:this.state.qrcode?this.state.qrcode.data.couponCode:"",
			stateCode:this.state.orderStatus,
			time:this.state.qrcode?this.state.qrcode.data.dueDate.trim().substr(0,10):"", //有效期
			fine:this.state.orderAmount, //订单金额
			applyRefundTime:this.state.applyRefundTime||"", //申请退款时间
			refundReason:this.state.refundReason||"",//退款原因
			canbuy:product!=undefined && product.status=="1",//服务是否下架
			refund:(p,c)=>{this.refund(p,c)},//申请退款
			buyService:()=>this.buyService(),//再次购买
		}
		let orderMsgProps={
			orderId:this.state.orderId,
			payTypeName:this.state.payTypeName,
			payNo:this.state.payNo,
			orderAmount:this.state.orderAmount,
			payTime:this.state.payTime,
		}
		let MerchantBriefProps ={
			...this.state.shop,
			location:(p)=>this.location(p),//地图
			consult:()=>this.consult(),//咨询
			phone:(p)=>this.telephoneCall(p),
		}
		let ServiceNoticeProps={
			title:"服务须知",
			content:this.state.productDescription!=undefined?this.state.productDescription.join("<br/>"):"",
		}
		return (
			<div className="scrollBox">
				{ product && product.status=="1" && <ServiceBrief {...product} handleClick={()=>this.jumptoDetail()}/>}
				{ product && product.status=="1" && <div className='whitespace-24'></div>}
				<ServiceState {...serviceStateProps}/>
				<div className='whitespace-24'></div>
				<ServiceNotice {...ServiceNoticeProps}/>
				{service && service.length>0 && <div className='whitespace-24'></div>}
				{service && service.length>0 && <ServiceList List={service} handleIconClick={(item)=>this.promptIconClick(item)}/>}
				<div className='whitespace-24'></div>
				<MerchantBrief {...MerchantBriefProps}/>
				<div className='whitespace-24'></div>
				<OrderMsg {...orderMsgProps}/>
				<div className='h56'></div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	//state:state //容器组件props
})

const mapDispatchToProps = dispatch => ({
	orderActions: bindActionCreators(orderActions, dispatch) //容器组件action
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CarServiceOrderDetail)