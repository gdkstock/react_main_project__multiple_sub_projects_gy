/**
 * 帮忙办业务代办订单流程-订单详情
 * Created by lijun on 2017/7/10.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as orderActions from '../../actions/orderActions'
import common from '../../utils/common'
import config from '../../config'
import userHelper from '../../utils/userHelper'

//基础组件
import { Consult , ViolationBrief , ServiceNotice , Procedure } from '../../components/common'
import FootBar from './footBar'

// import { Toast } from 'antd-mobile'
import moment from 'moment'

class ViolationOrder extends Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			orderStatus:"-1",
			violation: {
				carNumber: '', //车牌
				checkedNum: 0, //勾选违章总数
				checkedDegree: 0, //勾选总扣分
				checkedFine: 0, //勾选总罚款
				checkeds: '' //违章ID字符串，多个用;隔开
			}
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('订单详情');

		//进入页面如果是订单中心进入缓存hash路由
        if(!sessionStorage.getItem("indexHash")){
            let hash = window.location.hash
            sessionStorage.setItem("orderCenter_violation",hash) //用来判断切换用户登录时关闭webview
        }

		//判断是否需要返回
        if(common.isBack()){
			let hash = sessionStorage.getItem("orderCenter_violation")
			if(hash.indexOf("#/violation/orderDetail")>-1){
				userHelper.closeAppView()
			}
        }else{
			this.getOrderDetail();
		}

		//回到栈顶时触发
        window.violationOrderResume=()=>{
            if(sessionStorage.getItem("isFromLogin")){
				sessionStorage.removeItem("isFromLogin")
                 if(common.isBack()){
					let hash = sessionStorage.getItem("orderCenter_violation")
					if(hash.indexOf("#/violation/orderDetail")>-1){
						userHelper.closeAppView()
					}
				}else{
					this.getOrderDetail();
				}
            }

			//在app的时候隐藏头部标题
			if(common.isCXYApp()){
				userHelper.controlTitle("show")
			}
        }

		//在app的时候隐藏头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("show")
        }
	}

	componentWillReceiveProps(nextProps){
		//组件props更新

	}

	componentDidMount(){
		//进入组件埋点
		// common.sendCxytj({
		// 	eventId:"Violation_EnterCarDetails"
		// })
	}

	componentWillUnmount() {
		//离开记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)

		if(window.timer){
			//清除定时器
			window.clearInterval(window.timer);
		}

		window.violationOrderResume = null
    }

	//获取订单详情
	getOrderDetail(){
		let { orderId } = this.props.params;

		//请求订单详情数据
		this.props.orderActions.getOrderDetail({orderId},(result)=>{
			this.setState({
				...this.state,
				...result.data
			})
			//启动定时器
			if(result.data.orderStatus==0 || result.data.orderStatus==1){
				if(window.timer){
					//清除定时器
					window.clearInterval(window.timer);
				}
				let { dueTime , nowTime } = result.data 
				let offTime = moment(dueTime)._d.getTime()-moment(nowTime)._d.getTime()
				// alert(this.getTime(offTime))
				window.timer = window.setInterval(()=>{
					if(offTime<0){
						offTime=0;
						window.clearInterval(window.timer)
					}else{
						offTime-=1000
					}
					this.setState({
						timer:this.getTime(offTime)
					})
				},1000)
			}
		})
	}
	
	createViolationBriefProps(violation){
		if(violation){
			let { carNumber , count , degree , money } = violation;
			count = count!=undefined?count:"";
			degree = degree!=undefined?degree:"";
			money = money!=undefined?money:"";
			return {
				title:`${carNumber.substr(0,2)} ${carNumber.substr(2)}车辆违章代办服务`,
				detail:`共办理${count}条，扣${degree}分，罚款${money}元`,
				onClick:()=>this.jumpToViolationDeail(),
			}
		}
	}

	jumpToViolationDeail(){
		sessionStorage.setItem("checkViolationDetail",JSON.stringify(this.state.violation))
		this.props.router.push("violation/detail")
	}

	//获取时间
	getTime(time){

		let date = new Date(time),
			d = Math.floor(time/86400000),
			h = Math.floor((time-d*86400000)/3600000),
			m = Math.floor((time-d*86400000-h*3600000)/60000),
			s = Math.floor((time-d*86400000-h*3600000-m*60000)/1000);
		return {
			d,
			h,
			m,
			s
		}
	}

	//支付
	pay(){
		sessionStorage.setItem("violationServiceOrder",JSON.stringify(this.state))
		let orderId = this.state.orderId
		this.props.router.push("orderPay/merchantId/shopId/productId/210/"+orderId)
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_agentwz_pay"
		})
	}

	//确认验收
	check(){
		let {orderId} = this.state
		this.props.orderActions.check({orderId},(result)=>{
			this.getOrderDetail()
		})
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_agentwz_surepay"
		})
	}

	//延迟验收
	delay(){
		let {orderId} = this.state
		this.props.orderActions.delay({orderId},(result)=>{
			this.getOrderDetail()
		})
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_agentwz_dalaypay"
		})
	}

	//检查违章情况
	checkViolation(){
		let { violation } = this.state;
		let { carId , carNumber } = violation;
		userHelper.checkViolation({
			carId,
			carNumber,
		})
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_agentwz_carstatus"
		})
	}

	//openIM
	openIM(){
		let { shop } = this.state,
			merchantMsg={};
		if(shop){
			let user = shop.users[0]
			merchantMsg = {
				merchantUserId : user.userId, 
				merchantUserName : user.name, 
				merchantUserImage : user.photo, 
				topic:"wzbl",
			}
			userHelper.openIM(merchantMsg)
		} 
	}

	telephoneCall(props){
		window.location.href="tel:"+props.phoneNumber
	}

	render(){
		// console.log("state",this.state)
		let { orderStatus , shop ,violation} = this.state
		let ProcedureProps = {
			status:this.state.orderStatus,
			descriptions:this.state.descriptions,
			title:this.state.title,
			orderAmount:this.state.orderAmount,
			timer:this.state.timer || {},
			beforeOrderStatus:this.state.beforeOrderStatus,
			delay:()=>this.delay(),//延迟验收
			surplusDay:this.state.surplusDay,//延迟验收
			isDelay:this.state.isDelay,//是否可以延迟验收
			check:()=>{this.check()},//确认验收
			checkViolation:()=>{this.checkViolation()},
		}
		let ConsultProps ={
			shopName:shop?shop.shopName:"",
			phoneNumber:shop?shop.telephone:"",
			consult:()=>this.openIM(),
			phone:(p)=>this.telephoneCall(p),
		}
		let ViolationBriefProps = this.createViolationBriefProps(violation);
		let FootBarProps = {
			status:this.state.orderStatus,
			timer:this.state.timer || {},
			onClick:()=>this.pay(),
		}

		return (
			<div className={ orderStatus=="1"? "has-footBar":"" }>
				<Procedure {...ProcedureProps}/>
				<Consult {...ConsultProps}/>
				<div className='hr'></div>
				<ViolationBrief {...ViolationBriefProps}/>
				{
					orderStatus=="0" && <div className='whitespace-24'></div>
				}
				{
					orderStatus=="0"&&
					<ServiceNotice 
						title="注意事项"
						content={this.state.note}
					/>
				}
				<div className='whitespace-24'></div>
				{
					<div className="orderMsg">
						{this.state.orderId && <div>订单编号：{this.state.orderId}</div>}
						{this.state.payTypeName && orderStatus!=0 && orderStatus!=1 && orderStatus!=3 && <div>支付方式：{this.state.payTypeName}</div>}
						{this.state.payNo && <div>交易号：{this.state.payNo}</div>}
						{this.state.orderFinishTime && <div>订单确认：{this.state.orderFinishTime}</div>}
						{this.state.finishTime && <div>商家确认办完：{this.state.finishTime}</div>}
						{this.state.payTime && <div>支付时间：{this.state.payTime}</div>}
						{this.state.priceTime && <div>商家报价：{this.state.priceTime}</div>}
						{this.state.orderTime && <div>询价发起：{this.state.orderTime}</div>}
						{this.state.serviceType && <div>服务类型：{this.state.serviceType}</div>}
					</div>
				}
				<div className='h56'></div>
				{ 
					orderStatus=="1" && 
					<FootBar
						{...FootBarProps}
					/> 
				}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	//state:state //容器组件props
	shops:state.shops,
})

const mapDispatchToProps = dispatch => ({
	orderActions: bindActionCreators(orderActions, dispatch) //容器组件action
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ViolationOrder)