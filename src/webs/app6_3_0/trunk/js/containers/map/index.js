 import React, { Component }from 'react';
 import common from '../../utils/common'
 import userHelper from '../../utils/userHelper'
 import styles from './index.scss'

class GMap extends Component {
	componentDidMount(){
		//定义标题
		common.setViewTitle('地址详情');

		//在app的时候控制头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("show")
        }

		window.mapResume = ()=>{
			//在app的时候控制头部标题
			if(common.isCXYApp()){
				userHelper.controlTitle("show")
			}
		}

		let data = this.props.location.query
		let map = new AMap.Map('map', {
		    resizeEnable: true,
		    zoom:12,
		    center: [data.longitude,data.latitude]        
		});

		let infoWindow = new AMap.InfoWindow({
			content:data.address,
			offset: new AMap.Pixel(0, -25)
		});
		let marker = new AMap.Marker({
			position: [data.longitude, data.latitude],
			title: data.address,
			map: map
		});
		marker.on("click",function(){
			infoWindow.open(map, marker.getPosition());
		})
		
		infoWindow.open(map, marker.getPosition());
		//map.setFitView();
		// const markerClick=(e)=>{
		//     infoWindow.setContent(e.target.content);
		//     infoWindow.open(map, e.target.getPosition());
		// }
	}
	componentWillUnmount() {
		//离开记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
		window.mapResume = null
    }

	render(){
		return (
            <div id="map" className={styles.map}></div>
		)
	}
}

export default GMap