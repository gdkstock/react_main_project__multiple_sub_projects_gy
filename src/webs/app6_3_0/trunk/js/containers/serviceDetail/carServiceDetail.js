/**
 * 养护类服务详情
 * Created by lijun on 2017/7/11.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as MerchantActions from '../../actions/merchantActions'

//ceshi 
import * as CarActions from '../../actions/carActions'
import * as ShopActions from '../../actions/merchantActions'

import common from '../../utils/common'
import config from '../../config'
import userHelper from '../../utils/userHelper'

//基础组件
import { Consult , ServiceNotice , Banner , ServiceList , FullBtn , TopBar} from '../../components/common'
import { ServiceTitle } from '../../components/carServiceDetail'

import { Toast } from 'antd-mobile'
import jsApi from '../../utils/cx580.jsApi'

class ServiceDetail extends Component{
	constructor(props){
		super(props);
		//设置初始状态

		this.state = {
			"merchantId":this.props.params.merchantId,
			"shopId":this.props.params.shopId,
			"productId":this.props.params.productId,
			"_typeId":"220",
			"name": "",
			"productDesc": "",
			"shopName": "",
			"phone": "",
			"price": undefined,
			"status": "1",
			"serviceInfo": [
				
			],
			"productImageInfo": [
				
			],
			isShowTitle:false,
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle(' ');

		console.log(this.props)
		console.log("xxxx",userHelper);
		

		//请求服务详情数据  AA13FD960F0A49E5A188DC04C64F4804（养护） / B9F94C9AA63E457CAD0CCF25F3A0848B(代办)
		let productId = this.props.params.productId;
		this.props.merchantActions.getServiceDetail({productId})

		//请求服务列表数据
		this.props.merchantActions.getServiceList({
			merchantId:this.state.merchantId,
			shopId:this.state.shopId,	
		})

		//判断是否需要返回
        if(common.isBack()){
            window.history.back()
        }

		window.carServiceResume=()=>{
			//在app的时候隐藏头部标题
			if(common.isCXYApp()){
				userHelper.controlTitle("hide")
			}
		}

		//在app的时候隐藏头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("hide")
        }

	}

	componentWillReceiveProps(nextProps){
		//组件props更新
		let productId = this.props.params.productId;
		let { serviceList } = nextProps
		this.setState({
			...this.state,
			...serviceList.data[productId]
		})
	}

	componentDidMount(){
		common.sendCxytj({
            eventId: "cxyapp_p_nearby_maintainserver"
        })
		window.titleShowScroll = ()=>this.controlTitleLayout()
        window.addEventListener("scroll",window.titleShowScroll)

	}

	componentWillUnmount() {
		//离开记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
		window.removeEventListener("scroll",window.titleShowScroll)
		window.carServiceResume = null
    }

	//控制title显示隐藏
    controlTitleLayout(){
        let top = document.body.scrollTop;
        let preIsShowTitle = this.state.isShowTitle;
        let isShowTitle = top>100;
        if(isShowTitle !=preIsShowTitle){
            this.setState({
                isShowTitle
            })
        }
    }

	//生成图片列表数据
	createBannerListProps(data){
		return data.map((item)=>{
			return {
				imgUrl:item.imageUrl
			}
		})
	}

	//点击立即抢购按钮
	buyService(){
		let { name , serviceInfo , price } = this.state;
		let productDesc = "";
		serviceInfo && serviceInfo.map((item,i)=>{
			productDesc+=`${item.serviceTitle}${i!=serviceInfo.length-1?";":""}`
		})
		let orderMsg = {
			name:this.state.name,
			productDesc,
			price:this.state.price,
		}
		sessionStorage.setItem("carServiceOrder",JSON.stringify(orderMsg))
		
		let { merchantId , shopId , productId , _typeId } = this.state
		this.props.router.push("orderPay/"+merchantId+"/"+shopId+"/"+productId+"/"+_typeId);
		common.sendCxytj({
            eventId: "cxyapp_e_nearby_maintainserver_buy",
        })
	}

	//打开IM
	openIM(){
		let { shopId } = this.state,
			{ shops } = this.props,
			user = {
				userId:"",
				name:"",
				photo:"",
			};

		if(shops.data[shopId]){
			user = shops.data[shopId].users[0]	
		}else if(sessionStorage.getItem("merchantMsg")){
			user = JSON.parse(sessionStorage.getItem("merchantMsg"))[0]
		}
		let merchantMsg = {
			merchantUserId : user.userId, 
			merchantUserName : user.name, 
			merchantUserImage : user.photo, 
			topic:"qcyh",
		}
		userHelper.openIM(merchantMsg)
		common.sendCxytj({
            eventId: "cxyapp_e_nearby_maintainserver_chat",
			eventPropertis: {
                merchantUserId : user.userId, 
				merchantUserName : user.name, 
				merchantUserImage : user.photo, 
            }
        })
	}

	createServiceNoticeProps(state){
		let { productDesc } = state;
		let content = '';
		if(Object.prototype.toString.call(productDesc)=="[object Array]"){
			content = productDesc.join("<br>")
		}else{
			content = productDesc
		}
		return {
			title:"服务须知",
			content,
		}
	}

	createServiceListProps(state){
		let { serviceInfo } = state
		let List = []
		if(serviceInfo){
			List = serviceInfo.map((item)=>{
				return {
					title:item.serviceTitle,
					desc:item.serviceDesc,
					serviceDescUrl:item.serviceDescUrl,
				}
			})
		}
		return {
			List,
			handleIconClick:(item)=>this.promptIconClick(item)
		}
	}

	//i图标点击事件
	promptIconClick(item){
		if(item.serviceDescUrl){
			if(common.isCXYApp()){
				userHelper.openNewBrowser(item.serviceDescUrl)
			}else{
				window.location.href = item.serviceDescUrl
			}
		}
		common.sendCxytj({
            eventId: "cxyapp_e_nearby_maintainserver_tips"
        })
	}

	//后退
	back(){
		window.history.back();
	}

	telephoneCall(props){
		window.location.href="tel:"+props.phoneNumber
		let { merchantId , shopId } = this.state
		common.sendCxytj({
            eventId: "cxyapp_e_nearby_maintainserver_call",
			eventPropertis: {
                merchantId,
                shopId,
            }
        })
	}

	render(){

		console.log('______________________',this.state)
		let { status , isShowTitle , serviceInfo , productDesc} = this.state;
		let type = common.isIPhone()?"ios":"andriod";
		//顶部服务详情图片数据
		let bannerList = this.createBannerListProps(this.state.productImageInfo);
		let ServiceNoticeProps = this.createServiceNoticeProps(this.state)
		let ServiceListProps = this.createServiceListProps(this.state)
		return (
			<div className="has-footBar">
				<TopBar 
					backBtnClick={()=>this.back()}
					title="服务详情"
                    isShowTitle={isShowTitle}
                    type={type}
				/>
				<Banner bannerList={bannerList}/>
				<ServiceTitle
					title={this.state.name}
					price={this.state.price}
					labelList={["随时退","过期自动退"]}
				/>
				<div className='hr'></div>
				<Consult
					shopName={this.state.shopName}
					consult={()=>this.openIM()}
					phone={(p)=>this.telephoneCall(p)}
					phoneNumber = {this.state.phone}
				/>
				{serviceInfo && serviceInfo.length>0 && <div className='whitespace-24'></div>}
				{serviceInfo && serviceInfo.length>0 && <ServiceList {...ServiceListProps} />}
				{productDesc && productDesc.length>0 && <div className="whitespace-24"></div>}
				{productDesc && productDesc.length>0 && <ServiceNotice {...ServiceNoticeProps}/>}
				<div className="h56"></div>
				<FullBtn 
					text={status=="1"?"立即抢购":"该服务已下架"}
					isDisabled = {status!=1}
					handleClick = {()=>this.buyService()}
					type="1"
				/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	//state:state //容器组件props
	serviceList:state.serviceList,
	shops:state.shops,
})

const mapDispatchToProps = dispatch => ({
	merchantActions: bindActionCreators(MerchantActions, dispatch) ,
	carActions: bindActionCreators(CarActions, dispatch) ,//容器组件action
	shopActions: bindActionCreators(ShopActions, dispatch) 
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ServiceDetail)