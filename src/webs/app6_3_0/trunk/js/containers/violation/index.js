/**
 * 违章列表
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Toast } from 'antd-mobile'

//样式
import styles from './index.scss'; // css

//组件
import { Card, EmptyViolation } from '../../components/violation'

//actions
import * as violationActions from '../../actions/violationActions'
import * as carActions from '../../actions/carActions'

//常用工具类
import cx580JsApi from '../../utils/cx580.jsApi';
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'


class ViolationList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type:"tall", //UI展示类型 narrow 窄 tall 高
            carList: [], //车辆列表
            carKey: 0, //当前的车辆列表key
            carsData:{}, //车辆列表数据
            carIds:[], //车辆列表
            curCarId:"", //当前车辆id
            untreatedViolation: 0, //勾选违章条数
            degree: 0, //勾选违章的总扣分
            fine: 0, //勾选违章的总金额
            all: {
                untreatedViolation: 0, //总违章条数
                degree: 0, //总违章的总扣分
                fine: 0, //总违章的总金额
            },
            violations: [],
            violationIds: [],
            checkeds: [], //已选违章ID列表
            checkAlled: false, //全选
            total: {
                totalNum: 0, //可办理违章总数
                totalDegree: 0, //可办理总扣分
                totalFine: 0, //可办理总罚款
                checkedNum: 0, //勾选违章总数
                checkedDegree: 0, //勾选总扣分
                checkedFine: 0, //勾选总罚款
            },
        };
    }

    componentWillMount() {
        common.setViewTitle("选择违章");

        this.getData();

        window.violationShareResume = ()=>{
            if(sessionStorage.getItem("isFromLogin")){
                sessionStorage.removeItem("checkedViolationsState");
                sessionStorage.removeItem("isFromLogin");
                if(sessionStorage.getItem("isChangeUser")){
                    window.history.back()
                }else{
                    this.getData(); 
                }
            }
        }
    }

    componentWillUnmount() {
        sessionStorage.setItem('checkedViolationsState', JSON.stringify({...this.state,type:"tall"}))
        window.removeEventListener('scroll', window.violationListScroll)
        window.violationShareResume = null
    }

    componentDidMount() {
        window.violationListScroll = ()=>this.handleScroll()
        window.addEventListener('scroll', window.violationListScroll)
    }

    getData(){
        let { cars } = this.props
        if(sessionStorage.getItem('checkedViolationsState')){
            this.setState(JSON.parse(sessionStorage.getItem('checkedViolationsState')))
        }else{
            //判断全局state中是否存在车辆车辆信息
            if(cars.result.length>0){
                let carsData = cars.data
                let curCarId = cars.result[0]
                let state = {}
                if(carsData[curCarId].count==undefined || carsData[curCarId].violations==undefined){
                    this.props.violationActions.getCarViolations({carId: curCarId},(result)=>{
                        console.log("result",result)
                        if(result.data.violations){
                            let violations = result.data.violations;
                            state = this.dataInit(violations)
                            this.setState({
                                ...state,
                                carsData:cars.data,
                                carIds:cars.result,
                                curCarId,
                                carKey:0,
                            })
                        }
                    })
                }else{
                    let violations = carsData[curCarId].violations;
                    state = this.dataInit(violations)
                    this.setState({
                        ...state,
                        carsData:cars.data,
                        carIds:cars.result,
                        carKey: 0, //当前的车辆列表key
                        curCarId:cars.result[0], //当前车辆id
                    })
                }
            }else{
                this.props.carActions.getCars({},(result)=>{
                    let carsData = {},curCarId = "",state={},carIds=[],carKey=0;
                    if(result.data && result.data.length>0){
                        let len = result.data.length
                        let carList = result.data;
                        result.data.map((item,i)=>{
                            //获取违章列表
                            this.props.violationActions.getCarViolations({carId:item.carId},(result)=>{
                                if(result.code=="1000"){
                                    if(result.data.count!="0"){
                                        carsData[item.carId]= {carId:item.carId,carNumber:item.carNumber,...result.data}
                                        carIds.push(item.carId)
                                    }
                                }
                                len--;
                                if(len==0){
                                    if(carIds.length>0){
                                        curCarId = carIds[carKey];
                                        if(carsData[curCarId] && carsData[curCarId].violations){
                                            let violations = carsData[curCarId].violations;
                                            state = this.dataInit(violations)
                                            console.log(carIds,carsData,curCarId,carKey)
                                            this.setState({
                                                ...state,
                                                carsData,
                                                curCarId,
                                                carIds,
                                                carKey,
                                            })
                                        } 
                                    }else{
                                       Toast.info("暂无违章车辆",2) 
                                    }
                                }
                            })
                        })
                    }else{
                        Toast.info("暂无违章车辆",2)
                    }
                })
            }

        }
    }

    componentWillReceiveProps(nextProps){

    }

    //监听body滚动
    handleScroll(){
        let top = document.body.scrollTop;
        let type = top>100?"narrow":"tall";
        let preType = this.state.type;
        if(preType!=type){
            this.setState({
                type,
            })
        }
    }

    componentDidUpdate() {
        //console.log("componentDidUpdate");
    }

    /**
     * 数据初始化
     */
    dataInit(violations) {
        let violationIds = [];
        let checkAlled = true; //全选
        let total = {
            totalNum: 0, //可办理违章总数
            totalDegree: 0, //可办理总扣分
            totalFine: 0, //可办理总罚款
            checkedNum: 0, //勾选违章总数
            checkedDegree: 0, //勾选总扣分
            checkedFine: 0, //勾选总罚款
        }
        violations.map(violation => {
            let { violationId, degree, fine } = violation
            violationIds.push(violationId) //违章ID列表
            total.totalNum++;
            total.totalDegree += degree * 1
            total.totalFine += fine * 1

            total.checkedNum = total.totalNum
            total.checkedDegree = total.totalDegree
            total.checkedFine = total.totalFine
        })
        return {
            violations: violations,
            violationIds: violationIds,
            checkeds: violationIds, //默认全选
            checkAlled: checkAlled,
            total: total
        };
    }

    /**
     * 选中||取消 某一条违章
     * @param {*Object} violation 
     */
    checkViolation(violation) {
        let { checkeds, total } = this.state
        let { violationId, degree, fine } = violation
        let add = 1 //增加
        if (checkeds.indexOf(violationId) > -1) {
            //取消
            checkeds = checkeds.filter(item => item != violationId)
            add = -1
        } else {
            //选中
            checkeds.push(violationId)
            add = 1
        }

        total.checkedNum += add
        total.checkedDegree += degree * add
        total.checkedFine += fine * add

        this.setState({
            checkeds: checkeds,
            checkAlled: checkeds.length == total.totalNum, //勾选违章等于违章总数时，自动全选
            total: total
        })
    }

    /**
     * 全选
     */
    checkAll(check) {
        let { total, violationIds } = this.state

        if (check) {
            //全选
            total.checkedNum = total.totalNum
            total.checkedDegree = total.totalDegree
            total.checkedFine = total.totalFine

        } else {
            //取消
            total.checkedNum = 0
            total.checkedDegree = 0
            total.checkedFine = 0

            violationIds = []
        }

        this.setState({
            checkeds: violationIds,
            checkAlled: check,
            total: total
        });
    }

    /**
     * 分享
     */
    share() {
        let { carIds, total, checkeds , curCarId , carsData} = this.state

        if (checkeds.length == 0) {
            Toast.info("请选择分享违章", 1);
            return;
        }

        let carInfo = carsData[curCarId];
        let carNumber = carInfo.carNumber
        let carId = carInfo.carId;

        console.log("选择的违章：", checkeds)
        checkeds = checkeds.toLocaleString().replace(/\,/g, ';')
        let checkedViolations = Object.assign({}, total, {
            checkeds,
            carId,
            carNumber
        })
        sessionStorage.setItem('checkedViolations', JSON.stringify(checkedViolations)); //保存勾选的数据
        sessionStorage.setItem('checkedViolationsState', JSON.stringify({...this.state,type:"tall"}))
        window.history.back(); //页面后退
    }

    //选择下一辆车
    nextCar(){
        let { curCarId , carIds , carKey , carsData } = this.state
        let len = carIds.length;
        sessionStorage.setItem("car"+curCarId,JSON.stringify({...this.state,type:"tall"}))

        carKey = carKey == len-1 ? 0 : ++carKey;
        curCarId = carIds[carKey];
        if(sessionStorage.getItem("car"+curCarId)){
            this.setState(JSON.parse(sessionStorage.getItem("car"+curCarId)))
        }else{
            let state={}
            if(carsData[curCarId].violations){
                let violations = carsData[curCarId].violations;
                state = this.dataInit(violations)
                this.setState({
                    ...state,
                    curCarId,
                    carKey,
                })
            }else{
                this.props.violationActions.getCarViolations({carId:curCarId},(result)=>{
                    console.log("result",result)
                    if(result.data.violations){
                        let violations = result.data.violations;
                        state = this.dataInit(violations)
                        this.setState({
                            ...state,
                            curCarId,
                            carKey,
                        })
                    }
                })
            }
        }
    }

    render() {
        let { violations, checkeds, total, checkAlled , carsData , curCarId , carIds , type} = this.state
        console.log("________",this.state)
        let cardProps = {
            clickCheckbox: () => false, //点击了勾选框
            clickRight: () => false, //点击了右边区域
            clickFooterText: () => false, //点击了补充资料区域
        }
        console.log(sessionStorage.getItem("checkedViolationsState"))
        console.log(carsData[curCarId])
        let carNumber = carsData[curCarId]? carsData[curCarId].carNumber : "";

        return (
            <div className="has-footBar">
                {/*车辆信息 start*/}
                <div>
                    <div className={styles.carInfo}></div>
                    <div className='hr'></div>
                    <div className={styles.violation}></div>
                </div>
                <div className={styles.carInfoWrap}>
                    <div className={styles.carInfo}>
                        <div>{carNumber.substr(0,2)+" "+carNumber.substr(2)}</div>
                        { carIds.length>1 && <div className={styles.nextBtn} onClick={()=>this.nextCar()}>下一辆</div> }
                    </div>
                    <div className='hr'></div>
                    {type=="tall" &&
                        <div className={styles.violation}>
                            <div>
                                <h3>违章</h3>
                                <p>{total.checkedNum}</p>
                            </div>
                            <div>
                                <h3>罚款</h3>
                                <p>{total.checkedFine}</p>
                            </div>
                            <div>
                                <h3>扣分</h3>
                                <p>{total.checkedDegree}</p>
                            </div>
                        </div>
                    }
                    {type=="narrow" &&
                        <div className={styles.violationNarrow}>
                            <div>
                                <span>已选&nbsp;{total.checkedNum}&nbsp;条</span>
                                <span>罚款&nbsp;{total.checkedFine}&nbsp;元</span>
                                <span>扣分&nbsp;{total.checkedDegree}&nbsp;分</span>
                            </div>
                        </div>
                    }
                </div>
                {/*车辆信息 end*/}
                <div className="whitespace-24"></div>
                {/*违章列表 start*/}
                {violations.map((item, i) => <div key={item.violationId}>
                    <Card 
                        {...item}
                        clickCheckbox={data => this.checkViolation(data)}
                        checked={checkeds.indexOf(item.violationId) > -1}
                        isCheckboxShow = {true}
                    />
                    <div className="whitespace-24"></div>
                </div>)}
                {/*违章列表 end*/}

                {/*底部按钮 start*/}
                <div className={styles.fbtn}>
                    <div className={styles.checkbox + ' ' + (checkAlled ? styles.checkBoxTrue : styles.checkBoxFalse)} onClick={() => this.checkAll(!checkAlled)}></div>
                    <div className={styles.ftxt}>
                        <div className='abs-v-center'>
                            <span>全选</span>
                            <i>已选{checkeds.length}条违章</i>
                        </div>
                    </div>
                    {
                        checkeds.length === 0 ?
                            <div className={styles.btn + ' ' + styles.notClickBtn}>确认</div>
                            :
                            <div className={styles.btn} onClick={() => { this.share() }}>确认</div>
                    }
                </div>
                {/*底部按钮 end*/}
            </div>
        )
    }
}

ViolationList.defaultProps = {
    loading: true,
    error: undefined,
    datas: {
        carNumber: "",
        untreatedViolation: 0,
        degree: 0,
        fine: 0,
        violations: []

    }
}

const mapStateToProps = state => ({
    cars:state.cars,
})

const mapDispatchToProps = dispatch => ({
    violationActions: bindActionCreators(violationActions, dispatch),
    carActions: bindActionCreators(carActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViolationList)