/**
 * 车辆相关接口
 */

import apiHelper from './apiHelper';

class CarService {

    /**
     * 车辆违章概要信息列表
     * @param {*object} data
     */
    getCarList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}car/resume/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 车辆列表
     * @param {*object} data
     */
    getCars(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}car/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
    
}

export default new CarService()