/**
 * 车辆相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_CAR_LIST,
    ADD_CAR_LIST,
    GET_CARS,
    ADD_CARS,
} from '../actions/actionsTypes'

//service
import CarService from '../services/carService'

import { ChMessage } from '../utils/message.config' //提示信息

import { Toast } from 'antd-mobile' //UI

import { normalize, schema } from 'normalizr'; //范式化库

/***************获取车辆列表***************/
    function* watchGetCarList() {
        yield takeLatest(GET_CAR_LIST, getCarList)
    }

    function* getCarList(action) {
        Toast.loading("",0)
        try {
            let { result, timeout } = yield race({
                result: call(CarService.getCarList, action.param),
                timeout: call(delay, 30000)
            })
            Toast.hide()
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == "1000") {
                    console.log("车辆列表", result.data)
                    yield put({type:ADD_CAR_LIST,data:result.data})
                    if (action.callback) {
                        action.callback(result)
                    }
                }
            }
        } catch (error) {
            Toast.hide();
            window.networkError();
        }
    }

/***************获取车辆列表***************/
    function* watchGetCars() {
        yield takeLatest(GET_CARS, getCars)
    }

    function* getCars(action) {
        Toast.loading("",0)
        try {
            let { result, timeout } = yield race({
                result: call(CarService.getCars, action.param),
                timeout: call(delay, 30000)
            })
            Toast.hide();
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == "1000") {
                    console.log("车辆列表", result.data)
                    // yield put({type:ADD_CARS,data:result.data})
                    if (action.callback) {
                        action.callback(result)
                    }
                }
            }
        } catch (error) {
            Toast.hide();
        }
    }

export function* watchCarsFetch() {
    yield [
        fork(watchGetCarList),
        fork(watchGetCars),
    ]
}