// saga 模块化引入
import { fork } from 'redux-saga/effects'

import { watchCarsFetch } from './cars.js'
import { watchMerchantFetch } from './merchant'
import { watchViolationFetch } from './violation'
import { watchOrderFetch } from './order'

// 单一进入点，一次启动所有 Saga
export default function* rootSaga() {
  yield [
    fork(watchCarsFetch),
    fork(watchMerchantFetch),
    fork(watchViolationFetch),
    fork(watchOrderFetch),
  ]
}