/*
* 这里定义所有的action类型
* */

export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时

//商家相关
export const GET_SERVICE_LIST = 'GET_SERVICE_LIST' //获取商家服务列表
export const GET_SERVICE_DETAIL = 'GET_SERVICE_DETAIL' //获取商家服务详情
export const GET_SHOP_LIST = 'GET_SHOP_LIST' //获取商家列表
export const GET_SHOP_DETAIL = 'GET_SHOP_DETAIL' //获取商家详情
export const GET_SHOP_NUM = 'GET_SHOP_NUM' //获取可办理商家总数
export const GET_CITY_LIST = 'GET_CITY_LIST' //获取城市列表
export const GET_TYPE_LIST = 'GET_TYPE_LIST' //获取服务类型列表

//违章相关
export const GET_CAR_VIOLATIONS = 'GET_CAR_VIOLATIONS' //获取车辆的违章列表

//车辆相关
export const GET_CAR_LIST = 'GET_CAR_LIST'  //获取车辆违章概要信息列表
export const GET_CARS = 'GET_CARS' //获取车辆列表

//订单相关
export const CREATE_ORDER = 'CREATE_ORDER' //生成订单
export const GET_ORDER_DETAIL = 'GET_ORDER_DETAIL' //获取订单详情
export const CREATE_REFUND = 'CREATE_REFUND' //申请退款
export const ORDER_PAY = 'ORDER_PAY' //违章服务订单支付
export const ORDER_DELAY = 'ORDER_DELAY' //订单延迟验收
export const ORDER_CHECK = 'ORDER_CHECK' //订单确认验收
export const ADD_C_USER = 'ADD_C_USER' //订单支付成功，提交用户数据
export const GET_AD = 'GET_AD' //订单支付成功，获取广告

//所有同步actions
export const ADD_CAR_LIST = 'ADD_CAR_LIST' //添加车辆列表到state
export const ADD_SHOP_LIST = 'ADD_SHOP_LIST' //添加店铺列表到state
export const ADD_SERVICE_DETAIL = 'ADD_SERVICE_DETAIL' //添加到服务列表state
export const ADD_SHOP_NUM = 'ADD_SHOP_NUM' //添加店铺数量
export const ADD_CARS = "ADD_CARS" //添加车辆数据
export const ADD_CAR_VIOLATIONS = 'ADD_CAR_VIOLATIONS' //添加到车辆违章state
export const ADD_SHOP_DETAIL = 'ADD_SHOP_DETAIL' //添加到店铺详情
