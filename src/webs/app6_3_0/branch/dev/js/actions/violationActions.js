/*
 * 商家相关
 */

import {
    GET_CAR_VIOLATIONS
} from './actionsTypes'

//请求违章列表
export const getCarViolations = (param, callback) => ({ type: GET_CAR_VIOLATIONS, param, callback })
