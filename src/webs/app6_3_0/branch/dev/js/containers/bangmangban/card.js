/**
 * 帮忙办相关组件
 */

//style
import styles from './index.scss'

const defalutProps = {
    "carId": "2201",
    "carNumber": "粤Y03Q28",
    "count": 26,
    "degree": 104,
    "money": 6050,
    "warnMsg": [
        "违章数达到4条及以上，提醒：本车未处理违章已超过4条，根据部分地区的法规，您的车辆上路将会被查扣，行驶证也将会被扣留，请您尽快处理！",
        "车辆最早违章时间距离当前时间超过半年，提醒：您有违章超过半年未处理，为避免产生滞纳金，或是影响车辆年检，请您尽快处理。"
    ]
}

export const Card = props =>{
    // props = defalutProps
    return (
        <div className={styles.card}>
            <div className={styles.carNumber}><span>{props.carNumber && (props.carNumber.substr(0,2)+" "+props.carNumber.substr(2))}</span></div>
            <div className={styles.violation}>
                <div className={styles.violationItem}>
                    <h3>{props.count}</h3>
                    <p>违章数</p>
                </div>
                <div className={styles.violationItem}>
                    <h3>{props.money}</h3>
                    <p>总计罚款(元)</p>
                </div>
                <div className={styles.violationItem}>
                    <h3>{props.degree}</h3>
                    <p>已扣分</p>
                </div>
            </div>
            { props.warnMsg && props.warnMsg.length>0 &&
                <div className={styles.msgList}>
                    {
                        props.warnMsg && props.warnMsg.map((item,i)=>
                            <div key={i}>
                                <div></div>
                                {item}
                            </div>
                        )
                    }
                </div>
            }
            
        </div>
    )
}