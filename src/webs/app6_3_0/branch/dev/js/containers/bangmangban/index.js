/**
 * 帮忙办
 */

import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//组件
import { Card } from './card'

//style
import styles from './index.scss'

//通用公共类
import common from '../../utils/common'
import jsApi from '../../utils/cx580.jsApi'
import userHelper from '../../utils/userHelper'

import * as carActions from '../../actions/carActions'
import * as merchantActions from '../../actions/merchantActions'


class BangMangBan extends Component {
    constructor(props) {
        super(props);
        let { cars , shops } = this.props;
        this.state={
            cars,
            count:shops.count==undefined?"":shops.count,
        }

    }

    componentWillMount() {
        common.setViewTitle('帮忙办')

        //判断是否切换用户
        if(sessionStorage.getItem("isChangeUser")){
            sessionStorage.clear()
            this.getCarList()
        }

        //进入首页缓存首页hash路由
        if(!sessionStorage.getItem("indexHash")){
            let hash = window.location.hash
            sessionStorage.setItem("indexHash",hash)
        }

        //清楚商家列表页面缓存
        sessionStorage.removeItem("shopListState")

        //判断是否需要返回
        if(common.isBack()){
            window.history.back()
        }

        //回到栈顶时触发
        window.bangmangbanResume=()=>{
            // alert("11111"+new Date().getTime())
            sessionStorage.removeItem("bangmangbanNeedRefresh")
            if(sessionStorage.getItem("isFromLogin")){
                if(common.isBack()){
                    window.history.back();
                }else{
                    this.getCarList()
                }
                sessionStorage.removeItem("isFromLogin")
            }
        }

        if(common.isCXYApp()){
            userHelper.hideRightBtn()
        }

        //请求车辆列表数据
        this.getCarList()
    }

    componentDidMount() {
        //改变body的背景颜色
        document.body.style.background="#f2f2f2";
        //进入页面埋点
		common.sendCxytj({
			eventId: "cxyapp_p_agentwz"
		})
    }

    componentWillReceiveProps(nextProps) {
        let { cars , shops } = nextProps
        this.setState({
            cars,
            count:shops.count,
        })
    }

    toUrl(url) {
        window.location.href = common.getRootUrl() + url
    }

    componentWillUnmount() {
        //恢复body的背景颜色
        document.body.style.background="#fff";
        window.bangmangbanResume = null;
        //离开页面记录页面信
		sessionStorage.setItem("prevPageInfo", document.title)
    }
    

    /**
     * 跳转到帮忙办商家列表
     */
    toShopList() {
        this.toUrl('shopList/1')
        common.sendCxytj({
			eventId: "cxyapp_e_agentwz_handlebutton"
		})
    }

    /**
     * 获取车辆列表数据
     */
    getCarList(){
        //请求车辆列表数据
        let needRefresh =  sessionStorage.getItem("bangmangbanNeedRefresh")//首页是否需要刷新
        if(!needRefresh){
            sessionStorage.setItem("bangmangbanNeedRefresh","1")
            this.props.carActions.getCarList({},(result)=>{
                if(common.isCXYApp()){
                    try{
                        jsApi.call({
                            "commandId": "",
                            "command": "getSymbol",
                            "data": {
                                "city": "",
                                "lng": "",
                                "lat": "",
                                "cityCode":"",
                                "deviceId":"",
                            }
                        },(data)=>{
                            //请求商家总数

                            // data.data = {//模拟数据 正式删除
                            //     lng:"113.13111877441406",
                            //     lat:"23.25702476501465",
                            //     city:"广州", 
                            // }

                            sessionStorage.setItem("location",JSON.stringify(data.data))
                            sessionStorage.setItem("productUserId",data.deviceId)
                            this.props.merchantActions.getShopNum({
                                longitude:data.data.lng,//data.data.lng,//"113.13111877441406",
                                latitude:data.data.lat,//data.data.lat,//"23.25702476501465",
                                cityName:data.data.city,
                                cityCode:data.data.cityCode,
                                typeCode:"210",
                            })
                        })
                    }catch(e){

                    }
                }else{//不在app时模拟数据
                    this.props.merchantActions.getShopNum({
                        longitude:"113.338027",
                        latitude:"23.107678",
                        cityName:"广州",
                        cityCode:"4401",
                        typeCode:"210",
                    })
                }
            })
        }
    }

    render() {
        let { data , result } = this.state.cars
        let { count } = this.state
        return (
            <div className='box'>
                {
                    result.map((item,i)=><Card key={i} {...data[item]}/>)
                }
                { result.length>0 &&
                    <div className={styles.footer}>
                        <span>有{count}家商家可以帮你办理</span>
                    </div>
                }
                <div style={{ height: '1rem' }}></div>
                <div className={styles.btn} onClick={() => this.toShopList()}>一键求助，轻松办理</div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cars:state.cars,
    shops:state.shops,
})

const mapDispatchToProps = dispatch => ({
	carActions: bindActionCreators(carActions, dispatch) ,//容器组件action
    merchantActions: bindActionCreators(merchantActions, dispatch) ,
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(BangMangBan);