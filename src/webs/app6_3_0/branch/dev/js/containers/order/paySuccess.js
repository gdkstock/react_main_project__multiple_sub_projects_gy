/**
 * 订单支付成功
 * Created by lijun on 2017/7/10.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as orderActions from '../../actions/orderActions'
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'
import config from '../../config'

//基础组件
import PaySucc from '../../components/paySuccess'

// import { Toast } from 'antd-mobile'

class PaySuccess extends Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			name:this.props.params.name,
			orderId:this.props.params.orderId,
			orderDetail:()=>this.jumptoOrderDetail(),
			bannerClick:(p)=>this.bannerClick(p),
			adList:[]
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('支付成功');

		//在app的时候隐藏头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("show")
        }

		//请求广告接口
		this.props.orderActions.getAd({
			positionCode:"WZ_HELPDO_PAY_01"
		},(result)=>{
			this.setState({
				adList:result.data.adList
			})
		})
	}

	componentWillReceiveProps(nextProps){
		//组件props更新
	}

	componentDidMount(){
		//进入组件埋点
		common.sendCxytj({
			eventId:"cxyapp_p_shop_ordersuccess"
		})
	}

	componentWillUnmount() {
		//离开记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

	//查看详情
	jumptoOrderDetail(){
		let { orderId } = this.state
		window.location.replace(common.getRootUrl()+"carService/orderDetail/"+orderId)
		common.sendCxytj({
			eventId:"cxyapp_e_shop_ordersuccess_detail"
		})
	}

	//广告点击
	bannerClick(item){
		if(item){
			window.location.href=item.targetUrl
		}
	}

	render(){
		console.log(this.state)
		return (
			<div>
				<PaySucc {...this.state}/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	//state:state //容器组件props
})

const mapDispatchToProps = dispatch => ({
	orderActions: bindActionCreators(orderActions, dispatch) //容器组件action
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PaySuccess)