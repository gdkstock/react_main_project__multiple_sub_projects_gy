/**
 * 帮忙办-商家列表
 */

import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Icon } from 'antd-mobile';

//组件
import LoadingMore from 'app/components/common/LoadingMore'
import Notice from '../../components/notice'
import { Shop, HeaderNav, AddressList, ServerList } from '../../components/shopList'

//style
import styles from './index.scss'

//通用公共类
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'
import * as merchantActions from '../../actions/merchantActions'
import jsApi from '../../utils/cx580.jsApi'


class ShopList extends Component {
    constructor(props) {
        super(props);

        let { type } = this.props.params;
        let isShowTips = !localStorage.getItem("isShowTips");
        let location = undefined ;
        let curCity = "北京";
        let cityCode = "11";
        let { shops } = this.props
        if(sessionStorage.getItem("location")){
            location = JSON.parse(sessionStorage.getItem("location"));
            curCity = location.city;
            cityCode = location.cityCode;
        }
        this.state = {
            type: type || '1', //类型 1：帮忙办商家；2：附近商家
            pageNo:1, //页码
            distance:100, //附近商家距离展示范围 单位km
            pageSize:type=="1"?3:20, //每页数量
            location, //当前定位信息
            loadingMoreState:0, //上拉加载组件状态码
            curCity, //当前城市删选条件
            cityCode,//当前城市编码
            curType:"全部", //当前服务类型筛选条件
            code:type==1?"210":"", //当前服务类型码 210 违章类  220 养护类
            cityList:[],  //城市列表数据
            typeList:[],  //类型列表数据
            addressListShow:false,  //城市列表是否展示
            serverListShow:false,  //类型列表是否展示
            shops: {
                data:{},
                result:[],
                count:shops.count,
            },  //店铺列表
            noticeProps: {
                show: isShowTips,
                text:"",
            },
            scrolltop:0,
        }
    }

    componentWillMount() {
        //根据不同type选择展示不同的列表
        let { type } = this.props.params
        let title = '帮忙办商家'
        if (type == '2') {
            title = '附近商家'
        }
        common.setViewTitle(title)

        //切换账号登陆时返回上一页
        if(common.isBack()){
           window.history.back() 
        }else{
            if(sessionStorage.getItem("isChangeUser")){
                sessionStorage.clear();
            }
        }

        //进入首页缓存首页hash路由
        if(!sessionStorage.getItem("indexHash")){
            let hash = window.location.hash
            sessionStorage.setItem("indexHash",hash)
        }

        //在app的时候控制头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("show")
        }

        if(!sessionStorage.getItem("shopListState")){
            this.init()
        }else{
            this.setState(JSON.parse(sessionStorage.getItem("shopListState")))
        }

    }

    componentDidMount() {
        //进入页面埋点
        let { type , scrolltop} = this.state;
        document.body.scrollTop = scrolltop;
        if(type==1){
            common.sendCxytj({
                eventId: "cxyapp_p_agentwz_shoplist"
            })
        }else{
            common.sendCxytj({
                eventId: "cxyapp_p_nearby_shoplist"
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("nextProps",nextProps);
        let { shops } = nextProps
        this.setState({
            shops:shops,
            noticeProps:{
                ...this.state.noticeProps,
                text:shops.tips,
            }
        })
    }

    componentWillUnmount() {
        sessionStorage.setItem("shopListState",JSON.stringify({...this.state,scrolltop:document.body.scrollTop}));
        //离开页面记录页面信
		sessionStorage.setItem("prevPageInfo", document.title)
    }

    init(){
        let { location , shops } = this.state //定位信息
        let { type } = this.props.params

        if(common.isCXYApp()){
            //请求列表数据
            if(location){
                //获取数据
                this.loadingMore(location)
                //获取商家数量
                if(type=="1" && shops && shops.count==undefined){
                    this.props.merchantActions.getShopNum({
                        longitude:location.lng,//data.data.lng,//"113.13111877441406",
                        latitude:location.lat,//data.data.lat,//"23.25702476501465",
                        cityName:location.city,
                        cityCode:location.cityCode,
                        typeCode:"210",
                    })
                }
            }else{
                 common.ready(()=>{
                    try{
                        jsApi.call({
                            "commandId": "",
                            "command": "getSymbol",
                            "data": {
                                "city": "",
                                "lng": "",
                                "lat": "",
                                "cityCode":"",
                            }
                        },(data)=>{
                            sessionStorage.setItem("location",JSON.stringify(data.data))
                            location = data.data 
                            this.setState({
                                location,
                                curCity:location.city,
                                cityCode:location.cityCode,
                            })
                            this.loadingMore(location)
                            if(type=="1" && shops && shops.count==undefined){
                                this.props.merchantActions.getShopNum({
                                    longitude:location.lng,//data.data.lng,//"113.13111877441406",
                                    latitude:location.lat,//data.data.lat,//"23.25702476501465",
                                    cityName:location.city,
                                    cityCode:location.cityCode,
                                    typeCode:"210",
                                })
                            }
                        })
                    }catch(e){
                        // alert("chucuo")
                    }
                 })
            }
        }else{
            if(!location){
                location = {
                    lng:"113.338027",
                    lat:"23.107678",
                    city:"广州",
                    cityCode:"4401",
                }
                sessionStorage.setItem("location",JSON.stringify(location))
                this.setState({
                    location,
                    curCity:location.city,
                    cityCode:location.cityCode,
                })
            }
            //获取数据
            this.loadingMore(location)
            
        }

        if(type == '2'){
            this.props.merchantActions.getCityList({},(result)=>{
                this.setState({
                    cityList:result.data
                })
            })
            this.props.merchantActions.getTypeList({},(result)=>{
                this.setState({
                    typeList:result.data[0].serviceType
                })
            })
        }
    }

    /**
     * 显示城市列表
     */
    showAddressList(item) {
        console.log("显示||隐藏 城市列表" ,item)
        let name = item.name
        // this.selectTap(name);
        if (name) {
            let { addressListShow, serverListShow } = this.state
            addressListShow = !addressListShow; //取反
            serverListShow = false; //隐藏服务的列表

            this.setState({
                addressListShow,
                serverListShow,
            })
        }else{
            this.setState({
                addressListShow:false,
                serverListShow:false,
            })
        }
    }

    /**
     * 显示服务列表
     */
    showServerList(item) {
        console.log("显示服务列表")
        let name = item.name;
        if (name) {
            let { addressListShow, serverListShow  } = this.state;
            serverListShow = !serverListShow; //显示服务列表
            addressListShow= false; //隐藏城市的列表

            this.setState({
                addressListShow,
                serverListShow,
            })
        }else{
            this.setState({
                addressListShow:false,
                serverListShow:false,
            })
        }
    }

    /**
     * 选中Tap中的内容
     * @param {*} name 选中内容的name
     * @param {*} key state.headerNavProps的key
     */
    selectTab(item, key) {
        console.log(key, "点击了：", item)
        if (item) {
            let { curCity , curType , addressListShow, serverListShow , code , pageSize , cityCode , location , distance } = this.state
            if(key=="address"){
                if(curCity!=item.cityName){
                    //重新请求数据
                    this.setState({
                        loadingMoreState: 1, // 对应msg字段的下标
                    });
                    let params = {
                        longitude:location.lng || "",
                        latitude:location.lat || "",
                        pageNo:1,
                        pageSize,
                        cityName:item.cityName,
                        cityCode:item.cityId,
                        typeCode:code,
                        distance,
                    };
                    this.getShopList(params);
                }
                curCity = item.cityName;
                cityCode = item.cityId; 
                //统计
                common.sendCxytj({
                    eventId:"cxyapp_e_nearby_shoplist_location"
                })
            }else{
                if(code!= item.code){
                    //重新请求数据
                    this.setState({
                        loadingMoreState: 1, // 对应msg字段的下标
                    });
                    let params = {
                        longitude:location.lng || "",
                        latitude:location.lat || "",
                        pageNo:1,
                        pageSize,
                        cityName:curCity,
                        cityCode,
                        typeCode:item.code,
                        distance,
                    };
                    this.getShopList(params);
                }
                curType = item.name;
                code = item.code;
                //统计
                common.sendCxytj({
                    eventId:"cxyapp_e_nearby_shoplist_servertype"
                })
            }
            addressListShow = false; //隐藏
            serverListShow = false; //隐藏
            this.setState({
                curCity,
                curType,
                code,
                cityCode,
                addressListShow,
                serverListShow
            })
        }
    }

    /**
     * 滚动页面到指定的位置
     */
    indexesScroll(key) {
        let objs = document.getElementsByClassName('indexesKey-' + key);

        if (objs && objs.length > 0) {
            //存在索引则滚动页面到指定的位置
            document.getElementById('addressList').scrollTop = objs[0].offsetTop
        }
    }

    /**
     * 请求列表数据
     */
    getShopList(params) {
        let type=this.state.type;
        this.props.merchantActions.getShopList(params,(result)=>{
            let { nextFlag , pageNo } = result;
            let loadingMoreState;
            let list = result.data.shopList || [];
            loadingMoreState = (nextFlag && type!="1")?0:pageNo=="1" && list.length==0?3:2;
            pageNo+=nextFlag?1:0;
            // if(!nextFlag){
            //     loadingMoreState:2; // 对应msg字段的下标
            // }else{
            //     loadingMoreState=0;
            //     pageNo++;
            // }
            this.setState({
                loadingMoreState,
                pageNo,
            })
        })
    }

    /**
     * 加载更多
     */
    loadingMore(location) {
        this.setState({
            loadingMoreState: 1, // 对应msg字段的下标
        });
        let { pageNo , pageSize , code , curCity , type , distance} = this.state,
            cityName = location?location.city:curCity,
            cityCode = location?location.cityCode:this.state.cityCode,
            longitude = location?location.lng:this.state.location?this.state.location.lng:"",
            latitude = location?location.lat:this.state.location?this.state.location.lat:"",
            params = {
                longitude,
                latitude,
                pageNo,
                pageSize,
                cityName,
                cityCode,
                typeCode:code,
            };
            type==2 && (params.distance =distance);
        this.getShopList(params);
    }

    /**
     * 店铺点击事件
     */
    clickShop(props){
        console.log(props)
        let { merchantId , shopId , productId } = props
        let { type } = this.props.params
        // if(type==1){
        //     this.props.router.push(`violationServiceDetail/${merchantId}/${shopId}/B9F94C9AA63E457CAD0CCF25F3A0848B`)
        // }
        this.props.router.push(`shopDetail/${merchantId}/${shopId}`)
        if(type==1){
            common.sendCxytj({
                eventId:"cxyapp_e_agentwz_shoplist_shop"
            })
        }else{
            common.sendCxytj({
                eventId:"cxyapp_e_nearby_shoplist_shop"
            }) 
        }
    }

    /**
     * 关闭提示
     */
    closeTips(){
        localStorage.setItem("isShowTips","1");
        this.setState({
            noticeProps: {...this.state.noticeProps, show: false }
        })
        //关闭提醒埋点
        common.sendCxytj({
			eventId: "cxyapp_e_agentwz_shoplist_closenote"
		})
    }

    //生成加载跟多组件属性
    createLoadingMoreProps(msgType){
        return {
            showMsg: true, //显示文字提醒
            msgType, // 对应msg字段的下标
            msg: ['点击加载更多', <div><Icon type="loading" style={{color:"#2582ea"}}/><span style={{paddingLeft:"10px"}}>加载中...</span></div>, 'END',"暂无商家数据"], //文字提醒
            height: 20, //触发加载数据的高度 单位px
            loadingMore: () => this.loadingMore(), //到达底部时，触发的事件
            line: false, //END时,是否添加贯穿文本的横线
            bgColor:"#fff",
        }
    }

    createHeaderNavProps(props){
        let { curCity , curType , addressListShow , serverListShow} = this.state
        return { //头部导航条的props
                address: {
                    key:"address",
                    name: curCity,
                    checked: addressListShow,
                    click: item => this.showAddressList(item),
                },
                server: {
                    key:"server",
                    name: curType,
                    checked: serverListShow,
                    click: item => this.showServerList(item),
                }
            }
    }

    createAddressListProps(){
        let { curCity , cityList , addressListShow } = this.state
        let data = {}
        cityList && cityList.map(item=>{
            let key = item.level;
            !data[key] && (data[key]=[]);
            data[key];
            data[key].push(item);
        })
        return { //城市列表的props
            current: curCity,
            data,
            click: name => this.selectTab(name, 'address'), //点击了城市
            show: addressListShow, //显示
            clickIndexes: (key) => this.indexesScroll(key),
            clickMask:item => this.showAddressList(item)
        }
    }
    
    createServerListProps(){
        let { curType , typeList , serverListShow } = this.state
        console.log("AAAAA",typeList)
        return {
            current: curType,
            data: typeList,
            click: name => this.selectTab(name, 'server'), //点击了服务
            show: serverListShow,
            clickMask:item=>this.showServerList(item),
        }
    }

    render() {
        console.log("klsdjfl",this.state)
        let { type, loadingMoreState, shops, noticeProps} = this.state
        let loadingMoreProps = this.createLoadingMoreProps(loadingMoreState)
        let headerNavProps = this.createHeaderNavProps()
        let addressListProps = this.createAddressListProps()
        let serverListProps = this.createServerListProps()
        return (
            <div className='box'>
                {
                    type == '1'
                        ? <div>
                            { shops.tips && 
                                <div className={noticeProps.show ? styles.fixedTop : 'hide'}>
                                    <Notice {...noticeProps} close={()=>this.closeTips()}/>
                                </div>
                            }
                            { shops.tips && 
                                <div className={noticeProps.show ? '' : 'hide'} style={{ height: '1.12rem' }}></div>
                            }
                            <div className={styles.title}>有{shops.count}家商家可以帮你办理</div>
                        </div>
                        : <div>
                            <HeaderNav {...headerNavProps} />
                            <div style={{ height: '.8rem' }}></div>
                            {addressListProps.show && <AddressList {...addressListProps} />}
                            {serverListProps.show && <ServerList {...serverListProps} />}
                        </div>
                }

                <LoadingMore {...loadingMoreProps}>
                    <div className={styles.shopList}>
                        {
                            shops.result.map((item, i) => <Shop key={`Shop-${i}`} handleClick={(props)=>this.clickShop(props)} {...shops.data[item]}/>)

                        }
                    </div>
                </LoadingMore>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    shops:state.shops,
})

const mapDispatchToProps = dispatch => ({
    merchantActions: bindActionCreators(merchantActions, dispatch),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ShopList);