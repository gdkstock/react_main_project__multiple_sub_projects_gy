/**
 * 商家服务
 */

import styles from './service.scss'

const Service = props => {

    let { name, detail, money, image, click, clickBtn ,typeCode} = props

    return (
        <div className={styles.box} onClick={typeCode=="220"?() => click(props):()=>{clickBtn(props)}}>
            <img src={image} />
            <div className={styles.rightText}>
                <h3 className='text-overflow-1'>{name}</h3>
                <p className='text-overflow-1'>{detail}</p>
                {
                    typeCode=="220"
                        ? <div className={styles.money}>¥{money}</div>
                        : <div
                            className={styles.btn}
                            onClick={(e) => {
                                e.stopPropagation();
                                e.preventDefault();
                                clickBtn(props);
                            }}>我要询价</div>
                }
            </div>
        </div>
    )
}

export default Service