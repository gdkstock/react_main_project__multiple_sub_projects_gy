import React from 'react'
import {Carousel} from 'antd-mobile'
import styles from './banner.scss'

const defaultProps ={
    bannerList:[
        {
            eventId:"12312312",
            imgUrl:"http://pic.jj20.com/up/allimg/311/042211230040/110422230040-3.jpg",
            jumpUrl:"http://www.baidu.com"
        },
        {
            eventId:"12312311",
            imgUrl:"http://img4.imgtn.bdimg.com/it/u=726236793,3712739561&fm=26&gp=0.jpg",
            jumpUrl:"http://www.baidu.com"
        }
    ],
    handleClick:()=>{},
}

const Banner=(props)=>{
    // props = defaultProps;
    const bannerList = props.bannerList || [];
    const canPlay = bannerList.length>1?true:false;
    return (
        <div className={styles.wrap}>
            {bannerList.length>0 &&
                <Carousel 
                    dots={false} 
                    autoplay={canPlay} 
                    infinite={canPlay} 
                    swiping={canPlay}
                >
                    {
                        bannerList.map((item,i)=>
                            <div key={i} onClick={()=>props.handleClick && props.handleClick(item)}>
                                <img 
                                    src={item.imgUrl}  
                                    className={styles.banner}
                                />
                            </div>
                        )
                    }
                </Carousel>
            } 
        </div>
    );
};

export default Banner