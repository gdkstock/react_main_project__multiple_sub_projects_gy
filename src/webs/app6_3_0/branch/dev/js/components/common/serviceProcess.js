/*
 *  养护类订单详情 服务须知信息
 */

import styles from './serviceProcess.scss'

const ServiceProcess = props => {
    return (
        <div className={styles.wrap}>
            <div className={styles.title}>服务流程</div>
            <div className={styles.content}><img src="./images/procedure-primary.png"/></div>
        </div>
    )
}

export default ServiceProcess