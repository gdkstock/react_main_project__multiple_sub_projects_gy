/*
 *  按钮
 */

import styles from './fullBtn.scss'

//默认属性
const defaultProps = {
    text:"咨询商家",
    handleClick:()=>{},
    isDisabled:false,
    type:"2",
}

const FootBar = props => {
    // props = defaultProps
    let { type , isDisabled } = props;
    let btnClass = isDisabled ? (styles.btn+" "+styles.btnDisabled):type=="1"?(styles.btn+" "+styles.btnActive):(styles.btn+" "+styles.btnConsult)
    return (
        <div className={styles.wrap} onClick={()=>props.handleClick()}>
            <div className={btnClass}>
                <span>{ props.text }</span>
            </div>
        </div>
    )
}

export default FootBar