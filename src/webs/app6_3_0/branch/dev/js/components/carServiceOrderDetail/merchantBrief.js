/*
 *  养护类订单详情 商家概要信息
 */

import styles from './merchantBrief.scss'

//默认属性
const defaultProps = {
    shopId:"",
    telephone:"",
    shopName:"广州车行易汽车保养旗舰店",
    shopAddress:"广州天河区中山大道南138号中山大道南138号",
    distance:"1.2",
    latitude:"",//纬度
    longitude:"",//经度
}

const MerchantBrief = props => {
    // props = defaultProps
    return (
        <div className={styles.wrap}>
            <div className={styles.merchantMsg} onClick={()=>props.location(props)}>
                <div className={styles.content}>
                    <div className={styles.merchantName+" text-overflow-1"}>{props.shopName}</div>
                    <div className={styles.address+" text-overflow-1"}>{props.shopAddress}</div>
                </div>
                <div className={styles.distance}></div>
            </div>
            <div className="hr"></div>
            <div className={styles.btnGroup}>
                <div className={styles.btn+" "+styles.phoneBtn} onClick={()=>{props.phone(props.telephone)}}>
                    <span>拨打电话</span>
                </div>
                <div className={styles.btn+" "+styles.IMBtn} onClick={()=>props.consult()}>
                    <span>咨询商家</span>
                </div>
            </div>
        </div>
    )
}

export default MerchantBrief