/*
 *  养护类订单详情 顶部服务概要信息
 */

import styles from './serviceBrief.scss'

//默认属性
const defaultProps = {
    productImg:"http://img1.imgtn.bdimg.com/it/u=4241880856,2481309876&fm=214&gp=0.jpg",//图片链接
    name:"洗车(精洗)套餐",//标题
    productDesc:"考虑到房价了的开发经理说的立刻发酵饲料快递费都开了房间数量的咖啡机的快乐发酵饲料快递费",//内容
    price:"300",//价格
    handleClick:()=>{},
}

const ServiceBrief = props => {
    props = Object.assign({},defaultProps,props)
    return (
        <div className={styles.wrap} onClick={props.handleClick}>
            <div className={styles.leftAvader}>
                <img src={props.productImg}/>
            </div>
            <div className={styles.content}>
                <div className={styles.title}>
                    <span className={styles.name+" text-overflow-1"}>{props.name}</span>
                    <span className={styles.price}>￥{props.price}</span>
                </div>
                <div className={styles.text+" text-overflow-1"}>{props.productDesc}</div>
            </div>
            <div className={styles.rightIcon}></div>
        </div>
    )
}

export default ServiceBrief