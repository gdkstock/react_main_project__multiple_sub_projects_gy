/*
 *  订单支付成功
 */

import styles from './index.scss'

//默认属性
const defaultProps = {

}

const showQRmask = ()=>{
    let QRmask = document.getElementById("QRmask");
    QRmask.style.display="block";
}

const hideQRmask = () =>{
    let QRmask = document.getElementById("QRmask");
    QRmask.style.display="none";
}

const PaySucc = props => {
    // props = defaultProps
    let { adList } = props
    return (
        <div className={styles.wrap}>
            <div className={styles.iconSucc}></div>
            <div className={styles.text}>支付成功</div>
            <div className={styles.content}>
                <div className={styles.serviceTitle}>
                    <span className="text-overflow-1">{props.name}</span>
                    <span onClick={()=>{props.orderDetail()}}>订单详情</span>
                </div>
                <div className={styles.noticeList}>
                    <p><i></i>若购买的服务套餐需要预约，请提前与商家联系；</p>
                    <p><i></i>请注意服务有效期，在有效期内使用改服务；</p>
                    <p><i></i>对服务有疑问，可联系商家或官方客服进行进行咨询；</p>
                </div>
            </div>
            <div className={styles.footBanner} onClick={()=>props.bannerClick(adList[0])}>
                { adList.length>0 && <img src={adList[0].imageUrl}/> }
            </div>
        </div>
    )
}

export default PaySucc
