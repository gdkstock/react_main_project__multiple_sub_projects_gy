/*
 *  地步导航
 */

import styles from './index.scss'

//默认属性
const defaultProps = {
    price:"",
    handleClick:()=>{}
}

const FootBar = props => {
    // props = defaultProps
    return (
        <div className={styles.footBarWrap}>
            合计￥{props.price}
            {false && <span>（已优惠￥0.0）</span>}
            <div className={ styles.btn } onClick={()=>{props.handleClick()}}>去支付</div>
        </div>
    )
}

export default FootBar