/*
 *  订单支付顶部信息
 */

import styles from './index.scss'

//默认属性
const defaultProps = {
    name:"解放螺丝扣搭街坊",
    productDesc:"广州车行易汽车保养旗舰店广州车行易汽车保养旗舰店广州车行易汽车保养旗舰店",
}

const OrderMsg = props => {
    //props = defaultProps
    return (
        <div className={styles.wrap}>
            <div className={styles.orderMsg}>
                <div className={styles.content}>
                    <div className={styles.serviceName+" text-overflow-1"}>{props.name}</div>
                    <div className={styles.merchantName+" text-overflow-1"}>{props.productDesc}</div>
                </div>
            </div>
        </div>
    )
}

export default OrderMsg