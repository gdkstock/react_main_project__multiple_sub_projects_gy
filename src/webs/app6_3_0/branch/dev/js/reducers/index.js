import { combineReducers } from 'redux'

import cars from './cars'
import shops from './shops'
import serviceList from './serviceList'

const rootReducer = combineReducers({
    cars,
    shops,
    serviceList,
});

export default rootReducer;
