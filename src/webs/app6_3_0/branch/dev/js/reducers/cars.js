import {
	ADD_CAR_LIST,
    ADD_CARS,
    ADD_CAR_VIOLATIONS,
} from '../actions/actionsTypes'

//测试
let initState = {
	data: {
		'2201':{
			"carId": "2201",
			"carNumber": "粤Y03Q28",
			"count": 26,
			"degree": 104,
			"money": 6050,
			"warnMsg": [
                "违章数达到4条及以上，提醒：本车未处理违章已超过4条，根据部分地区的法规，您的车辆上路将会被查扣，行驶证也将会被扣留，请您尽快处理！",
                "车辆最早违章时间距离当前时间超过半年，提醒：您有违章超过半年未处理，为避免产生滞纳金，或是影响车辆年检，请您尽快处理。"
            ]
		},
        "126":{
            "carId": "126",
            "carNumber": "粤Z369C港",
            "count": 0,
            "degree": 0,
            "money": 0,
            "warnMsg": []
        }
	},
	result: ['2201',"126"]
}

initState = {
	data: {},
	result: []
}

export default function cars(state = initState, action) {
	let _data = {}, _result = [], carId = '';
	switch (action.type) {
		case ADD_CAR_LIST:
        case ADD_CARS:
			//添加车辆列表
            action.data && action.data.map((item)=>{
                if(item.carId){
                    let carId = item.carId;
                    let temp = state.data[carId] || {};
                    _data[carId] = { ...temp,...item }
				    _result.push(carId)
                }
            })
			return {
                data:_data,
                result: _result
            }
        case ADD_CAR_VIOLATIONS:
            if(action.data.carId){
                let carId = action.data.carId;
                let temp = state.data[carId] || {};
                _data[carId] = { ...temp,...action.data }
                _result.push(carId)
            }
            return {
                data:{ ...state.data,..._data },
                result: Array.from(new Set([..._result, ...state.result]))
            }
		default:
			return state;
	}
}
