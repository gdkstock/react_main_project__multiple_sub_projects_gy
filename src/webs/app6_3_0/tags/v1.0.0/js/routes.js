import React from 'react'
import { Route, IndexRoute } from 'react-router'

import {
  App,
  Home,
  NotFoundPage,
} from './containers'

// import BangMangBan from './containers/bangmangban'
// import ShopList from './containers/shopList'
// import ShopDetail from './containers/ShopDetail'
// import CarServiceDetail from './containers/serviceDetail/carServiceDetail'
// import ViolationServiceDetail from './containers/serviceDetail/violationServiceDetail'

// import CarServiceOrderDetail from './containers/order/carServiceOrderDetail'
// import ViolationConsult from './containers/order/violationConsult'
// import ViolationOrderDetail from './containers/order/violationOrderDetail'
// import ViolationList from './containers/violation'
// import ViolationDetail from './containers/violation/detail'

// import OrderPay from './containers/order/orderPay'
// import PaySucc from './containers/order/paySuccess'

// import GMap from './containers/map'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />

    {/*按需加载*/}

    {/*帮忙办*/}
    <Route path="bangmangban" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/bangmangban').default)
        }, 'bangmangbanIndex')
    }} />

    {/*商家列表(1) or 附近商家(2)*/}
    <Route path="shopList/:type" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/shopList').default)
        }, 'shop')
    }} />

    {/* 店铺详情 */}
    <Route path="shopDetail/:merchantId/:shopId" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/ShopDetail').default)
        }, 'shop')
    }} />
    
    {/* 服务详情 */}
    <Route path="carServiceDetail/:merchantId/:shopId/:productId" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/serviceDetail/carServiceDetail').default)
        }, 'shop')
    }} />
    <Route path="violationServiceDetail/:merchantId/:shopId/:productId" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/serviceDetail/violationServiceDetail').default)
        }, 'shop')
    }} />

    {/*订单相关开始*/}
    <Route path="carService/orderDetail/:orderId" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/order/carServiceOrderDetail').default)
        }, 'order')
    }} />

    <Route path="violation">
      <Route path="list(/:carId)" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/violation').default)
        }, 'order')
      }} />
      <Route path="consult/:merchantId/:shopId/:productId" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/order/violationConsult').default)
        }, 'order')
      }} />
      <Route path="orderDetail/:orderId" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/order/violationOrderDetail').default)
        }, 'order')
      }} />
      <Route path="detail" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/violation/detail').default)
        }, 'order')
      }} />
    </Route>

    {/*订单支付*/}
    <Route path="orderPay/:merchantId/:shopId/:productId/:typeId(/:orderId)" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/order/orderPay').default)
        }, 'order')
      }} />

    {/*订单支付成功*/}
    <Route path="orderPay/result/:name/:orderId" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/order/paySuccess').default)
        }, 'order')
      }} />

    {/*订单相关结束*/}

    {/*地图*/}
    <Route path="map" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./containers/map').default)
        }, 'map')
      }} />

    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}
    <Route path="*" component={NotFoundPage} />
  </Route>
);