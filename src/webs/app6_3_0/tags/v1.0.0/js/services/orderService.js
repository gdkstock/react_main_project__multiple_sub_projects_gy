/**
 * 订单相关接口
 */

import apiHelper from './apiHelper';

class OrderService {

    /**
     * 生成订单接口
     * @param {*object} data
     */
    orderPay(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/pay`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 查看订单详情
     * @param {*object} data
     */
    getOrderDetail(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/detail`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 申请退款
     * @param {*object} data
     */
    refund(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/apply/refund`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 违章类订单支付
     * @param {*object} data
     */
    pay(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}pay/`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 违章类订单延迟验收
     * @param {*object} data
     */
    delay(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/delay/acceptance`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 违章类订单确认验收
     * @param {*object} data
     */
    check(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/confirm/acceptance`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 订单支付成功添加用户数据
     * @param {*object} data
     */
    addCUser(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}user/c/add`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 订单支付成功获取广告
     * @param {*object} data
     */
    getAd(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}advert/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


}

export default new OrderService()