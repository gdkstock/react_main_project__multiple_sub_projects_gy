/**
 * 违章相关接口
 */

import apiHelper from './apiHelper';

class ViolationService {

    /**
     * 违章列表
     * @param {*object} data
     */
    carViolations(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}car/violations`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }


}

export default new ViolationService()