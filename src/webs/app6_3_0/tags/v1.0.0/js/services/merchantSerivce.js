/**
 * 商家相关接口
 */

import apiHelper from './apiHelper';

class MerchantService {

    /**
     * 商家列表接口
     * @param {*object} data
     */
    getShopList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}shop/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
    

    /**
     * 商家详情接口
     * @param {*object} data
     */
    getShopDetail(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}shop/detail`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 可办理商家数量
     * @param {*object} data
     */
    getShopNum(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}shop/count`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 商家服务列表
     * @param {*object} data
     */
    getServiceList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}merchant/services`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取商家服务订单详情
     * @param {*object} data
     */
    getDetail(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}merchant/service/detail`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取城市列表
     * @param {*object} data
     */
    getCityList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}city/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取类型列表
     * @param {*object} data
     */
    getTypeLsit(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}merchant/types`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new MerchantService()