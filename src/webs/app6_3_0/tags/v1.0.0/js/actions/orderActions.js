/*
 * 订单相关
 */

import {
    CREATE_ORDER,
    GET_ORDER_DETAIL,
    CREATE_REFUND,
    ORDER_PAY,
    ORDER_CHECK,
    ORDER_DELAY,
    ADD_C_USER,
    GET_AD,
} from './actionsTypes'

//创建订单
export const createOrder = (param, callback) => ({ type: CREATE_ORDER, param, callback })

//查看订单
export const getOrderDetail = (param, callback) => ({ type: GET_ORDER_DETAIL, param, callback })

//申请退款
export const refund = (param, callback) => ({ type: CREATE_REFUND, param, callback })

//违章订单支付
export const pay = (param,callback) =>({type:ORDER_PAY,param,callback})

//违章订单验收
export const check = (param,callback) =>({type:ORDER_CHECK,param,callback})

//违章订单延迟验收
export const delay = (param,callback) =>({type:ORDER_DELAY,param,callback})

//订单支付成功，提交用户数据
export const addCUser = (param,callback) =>({type:ADD_C_USER,param,callback})

//订单支付成功，获取广告
export const getAd = (param,callback) =>({type:GET_AD,param,callback})
