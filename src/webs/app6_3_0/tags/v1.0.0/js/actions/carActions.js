/*
 * 车辆相关
 */

import {
    GET_CAR_LIST,
    GET_CARS,
} from './actionsTypes'

//请求车辆违章概要信息列表
export const getCarList = (param,callback) => ({ type: GET_CAR_LIST, param ,callback}) //获取车辆违章概要信息
export const getCars = (param,callback) =>({type:GET_CARS,param,callback}) //获取车辆列表

