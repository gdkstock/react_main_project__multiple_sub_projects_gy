/*
 * 商家相关
 */

import {
    GET_SERVICE_LIST,
    GET_SERVICE_DETAIL,
    GET_SHOP_LIST,
    GET_SHOP_DETAIL,
    GET_SHOP_NUM,
    GET_CITY_LIST,
    GET_TYPE_LIST,
} from './actionsTypes'

//请求商家店铺列表
export const getShopList = (param, callback) => ({ type: GET_SHOP_LIST, param, callback })

//请求商家店铺详情
export const getShopDetail = (param, callback) => ({ type: GET_SHOP_DETAIL, param, callback })

//请求商家数量
export const getShopNum = ( param ) => ({type:GET_SHOP_NUM,param})

//请求商家服务列表
export const getServiceList = (param,callback) => ({ type: GET_SERVICE_LIST, param ,callback})

//请求商家服务详情
export const getServiceDetail = param => ({ type: GET_SERVICE_DETAIL, param })

//请求城市列表
export const getCityList = (param,callback) => ({ type: GET_CITY_LIST, param , callback})

//请求服务类型列表
export const getTypeList = (param,callback) => ({ type: GET_TYPE_LIST, param , callback})
