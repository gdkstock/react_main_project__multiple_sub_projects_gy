/*
 *  订单支付
 */

import OrderMsg from './orderMsg'
import FootBar from './footBar'
import PayWay from './payWay'

import styles from './index.scss'

//默认属性
const defaultProps = {
    price:300,
    payType:"2",
    payTypeChange:()=>{},//支付方式变化
    payBtnClick:()=>{},//支付按钮点击
}

const OrderPayBasic = props => {
    // props = defaultProps
    return (
        <div className={styles.wrap}>
            <div className='whitespace-24'></div>
            <div className={styles.orderMoney}>订单金额<span>￥{props.price}</span></div>
            <div className='whitespace-24'></div>
            <PayWay 
                payType={props.payType}
                handleClick = {(p)=>props.payTypeChange(p)}
            />
            <FootBar 
                price = {props.price}
                handleClick={()=>props.payBtnClick()}
            />
        </div>
    )
}

export default OrderPayBasic
