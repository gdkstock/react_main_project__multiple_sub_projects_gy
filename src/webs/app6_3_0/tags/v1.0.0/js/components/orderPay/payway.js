/*
 *  订单支付方式
 */

import styles from './index.scss'

//默认属性
const defaultProps = {
    payType:"2", // 2支付宝 3微信
    handleClick:()=>{},
}

const PayWay = props => {
    // props = defaultProps
    let alipayCheckBoxClass = props.payType=="2" ? ( styles.seleteIcon+" "+styles.selected ) : ( styles.seleteIcon+" "+styles.defalut )
    let weixinCheckBoxClass = props.payType=="3" ? ( styles.seleteIcon+" "+styles.selected ) : ( styles.seleteIcon+" "+styles.defalut )
    return (
        <div className={styles.wrap}>
            <div className={styles.item}>
                <div className={styles.payway+" "+styles.weixin}>微信支付</div>
                <div className={weixinCheckBoxClass} onClick={()=>props.handleClick("3")}></div>
            </div>
            <div className="hr"></div>
            <div className={styles.item}>
                <div className={styles.payway+" "+styles.alipay}>支付宝支付</div>
                <div className={alipayCheckBoxClass} onClick={()=>props.handleClick("2")}></div>
            </div>
            <div className="hr"></div>
        </div>
    )
}

export default PayWay