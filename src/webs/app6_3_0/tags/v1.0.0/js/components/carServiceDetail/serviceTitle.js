/*
 *  养护类服务详情顶部title
 */

import styles from './serviceTitle.scss'

//默认属性
// const defaultProps = {
//     title:"洗车(精洗)套餐洗车(精洗)套餐洗车(精洗)套餐洗车(精洗)套餐洗车(精洗)套餐",//标题
//     labelList:["随时退","过期自动退"],
//     price:"300",//价格
// }

const ServiceBrief = props => {
    // props = defaultProps
    let { labelList } = props
    return (
        <div className={styles.wrap} onClick={props.handleClick}>
            <div className={styles.content+" abs-v-center"}>
                <div className={styles.title+" text-overflow-1"}>{props.title}</div>
                <div className={styles.text+" text-overflow-1"}>
                    {
                        labelList && labelList.map((item,i)=>
                            <span className={styles.label}>{item}</span>
                        )
                    }
                </div>
            </div>
            {props.price!=undefined && <div className={styles.rightText+" abs-v-center"}>￥{props.price}</div>}
        </div>
    )
}

export default ServiceBrief