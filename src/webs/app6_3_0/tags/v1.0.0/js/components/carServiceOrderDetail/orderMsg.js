/*
 *  养护类订单详情 订单信息
 */

import styles from './orderMsg.scss'

//默认属性
const defaultProps = {
    orderId:"231231231231231",
    payTypeName:"支付宝",
    payNo:"234231534234123412334534532452345234",
    payTime:"2017-11-12 12:30",
    orderAmount:300,
}

const OrderMsg = props => {
    // props = defaultProps
    return (
        <div className={styles.wrap}>
           <div>订单编号：{props.orderId}</div>
           <div>支付方式：{props.payTypeName}</div>
           <div>支付交易号：{props.payNo}</div>
           <div>支付时间：{props.payTime}</div>
           <div>交易金额：￥{props.orderAmount}</div>
        </div>
    )
}

export default OrderMsg