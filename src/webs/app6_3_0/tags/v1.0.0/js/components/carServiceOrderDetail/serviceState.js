/*
 *  养护类订单详情 订单二维码及状态信息
 */
import React,{ Component } from 'react'
import styles from './serviceState.scss'
import { Popup , Toast } from 'antd-mobile';
import common from '../../utils/common'

class PopContent extends Component{
    constructor(props) {
        super(props)
        this.state={...this.props}

    }

    refundReasonChange(key){
        console.log(key)
        this.setState({
            refundReasonKey:key,
        })
    }

    //申请退款
    refund(){
        let { orderId , refundReasonList , refundReasonKey } = this.state
        if(refundReasonList[refundReasonKey]){
        this.props.refund({
            orderId,
            remark:refundReasonList[refundReasonKey]
        },()=>{Popup.hide()})
        }else{
            Toast.info("请选择退款原因")
        }
    }

    render(){
        let { fine , orderId , refundReasonList , stateCode , refundReasonKey , payStatus , refundReason , applyRefundTime } = this.state
        let list =  refundReasonList
        let key = refundReasonKey
        console.log(fine)
        return(
            <div className={ styles.popWrap }>
                <div className={ styles.popTop }>
                    <div className={styles.closeBtn} onClick={()=>{Popup.hide()}}></div>
                    <div className={styles.title}>退款金额：<span className={styles.fine+" abs-v-center"}>￥{fine}</span></div>
                    <div className={styles.content}>退款费用将会按照您的支付方式，原路返回到您之前的支付账号里。到账日期跟您原来的支付方式有关（银行、信用卡通常需要2~7个工作日）。</div>
                </div>
                { stateCode=="2" && payStatus!="3" && <div className='hr'></div> }
                { (stateCode=="2" && payStatus!="3")?
                    <div className={ styles.popBody }>
                        <div>退款原因：</div>
                        {
                            list.map((item,i)=><div key={i}>{item}<i className={item==list[key]?styles.selected:styles.default} onClick={()=>{this.refundReasonChange(i)}}></i></div>)
                        }
                    </div>
                    :<div className={ styles.popBody }>
                        <p>退款原因：{refundReason}</p>
                        <p>退款申请时间：{ applyRefundTime }</p>
                    </div>
                }
                <div className={ styles.popFoot }>
                    <div className={ styles.popBtn } 
                        onClick={(stateCode=="2" && payStatus!="3") ?()=>{this.refund()}:()=>{Popup.hide()}}
                    >{(stateCode=="2"& payStatus!="3") ? "申请退款" : "确定"}</div>
                </div>
            </div>
        )
    }
}

//弹出层
const showPopup = ( props ) => {
    Popup.show(<PopContent {...props}/>,
        { 
            animationType: 'slide-up',
            maskClosable:false
        }
    )
    let { stateCode , payStatus } = props
    if(stateCode=="2" && payStatus!="3"){
        common.sendCxytj({
            eventId:"cxyapp_e_maintainorder_drawback"
        })
    }else{
        common.sendCxytj({
            eventId:"cxyapp_e_maintainorder_drawbackdetail"
        })
    }
}

class ServiceResult extends Component{
    constructor(props) {
        super(props);
        this.state={
            code:"",//券码
            stateCode:"1",//状态
            payStatus:"2",//退款状态
            stateList:{
                "2":"待消费",
                "3":"退款中",
                "4":"已退款",
                "8":"已成功消费"
            },//状态
            time:"", // 有效期
            btnTextList:{
                "2":"申请退款",
                "3":"查看详情",
                "4":"查看详情",
                "8":"再次购买" 
            },//按钮文案
            canbuy:true, // 是否能够再次购买
            orderId:"",//订单id
            fine:"",//订单金额
            refundReasonList:["预约不到时间","商家已经停止此服务","个人原因行程有变","不想要了","其他"],
            refundReasonKey:"-1",//退款原因
            refundReason:"",
            applyRefundTime:"",
            refundTime:"",//申请退款时间
            refund:()=>this.props.refund(),
            buyService:()=>this.props.buyService(),
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("nextProps",nextProps);
        this.setState({
            ...this.state,
            ...nextProps,
        })
    }

    refundReasonChange(key){
        console.log(key)
        console.log(this.state)
        this.setState({
           refundReasonKey:key, 
        })
    }

    render(){
        let { code , time , stateCode , payStatus , stateList , btnTextList , canbuy , buyService , fine } = this.state
        //状态文案class
        let stateTitleClass = ( stateCode == "2" || stateCode == "3" ) ? styles.titleLight : styles.titleDefalut;
        //状态文案内容
        let stateTitle = payStatus=="3"?stateList[payStatus]:stateList[stateCode]
        //券码文案class
        let codeClass = (stateCode == "2" && payStatus!="3") ? styles.code : (styles.code+" "+styles.codeDisabled)
        //二维码Class
        let qrClass = (stateCode == "2" && payStatus!="3") ? styles.QR : (styles.QR+" "+styles.hide)
        return (
            <div className={styles.wrap}>
                <div className={styles.codeWrap}>
                    <div className={qrClass} id="code"></div>
                    { (stateCode != "2" || payStatus =="3") && <div className={styles.QR}><img src='./images/icon_qrcodeDefault.png'/></div>}
                    <div className={codeClass}>券码：<span>{code}</span></div>
                </div>
                {<div className="hr"></div>}
                <div className={styles.state}>
                    <div className={styles.leftContent}>
                        <div className={ stateTitleClass }>{stateTitle}</div>
                        { time && stateCode == "2" && payStatus!=3 && <div className={styles.text+" text-overflow-1"}>有效期：{time}</div> }
                    </div>
                    { (canbuy || stateCode!="8") && (stateCode!="2" || fine*1!=0) && <div className={styles.btn+" abs-v-center"} onClick={stateCode!=8?()=>showPopup(this.state):()=>buyService()}>{payStatus=="3"?btnTextList[payStatus]:btnTextList[stateCode]}</div> }
                </div>
            </div>
        )
    }
}

export default ServiceResult