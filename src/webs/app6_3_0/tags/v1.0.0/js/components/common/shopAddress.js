/**
 * 店铺地址
 */

import styles from './shopAddress.scss'

const ShopAddress = props => {

    let { address, km, clickIcon } = props
    return (
        <div className={styles.box}>
            <div className={styles.content+" abs-v-center"}>
                <div className={styles.address+' text-overflow-one'}>{address}</div>
                <div className={styles.km}>{km}</div>
            </div>
            <div className={styles.icon} onClick={() => clickIcon(props)}></div>
        </div>
    )
}

export default ShopAddress;