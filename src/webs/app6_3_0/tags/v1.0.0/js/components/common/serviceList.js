/*
 *  养护类服务项目
 */

import styles from './serviceList.scss'

//默认属性
const defaultProps = {
    List:[
        {
            title:"车辆安全概况检查(免费)",
            desc:"1. 更换或维修过减震、转向等系统后，必须做四轮定位，对底盘系统各部件间的角度精密度进行调校，如果四轮定位不准确，会过度磨损车辆底盘系统。2. 如果车辆经常上马路牙子（特别是从前轮上）、经常快速过减速带的话，也需要注意。3. 如果车辆出现跑偏、转向精度变差、方向盘不正、“啃胎”严重等现象，在保证“动平衡”正常的前提下，也有可能是定位角度不准确造成的，则需要考虑做四轮定位。",
            serviceDescUrl:"",
        },
        {
            title:"室内除尘",
            desc:"1. 更换或维修过减震、转向等系统后，必须做四轮定位，对底盘系统各部件间的角度精密度进行调校，如果四轮定位不准确，会过度磨损车辆底盘系统。2. 如果车辆经常上马路牙子（特别是从前轮上）、经常快速过减速带的话，也需要注意。3. 如果车辆出现跑偏、转向精度变差、方向盘不正、“啃胎”严重等现象，在保证“动平衡”正常的前提下，也有可能是定位角度不准确造成的，则需要考虑做四轮定位。",
            serviceDescUrl:"",
        },
        {
            title:"外观清洗",
            desc:"1. 更换或维修过减震、转向等系统后，必须做四轮定位，对底盘系统各部件间的角度精密度进行调校，如果四轮定位不准确，会过度磨损车辆底盘系统。2. 如果车辆经常上马路牙子（特别是从前轮上）、经常快速过减速带的话，也需要注意。3. 如果车辆出现跑偏、转向精度变差、方向盘不正、“啃胎”严重等现象，在保证“动平衡”正常的前提下，也有可能是定位角度不准确造成的，则需要考虑做四轮定位。",
            serviceDescUrl:"",
        },
        {
            title:"室内消毒",
            desc:"1. 更换或维修过减震、转向等系统后，必须做四轮定位，对底盘系统各部件间的角度精密度进行调校，如果四轮定位不准确，会过度磨损车辆底盘系统。2. 如果车辆经常上马路牙子（特别是从前轮上）、经常快速过减速带的话，也需要注意。3. 如果车辆出现跑偏、转向精度变差、方向盘不正、“啃胎”严重等现象，在保证“动平衡”正常的前提下，也有可能是定位角度不准确造成的，则需要考虑做四轮定位。",
            serviceDescUrl:"",
        },
    ]
}

const ServiceList= props => {
    // props = defaultProps
    let { List } = props
    return (
        <div className={styles.wrap}>
            <div className={styles.title}>服务项目</div>
            {
                List && List.map((item,i)=>
                    <div className={styles.item}  onClick={()=>props.handleIconClick(item)}>
                        <i className="abs-v-center"></i>
                        {item.title}
                        <div className={styles.iconPrompt+" abs-v-center"}></div>
                    </div>
                )
            }
        </div>
    )
}

export default ServiceList