/**
 * 店铺名称 - 电话和营业时间
 */

import styles from './shopName.scss'

const ShopName = props => {

    let { name, time, phone, clickIcon } = props
    return (
        <div className={styles.box}>
            <div className={styles.content+" abs-v-center"}>
                <div className={styles.name+' text-overflow-1'}>{name}</div>
                <div className={styles.time}>{time}</div>
            </div>
            <div className={styles.icon} onClick={() => clickIcon(phone)}></div>
        </div>
    )
}

export default ShopName;