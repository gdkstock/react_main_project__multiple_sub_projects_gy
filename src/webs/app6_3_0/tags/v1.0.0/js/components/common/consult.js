/*
 *  商家咨询信息
 */

import styles from './consult.scss'

//默认属性
const defaultProps = {
    shopName:'',
    consult: p => console.log("点击了咨询按钮", p),
    phone: p => console.log("点击了拨打电话按钮", p),
    phoneNumber:"",
}

const Consult = props => {
    // props = Object.assign({}, defaultProps, props)
    return (
        <div className={styles.wrap}>
            <div className={styles.title + " text-overflow-1"}>{props.shopName}</div>
            <div className={styles.consult + " abs-v-center"} onClick={() => props.consult(props)}></div>
            <a className={styles.phone + " abs-v-center"} onClick={() => props.phone(props)}></a>
        </div>
    )
}

export default Consult