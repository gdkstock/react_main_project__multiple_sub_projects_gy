/**
 * 店铺名称和店铺地址
 */

import styles from './shopNameAndAddress.scss'

import { ShopName, ShopAddress } from './index'

const ShopNameAndAddress = props => {
    let { ShopNameProps, ShopAddressProps } = props

    return (
        <div className={styles.box}>
            <ShopName {...ShopNameProps} />
            <div className={styles.line}></div>
            <ShopAddress {...ShopAddressProps} />
        </div>
    )
}

export default ShopNameAndAddress;