/*
 *  顶部标题栏
 */
import styles from './TopBar.scss'

const defaultProps ={
    title:"商家详情",
    backBtnClick:()=>{},
    isShowTitle:false,
    type:"ios",
}

const TopBar=(props)=>{
    // props = defaultProps;
    let { type , isShowTitle , title} = props
    let statusBarClass = type == "ios" ? styles.iosStatusBar : styles.andriodStatusBar;
    let titleBarClass = type == "ios" ? styles.iosTitleBar : styles.andriodTitleBar;
    return (
        <div className={styles.wrap}>
            {!isShowTitle && <div className={styles.backBtn} onClick={()=>{props.backBtnClick()}}></div>}
            {isShowTitle && 
                <div>
                    <div className={statusBarClass}></div>
                    <div className={titleBarClass}>
                        <div className={styles.leftBackBtn} onClick={()=>{props.backBtnClick()}}></div>
                        <div className={styles.title}>{title}</div>
                        <div></div>
                    </div>
                </div>
            }
        </div>
    );
};

export default TopBar