/*
 *  养护类订单详情 服务须知信息
 */

import styles from './serviceNotice.scss'

//默认属性
const defaultProps = {
    title: "服务须知",
    content: "1. 您可以点击下方立即预约按钮进行预约登记；<br>2. 预约登记成功后您可以根据预约成功信息到店凭预约信息免排队优先享受该服务；<br>3. 需提前一天预约；<br>4. 有效期：2017/03/15－2017/05/15；<br>5. 该服务仅限本店使用。",//服务须知文案
}

const ServiceNotice = props => {
    props = Object.assign({}, defaultProps, props)
    return (
        <div className={styles.wrap}>
            <div className={styles.title}>{props.title}</div>
            <div className={styles.content} dangerouslySetInnerHTML={{ __html: props.content }}></div>
        </div>
    )
}

export default ServiceNotice