/**
 * 订单相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    CREATE_ORDER,
    GET_ORDER_DETAIL,
    CREATE_REFUND,
    ORDER_PAY,
    ORDER_CHECK,
    ORDER_DELAY,
    ADD_C_USER,
    GET_AD,
} from '../actions/actionsTypes'

//service
import OrderService from '../services/orderService'

import { ChMessage } from '../utils/message.config' //提示信息

import { Toast } from 'antd-mobile' //UI

import { normalize, schema } from 'normalizr'; //范式化库


//创建订单
function* watchCreateOrder() {
    yield takeLatest(CREATE_ORDER, createOrder)
}

function* createOrder(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.orderPay, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
    }
}

//查看订单详情
function* watchGetOrderDetail() {
    yield takeLatest(GET_ORDER_DETAIL, getOrderDetail)
}

function* getOrderDetail(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.getOrderDetail, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
        window.networkError('./images/networkError-icon.png');
    }
}

//申请退款
function* watchRefund() {
    yield takeLatest(CREATE_REFUND, refund)
}

function* refund(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.refund, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
    }
}

//违章订单支付
function* watchPay() {
    yield takeLatest(ORDER_PAY, pay)
}

function* pay(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.pay, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
    }
}

//违章订单验收
function* watchCheck() {
    yield takeLatest(ORDER_CHECK, check)
}

function* check(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.check, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
    }
}

//违章订单延迟验收
function* watchDelay() {
    yield takeLatest(ORDER_DELAY, orderDelay)
}

function* orderDelay(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.delay, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
    }
}

//订单支付成功，提交用户数据
function* watchAddCUser() {
    yield takeLatest(ADD_C_USER, addCUser)
}

function* addCUser(action) {

    // Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.addCUser, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				// Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
    }
}

//订单支付成功，获取广告
function* watchGetAd() {
    yield takeLatest(GET_AD, getAd)
}

function* getAd(action) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(OrderService.getAd, action.param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            if(result.code=="1000"){
				if (action.callback) {
                    action.callback(result)
                }
			}else{
				Toast.info(result.msg);
			}
        }
    } catch (error) {
        Toast.hide()
    }
}


export function* watchOrderFetch() {
    yield [
        fork(watchCreateOrder),
        fork(watchGetOrderDetail),
        fork(watchRefund),
        fork(watchPay),
        fork(watchCheck),
        fork(watchDelay),
        fork(watchAddCUser),
        fork(watchGetAd),
    ]
}