/**
 * 商家相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_CAR_VIOLATIONS,
    ADD_CAR_VIOLATIONS,
} from '../actions/actionsTypes'

//service
import violationService from '../services/violationService'

import { ChMessage } from '../utils/message.config' //提示信息

import { Toast } from 'antd-mobile' //UI

import { normalize, schema } from 'normalizr'; //范式化库


//获取店铺详情
function* getCarViolations(param,callback) {

    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(violationService.carViolations, param),
            timeout: call(delay, 30000)
        })
        Toast.hide()
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            //这里可以做其他 yield put操作
            // alert(JSON.stringify(result))
            if(result.code=="1000"){
                // yield put({type:ADD_CAR_VIOLATIONS,data:{...result.data,carId:param.carId}})
            }
            if (callback) {
                callback(result)
            }
        }
    } catch (error) {
        Toast.hide()
    }
}

function* watchGetCarViolations() {
    // yield take(GET_CAR_VIOLATIONS, getCarViolations)
    while(true){
      let { param , callback} = yield take(GET_CAR_VIOLATIONS)
      yield fork(getCarViolations, param , callback)
    }
}

export function* watchViolationFetch() {
    yield [
        fork(watchGetCarViolations),
    ]
}