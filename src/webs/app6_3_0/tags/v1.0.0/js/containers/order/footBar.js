/*
 *  底部发起询价
 */

import styles from './footBar.scss'

//默认属性
const defaultProps = {
    onClick: () => console.log("点击了询价按钮"),
    btnText: '发起询价',
    time: '30',
    status:"-1",//订单状态报价完成
    timer:{
        h:"",
        m:"",
        s:"",
    }
}

const FootBar = props => {
    props = Object.assign({}, defaultProps, props)
    let { onClick, btnText, time ,status ,timer} = props;
    let speedClass = time>=20?(styles.speed+" "+styles.level2):styles.speed;
    return (
        <div className={styles.wrap}>
            <div className={styles.leftContent}>
            {
                (status==-1 || status==1) &&
                <div className="abs-center">
                    <div>{status=="1"?"待付款剩余时间":"此商家平均响应时间"}</div>
                    {
                        status == "-1" && 
                        <div>
                            <span className={styles.time}>{time}分钟</span>
                                <span className={speedClass}>
                                    <span>(速度</span>
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                    <span>)</span>
                                </span>
                        </div>
                    }
                    {
                        status == "1" && 
                        <div>
                            <span className={styles.time}>{`${timer.h || 0 }小时${timer.m || 0}分钟${timer.s || 0}秒`}</span>
                        </div>
                    } 
                </div>
            }
            </div>
            <div className={styles.rightBtn} onClick={() => onClick(props)}>
                <span>{status=="1"?"立即付款":"发起询价"}</span>
            </div>
        </div>
    )
}

export default FootBar