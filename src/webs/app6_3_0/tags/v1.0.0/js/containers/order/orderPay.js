/**
 * 订单支付
 * Created by lijun on 2017/7/10.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as orderActions from '../../actions/orderActions'
import common from '../../utils/common'
import config from '../../config'

//基础组件
import OrderPayBasic from '../../components/orderPay'
import OrderMsg from '../../components/orderPay/orderMsg'
import { Procedure , ViolationBrief , Consult } from '../../components/common'

import { Toast } from 'antd-mobile'

import userHelper from '../../utils/userHelper'

import jsApi from '../../utils/cx580.jsApi'

class OrderPay extends Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			"merchantId":this.props.params.merchantId,
			"shopId":this.props.params.shopId,
			"productId":this.props.params.productId,
			"typeId":this.props.params.typeId,
			"payType":"2",//1银联，2支付宝，3微信
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('订单支付');

		//判断是否需要返回
        if(common.isBack()){
            window.history.back()
        }else{
			this.initData()
		}

		window.orderPayResume = () =>{
			if(sessionStorage.getItem("isFromLogin")){
				sessionStorage.removeItem("isFromLogin")
				//判断是否需要返回
				if(common.isBack()){
					window.history.back()
				}else{
					this.initData()
				}
			}
		}

		//在app的时候隐藏头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("show")
        }

	}

	initData(){
		if(this.state.typeId=="220"){
			let order = JSON.parse(sessionStorage.getItem("carServiceOrder"))
			if(order){
				this.setState({
					name:order.name,
					productDesc:order.productDesc,
					price:order.price,
				})
			}
			// sessionStorage.removeItem("carServiceOrder")
		}else{
			let order = JSON.parse(sessionStorage.getItem("violationServiceOrder"))
			if(order){
				let { shop , orderAmount , violation } = order
				this.setState({ shop , price:orderAmount , violation })
			}
			// sessionStorage.removeItem("violationServiceOrder")
		}
	}

	componentWillReceiveProps(nextProps){
		//组件props更新
	}

	componentDidMount(){
		//进入组件埋点
		// common.sendCxytj({
		// 	eventId:"Violation_EnterCarDetails"
		// })
	}

	componentWillUnmount() {
		//离开记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title);
		window.orderPayResume = null;
    }

	//打电话
	phoneCall(props){
		if(props.phoneNumber){
			window.location.href="tel:"+props.phoneNumber
		}
	}

	//调起IM
	openIM(){
		let { shop } = this.state;
		let merchantMsg = {};
		if(shop.users){
			let user = shop.users[0]
			merchantMsg = {
				merchantUserId : user.userId, 
				merchantUserName : user.name, 
				merchantUserImage : user.photo, 
				topic:"wzbl",
			}
			userHelper.openIM(merchantMsg)
		}
	}
	
	//跳转到违章详情
	jumpToViolationDeail(){
		sessionStorage.setItem("checkViolationDetail",JSON.stringify(this.state.violation))
		this.props.router.push("violation/detail")
	}

	//测试
	submitOrder(){
		let { typeId , orderId , productId} = this.props.params
		let { name } = this.state
		if(typeId=="220"){
			let params = {
				merchantId:this.state.merchantId,
				shopId:this.state.shopId,
				productId:this.state.productId,
				typeId:this.state.typeId,
			}
			this.props.orderActions.createOrder(params,(result)=>{
				//掉jsdk支付
				if(common.isCXYApp()){
					let { orderAmt , orderId , orderType , token } = result.data;
					let payType = orderAmt*1==0?"17":this.state.payType;
					userHelper.orderPay({
						orderAmt,
						orderId,
						orderType,
						token,
						payType,
					},(data)=>{
						if(data.data.tradeStatus=="1"){
							let orderId = data.data.orderNo 
							//保存用户数据
							this.props.orderActions.addCUser({orderId})
							//跳转到支付成功
							window.location.replace(common.getRootUrl()+"orderPay/result/"+name+"/"+orderId)
						}
					})
				}
			})
			common.sendCxytj({
				eventId: "cxyapp_e_nearby_maintainserver_ordersubmit",
			})
		}else{
			let params = {
				orderId,
			}
			this.props.orderActions.pay(params,(result)=>{
				// alert(JSON.stringify(result))
				//掉jsdk支付
				if(common.isCXYApp()){
					let { orderAmt , orderId , orderType , token } = result.data
					userHelper.orderPay({
						orderAmt,
						orderId,
						orderType,
						token,
						payType:this.state.payType,
					},(data)=>{
						if(data.data.tradeStatus=="1"){
							let orderId = data.data.orderNo 
							//保存用户数据
							this.props.orderActions.addCUser({orderId})
							//违章类订单支付成功后退到订单详情页
							window.history.back()
						}
					})
				}
			})
			common.sendCxytj({
				eventId: "cxyapp_e_nearby_agentwz_paysubmit",
			})
		}
	}

	//支付方式变化
	payTypeChange(payType){
		console.log(payType)
		this.setState({
			payType,
		})
	}

	createViolationBriefProps(violation){
		if(violation){
			let { carNumber , count , degree , money } = violation
			return {
				title:`${carNumber.substr(0,2)} ${carNumber.substr(2)}车辆违章代办服务`,
				detail:`共办理${count}条，扣${degree}分，罚款${money}元`,
				onClick:()=>this.jumpToViolationDeail(),
			}
		}
	}

	render(){
		let { shop , violation } = this.state
		let ViolationBriefProps = this.createViolationBriefProps(violation);
		return (
			<div>
				{
					this.state.typeId=="210" && 	
					<div>
						<Procedure
							status="pay"
						/>
						<Consult 
							shopName={shop?shop.shopName:""}
							phoneNumber={shop?shop.telephone:""}
							consult = { ()=>{this.openIM()}}
							phone = {(p)=>this.phoneCall(p)}
						/>
						<div className="hr"></div>
						<ViolationBrief {...ViolationBriefProps}/>
					</div>
				}
                {
					this.state.typeId=="220" && 
					<OrderMsg 
						name={this.state.name}
						productDesc = { this.state.productDesc }
					/>
				}
				<OrderPayBasic 
					price={this.state.price}
					payType={this.state.payType}
					payBtnClick={()=>this.submitOrder()}
					payTypeChange={(p)=>this.payTypeChange(p)}
				/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	//state:state //容器组件props
	serviceList:state.serviceList,
	shops:state.shops,
})

const mapDispatchToProps = dispatch => ({
	orderActions: bindActionCreators(orderActions, dispatch) //容器组件action
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(OrderPay)