/**
 * 违章列表
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Toast } from 'antd-mobile'

//样式
import styles from './index.scss'; // css

//组件
import { Card, EmptyViolation } from '../../components/violation'

//actions
import * as violationActions from '../../actions/violationActions'
import * as carActions from '../../actions/carActions'

//常用工具类
import cx580JsApi from '../../utils/cx580.jsApi';
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'


class ViolationDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type:"tall", //UI展示类型 narrow 窄 tall 高
            carNumber:"", //车辆列表数据
            untreatedViolation: 0, //勾选违章条数
            degree: 0, //勾选违章的总扣分
            fine: 0, //勾选违章的总金额
            all: {
                untreatedViolation: 0, //总违章条数
                degree: 0, //总违章的总扣分
                fine: 0, //总违章的总金额
            },
            violations: [],
            violationIds: [],
            checkeds: [], //已选违章ID列表
            checkAlled: false, //全选
            total: {
                totalNum: 0, //可办理违章总数
                totalDegree: 0, //可办理总扣分
                totalFine: 0, //可办理总罚款
                checkedNum: 0, //勾选违章总数
                checkedDegree: 0, //勾选总扣分
                checkedFine: 0, //勾选总罚款
            },
        };
    }

    componentWillMount() {
        common.setViewTitle("查看违章");
        if(sessionStorage.getItem("checkViolationDetail")){
            let violation = JSON.parse(sessionStorage.getItem("checkViolationDetail")); 
            let state = this.dataInit(violation.violations)
            this.setState({
                carNumber:violation.carNumber,
                ...state
            })
        }   
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', window.violationListScroll)
    }

    componentDidMount() {
        window.violationListScroll = ()=>this.handleScroll()
        window.addEventListener('scroll', window.violationListScroll)
    }

    componentWillReceiveProps(nextProps){
        
    }

    //监听body滚动
    handleScroll(){
        let top = document.body.scrollTop;
        let type = top>100?"narrow":"tall";
        let preType = this.state.type;
        if(preType!=type){
            this.setState({
                type,
            })
        }
    }

    componentDidUpdate() {
        //console.log("componentDidUpdate");
    }

    /**
     * 数据初始化
     */
    dataInit(violations) {
        let violationIds = [];
        let checkAlled = true; //全选
        let total = {
            totalNum: 0, //可办理违章总数
            totalDegree: 0, //可办理总扣分
            totalFine: 0, //可办理总罚款
            checkedNum: 0, //勾选违章总数
            checkedDegree: 0, //勾选总扣分
            checkedFine: 0, //勾选总罚款
        }
        violations.map(violation => {
            let { violationId, degree, fine } = violation
            violationIds.push(violationId) //违章ID列表
            total.totalNum++;
            total.totalDegree += degree * 1
            total.totalFine += fine * 1

            total.checkedNum = total.totalNum
            total.checkedDegree = total.totalDegree
            total.checkedFine = total.totalFine
        })
        return {
            violations: violations,
            violationIds: violationIds,
            checkeds: violationIds, //默认全选
            checkAlled: checkAlled,
            total: total
        };
    }

    render() {
        let { violations, checkeds, total, checkAlled , type , carNumber} = this.state
        let cardProps = {
            clickCheckbox: () => false, //点击了勾选框
            clickRight: () => false, //点击了右边区域
            clickFooterText: () => false, //点击了补充资料区域
        }

        return (
            <div>
                {/*车辆信息 start*/}
                <div>
                    <div className={styles.carInfo}></div>
                    <div className='hr'></div>
                    <div className={styles.violation}></div>
                </div>
                <div className={styles.carInfoWrap}>
                    <div className={styles.carInfo}>
                        <div>{carNumber.substr(0,2)+" "+carNumber.substr(2)}</div>
                    </div>
                    <div className='hr'></div>
                    {type=="tall" &&
                        <div className={styles.violation}>
                            <div>
                                <h3>违章</h3>
                                <p>{total.checkedNum}</p>
                            </div>
                            <div>
                                <h3>罚款</h3>
                                <p>{total.checkedFine}</p>
                            </div>
                            <div>
                                <h3>扣分</h3>
                                <p>{total.checkedDegree}</p>
                            </div>
                        </div>
                    }
                    {type=="narrow" &&
                        <div className={styles.violationNarrow}>
                            <div>
                                <span>已选&nbsp;{total.checkedNum}&nbsp;条</span>
                                <span>罚款&nbsp;{total.checkedFine}&nbsp;元</span>
                                <span>扣分&nbsp;{total.checkedDegree}&nbsp;分</span>
                            </div>
                        </div>
                    }
                </div>
                {/*车辆信息 end*/}
                <div className="whitespace-24"></div>
                {/*违章列表 start*/}
                {violations.map((item, i) => <div key={item.violationId}>
                    <Card 
                        {...item}
                        clickCheckbox={data => this.checkViolation(data)}
                        checked={checkeds.indexOf(item.violationId) > -1}
                        isCheckboxShow = {false}
                    />
                    <div className="whitespace-24"></div>
                </div>)}
                {/*违章列表 end*/}
            </div>
        )
    }
}

ViolationDetail.defaultProps = {
    loading: true,
    error: undefined,
    datas: {
        carNumber: "",
        untreatedViolation: 0,
        degree: 0,
        fine: 0,
        violations: []

    }
}

const mapStateToProps = state => ({
    // cars:state.cars,
})

const mapDispatchToProps = dispatch => ({
    // violationActions: bindActionCreators(violationActions, dispatch),
    // carActions: bindActionCreators(carActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViolationDetail)