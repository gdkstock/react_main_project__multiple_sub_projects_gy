/**
 * 店铺详情
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//基础组件
import { Banner, ShopNameAndAddress, Services, FullBtn , TopBar } from '../../components/common'

//通用公共类
import common from '../../utils/common'
import * as merchantActions from '../../actions/merchantActions'
import userHelper from '../../utils/userHelper'
import jsApi from '../../utils/cx580.jsApi'

class ShopDetail extends Component {
    constructor(props) {
        super(props);
        this.state={
            shopDetail:{},
            services:[],
            isShowTitle:false,
        }
    }

    componentWillMount() {
        common.setViewTitle("店铺详情")

        let { merchantId , shopId } = this.props.params
        //在app的时候隐藏头部标题
        if(common.isCXYApp()){
            userHelper.controlTitle("hide")
            try{
                jsApi.call({
                    "commandId": "",
                    "command": "getSymbol",
                    "data": {
                        "city": "",
                        "lng": "",
                        "lat": "",
                        "cityCode":"",
                    }
                },(result)=>{
                    // alert(JSON.stringify(result))
                    let data = result.data;
                    let longitude = data.lng;
                    let latitude = data.lat;
                    this.props.merchantActions.getShopDetail({shopId,longitude,latitude});
                })
            }catch(e){
                // alert("chucuo")
            }
        }else{
            //请求店铺详情数据
            this.props.merchantActions.getShopDetail({shopId})
        }

        //进入首页缓存首页hash路由
        if(!sessionStorage.getItem("indexHash")){
            let hash = window.location.hash
            sessionStorage.setItem("indexHash",hash)
        }

        //请求店铺服务列表
        this.props.merchantActions.getServiceList({merchantId,shopId},(result)=>{
            console.log(result)
            this.setState({
                services:result.data
            })
        }) 

        window.shopDetailResume=()=>{
            //在app的时候隐藏头部标题
            if(common.isCXYApp()){
                userHelper.controlTitle("hide")
            }
        }

        //判断是否需要返回
        if(common.isBack()){
            window.history.back()
        }else{
            
        }
    }

    componentDidMount() {
        window.titleShowScroll = ()=>this.controlTitleLayout()
        window.addEventListener("scroll",window.titleShowScroll)
        common.sendCxytj({
            eventId: "cxyapp_p_nearby_shopdetail"
        })
    }

    componentWillReceiveProps(nextProps) {
        let { merchantId , shopId } = this.props.params;
        let { shops } = nextProps;
        if(shops.data[shopId]){
            this.setState({
                shopDetail:shops.data[shopId]
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener("scroll",window.titleShowScroll)
        window.shopDetailResume = null
        //离开页面记录页面信
		sessionStorage.setItem("prevPageInfo", document.title)
    }

    //控制title显示隐藏
    controlTitleLayout(){
        let top = document.body.scrollTop;
        let preIsShowTitle = this.state.isShowTitle;
        let isShowTitle = top>100;
        if(isShowTitle !=preIsShowTitle){
            this.setState({
                isShowTitle
            })
        }
    }

    //打开IM
    openIM(){
        let { shopDetail } = this.state
        console.log(shopDetail)
        let [ merchantUserId , merchantUserName , merchantUserImage ] = ["","",""];
        if(shopDetail.users){
            let user = shopDetail.users[0]
            merchantUserId = user.userId;
            merchantUserName = user.name;
            merchantUserImage = user.photo;
        }
        userHelper.openIM({
            merchantUserId,
            merchantUserName,
            merchantUserImage,
            topic:"",
        })
         //统计
        common.sendCxytj({
            eventId:"cxyapp_e_nearby_shop_chat",
            eventPropertis: {
                merchantUserId,
                merchantUserName,
                merchantUserImage,
            }
        })
    }

    //定位
	location(props){
		let { address , longitude , latitude } = props;
        if(common.isCXYApp()){
            let url = common.getRootUrl()+`map?address=${address}&longitude=${longitude}&latitude=${latitude}`
            userHelper.openNewBrowser(encodeURI(url))  
            // this.props.router.push(`map?address=${address}&longitude=${longitude}&latitude=${latitude}`);
        }else{
            this.props.router.push(`map?address=${address}&longitude=${longitude}&latitude=${latitude}`);
        }
         //统计
        common.sendCxytj({
            eventId:"cxyapp_e_nearby_shop_map",
        })
	}

    //跳转到服务详情
    jumpToCarServiceDetail(item){
        console.log(item)
        let { merchantId , shopId } = this.props.params
        this.props.router.push("carServiceDetail/"+merchantId+"/"+shopId+"/"+item.productId);
        //统计
        common.sendCxytj({
            eventId:"cxyapp_e_nearby_shop_maintainserver",
        })
    }

    jumpToViolationServiceDetail(item){
        console.log(item)
        let { merchantId , shopId } = this.props.params
        this.props.router.push("violationServiceDetail/"+merchantId+"/"+shopId+"/"+item.productId);
        //统计
        common.sendCxytj({
            eventId:"cxyapp_e_nearby_shop_agentserver",
        })
    }

    createBannerProps(state){
        let { shopImageInfo } = state.shopDetail
        let bannerList = [];
        if(shopImageInfo){
            bannerList = shopImageInfo.map((item , i) =>{
                return {
                    eventId:i,
                    imgUrl:item.imageUrl,
                }
            })
        }
        return {
            bannerList,
        }
    }

    //后退
    back(){
        let indexHash = sessionStorage.getItem("indexHash")
        if(indexHash.indexOf("#/shopDetail")>-1){
             userHelper.closeAppView();
        }else{
            window.history.back(); 
        }
    }

    //拨打电话
    telephoneCall(phone){
        window.location.href=`tel:${phone}`
        let { merchantId , shopId } = this.props.params
        //统计
        common.sendCxytj({
            eventId:"cxyapp_e_nearby_shop_call",
            eventPropertis: {
                merchantId,
                shopId,
            }
        })
    }

    render() {
        let { shopDetail , isShowTitle} = this.state
        let bannerProps = this.createBannerProps(this.state)

        let ShopNameProps = {
            name: shopDetail.shopName,
            time: `${shopDetail.openTime || ""}-${shopDetail.closeTime || ""}`,
            phone: shopDetail.telephone,
            clickIcon: (phone) => this.telephoneCall(phone)
        }

        let ShopAddressProps = {
            address: shopDetail.shopAddress,
            latitude:shopDetail.latitude,
            longitude:shopDetail.longitude,
            km: shopDetail.distance?(shopDetail.distance+'km'):"0km",
            clickIcon: (props) => this.location(props),
        }

        //商家名称和地址的props
        let ShopNameAndAddressProps = {
            ShopNameProps,
            ShopAddressProps
        }

        //商家服务的props
        let ServicesProps = {
            titel: '商家服务',
            list: this.state.services.map(item => {
                return {
                    productId:item.productId,
                    name: item.name,
                    detail: item.description?item.description.join(";"):"",
                    money: item.price,
                    image: item.productImage.length>0 && item.productImage[0].smallImageUrl,
                    typeCode:item.pTypeCode,
                    click: () => this.jumpToCarServiceDetail(item),
                    clickBtn: () => this.jumpToViolationServiceDetail(item), 
                }
            }) //模拟三条数据
        }

        //title栏
        let type = common.isIPhone()?"ios":"andriod";
        return (
            <div className='has-footBar'>
                <TopBar 
                    backBtnClick={()=>this.back()}
                    title="店铺详情"
                    isShowTitle={isShowTitle}
                    type={type}
                />
                <Banner {...bannerProps} />
                <ShopNameAndAddress {...ShopNameAndAddressProps} />
                { this.state.services.length>0 && <div className='h24'></div>}
                { this.state.services.length>0 && <Services {...ServicesProps} /> }
                <div className="h80"></div>
                <FullBtn
                    text="联系商家"
                    handleClick={() => this.openIM()}
                    isDisabled = {false}
                    type="2"
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    shops:state.shops,
})

const mapDispatchToProps = dispatch => ({
    merchantActions: bindActionCreators(merchantActions, dispatch) ,
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ShopDetail);