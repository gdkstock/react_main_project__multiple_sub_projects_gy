/**
 * 代办类服务详情
 * Created by lijun on 2017/7/17.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as MerchantActions from '../../actions/merchantActions'

//ceshi 
import * as ShopActions from '../../actions/merchantActions'

import common from '../../utils/common'
import config from '../../config'
import userHelper from '../../utils/userHelper'

//基础组件
import { ConsultViolation , ServiceList , ServiceNotice , Banner , ServiceProcess ,TopBar , FullBtn} from '../../components/common'
import  FootBar  from '../order/footBar'

import { Toast } from 'antd-mobile'
import jsApi from '../../utils/cx580.jsApi'

class ViolationServiceDetail extends Component{
	constructor(props){
		super(props);
		//设置初始状态

		this.state = {
			"merchantId":this.props.params.merchantId,
			"shopId":this.props.params.shopId,
			"productId":this.props.params.productId,
			"_typeId":"210",	
			"name": "",
			"productDesc": "",
			"shopName": "",
			"phone": "",
			"price": 15,
			"status": "1",
			"responseTime":"0",
			"serviceInfo": [
			
			],
			"productImageInfo": [
				
			]
		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('服务详情');

		//判断是否需要返回
        if(common.isBack()){
            window.history.back()
        }else{
			//在app的时候隐藏头部标题
			if(common.isCXYApp()){
				userHelper.controlTitle("hide")
			}
		}

		console.log(this.props)
		let { shopId , productId } = this.props.params;
		let { shops , serviceList } = this.props;

		//请求服务详情数据  AA13FD960F0A49E5A188DC04C64F4804（养护） / B9F94C9AA63E457CAD0CCF25F3A0848B(代办)
		this.props.merchantActions.getServiceDetail({productId})

		window.violationResume=()=>{
			//在app的时候隐藏头部标题
			if(common.isCXYApp()){
				userHelper.controlTitle("hide")
			}
		}

	}

	componentWillReceiveProps(nextProps){
		//组件props更新
		let { shopId , productId } = this.props.params;
		let { serviceList } = nextProps
		this.setState({
			...this.state,
			...serviceList.data[productId],
		})
	}

	componentDidMount(){
		//进入组件埋点
		common.sendCxytj({
			eventId:"cxyapp_p_nearby_agentwz"
		})
		window.titleShowScroll = ()=>this.controlTitleLayout()
        window.addEventListener("scroll",window.titleShowScroll)
	}

	componentWillUnmount() {
		//离开记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
		window.removeEventListener("scroll",window.titleShowScroll)
		window.violationResume = null
    }

	//控制title显示隐藏
    controlTitleLayout(){
        let top = document.body.scrollTop;
        let preIsShowTitle = this.state.isShowTitle;
        let isShowTitle = top>100;
        if(isShowTitle !=preIsShowTitle){
            this.setState({
                isShowTitle
            })
        }
    }

	//生成图片列表数据
	createBannerListProps(data){
		return data.map((item)=>{
			return {
				imgUrl:item.imageUrl
			}
		})
	}

	//打开IM
	openIM(){
		let { shopId } = this.state,
			{ shops } = this.props,
			merchantMsg={};
		if(shops.data[shopId]){
			let user = shops.data[shopId].users[0]
			merchantMsg = {
				merchantUserId : user.userId, 
				merchantUserName : user.name, 
				merchantUserImage : user.photo, 
				topic:"wzbl",
			}
			userHelper.openIM(merchantMsg)
		}
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_agentwz_chat"
		})
	}

	//跳转到询价页面
	jumptoConsult(){
		let { merchantId , shopId , productId , _typeId } = this.state
		this.props.router.push('violation/consult/'+merchantId+"/"+shopId+"/"+productId)
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_agentwz_askprice"
		})
	}

	createServiceNoticeProps(state){
		let { productDesc } = state;
		console.log(productDesc)
		let content = productDesc?productDesc.join("<br>"):"";
		return {
			title:"服务须知",
			content,
		}
	}

	back(){
		window.history.back();
	}

	createNotice(state){
		// let { serviceInfo } = this.state;
		// let content = "";
		// if(Object.prototype.toString.call(serviceInfo)=="[object Array]"){
		// 	serviceInfo.map((item,i)=>{
		// 		// alert("1111")
		// 		content+=`${item.serviceTitle}<br>`
		// 	})
		// }
		// return {
		// 	title : "服务项目",
		// 	content,
		// }
		let { serviceInfo } = state
		let List = []
		if(serviceInfo){
			List = serviceInfo.map((item)=>{
				return {
					title:item.serviceTitle,
					desc:item.serviceDesc,
					serviceDescUrl:item.serviceDescUrl,
				}
			})
		}
		return {
			List,
			handleIconClick:(item)=>this.promptIconClick(item)
		}
	}

	//i图标点击事件
	promptIconClick(item){
		if(item.serviceDescUrl){
			if(common.isCXYApp()){
				userHelper.openNewBrowser(item.serviceDescUrl)
			}else{
				window.location.href = item.serviceDescUrl
			}
		}
	}

	telephoneCall(props){
		window.location.href="tel:"+props.phoneNumber
		common.sendCxytj({
			eventId:"cxyapp_e_nearby_agentwz_call"
		})
	}

	render(){

		console.log('______________________',this.state)
		//顶部服务详情图片数据
		let bannerList = this.createBannerListProps(this.state.productImageInfo);
		let ServiceNoticeProps = this.createServiceNoticeProps(this.state)
		let notice = this.createNotice(this.state)
		let { isShowTitle } = this.state
        let type = common.isIPhone()?"ios":"andriod";
		return (
			<div className="has-footBar">
				<TopBar 
					backBtnClick={()=>this.back()}
					title="服务详情"
                    isShowTitle={isShowTitle}
                    type={type}
				/>
				<Banner bannerList={bannerList}/>
				<ConsultViolation
					name={this.state.name}
					shopName={this.state.shopName}
					consult={()=>this.openIM()}
					phone={(p)=>this.telephoneCall(p)}
					phoneNumber={this.state.phone}
				/>
                <div className="whitespace-24"></div>
				<ServiceList {...notice}/>
                <div className="whitespace-24"></div>
                <ServiceProcess/>
                <div className="whitespace-24"></div>
				<ServiceNotice {...ServiceNoticeProps}/>
				<div className="h56"></div>
				<FullBtn
                    text="我要询价"
                    handleClick={() =>this.jumptoConsult()}
                    isDisabled = {false}
                    type="1"
                />
			</div>
		)
	}
}

const mapStateToProps = state => ({
	//state:state //容器组件props
	serviceList:state.serviceList,
	shops:state.shops,
})

const mapDispatchToProps = dispatch => ({
	merchantActions: bindActionCreators(MerchantActions, dispatch),
	shopActions: bindActionCreators(ShopActions, dispatch) 
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ViolationServiceDetail)