import {
	ADD_SERVICE_DETAIL,
} from '../actions/actionsTypes'

//测试
let initState = {
	data: {
		'AA13FD960F0A49E5A188DC04C64F4804':   {
			"productId": "AA13FD960F0A49E5A188DC04C64F4804",
			"name": "第一家洗车",
			"productDesc": "第一家洗车",
			"shopName": null,
			"phone": null,
			"price": 15,
			"status": "1",
			"serviceInfo": [
				{
					"serviceId": "86B9EBDD45EE45619798F649EFE31821",
					"typeId": "86B9EBDD45EE45619798F649EFE31822",
					"typeName": "养护类",
					"serviceTitle": "快速洗车",
					"serviceDesc": null
				}
			],
			"productImageInfo": [
				{
					"imageUrl": "http://192.168.1.50:9016/gallery/20170712/welfare/0647d28023a74dc891182e58b41b682d.jpg",
					"smallImageUrl": "http://192.168.1.50:9016/gallery/20170712/welfare/8bb1148beb244dc2bf2c6b57c3755249.jpg"
				}
			]
		},
	},
	result: ['AA13FD960F0A49E5A188DC04C64F4804']
}

// initState = {
// 	data: {},
// 	result: []
// }

export default function serviceList(state = initState, action) {
	let _data = {}, _result = [], carId = '';
	switch (action.type) {
		case ADD_SERVICE_DETAIL:
			//添加违章列表
			if ( action.data && action.data.productId ) {
				let productId = action.data.productId;
				let temp = state.data[productId] || {} //如果已经存在更新现有的
				_data[productId] = { ...temp,...action.data }
				_result.push(productId)
			}
			return {
				data:{ ...state.data,..._data },
				result: Array.from(new Set([..._result, ...state.result]))
			}
		default:
			return state;
	}
}
