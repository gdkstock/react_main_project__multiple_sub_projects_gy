import {
	ADD_SHOP_LIST,
	ADD_SHOP_NUM,
	ADD_SHOP_DETAIL,
} from '../actions/actionsTypes'

//测试
let initState = {
	data: {
	
	},
	result: [],
    count:0, //商家数量
	cityName:"", //城市
	typeId:"", //服务类型
	tips:"", //提示消息
}

// initState = {
// 	data: {},
// 	result: []
// }

export default function shops(state = initState, action) {
	let _data = {}, _result = [], count = 0;
	switch (action.type) {
		case ADD_SHOP_LIST:
			let { tips , shopList , pageNo} = action.data
			shopList && shopList.map(item=>{
				let shopId = item.shopId;
				let temp = state.data[shopId] || {};
				_data[shopId] = {...temp,...item};
				_result.push(shopId);
			})
			if(pageNo=="1"){
				return {
					data:_data,
					result:_result,
					tips,
					count:state.count,
				}
			}else{
				return {
					...state,
					data:{ ...state.data,..._data },
					result: Array.from(new Set([...state.result,..._result])),
					tips,
				}
			}
		case ADD_SHOP_DETAIL:
			let shopId = action.data.shopId;
			let temp = state.data[shopId] || {};
			_data[shopId] = {...temp,...action.data};
			_result.push(shopId);
			return {
				...state,
				data:{ ...state.data,..._data },
				result: Array.from(new Set([..._result, ...state.result])),
			}
		case ADD_SHOP_NUM:
			return {
				...state,
				count:action.count,
			} 
		default:
			return state;
	}
}
