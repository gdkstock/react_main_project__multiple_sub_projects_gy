import React from 'react'
import styles from './index.scss'

const defaultProps={
	list:[
		{
			name:"车世界",
			imageUrl:"./images/icon-csj.jpg",
			readCount:"26236",
			jumpUrl:"http://zmt.cx580.com/media.aspx?id=100903",
		},
		{
			name:"华山论车",
			imageUrl:"./images/icon-hslc.jpg",
			readCount:"21858",
			jumpUrl:"http://zmt.cx580.com/media.aspx?id=101135",
		},
		{
			name:"玩车不歇",
			imageUrl:"./images/icon-wcbx.jpg",
			readCount:"25774",
			jumpUrl:"http://zmt.cx580.com/media.aspx?id=101130",
		},
		{
			name:"赏车猎人",
			imageUrl:"./images/icon-sclr.jpg",
			readCount:"18072",
			jumpUrl:"http://zmt.cx580.com/media.aspx?id=100971",
		},
	]
}
const List = ( props = defaultProps ) => {
	console.log("listprops",props)
	const list = props.list || defaultProps.list
	return (
		<div className={styles.list}>
			{
				list.length>0 && list.map((item,index)=>
					<div key={index} className={styles.item} onClick={()=>props.handleClick(item.jumpUrl)}>
						<div>
							<div><img className={styles.icon} src={item.imageUrl}/></div>
							<div className={styles.name}>{item.name}</div>
							<div className={styles.readCount}>{item.readCount}订阅</div>
						</div>
					</div>
				)
			}
		</div>
	)
}

export default List