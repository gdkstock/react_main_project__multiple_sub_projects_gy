import React from 'react'
import styles from './index.scss'

//ip访问：http://118.178.57.249:6002/mpMobile.aspx
//域名访问：http://zmt.cx580.com/mpMobile.aspx
const defaultProps={
	recommend:{
		title:"官方也有假？揭秘百公里油耗的真相！",
		imageUrl:"./images/icon-weMedia20170919.png",
		jumpUrl:"https://zmt.cx580.com/article.aspx?id=102408",
		readCount:"100000+",
	}
}

const Recommend = ( props) => {
	console.log("listprops",props)
	props = {...defaultProps,...props}
	const { recommend } = props
	return (
		<div className={styles.recommend} onClick={()=>props.handleClick(recommend.jumpUrl)}>
			<div className={styles.title}>
				<div className={styles.icon}>车行易头条</div>
				<span className={styles.content}>{recommend.title}</span>
			</div>
			<div className={styles.img}>
				<img src={recommend.imageUrl}/>
				<div>
					一个有趣有用更有料的平台
					<span className={styles.eye}>{recommend.readCount}</span>
				</div>
			</div>
		</div>
	)
}

export default Recommend