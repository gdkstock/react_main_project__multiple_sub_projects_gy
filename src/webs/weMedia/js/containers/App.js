import React, { Component } from 'react'
import { WeMediaList , HotRecommend } from '../components'
import common from '../utils/common'
import apiHelper from '../services/apiHelper'

export default class App extends Component {
	constructor(props) {
        super(props);

        this.state = {

        }
    }

	componentWillMount(){
		//获取推荐文章列表
		this.getArticleList()
		//获取自媒体人列表
		this.getWeMediaList()
	}

	// getArticleList(){
	// 	//没有缓存没有接口请求数据时显示
	// 	let title = "交警：这6种情况允许右转和掉头",
	// 		imageUrl = "./images/icon-weMedia20170731.png",
	// 		jumpUrl = "http://zmt.cx580.com/article.aspx?id=101856",
	// 		readCount = "100000+";
	// 	//没有接口请求数据，有缓存数据时显示
	// 	if(localStorage.getItem("articleMsg")){
	// 		let articleMsg = JSON.parse(localStorage.getItem("articleMsg"))
	// 		title = articleMsg.title
	// 		imageUrl = articleMsg.imgUrl;
	// 		jumpUrl = articleMsg.targetUrl;
	// 		readCount = articleMsg.readCount;
	// 	}
	// 	let requestParam = {
	// 		url:`${apiHelper.baseApiUrl}media/getMedia`,
	// 		data:{
	// 			method:"get",
	// 		}
	// 	};
	// 	apiHelper.fetch(requestParam).then(result=>{
	// 		//将数据转换成我们需要的格式
	// 		//alert(JSON.stringify(data))
	// 		if(result.code=="1000"){
	// 			//有数据时显示
	// 			if(result.data.imgUrl && result.data.targetUrl && result.data.title && result.data.readCount){
	// 				title = result.data.title
	// 				imageUrl = result.data.imgUrl;
	// 				jumpUrl = result.data.targetUrl;
	// 				readCount = result.data.readCount;
	// 				//请求成功数据，加入缓存
	// 				localStorage.setItem("articleMsg",JSON.stringify({ 
	// 					title,
	// 					imageUrl,
	// 					jumpUrl,
	// 					readCount
	// 				}))		
	// 			}
	// 		}

	// 		this.setState({
	// 			article:{
	// 				recommend:{
	// 					title,
	// 					imageUrl,
	// 					jumpUrl,
	// 					readCount,
	// 				}
	// 			}
	// 		})
	// 	}).catch(e=>{
	// 		this.setState({
	// 			article:{
	// 				recommend:{
	// 					title,
	// 					imageUrl,
	// 					jumpUrl,
	// 					readCount,
	// 				}
	// 			}
	// 		})
	// 	})
	// }

	getArticleList(){
		//没有缓存没有接口请求数据时显示
		let title = "官方也有假？揭秘百公里油耗的真相！",
			imageUrl = "./images/icon-weMedia20170919.png",
			jumpUrl = "https://zmt.cx580.com/article.aspx?id=102408",
			readCount = "100000+";
		//没有接口请求数据，有缓存数据时显示
		if(localStorage.getItem("articleMsg")){
			let articleMsg = JSON.parse(localStorage.getItem("articleMsg"))
			title = articleMsg.title
			imageUrl = articleMsg.imageUrl;
			jumpUrl = articleMsg.targetUrl;
			readCount = articleMsg.readCount;
		}
		this.setState({
			article:{
				recommend:{
					title,
					imageUrl,
					jumpUrl,
					readCount,
				}
			}
		})
		let requestParam = {
			url:`https://zmt.cx580.com/api/mp/public.media.article.list.ashx`,
			data:{
				method:"post",
				body:{
					mp_id:106308,
					page_size:1,
					current_page:1,
				},
			}
		};
		apiHelper.fetch(requestParam).then(result=>{
			//将数据转换成我们需要的格式
			//alert(JSON.stringify(data))
			console.log(result.list[0]);
			if(result.list && result.list.length>0){
				let id = result.list[0].Id;
				title = result.list[0].Title
				jumpUrl = `https://zmt.cx580.com/article.aspx?id=${id}`;
				readCount = result.list[0].ReadCount;
				$.ajax({
				  url: `https://zmt.cx580.com/article.aspx?id=${id}`,  // 请求的URL
				  type: "get", // 请求的方式,
				  async: true,  // 请求
				  success:(data)=>{
				  	try{
					    window.html=data;
					    let regImg = /<img[^>]+?src="https:\/\/zmp.cx580.com\/res\/img\/[^>]+?>/g;
					    let imgArr = data.match(regImg);
					    console.log(imgArr);
					    if(imgArr && imgArr.length>0){ 
						    let regSrc = /src="https:\/\/zmp.cx580.com\/res\/img\/[^"]+/g
						    let src=imgArr[imgArr.length-1].match(regSrc);
						    if(src && src.length>0){
						    	imageUrl = src[0].slice(5);
						    	console.log(title,imageUrl,jumpUrl,readCount)
						    	localStorage.setItem("articleMsg",JSON.stringify({ 
									title,
									imageUrl,
									jumpUrl,
									readCount
								}))	
								this.setState({
									article:{
										recommend:{
											title,
											imageUrl,
											jumpUrl,
											readCount,
										}
									}
								})
						    }
						}
					}catch(e){
						console.log(e)
					}
				  },
				  complete:(result)=>{
				  	
				  }
				})
			}
			// if(result.code=="1000"){
			// 	//有数据时显示
			// 	if(result.data.imgUrl && result.data.targetUrl && result.data.title && result.data.readCount){
			// 		title = result.data.title
			// 		imageUrl = result.data.imgUrl;
			// 		jumpUrl = result.data.targetUrl;
			// 		readCount = result.data.readCount;
			// 		//请求成功数据，加入缓存
			// 		localStorage.setItem("articleMsg",JSON.stringify({ 
			// 			title,
			// 			imageUrl,
			// 			jumpUrl,
			// 			readCount
			// 		}))		
			// 	}
			// }

			// this.setState({
			// 	article:{
			// 		recommend:{
			// 			title,
			// 			imageUrl,
			// 			jumpUrl,
			// 			readCount,
			// 		}
			// 	}
			// })
		}).catch(e=>{
			this.setState({
				article:{
					recommend:{
						title,
						imageUrl,
						jumpUrl,
						readCount,
					}
				}
			})
		})
	}

	getWeMediaList(){
		let requestParam = {
			url:"https://zmt.cx580.com/api/mp/cxy.media.recommend.ashx",
			data:{
				method:"get"
			}
		};
		apiHelper.fetch(requestParam).then(data=>{
			//将数据转换成我们需要的格式
			//alert(JSON.stringify(data))
			if(data.status=="0"){
				let weMediaList=[]
				if(Object.prototype.toString.call(data.list)=="[object Array]"){
					weMediaList = data.list.map((item)=>{
						let targetItem={
							name:item.AccountName,
							readCount:item.FollowerNum,
							imageUrl:item.Logo,
							jumpUrl:item.MPUrl
						}
						return targetItem
					})
				}
				this.setState({
					weMediaList
				})
			}
		})
	}
  
  	jumpTo(url){
  		common.openNewBrowserWithURL(url);
  	}

	render() {

		return (
		  <div>
		  	<HotRecommend
		  		{...this.state.article} 
		  		handleClick={this.jumpTo}
		  	/>
		    <WeMediaList 
		    	handleClick={this.jumpTo}
		    	list={this.state.weMediaList}
		    />
		  </div>
		)
	}
}