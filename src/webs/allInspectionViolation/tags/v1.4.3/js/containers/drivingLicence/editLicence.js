/*
* 编辑驾照
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn ,Confirm ,Modal} from '../../components'
import { TxtGroup ,DateGroup} from '../../components/Form'
import styles from './addLicence.scss'
import * as carListActions from '../../actions/carListActions'
import { Toast } from 'antd-mobile'
import moment from 'moment'
import common from '../../utils/common'

class EditLicence extends React.Component{
	constructor(props){
		super(props);
		//初始化状态	
		this.state = this.getNewState({},this.props);

	}

	getNewState(state){
		let carNumber;
		let carId = this.props.params.carId || "";
		let licenceId = this.props.params.licenceId || "";

		let	driverName = '',
			drivingLicenseNo = '',
			drivingFileNumber = '',
			firstLicensingDate = '',
			drivingTelephone = '';
		if(licenceId && this.props.drivingLicence.data[licenceId]){
			driverName = this.props.drivingLicence.data[licenceId].driverName || '';
			drivingLicenseNo = this.props.drivingLicence.data[licenceId].drivingLicenseNo || '';
			drivingFileNumber = this.props.drivingLicence.data[licenceId].drivingFileNumber || '';
			drivingTelephone = this.props.drivingLicence.data[licenceId].drivingTelephone || '';
			firstLicensingDate = this.props.drivingLicence.data[licenceId].firstLicensingDate || '';
			//转化date
			if(firstLicensingDate){
				firstLicensingDate=new Date(firstLicensingDate)
				firstLicensingDate = moment(firstLicensingDate).format("YYYY-MM-DD")
			}
		}
		if(carId && this.props.cars.data[carId]){
			carNumber = this.props.cars.data[carId].carNumber || "";
		}
		return {
			...state,
			carId,
			drivingLicenseId:licenceId,
			driverName,
			drivingLicenseNo,
			drivingFileNumber,
			firstLicensingDate,
			drivingTelephone,
			isBtnDisabled:true,
		}
	}


	componentWillMount(){
		//设置标题
		common.setViewTitle('编辑驾照');

		//请求车辆信息
		let carId = this.props.params.carId;
		let { cars ,drivingLicence} = this.props;
		if(!cars.data[carId]){
			this.props.actions.queryCarInfo({carId})
		}
		let licenceId = this.props.params.licenceId || "";
		if(licenceId && !drivingLicence.data[licenceId]){
			this.props.actions.queryDrivingLicence({drivingLicenseId:licenceId})
		}	
	}

	componentWillReceiveProps(nextProps){
		let nextState = this.getNewState(this.state)
		this.setState(nextState)
	}

	componentDidMount(){
		//组件加载完成

		//后退时显示确认对话框
		common.backShowConfirm(() => {
			let obj = {
				show: true, //显示对话框
				content: '编辑的信息尚未保存，确定退出？', //内容
				leftBtn: {
					name: '离开', //左按钮文字
					onClick: () => {sessionStorage.removeItem("showConfirm");history.back()}  //点击左按钮
				},
				rightBtn: {
					name: '取消',//右按钮文字
					onClick: () => history.forward() //点击右按钮
				},
				onClose: () => history.forward() //点击关闭按钮
			}
			Modal.confirm(obj);
		})
	}

	componentDidUpdate(){
		//如果编辑驾照信息
		if(!this.state.isBtnDisabled){
			sessionStorage.setItem("showConfirm",1);
		}
	}

	componentWillUnmount() {
		//离开编辑驾照页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

	//输入改变时验证数据
	preValiDate(e){
		let target = e.target,
			name = target.name, 
			len,
			value,
			currValue = target.value.trim();
		switch(name){
			case "driverName":
				len = 32;
				break;
			case "drivingLicenseNo":
				currValue = currValue.toUpperCase();
				len = 18;
				break;
			case "drivingFileNumber":
				currValue = currValue.toUpperCase();
				len = 12;
				break;
			case "drivingTelephone":
				len = 11;
				currValue = currValue.replace(/[^\d]/,'')
				break;
		}
		value = this.cutInput(currValue,len);
		this.setState({
			[name]:value,
			isBtnDisabled:false,
		})
	}

	//截取输入框输入文本长度
	cutInput(value ,len){
		let output = value;
		if(value.length>len){
			output = value.slice(0,len)
		}
		return output
	}

	//初次领证日期选择变化
	firstLicensingDateChange(date){
		this.setState({
			firstLicensingDate:date.format("YYYY-MM-DD"),
			isBtnDisabled:false
		})
	}

	//提交数据
	editInfo(){
		if(this.valiDate()){
			let param = this.state
			this.props.actions.queryUpdateDrivingLicence(param)
		}
	}

	//点击提交时验证数据
	valiDate(){
		let {
			carId,
			driverName,
			drivingLicenseNo,
			drivingFileNumber,
			firstLicensingDate,
			drivingTelephone,
		} = this.state

		//验证姓名
		if(driverName.length == 0){
			Toast.info("请输入姓名",1)
			return false
		}

		//验证驾驶证号
		if(drivingLicenseNo.length == 0){
			Toast.info("请输入驾驶证号",1)
			return false
		}else if(!(drivingLicenseNo.length == 15 || drivingLicenseNo.length == 18)){
			Toast.info("请输入正确驾驶证号",1)
			return false
		}

		//验证档案编号
		if(drivingFileNumber.length == 0){
			Toast.info("请输入档案编号",1)
			return false
		}else if( drivingFileNumber.length != 12 ){
			Toast.info("请输入正确的档案编号",1)
			return false
		}

		//验证领证日期
		if(firstLicensingDate.length == 0){
			Toast.info("请选择初次领证日期",1)
			return false
		}

		//验证手机号码
		if(drivingTelephone.length == 0){
			Toast.info("请输入手机号")
			return false
		}else if(!/^\d{11}$/.test(drivingTelephone)){
			Toast.info("请输入正确的手机号")
			return false
		}

		return true
	}

	//删除驾照按钮点击
	deleteBtnClick(){
		this.setState({
			confirm:{
			    show: true, //显示对话框
			    content: '确定要删除此驾照吗？', //内容
			    leftBtn: {
			        name: '删除', //左按钮文字
			        onClick: () => this.confirmOk()  //点击左按钮
			    },
			    rightBtn: {
			        name: '取消',//右按钮文字
			        onClick: () => this.confirmCancel() //点击右按钮
			    },
			    onClose: () => this.confirmClose() //点击关闭按钮
			}
		})
	}

	//confirm确认事件
	confirmOk(){
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
		this.deleteLicence()
	}

	//confirm取消事件
	confirmCancel(){
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
	}

	//confirm关闭事件
	confirmClose(){
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
	}

	//删除驾照
	deleteLicence(){
		let drivingLicenseId = this.state.drivingLicenseId
		this.props.actions.queryDeleteDrivingLicence({drivingLicenseId})
	}

	render(){

		//获取车牌号码
		let { cars } =this.props,
			carId = this.props.params.carId,
			carNumber = "";
		if(cars.data[carId]){
			carNumber = cars.data[carId].carNumber
		}

		//车主姓名
		const driverName={
			groupId:"driverName" ,//唯一编码
			label:'车主姓名',//标签文本
			placeholder:"车主姓名" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			value:this.state.driverName,//输入框值
		}

		//驾驶证号
		const drivingLicenseNo={
			groupId:"drivingLicenseNo" ,//唯一编码
			label:'驾驶证号',//标签文本
			placeholder:"驾驶证号完整15位或18位" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			hasIcon:true,//是否有提示icon
			iconImg:"./images/demo-jashiZhengHaoLen.png",//icon弹出图片
			value:this.state.drivingLicenseNo,//输入框值
		}

		//档案编号
		const drivingFileNumber={
			groupId:"drivingFileNumber" ,//唯一编码
			label:'档案编号',//标签文本
			placeholder:"档案编号完整12位数" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			hasIcon:true,//是否有提示icon
			iconImg:"./images/demo-danganBianHaoLen.png",//icon弹出图片
			value:this.state.drivingFileNumber,//输入框值
		}
		
		//初次领证日期选择组件props
		const firstLicensingDateProps = {
			groupId:"firstLicensingDate" ,//唯一编码
			label:'初次领证日期',//标签文本
			placeholder:'请选择' ,//占位符文本
			type:"date" ,//日期选择器类型
			format:"YYYY年MM月DD日", //日期格式
			maxDate:moment(),//最大值
			handleChange:(v)=>{this.firstLicensingDateChange(v)} ,//输入框文本改变操作
			value:this.state.firstLicensingDate?moment(this.state.firstLicensingDate,'YYYY年MM月DD日'):"" //输入框值
		}

		//驾驶证绑定手机号
		const drivingTelephone={
			groupId:"drivingTelephone" ,//唯一编码
			label:'驾驶证绑定手机号',//标签文本
			placeholder:"输入手机号码" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			value:this.state.drivingTelephone,//输入框值
		}

		//保存按钮
		const btnProps = {
			text:"保存",
			handleClick:()=>this.editInfo(),
			disabled:this.state.isBtnDisabled,
		}

		return (
			<div>
				<div className="wz_whiteSpace_30"></div>
				<div className="wz_headMsg">
					请填写{carNumber.slice(0,2)+" "+carNumber.slice(2)}<span className={styles.promptInfo}>车主本人</span>的信息
					<div className={styles.deleteBtn} onClick={()=>this.deleteBtnClick()}><i className={styles.icon}></i>删除</div>
				</div>
				<div className="wz_list">
					<TxtGroup {...driverName}/>
					<TxtGroup {...drivingLicenseNo}/>
					<TxtGroup {...drivingFileNumber}/>
					<DateGroup {...firstLicensingDateProps}/>
					<TxtGroup {...drivingTelephone}/>
				</div>
				<div className="wz_whiteSpace_50"></div>
				<Btn {...btnProps}/>
				<Confirm {...this.state.confirm}/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars:state.cars,
	drivingLicence:state.drivingLicence
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditLicence)

