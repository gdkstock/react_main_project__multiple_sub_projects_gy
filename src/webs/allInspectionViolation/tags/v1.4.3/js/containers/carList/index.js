/**
 * 违章业务首页车辆列表页面
 * Created by lijun on 2017/5/11.
   props = {
		data:[
			{},
		]
   }
 */
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Toast } from 'antd-mobile'

//style
import styles from './index.scss'

//组件
import { Btn, Slogan, ListA, Msg, Banner } from '../../components'
import AddCar from '../../components/addCar'

//action
import * as carListActions from '../../actions/carListActions'

//常用工具类
import config from '../../config'
import common from '../../utils/common'

class carList extends React.Component {
	constructor(props) {
		super(props);
		//设置初始状态
		this.state = {
			showAddCar: false, //显示添加车辆蒙层
			carPrefixList: undefined, //车牌前缀列表 不传则使用默认的列表
		}
	}

	componentWillMount() {
		//设置标题
		common.setViewTitle('车辆列表');

		//请求车辆列表数据
		let isGet = sessionStorage.getItem("isGet")//首次进入
		if (!isGet) {
			this.props.actions.queryCarList({ isGet: 1 });
			setTimeout(() => {
				sessionStorage.setItem("isGet", 1); //在延迟中执行，避免token过期走单点登录重新回来时，isGet在接口请求前就变成1
			}, 1000)
		} else {
			this.props.actions.queryCarList();
		}

		//请求车辆违章约束条件
		// this.props.actions.queryCarCondition();

		//请求banners
		this.props.actions.queryBanners({ positionCode: "WZ_CAR_SY_01" });
		this.props.actions.queryBanners({ positionCode: "WZ_CAR_LIST_01" });
		//this.props.actions.queryBanners({positionCode:"WZ_QUERY_02"});

	}

	componentWillReceiveProps(nextProps) {
		let { carPrefixList } = this.state
		let { carConditionList } = nextProps

		if (!carPrefixList && carConditionList && carConditionList.result.length > 0) {
			//当前组件的车牌前缀列表不存在 但是全局state存在
			this.setState({
				carPrefixList: carConditionList.result
			})
		}
	}

	componentDidMount() {
		//进入首页埋点
		common.sendCxytj({
			eventId: "Violation_EnterCarList"
		})
	}

	componentWillUnmount() {
		//离开车辆列表页记录页面信
		sessionStorage.setItem("prevPageInfo", document.title)
	}

	//添加车辆
	addCar() {
		//点击添加车辆埋点
		common.sendCxytj({
			eventId: "Violation_AddCar"
		})

		//全渠道都使用引导注册的方式

		//获取车牌前缀
		this.props.actions.getCarConditionList({}, result => {
			let { carPrefixList } = this.state
			if (result && result.code == '1000' && result.result.length > 0) {
				carPrefixList = result.result
			}

			this.setState({
				showAddCar: true,
				carPrefixList: carPrefixList
			})
		})

		// if (common.getJsApiUserType() == 'weixin') {
		// 	//用引导注册的添加车辆

		// 	//获取车牌前缀
		// 	this.props.actions.getCarConditionList({}, result => {
		// 		let { carPrefixList } = this.state
		// 		if (result && result.code == '1000' && result.result.length > 0) {
		// 			carPrefixList = result.result
		// 		}

		// 		this.setState({
		// 			showAddCar: true,
		// 			carPrefixList: carPrefixList
		// 		})
		// 	})
		// } else {
		// 	//用原来的添加车辆页面
		// 	window.open(common.getRootUrl() + "addCar", '_self')
		// }
	}

	/**
	 * 检测输入的合法性
	 */
	checkInputs(carNumber) {
		let checkAZ = /[A-Z]/g.exec(carNumber.substr(1, 1)); //检查车牌号码是否以字母开头

		if (carNumber.length < 2) {
			Toast.info("车牌号码不能为空", 1);
		} else if (carNumber.length < 7 || carNumber.length > 8 || !checkAZ) {
			Toast.info("您输入的车牌号码有误", 1);
		} else {
			return true
		}
		return false
	}

	//跳转到添加车辆验证页面
	toAddCarAuthPage(carNumber) {
		if (this.checkInputs(carNumber)) {
			this.props.actions.getCarInformation({ carNumber: carNumber }, result => {
				if (result && result.code == '1000') {
					this.hideAddCar();

					let { carId, count, degree, money, tel, flag } = result.data
					switch (flag + '') {
						case '0': //无车辆信息
							if (carNumber.substr(0, 1) == '粤') {
								//粤牌车可以进行身份验证
								this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							} else {
								this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
							}
							break;
						case '1': //存在车辆信息
							if (carNumber.substr(0, 1) != '粤' && count == -1 && !tel) {
								//非粤牌车 && 无违章 && 无手机号码
								this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
							} else {
								this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							}

							break;
						case '2': //车辆已经存在车辆列表中
							window.location.href = common.getRootUrl() + 'violationList/' + carId; //跳转到违章列表
							break;
						default:
							this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
					}

				} else {
					if (result.code == '4443') {
						//满三辆车
						Toast.info('违章查询最多支持绑定3辆车辆,请前往车辆列表删除后再继续查询', 3, () => this.hideAddCar())
					} else {
						Toast.info(result.msg || ChMessage.FETCH_FAILED)
					}
				}
			})
			// 
			// carNumber = encodeURIComponent(carNumber);
			// window.location.href = `${config.addCarUrl}#authPage/${carNumber}(/:count/:degree/:money)(/:tel)` //跳转到添加车辆的身份验证页面
		}
	}

	//跳转到添加车辆页面
	toAddCarUrl(url) {
		window.location.href = `${config.addCarUrl}#${url}` //跳转到添加车辆的相关路由页面
	}

	//隐藏添加车辆
	hideAddCar() {
		this.setState({
			showAddCar: false
		})
	}

	//点击每个车辆违章信息
	handleClick(item) {
		if (item.isCorrect == 0) {
			//点击补充资料埋点
			common.sendCxytj({
				eventId: "Violation_AddCarData"
			})
			window.open(common.getRootUrl() + "replenishInfo/" + item.carId + "?msg=" + item.msg, '_self')
		} else {
			window.open(common.getRootUrl() + "carDetail/" + item.carId, '_self')
		}
	}

	//生成车辆列表
	createCarList() {
		let { cars } = this.props,
			data = cars.data,
			list = cars.result,
			rightText = "",
			totalDegree,
			untreatedNum,
			totalFine,
			outputList = [];

		outputList = list.map((item) => {
			if (data[item].totalDegree == -1 && data[item].totalFine == -1 && data[item].untreatedNum == -1) {
				totalDegree = "?";
				totalFine = "?";
				untreatedNum = "?";
			} else {
				totalDegree = data[item].totalDegree;
				totalFine = data[item].totalFine;
				untreatedNum = data[item].untreatedNum;
			}
			if (data[item].isCorrect == 0) {
				rightText = "补充资料";
			} else {
				rightText = "";
			};
			return {
				...data[item],
				totalDegree,
				totalFine,
				untreatedNum,
				rightText,
			}
		})

		return outputList
	}

	//点击广告跳转
	bannerClick(item) {
		if (item.targetUrl) {
			window.location.href = item.targetUrl;
		}
	}

	render() {
		let { showAddCar, carPrefixList } = this.state
		let { cars, banners } = this.props

		//车辆概要信息展示数据
		let carList = this.createCarList()

		//广告列表信息展示
		let bannerLG = {}
		let bannerSM = {}
		if (banners.data["WZ_CAR_SY_01"]) {
			bannerLG = {//未添加车辆时
				list: banners.data["WZ_CAR_SY_01"].adList,
				positionCode: "WZ_CAR_SY_01",
				handleClick: this.bannerClick
			}
		}

		if (banners.data["WZ_CAR_LIST_01"]) {
			bannerSM = {//有车辆信息时
				list: banners.data["WZ_CAR_LIST_01"].adList,
				positionCode: "WZ_CAR_LIST_01",
				handleClick: this.bannerClick
			}
		}

		return (
			<div>
				{carList.length > 0 ?
					<div>
						<Banner {...bannerSM} />
						<ListA list={carList} handleClick={this.handleClick} />
					</div>
					: <div>
						<div style={{ marginBottom: "37px" }}>
							<Banner {...bannerLG} />
						</div>
						<Slogan />
					</div>
				}
				{carList.length < 3 ?
					<div style={{ width: "100%", position: "fixed", bottom: "55px" }}>
						<Btn
							text="添加车辆"
							handleClick={() => this.addCar()}
						/>
					</div>
					: <Msg />
				}

				{/*添加车辆蒙层 start*/}
				{
					showAddCar
						? <div className={styles.addCar}>
							<AddCar
								carPrefixList={carPrefixList}
								onClick={(carNumber) => this.toAddCarAuthPage(carNumber)}
								close={() => this.hideAddCar()}
							/>
						</div>
						: ''
				}

				{/*添加车辆蒙层 end*/}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars: state.cars,
	banners: state.banners,
	carConditionList: state.carConditionList
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(carList)