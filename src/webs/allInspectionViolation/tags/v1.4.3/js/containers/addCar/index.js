/*
* 添加车辆
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn , Slogan } from '../../components'
import { Switch } from 'antd-mobile';
import CarPrefixList from 'app/components/common/carPrefixList'
import { TxtGroup } from '../../components/Form'
import styles from './index.scss'
import * as carListActions from '../../actions/carListActions'
import { Toast } from 'antd-mobile'
import apiHelper from '../../services/apiHelper';

class AddCar extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		//初始化状态	
		this.state = this.getNewState({},this.props);
	}

	//获取新状态
	getNewState(state,props){
		//定义相关变量
		let carPrefix="",
			condition={},
			currentProvince={},
			provinces =[],
			carCodeLen = "",
			carEngineLen = '',
			carCodePlaceholder = "",
			engineNumberPlaceholder = "",
			list = [],
			isProvincesExist = props.provinces.length>0?true:false;
		if(isProvincesExist){
			provinces = props.provinces;
			carPrefix = provinces[0].provincePrefix;
			currentProvince = provinces[0];
			carCodeLen = provinces[0].cities[0].carCodeLen;
			carEngineLen = provinces[0].cities[0].carEngineLen;
			carCodePlaceholder = this.getPlaceholder("车身架号",carCodeLen);
			engineNumberPlaceholder = this.getPlaceholder("发动机号",carEngineLen);
			list = this.createCarPrefixList(provinces);
			condition = {
				carCodeLen, //车架号长度,0表示不需要,99表示整个号码，其他数字表示需要号码的后几位，下同
				carEngineLen, //发动机号长度
			};
		};
		let nextState = {
			carPrefix,
			carPrefixList:{
				getName:(name)=>this.getCarPrefix(name),
				show:false,
				list
			},
			CarCode:{
				groupId:"carCode" ,//唯一编码
				label:'车身架号',//标签文本
				placeholder:carCodePlaceholder ,//占位符文本
				handleChange:(e)=>{this.preValiDate(e)} ,//输入框文本改变操作
				type:"text", //输入框type
				value:"",//输入框值
			},
			EngineNumber:{
				groupId:"engineNumber" ,//唯一编码
				label:'发动机号',//标签文本
				placeholder:engineNumberPlaceholder ,//占位符文本
				handleChange:(e)=>{this.preValiDate(e)} ,//输入框文本改变操作
				type:"text", //输入框type
				value:"",//输入框值
			},
			firstCode:"",//车牌号码首字母
			carNum:"", //除去车牌前缀后的车牌号码
			isSubscribe:true,//是否开启消息通知
			userTelephone:"",//用户手机号码
			condition, //车辆查询条件
			currentProvince,//当前省份信息
			isProvincesExist, //是否存在省份信息
		}
		return {...state,...nextState}
	}

	componentWillMount(){
		//设置标题
		document.querySelector("title").innerHTML="添加车辆"
		//请求车辆查询约束条件
		this.props.actions.queryCarCondition()
	}

	componentWillReceiveProps(nextProps){
		let nextState = this.getNewState(this.state,nextProps)
		this.setState(nextState)
	}

	componentDidMount(){
		//组件加载完成
	}

	//显示车牌前缀列表
	showCarPrefixList(){
		this.setState({
			carPrefixList:{...this.state.carPrefixList,show:true}
		})
	}

	//获取用户选择的车牌前缀
	getCarPrefix(name){
		if(name){
			//存储当前省份信息,并返回省份第一个城市的查询条件
			for (let i = 0; i < this.props.provinces.length; i++) {
				let curr = this.props.provinces[i]
				if(curr.provincePrefix==name){
					let carCodeLen = curr.cities[0].carCodeLen,
						carEngineLen = curr.cities[0].carEngineLen
					this.setState({
						carPrefix:name,
						currentProvince:curr,
						condition:{ 
							carCodeLen, 
			               	carEngineLen,
						},
						CarCode:{
							...this.state.CarCode,
							placeholder:this.getPlaceholder("车身架号",carCodeLen) ,//占位符文本
							value:"",//输入框值
						},
						EngineNumber:{
							...this.state.EngineNumber,
							placeholder:this.getPlaceholder("发动机号",carEngineLen) ,//占位符文本
							value:"",//输入框值
						},
						carNum:""
					})
					this.refs.carNum.value="";
					break;
				}
			}
		}
		this.setState({
			carPrefixList:{...this.state.carPrefixList,show:false}
		})
	}

	//车牌号码改变
	carNumChange(e){
		let value="";
		let currentValue = e.target.value.trim()  //当前输入值
		let preValue = this.state.carNum //前一个值

		let preFirstCode = this.state.firstCode //前一个首字母
		let currentFirstCode = currentValue.length>0?currentValue[0]:""; //当前首字符

		//判断首字符是不是英文
		let reg = /^[a-zA-Z]$/
		if(reg.test(currentFirstCode)){
			value = currentValue.length>6?currentValue.slice(0,6):currentValue
			if(currentFirstCode!= preFirstCode){
				//更新车辆查询条件输入
				let carPrefix = this.state.carPrefix;
				if(carPrefix!="京"&&carPrefix!="津"&&carPrefix!="沪"&&carPrefix!="渝"){
					let carNumberPrefix = this.state.carPrefix+currentFirstCode.toUpperCase()
					this.updateCarCondition(carNumberPrefix)
				}
			}
		}
		this.refs.carNum.value = value.toUpperCase();
		this.setState({
			carNum:value.toUpperCase()
		})
	}

	//车牌输入变化时更新车辆条件
	updateCarCondition(carPrefix){
		let cities = this.state.currentProvince.cities

		for(let i=0;i<cities.length;i++){
			if(cities[i].carNumberPrefix==carPrefix){
				let carCodeLen = cities[i].carCodeLen,
					carEngineLen = cities[i].carEngineLen
				this.setState({
					condition:{ 
						carCodeLen, 
		               	carEngineLen,
					},
					CarCode:{
						...this.state.CarCode,
						placeholder:this.getPlaceholder("车身架号",carCodeLen) ,//占位符文本
						value:""
					},
					EngineNumber:{
						...this.state.EngineNumber,
						placeholder:this.getPlaceholder("发动机号",carEngineLen) ,//占位符文本
						value:""
					},
				})
				break;
			}
		}
	}

	//除车牌号外其他输入框改变时判断
	preValiDate(e){

		let target = e.target,
			name = e.target.name, 
			len,
			value,
			currValue = target.value;
		switch(name){
			case "carCode":
				len = this.state.condition.carCodeLen;
				value = this.cutInput(currValue,len);
				this.setState({
					CarCode:{...this.state.CarCode,value:value}
				})
				break;
			case "engineNumber":
				len = this.state.condition.engineNumber;
				value = this.cutInput(currValue,len)
				this.setState({
					EngineNumber:{...this.state.EngineNumber,value:value}
				})
				break;
			case "userTelephone":
				len = 11;
				value = this.cutInput(currValue,len)
				this.setState({
					userTelephone:value
				})
				break;
		}
	}

	//截取输入框输入文本长度
	cutInput(value ,len ){
		let output = value;
		if(value.length>len){
			output = value.slice(0,len)
		}
		return output
	}

	//获取输入框placeholder
	getPlaceholder(txt,len){
		switch(len){
			case 99:
				return "请输入完整的"+txt
			default:
				return txt+"后"+len+"位数"
		}
	}

	//switch选择改变
	checkboxChange(checked){
		this.setState({
			isSubscribe:checked
		})
	}

	//点击添加车辆时前验证信息
	valiDate(){
		let carNum = this.state.carNum;
		let hasCarCode = this.state.condition.carCodeLen>0 ? true :false;
		let hasEngineNumber = this.state.condition.carEngineLen>0? true : false;

		//验证车牌号码
		if(!this.valiCarNum(carNum)){
			return false
		}

		//验证车架号
		if(hasCarCode){
			let carCode = this.state.CarCode.value;
			let len = this.state.condition.carCodeLen
			if(!this.valiCarCode(carCode,len)){
				return false
			}
		}

		//验证发动机号
		if(hasEngineNumber){
			let engineNumber = this.state.EngineNumber.value;
			let len = this.state.condition.carEngineLen
			if(!this.valiEngineNumber(engineNumber,len)){
				return false
			}
		}

		//验证手机号码
		if(this.state.isSubscribe){
			let phone = this.state.userTelephone;
			if(!this.valiPhone(phone)){
				return false
			}
		}

		return true;
	}

	//valiCarNum验证车牌信息
	valiCarNum(carNum){
		//验证车牌号码
		if(carNum.length!=6){
			Toast.info("请输入正确的车牌号码",1)
			return false
		}else{
			return true
		}
	}

	//验证车架号信息
	valiCarCode(carCode,len){
		if(carCode.length!=len){
			Toast.info("请输入正确的车身架号",1)
			return false
		}else{
			return true
		}
	}

	//验证发动机号信息
	valiEngineNumber(engineNumber,len){
		if(engineNumber.length!=len){
			Toast.info("请输入正确的发动机号",1)
			return false
		}else{
			return true
		}
	}

	//验证手机信息
	valiPhone(phone){
		if(phone.length!=11){
			Toast.info("请输入正确的手机号",1)
			return false
		}else{
			return true
		}
	}

	//添加车辆
	addCar(){
		//验证数据
		if(this.valiDate()){
			//构建请求参数
			let param = {
				carNumber:this.state.carPrefix+this.state.carNum,
			}
			this.state.condition.carCodeLen>0 && (param.carCode = this.state.CarCode.value);
			this.state.condition.carEngineLen>0 && (param.engineNumber = this.state.EngineNumber.value);
			this.state.isSubscribe && (param.userTelephone = this.state.userTelephone);
			//发送请求
			this.props.actions.queryAddCar(param)
		}
	}

	//生成车牌前缀列表
	createCarPrefixList(data){
		let arr = []
		for (let i = 0; i < data.length; i++) {
			arr.push(data[i].provincePrefix) 
		}
		return arr
	}

	render(){
		let condition = this.state.condition
		let sloganClass =""
		if(condition.carCodeLen>0 && condition.carEngineLen>0){
			sloganClass = styles.whiteSpace_168
		}else if(condition.carCodeLen==0 && condition.carEngineLen==0){
			sloganClass = styles.whiteSpace_368
		}else{
			sloganClass = styles.whiteSpace_268
		}
		return (
			<div>
				<div className="wz_whiteSpace_30"></div>
				<div className="wz_headMsg">一下信息仅供于交管局查询，我们将严格保密</div>
				<div className="wz_list">
					<div className={"wz_item "+styles.carNum}>
						<span className="label">车牌号码</span>
						<div className={styles.carPrefix} onClick={()=>this.showCarPrefixList()}>
							{this.state.carPrefix}<i className={styles.rightIcon}>&nbsp;</i>
						</div>
						<input 
							className="txt" 
							ref="carNum" 
							placeholder="车牌号码"
							type="url"
							onChange={(e)=>this.carNumChange(e)}
						/>
					</div>
					{this.state.condition.carCodeLen>0 && <TxtGroup {...this.state.CarCode}/>}
					{this.state.condition.carEngineLen>0 && <TxtGroup {...this.state.EngineNumber}/>}
				</div>
				<div className="wz_whiteSpace_28"></div>
				<div className="wz_list">
					<div className="wz_item">
						<span className="label">新违章消息通知及年检提醒服务</span>
						<span className={styles.switch}>
							<Switch checked={this.state.isSubscribe} onChange={(checked)=>this.checkboxChange(checked)}/>
						</span>
					</div>
					<div className="wz_item">
						<span className={this.state.isSubscribe?"label":"label "+styles.phoneDisabled}>手机号码</span>
						{this.state.isSubscribe && 
							<input 
								className="txt" 
								placeholder="手机号码" 
								name="userTelephone"
								value={this.state.userTelephone}
								onChange={(e)=>this.preValiDate(e)}
								type="tel"
							/>
						}
					</div>
				</div>
				<div className="wz_whiteSpace_50"></div>
				<Btn text="保存并查询" handleClick={()=>this.addCar()}/>
				<div className="wz_whiteSpace_30"></div>
				<div className={styles.agreeMsg}>
					点击保存并查询，即表示您同意 <a href={apiHelper.baseApiUrl+"agree/AgreeMent.html"}>用户授权协议</a>
				</div>
				<div className={sloganClass}></div>
				<Slogan />
				<CarPrefixList {...this.state.carPrefixList}/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars: state.cars,
	provinces:state.provinces
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddCar)