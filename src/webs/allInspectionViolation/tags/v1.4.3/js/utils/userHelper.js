import config from '../config'

/**
 * 用户帮助类
 */
class UserHelper {

    /**
     * 跳转到单点登录
     */
    toAuthUrl() {
        let userId = sessionStorage.getItem("userId");
        sessionStorage.clear(); //清空sessionStorage缓存
        let url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.hash; //好像不用编码 编码反而出错？
        url = url.replace('#', '%23'); //替换#号
        url = url.split('?')[0]; //过滤？号
        url += '?t=' + new Date().getTime(); //加一个时间戳，避免服务器挂后，一直读取缓存的数据
        url += '%26app_cxy_pre_userId=' + userId;
        window.location.replace(config.authUrl + url); //跳转到单点登录
    }

    /**
     * 获取 userId 和 token
     */
    getUserIdAndToken() {

        return {
            userId: sessionStorage.getItem('userId'),
            token: sessionStorage.getItem('token'),
            deviceId: sessionStorage.getItem("deviceId"),
            userType: sessionStorage.getItem('userType'),
            authType: sessionStorage.getItem('authType')
        }

    }

    /**
     * 登陆
     * @param callback function 登陆成功之后的回调
     */
    Login(callback) {
        if (sessionStorage.getItem("userType") && sessionStorage.getItem("authType")) {
            this.toAuthUrl() //跳转到单点登录
        }
    }

};

// 实例化后再导出
export default new UserHelper()