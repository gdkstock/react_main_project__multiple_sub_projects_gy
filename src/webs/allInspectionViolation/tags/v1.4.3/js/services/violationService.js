/**
 * 违章相关接口
 */

import apiHelper from './apiHelper';

class ViolationService {

    /**
     * 获取违章列表信息
     * @param {*object} data
     */
    getViolationList(data) {
        const cityName = sessionStorage.getItem('city'); // app 环境需要传city
        if (cityName) {
            data.cityName = cityName;
        }
        let requestParam = {
            url: `${apiHelper.baseApiUrl}violation/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 修改违章信息
     * @param {*object} data 
     */
    updateViolation(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}violation/update`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 违章地点违章次数查询
     * @param {*object} data 
     */
    locationViolationNumber(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}violation/locationViolationNumber`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 高德地图地理编码
     */
    geocode(data) {
        let requestParam = {
            url: `//restapi.amap.com/v3/geocode/geo`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }
    /**
     * 获取违章热点评论
     */
    getComment(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}violation/hot/comment`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new ViolationService()