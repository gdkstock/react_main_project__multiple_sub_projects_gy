/**
 * 违章相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    ADD_ORDER,
    ADD_ORDER_ASYNC,
    GET_COMMENT,//fetch请求出错
} from '../actions/actionsTypes'

//service
import orderService from '../services/orderService';

import { ChMessage } from '../utils/message.config' //提示信息

import { Toast } from 'antd-mobile' //UI

import { normalize, schema } from 'normalizr'; //范式化库

function* addOrderAsync(action) {
    // Toast.loading('', 0)
    // try {
    //     let { result, timeout } = yield race({
    //         result: call(violationService.getComment, action.data),
    //         timeout: call(delay, 30000)
    //     })
    //     Toast.hide();
    //     if (timeout) {
    //         window.networkError('./images/networkError-icon.png');
    //     } else {
    //         if (result.code == "1000") {
    //             if (action.callback) {
    //                 action.callback(result)
    //             }
    //         }
    //     }
    // } catch (error) {
    //     Toast.hide();
    // }
}

function* watchAddOrderAsync() {
    yield takeLatest(ADD_ORDER_ASYNC, addOrderAsync)
}

export function* watchOrderFetch() {
    yield [
        fork(watchAddOrderAsync)
    ]
}