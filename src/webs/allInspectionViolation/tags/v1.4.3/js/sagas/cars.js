/*
	车辆信息相关请求
*/
import {
  takeEvery,
  delay,
  takeLatest,
  buffers,
  channel,
  eventChannel,
  END
} from 'redux-saga'
import {
  race,
  put,
  call,
  take,
  fork,
  select,
  actionChannel,
  cancel,
  cancelled
} from 'redux-saga/effects'

import moment from 'moment'
import apiHelper from '../services/apiHelper';
import fetch from 'isomorphic-fetch'
import { Toast } from 'antd-mobile'
import { ChMessage } from '../utils/message.config'
import carService from '../services/carService'
import common from '../utils/common'
import jsApi from '../utils/jsApi'

import { normalize, schema } from 'normalizr'; //范式化库

import {
  REQUEST_CAR_LIST,
  ADD_CAR,
  ADD_CAR_LIST,
  QUERY_CAR_CONDITION_LIST,
  GET_CAR_CONDITION_LIST,
  QUERY_ADD_CAR,
  ADD_TO_PROVINCES,
  QUERY_UPDATE_CAR,
  UPDATE_CAR,
  QUERY_DELETE_CAR,
  DELETE_CAR,
  QUERY_BANNERS,
  ADD_TO_BANNERS,
  QUERY_CAR_INFO,
  QUERY_VIOLATION_BRIEF,
  QUERY_BRAND_LIST,
  ADD_BRAND_LIST,
  QUERY_DRIVINGLICENCE,
  QUERY_DRIVINGLICENCE_LIST,
  QUERY_ADD_DRIVINGLICENCE,
  QUERY_UPDATE_DRIVINGLICENCE,
  QUERY_DELETE_DRIVINGLICENCE,
  UPDATE_DRIVINGLICENCE_DATA,
  QUERY_INSPECTION,
  DELETE_DRIVINGLICENCE_DATA,
  ADD_DRIVINGLICENCE_LIST,
  GET_CAR_INFORMATION,
  ADD_CAR_CONDITION_LIST,
  QUERY_CAR_LIST_ONLY,
  GET_AD,
} from '../actions/actionsTypes'


/*===========================获取车辆列表！===================================*/
function* fetchCarLists(data, callback) {

  let isGet = sessionStorage.getItem("isGet") //首次进入
  let isShowToast = sessionStorage.getItem("isShowToast") //回到首页是否显示加载中
  if (!isGet) {
    Toast.loading("", 0)
  } else if (isShowToast) {
    Toast.loading("", 0)
    sessionStorage.removeItem("isShowToast")
  }

  try {
    const result = yield call(carService.carList, data)
    Toast.hide()
    if (result.code == "1000") {
      let list = result.data.carViolationInfoList || []
      yield put({ type: ADD_CAR_LIST, data: list })
    } else {
      if (result.msg && result.code != "4222") {
        Toast.info(result.msg, 1);
      }
    }
    if (callback) {
      callback(result)
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
    if (callback) {
      callback(error)
    }
  }
}

function* watchCarLists() {
  while (true) {
    let { param, callback } = yield take(REQUEST_CAR_LIST)
    yield fork(fetchCarLists, param, callback)
  }
}

/*===========================获取车辆列表！(不包含违章)===================================*/
function* fetchCarListOnly(data, callback) {

  // let isGet = sessionStorage.getItem("isGet") //首次进入
  // // let isShowToast = sessionStorage.getItem("isShowToast") //回到首页是否显示加载中
  // if (!isGet) {
  //   Toast.loading("", 0)
  // } else if (isShowToast) {
  //   Toast.loading("", 0)
  //   sessionStorage.removeItem("isShowToast")
  // }
  Toast.loading("", 0)
  try {
    const result = yield call(carService.carListOnly, data)
    Toast.hide()
    if (result.code == "1000") {
      let list = result.data || []
      yield put({ type: ADD_CAR_LIST, data: list })
      if (callback) {
        callback(result)
      }
    } else {
      if (result.msg && result.code != "4222") {
        Toast.info(result.msg, 1);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
    if (callback) {
      callback(error)
    }
  }
}

function* watchCarListOnly() {
  while (true) {
    let { param, callback } = yield take(QUERY_CAR_LIST_ONLY)
    yield fork(fetchCarListOnly, param, callback)
  }
}

/*===========================获取违章查询条件信息！===================================*/
function* fetchCarCondition(data) {
  Toast.loading("", 0)
  try {
    let result;
    if (sessionStorage.getItem("carCondition")) {
      result = JSON.parse(sessionStorage.getItem("carCondition"))
    } else {
      result = yield call(carService.carCondition, data)
    }
    Toast.hide()
    if (result.code == "1000") {
      sessionStorage.setItem("carCondition", JSON.stringify(result)) //成功的时候 才保存，避免网络错误时，每次都提示网络错误
      let list = result.data.carConditionList || []
      yield put({ type: ADD_TO_PROVINCES, data: list })
    } else {
      Toast.info(result.msg || ChMessage.FETCH_FAILED, 1);
    }
    sessionStorage.getItem("carCondition")
    if (sessionStorage.getItem("carCondition") && sessionStorage.getItem("needJumpToEditCarInfo")) {
      sessionStorage.removeItem("needJumpToEditCarInfo")
      let hash = window.location.hash;
      let carId = new RegExp('carDetail\/([0-9]+)', 'g');
      carId = carId.exec(hash);
      carId = carId ? carId[1] : "";
      window.open(common.getRootUrl() + "editCar/inner/" + carId, '_self')
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchCarCondition() {
  while (true) {
    let { param } = yield take(QUERY_CAR_CONDITION_LIST)
    yield fork(fetchCarCondition, param)
  }
}

/*===========================添加车辆！===================================*/
function* fetchAddCar(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(carService.addCar, data)
    Toast.hide()
    if (result.code == "1000") {
      sessionStorage.setItem("isShowToast", 1)

      //点击保存成功埋点
      if (data.isSubscribe == 1) {//开启提醒
        common.sendCxytj({
          eventId: "Violation_SaveandCheck_OpenReminder"
        })
      } else if (data.isSubscribe == 0) {//关闭提醒
        common.sendCxytj({
          eventId: "Violation_SaveandCheck_CloseReminder"
        })
      }

      //添加车辆成功后向app广播更新车辆信息
      if (common.isCXYApp()) {
        jsApi.appUpdateCarMsg();
      }

      yield put({ type: ADD_CAR, data: { carNumber: data.carNumber, ...result.data, totalDegree: "?", totalFine: "?", untreatedNum: "?" } })
      window.history.go(-1);
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      } else {
        Toast.info(ChMessage.FETCH_FAILED, 1);
      }
    }
  } catch (error) {
    Toast.hide();
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchAddCar() {
  while (true) {
    let { param } = yield take(QUERY_ADD_CAR)
    yield fork(fetchAddCar, param)
  }
}

/*===========================删除车辆！===================================*/
function* fetchDeleteCar(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(carService.deleteCar, data)
    Toast.hide()
    if (result.code == "1000") {
      //删除车辆成功后向app广播更新车辆信息
      if (common.isCXYApp()) {
        jsApi.appUpdateCarMsg();
      }
      yield put({ type: DELETE_CAR, data: { carId: data.carId } })
      let hash = window.location.hash
      if (hash.indexOf("editCar") != -1) {
        sessionStorage.removeItem("showConfirm")
      }
      sessionStorage.setItem('deleteCar-' + data.carId, 1) //定义临时缓存给外部页面使用
      // window.history.go(-1);
      jsApi.back();
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      } else {
        Toast.info(ChMessage.FETCH_FAILED, 1);
      }
    }
  } catch (error) {
    console.error(error)
    Toast.hide();
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchDeleteCar() {
  while (true) {
    let { param } = yield take(QUERY_DELETE_CAR)
    yield fork(fetchDeleteCar, param)
  }
}

/*===========================修改车辆！===================================*/
function* fetchUpdateCar(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(carService.updateCar, data)
    Toast.hide()
    if (result.code == "1000") {
      yield put({ type: UPDATE_CAR, data: data })
      //修改车辆成功后向app广播更新车辆信息
      if (common.isCXYApp()) {
        jsApi.appUpdateCarMsg();
      }

      let hash = window.location.hash
      if (hash.indexOf("replenishInfo") != -1) {//当前页面是补充资料页面
        sessionStorage.setItem("isShowToast", 1)
        //点击修改按钮成功埋点
        if (data.isSubscribe == 1) {//开启提醒
          common.sendCxytj({
            eventId: "Violation_Save_OpenReminder"
          })
        } else {//关闭提醒
          common.sendCxytj({
            eventId: "Violation_Save_CloseReminder"
          })
        }
      }
      sessionStorage.removeItem("showConfirm")
      // window.history.go(-1);
      jsApi.back();
    } else {
      Toast.info(result.msg || ChMessage.FETCH_FAILED, 1);
    }
  } catch (error) {
    Toast.hide();
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }
}

function* watchUpdateCar() {
  while (true) {
    let { param } = yield take(QUERY_UPDATE_CAR)
    yield fork(fetchUpdateCar, param)
  }
}

/*==========================查询车辆信息============================*/
function* fetchCarInfo(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(carService.carInfo, data)
    Toast.hide()
    if (result.code == "1000") {
      yield put({ type: ADD_CAR, data: result.data })
      sessionStorage.removeItem("isQueryCarInfo");

      //鉴定当前页面信息
      let hash = window.location.hash
      if (hash.indexOf("carDetail") != -1) {
        if (result.data.drivingLicenseId) {
          yield put({ type: QUERY_DRIVINGLICENCE, param: { drivingLicenseId: result.data.drivingLicenseId, carId: result.data.carId } })
        }
        if (result.data.registerDate && result.data.checkDate) {
          yield put({ type: QUERY_INSPECTION, param: { carId: result.data.carId } })
        }
        if (sessionStorage.getItem("carCondition") && sessionStorage.getItem("needJumpToEditCarInfo")) {
          sessionStorage.removeItem("needJumpToEditCarInfo")
          window.open(common.getRootUrl() + "editCar/inner/" + data.carId, '_self')
        }
      }
    } else {
      sessionStorage.removeItem("isQueryCarInfo");
      Toast.info(result.msg || ChMessage.FETCH_FAILED, 1);
    }
  } catch (error) {
    sessionStorage.removeItem("isQueryCarInfo");
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchCarInfo() {
  while (true) {
    let { param } = yield take(QUERY_CAR_INFO)
    yield fork(fetchCarInfo, param)
  }
}

/*============================获取广告信息==================================*/
function fetchBannersApi(data) {
  let requestParam = {
    url: `${apiHelper.baseApiUrl}violation/car/list`,
    data: {
      method: 'get',
      body: data
    }
  };

  return apiHelper.fetch(requestParam);
}

function* fetchBanners(data) {

  try {
    const result = yield call(fetchBannersApi, data)
    if (result.code == "1000") {
      yield put({ type: ADD_TO_BANNERS, data: result.data })
    } else {
      // if (result.msg && result.code != "4222") {
      //   Toast.info(result.msg, 1);
      // }
    }
  } catch (error) {
    // Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchBanners() {
  while (true) {
    let { param } = yield take(QUERY_BANNERS)
    yield fork(fetchBanners, param)
  }
}

/*============================查询车辆违章概要信息==================================*/
function* fetchViolationBrief(data) {
  Toast.loading("", 0)
  console.log("a1")
  try {
    const result = yield call(carService.violationBrief, data)
    Toast.hide();
    console.log("_a1")
    sessionStorage.setItem("isQueryViolationDataDone", "1");
    if (result.code == "1000") {
      yield put({ type: UPDATE_CAR, data: result.data })
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      }
    }
  } catch (error) {
    Toast.hide();
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }
}

function* watchViolationBrief() {
  while (true) {
    let { param } = yield take(QUERY_VIOLATION_BRIEF)
    yield fork(fetchViolationBrief, param)
  }
}

/*==========================车辆年检信息============================*/
function* fetchInspection(data) {
  try {
    const result = yield call(carService.inspectionBrief, data)
    sessionStorage.setItem("isQueryAnnualDataDone", "1");
    if (result.code == "1000") {
      //添加到车辆信息中
      let info = { carId: data.carId, ...result.data }
      yield put({ type: UPDATE_CAR, data: info })
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      }
    }
  } catch (error) {
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchInspection() {
  while (true) {
    let { param } = yield take(QUERY_INSPECTION)
    yield fork(fetchInspection, param)
  }
}

/*============================查询车辆品牌信息==================================*/
function* fetchBrand(data) {
  try {
    const result = yield call(carService.carBrand, data)
    Toast.hide()
    if (result.code == "1000") {
      yield put({ type: ADD_BRAND_LIST, data: result.data })
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }
}

function* watchBrand() {
  while (true) {
    let { param } = yield take(QUERY_BRAND_LIST)
    yield fork(fetchBrand, param)
  }
}

/*==========================查询单个驾照信息============================*/
function fetchDrivingLicenceApi(data) {
  let requestParam = {
    url: `${apiHelper.baseApiUrl}drivingLicence/view`,
    data: {
      method: 'post',
      body: data
    }
  };

  return apiHelper.fetch(requestParam);
}

function* fetchDrivingLicence(data) {

  try {
    const result = yield call(fetchDrivingLicenceApi, data)
    //驾照信息请求完成
    sessionStorage.setItem("isQueryDrivingLicenceDataDone", "1")
    if (result.code == "1000") {
      yield put({ type: UPDATE_DRIVINGLICENCE_DATA, data: result.data })
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      }
    }
  } catch (error) {
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchDrivingLicence() {
  while (true) {
    let { param } = yield take(QUERY_DRIVINGLICENCE)
    yield fork(fetchDrivingLicence, param)
  }
}

/*==========================查询驾照列表信息============================*/
function fetchDrivingLicenceListApi(data) {
  let requestParam = {
    url: `${apiHelper.baseApiUrl}drivingLicence/list`,
    data: {
      method: 'post',
      body: data
    }
  };

  return apiHelper.fetch(requestParam);
}

function* fetchDrivingLicenceList(data) {
  try {
    const result = yield call(fetchDrivingLicenceListApi, data)
    Toast.hide()
    if (result.code == "1000") {
      yield put({ type: ADD_DRIVINGLICENCE_LIST, data: result.data })
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchDrivingLicenceList() {
  while (true) {
    let { param } = yield take(QUERY_DRIVINGLICENCE_LIST)
    yield fork(fetchDrivingLicenceList, param)
  }
}

/*==========================添加驾照信息============================*/
function fetchAddDrivingLicenceApi(data) {
  let requestParam = {
    url: `${apiHelper.baseApiUrl}drivingLicence/save`,
    data: {
      method: 'post',
      body: data
    }
  };

  return apiHelper.fetch(requestParam);
}

function* fetchAddDrivingLicence(data) {
  Toast.loading("", 0);
  try {
    const result = yield call(fetchAddDrivingLicenceApi, data)
    Toast.hide()
    if (result.code == "1000") {
      sessionStorage.removeItem("showConfirm")
      window.history.go(-1)
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      } else {
        Toast.info(ChMessage.FETCH_FAILED, 1);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchAddDrivingLicence() {
  while (true) {
    let { param } = yield take(QUERY_ADD_DRIVINGLICENCE)
    yield fork(fetchAddDrivingLicence, param)
  }
}

/*==========================修改驾照信息============================*/
function fetchUpdateDrivingLicenceApi(data) {
  let requestParam = {
    url: `${apiHelper.baseApiUrl}drivingLicence/update`,
    data: {
      method: 'post',
      body: data
    }
  };

  return apiHelper.fetch(requestParam);
}

function* fetchUpdateDrivingLicence(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(fetchUpdateDrivingLicenceApi, data)
    Toast.hide()
    if (result.code == "1000") {
      //yield put({type:UPDATE_DRIVINGLICENCE_DATA,data:result.data}) 
      sessionStorage.removeItem("showConfirm");
      window.history.go(-1)
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      } else {
        Toast.info(ChMessage.FETCH_FAILED, 1);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }
}

function* watchUpdateDrivingLicence() {
  while (true) {
    let { param } = yield take(QUERY_UPDATE_DRIVINGLICENCE)
    yield fork(fetchUpdateDrivingLicence, param)
  }
}

/*==========================删除驾照信息============================*/
function fetchDeleteDrivingLicenceApi(data) {
  let requestParam = {
    url: `${apiHelper.baseApiUrl}drivingLicence/delete`,
    data: {
      method: 'post',
      body: data
    }
  };

  return apiHelper.fetch(requestParam);
}

function* fetchDeleteDrivingLicence(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(fetchDeleteDrivingLicenceApi, data)
    Toast.hide()
    if (result.code == "1000") {
      //删除驾照列表中数据
      yield put({ type: DELETE_DRIVINGLICENCE_DATA, data: { drivingLicenseId: data.drivingLicenseId } })
      sessionStorage.removeItem("showConfirm")
      window.history.go(-1)
    } else {
      if (result.msg) {
        Toast.info(result.msg, 1);
      } else {
        Toast.info(ChMessage.FETCH_FAILED, 1);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
  }
}

function* watchDeleteDrivingLicence() {
  while (true) {
    let { param } = yield take(QUERY_DELETE_DRIVINGLICENCE)
    yield fork(fetchDeleteDrivingLicence, param)
  }
}

/*==========================根据车牌号码获取相关信息============================*/
function* getCarInformation(action) {
  Toast.loading('', )
  try {
    const { result, timeout } = yield race({
      result: call(carService.information, action.data),
      timeout: call(delay, 30000)
    })

    Toast.hide()
    if (timeout) {
      Toast.info(ChMessage.FETCH_FAILED)
      return;
    } else {
      if (result.code == '1000') {

      } else {
        Toast.info(result.msg || ChMessage.FETCH_FAILED)
      }
      if (action.callback) {
        action.callback(result)
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
    if (action.callback) {
      action.callback(error)
    }
  }
}
function* watchGetCarInformation() {
  yield takeLatest(GET_CAR_INFORMATION, getCarInformation)
}

/*==========================获取违章查询条件信息-引导注册============================*/
function* fetchCarConditionList(action) {
  Toast.loading("", 0)
  try {
    let result;
    if (sessionStorage.getItem("carCondition")) {
      result = JSON.parse(sessionStorage.getItem("carCondition"))
    } else {
      const { _result, timeout } = yield race({
        _result: call(carService.carCondition, action.data),
        timeout: call(delay, 30000)
      })
      result = _result

      if (timeout) {
        window.networkError('./images/networkError-icon.png');
        return;
      }
    }
    Toast.hide()
    if (result.code == "1000") {
      sessionStorage.setItem("carCondition", JSON.stringify(result)) //成功的时候 才保存，避免网络错误时，每次都提示网络错误

      //提取城市
      const citi = new schema.Entity('citi', {}, { idAttribute: 'carNumberPrefix' });
      const citis = new schema.Array(citi)
      const province = { cities: citis }
      const provinces = new schema.Array(province)
      let normalizedData = normalize(result.data.carConditionList, provinces)

      //提取省份
      const newProvince = new schema.Entity('province', {}, { idAttribute: 'provincePrefix' })
      const newProvinces = new schema.Array(newProvince)
      const newNormalizedData = normalize(normalizedData.result, newProvinces)

      yield put({
        type: ADD_CAR_CONDITION_LIST, data: {
          data: normalizedData.entities.citi,
          result: newNormalizedData.result
        }
      })

      if (action.callback) {
        action.callback({
          code: '1000',
          data: normalizedData.entities.citi,
          result: newNormalizedData.result
        })
      }
    } else {
      Toast.info(result.msg || ChMessage.FETCH_FAILED, 1);
      if (action.callback) {
        action.callback({
          code: 'failed',
          data: {},
          result: []
        })
      }
    }

  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 1);
    if (action.callback) {
      action.callback(error)
    }
  }

}

function* watchCarConditionList() {
  yield takeLatest(GET_CAR_CONDITION_LIST, fetchCarConditionList)
}

/*============================获取广告信息(车辆详情页和违章页)==================================*/
function* fetchGetAd(data, callback) {

  try {
    if (!window.myCache_fetchGetAd) {
      window.myCache_fetchGetAd = {}
    }

    const { positionCode } = data;
    
    const result = yield window.myCache_fetchGetAd[positionCode] || call(carService.getAd, data)

    if (result.code == "1000") {
      window.myCache_fetchGetAd[positionCode] = result;
      if (callback) {
        callback(result)
      }
    } else {
      // if (result.msg && result.code != "4222") {
      //   Toast.info(result.msg, 1);
      // }
    }
  } catch (error) {
    // Toast.info(ChMessage.FETCH_FAILED, 1);
  }

}

function* watchAds() {
  while (true) {
    let { data, callback } = yield take(GET_AD)
    yield fork(fetchGetAd, data, callback)
  }
}


/*********监听所有车辆相关请求！*********/
export function* watchCars() {
  yield [
    fork(watchCarLists),
    fork(watchCarCondition),
    fork(watchCarConditionList), //引导注册中的获取车牌前缀
    fork(watchAddCar),
    fork(watchBanners),
    fork(watchCarInfo),
    fork(watchViolationBrief),
    fork(watchDeleteCar),
    fork(watchUpdateCar),
    fork(watchDrivingLicence),
    fork(watchDrivingLicenceList),
    fork(watchAddDrivingLicence),
    fork(watchUpdateDrivingLicence),
    fork(watchDeleteDrivingLicence),
    fork(watchInspection),
    fork(watchBrand),
    fork(watchGetCarInformation),
    fork(watchCarListOnly),
    fork(watchAds),
  ]
}