import styles from './handle.scss';

//配置
import config from '../../config';

//违章办理 - 车辆违章列表
export const CarViolationList = props => {
    let { list, msg, carNumber, carId, clickFooter, toViolationList, toViolationDetail } = props;
    list = list || [];
    carNumber = carNumber || '';

    return (
        <div className={styles.box}>
            <div className={styles.top} onClick={() => toViolationList && toViolationList()}>
                <span className={styles.carNumber}>{carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}</span>
                {
                    list.length > 0
                        ? <span className={styles.handleBtn}>去办理</span>
                        : <span className={styles.topText}>{msg || '暂无需办理违章'}</span>
                }
            </div>
            <ul className={list.length > 0 ? styles.list : 'hide'}>
                {
                    list.map((item, i) =>
                        <li key={'car-' + i} onClick={() => toViolationDetail && toViolationDetail(`/violationDetail/${item.violationId}/${carId}`)}>
                            <h4 className='text-overflow-1'>{item.locationName + item.location}</h4>
                            <p>
                                <i>{item.occurTime.substr(0, 10)}</i>
                                <i className={styles.reason}>{item.reason}</i>
                                <span>罚款{item.fine}元</span>
                                <span className={styles.line}>|</span>
                                <span>{item.degree > 0 ? `扣${item.degree}分` : '无扣分'}</span>
                            </p>
                        </li>
                    )
                }
            </ul>
            <div className={list.length > 0 ? styles.footer : 'hide'} onClick={() => toViolationList && toViolationList()}>
                <span className={styles.moveBtn}>查看更多</span>
            </div>
        </div>
    )
}