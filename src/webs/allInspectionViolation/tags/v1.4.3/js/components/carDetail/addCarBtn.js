/*
*	车辆详情页添加车辆按钮
*/

import styles from './addCarBtn.scss'

const defaultProps = {
	handleClick:()=>{},
}

const AddCarBtn = props => {
	// props = defaultProps
	return (
		<div className={styles.wrap} onClick={props.handleClick}>
			添加车辆
		</div>
	)
}

export default AddCarBtn