/*
 * 首页违章概要信息底部提示
 * 
 */

import styles from './index.scss'

const Msg = props => {
	return (
		<div className={styles.wrap}>
			<div className={styles.msgWrap}>
				<div className={styles.leftIcon}></div>
				<div className={styles.content}>添加车辆已达上限</div>
			</div>
		</div>
	)
}

export default Msg