import styles from './index.scss'

const defaultProps = {
	rightText: "补充资料",
	totalFine: "?",
	totalDegree: "?",
	untreatedNum: "?",
	carNumber: "粤A23456",
}

const Item = props => {
	// props = defaultProps
	console.log('---------------------', props)
	return (
		<div style={{ background: "#fff" }} onClick={() => { props.handleClick(props) }}>
			<div className={styles.carNumberWrap}>
				<div className={styles.leftIcon}></div>
				<div className={styles.carNumber}>{props.carNumber.slice(0, 2) + " " + props.carNumber.slice(2)}</div>
				{props.rightText && <div className={styles.rightText}>{props.rightText}</div>}
				<div className={styles.rightIcon}></div>
			</div>
			<div className={styles.hr}></div>
			<div className={styles.violationDetail}>
				<div className={styles.commonItem}>
					<div>{props.untreatedNum}</div>
					<div>违章数</div>
				</div>
				<div className={styles.commonItem}>
					<div>{props.totalFine}</div>
					<div>待罚款(元)</div>
				</div>
				<div className={styles.commonItem}>
					<div>{props.totalDegree}</div>
					<div>待扣分</div>
				</div>
			</div>
		</div>
	)
}

export default Item