import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as carListActions from '../../actions/carListActions'
import { ListCars, ListKey } from 'app/components/selectCar/'
import Style from './carbrand.scss'
import { Toast } from 'antd-mobile'
import apiHelper from '../../services/apiHelper'

class SelectCar extends Component {
	constructor(props) {
		super(props);

		let state = {
			top: 0,
			keyTops: [], //所有序号的scrollTop位置
			keyName: "", //当前点击的key
			showKeyName: false, //是否显示key
			carBrand:"",
			data: {
				carClass: {
					keyName: '',
					data: {
						img: '',
						title: ''
					}
				},
				carType: {
					keyName: '',
					data: {
						img: '',
						title: ''
					}
				}
			},
			ListCars: {list:[]}, //车系列表
			ListKey: { list: [] }, //字母索引列表
			cars: { list: [] }, //车型列表
		}

		this.state = this.getNewState(state,this.props)
	}

	//获取新state
	getNewState(state,props){
		let newState = {}
		let carBrands = props.carBrand.carBrands

		if(carBrands && carBrands.length!=0){
			newState = this.changeBrandList(carBrands)
			this.getScrollTop(newState.ListCars.list)
		}

		return {...state,...newState}
	}

	toUrl() {
		this.context.router.push('/edit');
	}

	//获取点击的序号
	getKeyName(keyName) {
		this.refs.ListCars.scrollTop = this.state.keyTops[keyName]; //页面滚动到指定位置
		this.setState({
			keyName: keyName,
			showKeyName: true
		});
		setTimeout(() => {
			this.setState({
				showKeyName: false
			});
		}, 300);
	}

	//页面滚动
	scroll(i) {
		let top = this.refs.ListCars.scrollTop;
	}

	//获取所有序号的scrollTop位置
	getScrollTop(list) {
		let tops = {};
		let top = 0;
		let top_temp = 0;
		let t_h = 35; //序号高度
		let c_h = 44; //单个车系的高度
		list.map((item, i) => {
			if(i === 0){
				top = 0;
			}else{
				top = top_temp
			}
			top_temp += t_h + (item.list.length * c_h); //上一个序号的总高度
			tops[item.keyName] = top;
		});
		setTimeout(()=>{
		this.setState({
			keyTops:tops
		})
		},10)
	}

	//隐藏车型列表
	hideCars() {
		this.refs.Cars.style.display = "none";
		this.setState({
			cars:{
				list:[]
			}
		})
	}

	//显示车型列表
	showCars(data) {
		let brandId = data.data.id;
		this.setState({
			carBrand:data.data.title
		})
		this.getModelList(brandId);
		this.refs.Cars.style.display = "block";
	}

	//获取车辆信息
	getCar(data) {
		let carBrand = this.state.carBrand;
		let model = data.data.title.replace(carBrand,"");
		//alert(this.props.params.carId);
		if(!this.props.params.carId){
			let state = JSON.parse(sessionStorage.getItem("editCarState"));
			if(state.carModelId != data.data.modelId){
				state.isDirty = true
			}
			state.carModelId = data.data.modelId;
			state.carModelName = carBrand+" "+model;
			sessionStorage.setItem("editCarState",JSON.stringify(state));
			window.history.go(-1);
		}else{
			let param = {};
			let {cars} = this.props;
			let obj = cars.data[this.props.params.carId];
			param.carId = this.props.params.carId;
			param.carModelId = data.data.modelId;
			param.carModelName = carBrand+" "+model;
			param.carCode = obj.carCode;
			param.engineNumber = obj.engineNumber;
			param.carNumber = this.props.params.carNumber
			this.props.actions.queryUpdateCar(param);
		}
	}

	componentWillMount() {
		document.querySelector("title").innerHTML = "选择车型";

		//加载品牌数据
		let carBrands = this.props.carBrand.carBrands
		if(!carBrands){
			Toast.loading("",0)
		}
		this.props.actions.queryBrandList()

		// let state = sessionStorage.getItem("editCarState");
		// alert(state);

		//模拟数据
		// this.setState({
		// 	ListCars: this.props.ListCars, //车系列表
		// 	ListKey: this.props.ListKey, //字母索引列表
		// 	cars: this.props.cars, //车型列表
		// })
	}

	componentWillReceiveProps(nextProps){
		let nextState = this.getNewState(this.state,nextProps);
		this.setState(nextState)
	}

	componentDidUpdate(){
		//this.getScrollTop();
	}

	componentWillUnmount() {
		//离开车系选择页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

	// 获取车辆品牌信息
	getBrandList(){
		let requestParam = {
		    url: `${apiHelper.baseApiUrl}car/brandList`,
		    data: {
		        method: 'get',
		    }
	    };
		    
		apiHelper.fetch(requestParam).then(result=>{
			//将数据转换成目标格式
			if(result.code==1000){
				let carBrands = result.data.carBrands
				let state = this.changeBrandList(carBrands)
				this.setState({...this.state,...state})
				this.getScrollTop(state.ListCars.list)
			}else{
				if(result.msg){
					Toast.info(result.msg,1);
				}				
			}
		}).catch(e=>{

		});
	}

	//转变车牌数据格式
	changeBrandList(data){
		let keyList = []//保存首字母
		let list = [] //临时对象

		//获取字母列表
		for (let i = 0; i < data.length; i++) {
			let letter = data[i].sortLetter
			keyList = Array.from(new Set([...keyList,letter]))
		}

		//转换车牌数据
		for (var i = 0; i < keyList.length; i++) {
			let obj = {}
			obj.keyName = keyList[i] 
			let tmp = data.filter(function(item){
				return item.sortLetter == keyList[i]	
			})
			obj.list = tmp.map((item)=>{
				return {
					img:item.logoFileName,
					title:item.brand,
					id:item.id,
				}
			})
			list.push(obj)
		}
		return {
			ListKey:{
				list:keyList
			},
			ListCars:{
				list:list
			}
		}
	}

	//获取车型信息
	getModelList(brandId){
		let requestParam = {
		    url: `${apiHelper.baseApiUrl}car/modelList`,
		    data: {
		        method: 'get',
		        body:{
		        	brandId,
		        }
		    }
	    };
		    
		apiHelper.fetch(requestParam).then(result=>{
			//将数据转换成目标格式
			if(result.code=="1000"){
				let data = result.data.carModels
				let state = this.changeModelList(data);
				this.setState({...this.state,...state});
			}
		}).catch(e=>{

		});
	}

	//转变车型数据格式
	changeModelList(data){
		let list = [];

		//转换车型数据
		list = data.map(item=>{
			let tmp = item.modelList.map(item=>{
				return {
					modelId:item.modelId,
					img:item.imgUrl,
					title:item.model,
				}
			})
			return {
				keyName:item.brandType,
				list:tmp
			}
		})
		return {
			cars:{
				list:list,
			}
		}
	}

	render() {
		let param = this.props.params.param;
		return (
			<div className={Style.fiexd100}>
				<div className={Style.container}>
					<div ref="ListCars" className={Style.left}>
						{this.state.ListCars.list.length > 0 ?
							<ListCars {...this.state.ListCars} onClick={(data) => this.showCars(data)} />
							: ''
						}
					</div>
					<div className={Style.right}>
						<ListKey {...this.state.ListKey} touchStart={(keyName) => this.getKeyName(keyName)} />
					</div>
					<div className={Style.showKeyName} style={this.state.showKeyName ? { "display": "block" } : {}}>{this.state.keyName}</div>
					<div ref="Cars" className={Style.left + " " + Style.cars} onClick={() => this.hideCars()}>
						<ListCars {...this.state.cars} onClick={(data) => this.getCar(data)} />
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars:state.cars,
	carBrand:state.carBrand
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SelectCar)

//使用context
SelectCar.contextTypes = {
	router: React.PropTypes.object.isRequired
}

// SelectCar.defaultProps = {
// 	ListCars: {
// 		list: [{
// 			keyName: "A",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奔驰"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马"
// 			}]
// 		}, {
// 			keyName: "B",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奔驰2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马2"
// 			}]
// 		}, {
// 			keyName: "C",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奔驰3"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马3"
// 			}]
// 		}, {
// 			keyName: "D",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奔驰4"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马4"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马4"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马4"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马4"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马4"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马4"
// 			}]
// 		}, {
// 			keyName: "E",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奔驰5"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马5"
// 			}]
// 		}, {
// 			keyName: "F",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奔驰6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "宝马6"
// 			}]
// 		}]
// 	},
// 	ListKey: {
// 		list: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
// 	},
// 	cars: {
// 		list: [{
// 			keyName: "一汽 - 大众奥迪",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奥迪A4L"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "奥迪A6L"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "奥迪Q3"
// 			}]
// 		}, {
// 			keyName: "奥迪进口",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奥迪Q7"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S5"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S8"
// 			}]
// 		}, {
// 			keyName: "奥迪进口2",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奥迪Q7"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S5"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S8"
// 			}]
// 		}, {
// 			keyName: "奥迪进口3",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奥迪Q7"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S5"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S8"
// 			}]
// 		}, {
// 			keyName: "奥迪进口4",
// 			list: [{
// 				img: 'http://img5.imgtn.bdimg.com/it/u=3747381357,1005215364&fm=21&gp=0.jpg',
// 				title: "奥迪Q7"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S5"
// 			},
// 			{
// 				img: 'http://img0.imgtn.bdimg.com/it/u=3828647692,1403014506&fm=21&gp=0.jpg',
// 				title: "S8"
// 			}]
// 		}]
// 	}
// }