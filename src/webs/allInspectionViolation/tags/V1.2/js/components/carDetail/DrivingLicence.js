/*
*	驾照卡片信息组件
	props = {
		drivingLicenceId:"",//驾照id
		handleClick:()=>{}
	}
*/

import styles from './index.scss'
import WarningMsg from './WarningMsg'

const defaultProps = {
	carId:"",
	drivingLicenseId:"",//驾照id
	title:"驾照查分",
	content:{__html:"查看驾照余分，到期清零提醒"},
	rightText:"添加驾照",
	drivingWarningMsg:"查看驾照余分，到期清零提醒",
	type:"add",
	handleClick:()=>{},
}

const DrivingLicence = props => {
	// props = defaultProps;
	//alert(props.licenceWraningMsg)
	return (
		<div>
			<div className={styles.wrap} onClick={()=>{props.handleClick(props)}}>
				<div className={styles.leftText}>
					<div className={styles.title}>{props.title}</div>
					<div className={styles.content} dangerouslySetInnerHTML={props.content}></div>
				</div>
				<div>
					<span className={styles.rightText}>{props.rightText}</span><i className={styles.rightIcon}></i>
				</div>
			</div>
			<WarningMsg text = {props.drivingWarningMsg} id="drivingWarningMsg"/>
		</div>
	)
}

export default DrivingLicence