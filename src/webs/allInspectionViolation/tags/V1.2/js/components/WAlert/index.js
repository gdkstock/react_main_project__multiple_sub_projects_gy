import React, { Component } from 'react'
import Style from './index.scss'

export default class WAlert extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	close() {
		this.props.onClose();
	}

	showVipTexts() {
		let texts = [
			'有效期内违章代办/罚款代缴办理免服务费',
			'有效期前1年内的违章代办/代缴享受服务费5折',
			'车辆6年免检代办服务费低至0元',
			'购买人保车险时享受每满1000元减260元',
			'优先使用充值88折套餐优惠的名额',
			'开通加急办理的城市免加急费用',
			'未开通加急办理的城市享受优先处理权益',
			'福利栏目优惠专享，各种活动无门槛参与'
		]

		return texts.map((item, i) => (
			<p key={'text' + i}><span className={Style.wIconO}></span>{item}</p>
		))
	}

	showUrgentTexts() {
		let texts = [
			'加急业务仅在工作日（周一至周五08:00-17:00）受理，周末及节假日不受理',
			'加急业务仅支持粤牌、京牌、闽牌及苏A牌（南京）车辆的违章',
			'加急业务为两小时内办结订单，消网信息以官网数据刷新显示为准',
			'如加急订单无法在两小时内办结  我司将退还加急服务费',
			'最终业务解释权归车行易所有'
		]

		return texts.map((item, i) => (
			<p key={'text' + i}><span className={Style.wIconO}></span>{item}</p>
		))
	}

	render() {
		let alerts = [
			<div className={Style.wAlert} ref="alertBox">
				<div className={Style.wAlertBg}></div>
				<div className={Style.wAlertBody + " " + Style.flexV}>
					<div className={Style.wAlertTitle}>
						<span className={Style.wIcon + " " + Style.wIconVip}></span>
						<span>会员（易卡通） 188元/年</span>
					</div>
					<div className={Style.wAlertContent + " " + Style.flex1}>
						{this.showVipTexts()}
						<p className={Style.fontSize24}>注：具体会员减免金额根据用户办理的会员卡类型而定</p>
					</div>
					<div onClick={() => this.close()} className={Style.wAlertFooter}><span>我知道了</span></div>
				</div>
			</div>,
			<div className={Style.wAlert} ref="alertBox">
				<div className={Style.wAlertBg}></div>
				<div className={Style.wAlertBody + " " + Style.flexV}>
					<div className={Style.wAlertTitle}>
						<span className={Style.wIcon + " " + Style.wIconVip}></span>
						<span>加急业务说明</span>
					</div>
					<div className={Style.wAlertContent + " " + Style.flex1}>
						{this.showUrgentTexts()}
					</div>
					<div onClick={() => this.close()} className={Style.wAlertFooter}><span>我知道了</span></div>
				</div>
			</div>
		]

		return (
			<div>
				{alerts[this.props.num]}
			</div>
		)
	}
}

WAlert.defaultProps = {
	num: 0, //0：未购买车务卡说明；1：加急处理
	onClose: () => false //点击了关闭按钮
}