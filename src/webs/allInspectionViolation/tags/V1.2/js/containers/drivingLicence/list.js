/**
 * 驾照列表页面
 */
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn } from '../../components'
import WarningMsg from '../../components/carDetail/WarningMsg'
import { LicenceCard } from '../../components/drivingLicence'
import * as carListActions from '../../actions/carListActions'
import common from '../../utils/common'
import styles from './list.scss'
import { Toast } from 'antd-mobile'

class DrivingLicenceList extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {

		}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('驾照信息');

		let list = this.props.drivingLicence.result
		if(list.length==0){
			Toast.loading("",0)
			window.history.go(-1)
		}

		let carId = this.props.params.carId;

		//请求车辆信息为了本人本车更新数据
		this.props.actions.queryCarInfo({carId})

		//请求驾照列表数据
		this.props.actions.queryDrivingLicenceList()
	}

	componentWillReceiveProps(nextProps){
	}

	componentDidMount(){
		//进入驾照列表页埋点
		common.sendCxytj({
			eventId:"Violation_EnterDrivingLicence"
		})
	}

	componentWillUnmount() {
		//离开驾照列表页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

	componentDidUpdate(){
		//组件更新完成
		this.createCircle();
	}

	//生成圆环
	createCircle(){
		let circleList = document.querySelectorAll(".circle");
		for (var j = 0; j < circleList.length; j++) {
			let circle = circleList[j]
			let child = circle.firstElementChild
			if(circle){
				let score = 12-circle.firstElementChild.innerHTML;//获取扣分数
				let num = score*30;
				let D = circle.clientWidth;//外圆直径
				let d = child.clientWidth;//内圆直径
				let r = d/2+(D-d)/4 ;//圆环半径
				let html = "";
				for(let i=0;i<num;i++){
			        html+=`<div class="unit" style="-webkit-transform:rotate(${i}deg) translateY(-${r}px);transform:rotate(${i}deg) translateY(-${r}px)"></div>`
			    }
			    circle.innerHTML = circle.innerHTML+html;
			}
		}
		
	}

	//打开编辑驾照
	openEditLicence(props){
		let carId = this.props.params.carId
		let licenceId = props.drivingLicenseId
		window.open(common.getRootUrl()+"editDrivingLicence/"+carId+"/"+licenceId,'_self')
	}

	//生成驾照列表数据
	createLicenceList(){
		let carId = this.props.params.carId
		let { drivingLicence ,cars} = this.props;
		let list = drivingLicence.result;
		let data = drivingLicence.data;
		let licenceId;
		if(cars.data[carId]){
			licenceId = cars.data[carId].drivingLicenseId || "";
		}
		let outPutList = []

		let tmp = list.map(item=>{
			let tmpObj = {}
			if(data[item].drivingLicenseId == licenceId){
				tmpObj = {
					...data[item],
					isOwner:true,
				}
				outPutList.unshift(tmpObj)
			}else{
				tmpObj = {
					...data[item],
					isOwner:false,
				}
				outPutList.push(tmpObj)
			}
		})

		return outPutList
	}

	//添加驾照
	addLicence(){
		let carId = this.props.params.carId

		//点击添加驾照埋点
		common.sendCxytj({
			eventId:"Violation_AddDrivingLicence_List"
		})

		window.open(common.getRootUrl()+"addDrivingLicence/"+carId,'_self')
	}

	render(){
		//顶部提示信息props
		const WarningMsgProps = {
			text:"该服务目前仅支持广东全省，其他城市将尽快开通，敬请期待",
			hasBrodcastIcon:true,
			id:"drivingInfo",
		}

		//驾照列表数据
		const drivingLiceceList = this.createLicenceList()

		const btnProps = {
			text:"添加驾照",
			disabled:drivingLiceceList.length>=5?true:false,
			handleClick:()=>this.addLicence()
		}

		return (
			<div className={styles.wrap}>
				<WarningMsg {...WarningMsgProps}/>
				<div className={styles.line}></div>
				{
					drivingLiceceList.length>0 && drivingLiceceList.map((item,i)=>
						<div key={i}>
							<div className='wz_whiteSpace_30'></div>
							<LicenceCard {...item} handleClick={(o)=>this.openEditLicence(o)}/>
						</div>
					)
				}		
				<div className={styles.btnWrap}>
					<Btn {...btnProps}/>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars:state.cars,
	drivingLicence: state.drivingLicence,
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(DrivingLicenceList)