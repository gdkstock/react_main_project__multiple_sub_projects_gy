/*
* 添加驾照
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn ,Modal} from '../../components'
import { TxtGroup ,DateGroup} from '../../components/Form'
import styles from './addLicence.scss'
import * as carListActions from '../../actions/carListActions'
import { Toast } from 'antd-mobile'
import moment from 'moment'
import common from '../../utils/common'

class AddLicence extends React.Component{
	constructor(props){
		super(props);

		let carId = this.props.params.carId || "";
		this.state = {
			carId,
			driverName:"",
			drivingLicenseNo:"",
			drivingFileNumber:"",
			firstLicensingDate:"",
			drivingTelephone:"",
		}
	}


	componentWillMount(){
		//设置标题
		common.setViewTitle('添加驾照');

		//请求车辆信息
		let carId = this.props.params.carId;
		let { cars } = this.props;
		if(!cars.data[carId]){
			this.props.actions.queryCarInfo({carId})
		}	
	}

	componentDidMount(){
		//组件加载完成

		//后退时显示确认对话框
		common.backShowConfirm(() => {
			let obj = {
				show: true, //显示对话框
				content: '编辑的信息尚未保存，确定退出？', //内容
				leftBtn: {
					name: '离开', //左按钮文字
					onClick: () => {sessionStorage.removeItem("showConfirm");history.back()}  //点击左按钮
				},
				rightBtn: {
					name: '取消',//右按钮文字
					onClick: () => history.forward() //点击右按钮
				},
				onClose: () => history.forward() //点击关闭按钮
			}
			Modal.confirm(obj);
		})
	}

	componentDidUpdate(){
		//如果编辑驾照信息
		if(this.state.isDirty){
			sessionStorage.setItem("showConfirm",1);
		}
	}

	componentWillUnmount() {
		//离开添加驾照页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

	//输入改变时验证数据
	preValiDate(e){
		let target = e.target,
			name = target.name, 
			len,
			value,
			isDirty = this.state.Dirty,
			currValue = target.value.trim();
		switch(name){
			case "driverName":
				len = 32;
				break;
			case "drivingLicenseNo":
				currValue = currValue.toUpperCase();
				len = 18;
				break;
			case "drivingFileNumber":
				currValue = currValue.toUpperCase();
				len = 12;
				break;
			case "drivingTelephone":
				len = 11;
				currValue = currValue.replace(/[^\d]/,'')
				break;
		}
		value = this.cutInput(currValue,len);
		if(value != this.state[name]){
			isDirty = true;
		}
		this.setState({
			[name]:value,
			isDirty
		})
	}

	//截取输入框输入文本长度
	cutInput(value ,len){
		let output = value;
		if(value.length>len){
			output = value.slice(0,len)
		}
		return output
	}

	//初次领证日期选择变化
	firstLicensingDateChange(date){
		this.setState({
			firstLicensingDate:date.format("YYYY-MM-DD"),
			isDirty:true
		})
	}

	//提交数据
	submitInfo(){
		if(this.valiDate()){
			let param = this.state
			this.props.actions.queryAddDrivingLicence(param)
		}
	}

	//点击提交时验证数据
	valiDate(){
		let {
			carId,
			driverName,
			drivingLicenseNo,
			drivingFileNumber,
			firstLicensingDate,
			drivingTelephone,
		} = this.state

		//验证姓名
		if(driverName.length == 0){
			Toast.info("请输入姓名",1)
			return false
		}

		//验证驾驶证号
		if(drivingLicenseNo.length == 0){
			Toast.info("请输入驾驶证号",1)
			return false
		}else if(!(drivingLicenseNo.length == 15 || drivingLicenseNo.length == 18)){
			Toast.info("请输入正确驾驶证号",1)
			return false
		}

		//验证档案编号
		if(drivingFileNumber.length == 0){
			Toast.info("请输入档案编号",1)
			return false
		}else if( drivingFileNumber.length != 12 ){
			Toast.info("请输入正确的档案编号",1)
			return false
		}

		//验证领证日期
		if(firstLicensingDate.length == 0){
			Toast.info("请选择初次领证日期",1)
			return false
		}

		//验证手机号码
		if(drivingTelephone.length == 0){
			Toast.info("请输入手机号")
			return false
		}else if(!/^\d{11}$/.test(drivingTelephone)){
			Toast.info("请输入正确的手机号")
			return false
		}

		return true
	}

	render(){

		//获取车牌号码
		let { cars } =this.props,
			carId = this.props.params.carId,
			carNumber = "";
		if(cars.data[carId]){
			carNumber = cars.data[carId].carNumber
		}

		//车主姓名
		const driverName={
			groupId:"driverName" ,//唯一编码
			label:'车主姓名',//标签文本
			placeholder:"车主姓名" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			value:this.state.driverName,//输入框值
		}

		//驾驶证号
		const drivingLicenseNo={
			groupId:"drivingLicenseNo" ,//唯一编码
			label:'驾驶证号',//标签文本
			placeholder:"驾驶证号完整15位或18位" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			hasIcon:true,//是否有提示icon
			iconImg:"./images/demo-jashiZhengHaoLen.png",//icon弹出图片
			value:this.state.drivingLicenseNo,//输入框值
		}

		//档案编号
		const drivingFileNumber={
			groupId:"drivingFileNumber" ,//唯一编码
			label:'档案编号',//标签文本
			placeholder:"档案编号完整12位数" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			hasIcon:true,//是否有提示icon
			iconImg:"./images/demo-danganBianHaoLen.png",//icon弹出图片
			value:this.state.drivingFileNumber,//输入框值
		}
		
		//初次领证日期选择组件props
		const firstLicensingDateProps = {
			groupId:"firstLicensingDate" ,//唯一编码
			label:'初次领证日期',//标签文本
			placeholder:'请选择' ,//占位符文本
			type:"date" ,//日期选择器类型
			format:"YYYY年MM月DD日", //日期格式
			maxDate:moment(),//最大值
			handleChange:(v)=>{this.firstLicensingDateChange(v)} ,//输入框文本改变操作
			value:this.state.firstLicensingDate?moment(this.state.firstLicensingDate,'YYYY年MM月DD日'):"" //输入框值
		}

		//驾驶证绑定手机号
		const drivingTelephone={
			groupId:"drivingTelephone" ,//唯一编码
			label:'驾驶证绑定手机号',//标签文本
			placeholder:"输入手机号码" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			value:this.state.drivingTelephone,//输入框值
		}

		return (
			<div>
				<div className="wz_whiteSpace_30"></div>
				<div className="wz_headMsg">请填写{carNumber.slice(0,2)+" "+carNumber.slice(2)}<span className={styles.promptInfo}>车主本人</span>的信息</div>
				<div className="wz_list">
					<TxtGroup {...driverName}/>
					<TxtGroup {...drivingLicenseNo}/>
					<TxtGroup {...drivingFileNumber}/>
					<DateGroup {...firstLicensingDateProps}/>
					<TxtGroup {...drivingTelephone}/>
				</div>
				<div className="wz_whiteSpace_50"></div>
				<Btn text="保存并查询" handleClick={()=>this.submitInfo()}/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars:state.cars
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddLicence)

