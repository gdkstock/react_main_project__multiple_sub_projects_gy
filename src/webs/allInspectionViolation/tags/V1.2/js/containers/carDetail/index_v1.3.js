/**
 * 违章业务车辆详情页面
 * Created by lijun on 2017/8/16.
 */
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import common from '../../utils/common'
import { TopHeader, ViolationBrief, Annual, DrivingLicence } from '../../components/carDetail'
import * as carListActions from '../../actions/carListActions'
import config from '../../config'
import { Toast } from 'antd-mobile'

class carDetail extends React.Component {
	constructor(props) {
		super(props);
		//设置初始状态
		this.state = {
		}
	}

	componentWillMount() {
		//设置标题
		common.setViewTitle('车辆详情');

		//请求车辆信息
		let { id, getDatas } = this.props.params
		if (!this.props.cars.data[id] && !getDatas) {
			//如果缓存不存在，并且不需要获取车辆相关数据 则页面后退
			window.history.go(-1)
		} else {
			if (sessionStorage.getItem('deleteCar-' + id)) {
				//车辆已删除
				sessionStorage.removeItem('deleteCar-' + id) //删除，避免一直都后退
				window.history.go(-1)
			} else {
				sessionStorage.setItem("isQueryCarInfo", 1);
				this.props.actions.queryCarInfo({ carId: id });

				//请求车辆违章概要信息
				this.props.actions.queryViolationBrief({ carId: id })

				//请求车辆年检信息
				//this.props.actions.queryInspection({carId:id})

				//查询驾驶证列表
				this.props.actions.queryDrivingLicenceList()
			}
		}
	}

	componentWillReceiveProps(nextProps) {
		//组件跟新时，初始化状态
	}

	componentDidMount() {
		//进入车辆详情页埋点
		common.sendCxytj({
			eventId: "Violation_EnterCarDetails"
		})
	}

	componentWillUnmount() {
		//离开车辆列表页记录页面信息
		sessionStorage.setItem("prevPageInfo", document.title)
	}

	//判断是否有未读违章
	judgeUnreadViolation(readedList, untreatedIdList) {
		if (untreatedIdList.length == 0) {
			return false
		} else {
			let i = 0;
			for (; i < untreatedIdList.length; i++) {
				if (readedList.indexOf(untreatedIdList[i]) == -1) {
					return true
				}
			}
			if (i == untreatedIdList.length) {
				return false
			}
		}
		// if(readedList.length<untreatedIdList.length){
		// 	return true;
		// }else {
		// 	return false;
		// }
	}

	//跳转到编辑车辆页面
	jumpToEditCar(id) {
		//点击车辆认证信息埋点
		common.sendCxytj({
			eventId: "Violation_CarCertifiction"
		})
		if (!sessionStorage.getItem("carCondition")) {
			this.props.actions.queryCarCondition();
		}
		if (sessionStorage.getItem("isQueryCarInfo") || !sessionStorage.getItem("carCondition")) {
			Toast.loading("", 0)
			sessionStorage.setItem("needJumpToEditCarInfo", 1)
		} else {
			console.log('跳转到：' + id)
			window.open(common.getRootUrl() + "editCar/" + id, '_self')
		}
		// window.open(common.getRootUrl()+"editCar/"+id,'_self')
	}

	//跳转到违章列表页面
	jumpToViolationList(carId) {
		//点击违章埋点
		common.sendCxytj({
			eventId: "Violation_ViewViolationDetails"
		})
		window.open(common.getRootUrl() + "violationList/" + carId, '_self')
	}

	//驾照组件点击事件
	handleDrivingLicenceClick(props) {
		let { carId, drivingLicenseId, type } = props
		if (type == "add") {
			//跳转到添加页面

			//点击添加驾照埋点
			common.sendCxytj({
				eventId: "Violation_AddDrivingLicence"
			})

			window.open(common.getRootUrl() + "addDrivingLicence/" + carId, '_self')
		} else if (type == "list") {
			//跳转到列表页面

			//点击驾照查看详情埋点
			common.sendCxytj({
				eventId: "Violation_ViewLicenceDetails"
			})

			window.open(common.getRootUrl() + "drivingLicenceList/" + carId + "/" + drivingLicenseId, '_self')
		}
	}

	//跳转到年检跳转
	jumpToAnnual() {
		let carId = this.props.params.id
		let { cars } = this.props
		let carNumber = ""
		if (carId && cars.data[carId]) {
			carNumber = cars.data[carId].carNumber
		}
		//点击办理年检埋点
		common.sendCxytj({
			eventId: "Violation_HandletheInsepection"
		})
		window.open(config.inspectionReminderUrl + "/" + carNumber + "/" + carId , '_self')
	}

	//跳转到年检补充资料
	jumpToAddInfo() {
		// alert(111)
		let carId = this.props.params.id

		//点击年检补充资料埋点
		common.sendCxytj({
			eventId: "Violation_AddInsepectionData"
		})

		window.open(common.getRootUrl() + "annualInfo/" + carId, '_self')
	}

	//生成顶部车辆概要信息组件props
	createTopHeaderProps(id) {
		let { cars } = this.props
		if (cars.data[id]) {
			let {
				carId,
				carNumber,
				carModelId,
				carModelName,
				completely,
				carBrandLogoUrl,
			} = cars.data[id]

			return {
				carId,
				carNumber,
				carModelId,
				carModelName,
				completely,
				carBrandLogoUrl,
				handleClick: (id) => this.jumpToEditCar(id),
			}
		}
	}

	//生成违章概要信息组件props方法
	createViolationBriefProps(id) {
		let { cars } = this.props
		if (cars.data[id]) {
			let {
				carId,
				violationWarningMsg,
				totalFine,
				totalDegree,
				untreatedNum,
			} = cars.data[id]

			//判断是否有未读违章
			let readedList = cars.data[id].readViolationIds || []
			let untreatedIdList = cars.data[id].untreatedIdList || []
			let violationNew = this.judgeUnreadViolation(readedList, untreatedIdList)

			if (totalFine == -1 && totalDegree == -1 && untreatedNum == -1) {
				totalFine = "?";
				totalDegree = "?";
				untreatedNum = "?";
			}

			//判断违章文案
			let rightText = (untreatedNum == 0 && totalFine == 0 && totalDegree == 0) ? "查看详情" : "立即办理"

			return {
				carId,
				violationNew,
				violationWarningMsg,
				totalDegree,
				totalFine,
				untreatedNum,
				rightText,
				handleClick: this.jumpToViolationList,
			}
		}
	}

	//生成年检信息
	createAnnualProps() {
		let { cars } = this.props,
			carId = this.props.params.id,
			content = "距年检预约还有？天",
			rightText = "立即办理",
			state = '',
			day = '',
			registerDate = '',
			checkDate = '',
			handleClick = null;
		if (carId && cars.data[carId]) {
			state = cars.data[carId].state;
			day = cars.data[carId].day;
			if (!cars.data[carId].registerDate || !cars.data[carId].checkDate) {
				rightText = "补充资料";
				content = "只需一步，年检标志寄到家",
					handleClick = () => this.jumpToAddInfo();
			} else {
				switch (state) {
					case -1:
						content = `距年检预约还有<span class='number'>?</span>天`;
						break;
					case 0:
						content = `距年检预约还有<span class='number'>${day}</span>天`
						break;
					case 1:
						content = `距年检截止还剩<span class='number'>${day}</span>天`
						break;
					case 2:
						content = `年检已逾期<span class='number'>${day}</span>天，仍可免检`
						break;
					case 3:
						content = `年检已逾期<span class='number'>${day}</span>天，需上线检测`
						break;
					case 4:
						content = `年检已严重逾期，需上线检测`
						break;
					case 5:
						content = `车辆已报废，请勿驾驶`
						break;
					case 6:
						content = `年检办理中`
						break;
				}
				rightText = '立即办理';
				handleClick = () => this.jumpToAnnual()
			}
			let stateList = {
				"-1": "未知",
				"0": "未进入预约期",
				"1": "可预约",
				"2": "逾期但不足一年",
				"3": "逾期且超过一年",
				"4": "严重逾期（上线年检）",
				"5": "报废",
				"6": "年审订单办理中",
			}
		}
		return {
			content: { __html: content },
			rightText,
			state,
			day,
			handleClick,
		}
	}

	//生成驾照概要信息组件props
	createDrivingLicenceProps(id) {
		let { cars, drivingLicence } = this.props
		let title = "驾照查分",
			rightText = "添加驾照",
			content = "查看驾照余分，到期清零提醒",
			drivingWarningMsg = "",
			type = "add",
			carId = id,
			drivingLicenseId = "";
		if (cars.data[id]) {
			drivingLicenseId = cars.data[id].drivingLicenseId || "";
		}

		if (drivingLicenseId) {
			if (drivingLicence.data[drivingLicenseId]) {
				let curr = drivingLicence.data[drivingLicenseId]
				title = curr.remainScore == 12 ? "漂亮！请继续保持" : "请注意日常驾驶";
				content = `<span class="float-left">车主(</span><span class='driverName float-left'>${curr.driverName}</span><span class="float-left">)驾照剩余分数为<span class='number'>${curr.remainScore}</span>分</span>`;
				rightText = "查看详情";
				drivingWarningMsg = curr.drivingWarningMsg;
				type = "list";
			}
		} else if (drivingLicence.result && drivingLicence.result.length > 0) {
			title = "请添加车主本人驾照";
			content = "";
			rightText = "查看详情";
			type = "list";
		}

		return {
			carId,
			drivingLicenseId,
			title,
			content: { __html: content },
			rightText,
			drivingWarningMsg,
			type,
			handleClick: this.handleDrivingLicenceClick,
		}
	}

	//判断驾照信息是否显示
	judgeDrivingLicenceShow(id) {
		let cars = this.props.cars
		if (id && cars.data[id]) {
			let carNumber = cars.data[id].carNumber
			if (carNumber) {
				let carPrefix = carNumber.slice(0, 1);
				if (carPrefix == "粤") {
					return true
				}
			}
		}
		return false
	}

	render() {
		const id = this.props.params.id
		console.log('id--------------------', id)
		//顶部tab组件
		
		//顶部车辆概要信息组件props
		const TopHeaderProps = this.createTopHeaderProps(id)

		//违章概要信息组件props
		const ViolationBriefProps = this.createViolationBriefProps(id)

		//年检概要信息
		const AnnualProps = this.createAnnualProps()

		//驾照概要信息组件props
		const drivingLicenceProps = this.createDrivingLicenceProps(id)

		//判断驾照组件是否显示
		const isDrivingLicenceShow = this.judgeDrivingLicenceShow(id)

		return (
			<div>
				<TopHeader {...TopHeaderProps} />
				<div className="wz_whiteSpace_28"></div>
				<ViolationBrief {...ViolationBriefProps} />
				<Annual {...AnnualProps} />
				<div className="hr"></div>
				{isDrivingLicenceShow && <DrivingLicence {...drivingLicenceProps} />}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars: state.cars,
	drivingLicence: state.drivingLicence,
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(carDetail)