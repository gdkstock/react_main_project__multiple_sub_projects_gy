/**
 * 配置
 */
import common from './utils/common'

const userId = sessionStorage.getItem('userId') || ''
const token = sessionStorage.getItem('token') || ''
const userType = sessionStorage.getItem('userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getJsApiUserType() || 'alipay'
const production = window.location.host.indexOf('daiban.cx580.com') > -1 || window.location.host.indexOf('pre.cx580.com') > -1; //是否为生产环境或预发布环境
const authInfo = `userId=${userId}&token=${token}&userType=${userType}&authType=${authType}` //授权信息
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;
const inspectionReminderUrl = production
    ? `https://annualcheck.cx580.com/user/bindInfo?authType=${authType}&userType=${userType}&clientId=CheWu&redirectUrl=https://annualcheck.cx580.com/inspection/index.html?detailUserType=${userType}/%23/remindHome`
    : `http://192.168.1.165:7083/user/bindInfo?authType=${authType}&userType=${userType}&clientId=CheWu&redirectUrl=http://192.168.1.165:7083/inspection/index.html?detailUserType=${userType}/%23/remindHome`;
const addCarUrl = (production
    ? `${window.location.protocol + "//" + window.location.host}/AddCar/index.html`
    : `http://webtest.cx580.com:9021/AddCar/index.html`) + `?${authInfo}`

export default {
    production: production, //是否为生产环境
    baseApiUrl: production ? window.location.protocol + "//" + window.location.host + "/" : "http://webtest.cx580.com:9021/", //接口地址
    authUrl: authUrl,//单点登录地址 回调地址需自行补全
    inspectionReminderUrl: inspectionReminderUrl, //年检提醒URL carId和carNumber需要拼接进来（示例：inspectionReminderUrl+'&carId=1carNumber=粤A12345'）
    indexPageUrl: window.location.protocol + "//" + window.location.host + window.location.pathname + `?${authInfo}`, //首页URL
    myOrderUrl: `https://banli.cx580.com/Myorder/orderList.aspx?userType=${userType}`, //我的订单URL
    vipInfoUrl: (production ? `https://vip.cx580.com/auth?userType=` : `http://192.168.1.235:10004/auth?userType=`) + userType, //vip说明页路径
    addCarUrl: addCarUrl,
    debugUsers: ['B406E4D73A704585AB64B35E2A7896BA'], //debug的userId账号
}