/**
 * 订单相关接口
 */

import apiHelper from './apiHelper';

class CarService {

    /**
     * 订单确认接口 || 订单试算
     * @param {*object} data
     */
    confirm(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/confirm`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 订单详情
     * @param {*object} data
     * {
     * orderId: '' //订单ID
     * }
     */
    view(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/view`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 订单支付
     * @param {*object} data
     * {
     * carId:'',
     * violationInfos: '' //违章ID,多个用半角逗号“;”隔开
     * couponId:'',
     * carNumber:'',
     * urgentFlag:'' //1：加急；2：不加急
     * buyVipCard:'' //1：购买；2：不购买
     * }
     */
    pay(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/pay`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new CarService()