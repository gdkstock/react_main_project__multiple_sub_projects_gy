/*
	全局状态cars
*/
import {
	ADD_CAR,
	ADD_CAR_LIST,
	UPDATE_CAR,
	DELETE_CAR,
	ADD_READ_VIOLATION,
} from '../actions/actionsTypes'

const initState = {
	data: {

	},
	result: []
}

//测试
// const initState = {
// 	data: {
// 		"1234":{
// 			carId:"1234",
// 			carNumber:'粤A12345',
// 			untreatedNum:"26",
// 			totalFine: "9999",
// 			totalDegree: "99",
// 			violationWarningMsg:"",
// 			drivingLicenceWarningMsg:"",
// 			violationNew:false, //有未读违章
// 			newList:[], //已阅读的违章列表
// 			violations:[1,2,3]
// 		},
// 		"1236":{
// 			carId:"1236",
// 			carNumber:'粤A12345',
// 			untreatedNum:"26",
// 			totalFine: "9999",
// 			totalDegree: "99",
// 			violationWarningMsg:"",
// 			drivingLicenceWarningMsg:"",
// 			violationNew:false, //有未读违章
// 			newList:[], //已阅读的违章列表
// 			violations:[1,2,3]
// 		}
// 	},
// 	result: [ '1234', '1236' ] 
// }

export default function cars(state = initState, action) {
	let data = {}, object = {};
	switch (action.type) {
		case ADD_CAR:
			//给cars中添加车辆信息
			data = action.data;
			if (state.data[data.carId]) {
				object.data = {}
				object.data[data.carId] = { ...state.data[data.carId], ...data }
				object.data = { ...state.data, ...object.data }
			} else {
				object.data = { ...state.data, [data.carId]: data }
			}
			object.result = Array.from(new Set([...state.result,data.carId*1]))
			return object;
		case ADD_CAR_LIST:
			//添加车辆列表
			data = action.data;
			object.data = {};
			object.result = [];
			for (var i = 0; i < data.length; i++) {
				let carId = data[i].carId
				if(state.data[carId]){
					object.data[carId] = {...state.data[carId],...data[i]}
				}else{
					object.data[carId] = data[i]
				}
				object.result.push(carId*1);
			}
			return object;
		case UPDATE_CAR:
			//更新车辆信息
			data = action.data;
			if(data && data.carId){
				if (state.data[data.carId]) {
					object.data = {}
					object.data[data.carId] = { ...state.data[data.carId], ...data }
					object.data = Object.assign({},state.data, object.data)
				} else {
					// object.data = { ...state.data, [data.carId]: data }
					object.data = Object.assign({},state.data, {[data.carId]:data})
				}
				object.result = Array.from(new Set([...state.result, data.carId*1]))
			}else{
				object.data ={ ...state.data }
				object.result = Array.from(new Set([...state.result]))
			}
			return object;
		case DELETE_CAR:
			//删除车辆信息
			data = action.data
			object = { ...state }
			delete object.data[data.carId]
			let i = object.result.indexOf(data.carId*1)
			object.result.splice(i, 1);
			return object;
		case ADD_READ_VIOLATION:
			//添加已读违章ID
			object = state.data[action.data.carId]
			if (object) {
				let readViolationIds = [];
				if (object.readViolationIds && object.readViolationIds.length > 0) {
					readViolationIds = Array.from(new Set([...object.readViolationIds, action.data.violationId]))
				} else {
					readViolationIds.push(action.data.violationId)
				}
				return {
					data: Object.assign({}, state.data, {
						[action.data.carId]: Object.assign({}, object, {
							readViolationIds: readViolationIds
						})
					}),
					result: state.result
				}
			}

		default:
			return state;
	}
}


