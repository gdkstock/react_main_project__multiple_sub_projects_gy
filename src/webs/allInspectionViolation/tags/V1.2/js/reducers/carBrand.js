/*
	全局状态carBrand
*/
import {
    ADD_BRAND_LIST,
} from '../actions/actionsTypes'

const initState = {

}

//测试
// const initState = {
// 	data: {
// 		"1234":{
// 			drivingLicenseId:"1234",
// 			driverName:"李君",
// 			drivingLicenseNo:"432503199008035697",
// 			drivingFileNumber:"432503199008035697",
// 			drivingTelephone:"18738116546",
// 			score:"12",
// 			deductionScore:"6",
// 			remainScore:"6",
// 			cycleEndDate:"2017-12-30",
// 			drivingWarningMsg:"",
// 		},
// 		"1236":{
// 			drivingLicenseId:"1236",
// 			driverName:"李君",
// 			drivingLicenseNo:"432503199008035697",
// 			drivingFileNumber:"432503199008035697",
// 			drivingTelephone:"18738116546",
// 			score:"12",
// 			deductionScore:"6",
// 			remainScore:"6",
// 			cycleEndDate:"2017-12-30",
// 			drivingWarningMsg:"",
// 		}
// 	},
// 	result: [ '1234', '1236' ] 
// }

export default function carBrand( state = initState, action ){
	switch(action.type){
		case ADD_BRAND_LIST:
			return {...state,...action.data}
		default:
			return state;
	}
}


