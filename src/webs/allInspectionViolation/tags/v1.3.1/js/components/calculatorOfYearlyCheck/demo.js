import React, { Component, PropTypes } from 'react';
import CalculatorOfYearlyCheck from './index'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as carListActions from '../../actions/carListActions'
import common from '../../utils/common'
import carService from '../../services/carService';

import { Toast } from 'antd-mobile'

class Demo extends Component {
    constructor(props) {
        super(props);

        let carId = this.props.params.carId || "";
        let curr = {};
        if(carId && this.props.cars.data[carId]){
            curr = this.props.cars.data[carId]
        }

        this.state = {
            data: { //年检计算机的props数据
                carId:curr.carId || "",
                carNum: curr.carNumber || "", //车牌号码
                carCode:curr.carCode,
                engineNumber:curr.engineNumber,
                registerDate:curr.registerDate,
                checkDate:curr.checkDate,
                dateList: [], //检验有效期列表
                getRegisterDate: d => this.getRegisterDate(d), //获取车辆注册日期
                getInspectDate: d => this.getInspectDate(d), //获取检验有效期
                onClick: () => this.saveMsg(),
            }
        }
    }

    componentWillMount(){
        //设置标题
        common.setViewTitle('补充资料');
        if (this.state.registerDate) {
            this.getRegisterDate(this.state.registerDate)
        }
    }

    componentWillReceiveProps(nextProps){
        let carId = nextProps.params.carId
        let registerDate = (carId && nextProps.cars.data[carId])? nextProps.cars.data[carId].registerDate:"";
        if (this.state.registerDate) {
            this.getRegisterDate(registerDate)
        }
    }

    componentWillUnmount() {
        //离开年检补充资料页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

    /**
     * 联动获取检验有效时间列表
     * @param registerDate 注册时间
     */
    getRegisterDate(registerDate) {
        //这里触发接口请求
        carService.checkDateList({registerDate}).then(result=>{
            if(result.code=="1000"){

                //计算推荐的年检到期时间
                let defaultList = result.data.dateList.filter(item=>item.default=="1");
                let inspectDate = "";
                if(defaultList.length>0){
                    inspectDate = defaultList[0].checkDate
                }

                this.setState({
                    data: Object.assign({}, this.state.data, {
                        dateList: result.data.dateList.map(item=>{
                            return item.checkDate;
                        }),
                        registerDate,
                        inspectDate,
                    }),
                })
            }else{
                if(result.msg){
                    Toast.info(result.msg,1)
                }
            }
        }).catch(e=>{
            Toast.info("系统繁忙，请稍后再试",1)
        })
    }

    //获取年检到期时间
    getInspectDate(checkDate){
        this.setState({
            data: Object.assign({}, this.state.data, {
                inspectDate:checkDate
            })
        })
    }

    //保存提交数据
    saveMsg(){
        if(!this.state.data.registerDate){
            Toast.info("请选择注册日期",1)
            return
        }
        if(!this.state.data.inspectDate){
            Toast.info("请选择年检到期时间",1)
            return
        }
        let params={
            carId:this.state.data.carId,
            carNumber:this.state.data.carNum,
            carCode:this.state.data.carCode,
            engineNumber:this.state.data.engineNumber,
            registerDate:this.state.data.registerDate,
            checkDate:this.state.data.inspectDate, //只需要保存年月
        }
        this.props.actions.queryUpdateCar(params)
    }

    render() {
        let calculatorOfYearlyCheckProps = this.state.data
        let yearlyCheckDetailProps = this.state.yearlyCheckDetailProps
        return (
            <div>
                <CalculatorOfYearlyCheck {...calculatorOfYearlyCheckProps} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cars:state.cars,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Demo)

// //使用context
// Demo.contextTypes = {
//     router: React.PropTypes.object.isRequired
// }

// Demo.propTypes = {

// };

// export default Demo;