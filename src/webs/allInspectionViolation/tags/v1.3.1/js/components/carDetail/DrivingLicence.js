/*
*	驾照卡片信息组件
	props = {
		drivingLicenceId:"",//驾照id
		handleClick:()=>{}
	}
*/

import styles from './index.scss'
import DrivingWarningMsg from './drivingWarningMsg'
import common from '../../utils/common'

const defaultProps = {
	carId:"",
	drivingLicenseId:"",//驾照id
	title:"驾照查分",
	remainScore:0,
	content:{__html:"查看驾照余分，到期清零提醒"},
	rightText:"",
	drivingWarningMsg:"记分周期即将到期，建议将全部违章尽快处理完毕",
	type:"add",
	handleClick:()=>{},
}


class DrivingLicence extends React.Component{
	// props = defaultProps;
	

	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			...this.props,
		}
	}

	componentDidUpdate(){
		// let { carId , remainScore } = this.state;
		// if(remainScore>0){
		// 	let obj={
		// 	 	id:"drivingLicenceScore"+carId,//定时器id
		// 		endNum:remainScore,//目标值
		// 		step:12,//初始值到目标值中间步数 默认值100
	 	// 		totalTime:1000,//总时间  默认值2s
		// 	}
		// 	if(sessionStorage.getItem("isQueryDrivingLicenceDataDone")){
		// 		sessionStorage.removeItem("isQueryDrivingLicenceDataDone")
		// 		//驾照数据请求完成执行动画
		// 		common.animation(obj)
		// 	}
		// }
	}

	componentWillUnmount() {
		// let { carId } = this.state
		// window["drivingLicenceScore"+carId] && (window["drivingLicenceScore"+carId]=null);
    }

	render(){
		const props = {...this.props}
		return (
			<div>
				<div className={styles.wrap} onClick={()=>{props.handleClick(props)}}>
					<div className={styles.leftText}>
						<div className={styles.title}>{props.title}</div>
						<div className={styles.content} dangerouslySetInnerHTML={props.content}></div>
					</div>
					<div>
						<span className={styles.rightText}>{props.rightText}</span><i className={styles.rightIcon}></i>
					</div>
				</div>
				{props.drivingWarningMsg && 
					<div className={styles.msgWrap}>
						<DrivingWarningMsg text = {props.drivingWarningMsg} id="drivingWarningMsg"/>
					</div>
				}
			</div>
		)
	}
}

export default DrivingLicence