/*
*	广告
	
*/

import styles from './index.scss'
import DrivingWarningMsg from './drivingWarningMsg'

const defaultProps = {
	title:"驾照查分",
	content:{__html:"查看驾照余分，到期清零提醒"},
    targetUrl:"",
	rightText:"",
	handleClick:()=>{},
}

const Ad = props => {
	// props = defaultProps;
	//alert(props.licenceWraningMsg)
	return (
		<div>
			<div className={styles.wrap} onClick={()=>{props.handleClick(props)}}>
				<div className={styles.leftText}>
					<div className={styles.title}>{props.title}</div>
					<div className={styles.content} dangerouslySetInnerHTML={props.content}></div>
				</div>
				<div>
					<span className={styles.rightText}>{props.rightText}</span><i className={styles.rightIcon}></i>
				</div>
			</div>
		</div>
	)
}

export default Ad