/*
*	警告轮播信息组件
	props = {
		text:"",//轮播文本
	}
*/
import React from 'react'
import styles from './warningInfo.scss'

class WarningInfo extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			id:this.props.id,
			text:this.props.text
		}
	}

	componentDidMount(){
	}

	componentWillReceiveProps(nextProps) {
		
	}

	componentDidUpdate(){
		//获得父元素
		let parentId = this.state.id 
		//执行滚动动画
		// this.animation(parentId)
	}

	componentWillUnmount(){
		// alert("组件卸载了")
		// alert(window.timer)
		let id = this.state.id ;
		if(window['timer'+id]){
			window.clearInterval(window['timer'+id])
		};
	}

	animation(parentId){
		try{
			let wrap = document.getElementById(parentId);
			let textWidth = wrap.firstElementChild.clientWidth;
			if(window['timer'+parentId]){
				clearInterval(window['timer'+parentId]);
			}
			window['timer'+parentId]=setInterval(()=>{
				// wrap = document.getElementById(parentId)
				wrap.style.transition="";
				wrap.style.left=0;
				setTimeout(()=>{
					wrap.style.transition="left 6s linear";
					wrap.style.left=0-textWidth+"px";
				},20)	
			},6020)

		}catch(e){

		}
	}

	render(){
		return (
			<div className={styles.wrap}>
				{
					this.props.text && <div className={styles.textView}>
						<div className={styles.textWrap}  id={this.props.id}>
							<div className={styles.text}>{this.props.text+"。 "+this.props.text+"。 "}</div>
						</div>
					</div>
				}
			</div>
		)
	}
}

export default WarningInfo	