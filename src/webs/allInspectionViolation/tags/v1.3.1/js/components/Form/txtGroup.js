/*
	文本输入框组件
	props={
		groupId:"carCode" ,//唯一编码
		label:'车身架号',//标签文本
		placeholder:'' ,//占位符文本
		handleChange:()=>{} ,//输入框文本改变操作
		type:"text" ,//输入框type
		hasIcon:true,//是否有提示icon
		iconImg:"./images/demo-danganBianHaoLen.png",//icon弹出图片
		value:"" //输入框值
	}
*/
import styles from './txtGroup.scss'

const showMask = (id) =>{
	let mask = document.getElementById(id);
	mask.style.display="block";
}

const hideMask = (id) =>{
	let mask = document.getElementById(id);
	mask.style.display="none";
}

const TxtGroup = props =>{
	return (
		<div className="wz_item">
			<span className="label">{props.label}</span>
			{props.hasIcon && <i className='icon-prompt' onClick={()=>showMask(props.groupId)}></i>}
			<input 
				className="txt" 
				type={props.type}
				value = {props.value}
				placeholder={props.placeholder} 
				name={props.groupId}
				onChange={(e)=>props.handleChange(e)}
			/>
			<div id={props.groupId} className={styles.mask} onClick={()=>hideMask(props.groupId)}>
				<img src={props.iconImg}/>
			</div>
		</div>
	)
}

export default TxtGroup