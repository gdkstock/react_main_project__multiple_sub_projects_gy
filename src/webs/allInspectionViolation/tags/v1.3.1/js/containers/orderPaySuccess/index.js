/**
 * 订单支付成功页
 */

import React, { Component } from 'react';

//styles
import styles from './index.scss';

class OrderPay extends Component {
    constructor(props) {
        super(props)

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div className='box'>
                <div className='whiteBg'></div>
                <div className={styles.successBox}>
                    <img src='./images/icon_wddhsdg.png' />
                    <h3>支付成功</h3>
                </div>
                <div className={styles.info}>
                    <div className={styles.top}>
                        <div className={styles.title + ' fl'}>粤A 12345 广东广州违章</div>
                        <div className={styles.orderDetailBtn + ' fr'}>订单详情</div>
                    </div>
                    <ul className={styles.txt}>
                        <li>已下单用户请勿再私下重复处理违章，否则该违章重复缴款损失将由客户自行承担；</li>
                        <li>异地差异罚款和产生滞纳金的罚单将由客服通知补齐；</li>
                        <li>办理周期为2-5个工作日；</li>
                        <li>缴费不成功的违章订单将全额退款；</li>
                    </ul>
                </div>
                <iframe className={styles.iframe} src='http://www.baidu.com'>

                </iframe>
            </div>
        )
    }
}

export default OrderPay