/**
 * 违章业务车辆详情页面
 * Created by lijun on 2017/8/16.
 */
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import common from '../../utils/common'
import { TopHeader, ViolationBrief, Annual, DrivingLicence , Card , AddCarBtn , Ad} from '../../components/carDetail'
import AddCar from '../../components/addCar'
import * as carListActions from '../../actions/carListActions'
import config from '../../config'
import { Toast } from 'antd-mobile'
import styles from './index.scss'
import carservice from '../../services/carService'

import { Tabs, WhiteSpace, Badge } from 'antd-mobile';
const TabPane = Tabs.TabPane;

class carDetail extends React.Component {
	constructor(props) {
		super(props);
		//设置初始状态
		this.state = {
			curCarId:this.props.params.id
		}
	}

	componentWillMount() {
		//设置标题
		common.setViewTitle('车辆详情');

		//进入车辆详情页面缓存

		//请求车辆信息
		let { id , getDatas } = this.props.params;
		let { data , result } = this.props.cars;

		if(result.length>0){
			let curCarId = data[id]?id:result[0];
			this.setState({curCarId})
		}
		this.props.actions.queryCarListOnly({},(result)=>{
			if(result.data.length==0){//如果没有车辆数据，页面后退
				window.history.go(-1)
			}else{
				//显示当前车辆
				let carDatas = {};
				result.data.map(item=>carDatas[item.carId]=item);
				let curCarId = carDatas[id]?id:result.data[0].carId;
				this.setState({curCarId})
				this.getData(curCarId)

				//请求广告信息
				this.props.actions.getAd({positionCode:"WZ_CAR_ACCIDENT_INSURANCE"},(result)=>{
					this.setState({
						adList:result.data.adList
					})
				})
			}
		})
	
	}

	componentWillUpdate(){
		// let { curCarId } = this.state;
		// let nextCarId = this.props.params.id;
		// if(curCarId!=nextCarId){
		// 	this.setState({curCarId:nextCarId})
		// }
	}

	//获取车辆详情相关数据
	getData(carId){
		sessionStorage.setItem("isQueryCarInfo", 1);
		this.props.actions.queryCarInfo({ carId });
		//请求车辆违章概要信息
		this.props.actions.queryViolationBrief({ carId })
	}

	componentWillReceiveProps(nextProps) {
		//组件跟新时，初始化状态
	}

	componentDidMount() {
		//进入车辆详情页埋点
		common.sendCxytj({
			eventId: "Violation_EnterCarDetails"
		})
	}

	componentDidUpdate(){
	
	}

	componentWillUnmount() {
		//离开车辆列表页记录页面信息
		sessionStorage.setItem("prevPageInfo", document.title)
		sessionStorage.removeItem("isQueryAnnualDataDone");
		sessionStorage.removeItem("isQueryDrivingLicenceDataDone");
		sessionStorage.removeItem("isQueryViolationDataDone");

	}

	//添加车辆
	addCar(){
		//点击添加车辆埋点
		common.sendCxytj({
			eventId: "Violation_AddCar"
		})

		//全渠道都使用引导注册的方式

		//获取车牌前缀
		this.props.actions.getCarConditionList({}, result => {
			let { carPrefixList } = this.state
			if (result && result.code == '1000' && result.result.length > 0) {
				carPrefixList = result.result
			}

			this.setState({
				showAddCar: true,
				carPrefixList: carPrefixList
			})
		})
	}

	//隐藏添加车辆
	hideAddCar() {
		this.setState({
			showAddCar: false
		})
	}

	/**
	 * 检测输入的合法性
	 */
	checkInputs(carNumber) {
		let checkAZ = /[A-Z]/g.exec(carNumber.substr(1, 1)); //检查车牌号码是否以字母开头

		if (carNumber.length < 2) {
			Toast.info("车牌号码不能为空", 1);
		} else if (carNumber.length < 7 || carNumber.length > 8 || !checkAZ) {
			Toast.info("您输入的车牌号码有误", 1);
		} else {
			return true
		}
		return false
	}

	//跳转到添加车辆页面
	toAddCarUrl(url) {
		window.location.href = `${config.addCarUrl}#${url}` //跳转到添加车辆的相关路由页面
	}


	//跳转到添加车辆验证页面
	toAddCarAuthPage(carNumber) {
		if (this.checkInputs(carNumber)) {
			this.props.actions.getCarInformation({ carNumber: carNumber }, result => {
				if (result && result.code == '1000') {
					this.hideAddCar();

					let { carId, count, degree, money, tel, flag } = result.data
					switch (flag + '') {
						case '0': //无车辆信息
							if (carNumber.substr(0, 1) == '粤') {
								//粤牌车可以进行身份验证
								this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							} else {
								this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
							}
							break;
						case '1': //存在车辆信息
							if (carNumber.substr(0, 1) != '粤' && count == -1 && !tel) {
								//非粤牌车 && 无违章 && 无手机号码
								this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
							} else {
								this.toAddCarUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							}

							break;
						case '2': //车辆已经存在车辆列表中
							window.location.href = common.getRootUrl() + 'violationList/' + carId; //跳转到违章列表
							break;
						default:
							this.toAddCarUrl(`addCar/${encodeURIComponent(carNumber)}`)
					}

				} else {
					if (result.code == '4443') {
						//满三辆车
						Toast.info('违章查询最多支持绑定3辆车辆,请前往车辆列表删除后再继续查询', 3, () => this.hideAddCar())
					} else {
						Toast.info(result.msg || ChMessage.FETCH_FAILED)
					}
				}
			})
			// 
			// carNumber = encodeURIComponent(carNumber);
			// window.location.href = `${config.addCarUrl}#authPage/${carNumber}(/:count/:degree/:money)(/:tel)` //跳转到添加车辆的身份验证页面
		}
	}


	//判断是否有未读违章
	judgeUnreadViolation(readedList, untreatedIdList) {
		if (untreatedIdList.length == 0) {
			return false
		} else {
			let i = 0;
			for (; i < untreatedIdList.length; i++) {
				if (readedList.indexOf(untreatedIdList[i]) == -1) {
					return true
				}
			}
			if (i == untreatedIdList.length) {
				return false
			}
		}
	}

	//跳转到编辑车辆页面
	jumpToEditCar(id) {
		//点击车辆认证信息埋点
		common.sendCxytj({
			eventId: "Violation_CarCertifiction"
		})
		if (!sessionStorage.getItem("carCondition")) {
			this.props.actions.queryCarCondition();
		}
		if (sessionStorage.getItem("isQueryCarInfo") || !sessionStorage.getItem("carCondition")) {
			Toast.loading("", 0)
			sessionStorage.setItem("needJumpToEditCarInfo", 1)
		} else {
			console.log('跳转到：' + id)
			window.open(common.getRootUrl() + "editCar/" + id, '_self')
		}
		// window.open(common.getRootUrl()+"editCar/"+id,'_self')
	}

	//跳转到违章列表页面
	jumpToViolationList(carId) {
		//点击违章埋点
		common.sendCxytj({
			eventId: "Violation_ViewViolationDetails"
		})
		window.open(common.getRootUrl() + "violationList/" + carId, '_self')
	}

	//驾照组件点击事件
	handleDrivingLicenceClick(props) {
		let { carId, drivingLicenseId, type } = props
		switch(type){
			case "add":
				//跳转到添加页面

				//点击添加驾照埋点
				common.sendCxytj({
					eventId: "Violation_AddDrivingLicence"
				});

				window.open(common.getRootUrl() + "addDrivingLicence/" + carId, '_self');
				break;
			case "list":
				//跳转到列表页面

				//点击驾照查看详情埋点
				common.sendCxytj({
					eventId: "Violation_ViewLicenceDetails"
				});

				window.open(common.getRootUrl() + "drivingLicenceList/" + carId + "/" + drivingLicenseId, '_self');
				break;
			case "edit":
				//跳转到编辑驾照页

				window.open(common.getRootUrl() + "editDrivingLicence/" + carId + "/" + drivingLicenseId, '_self');
				break;
		}
	}

	//跳转到年检跳转
	jumpToAnnual() {
		let carId = this.state.curCarId
		let { cars } = this.props
		let carNumber = ""
		if (carId && cars.data[carId]) {
			carNumber = cars.data[carId].carNumber
		}
		//点击办理年检埋点
		common.sendCxytj({
			eventId: "Violation_HandletheInsepection"
		})
		window.open(config.inspectionReminderUrl + "/" + encodeURI(carNumber) + "/" + carId , '_self')
	}

	//跳转到年检补充资料
	jumpToAddInfo() {
		// alert(curCarId)
		let { curCarId } = this.state
		//点击年检补充资料埋点
		common.sendCxytj({
			eventId: "Violation_AddInsepectionData"
		})

		window.open(common.getRootUrl() + "annualInfo/" + curCarId, '_self')
	}

	//生成顶部车辆概要信息组件props
	createTopHeaderProps(id) {
		let { cars } = this.props
		if (cars.data[id]) {
			let {
				carId,
				carNumber,
				carModelId,
				carModelName,
				completely,
				carBrandLogoUrl,
			} = cars.data[id]

			return {
				carId,
				carNumber,
				carModelId,
				carModelName,
				completely,
				carBrandLogoUrl,
				handleClick: (id) => this.jumpToEditCar(id),
			}
		}
	}

	//生成违章概要信息组件props方法
	createViolationBriefProps(id) {
		let { cars } = this.props
		if (cars.data[id]) {
			let {
				carId,
				violationWarningMsg,
				totalFine,
				totalDegree,
				untreatedNum,
			} = cars.data[id]

			//判断是否有未读违章
			let readedList = cars.data[id].readViolationIds || []
			let untreatedIdList = cars.data[id].untreatedIdList || []
			let violationNew = this.judgeUnreadViolation(readedList, untreatedIdList)

			if (totalFine == -1 && totalDegree == -1 && untreatedNum == -1) {
				totalFine = "?";
				totalDegree = "?";
				untreatedNum = "?";
			}

			//判断违章文案
			let rightText = (untreatedNum == 0 && totalFine == 0 && totalDegree == 0) ? "查看详情" : "立即办理"

			return {
				carId,
				violationNew,
				violationWarningMsg,
				totalDegree,
				totalFine,
				untreatedNum,
				rightText,
				handleClick: this.jumpToViolationList,
			}
		}
	}

	//生成卡片信息组件
	createCardProps(id){
		let { cars } = this.props
		if (cars.data[id]) {
			let {
				carId,
				carNumber,
				carModelId,
				carModelName,
				completely,
				carBrandLogoUrl,
				violationWarningMsg,
				totalFine,
				totalDegree,
				untreatedNum,
			} = cars.data[id]

			//判断是否有未读违章
			let readedList = cars.data[id].readViolationIds || []
			let untreatedIdList = cars.data[id].untreatedIdList || []
			let violationNew = this.judgeUnreadViolation(readedList, untreatedIdList)

			//判断违章文案
			let rightText = (untreatedNum == 0 && totalFine == 0 && totalDegree == 0) ? "查看详情" : "立即办理"

			if (totalFine == -1 && totalDegree == -1 && untreatedNum == -1) {
				totalFine = "?";
				totalDegree = "?";
				untreatedNum = "?";
				rightText = "补充资料";
			}

			return {
				carId,
				carNumber,
				carModelId,
				carModelName,
				completely,
				carBrandLogoUrl,
				violationNew,
				violationWarningMsg,
				totalDegree:(totalDegree || totalDegree==0)?totalDegree:"?",
				totalFine:(totalFine || totalFine==0)?totalFine:"?",
				untreatedNum:(untreatedNum || untreatedNum==0)?untreatedNum:"?",
				rightText,
				editCar: (id) => this.jumpToEditCar(id),
				violationDetail:this.jumpToViolationList,
			}
		}
	}

	//生成年检信息
	createAnnualProps(id) {
		let { cars } = this.props,
			carId = id,
			content = "只需一步，年检标志寄到家",
			rightText = "立即办理",
			state = '',
			day = '',
			registerDate = '',
			checkDate = '',
			handleClick = null;
		if (carId && cars.data[carId]) {
			state = cars.data[carId].state;
			day = cars.data[carId].day;
			if (!cars.data[carId].registerDate || !cars.data[carId].checkDate) {
				rightText = "补充资料";
				content = "只需一步，年检标志寄到家",
				handleClick = () => this.jumpToAddInfo();
			} else {
				switch (state) {
					case -1:
						content = `距年检预约还有<span class='number' id='annualDays${carId}'>?</span>天`;
						break;
					case 0:
						content = `距年检预约还有<span class='number' id='annualDays${carId}'>${day}</span>天`
						break;
					case 1:
						content = `距年检截止还剩<span class='number numberWarn' id='annualDays${carId}'>${day}</span>天`
						break;
					case 2:
						content = `年检已逾期<span class='number' id='annualDays${carId}'>${day}</span>天，仍可免检`
						break;
					case 3:
						content = `年检已逾期<span class='number' id='annualDays${carId}'>${day}</span>天，需上线检测`
						break;
					case 4:
						content = `年检已严重逾期，需上线检测`
						break;
					case 5:
						content = `车辆已报废，请勿驾驶`
						break;
					case 6:
						content = `年检办理中`
						break;
				}
				rightText = '立即办理';
				handleClick = () => this.jumpToAnnual()
			}
			let stateList = {
				"-1": "未知",
				"0": "未进入预约期",
				"1": "可预约",
				"2": "逾期但不足一年",
				"3": "逾期且超过一年",
				"4": "严重逾期（上线年检）",
				"5": "报废",
				"6": "年审订单办理中",
			}
		}
		return {
			carId,
			content: { __html: content },
			state,
			day,
			handleClick,
		}
	}

	//生成驾照概要信息组件props
	createDrivingLicenceProps(id) {
		let { cars, drivingLicence } = this.props
		let title = "添加车主本人驾照",
			rightText = "添加驾照",
			content = "查看驾照余分，到期清零提醒",
			drivingWarningMsg = "",
			type = "add",
			carId = id,
			remainScore=0,
			drivingLicenseId = "";
		if (cars.data[id]) {
			drivingLicenseId = cars.data[id].drivingLicenseId || "";
		}
		if (drivingLicenseId) {
			if (drivingLicence.data[drivingLicenseId]) {
				let curr = drivingLicence.data[drivingLicenseId]
				title = curr.remainScore == 12 ? "漂亮！请继续保持" : "请注意日常驾驶";
				content = `<span class="float-left">车主(</span><span class='driverName float-left'>${curr.driverName}</span><span class="float-left" style="position:relative;top:-0.02rem;">)驾照剩余分数为<span class='number' id="drivingLicenceScore${carId}">${curr.remainScore}</span>分</span>`;
				rightText = "查看详情";
				drivingWarningMsg = curr.drivingWarningMsg;
				type = "edit";
				remainScore = curr.remainScore;
			}
		}

		return {
			carId,
			drivingLicenseId,
			title,
			remainScore,
			content: { __html: content },
			drivingWarningMsg,
			type,
			handleClick: this.handleDrivingLicenceClick,
		}
	}

	//判断驾照信息是否显示
	judgeDrivingLicenceShow(id) {
		let cars = this.props.cars
		if (id && cars.data[id]) {
			let carNumber = cars.data[id].carNumber
			if (carNumber) {
				let carPrefix = carNumber.slice(0, 1);
				if (carPrefix == "粤") {
					return true
				}
			}
		}
		return false
	}

	//切换车辆
	selectCarTab(carId){
		this.setState({curCarId:carId})
		this.getData(carId)
		let { getDatas } = this.props.params;
		let url=getDatas?common.getRootUrl()+"carDetail/"+carId+"/getDatas":common.getRootUrl()+"carDetail/"+carId
		window.location.replace(url)
	}

	//createAdProps
	createAdProps(adList){
		let title = "" , content="" , targetUrl="";
		if(adList && adList.length>0){
			title = adList[0].title;
			content = adList[0].description;
			targetUrl = adList[0].targetUrl;
		}

		return {
			title,
			content: { __html: content },
			targetUrl,
			handleClick: (p)=>this.handleAdClick(p),
		}
	}

	//点击广告跳转
	handleAdClick(item){
		if(item.targetUrl){
			window.location.href=item.targetUrl;
		}
	}

	render() {
		let { showAddCar , carPrefixList , curCarId } = this.state

		let { cars } = this.props;
		let { data , result } = cars;
		let { adList } = this.state;
		return (
			<div className="carDetail" key={curCarId}>
				{result.length<3 && result.length>0  && <AddCarBtn handleClick={()=>this.addCar()}/>}
				<Tabs defaultActiveKey={curCarId+""} onChange={(v)=>this.selectCarTab(v)} swipeable={false}>
					{result.map(item=>{
							//生成车辆信息卡片组件props
							const CardProps = this.createCardProps(item)
							//年检概要信息
							let AnnualProps = this.createAnnualProps(item)
							//驾照概要信息组件props
							let drivingLicenceProps = this.createDrivingLicenceProps(item)
							//判断驾照组件是否显示
							let isDrivingLicenceShow = this.judgeDrivingLicenceShow(item)
							//广告
							let AdProps = this.createAdProps(adList)

							return <TabPane tab={<Badge>{data[item].carNumber}</Badge>} key={item}>
								<div className="h34"></div>
								<Card {...CardProps}/>
								<Annual {...AnnualProps} />
								<div className="hr"></div>
								{isDrivingLicenceShow && <DrivingLicence {...drivingLicenceProps} />}
								{isDrivingLicenceShow && <div className="hr"></div>}
								{adList && adList.length>0 && <Ad {...AdProps} />}
								{adList && adList.length>0 && <div className="hr"></div>}
							</TabPane>
						}
					)}
				</Tabs>	
				{/*添加车辆蒙层 start*/}
				{
					showAddCar
						? <div className={styles.addCar}>
							<AddCar
								carPrefixList={carPrefixList}
								onClick={(carNumber) => this.toAddCarAuthPage(carNumber)}
								close={() => this.hideAddCar()}
							/>
						</div>
						: ''
				}
				{/*添加车辆蒙层 end*/}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars: state.cars,
	drivingLicence: state.drivingLicence,
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(carDetail)