/**
 * 违章相关action
 */

import {
    ADD_VIOLATION,
    UPDATE_VIOLATION,
    DELETE_VIOLATION,
    ADD_VIOLATIONS,
    GET_VIOLATION_ASYNC,
    GET_VIOLATION_LIST_ASYNC,
    UPDATE_VIOLATION_ASYNC,
    GET_LOCATION_VIOLATION_NUMBER_ASYNC,
    ADD_READ_VIOLATION,
    DELETE_PROXY_RULE_KEY,
    GET_REQUIREMENT_ASYNC,
    GET_CARS_VIOLATIONS,
    GET_COMMENT
} from './actionsTypes.js'

/**
 * 同步action
 */
export const addViolation = data => ({ type: ADD_VIOLATION, data: data })
export const addViolations = data => ({ type: ADD_VIOLATIONS, data: data })
export const updateViolation = data => ({ type: UPDATE_VIOLATION, data: data })
export const deleteViolation = violationId => ({ type: DELETE_VIOLATION, violationId: violationId })
export const addReadViolation = data => ({ type: ADD_READ_VIOLATION, data: data })
export const deleteProxyRuleKey = data => ({ type: DELETE_PROXY_RULE_KEY, data: data })
export const getCarsViolations = (data, callback) => ({ type: GET_CARS_VIOLATIONS, data: data, callback: callback })

/**
 * 异步action
 */
export const getViolationAsync = (data, callback) => ({ type: GET_VIOLATION_ASYNC, data: data, callback: callback })
export const getViolationListAsync = (data, showLoading = true, callback = () => false) => ({ type: GET_VIOLATION_LIST_ASYNC, data: data, showLoading: showLoading, callback: callback })
export const updateViolationListAsync = data => ({ type: UPDATE_VIOLATION_ASYNC, data: data })
export const getLocationViolationNumberAsync = (data, violationId,callback) => ({ type: GET_LOCATION_VIOLATION_NUMBER_ASYNC, data: data, violationId: violationId,callback })
export const getRequirementAsync = (data, callback = () => false) => ({ type: GET_REQUIREMENT_ASYNC, data: data, callback: callback })
export const getComment = (data,callback)=>({type:GET_COMMENT,data,callback})