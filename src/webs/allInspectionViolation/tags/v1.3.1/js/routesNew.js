import React from 'react'
import { Route, IndexRoute } from 'react-router'

//antd UI
import { Toast } from 'antd-mobile'

import {
  App,
  // Home,
  NotFoundPage,
} from './containers'

//显示加载中
const showLoading = () => {
  Toast.loading('', 30, () => {
    window.networkError('./images/networkError-icon.png')
  })
}


//chunkName index
const indexComponents = (cb, path) => {
  require.ensure([], (require) => {
    cb(null, require(path).default);
  }, 'index');
}

//chunkName car
const carComponents = (cb, path) => {
  showLoading();
  require.ensure([], (require) => {
    Toast.hide();
    cb(null, require(path).default);
  }, 'car');
}

//chunkName violation
const violationComponents = (cb, path) => {
  showLoading();
  require.ensure([], (require) => {
    Toast.hide();
    cb(null, require(path).default);
  }, 'violation');
}

//chunkName order
const orderComponents = (cb, path) => {
  showLoading();
  require.ensure([], (require) => {
    Toast.hide();
    cb(null, require(path).default);
  }, 'order');
}


export default (
  <Route path="/" component={App}>
    {/*<IndexRoute component={CarList} />*/}
    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}


    <IndexRoute getComponents={(nextState, cb) => indexComponents(cb, './containers/carList/index')} />

    {/* 专门给外部跳转过来的URL使用，避免出现自动关闭webView的情况 */}
    <Route path="index" getComponents={(nextState, cb) => indexComponents(cb, './containers/carList/index')} />

    {/*车辆相关 start*/}
    <Route path="addCar" getComponents={(nextState, cb) => carComponents(cb, './containers/carInfo/addCar')} />
    <Route path="carDetail(/:id(/:getDatas))" getComponents={(nextState, cb) => carComponents(cb, './containers/carDetail/index_v1.3')} />
    <Route path="editCar/:id" getComponents={(nextState, cb) => carComponents(cb, './containers/carInfo/editCar')} />
    <Route path="replenishInfo/:id" getComponents={(nextState, cb) => carComponents(cb, './containers/carInfo/replenishInfo')} />
    <Route path="annualInfo/:carId" getComponents={(nextState, cb) => carComponents(cb, './components/calculatorOfYearlyCheck/demo')} />
    <Route path="brand(/:carId/:carNumber)" getComponents={(nextState, cb) => carComponents(cb, './components/Form/carbrand')} />
    <Route path="drivingLicenceList/:carId/(:licenceId)" getComponents={(nextState, cb) => carComponents(cb, './containers/drivingLicence/list')} />
    <Route path="addDrivingLicence/:carId" getComponents={(nextState, cb) => carComponents(cb, './containers/drivingLicence/addLicence')} />
    <Route path="editDrivingLicence/:carId/:licenceId" getComponents={(nextState, cb) => carComponents(cb, './containers/drivingLicence/editLicence')} />
    {/*车辆相关 end*/}

    {/*违章相关 start*/}
    <Route path="/updateCarRequirement/:carId(/:violationIds)" getComponents={(nextState, cb) => violationComponents(cb, './containers/updateCarRequirement/index')} />
    <Route path="/violationList/:carId" getComponents={(nextState, cb) => violationComponents(cb, './containers/violation/index')} />
    <Route path="/violationDetail/:violationId(/:carId)" getComponents={(nextState, cb) => violationComponents(cb, './containers/violation/detail')} />
    {/*违章相关 end*/}

    {/*订单相关 start*/}
    <Route path="/orderConfirm/:violationIds" getComponents={(nextState, cb) => orderComponents(cb, './containers/orderConfirm/index')} />
    <Route path="/orderDetail/:orderId(/:userId/:token/:userType/:authType)(/:from)" getComponents={(nextState, cb) => orderComponents(cb, './containers/orderDetail/index')} />
    <Route path="/orderPaySuccess/:orderId(/:orderTitle)" getComponents={(nextState, cb) => orderComponents(cb, './containers/orderPaySuccess/index.js')} />
    <Route path="/coupons/:couponIds(/:couponId)" getComponents={(nextState, cb) => orderComponents(cb, './containers/coupon/index')} />
    {/*订单相关 end*/}

    <Route path="*" component={NotFoundPage} />
  </Route>
);