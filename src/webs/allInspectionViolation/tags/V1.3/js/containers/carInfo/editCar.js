/*
* 编辑车辆信息
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn, Modal } from '../../components'
import { Switch } from 'antd-mobile';
import { TxtGroup, DateGroup, BrandGroup, CheckDateGroup } from '../../components/Form'
import styles from './editCar.scss'
import * as carListActions from '../../actions/carListActions'
import { Toast } from 'antd-mobile'
import moment from 'moment'
import common from '../../utils/common'
import carService from '../../services/carService';

const maxDate = moment()

class EditCar extends React.Component {
	constructor(props) {
		super(props);
		//设置初始状态
		//为避免页面跳出问题采用会话存储该页面的state,页面退出时注意清楚缓存
		let sessionState = sessionStorage.getItem("editCarState");
		// alert(sessionState)
		if (sessionState) {
			this.state = JSON.parse(sessionState);
		} else {
			this.state = this.getNewState({}, this.props);
		}
	}

	//获取新状态
	getNewState(state, props) {
		//定义相关变量
		let carId = props.params.id || "",
			isCurrentCarExist = props.cars.data[carId] ? true : false,//判断当前车辆信息是否存在
			condition = {},
			carNumber = '',
			carCodePlaceholder = "",
			engineNumberPlaceholder = "",
			registerDate = "",
			checkDate = "",
			carModelName = "",
			carModelId = '',
			userTelephone = "",
			carCode = '',
			engineNumber = '',
			isSubscribe = true,
			isProvincesExist = props.provinces.length > 0 ? true : false;

		if (isCurrentCarExist) {
			let currentCarData = props.cars.data[carId];

			carNumber = currentCarData.carNumber || "";
			carModelName = currentCarData.carModelName || "";
			carCode = currentCarData.carCode || "";
			engineNumber = currentCarData.engineNumber || "";
			isSubscribe = currentCarData.isSubscribe;
			userTelephone = currentCarData.userTelephone || "";
			registerDate = currentCarData.registerDate || "";
			checkDate = currentCarData.checkDate || "";
			carModelId = currentCarData.carModelId || "";

			//获取当前车牌前缀
			//alert(carNumber);
			let carNumberPrefix = carNumber.slice(0, 2);

			if (isProvincesExist) {
				condition = this.getCarCondition(carNumberPrefix, props)
				let { carCodeLen, carEngineLen } = condition

				carCodePlaceholder = this.getPlaceholder("车身架号", carCodeLen);
				engineNumberPlaceholder = this.getPlaceholder("发动机号", carEngineLen);
			};
		}
		let nextState = {
			carId, //车辆id
			carNumber, // 车牌
			isSubscribe,//是否开启消息通知
			userTelephone,//用户手机号码
			condition, //车辆查询条件
			registerDate,  // 注册日期
			carModelName,  //车辆品牌信息
			carModelId,
			checkDate,
			CarCode: {
				groupId: "carCode",//唯一编码
				label: '车身架号',//标签文本
				placeholder: carCodePlaceholder,//占位符文本
				handleChange: (e) => { this.preValiDate(e) },//输入框文本改变操作
				type: "text", //输入框type
				value: carCode,//输入框值
			},
			EngineNumber: {
				groupId: "engineNumber",//唯一编码
				label: '发动机号',//标签文本
				placeholder: engineNumberPlaceholder,//占位符文本
				handleChange: (e) => { this.preValiDate(e) },//输入框文本改变操作
				type: "text", //输入框type
				value: engineNumber,//输入框值
			},
			isProvincesExist, //是否存在省份信息
			isCurrentCarExist, //是否存在当前车辆信息
			isDirty: false,//判断输入信息是否改变
		}
		return { ...state, ...nextState }
	}

	componentWillMount() {
		//设置标题
		common.setViewTitle('车辆认证');
		let id = this.props.params.id
		//请求车辆查询约束条件
		if (this.props.provinces.length == 0) {
			this.props.actions.queryCarCondition()
		}
		//请求车辆信息
		if (!this.props.cars.data[id]) {
			this.props.actions.queryCarInfo({ carId: id })
		}

		//如果有注册日期，获取年检到期时间
		let registerDate = this.state.registerDate;
		if (registerDate) {
			carService.checkDateList({ registerDate }).then(result => {
				if (result.code == "1000") {
					this.setState({
						dateList: result.data.dateList.map(item => {
							return item.checkDate;
						})
					})
				} else {
					if (result.msg) {
						Toast.info(result.msg, 1)
					}
				}

			}).catch(e => {
				Toast.info("系统繁忙，请稍后再试", 1)
			})
		}
	}

	componentWillReceiveProps(nextProps) {
		let state = this.getNewState({}, nextProps);
		this.setState(state);
	}

	componentDidMount() {
		//组件加载完成

		//进入车辆认证页埋点
		common.sendCxytj({
			eventId: "Violation_EnterCarCertifiction"
		})

		let sessionState = sessionStorage.getItem("editCarState");
		// alert(sessionState)
		if (!sessionState) {
			//后退时显示确认对话框
			common.backShowConfirm(() => {
				let obj = {
					show: true, //显示对话框
					content: '编辑的信息尚未保存，确定退出？', //内容
					leftBtn: {
						name: '离开', //左按钮文字
						onClick: () => { sessionStorage.removeItem("showConfirm"); history.back() }  //点击左按钮
					},
					rightBtn: {
						name: '取消',//右按钮文字
						onClick: () => history.forward() //点击右按钮
					},
					onClose: () => history.forward() //点击关闭按钮
				}
				Modal.confirm(obj);
			})
		}else{
			sessionStorage.removeItem("editCarState");
		}
		//如果编辑车辆信息
		if (this.state.isDirty) {
			sessionStorage.setItem("showConfirm", 1);
		}

	}

	componentDidUpdate() {
		//如果编辑车辆信息
		if (this.state.isDirty) {
			sessionStorage.setItem("showConfirm", 1);
		}
	}

	componentWillUnmount() {
		//离开车辆认证页记录页面信息
		sessionStorage.setItem("prevPageInfo", document.title)
	}

	//获取车牌对应的违章查询条件
	getCarCondition(carNumberPrefix, props) {
		let provincePrefix = carNumberPrefix[0] //获取车牌前缀第一位
		let provinces = props.provinces;
		for (let i = 0; i < provinces.length; i++) {
			if (provinces[i].provincePrefix == provincePrefix) {
				let cities = provinces[i].cities;
				for (let j = 0; j < cities.length; j++) {
					let target = "";
					if (provincePrefix != "京" && provincePrefix != "津" && provincePrefix != "沪" && provincePrefix != "渝") {
						target = carNumberPrefix
					} else {
						target = provincePrefix
					}
					if (cities[j].carNumberPrefix == target) {
						return {
							carCodeLen: cities[j].carCodeLen,
							carEngineLen: cities[j].carEngineLen
						}
					}
				}
				break;
			}
		}
		return {
			carCodeLen: 99,
			carEngineLen: 99
		}
	}

	//除车牌号外其他输入框改变时判断
	preValiDate(e) {
		let target = e.target,
			name = e.target.name,
			len,
			value,
			isDirty = this.state.isDirty,
			currValue = target.value;
		switch (name) {
			case "carCode":
				len = this.state.condition.carCodeLen;
				value = currValue.trim()//this.cutInput(currValue, len);
				if (value != this.state.CarCode.value) {
					isDirty = true;
				}
				this.setState({
					isDirty,
					CarCode: { ...this.state.CarCode, value: value.toUpperCase() }
				})
				break;
			case "engineNumber":
				len = this.state.condition.engineNumber;
				value = currValue.trim()//this.cutInput(currValue, len)
				if (value != this.state.EngineNumber.value) {
					isDirty = true;
				}
				this.setState({
					isDirty,
					EngineNumber: { ...this.state.EngineNumber, value: value.toUpperCase() }
				})
				break;
			case "userTelephone":
				len = 11;
				value = this.cutInput(currValue, len)
				value = value.replace(/[^\d]/, "")
				if (value != this.state.userTelephone) {
					isDirty = true;
				}
				this.setState({
					isDirty,
					userTelephone: value
				})
				break;
		}
	}

	//提交修改数据
	saveCarInfo() {
		if (this.valiDate()) {
			let param = {
				carId: this.state.carId,
				carNumber: this.state.carNumber,
				isSubscribe: this.state.isSubscribe,
				carModelId: this.state.carModelId,
				carModelName: this.state.carModelName,
			};
			this.state.condition.carCodeLen > 0 && (param.carCode = this.state.CarCode.value);
			this.state.condition.carEngineLen > 0 && (param.engineNumber = this.state.EngineNumber.value);
			this.state.isSubscribe == 1 && (param.userTelephone = this.state.userTelephone);
			this.state.checkDate && (param.checkDate = this.state.checkDate); //只保存年月
			this.state.registerDate && (param.registerDate = this.state.registerDate);
			this.props.actions.queryUpdateCar(param);
		};
	}

	//截取输入框输入文本长度
	cutInput(value, len) {
		let output = value;
		if (value.length > len) {
			output = value.slice(0, len)
		}
		return output
	}

	//获取输入框placeholder
	getPlaceholder(txt, len) {
		switch (len) {
			case 99:
				return "请输入完整的" + txt
			default:
				return txt + "后" + len + "位数"
		}
	}

	//switch选择改变
	checkboxChange(checked) {
		this.setState({
			isDirty: true,
			isSubscribe: checked ? 1 : 0
		})
	}

	//点击保存车辆时前验证信息
	valiDate() {
		let hasCarCode = this.state.condition.carCodeLen > 0 ? true : false;
		let hasEngineNumber = this.state.condition.carEngineLen > 0 ? true : false;

		//验证车架号
		if (hasCarCode) {
			let carCode = this.state.CarCode.value;
			let len = this.state.condition.carCodeLen
			if (!this.valiCarCode(carCode, len)) {
				return false
			}
		}

		//验证发动机号
		if (hasEngineNumber) {
			let engineNumber = this.state.EngineNumber.value;
			let len = this.state.condition.carEngineLen
			if (!this.valiEngineNumber(engineNumber, len)) {
				return false
			}
		}

		//验证手机号码
		if (this.state.isSubscribe == 1) {
			let phone = this.state.userTelephone;
			if (!this.valiPhone(phone)) {
				return false
			}
		}

		return true;
	}

	//验证车架号信息
	valiCarCode(carCode, len) {
		// alert(carCode)
		if (len != 99) {
			if (carCode.length < len) {
				Toast.info("请输入正确的车身架号", 1)
				return false
			} else {
				return true
			}
		} else {
			if (carCode.length > 0) {
				return true
			} else {
				Toast.info("请输入正确的车身架号", 1)
				return false
			}
		}
	}

	//验证发动机号信息
	valiEngineNumber(engineNumber, len) {
		if (len != 99) {
			if (engineNumber.length < len) {
				Toast.info("请输入正确的发动机号", 1)
				return false
			} else {
				return true
			}
		} else {
			if (engineNumber.length > 0) {
				return true
			} else {
				Toast.info("请输入正确的发动机号", 1)
				return false
			}
		}
	}

	//验证手机信息
	valiPhone(phone) {
		if (phone.length != 11) {
			Toast.info("请输入正确的手机号", 1)
			return false
		} else {
			if (/^\d{11}$/.test(phone)) {
				return true
			} else {
				Toast.info("请输入正确的手机号", 1)
				return false
			}
		}
	}

	//注册日期选择变化
	registerDateChange(date) {
		let isDirty = this.state.isDirty;
		if (date.format("YYYY-MM-DD") != this.state.registerDate) {
			isDirty = true
		}
		this.setState({
			isDirty,
			registerDate: date.format("YYYY-MM-DD")
		})

		let registerDate = date.format("YYYY-MM-DD")

		//这里触发接口请求
		this.getRegisterDate(registerDate);
	}

	getRegisterDate(registerDate) {
		carService.checkDateList({ registerDate }).then(result => {

			if (result.code == "1000") {
				//计算推荐的年检到期时间
				let defaultList = result.data.dateList.filter(item => item.default == "1");
				let inspectDate = "";
				if (defaultList.length > 0) {
					inspectDate = defaultList[0].checkDate
				}
				// inspectDate = this.clcCheckDate(registerDate,result.data.dateList)

				let isDirty = this.state.isDirty;
				if (inspectDate != this.state.checkDate) {
					isDirty = true
				}

				this.setState({
					dateList: result.data.dateList.map(item => {
						return item.checkDate;
					}),
					checkDate: inspectDate,
					isDirty,
				})
			} else {
				if (result.msg) {
					Toast.info(result.msg, 1)
				}
			}

		}).catch(e => {
			Toast.info("系统繁忙，请稍后再试", 1)
		})
	}

	//打开品牌列表
	openBrandList() {
		sessionStorage.setItem("editCarState", JSON.stringify(this.state));
		window.open(common.getRootUrl() + "brand", '_self')
	}

	//点击删除按钮
	deleteBtnClick() {

		//点击车辆删除埋点
		common.sendCxytj({
			eventId: "Violation_DeleteCar"
		})

		let obj = {
			show: true, //显示对话框
			content: '确定要删除此车辆吗？', //内容
			leftBtn: {
				name: '删除', //左按钮文字
				onClick: () => this.confirmOk()  //点击左按钮
			},
			rightBtn: {
				name: '取消',//右按钮文字
				onClick: () => this.confirmCancel() //点击右按钮
			},
			onClose: () => this.confirmClose() //点击关闭按钮
		}
		Modal.confirm(obj)
	}

	//confirm确认事件
	confirmOk() {
		this.deleteCar()
	}

	//confirm取消事件
	confirmCancel() {
	}

	//confirm关闭事件
	confirmClose() {
	}

	//删除车辆
	deleteCar() {
		this.props.actions.queryDeleteCar({ carId: this.state.carId })
	}

	//获取年检到期时间
	getInspectDate(checkDate) {
		let isDirty = this.state.isDirty;
		if (checkDate != this.state.checkDate) {
			isDirty = true
		}
		this.setState({
			isDirty,
			checkDate: checkDate
		})
	}

	render() {

		//注册日期选择组件props
		const registerDateProps = {
			groupId: "registerDate",//唯一编码
			label: '车辆注册日期',//标签文本
			placeholder: '请选择',//占位符文本
			type: "date",//日期选择器类型
			format: "YYYY-MM-DD", //日期格式
			maxDate,
			handleChange: (v) => { this.registerDateChange(v) },//输入框文本改变操作
			value: this.state.registerDate ? moment(this.state.registerDate, 'YYYY-MM-DD') : "" //输入框值
		}

		//检验有效期选择组件props
		const checkDateProps = {
			groupId: "checkDate",//唯一编码
			label: '检验有效期至',//标签文本
			placeholder: '请选择',//占位符文本
			dateList: this.state.dateList || [], //检验有效期列表
			registerDate: this.state.registerDate, //车辆注册日期
			getInspectDate: d => this.getInspectDate(d), //获取检验有效期
			value: this.state.checkDate//输入框值
		}

		//品牌选择组件Props
		const carModelNameProps = {
			groupId: "carModelName",//唯一编码
			label: '品牌车系',//标签文本
			placeholder: '请选择',//占位符文本
			handleChange: () => this.openBrandList(),//输入框文本改变操作
			value: this.state.carModelName //输入框值
		}

		//按钮属性
		const BtnProps = {
			text: "保存",
			disabled: !this.state.isDirty,
			handleClick: () => this.saveCarInfo()
		}

		return (
			<div>
				<div className="wz_whiteSpace_30"></div>
				<div className="wz_headMsg">
					请输入{this.state.carNumber.slice(0, 2) + " " + this.state.carNumber.slice(2)}车辆的相关信息
					<div className={styles.deleteBtn} onClick={() => this.deleteBtnClick()}><i className={styles.icon}></i>删除</div>
				</div>
				<div className="wz_list">
					{this.state.condition.carCodeLen > 0 && <TxtGroup {...this.state.CarCode} />}
					{this.state.condition.carEngineLen > 0 && <TxtGroup {...this.state.EngineNumber} />}
					<DateGroup {...registerDateProps} />
					<CheckDateGroup {...checkDateProps} />
					<BrandGroup {...carModelNameProps} />
				</div>
				<div className="wz_whiteSpace_28"></div>
				<div className="wz_list">
					<div className="wz_item">
						<span className="label">新违章消息通知及年检提醒服务</span>
						<span className={styles.switch}>
							<Switch checked={this.state.isSubscribe} onChange={(checked) => this.checkboxChange(checked)} />
						</span>
					</div>
					<div className="wz_item">
						<span className={this.state.isSubscribe ? "label" : "label " + styles.phoneDisabled}>手机号码</span>
						{this.state.isSubscribe == 1 &&
							<input
								className="txt"
								placeholder="手机号码"
								name="userTelephone"
								value={this.state.userTelephone}
								onChange={(e) => this.preValiDate(e)}
								type="tel"
							/>
						}
					</div>
				</div>
				<div className="wz_whiteSpace_50"></div>
				<Btn {...BtnProps} />
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars: state.cars,
	provinces: state.provinces
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditCar)


