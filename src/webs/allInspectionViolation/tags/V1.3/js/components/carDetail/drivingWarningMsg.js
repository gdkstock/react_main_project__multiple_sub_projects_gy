/*
*	警告轮播信息组件
	props = {
		text:"",//轮播文本
	}
*/
import React from 'react'
import styles from './drivingWarningMsg.scss'

class DrivingWarningMsg extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			id:this.props.id,
            drivingWarningMsg:this.props.drivingWarningMsg
		}
	}

	componentDidMount(){
	}

	componentDidUpdate(){
		
	}

	componentWillUnmount(){
		// alert("组件卸载了")
		// alert(window.timer)
	}

	render(){
		return (
            <div className={styles.wrap+" "+styles.animate}>
                <div className={styles.angle}></div>
                <div className={styles.wraningMsg}>记分周期即将到期，建议将全部违章尽快处理完毕</div>
			</div>
		)
	}
}

export default DrivingWarningMsg	