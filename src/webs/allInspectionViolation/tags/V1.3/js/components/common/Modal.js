
import Style from "./Confirm.scss"

const Modal = {

	confirm(obj){
		// obj格式
		// {
		//     show: false, //显示对话框
		//     content: '', //内容
		//     leftBtn: {
		//         name: '取消', //左按钮文字
		//         onClick: () => console.log('点击了取消按钮') //点击左按钮
		//     },
		//     rightBtn: {
		//         name: '确认',//右按钮文字
		//         onClick: () => console.log('点击了确认按钮') //点击右按钮
		//     },
		//     onClose: () => console.log("点击了关闭按钮") //点击关闭按钮
		// }
		let { content, leftBtn, rightBtn, onClose } = obj

		//生成模态框的DOM节点
		let Modal = document.createElement("div");
		Modal.id = "Modal";
		Modal.className = Style.fixedBox;
		Modal.innerHTML = `<div id='confirm' class=${Style.confirm + ' ' + Style.hideConfirm}>
                    <i id="closeBtn" class=${Style.closeIcon}></i>
                    <div class=${Style.content}>${content}</div>
                    <div class=${Style.btns}>
                        <div id="leftBtn" class=${Style.leftBtn}>${leftBtn.name}</div>
                        <div id="rightBtn" class=${Style.rightBtn}>${rightBtn.name}</div>
                    </div>
                </div>`
		document.body.appendChild(Modal);

		//为按钮绑定单击事件
		let closeBtnDOM = document.querySelector("#closeBtn")
		let leftBtnDOM = document.querySelector("#leftBtn")
		let rightBtnDOM = document.querySelector("#rightBtn")
		closeBtnDOM.onclick = ()=>{this.onClickBtn(()=>onClose())}
		leftBtnDOM.onclick = ()=>{this.onClickBtn(()=>leftBtn.onClick())}
		rightBtnDOM.onclick = ()=>{this.onClickBtn(()=>rightBtn.onClick())}

		document.querySelector("#confirm").className = Style.confirm + ' ' + Style.showConfirm;//为了动画效果放在最后
	},

	onClickBtn(callback) {
		let confirm = document.querySelector('#confirm');
        confirm.className = Style.confirm + ' ' + Style.hideConfirm;
        setTimeout(() => {
            try {
                callback && callback()
                // confirm.className = Style.confirm + ' ' + Style.showConfirm
                this.hide()
            } catch (error) {
                console.error(error)
            }
        }, 200)
    },

	hide(){
		//移除DOM节点
		let Modal = document.querySelector("#Modal");
        Modal.parentNode.removeChild(Modal);
	}
}

export default Modal