/*
 *  广告轮播组件
    props={
        list:[] ,//广告列表
        handleClick:()=>{} ,//点击广告触发的事件
        positionCode:""//广告位置
    }
 */
import React from 'react'
import {Carousel} from 'antd-mobile'
import styles from './index.scss'

const Banner=(props)=>{
    const bannerList = props.list || [];
    const canPlay = bannerList.length>1?true:false; //是否能够轮播
    let isLG = props.positionCode == "WZ_CAR_SY_01" ? true : false
    let calssName = isLG? styles.banner+" "+styles.lg:styles.banner+" "+styles.sm
    return (
        <div>
            {bannerList.length>0 &&
                <Carousel 
                    dots={false} 
                    autoplay={canPlay} 
                    infinite={canPlay} 
                    swiping={canPlay}
                >
                    {
                        bannerList.map((item,index)=>
                            <div key={index} onClick={()=>props.handleClick(item)}>
                                <img 
                                    src={item.imageUrl}  
                                    className={ calssName}
                                />
                            </div>
                        )
                    }
                </Carousel>
            }   
        </div>
    );
};

export default Banner