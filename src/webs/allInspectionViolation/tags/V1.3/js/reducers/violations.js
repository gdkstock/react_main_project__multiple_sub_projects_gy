import {
	ADD_VIOLATION,
	ADD_VIOLATIONS,
	UPDATE_VIOLATION,
	DELETE_VIOLATION,
	GET_VIOLATION_ASYNC,
	GET_VIOLATION_LIST_ASYNC,
	UPDATE_VIOLATION_ASYNC,
	DELETE_PROXY_RULE_KEY
} from '../actions/actionsTypes'

//测试
let initState = {
	data: {
		'violationId1': {
			carId: '123456', //车辆ID
			violationId: 'violationId1', //违章ID
			backendId: '1234', //接口返回违章id，下单时使用
			occurTime: '2017-5-18 17:28:12', //违章发生时间（车管所中记录的时间）
			reason: '机动车违反规定停放、临时停车，驾驶人拒绝立即驶离，妨碍其他车辆、行人通行。', //违章原因
			fine: '200', //违章罚金,单位：元
			lateFine: '0', //总滞纳金,单位：元(目前没有使用？)
			degree: '6', //违章扣分
			realDegree: '6', //实际扣分
			poundage: '7', //服务费(合作价),单位：元
			cityId: '7', //违章城市id，数据中主键
			cityName: '广东广州', //违章城市名称
			location: '揭阳县中山路段阿萨德刚阿萨德刚', //违章地点信息
			longitude: '111', //经度
			latitude: '222', //纬度
			violationLocation: '121121212', //违章定位地址
			cityCode: '1111', //违章定位城市编码，区号
			canProcess: '1', //是否可代办标记0：不可代办1：可代办
			canProcessMsg: '可以代办的时候，这句话是不应该出现的', //是否可办理的提示信息
			violationStatus: '0', //违章的处理状态 0：待处理；1：已处理；2：处理中
			violationCode: '111111111', //违章代码
			archive: 'sdgaddaga', //文书编号
			isLocale: '0', //是否为现场单（0 非现场单；1 现场单）
			isWarn: '0', //是否为警告单（0 非警告单；1 警告单）6.0.0开始启用
			fkdjUrl: '', //罚款代缴请求Url
			isCantAutoSelect: '', //不能自动选上标识：1：该条违章不可以自动选择 其他：可以自动选择
			proxyRule: '' //需要补充的资料结构体（有返回字段时，表示需要补充的资料）
		},
		'violationId2': {
			carId: '123456', //车辆ID
			violationId: 'violationId2', //违章ID
			backendId: '1234', //接口返回违章id，下单时使用
			occurTime: '2017-5-18 17:28:12', //违章发生时间（车管所中记录的时间）
			reason: '机动车违反规定停放、临时停车，驾驶人拒绝立即驶离，妨碍其他车辆、行人通行。', //违章原因
			fine: '200', //违章罚金,单位：元
			lateFine: '0', //总滞纳金,单位：元(目前没有使用？)
			degree: '6', //违章扣分
			realDegree: '6', //实际扣分
			poundage: '7', //服务费(合作价),单位：元
			cityId: '7', //违章城市id，数据中主键
			cityName: '北京', //违章城市名称
			location: '揭阳县中山路段阿萨德刚阿萨德刚', //违章地点信息
			longitude: '111', //经度
			latitude: '222', //纬度
			violationLocation: '121121212', //违章定位地址
			cityCode: '1111', //违章定位城市编码，区号
			canProcess: '1', //是否可代办标记0：不可代办1：可代办
			canProcessMsg: '可以代办的时候，这句话是不应该出现的', //是否可办理的提示信息
			violationStatus: '0', //违章的处理状态 0：待处理；1：已处理；2：处理中
			violationCode: '111111111', //违章代码
			archive: 'sdgaddaga', //文书编号
			isLocale: '0', //是否为现场单（0 非现场单；1 现场单）
			isWarn: '0', //是否为警告单（0 非警告单；1 警告单）6.0.0开始启用
			fkdjUrl: '', //罚款代缴请求Url
			isCantAutoSelect: '', //不能自动选上标识：1：该条违章不可以自动选择 其他：可以自动选择
			proxyRule: '' //需要补充的资料结构体（有返回字段时，表示需要补充的资料）
		}
	},
	result: []
}

initState = {
	data: {},
	result: []
}

export default function violations(state = initState, action) {
	let _data = {}, _result = [], carId = '';
	switch (action.type) {
		case ADD_VIOLATIONS:
			//添加违章列表
			if (action.data.result && action.data.result.length > 0) {
				carId = action.data.data[action.data.result[0]].carId
				state.result.map(violationId => {
					if (state.data[violationId].carId != carId) {
						//过滤之前的违章数据
						_data[violationId] = state.data[violationId];
						_result.push(violationId);
					}
				})
				return {
					data: Object.assign({}, _data, action.data.data),
					result: Array.from(new Set([..._result, ...action.data.result]))
				}

				// return {
				// 	data: action.data.data,
				// 	result: action.data.result
				// }
			}
		case UPDATE_VIOLATION:
			//更新违章信息
			if (state.result.indexOf(action.data.violationId) !== -1) {

				return {
					data: Object.assign({}, state.data, {
						[action.data.violationId]: Object.assign({}, state.data[action.data.violationId], action.data)
					}),
					result: state.result
				}
			} else {
				return state
			}
		case DELETE_PROXY_RULE_KEY:
			//删除所需补充资料的key
			_data = Object.assign({}, state.data) //复制data
			state.result.map(item => {
				if (_data[item].carId == action.data.carId) { //只修改对应的车辆
					for (let key in action.data) {
						delete (_data[item].proxyRule[key])
					}
				}

			})
			return {
				data: Object.assign({}, state.data, _data),
				result: state.result
			}
		default:
			return state;
	}
}