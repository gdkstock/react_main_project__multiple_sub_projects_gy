/**
 * 配置
 */
import common from './utils/common'

const userId = sessionStorage.getItem('userId') || ''
const token = sessionStorage.getItem('token') || ''
const userType = sessionStorage.getItem('userType') || common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay'
const production = window.location.host.indexOf('daiban.cx580.com') > -1 || window.location.host.indexOf('pre.cx580.com') > -1; //是否为生产环境或预发布环境
const authInfo = `userId=${userId}&token=${token}&userType=${userType}&authType=${authType}` //授权信息
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?forceLogin=true&userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;
const inspectionReminderUrl = production
    ? `https://annualcheck.cx580.com/user/bindInfo?authType=${authType}&userType=${userType}&clientId=CheWu&redirectUrl=https://annualcheck.cx580.com/inspection/index.html?detailUserType=${userType}/%23/remindHome`
    : `http://192.168.1.165:7083/user/bindInfo?authType=${authType}&userType=${userType}&clientId=CheWu&redirectUrl=http://192.168.1.165:7083/inspection/index.html?detailUserType=${userType}/%23/remindHome`;
const addCarUrl = (production ? (window.location.protocol + "//" + window.location.host + "/" + "AddCar_v1.3/index.html") : `http://webtest.cx580.com:9021/AddCar/index.html`) + `?${authInfo}`

export default {
    //是否为生产环境
    production: production,

    //接口地址
    baseApiUrl: production
        ? window.location.protocol + "//" + window.location.host + "/"
        : window.location.host.indexOf('testnodejs.cx580.com') > -1
            ? "http://testnodejs.cx580.com/backend/" //node java 同一台服务器的调试地址
            : "http://webtest.cx580.com:9021/",

    //单点登录地址 回调地址需自行补全
    authUrl: authUrl,

    //年检提醒URL carId和carNumber需要拼接进来（示例：inspectionReminderUrl+'&carId=1carNumber=粤A12345'）
    inspectionReminderUrl: inspectionReminderUrl,

    //首页URL
    indexPageUrl: window.location.protocol + "//" + window.location.host + window.location.pathname + `?${authInfo}`,

    //我的订单URL
    myOrderUrl: (production ? `https://h5index.cx580.com:9027/MyOrder/index.html` : `http://webtest.cx580.com:9026/MyOrder/index.html`) + `?userType=${userType}`,

    //vip业务入口
    vipInfoUrl: (production ? `https://vip.cx580.com/auth?userType=` : `http://webtest.cx580.com:20004/auth?userType=`) + userType,

    //添加车辆
    addCarUrl: addCarUrl,

    //常见问题地址
    helpUrl: production ? 'https://mobile.cx580.com:4443/H5/more/help.html' : 'http://mobile.cx580.com:14443/H5/more/help.html',

    //在线客服地址
    udeskUrl: window.location.protocol + '//cx580.udesk.cn/im_client/?web_plugin_id=24449&api_version=3',

    // 支付JSDK
    cxyPaysJS: {
        reqwest: '//oss.cx580.com/public_script/reqwest.min.js',
        cxyPays: production ? '//oss.cx580.com/public_script/cxyPays.min.js?t=1' : '//oss.cx580.com/cweb/js/cxyPays.min.js?t=1',
    },

    //debug的userId账号
    debugUsers: ['TEST'],
}