/**
 * 支付方式公共组件
*/

import styles from './payWay.scss'

const PayWay = props => {
    return (
        <div className={styles.container}>
            <div className={styles.title}>支付方式</div>
            <div className={styles.item} onClick={()=>props.payWayChange()}>
                <img className={styles.payWayIcon} src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1508301726061&di=9118d2f60ac79f8f85b8000d09574a14&imgtype=jpg&src=http%3A%2F%2Fimg0.imgtn.bdimg.com%2Fit%2Fu%3D2657703228%2C3102632875%26fm%3D214%26gp%3D0.jpg"/>
                <span>微信支付</span>
                <div className={styles.selectIcon+" "+styles.selected}></div>
            </div>
        </div>
    )
}

export default PayWay