import Style from './index.scss'

/**
 * 违章卡片
 * @param {*object} props
 * {
 * cardId:'', //卡片的唯一标识符
 * clickCheckbox: () => false, //点击了勾选框
 * clickRight: () => false, //点击了右边区域
 * clickFooterText: () => false, //点击了补充资料区域
 * isNew: false, //未读
 * extendData: true, //已补充资料
 * canProcess: false, //是否可代办
 * title:'广东揭阳揭西县中山路段广东揭阳揭西县中山路段', //标题
 * occurTime:'2017-05-16 09:37', //违章发生时间
 * reason:'机动车违反规定停放、临时停车，驾驶人拒绝立即驶离，妨碍其他车辆、行人通行。', //违章原因
 * degree: '3', //扣分
 * fine: '200', //罚款
 * violationStatus: '0', //违章处理状态 0：待处理；1：已处理；2：处理中
 * footerText:'因违法行为涉及扣分事项，须驾驶者本人前往当地处罚机关接受处理。', //底部提示文案
 * checked:false, //选中
 * canProcessMsg:'',// 可否办理提示信息
 * }
 */
export const Card = props => {
    let _props = Object.assign({
        clickCheckbox: () => false, //点击了勾选框
        clickRight: () => false, //点击了右边区域
        clickFooterText: () => false, //点击了补充资料区域
        checked: false, //默认不选中
        clickDealBtn: () => false,//点击去办理按钮
        clickConsult: () => false,//点击去咨询按钮
        clickRealDegree: () => false, //点击扣分不执行说明icon
    }, props) //自定义的props事件

    let { cardId, carNumber, isNew, extendData, canProcess, title,
        occurTime, reason, degree, realDegree,
        fine, violationStatus, footerText, checked, canProcessMsg,
        isLocale, archive, informations, cooperPoundage, poundage,
    } = props;

    // 车主服务
    const userType = sessionStorage.getItem('userType') || '';
    const isAlipayCarService = userType.toLocaleLowerCase() === 'alipaycarservice';
    if (isAlipayCarService) {
        // 服务费超过40元则不可办理
        if (cooperPoundage > 40) {
            canProcess = 0;
        }

        // 闽牌车不可办理
        if (carNumber.substr(0, 1) === '闽') {
            canProcess = 0;
        }
    }

    // 服务费减免  原价不等于活动价 并且 优惠不等于0
    const poundageText = poundage && poundage > 0 && cooperPoundage && cooperPoundage > 0 && poundage - cooperPoundage > 0
        ? `办理该条违章可享服务费减免${poundage - cooperPoundage}元`
        : '';

    return (
        <div className={Style.cardBox}>
            <div className={Style.whileBg}></div>
            <div className={Style.card}>
                <div style={{ position: 'relative' }}>
                    <div className={Style.checkbox + (canProcess != '0' && violationStatus == '0' ? checked ? ' checkBoxTrue' : ' checkBoxFalse' : 'hide')} onTouchStart={id => _props.clickCheckbox(cardId)}></div>
                    <div className={Style.cardInfo}>
                        <div className={Style.cardRight} onClick={id => _props.clickRight(cardId)}>
                            <div className={isNew ? Style.newIcon : 'hide'}></div>
                            <div className={Style.cardTop}>
                                <span>{title}</span>
                                {isLocale == "1" ? <img className={Style.cardIcon} src={cardIcons[violationStatus]} />
                                    : violationStatus != '0'
                                        ? <img className={Style.cardIcon} src={cardIcons[violationStatus]} />
                                        : canProcess == '0'
                                            ? <img className={Style.cardIcon} src={cardIcons['3']} />
                                            : !extendData
                                                ? <img className={Style.cardIcon} src={cardIcons['4']} />
                                                : <img className={Style.cardIcon} src={cardIcons[violationStatus]} />
                                }
                                <div className={Style.rightIcon}></div>
                            </div>
                            <div className={Style.cardText}>
                                <p className={Style.cardTime}>{occurTime}</p>
                                <p className={Style.cardMsg}>{reason}</p>
                            </div>
                        </div>
                    </div>
                    <div className={Style.degreeBox}>
                        {isAlipayCarService && cooperPoundage && cooperPoundage > 0 ? <span>服务费{cooperPoundage}元</span> : ''}
                        <span>罚款{fine || '0'}元</span>
                        <span>扣{degree || '0'}分{degree > 0 && realDegree == '0' ? '不执行' : ''}
                            {degree > 0 && realDegree == '0' && <i className="icon-prompt" onClick={() => _props.clickRealDegree()}></i>}
                        </span>
                    </div>
                </div>
                {
                    violationStatus == "0" && isLocale == "1" ?
                        <div className={Style.localeWrap} onClick={id => _props.clickDealBtn(cardId)}>
                            决定书编号：{archive}
                            <span>去办理</span>
                        </div> :
                        violationStatus == "0" && informations && informations.length > 0 ?
                            <div className={Style.localeWrap} onClick={url => _props.clickConsult(informations[0].informationUrl)}>
                                可到线下合作门店咨询办理
                            <span>咨询办理</span>
                            </div> : ""
                }
                {
                    poundageText ?
                        <div className={Style.cardFooterText} onClick={id => _props.clickFooterText(cardId)}>
                            <p>
                                <span className={Style.iconTehui}>特惠</span>
                                {poundageText}
                            </p>
                        </div>
                        :
                        <div className={!canProcessMsg || isLocale == "1" ? 'hide' : Style.cardFooterText} onClick={id => _props.clickFooterText(cardId)}>
                            {/*{props.footerText || <p>一分钟补充资料，即可在线办理。 <span>补充资料>></span></p>}*/}
                            {canProcessMsg}
                        </div>
                }
            </div>
            <div className="h20"></div>
        </div>
    )
}

//卡片办理状态图标
const cardIcons = {
    '0': './images/process-icon0.png', //可办理
    '1': './images/process-icon1.png', //已完成
    '2': './images/process-icon2.png', //办理中
    '3': './images/process-icon3.png', //不可办理
    '4': './images/process-icon4.png', //补充资料
}

//无违章
export const EmptyViolation = props => {
    let { noViolation, handleClick } = props
    let textClass = noViolation ? Style.fontA : "";
    return (
        <div className={Style.emptyViolation}>
            <img src='./images/bitmap.png' />
            <div className={Style.emptyViolationText + " " + textClass}>{props.text || '该车辆暂无违章'}</div>
            {noViolation && <div className={Style.guidance}>
                <div className={Style.guidanceTitle}>{noViolation.title}</div>
                <div className={Style.guidanceBtn} onClick={() => handleClick(noViolation)}>{noViolation.description}</div>
            </div>}
        </div>
    )
}