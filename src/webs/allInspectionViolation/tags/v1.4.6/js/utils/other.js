/**
 * 其他jS文件
 */
import common from './common'
import jsApi from './jsApi'


/**
 * 需要关闭webView
 */
window.needCloseWebView = () => {
    //判断是否应该关闭webView
    let url = window.location.href;

    //当前地址和URL中的地址相同时，关闭webView
    let closeWebViewUrl = sessionStorage.getItem('closeWebViewUrl');
    if (closeWebViewUrl) {
        if (closeWebViewUrl == url) {
            try {
                closeWebView(); //关闭webView
            } catch (error) {
                console.log("closeWebView出错了：", error);
            }
            return true; //需要关闭webView
        }
    }

    //当前地址和URL中的地址不相同时，关闭webView
    let noCloseWebViewUrl = sessionStorage.getItem('noCloseWebViewUrl');
    if (noCloseWebViewUrl) {
        if (noCloseWebViewUrl != url) {
            try {
                if (noCloseWebViewUrl.indexOf('/orderPaySuccess/') !== -1 && url.indexOf('/orderPaySuccess/') !== -1) {
                    //支付成功页 不关闭web
                } else if (url.indexOf('/orderDetail/') !== -1) {
                    //订单详情页面 不关闭web
                } else {
                    closeWebView(); //关闭webView
                }
            } catch (error) {
                console.log("closeWebView出错了：", error);
            }
            return true; //需要关闭webView
        }
    }
    return false;
}

/**
 * 关闭webView
 */
window.closeWebView = () => {
    var userType = sessionStorage.getItem('userType') + '';
    userType = userType.toLocaleLowerCase();
    var ua = navigator.userAgent;
    if (userType == 'weixin') {
        if (window.jsApiIsLoad) {
            closeWeiXinWebView() //执行JsApi关闭微信webView
        } else {
            window.jsApiIsReady = function () {
                closeWeiXinWebView() //执行JsApi关闭微信webView
            }
        }
    } else if (userType == 'qq') {
        //QQ不存在关闭的问题
    } else if (userType == "alipay" && ua.indexOf("AlipayClient") !== -1) { //支付宝
        if (window.AlipayJSBridge) {
            AlipayJSBridge.call('closeWebview');
        }
    } else if (userType == 'app') {
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "close",
            }, function (data) {

            });
        } catch (e) {
        }
    }
};

window.closeWeiXinWebView = () => {
    wx.closeWindow();
    setTimeout(function () {
        wx.closeWindow();
    }, 500);
    setTimeout(function () {
        wx.closeWindow();
    }, 1000);
    setTimeout(function () {
        wx.closeWindow();
    }, 2000);
}

/**
 * 监听返回
 */
window.onpopstate = function (e) {
    if (sessionStorage.getItem('closeWebViewUrl')) {
        return; //需要关闭webView的时候 不再往下执行
    }
    // console.log("history state:", e.state, "url:", window.location.href)
    let Modal = document.getElementById('Modal');
    if (e.state) {
        if (e.state.close) {
            closeWebView(); //关闭webView 注：处于首页的时候才执行关闭webView操作
        }
        if (e.state.toUrl) {
            sessionStorage.setItem('closeWebViewUrl', window.location.href); //关闭webView的URL
            window.location.href = e.state.toUrl; //跳转到对应的页面
        }

        if (e.state.confirm) {
            //后退时弹窗
            try {
                if (sessionStorage.getItem('showConfirm')) {
                    if (window['stateConfirmCallback']) {
                        window['stateConfirmCallback']();
                    }
                } else {
                    jsApi.back();
                }
            } catch (error) {
                console.error(error);
            }
        } else if (Modal && document.body) {
            //离开页面时，关闭弹窗
            document.body.removeChild(Modal);
        }
    } else {
        //离开页面时，关闭弹窗
        if (Modal && document.body) {
            document.body.removeChild(Modal);
            sessionStorage.removeItem('showConfirm'); //移除弹窗判断条件
        }
    }
};

/**
 * app渠道首次登陆、切换用户时关闭webView
*/

(function () {
    if (common.isCXYApp()) {
        if (window.location.href.indexOf("app_cxy_pre_userId") > -1) {
            let preUserId = common.getUrlKeyValue("app_cxy_pre_userId");
            let curUserId = sessionStorage.getItem("userId");
            if (preUserId != curUserId) {
                jsApi.closeAppView();
            }
        }
    }
})()

setInterval(function () {
    needCloseWebView();
}, 100); //避免微信后退不刷新，所以这里定时循环

/**
 * 加载QQ告警js
 */
if (common.getJsApiUserType() === 'qq') {
    if (window.jsApiIsLoad) {
        common.requireJs('//h5.qianbao.qq.com/plugin?mod=1&rpt_bid=208&monitor_try=31');
    } else {
        document.addEventListener('jsApiIsReady', () => common.requireJs('//h5.qianbao.qq.com/plugin?mod=1&rpt_bid=208&monitor_try=31'));
    }
}