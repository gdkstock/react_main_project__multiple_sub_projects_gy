/**
 * 支付方式公共组件
*/

import styles from './payType.scss'

const defaultProps = {
    curPayType: "3",
    defaultPayType: "2",
    payTypeList: [
        {
            "payType": "2",
            "payTypeName": "支付宝",
            "selectable": "1",
            "icon": "http://oss.cx580.com/uiicon/alipay.png"
        },
        {
            "payType": "3",
            "payTypeName": "微信",
            "selectable": "1",
            "icon": "http://oss.cx580.com/uiicon/wechat.png"
        },
        {
            "payType": "13",
            "payTypeName": "钱包",
            "icon": "http://oss.cx580.com/uiicon/money.png",
            "info": "(剩余￥2147483647)",
            "selectable": "1"
        }
    ]
}

const PayType = props => {
    let { curPayType, payTypeList } = props
    return (
        <div className={styles.container}>
            <div className={styles.title}>支付方式</div>
            {
                payTypeList && payTypeList.map((item, index) =>
                    <div className={item.selectable == "1" ? styles.item : (styles.item + " " + styles.disable)} onClick={item.selectable == "1" ? () => props.payTypeChange(item.payType) : () => { }} key={index}>
                        <img className={styles.payTypeIcon} src={item.icon} />
                        <div className={styles.content}>
                            <p>{item.payTypeName}</p>
                            {item.info && <p className={styles.info}>{item.info}</p>}
                        </div>
                        <div className={curPayType == item.payType ? (styles.selectIcon + " " + styles.selected) : styles.selectIcon}></div>
                    </div>
                )
            }
        </div>
    )
}

export default PayType