/**
 * 违章相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    ADD_VIOLATION,
    ADD_VIOLATIONS,
    UPDATE_VIOLATION,
    DELETE_VIOLATION,
    GET_VIOLATION_ASYNC,
    GET_VIOLATION_LIST_ASYNC,
    UPDATE_VIOLATION_ASYNC,
    GET_LOCATION_VIOLATION_NUMBER_ASYNC,
    GET_REQUIREMENT_ASYNC,
    UPDATE_CAR,
    GET_CARS_VIOLATIONS,
    ADD_CAR_LIST,
    FETCH_FAILED,
    GET_COMMENT,//fetch请求出错
} from '../actions/actionsTypes'

//service
import violationService from '../services/violationService'
import carService from '../services/carService'

//actions
import * as violationActions from '../actions/violationActions'
import * as carListActions from '../actions/carListActions'

import { ChMessage } from '../utils/message.config' //提示信息

import { Toast } from 'antd-mobile' //UI

import { normalize, schema } from 'normalizr'; //范式化库

function* getViolationList(action) {
    try {
        if (action.showLoading) {
            Toast.loading('', 0)
        }

        if (!window.myCache_getViolationList) {
            window.myCache_getViolationList = {};
        }

        const { result, timeout } = yield window.myCache_getViolationList[action.data.carId] || race({
            result: call(violationService.getViolationList, action.data),
            timeout: call(delay, 30000)
        })
        Toast.hide();
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            if (result.code == '1000') {
                // 保存请求成功的接口 避免重复请求接口
                window.myCache_getViolationList[action.data.carId] = { result };

                const violation = new schema.Entity('violation', {}, { idAttribute: 'violationId' });
                const violations = new schema.Array(violation)
                const normalizedData = normalize(result.data.violationList, violations)
                if (normalizedData.result.length > 0) {
                    yield put(violationActions.addViolations({
                        data: normalizedData.entities.violation,
                        result: normalizedData.result
                    }))
                }
            } else {
                Toast.info(result.msg || ChMessage.FETCH_FAILED);
            }

        }
        action.callback(result); //回调函数
    } catch (error) {
        Toast.hide();
        yield put({ type: FETCH_FAILED, error })
        Toast.info(ChMessage.FETCH_FAILED)
        action.callback(error); //回调函数
    }
}

function* getLocationViolationNumber(action) {
    try {
        if (!window.myCache_getLocationViolationNumber) {
            window.myCache_getLocationViolationNumber = {};
        }

        const { result, timeout } = yield window.myCache_getLocationViolationNumber[action.violationId] || race({
            result: call(violationService.locationViolationNumber, action.data),
            timeout: call(delay, 30000)
        })

        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            if (result.code == '1000') {
                window.myCache_getLocationViolationNumber[action.violationId] = { result };

                yield put(violationActions.updateViolation({ violationId: action.violationId, locationViolationNumber: result.data.number }))
                if (action.callback) {
                    action.callback(result)
                }
            }
        }

    } catch (error) {
        console.log("error", error)
        yield put({ type: FETCH_FAILED, error })
    }
}

function* getRequirement(action) {
    try {
        if (sessionStorage.getItem('getRequirement-' + JSON.stringify(action.data))) {
            //数据已经存在，这里不再请求接口
            if (action.callback) {
                action.callback(); //回调函数
            }
        } else {
            //未请求过数据
            sessionStorage.setItem('getRequirement-' + JSON.stringify(action.data), 1) //保存数据 ，避免重复请求接口
            Toast.loading('', 0)
            const { result, timeout } = yield race({
                result: call(carService.getRequirement, action.data),
                timeout: call(delay, 30000)
            })
            Toast.hide()
            if (timeout) {
                window.networkError('./images/networkError-icon.png');
            } else {
                if (result.code == '1000') {
                    try {
                        if (Object.keys(result.data.requirements).length > 0) {
                            yield put({
                                type: UPDATE_CAR,
                                data: Object.assign({ carId: action.data.carId }, result.data.requirements)
                            })
                        }
                    } catch (error) {
                        //不知道这里为什么会出错
                        console.log("getRequirement出错了", error)
                    }
                } else {
                    if (result.msg) {
                        Toast.info(result.msg)
                    }
                }
            }
            if (action.callback) {
                action.callback(); //回调函数
            }
        }
    } catch (error) {
        Toast.hide()
        if (action.callback) {
            action.callback(); //回调函数
        }
        yield put({ type: FETCH_FAILED, error })
    }
}

function* getCarsViolations(action) {
    let { carId } = action.data
    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(carService.carList, {}),
            timeout: call(delay, 30000)
        })
        Toast.hide();
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            if (result.code == "1000") {
                let list = result.data.carViolationInfoList || []

                //获取车辆列表
                yield put({ type: ADD_CAR_LIST, data: list })

                //获取违章列表
                yield put({
                    type: GET_VIOLATION_LIST_ASYNC,
                    data: {
                        type: '3', //全部
                        carId: carId
                    },
                    callback: action.callback
                })
            }
        }
    } catch (error) {
        Toast.hide();
        if (action.callback) {
            action.callback(error)
        }
    }
}

function* getComment(action) {
    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(violationService.getComment, action.data),
            timeout: call(delay, 30000)
        })
        Toast.hide();
        if (timeout) {
            window.networkError('./images/networkError-icon.png');
        } else {
            if (result.code == "1000") {
                if (action.callback) {
                    action.callback(result)
                }
            }
        }
    } catch (error) {
        Toast.hide();
    }
}

function* watchGetViolationList() {

    yield takeEvery(GET_VIOLATION_LIST_ASYNC, getViolationList)

}

function* watchGetLocationViolationNumber() {
    yield takeLatest(GET_LOCATION_VIOLATION_NUMBER_ASYNC, getLocationViolationNumber)
}

function* watchgetRequirementAsync() {
    yield takeLatest(GET_REQUIREMENT_ASYNC, getRequirement)
}

function* watchGetCarsViolations() {
    yield takeLatest(GET_CARS_VIOLATIONS, getCarsViolations)
}

function* watchGetComment() {
    yield takeLatest(GET_COMMENT, getComment)
}

export function* watchVoilationsFetch() {
    yield [
        fork(watchGetViolationList),
        fork(watchGetLocationViolationNumber),
        fork(watchgetRequirementAsync),
        fork(watchGetCarsViolations),
        fork(watchGetComment)
    ]
}