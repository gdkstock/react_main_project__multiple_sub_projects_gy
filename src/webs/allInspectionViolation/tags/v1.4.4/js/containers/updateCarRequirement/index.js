/**
 * 补充代办所需资料
 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//组件
import Modal from '../../components/common/Modal'

//style
import Style from './index.scss'

//UI
import { Toast, Icon } from 'antd-mobile'

//actions
import { deleteProxyRuleKey } from '../../actions/violationActions'

//server
import carService from '../../services/carService'
import UploadService from '../../services/uploadService'

//常用工具类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config'
require("lrz")

var postDataInit = true;
class UpdateCarRequirement extends Component {
    constructor(props) {
        super(props);

        this.state = {
            postData: {}, //post提交的所需资料列表数据
            showSendConfirm: false, // 点击“保存并办理”按钮时 显示确认提示框
        }
    }

    componentWillMount() {
        common.setViewTitle('补充代办资料');
        postDataInit = true;
    }

    componentDidMount() {
        //后退时显示确认对话框
        common.backShowConfirm(() => {
            let obj = {
                show: true, //显示对话框
                content: '编辑的信息尚未保存，确定退出？', //内容
                leftBtn: {
                    name: '离开', //左按钮文字
                    onClick: () => { sessionStorage.removeItem("showConfirm"); history.back() }  //点击左按钮
                },
                rightBtn: {
                    name: '取消',//右按钮文字
                    onClick: () => history.forward() //点击右按钮
                },
                onClose: () => history.forward() //点击关闭按钮
            }
            Modal.confirm(obj);
        });

        // 判断是否需要显示确认提示框
        const { query } = this.props.location;
        const { MajorViolation, MajorSecondViolation, JashiZhengHaoLen, DanganBianHaoLen, FilePhoneLen } = query;
        if (MajorViolation || MajorSecondViolation || JashiZhengHaoLen || DanganBianHaoLen || FilePhoneLen) {
            const { carId } = this.props.params
            const { carNumber } = this.props.cars.data[carId] ? this.props.cars.data[carId] : { carNumber: '' };
            if (carNumber.substr(0, 1) !== '粤' && carNumber.substr(0, 2) !== '鄂A') {
                this.setState({
                    showSendConfirm: true, // 显示确认提示框
                })
            }
        }
    }

    componentWillUnmount() {
        sessionStorage.setItem("prevPageInfo", document.title)
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    /**
	 * 图片上传
	 */
    uploadImg(target, name) {
        sessionStorage.setItem('showConfirm', 1); //返回时显示提示框的标识符
        this._lrz(target.files[0], name);
    }

    /**
	 * 图片压缩上传
	 * @param e 图片资源
	 */
    _lrz(files, name) {
        this.setState({
            [name + 'Loading']: true //显示加载中
        })
        try {
            let quality = 1;
            if (files.size > 1024 * 1024 * 5) {
                quality = .5;
            }
            else if (files.size > 1024 * 1024 * 2) {
                quality = .5;
            }
            else if (files.size > 1024 * 1024) {
                quality = .5;
            }
            else if (files.size > 1024 * 500) {
                quality = .4;
            }
            else if (files.size > 1024 * 100) {
                quality = .5;
            } else {
                quality = .7;
            }

            lrz(files, {
                width: 1024,
                quality: quality
            }).then((rst) => {
                // 处理成功会执行
                this.postImgToUploadImg(rst.base64, name);

            }).catch((err) => {
                // 处理失败会执行
                Toast.info('上传失败!', 2);
            }).always(() => {
                // 不管是成功失败，都会执行
                this.refs[name].value = ''; //清空文件上传 避免上传同一张照片或是拍照时出现的图片无法展示的bug
            });
        } catch (e) {
            Toast.info("图片上传失败", 2)
            this.setState({
                [name + 'Loading']: false //隐藏加载中
            })
        }

    }

    /**
	 * 上传图片到服务器
	 * @param base64 base64图片
	 */
    postImgToUploadImg(base64, name) {
        //上传图片到服务器
        let uploadimgData = {
            content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
            format: 'jpg',
            channel: 'app',
            thumbnail: '100x100' //缩略图尺寸
            // watermark: true //加水印
        }
        UploadService.uploadImg(uploadimgData).then(data => {
            if (data.code === 0) {
                this.setState({
                    postData: Object.assign({}, this.state.postData, {
                        [name]: data.data.url
                    })
                })
            } else {
                if (data.msg) {
                    Toast.info(data.msg, 2);
                }
            }
        }, () => {
            Toast.info("系统繁忙，请稍后再试");
        }).then(() => {
            this.setState({
                [name + 'Loading']: false //隐藏加载中
            })
        })
    }

    showInputs() {
        try {
            let { location } = this.props;
            let query = { ...location.query }; // 传递过来需要补充的资料
            let inputs = []; // 输入框或图片上传
            const keyArr = ["CarOwnerLen", "CarCodeLen", "CarDriveLen", "XingShiZhengHaoLen", "DrivingLicense", "DrivingSecondLicense",
                "JashiZhengHaoLen", "DanganBianHaoLen", "TiaoXingMaLen", "MajorViolation", "MajorSecondViolation", "FilePhoneLen",
                "CheliangZhengShuLen", "OwnerCardLen", 'CarOwnerPhoneLen', "VerifyCodeLen"]; //输入框的排序
            keyArr.map(key => {
                if (query[key]) {
                    // 不要补充的资料中 存在当前key
                    inputs.push(this.getInput(key))
                    delete query[key]; //删除当前key
                }
            });
            // 遍历落网之鱼
            for (let name in query) {
                inputs.push(this.getInput(name))
            }
            postDataInit = false

            return inputs
        } catch (error) {
            this.toUrl('/') //非法来源时，直接跳转到首页
        }

    }

    /**
     * 获取输入框的名称
     * @param {*string} name 
     */
    getInputName(name) {
        let names = {
            OwnerCardLen: '车主身份证号码', //车主身份证号码
            CarDriveLen: '发动机号', //发动机号
            CarCodeLen: '车身架号', //车身架号
            CheliangZhengShuLen: '车辆登记证书号', //车辆证书(车辆登记证书号)
            XingShiZhengHaoLen: '行驶证档案编号', //行驶证档案编号
            TiaoXingMaLen: '行驶证条形码', //条形码(行驶证)
            FilePhoneLen: '驾驶证绑定手机', //注册电话（注册车辆的电话号码）
            DrivingLicense: '行驶证正页照片', //行驶证正页照片
            DrivingSecondLicense: '行驶证副页照片', //行驶证副页照片
            CarOwnerLen: '车主姓名', //车主姓名
            CarOwnerPhoneLen: '用户联系电话', //车主电话（驾照办理时留的手机号）
            MajorViolation: '驾驶证正页照片', //驾驶证正页照片
            MajorSecondViolation: '驾驶证副页照片', //驾驶证副页照片
            JashiZhengHaoLen: '驾驶证号', //驾驶证号
            DanganBianHaoLen: '驾驶证档案编号', //驾驶证档案编号
            VerifyCodeLen: '车管所手机验证码', //车管所手机验证码
        }
        return names[name] || name //返回对应的名称
    }

    /**
     * 获取输入框
     * @param {*string} name
     */
    getInput(name) {
        let { carId } = this.props.params
        let carInfo = this.props.cars.data[carId]

        let { query } = this.props.location
        let placeholder = '', type = 'url', maxLength = query[name];
        if (!query[name]) {
            return ''; //不需要输入
        } else if (query[name] > 0 && query[name] != '99') {
            placeholder = this.getInputName(name) + '后' + query[name] + '位数'
        } else {
            placeholder = '请输入' + this.getInputName(name)
            if (['CarDriveLen', 'CarCodeLen'].indexOf(name) !== -1) {
                placeholder = '请输入完整' + this.getInputName(name)
            }
        }
        if (['CarOwnerPhoneLen', 'FilePhoneLen'].indexOf(type) !== -1) {
            type = 'number'
        }
        let promptIcon = ['CarCodeLen', 'JashiZhengHaoLen', 'DanganBianHaoLen', 'TiaoXingMaLen'].indexOf(name) !== -1
            ? <i className='icon-prompt' onClick={() => this.showDeomImg(name)}></i>
            : '' //demo示例小图标

        //初始化postData的默认值
        if (postDataInit) {
            setTimeout(() => this.setState({
                postData: Object.assign({}, this.state.postData, {
                    [name]: carInfo[name]
                })
            }), 10)
        }

        if (['DrivingLicense', 'DrivingSecondLicense', 'MajorViolation', 'MajorSecondViolation'].indexOf(name) !== -1) {
            //图片
            let imgSrc = this.state.postData[name] ? this.state.postData[name].substr(0, this.state.postData[name].length - 4) + '_thumbnail.jpg' : "./images/icon-photo.png"
            return (
                <div className={'wz_item ' + Style.photoInput} key={name + 'input'} style={{ height: '1.4rem', lineHeight: '1.4rem' }}>
                    <span className="label">{this.getInputName(name)}{promptIcon}</span>
                    <img src={imgSrc} alt="上传图片" />
                    <div className={this.state[name + 'Loading'] ? Style.loadingBox : 'hide'}><Icon type='loading' /></div>
                    <input
                        ref={name}
                        className="txt"
                        type="file"
                        accept="image/*"
                        name={name}
                        onChange={(e) => this.uploadImg(e.target, name)}
                        style={{ opacity: '0' }}
                    />
                </div>
            )
        } else {
            //输入框
            return (
                <div className="wz_item" key={name + 'input'}>
                    <span className="label">{this.getInputName(name)}{promptIcon}</span>
                    <input
                        ref={name}
                        className="txt"
                        placeholder={placeholder}
                        name={name}
                        onChange={(e) => this.handleInput(e.target, name)}
                        type={type}
                        maxLength={maxLength}
                        defaultValue={carInfo[name] && maxLength > 0 ? carInfo[name].substr(Math.abs(maxLength) * -1) : carInfo[name]}
                    />
                </div>
            )
        }
    }

    //输入框内容发生改变
    handleInput(input, name) {
        sessionStorage.setItem('showConfirm', 1);

        let { postData } = this.state
        let setTime = false; //是否延迟

        //限制手机号码的输入和格式
        if (['FilePhoneLen', 'CarOwnerPhoneLen'].indexOf(name) !== -1) {
            input.value = input.value.replace(/[^0-9]*/g, '').substr(0, 11);//最大允许输入的长度
        }

        //只能输入字母和数字
        if (['OwnerCardLen', 'JashiZhengHaoLen', 'DanganBianHaoLen'].indexOf(name) !== -1) {
            input.value = input.value.replace(/[^A-Za-z0-9]*/g, '');
            setTime = true;
        }

        //转为大写字母
        if (['CarDriveLen', 'CarCodeLen', 'JashiZhengHaoLen'].indexOf(name) !== -1) {
            input.value = input.value.toLocaleUpperCase();
            setTime = true;
        }

        //更新输入框的内容
        if (setTime) {
            setTimeout(() => {
                this.setState({
                    postData: Object.assign({}, postData, {
                        [name]: input.value
                    })
                })
            }, 0)
        } else {
            this.setState({
                postData: Object.assign({}, postData, {
                    [name]: input.value
                })
            })
        }

    }

    /**
     * 检查输入是否合法 合法则返回true
     */
    checkInput() {
        let { query } = this.props.location
        let check = true;
        for (let name in query) {
            if (['DrivingLicense', 'DrivingSecondLicense', 'MajorViolation', 'MajorSecondViolation'].indexOf(name) !== -1) {
                //图片
                if (!this.state.postData[name]) {
                    Toast.info('请上传' + this.getInputName(name), 1);
                    check = false;
                    break;
                }
            } else {
                //不能为空
                if (this.refs[name].value == '' && query[name] > 0) {
                    Toast.info('请输入' + this.getInputName(name), 1);
                    check = false;
                    break;
                }

                //手机号码需要11位数
                if (['FilePhoneLen', 'CarOwnerPhoneLen'].indexOf(name) !== -1 && this.refs[name].value.length !== 11) {
                    Toast.info('请输入' + this.getInputName(name) + '（11位数）', 1);
                    check = false;
                    break;
                }

                //需要输入指定的位数
                if (query[name] > 0 && query[name] != '99') {
                    if (this.refs[name].value.length != query[name]) {
                        Toast.info('请输入' + this.getInputName(name) + '后' + query[name] + '位数', 1);
                        check = false;
                        break;
                    }
                }

                //输入的内容长度不能小于7位
                if (query[name] == '99' && this.refs[name].value.length < 7) {
                    if (['CarOwnerLen', 'DrivingLicense', 'DrivingSecondLicense', 'MajorViolation', 'MajorSecondViolation'].indexOf(name) === -1) {
                        Toast.info('请输入完整' + this.getInputName(name)); //除了车主姓名，还有图片 之外  其他的都会做>=7位的位数判断
                        check = false;
                        break;
                    }
                }
            }
        }
        return check;
    }

    /**
     * 显示demo图片
     */
    showDeomImg(name) {
        this.setState({
            demoImg: './images/demo-' + name[0].toLocaleLowerCase() + name.substr(1) + '.png'
        })
    }

    /**
     * 隐藏demo图片
     */
    hideDemoBox() {
        this.setState({
            demoImg: ''
        })
    }

    /**
     * 保存并办理 or 保存并后退
     */
    send() {
        if (this.checkInput()) {
            let { postData } = this.state
            let { carId, violationIds } = this.props.params

            postData.carId = carId
            Toast.loading('', 0)
            carService.updateCarRequirement(postData).then(result => {
                Toast.hide();
                if (result.code == '1000') {
                    //这里是应该触发对应的action 并且跳转到办理页面
                    try {
                        this.props.dispatch({ type: 'UPDATE_CAR', data: postData }) //更新车辆信息
                        this.props.dispatch(deleteProxyRuleKey(postData)) //删除所需补充的资料
                    } catch (error) {
                        //不知道这里为什么会出错
                    }
                    sessionStorage.removeItem('showConfirm'); //移除弹窗
                    if (violationIds) {
                        //保存并办理
                        window.location.replace(common.getRootUrl() + `orderConfirm/${violationIds}`) //后退不能后退到当前页面
                    } else {
                        //保存并后退
                        window.history.back();
                    }

                } else {
                    Toast.info(result.msg || ChMessage.FETCH_FAILED)
                }
            }, error => {
                Toast.hide();
                Toast.info(ChMessage.FETCH_FAILED)
            })
        }
    }

    /**
     * 保存确认提示框
     */
    sendConfirm() {
        if (this.checkInput()) {
            const { carId } = this.props.params
            const { carNumber } = this.props.cars.data[carId] ? this.props.cars.data[carId] : { carNumber: '' };
            const obj = {
                show: true, //显示对话框
                content: '请务必输入车辆' + carNumber.substr(0, 2) + ' ' + carNumber.substr(2) + ' 车主本人驾照信息，否则业务可能无法办理，客服将进行退单处理', //内容
                leftBtn: {
                    name: '暂不办理', //左按钮文字
                    onClick: () => false  //点击左按钮
                },
                rightBtn: {
                    name: '确认办理',//右按钮文字
                    onClick: () => this.send() //点击右按钮
                },
                onClose: () => false //点击关闭按钮
            }
            Modal.confirm(obj);
        }
    }

    render() {
        const { cars } = this.props
        const { query } = this.props.location
        const { carId, violationIds } = this.props.params
        const { demoImg, showSendConfirm } = this.state
        const carInfo = this.props.cars.data[carId] ? this.props.cars.data[carId] : { carNumber: '' }

        return (
            <div className="box">
                <div className={demoImg ? Style.demoBox : 'hide'} onClick={() => this.hideDemoBox()}><img src={demoImg} /></div>
                <div className="wz_whiteSpace_30"></div>
                <div className="wz_headMsg">
                    请补充{carInfo.carNumber.substr(0, 2) + ' ' + carInfo.carNumber.substr(2)}车辆违章代办所需的<span style={{ color: '#ff9a0a' }}>车主本人</span>资料
                </div>
                <div className="wz_list mb20">
                    {this.showInputs()}
                </div>
                <div className='plr30 ptb30'>
                    {violationIds
                        ? <div className='btn' onClick={() => showSendConfirm ? this.sendConfirm() : this.send()}>保存并办理</div>
                        : <div className='btn' onClick={() => this.send()}>保存</div>
                    }

                </div>
            </div>
        );
    }
}

//使用context
UpdateCarRequirement.contextTypes = {
    router: React.PropTypes.object.isRequired
}

UpdateCarRequirement.propTypes = {

};

const mapStateToProps = state => ({
    cars: state.cars
})

export default connect(
    mapStateToProps
)(UpdateCarRequirement);