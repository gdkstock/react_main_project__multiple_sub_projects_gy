import baseJsApi from 'app/utils/jsApi';

class JsApi extends baseJsApi {
    constructor() {
        super();

    }

    ready(callback) {
        if (window.jsApiIsLoad) {
            callback();
        } else {
            document.addEventListener('jsApiIsReady', callback);
        }
    }

    //向app广播更新车辆信息
    appUpdateCarMsg() {
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "sendBroadcast",
                "data": {
                    "eventId": "H5_updateCar",
                    "msgObj": { "needUpdate": "true" }
                }
            }, function (data) { });
        } catch (e) {

        }
    }

    //向app广播更新违章信息
    appUpdateViolation(carId) {
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "sendBroadcast",
                "data": {
                    "eventId": "H5_updateViolation",
                    "msgObj": { "carId": carId }
                }
            }, function (data) { });
        } catch (e) {

        }
    }

    /**
     * 调用jsdk支付
     *
    */
    pay(data, callback) {
        let { orderAmt, payType } = data;
        payType = orderAmt * 1 == 0 ? "17" : payType;
        data = {
            ...data,
            payType
        }
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "payOrder",
                "data": data,
            }, function (data) {
                if (callback) {
                    callback(data)
                }
            })
        } catch (e) {
            if (callback) {
                callback(e)
            }
        }
    }

    /**
     * 关闭webview
    */

    closeAppView() {
        if (window.jsApiIsLoad) {
            this._close()
        } else {
            document.addEventListener('jsApiIsReady', () => this._close());
        }
    }

    _close() {
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "close",
            }, function (data) {

            });
        } catch (e) {
        }
    }

    getSymbol(callback) {
        try {
            this.ready(() => {
                window.cx580.jsApi.call({
                    "commandId": "",
                    "command": "getSymbol",
                    "data": {
                        "userid": "", //2.0版本
                        "deviceId": "",
                        "lng": "",
                        "lat": "",
                        "version": "",
                        "channel": "",
                        "headImage": "",
                        "phone": "",
                        "nickName": "",
                        "city": "",
                        "cityCode": "",
                        "accountId": "",
                        "token": "",
                        "productId": "",
                        "sessionId": "",
                        "statusBarHeight": "",
                    }
                }, res => {
                    const { accountId, token, deviceId, userid, version, productId,
                        channel, city, cityCode, phone, sessionId, lng, lat
                    } = res.data;

                    // 保存数据到session中
                    sessionStorage.setItem('userId', accountId);
                    sessionStorage.setItem('token', token);
                    sessionStorage.setItem('deviceId', deviceId || userid);
                    sessionStorage.setItem('appVersion', version);
                    sessionStorage.setItem('productId', productId);
                    sessionStorage.setItem('channel', channel);
                    sessionStorage.setItem('city', city);
                    sessionStorage.setItem('cityCode', cityCode);
                    sessionStorage.setItem('sessionId', sessionId);
                    sessionStorage.setItem('lng', lng);
                    sessionStorage.setItem('lat', lat);
                    callback(res);
                });
            });
        } catch (e) {
            callback(e);
        }
    }
}

// 实例化后再导出
export default new JsApi()