/**
 * 车辆相关接口
 */

import apiHelper from './apiHelper';

class CarService {

    /**
     * 更新车辆的补充资料
     * @param {*object} data
     */
    updateCarRequirement(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}car/updateCarRequirement`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取车辆列表
     * @param {*object} data
     */
    carList(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/violations`,
          data: {
            method: 'post',
            body: data
          }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取车辆列表(不包含违章)
     * @param {*object} data
     */
    carListOnly(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/list`,
          data: {
            method: 'post',
            body: data
          }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取查询违章条件列表
     * @param {*object} data
     */
    carCondition(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/getCarConditionList`,
          data: {
            method: 'get',
            body: data
          }
        };  
        return apiHelper.fetch(requestParam);
    }

    /**
     * 添加车辆
     * @param {*object} data
     */
    addCar(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/save`,
          data: {
            method: 'post',
            body: data
          }
        };   
        return apiHelper.fetch(requestParam);
    }

    /**
     * 删除车辆
     * @param {*object} data
     */
    deleteCar(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/delete`,
          data: {
            method: 'post',
            body: data
          }
        };
        
        return apiHelper.fetch(requestParam);
    }

    /**
     * 修改车辆
     * @param {*object} data
     */
    updateCar(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/update`,
          data: {
            method: 'post',
            body: data
          }
        }; 
        return apiHelper.fetch(requestParam);
    }

    /**
     * 查询车辆基础信息
     * @param {*object} data
     */
    carInfo(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/view`,
          data: {
            method: 'post',
            body: data
          }
        };
        
        return apiHelper.fetch(requestParam);
    }

    /**
     * 查询车辆违章概要信息
     * @param {*object} data
     */
    violationBrief(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/getViolation`,
          data: {
            method: 'post',
            body: data
          }
        };
        
        return apiHelper.fetch(requestParam);
    }

    /**
     * 查询车辆年检概要信息
     * @param {*object} data
     */
    inspectionBrief(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/inspection`,
          data: {
            method: 'post',
            body: data
          }
        };
        
        return apiHelper.fetch(requestParam);
    }

    /**
     * 查询车辆品牌信息
     * @param {*object} data
     */
    carBrand(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/brandList`,
          data: {
            method: 'get',
            body: data
          }
        };
        
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取车辆年检日期列表
     * @param {*object} data
     */
    checkDateList(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/inspection/checkList`,
          data: {
            method: 'post',
            body: data
          }
        };
        
        return apiHelper.fetch(requestParam);
    }

    /**
     * 查询车辆的补充资料
     * @param {*object} data
     * {
     * carId:'', //车辆ID
     * backendIds:'', //B端接口返回的违章id，下单时使用，多个时，使用英文分号分隔“;”
     * }
     */
    getRequirement(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/getRequirement`,
          data: {
            method: 'post',
            body: data
          }
        };
        
        return apiHelper.fetch(requestParam);
    }

    /**
     * 根据车牌号码获取相关信息
     * @param {*object} data
     * {
     * carNumber:''//车牌号码
     * }
     */
    information(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}car/information`,
          data: {
            method: 'post',
            body: data
          }
        };   
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取文案广告信息
     * @param {*object} data
     */
    getAd(data) {
        let requestParam = {
          url: `${apiHelper.baseApiUrl}ad/list`,
          data: {
            method: 'get',
            body: data
          }
        };   
        return apiHelper.fetch(requestParam);
    }

}

export default new CarService()