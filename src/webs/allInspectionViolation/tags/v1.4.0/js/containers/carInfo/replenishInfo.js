/*
* 补充资料
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn,Confirm  , Slogan} from '../../components'
import { Switch } from 'antd-mobile';
import { TxtGroup ,DateGroup ,BrandGroup} from '../../components/Form'
import styles from './editCar.scss'
import * as carListActions from '../../actions/carListActions'
import { Toast } from 'antd-mobile'
import moment from 'moment'
import common from '../../utils/common'
import apiHelper from '../../services/apiHelper';

class EditCar extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态

		this.state = this.getNewState({},this.props);
	}

	//获取新状态
	getNewState(state,props){
		//定义相关变量
		let carId = props.params.id || "",
			isCurrentCarExist = props.cars.data[carId]?true:false,//判断当前车辆信息是否存在
			condition={},
			carNumber = '',
			carCodePlaceholder = "",
			engineNumberPlaceholder = "",
			userTelephone = "",
			carCode = '',
			engineNumber = '',
			isSubscribe = true,
			isProvincesExist = props.provinces.length>0?true:false,
			warningMsg = "",
			canTreat = true ;//标识能否办理

		if(isCurrentCarExist){
			let currentCarData = props.cars.data[carId];

			carNumber = currentCarData.carNumber || "";
			carCode = currentCarData.carCode || "";
			engineNumber = currentCarData.engineNumber || "";
			isSubscribe = currentCarData.isSubscribe;
			userTelephone = currentCarData.userTelephone || "";

			//获取当前车牌前缀
			let carNumberPrefix = carNumber.slice(0,2);

			//判断是否有错
			let msg = this.props.location.query.msg
			if(msg){
				msg = this.props.location.query.msg.trim().toUpperCase()
				if(msg !="NULL"){
					canTreat = false;
					warningMsg = msg
				}
			}

			if(isProvincesExist){
				condition = this.getCarCondition(carNumberPrefix,props)
				//不能办理违章
				if(!condition){
					condition = {
						carCodeLen:6,
						carEngineLen:6
					}
					canTreat = false
					warningMsg = "暂不支持该地区违章查询服务，带来不便，敬请谅解！"
				}
				let {carCodeLen, carEngineLen} = condition

				carCodePlaceholder = this.getPlaceholder("车身架号",carCodeLen);
				engineNumberPlaceholder = this.getPlaceholder("发动机号",carEngineLen);
			};
		}
		let nextState = {
			carId, //车辆id
			carNumber, // 车牌
			isSubscribe,//是否开启消息通知
			userTelephone,//用户手机号码
			condition, //车辆查询条件
			CarCode:{
				groupId:"carCode" ,//唯一编码
				label:'车身架号',//标签文本
				placeholder:carCodePlaceholder ,//占位符文本
				handleChange:(e)=>{this.preValiDate(e)} ,//输入框文本改变操作
				type:"text", //输入框type
				value:carCode,//输入框值
			},
			EngineNumber:{
				groupId:"engineNumber" ,//唯一编码
				label:'发动机号',//标签文本
				placeholder:engineNumberPlaceholder ,//占位符文本
				handleChange:(e)=>{this.preValiDate(e)} ,//输入框文本改变操作
				type:"text", //输入框type
				value:engineNumber,//输入框值
			},
			isProvincesExist, //是否存在省份信息
			isCurrentCarExist, //是否存在当前车辆信息
			canTreat,
			warningMsg,
		}
		return {...state,...nextState}
	}

	componentWillMount(){
		//设置标题
		common.setViewTitle('补充资料');
		let id = this.props.params.id
		//请求车辆查询约束条件
		if(this.props.provinces.length==0){
			this.props.actions.queryCarCondition()
		}
		//请求车辆信息
		this.props.actions.queryCarInfo({carId:id})

		// var detectBack = {

	 //        initialize: function() {
	 //            //监听hashchange事件
	 //            window.addEventListener('hashchange', function() {

	 //                //为当前导航页附加一个tag
	 //                //this.history.replaceState('hasHash', '', '');

	 //            }, false);

	 //            window.addEventListener('popstate', function(e) {
	 //                if (e.state) {
	 //                    //侦测是用户触发的后退操作, dosomething
	 //                    //这里刷新当前url
	 //                    this.location.reload();

	 //                }
	 //            }, false);
	 //        }
	 //    }

	 //    detectBack.initialize(); 
	}

	componentWillReceiveProps(nextProps){
		let nextState = this.getNewState(this.state,nextProps)
		this.setState(nextState)
	}

	componentDidMount(){
		//进入补充资料页面埋点
		common.sendCxytj({
            eventId:"Violation_EnterAddCarData"
        })
	}

	componentWillUnmount() {
		//离开补充资料页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

	//获取车牌对应的违章查询条件
	getCarCondition(carNumberPrefix,props){
		let provincePrefix = carNumberPrefix[0] //获取车牌前缀第一位

		let provinces = props.provinces;
		for (let i = 0; i < provinces.length; i++) {
			if(provinces[i].provincePrefix==provincePrefix){
				let cities = provinces[i].cities;
				for (let j = 0; j < cities.length; j++) {
					let target ="";
					if(provincePrefix!="京"&&provincePrefix!="津"&&provincePrefix!="沪"&&provincePrefix!="渝"){
						target = carNumberPrefix
					}else{
						target = provincePrefix
					}

					if(cities[j].carNumberPrefix == target){
						return {
							carCodeLen:cities[j].carCodeLen,
							carEngineLen:cities[j].carEngineLen
						}
					}
				}
				break;
			}
		}
	}

	//除车牌号外其他输入框改变时判断
	preValiDate(e){
		let target = e.target,
			name = e.target.name, 
			len,
			value,
			currValue = target.value;
		switch(name){
			case "carCode":
				len = this.state.condition.carCodeLen;
				value = currValue.trim()//this.cutInput(currValue,len);
				this.setState({
					CarCode:{...this.state.CarCode,value:value.toUpperCase()}
				})
				break;
			case "engineNumber":
				len = this.state.condition.carEngineLen;
				value = currValue.trim()//this.cutInput(currValue,len)
				this.setState({
					EngineNumber:{...this.state.EngineNumber,value:value.toUpperCase()}
				})
				break;
			case "userTelephone":
				len = 11;
				value = this.cutInput(currValue,len)
				value = value.replace(/[^\d]/,'')
				this.setState({
					userTelephone:value
				})
				break;
		}
	}

	//提交修改数据
	saveCarInfo(){
		if(this.valiDate()){
			let param = {
				carId:this.state.carId,
				carNumber:this.state.carNumber,
				isSubscribe:this.state.isSubscribe,		
			};
			this.state.condition.carCodeLen>0 && (param.carCode = this.state.CarCode.value);
			this.state.condition.carEngineLen>0 && (param.engineNumber = this.state.EngineNumber.value);
			this.state.isSubscribe == 1 && (param.userTelephone = this.state.userTelephone);

			this.props.actions.queryUpdateCar(param);
		};
	}

	//截取输入框输入文本长度
	cutInput(value ,len ){
		let output = value;
		if(value.length>len){
			output = value.slice(0,len)
		}
		return output
	}

	//获取输入框placeholder
	getPlaceholder(txt,len){
		switch(len){
			case 99:
				return "请输入完整的"+txt
			default:
				return txt+"后"+len+"位数"
		}
	}

	//switch选择改变
	checkboxChange(checked){
		this.setState({
			isSubscribe:checked?1:0
		})
	}

	//点击保存车辆时前验证信息
	valiDate(){
		let hasCarCode = this.state.condition.carCodeLen>0 ? true :false;
		let hasEngineNumber = this.state.condition.carEngineLen>0? true : false;

		//验证车架号
		if(hasCarCode){
			let carCode = this.state.CarCode.value;
			let len = this.state.condition.carCodeLen
			if(!this.valiCarCode(carCode,len)){
				return false
			}
		}

		//验证发动机号
		if(hasEngineNumber){
			let engineNumber = this.state.EngineNumber.value;
			let len = this.state.condition.carEngineLen
			if(!this.valiEngineNumber(engineNumber,len)){
				return false
			}
		}

		//验证手机号码
		if(this.state.isSubscribe == 1){
			let phone = this.state.userTelephone;
			if(!this.valiPhone(phone)){
				return false
			}
		}

		return true;
	}

	//验证车架号信息
	valiCarCode(carCode,len){
		if(len!=99){
			if(carCode.length<len){
				Toast.info("请输入正确的车身架号",1)
				return false
			}else{
				return true
			}
		}else{
			if(carCode.length>0){
				return true
			}else {
				Toast.info("请输入正确的车身架号",1)
				return false
			}
		}
	}

	//验证发动机号信息
	valiEngineNumber(engineNumber,len){
		if(len!=99){
			if(engineNumber.length<len){
				Toast.info("请输入正确的发动机号",1)
				return false
			}else{
				return true
			}
		}else{
			if(engineNumber.length>0){
				return true
			}else {
				Toast.info("请输入正确的发动机号",1)
				return false
			}
		}
	}

	//验证手机信息
	valiPhone(phone){
		if(phone.length!=11){
			Toast.info("请输入正确的手机号",1)
			return false
		}else{
			return true
		}
	}

	//点击删除按钮
	deleteBtnClick(){
		this.setState({
			confirm:{
			    show: true, //显示对话框
			    content: '确定要删除此车辆吗?', //内容
			    leftBtn: {
			        name: '删除', //左按钮文字
			        onClick: () => this.confirmOk()  //点击左按钮
			    },
			    rightBtn: {
			        name: '取消',//右按钮文字
			        onClick: () => this.confirmCancel() //点击右按钮
			    },
			    onClose: () => this.confirmClose() //点击关闭按钮
			}
		})
	}

	//confirm确认事件
	confirmOk(){
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
		this.deleteCar()
	}

	//confirm取消事件
	confirmCancel(){
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
	}

	//confirm关闭事件
	confirmClose(){
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
	}

	//删除车辆
	deleteCar(){
		this.props.actions.queryDeleteCar({carId:this.state.carId})
	}

	render(){
		let condition = this.state.condition
		let sloganClass =""
		if(condition.carCodeLen>0 && condition.carEngineLen>0){
			sloganClass = styles.whiteSpace_268
		}else if(condition.carCodeLen==0 && condition.carEngineLen==0){
			sloganClass = styles.whiteSpace_468
		}else{
			sloganClass = styles.whiteSpace_368
		}

		if(!this.state.canTreat){
			sloganClass = styles.whiteSpace_196
		}

		let warningMsg = this.props.location.query.msg

		return (
			<div>
				{!this.state.canTreat && <div className={styles.warningMsg}>{this.state.warningMsg}</div>}
				<div className="wz_whiteSpace_30"></div>
				<div className="wz_headMsg">
					请输入{this.state.carNumber.slice(0,2)+" "+this.state.carNumber.slice(2)}车辆的相关信息
					<div className={styles.deleteBtn} onClick={()=>this.deleteBtnClick()}><i className={styles.icon}></i>删除</div>
				</div>
				<div className="wz_list">
					{this.state.condition.carCodeLen>0 && <TxtGroup {...this.state.CarCode}/>}
					{this.state.condition.carEngineLen>0 && <TxtGroup {...this.state.EngineNumber}/>}
				</div>
				<div className="wz_whiteSpace_28"></div>
				<div className="wz_list">
					<div className="wz_item">
						<span className="label">新违章消息通知及年检提醒服务</span>
						<span className={styles.switch}>
							<Switch checked={this.state.isSubscribe} onChange={(checked)=>this.checkboxChange(checked)}/>
						</span>
					</div>
					<div className="wz_item">
						<span className={this.state.isSubscribe?"label":"label "+styles.phoneDisabled}>手机号码</span>
						{this.state.isSubscribe == 1 && 
							<input 
								className="txt" 
								placeholder="手机号码" 
								name="userTelephone"
								value={this.state.userTelephone}
								onChange={(e)=>this.preValiDate(e)}
								type="tel"
							/>
						}
					</div>
				</div>
				<div className="wz_whiteSpace_50"></div>
				<Btn text="保存" handleClick={()=>this.saveCarInfo()} disabled={!this.state.canTreat}/>
				<div className="wz_whiteSpace_30"></div>
				<div className={styles.agreeMsg}>
					点击保存并查询，即表示您同意 <a href={apiHelper.baseApiUrl+"agree/AgreeMent.html"}>用户授权协议</a>
				</div>
				<div className={sloganClass}></div>
				<Slogan />
				<Confirm {...this.state.confirm}/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cars: state.cars,
	provinces:state.provinces
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditCar)


