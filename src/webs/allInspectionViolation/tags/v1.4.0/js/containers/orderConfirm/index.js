/**
 * 订单确认
 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, Toast } from 'antd-mobile'
import { normalize, schema } from 'normalizr'; //范式化库

//组件
import WAlert from '../../components/WAlert'
import PayType from '../../components/common/payType'

//action
import * as couponActions from '../../actions/couponActions'

//server
import orderService from '../../services/orderService'

//style
import Style from './index.scss'

//常用工具类
import common from '../../utils/common'
import jsApi from '../../utils/jsApi'
import { ChMessage } from '../../utils/message.config'

//配置
import config from '../../config';

// 为了配置钱包测试，在测试环境全部使用收银台的支付方式。（搜索“ && config.production”即可看到改动的地方），后端也做了对应的修改，切记！切记！
class OrderConfirm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            carId: '',
            carNumber: '',
            isUrgent: false, //加急
            buyVipCard: false, //购买会员
            coupon: {},
            couponId: '', //使用的优惠券ID
            autoSelectCouponId: true, //自动选择优惠券
            showViolationBox: false, //显示违章列表
            orderConfirmDatas: sessionStorage.getItem('orderConfirmDatas') ? JSON.parse(sessionStorage.getItem('orderConfirmDatas')) : {}, //订单试算的结果 用于保存历史结果 减少重复请求服务器的次数
            firstGetData: true, // 首次进入
            showVipAlert: false, //显示VIP弹窗说明
            showUrgentAlert: false, //显示加急弹窗说明
            showPayType: false,//显示支付方式选择
            curPayType: localStorage.getItem("payType") || "", //支付方式
        }
    }

    componentWillMount() {
        common.setViewTitle('订单确认');
    }

    componentDidMount() {
        if (sessionStorage.getItem('orderConfirmPage')) {
            //跳转前的数据
            let orderConfirmPage = JSON.parse(sessionStorage.getItem('orderConfirmPage'))
            this.setState(Object.assign({}, orderConfirmPage.state))
            document.body.scrollTop = orderConfirmPage.scrollTop
            sessionStorage.removeItem('orderConfirmPage')
        }
        if (sessionStorage.getItem('couponId') || sessionStorage.getItem('couponId') === '') {
            this.setState({
                couponId: sessionStorage.getItem('couponId')
            })
            sessionStorage.removeItem('couponId')
        }

        setTimeout(() => this.orderConfirm(), 10) //异步请求，确保state已经更新了

        common.sendCxytj({
            eventId: 'Violation_EnterOrderConfirm'
        })
    }

    componentWillUnmount() {
        sessionStorage.setItem("prevPageInfo", document.title)

        this.setState = () => false; // 避免组件被卸载时出错
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    /**
     * 订单确认 || 订单试算
     */
    orderConfirm() {
        let violationIds = this.props.params.violationIds || localStorage.getItem("orderConfirm_violationIds");
        let { violations, cars } = this.props
        let { isUrgent, buyVipCard, couponId, orderConfirmDatas, autoSelectCouponId, firstGetData } = this.state
        let violationIdArr = violationIds.split(',');
        let { carId } = violations.data[violationIdArr[0]]; //直接拿第一条违章的carId

        let postData = {
            carId: carId,
            carNumber: cars.data[carId].carNumber,
            urgentFlag: isUrgent ? '1' : '2', //订单加急标记1：加急2：不加急
            buyVipCard: buyVipCard ? '1' : '2', //订单是否购买会员，1：购买 2：不购买
            couponId: buyVipCard ? '' : couponId, //优惠券ID
            violationInfos: [] //违章信息
        }

        let violationInfo = {}
        violationIdArr.map(violationId => {
            let { backendId, locationId, violationCode, realDegree, fine, cooperPoundage } = violations.data[violationId];
            violationInfo = {
                violationId: violationId,
                backendId: backendId,
                locationId: locationId,
                violationCode: violationCode,
                realDegree: realDegree,
                fine: fine,
                cooperPoundage: cooperPoundage
            }
            postData.violationInfos.push(JSON.stringify(violationInfo))
        })
        postData.violationInfos = '[' + postData.violationInfos.toString() + ']' //转为json字符串

        let postDataStr = JSON.stringify(postData);

        //数据已经存在 并且 非首次进入（首次进入需要自动选择优惠券）
        if (orderConfirmDatas[postDataStr] && !firstGetData) {
            let result = orderConfirmDatas[postDataStr] //直接读取之前的试算数据

            this.setState(Object.assign({}, this.state, result.data, {
                //这里需要key的转换以及转为对应的数据类型
                carId: postData.carId,
                carNumber: postData.carNumber,
                buyVipCard: result.data.vip.isSwitchStatus == '1' ? true : false,
                orderConfirmDatas: Object.assign({}, orderConfirmDatas, {
                    [postDataStr]: result
                }) //保存试算数据
            }))
        } else {
            //从未试算过
            Toast.loading('', 0)
            orderService.confirm(postData).then(result => {
                Toast.hide();
                if (result.code == '1000') {
                    //添加优惠券到全局start
                    this.addCoupons(result.data.coupon);

                    //更新支付方式
                    this.updatePayType(result.data)

                    //更新状态
                    this.setState(Object.assign({}, this.state, result.data, {
                        //这里需要key的转换以及转为对应的数据类型
                        carId: postData.carId,
                        carNumber: postData.carNumber,
                        buyVipCard: result.data.vip.isSwitchStatus == '1' ? true : false,
                        orderConfirmDatas: Object.assign({}, orderConfirmDatas, {
                            [postDataStr]: result
                        }), //保存试算数据
                        firstGetData: false, // 非首次进入
                    }), () => {
                        //自动选择优惠券
                        let { couponList, couponCount } = result.data.coupon;
                        if (autoSelectCouponId && couponCount > 0) {
                            let autoList = couponList.filter(coupon => coupon.choose === 1);
                            if (autoList[0]) {
                                let autoCouponId = autoList[0].couponId; //默认选中的优惠券id
                                this.autoSelectCouponId(autoCouponId);
                            }
                        }
                        sessionStorage.setItem('orderConfirmDatas', JSON.stringify(this.state.orderConfirmDatas)) //保存试算数据到临时缓存
                    })
                } else if (result.code != "4222" && result.code != "2222") {
                    if (result.msg) {
                        Toast.info(result.msg, 3, () => window.history.back()); //试算失败后 页面后退
                    }
                }
            }, error => {
                Toast.hide();
                Toast.info(ChMessage.FETCH_FAILED)
            })
        }
    }

    /**
     * 获取支付方式
    */
    updatePayType(data) {
        let appVersion = common.getAppVersion();
        let params = {
            appVersion,
            orderType: data.orderType,
            orderAmt: data.totalFee,
        }
        orderService.payTypes(params).then((result) => {
            if (result.code == "1000") {
                //获取初始化的支付方式
                let state = this.getInitPayType(result.data);
                this.setState({
                    ...state,
                    payTypes: result.data
                })
            } else {

            }
        })
    }

    /**
     * 初始化支付方式
    */
    getInitPayType(data) {
        let outPuOobj = {
            curPayType: "",
            showPayType: this.state.showPayType
        };
        let { curPayType } = this.state;
        if (data && data.payTypeList && data.payTypeList.length > 0) {
            outPuOobj.showPayType = true;
            let defaultPayType = data.defaultPayType;
            for (let i = 0; i < data.payTypeList.length; i++) {
                let item = data.payTypeList[i]
                if (item.selectable == "1") {
                    if (curPayType == item.payType) {
                        outPuOobj.curPayType = curPayType;
                        break;
                    } else if (defaultPayType == item.payType) {
                        outPuOobj.curPayType = outPuOobj.curPayType || defaultPayType;
                    } else {
                        outPuOobj.curPayType = outPuOobj.curPayType || item.payType;
                    }
                }
            }
        }
        return outPuOobj
    }

    getPayEventId(payType) {
        switch (payType + '') {
            // 银联支付
            case '1': return 'cxyapp_e_pay_unionpay';

            // 支付宝
            case '2': return 'cxyapp_e_pay_alipay';

            // 微信
            case '3': return 'cxyapp_e_pay_wxpay';

            // 钱包
            case '13': return 'cxyapp_e_pay_walletpay';

            // 交通信用卡
            case '15': return 'cxyapp_e_pay_bcmpay';

            // 威富通
            case '80': return common.getJsApiUserType() === 'weixin' ? 'cxyapp_e_pay_wftpay_wx' : 'cxyapp_e_pay_wftpay_ali';

            default: return '';
        }
    }

    /**
     * 自动选中优惠券
     * @param couponId 优惠券ID
     */
    autoSelectCouponId(couponId) {
        this.setState({
            couponId,
            autoSelectCouponId: false
        }, () => {
            this.orderConfirm();
        })
    }

    /**
     * 加急开关
     * @param checked 选择状态
     * @param canUrgent 可否加急
     */
    urgentSwitch(checked, canUrgent = true) {
        if (canUrgent == 'true') {
            //可加急
            this.setState({
                isUrgent: canUrgent ? checked : false
            }, () => {
                this.orderConfirm()
            })
        } else {
            //不可加急
            if (this.state.urgent.urgentErrorMsg) {
                Toast.info(this.state.urgent.urgentErrorMsg, 2)
            } else {
                Toast.info('此订单不可加急', 2)
            }

        }
    }

    /**
     * 会员开关
     */
    vipSwitch(checked) {
        this.setState({
            buyVipCard: checked
        }, () => this.orderConfirm())
    }

    /**
     * 跳转到优惠券页面
     */
    toCoupons() {

        common.sendCxytj({
            eventId: 'h5_e_wz_wzdetail_selectcoupon'
        })

        let { couponId } = this.state
        let { coupons } = this.props

        let couponIds = coupons.result.toString();
        this.toUrl(`/coupons/${couponIds}/${couponId}`)
        this.sessionStorage()
    }

    /**
     * 保存优惠券列表到全局state
     */
    addCoupons(coupon) {
        const couponEntity = new schema.Entity('coupon', {}, { idAttribute: 'couponId' });
        const couponArray = new schema.Array(couponEntity)
        const normalizedData = normalize(coupon.couponList, couponArray)
        if (normalizedData.result.length > 0) {
            this.props.couponActions.addCoupons({
                data: normalizedData.entities.coupon,
                result: normalizedData.result
            })
        }
    }

    /**
     * 保存页面状态以及数据
     */
    sessionStorage() {
        let data = {
            state: this.state,
            scrollTop: document.body.scrollTop
        }
        sessionStorage.setItem('orderConfirmPage', JSON.stringify(data))
    }

    /**
     * 获取扣分
     */
    getdegree(violationIds) {
        let { violations } = this.props
        let degree = 0; //实际扣分
        violationIds.split(',').map(violationId => {
            degree += violations.data[violationId].degree * 1
        })
        return degree || 0
    }

    /**
     * 支付
     */
    pay() {
        let violationIds = this.props.params.violationIds || localStorage.getItem("orderConfirm_violationIds");
        let { violations, cars } = this.props
        let { isUrgent, buyVipCard, couponId, showPayType, curPayType } = this.state
        let violationIdArr = violationIds.split(',');
        let { carId } = violations.data[violationIdArr[0]]; //直接拿第一条违章的carId

        // 提交埋点
        common.sendCxytj({
            eventId: 'Violation_PayOrder'
        })

        // 提交支付方式的埋点
        if (showPayType) {
            const payEventId = this.getPayEventId(curPayType);
            if (payEventId) {
                common.sendCxytj({
                    eventId: this.getPayEventId(curPayType)
                })
            }
        }

        let postData = {
            carId: carId,
            carNumber: cars.data[carId].carNumber,
            urgentFlag: isUrgent ? '1' : '2', //订单加急标记1：加急2：不加急
            buyVipCard: buyVipCard ? '1' : '2', //订单是否购买会员，1：购买 2：不购买
            couponId: buyVipCard ? '' : couponId, //优惠券ID
            violationInfos: violationIds, //违章信息
            version: 'v1.3', //版本标识符，支付成功后 跳转到支付成功页
        }

        Toast.loading('', 0)
        orderService.pay(postData).then(result => {
            Toast.hide();
            if (result.code == '1000' && result.data.resultFlag == '1') {
                //app渠道调用jsdk支付 正式环境才调用 测试环境使用收银台
                if (common.isCXYApp() && config.production) {
                    let data = Object.assign({}, result.data, { payType: curPayType })
                    jsApi.pay(data, (result) => {
                        if (result.data && result.data.tradeStatus && result.data.tradeStatus == "1") {
                            //缓存支付方式
                            localStorage.setItem("payType", curPayType);
                            //跳转到支付成功页面
                            this.props.router.replace(`/orderPaySuccess/${data.orderId}`)
                            //发送更新违章数据广播
                            jsApi.appUpdateViolation(carId);
                        } else {

                        }
                    })
                } else {
                    window.location.href = result.data.paySign //跳转到收银台
                }
            } else {
                Toast.info(result.msg || ChMessage.FETCH_FAILED)
            }
        }, error => {
            Toast.hide()
            Toast.info(ChMessage.FETCH_FAILED)
        })
        // this.toUrl(`/orderDetail/:orderId`)
    }

    /**
     * 显示违章概要
     */
    showViolationBox() {
        common.sendCxytj({
            eventId: 'Violation_ViewViolationSummary'
        })
        this.setState({ showViolationBox: false })
    }

    /**
     * 支付方式变化
    */
    payTypeChange(payType) {
        let { curPayType } = this.state
        if (curPayType != payType) {
            this.setState({
                curPayType: payType,
            })
        }
    }

    render() {
        let violationIds = this.props.params.violationIds || localStorage.getItem("orderConfirm_violationIds");
        let { carId, carNumber, orderTitle, isUrgent, buyVipCard, couponId, showViolationBox, drivingLicense, urgent, activity, coupon, vip, accountList, totalFee, discountAmount, showVipAlert, showUrgentAlert, showPayType, curPayType, payTypes } = this.state
        let { violations, cars, coupons } = this.props
        let degree = this.getdegree(violationIds) //扣分
        return (
            <div className='box'>
                {/*违章列表 start*/}
                <div className={showViolationBox ? Style.violationBox : 'hide'}>
                    <div className={Style.violationList}>
                        {
                            violationIds.split(',').map((item, i) =>
                                violations.data[item] ? <div key={item} className={Style.violationItem}>
                                    <div className={Style.violationTitle}>{violations.data[item].locationName + violations.data[item].location}</div>
                                    <div className={Style.violationTime}>{violations.data[item].occurTime}</div>
                                    <div className={Style.violationReason}>{violations.data[item].reason}</div>
                                    <div className={Style.violationFine}>
                                        <span>罚款{violations.data[item].fine}元</span>
                                        <span>扣{violations.data[item].degree}分</span>
                                        {violations.data[item].lateFine && <span>滞纳金{violations.data[item].lateFine}元</span>}
                                        <span>服务费{violations.data[item].poundage}元</span>
                                    </div>
                                </div> : ''
                            )
                        }
                    </div>
                    <div className={Style.closeViolation}
                        onClick={() => this.showViolationBox()}
                        style={{ backgroundImage: 'url("./images/icon-close.png")' }}></div>
                </div>
                {/*违章列表 end*/}

                {/*订单标题 start*/}
                <div className={Style.orderTitle} onClick={() => this.setState({ showViolationBox: true })}>
                    <div className='absolute-vertical-center'>
                        <span>{carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}{orderTitle}</span>
                        <p>共办理{violationIds.split(',').length}条，扣{degree}分{drivingLicense ? '，扣分驾驶证号' + drivingLicense : ''}</p>
                        {
                            carNumber.substr(0, 2) === '粤X' &&
                            <p>{isUrgent ? '加急下单支付后两小时内办结' : '预计3个工作日处理完成'}</p>
                        }
                    </div>
                    <i className={Style.iconRight}></i>
                </div>
                {/*订单标题 end*/}

                {/*会员、加急、优惠券、活动减免 start*/}
                <div className={Style.whiteBoxList}>

                    {/*会员 start*/}
                    {
                        !vip ? '' :
                            <div className={vip.showVip == '1' ? Style.vipBox : 'hide'}>
                                <div className={Style.vipTop}>
                                    <span>开通会员</span>
                                    <i className="icon-prompt" onClick={() => this.setState({ showVipAlert: true })}></i>
                                    <div className={Style.infomation}>违章立享5折，全年免违章服务费</div>
                                </div>
                                <div className={Style.vipSwitchBox}>
                                    <Switch
                                        checked={buyVipCard}
                                        onChange={(checked) => this.vipSwitch(checked)}
                                    />
                                </div>
                            </div>
                    }
                    {/*会员 end*/}

                    {//优惠券
                        !coupon ? '' :
                            !buyVipCard ? coupon.couponCount > 0
                                ? <div className={coupon.showCoupon == '1' ? Style.couponBox : 'hide'} onClick={() => this.toCoupons()}>
                                    <div className={Style.couponTitle}>
                                        <span>优惠券</span>
                                    </div>
                                    <div className={Style.couponSelect}>
                                        {
                                            couponId && coupons.data[couponId]
                                                ? <span className={Style.couponMoney}>{(coupons.data[couponId].description || coupons.data[couponId].couponAmount + '元') + coupons.data[couponId].couponTitle}</span>
                                                : <span className={Style.couponNum}>{coupon.couponCount}张可用</span>
                                        }
                                        <i className={Style.iconRight}></i>
                                    </div>
                                </div>
                                : <div className={coupon.showCoupon == '1' ? Style.couponBox : 'hide'}>
                                    <div className={Style.couponTitle}>
                                        <span>优惠券</span>
                                    </div>
                                    <div className={Style.noCoupons}>
                                        <span>暂无可用优惠券</span>
                                    </div>
                                </div>
                                :
                                <div className={coupon.showCoupon == '1' ? Style.couponBox + ' ' + Style.notSelectCoupon : 'hide'}>
                                    <div className={Style.couponTitle}>
                                        <span>优惠券</span><i>不与会员权益同时使用</i>
                                    </div>
                                    {
                                        coupon.couponCount > 0
                                            ? <div className={Style.couponSelect}>
                                                <span className={Style.couponNum}>{coupon.couponCount}张可用</span>
                                                <i className={Style.iconRight}></i>
                                            </div>
                                            : <div className={Style.noCoupons}>
                                                <span>暂无可用优惠券</span>
                                            </div>
                                    }
                                </div>
                    }
                    <div className="line"></div>

                    { //加急
                        !urgent ? '' :
                            <div className={urgent.showUrgent == '1' ? Style.urgentBox : 'hide'}>
                                <div className={Style.urgentTitle}>
                                    <div className={Style.urgentTitleText}>
                                        <span>{urgent.title}{urgent.describe ? `(${urgent.describe})` : ''}</span>
                                        <i className="icon-prompt"
                                            style={{ position: 'absolute', top: '-4px' }}
                                            onClick={() => this.setState({ showUrgentAlert: true })}></i>
                                    </div>
                                </div>
                                <div className={Style.urgentSwitchBox}>
                                    <Switch
                                        checked={isUrgent}
                                        onChange={(checked) => this.urgentSwitch(checked, urgent.canUrgent)}
                                    />
                                </div>
                            </div>
                    }

                    { //活动
                        !activity ? '' :
                            <div className={activity.showActivity == '1' ? Style.activityBox : 'hide'}>
                                <span>{activity.title || "活动减免"}</span>
                                <i>{activity.subTitle}</i>
                            </div>
                    }

                </div>
                {/*加急、活动减免、优惠券 end*/}

                {/*订单详情 start*/}
                <div className={Style.orderInfo}>
                    {
                        accountList && accountList.map((item, i) => item && item.name && item.account
                            ? <p key={'account' + i}><span>{item.name}</span><i>{item.account < 0 ? `- ¥${item.account * -1}` : `¥${item.account}`}</i></p>
                            : ''
                        )
                    }
                </div>
                {/*订单详情 end*/}

                {/*支付方式选择 start*/}
                {showPayType && config.production && <div className="h24"></div>}
                {showPayType && config.production && <PayType {...payTypes} curPayType={curPayType} payTypeChange={(payType) => this.payTypeChange(payType)} />}
                {/*支付方式选择 end*/}

                {/*合计和支付按钮 start*/}
                <div style={{ height: '1.4rem' }}></div>
                <div className={Style.footerBox}>
                    <div className={Style.footerText}>
                        <span>合计￥{totalFee}</span><i className={discountAmount > 0 ? '' : 'hide'}>(已优惠￥{discountAmount})</i>
                    </div>
                    <div className={Style.footerBtn} onClick={() => this.pay()}>去支付</div>
                </div>
                {/*合计和支付按钮 end*/}

                {/*说明弹窗 start*/}
                {showVipAlert ? <WAlert num={0} onClose={() => this.setState({ showVipAlert: false })} /> : ''}
                {showUrgentAlert ? <WAlert num={1} onClose={() => this.setState({ showUrgentAlert: false })} /> : ''}
                {/*说明弹窗 end*/}
            </div >
        );
    }
}

//使用context
OrderConfirm.contextTypes = {
    router: React.PropTypes.object.isRequired
}

OrderConfirm.propTypes = {

};

const mapStateToProps = state => ({
    cars: state.cars,
    violations: state.violations,
    coupons: state.coupons
})

const mapDispatchToProps = dispatch => ({
    couponActions: bindActionCreators(couponActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderConfirm);