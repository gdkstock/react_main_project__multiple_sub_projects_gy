
import {
	ADD_CAR_CONDITION_LIST
} from '../actions/actionsTypes'

const initState = {
	data: {},
	result: []
}

export default function provinces(state = initState, action) {
	switch (action.type) {
		case ADD_CAR_CONDITION_LIST:
			return action.data
		default:
			return state
	}
}