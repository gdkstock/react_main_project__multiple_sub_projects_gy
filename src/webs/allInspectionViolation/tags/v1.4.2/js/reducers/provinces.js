
import {
    ADD_TO_PROVINCES
} from '../actions/actionsTypes'

const initState = []

export default function provinces( state = initState, action ){
	switch(action.type){
		case ADD_TO_PROVINCES:
			return action.data
		default:
			return state
	}
}