import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//组件
import { CarViolationList } from '../../components/violation/handle';

//styles
import styles from './handleList.scss';

//actions
import * as carListActions from '../../actions/carListActions';
import * as violationActions from '../../actions/violationActions';

//通用工具类
import common from '../../utils/common';

//配置
import config from '../../config';

class HandleList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            //违章数据请求中 数组里分别存在车辆违章是否处于请求状态
            fetching: [true, true, true],

            //违章信息 {carId:{违章详情}}
            violations: {},

            //车辆无违章的提示信息 {carId:{msg:'提示信息'}}
            carMsgs: {},
        }
    }

    componentWillMount() {
        common.setViewTitle('违章办理');
        this.getData();
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    toUrl(url) {
        document.body.scrollTop = 0;
        this.props.router.push(url);
    }

    getData() {
        let _state = sessionStorage.getItem('violationHandle_state'); //优先从缓存中获取数据
        if (_state) {
            this.setState(JSON.parse(_state));
        }

        //请求数据
        this.props.carListActions.queryCarListOnly({}, res => {

            //接口请求成功
            if (res.code == 1000) {
                //如果没有车辆数据，跳转到添加车辆页面
                if (res.data.length === 0) {
                    window.location.replace(config.addCarUrl);
                } else {
                    //请求车辆违章信息
                    res.data.map((car, i) =>
                        this.props.violationActions.getViolationListAsync({
                            type: '3', //待处理
                            carId: car.carId
                        }, false, res => {
                            let { fetching, violations, carMsgs } = this.state;

                            if (res.code == 1000) {
                                let list = res.data.violationList || [];

                                //过滤代办违章
                                list = list.filter(violation => violation.violationStatus === 0);

                                //过滤不可在线办理的违章
                                violations[car.carId] = list.filter(violation => violation.canProcess);

                                //过滤不可代办的违章
                                let _list = list.filter(violation => !violation.canProcess);

                                //不存在可在线代办违章 但是 存在可线下办理违章
                                if (violations[car.carId].length === 0 && _list.length > 0) {
                                    carMsgs[car.carId] = '此车有需线下办理的违章';
                                }

                            }

                            //更新state
                            fetching[i] = false; //加载完毕
                            this.setState({
                                fetching,
                                violations
                            }, () => {
                                //保存state
                                sessionStorage.setItem('violationHandle_state', JSON.stringify(this.state))
                            })
                        })
                    )
                }
            }
        })
    }

    render() {
        let { fetching, violations, carMsgs } = this.state
        let { cars } = this.props

        let violationLen = 0;
        Object.keys(violations).map(carId => violationLen += violations[carId].length);

        return (
            <div className='box'>
                {
                    violationLen === 0 ? '' :
                        <div className={styles.remind} style={common.isAndroid() ? { paddingTop: '2px' } : {}}><p>您有{violationLen}个违章可在线办理</p></div>
                }

                {
                    cars.result.map((carId, i) =>
                        <div key={'car-item-' + i}>
                            <CarViolationList
                                msg={fetching[i] ? '加载中...' : carMsgs[carId] || ''}
                                carNumber={cars.data[carId].carNumber}
                                list={violations[carId] || []} //违章列表
                                toViolationList={() => this.toUrl(`/violationList/${carId}`)} //跳转到违章列表
                                toViolationDetail={url => this.toUrl(url)} //跳转到违章详情
                            />
                            <div className='h20'></div>
                        </div>
                    )
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cars: state.cars,
})

const mapDispatchToProps = dispatch => ({
    carListActions: bindActionCreators(carListActions, dispatch),
    violationActions: bindActionCreators(violationActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HandleList)