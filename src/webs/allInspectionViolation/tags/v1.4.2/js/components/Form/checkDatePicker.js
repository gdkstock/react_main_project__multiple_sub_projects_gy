/*
	年检日期组件
	props={
		groupId:"registerDate" ,//唯一编码
		label:'车辆注册日期',//标签文本
		placeholder:'请选择' ,//占位符文本
		type:"date" ,//日期选择器类型
		format:"", //日期格式
		handleChange:()=>{} ,//输入框文本改变操作
		value:"" //输入框值

	    dateList: [], //检验有效期列表
	    registerDate: '', //车辆注册日期
	    inspectDate: '', //检验有效期
	    getInspectDate: d => console.log("检验有效期至：", d), //获取检验有效期
	}
*/
import React, { Component} from 'react';
import { Popup } from 'antd-mobile'
import moment from 'moment';
import Style from './checkDatePicker.scss'
import { Toast } from 'antd-mobile'

class CheckDatePicker extends Component{
	constructor(props) {
        super(props);
        this.state = {
            dateList: {
                show: false, //显示
            }
        }
    }
    /**
     * 显示 检验有效期列表
     */
    showDateList() {
    	let { registerDate } = this.props
        let dpValue = registerDate ? moment(registerDate + ' +0800', 'YYYY-MM-DD Z').utcOffset(8) : '';//车辆注册时间
        registerDate = dpValue ? dpValue.format("YYYY-MM-DD") : ''

        if (!registerDate) {
            Toast.info("请先选择车辆注册日期", 1)
            return;
        }

        let dateList = this.props.dateList && this.props.dateList.constructor === Array ? this.props.dateList : [] //检验有效期列表
        let list = dateList.map((item, i) =>
            <div key={'dateList' + i} onClick={(date) => this.showDate(item)}>{item}</div>
        )
        let JSX = <div className={Style.dateList}>{list}</div>
        Popup.show(JSX, { animationType: 'slide-up' }
        );
    }

    /**
     * 显示检验有效期
     */
    showDate(date) {
        Popup.hide()

        this.props.getInspectDate(date) //返回检验有效期给父组件
    }

    render(){
    	return (
			<div className="wz_item">
		        <div onClick={()=>this.showDateList()}>
		        	<span className="label">{this.props.label}</span>
					<input 
						className="txt hasRightIcon" 
						placeholder="请选择" 
						value = {this.props.value}
						disabled
					/>
					<i className="rightIcon"></i>
				</div>
			</div>
		)
    }
	
}

export default CheckDatePicker