/**
 * 违章详情
 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Toast } from 'antd-mobile'

//组件
import { Card } from '../../components/violation'
import Comfirm from '../../components/common/Confirm'

//action
import * as violationActions from '../../actions/violationActions'

//server
import violationService from '../../services/violationService'

//style
import '../../../styles/violation.scss'
import Style from './detail.scss'

//常用工具类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config' //提示信息
import config from '../../config'

class ViolationDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            confirmInfo: {
                show: false, //显示
                content: '该车辆目前有3条可代办违章尚未处理',
                leftBtn: {
                    name: '办理全部',
                    onClick: () => this.confirmLeftBtn()
                },
                rightBtn: {
                    name: '仅办理该条',
                    onClick: () => this.confirmRightBtn()
                },
                onClose: () => this.hideConfirm()
            },
            ready: false, //已加载完毕
        }
    }

    componentWillMount() {
        common.setViewTitle('违章详情');
    }

    componentDidMount() {
        let violationInfo = this.props.violations.data[this.props.params.violationId];
        if (violationInfo && violationInfo.carId && violationInfo.violationId) {
            //存在全局state || 缓存
            this.setState({
                ready: true
            })
            this.getData(); //获取数据
        } else {
            //没有全局state也没有缓存数据
            this.fetchData(); //请求数据
        }
    }

    componentWillUnmount() {
        sessionStorage.setItem("prevPageInfo", document.title)
        this.setState = () => false; // 避免组件被卸载时出错
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    /**
     * 获取数据
     */
    getData() {
        try {

            let violationInfo = this.props.violations.data[this.props.params.violationId];
            let { carId, violationId, locationName, location, violationStatus } = violationInfo
            if (!violationInfo || !violationId) {
                this.toUrl('/')
                return;
            }
            if (violationStatus == '0') {
                //如果是未办理违章，这添加已读标识符
                this.props.violationActions.addReadViolation({
                    carId: carId,
                    violationId: violationId
                }) //已读
                //设置未读违章
                common.setUnreadList(violationId);
            }
            //获取违章地点人数
            this.props.violationActions.getLocationViolationNumberAsync({
                cityName: locationName,
                locationName: location
            }, violationId, (result) => {
                if (result.data.violationPointList && result.data.violationPointList.length > 0) {
                    let id = result.data.violationPointList[0].id
                    let location = result.data.violationPointList[0].location
                    //请求违章热点评论
                    this.props.violationActions.getComment({ id, location }, (result) => {
                        this.setState({
                            comment: result.data
                        })
                    })
                }
            })
            violationService.geocode({
                key: 'a9999d6a04e61d9ac4c459d05081874d',
                address: violationInfo.location,
                city: violationInfo.locationName,
                userId: 'map', //不泄露用户信息给高德
                token: 'map',
                authType: 'map',
                userType: 'map'
            }).then(data => {
                if (data.infocode == '10000') {
                    //请求成功
                    let longitude, latitude;
                    try {
                        let locationArr = data.geocodes[0].location.split(',')
                        longitude = locationArr[0]
                        latitude = locationArr[1]
                    } catch (error) {
                        console.error("高德地图加载出错了：")
                        console.error(error)
                    }
                    this.addMapJs()
                    window.showMap = () => this.showMap(longitude, latitude) //显示地图
                }

            })

            let eventId = violationInfo.canProcess == '0'
                ? 'Violation_EnterViolationDetails_No'
                : violationInfo.proxyRule && Object.keys(violationInfo.proxyRule).length > 0
                    ? 'Violation_EnterViolationDetails_Add'
                    : 'Violation_EnterViolationDetails_Complete';
            common.sendCxytj({
                eventId: eventId
            })
        } catch (error) {
            //报错后直接跳转到首页
            this.toUrl('/')
        }
    }

    /**
     * 请求接口获取违章数据
     */
    fetchData() {
        let { violationId, carId } = this.props.params
        if (carId) {
            //存在carId
            this.getCarsViolations(carId);
        } else {
            //不存在carId
            Toast.info(ChMessage.FETCH_FAILED, 1, () => {
                window.location.replace(config.indexPageUrl) //重定向到首页
            })
        }
    }

    /**
     * 请求车辆信息和违章数据
     */
    getCarsViolations(carId) {
        this.props.violationActions.getCarsViolations({
            carId: carId
        }, result => {
            if (result && result.code == '1000') {
                //接口请求成功
                this.showViolationDeatail()
                this.getData()
            } else {
                Toast.info(ChMessage.FETCH_FAILED, 1, () => {
                    window.location.replace(config.indexPageUrl) //重定向到首页
                })
            }
        })
    }

    /**
     * 显示违章详情内容
     */
    showViolationDeatail() {
        this.setState({
            ready: true
        })
    }

    /**
     * 加载地图
     */
    addMapJs() {
        var hm = document.createElement("script");
        hm.src = "//webapi.amap.com/maps?v=1.3&key=8b6a036e7be5b0283ff1600c9795599f&callback=showMap"; //统计JS的路径
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    }

    /**
     * 显示地图
     * @param {*double} longitude 经度
     * @param {*double} latitude 纬度
     */
    showMap(longitude = 0, latitude = 0) {
        //高德地图
        var marker, map = new AMap.Map('container', {
            resizeEnable: true,
            zoom: 15,
            center: [longitude, latitude]

        });
        marker = new AMap.Marker({
            icon: "./images/icon-map-peccancy.png",
            position: [longitude, latitude],
            map: map
        });
    }

    /**
     * 显示confirm确认对话框
     * @param {object} props 确认对话框的props值
     */
    showConfirm(props = {}) {
        let { confirmInfo } = this.state

        props.show = true //显示确认框
        this.setState({
            confirmInfo: Object.assign({}, confirmInfo, props)
        })

    }

    /**
     * 隐藏confirm确认对话框
     */
    hideConfirm() {
        let { confirmInfo } = this.state

        this.setState({
            confirmInfo: Object.assign({}, confirmInfo, {
                show: false
            })
        })
    }

    /**
     * 办理全部按钮
     */
    confirmLeftBtn() {
        this.hideConfirm()
        let carInfo = this.getCarInfo() //车辆信息
        let canProcessArr = this.getCanProcessViolationIds()
        try {
            let proxyRule = this.getProxyRule(canProcessArr);
            if (Object.keys(proxyRule).length > 0) {
                //需要补充资料 跳转到补充资料页面
                common.sendCxytj({
                    eventId: 'Violation_Popup_HandleAll_AddData',
                })
                let search = '';
                for (let i in proxyRule) {
                    search += '&' + i + '=' + proxyRule[i]
                }
                search = '?' + search.substr(1)
                this.toUrl(`/updateCarRequirement/${carInfo.carId}/${canProcessArr.toString()}${search}`)
            } else {
                //跳转到订单确认页面
                common.sendCxytj({
                    eventId: 'Violation_Popup_HandleAll_OrderConfirm',
                })
                this.toOrderConfirm(canProcessArr.toString())
            }
        } catch (error) {
            console.log("出错了：", error)
        }
    }

    /**
     * 仅办理该条
     */
    confirmRightBtn() {
        common.sendCxytj({
            eventId: 'Violation_Popup_HandleOnly_NotAll',
        })
        let { violationId } = this.props.params
        this.toOrderConfirm(violationId)
    }

    /**
     * 立即办理按钮
     */
    sendBtn() {
        let { violationId } = this.props.params
        let canProcessArr = this.getCanProcessViolationIds();
        common.sendCxytj({
            eventId: 'h5_e_wz_wzdetail_surenow',
        })
        if (canProcessArr && canProcessArr.length > 1) {
            common.sendCxytj({
                eventId: 'Violation_HandleAllorOnly_Popup',
            })
            this.showConfirm({
                show: true,
                content: `该车辆目前有${canProcessArr.length}条可代办违章尚未处理`,
            })
        } else {
            common.sendCxytj({
                eventId: 'Violation_HandletheOnlyone_OrderConfirm',
            })
            this.toOrderConfirm(violationId)
        }
    }

    /**
     * 获取需要补充的资料
     */
    getProxyRule(canProcessArr) {
        let { violations } = this.props
        let carInfo = this.getCarInfo() //车辆信息
        let { untreatedIdList } = carInfo
        let proxyRule = {}
        if (canProcessArr) {
            canProcessArr.map(violationId => {
                proxyRule = Object.assign(proxyRule, violations.data[violationId].proxyRule)
            })
        }

        return proxyRule
    }

    /**
     * 获取车辆信息
     */
    getCarInfo() {
        let { cars, violations } = this.props
        let violationInfo = violations.data[this.props.params.violationId];
        let { carId } = violationInfo
        let carInfo = cars.data[carId]

        return carInfo
    }

    /**
     * 获取可代办的违章Ids 返回数组
     */
    getCanProcessViolationIds() {
        let { violations } = this.props
        let carInfo = this.getCarInfo() //车辆信息
        let canProcessIds = []; //可办理id列表
        let violationInfo = {}; //违章信息

        violations.result.map(item => {
            violationInfo = violations.data[item]

            if (violationInfo.carId == carInfo.carId && violationInfo.canProcess == '1' && violationInfo.violationStatus == '0') {
                canProcessIds.push(item)
            }
        })
        return canProcessIds
    }

    /**
     * 跳转到订单确认页面
     */
    toOrderConfirm(violationIds) {
        localStorage.setItem("orderConfirm_violationIds", violationIds)
        this.toUrl(`/orderConfirm`) //跳转到违章办理页面
    }

    /**
    * 跳转到补充资料页面
    */
    toUpdateCarRequirement(proxyRule) {
        common.sendCxytj({
            eventId: 'Violation_AddHandingData_Details',
        })
        let { carId, violationId } = this.props.violations.data[this.props.params.violationId]
        let search = '';
        for (let i in proxyRule) {
            search += '&' + i + '=' + proxyRule[i]
        }
        search = '?' + search.substr(1)
        this.toUrl(`/updateCarRequirement/${carId}/${violationId}${search}`)
    }

    //跳转到评论
    jumptoComment(item) {
        common.sendCxytj({
            eventId: 'h5_e_wz_wzdetail_redian',
        })
        let userId = sessionStorage.getItem("userId");
        let token = sessionStorage.getItem("token");
        if (item.commentURL) {
            window.location.href = encodeURI(item.commentURL + "userId=" + userId + "&token=" + token);
        }
    }

    //跳转到罚款代缴
    jumptoLocale(item) {
        let token = sessionStorage.getItem("token");
        let param = "";
        //app渠道解决未设置userType的cookie导致违章办理不了的问题
        if (common.isCXYApp()) {
            param = "&userType=app_cxy&token=" + token;
        }
        if (item.forwardUrl) {
            window.location.href = item.forwardUrl + param;
        }
    }

    render() {
        let violationInfo = this.props.violations.data[this.props.params.violationId];
        let { confirmInfo, ready, comment } = this.state
        let footerWrap = violationInfo.canProcess == '0' || violationInfo.violationStatus != '0' ? Style.noBorder : "";
        let placeHolder = violationInfo.canProcess == '0' || violationInfo.violationStatus != '0' ? Style.H170 : violationInfo.proxyRule && Object.keys(violationInfo.proxyRule).length > 0 ? Style.H238 : Style.H170
        console.log(violationInfo)
        return (
            <div>
                {
                    !ready ? '' :
                        <div className='box'>
                            <Comfirm {...confirmInfo} />
                            <div className={Style.detailBox}>
                                <div className={Style.location}>{violationInfo.locationName + violationInfo.location}</div>
                                <p className={Style.occurTime}>{violationInfo.occurTime}</p>
                                <div className={Style.reason}>{violationInfo.reason}</div>
                                <div className={Style.orangeBox}>
                                    <span>罚款{violationInfo.fine}元</span>
                                    <span>扣{violationInfo.degree}分</span>
                                    {violationInfo.lateFine && <span>滞纳金{violationInfo.lateFine}元</span>}
                                </div>
                                <div className={Style.detailInfo}>
                                    <p>违章代码：{violationInfo.violationCode}</p>
                                    <p>处罚决定书编号：{violationInfo.archive}</p>
                                </div>
                                <div className={Style.locationBox}>
                                    <div className={Style.locationTop}>
                                        {/*<div className={'text-overflow-1 ' + Style.locationName}>{violationInfo.locationName + violationInfo.location}</div>*/}
                                        <div className={Style.locationNum + (violationInfo.locationViolationNumber ? '' : ' hide')}>
                                            此处违章 : {violationInfo.locationViolationNumber}
                                        </div>
                                    </div>
                                    <div className={Style.mapBox} id='container'>
                                        <div className={Style.mapH}></div>
                                        {/*这里是地图*/}
                                    </div>
                                    {comment && <div>
                                        {comment.commentCount > 0 ?
                                            <div className={Style.commentBox} onClick={() => this.jumptoComment(comment)}>
                                                <div className={Style.head}>
                                                    <div className={Style.headImg}><img src={comment.headImgUrl} /></div>
                                                    <div className={Style.nickName}>{comment.commentator}</div>
                                                    <div className={Style.time}>{comment.commentTime}</div>
                                                </div>
                                                <div className={Style.content + " text-overflow-2"}>{comment.comment}</div>
                                                <div className={Style.moreComment}>更多评论({comment.commentCount})</div>
                                            </div>
                                            : <div className={Style.noCommentBox}>
                                                <span onClick={() => this.jumptoComment(comment)}>写下您的违章故事，留给他人一份安全</span>
                                            </div>
                                        }
                                    </div>
                                    }
                                </div>
                                <div className={placeHolder}></div>
                            </div>
                            <div className={Style.footerBtnBox + " " + footerWrap}>
                                {violationInfo.isLocale == "1" ? (violationInfo.violationStatus != '0' ? "" : <div className='btn' onClick={() => this.jumptoLocale(violationInfo)}>立即办理</div>) :
                                    violationInfo.canProcess == '0' || violationInfo.violationStatus != '0' ? "" :
                                        violationInfo.proxyRule && Object.keys(violationInfo.proxyRule).length > 0 ?
                                            <div>
                                                <p className={Style.footerText}><span className={Style.footerIcon}>一分钟补充资料，即可在线办理</span></p>
                                                <div className='btn' onClick={() => this.toUpdateCarRequirement(violationInfo.proxyRule)}>补充资料</div>
                                            </div>
                                            :
                                            <div className='btn' onClick={() => this.sendBtn()}>立即办理</div>
                                }
                            </div>
                        </div>
                }
            </div>

        );
    }
}

//使用context
ViolationDetail.contextTypes = {
    router: React.PropTypes.object.isRequired
}

ViolationDetail.propTypes = {

};

const mapStateToProps = state => ({
    cars: state.cars,
    violations: state.violations
})

const mapDispatchToProps = dispatch => ({
    violationActions: bindActionCreators(violationActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViolationDetail);