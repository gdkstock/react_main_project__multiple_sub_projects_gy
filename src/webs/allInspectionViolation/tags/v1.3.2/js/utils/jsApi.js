import baseJsApi from 'app/utils/jsApi';

class JsApi extends baseJsApi {
    constructor() {
        super();

    }

    //向app广播更新车辆信息
    appUpdateCarMsg(){
        try{
            window.cx580.jsApi.call({
                "commandId":"",
                "command":"sendBroadcast",
                "data":{
                    "eventId": "H5_updateCar",
                    "msgObj":{"needUpdate":"true"}
                }
            },function(data){});
        }catch(e){

        }
    }

    //向app广播更新违章信息
    appUpdateViolation(carId){
        try{
            window.cx580.jsApi.call({
                "commandId":"",
                "command":"sendBroadcast",
                "data":{
                    "eventId": "H5_updateViolation",
                    "msgObj":{"carId":carId}
                }
            },function(data){});
        }catch(e){

        }
    }

    /**
     * 调用jsdk支付
     *
    */
    pay(data,callback){
        let { orderAmt , payType} = data;
        payType = orderAmt*1==0?"17":payType;
        data = {
            ...data,
            payType
        }
        try{
            window.cx580.jsApi.call({
                "commandId":"",
                "command":"payOrder",
                "data":data,
            },function(data){
                if(callback){
                    callback(data)
                }
            })
        }catch(e){
            if(callback){
                callback(e)
            }
        }
    }

    /**
     * 关闭webview
    */

    closeAppView(){
        if (window.jsApiIsLoad) {
            this._close()
        } else {
            document.addEventListener('jsApiIsReady', () => this._close());
        }
    }

    _close(){
        try {
            window.cx580.jsApi.call({
                "commandId": "",
                "command": "close",
            }, function (data) {

            });
        } catch (e) {
        }
    }
    
}

// 实例化后再导出
export default new JsApi()