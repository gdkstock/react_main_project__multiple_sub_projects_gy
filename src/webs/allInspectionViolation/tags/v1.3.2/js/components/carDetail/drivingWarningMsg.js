/*
*	警告轮播信息组件
	props = {
		text:"",//轮播文本
	}
*/
import React from 'react'
import styles from './drivingWarningMsg.scss'

class DrivingWarningMsg extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			id:this.props.id,
			text:this.props.text
		}
	}

	componentDidMount(){
	}

	componentDidUpdate(){
		
	}

	render(){
		return (
            <div className={styles.wrap+" "+styles.animate}>
                <div className={styles.angle}></div>
				<div className={styles.wraningMsg}>{this.props.text}</div>
			</div>
		)
	}
}

export default DrivingWarningMsg	