/*
*	年检卡片信息组件
	props = {
		days:"",//年检天数
	}
*/

import styles from './index.scss'
import common from '../../utils/common'

const defaultProps = {
	carId:"",
	days:"",	//距离年检天数
	state:"", 	//年检状态
	content:{__html:"只需一步，年检标志寄到家"},
	rightText:"立即办理",
	handleClick:()=>{},
}

class Annual extends React.Component{
	// props = defaultProps
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			...this.props,
		}
	}

	componentDidUpdate(){
		// let { carId , day } = this.props;
		// let preDay = this.state.day;
		// if(day>0){
		// 	let obj={
		// 	 	id:"annualDays"+carId,//定时器id
		// 		endNum:day,//目标值
		// 	}
		// 	if(sessionStorage.getItem("isQueryAnnualDataDone")){
		// 		sessionStorage.removeItem("isQueryAnnualDataDone")
		// 		//年检数据请求完成执行动画
		// 		common.animation(obj)
		// 	}
		// }
	}

	componentWillUnmount() {
		// let { carId } = this.props
		// window["timerannualDays"+carId] && (window["timerannualDays"+carId]=null);
    }
	
	render(){
		const props = this.props
		return (
			<div className={styles.wrap} onClick={props.handleClick}>
				<div className={styles.leftText}>
					<div className={styles.title}>车辆年检</div>
					<div className={styles.content} dangerouslySetInnerHTML={props.content}></div>
				</div>
				<div>
					<span className={styles.rightText}>{props.rightText?props.rightText:""}</span><i className={styles.rightIcon}></i>
				</div>
			</div>
		)
	}
}

export default Annual