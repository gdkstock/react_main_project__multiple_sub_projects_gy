/*
 * 首页违章概要信息列表组件
 * props={
	list:[
		{
			carId:"1234",
			carNumber:"粤A02188",
			totalNum:"26",
			totalFine:"9999",
			totalDegree:"99"
		}
	],
	handleClick:()=>{}
 }
 */
 import Item from './item'
 import styles from './index.scss'

 const ListA = props => {
 	const { list ,handleClick } = props;
 	return (
 		<div>
 			{
 				list.length>0 && list.map((item,i)=>
 					<div key={i}>
 						<div className={styles.whiteSpace_28}></div>
 						<Item {...item} handleClick={ handleClick }/>
 					</div>
 				)
 			}
 		</div>
 	)
 }

 export default ListA