/**
 * 支付成功页面
*/

import React, { Component } from 'react';
import styles from './defaultAd.scss'
import config from '../../config'

class DefaultAd extends Component{
    constructor(props) {
        super(props)
        this.state={
            list:[
                {
                    icon:"./images/icon_search_blue.png",
                    test_targetUrl:"http://webtest.cx580.com:9021/drivingLicence/index.html?api_version=3",
                    targetUrl:"http://daiban.cx580.com/drivingLicence/index.html?api_version=3",
                    text:"驾照查分",
                },
                {
                    icon:"./images/icon_water_blue.png",
                    test_targetUrl:"http://webtest.cx580.com:5000/page/PackageOil/PackageIndex.aspx?userType=app",
                    targetUrl:"https://chewu.cx580.com/OrderSite/page/OilCard/index.aspx?userType=APP",
                    text:"油卡充值",
                },
                {
                    icon:"./images/icon_punish_blue.png",
                    test_targetUrl:"http://webtest.cx580.com:8377/authorization?userType=app_cxy",
                    targetUrl:"https://finegw.cx580.com/authorization?userType=app_cxy",
                    text:"罚款缴费",
                },
                {
                    icon:"./images/icon_insurance_blue.png",
                    test_targetUrl:"http://webtest.cx580.com:5000/page/Orders/AnnVer/AnnVerGuide.aspx?userType=app",
                    targetUrl:"https://discovery.cx580.com/content/index.html?userType=app_cxy",
                    text:"车险报价",
                }

            ]
        }
    }

    toUrl(url){
        if(url)window.parent.location.href=url;
    }

    render(){
        let { list } = this.state;
        return (
            <div className={styles.container}>
                <div className={styles.title}><span>办完违章后大家还办了以下业务</span></div>
                <div className={styles.list}>
                    {
                        list.map((item,index)=>{
                            return (
                                <div key={index} className={styles.item} onClick={()=>this.toUrl(config.production?item.targetUrl:item.test_targetUrl)}>
                                    <img src={item.icon}/>
                                    <p>{item.text}</p>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default DefaultAd