/**
 * 违章相关action
 */

import {
    ADD_ORDER,
    ADD_ORDER_ASYNC
} from './actionsTypes.js'

/**
 * 同步action
 */
export const addOrder = data => ({ type: ADD_ORDER, data: data }) //添加订单

/**
 * 异步action
 */
export const addOrderAsync = data => ({ type: ADD_ORDER_ASYNC, data: data }) //添加订单