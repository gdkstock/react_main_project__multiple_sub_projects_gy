/*
* 车辆及驾照相关action
*/
import {
	REQUEST_CAR_LIST,
	QUERY_BANNERS,
	QUERY_CAR_CONDITION_LIST,
	QUERY_ADD_CAR,
	QUERY_CAR_INFO,
	QUERY_VIOLATION_BRIEF,
	QUERY_DELETE_CAR,
	QUERY_UPDATE_CAR,
	QUERY_BRAND_LIST,
	// QUERY_MODEL_LIST,
	QUERY_DRIVINGLICENCE,
	QUERY_DRIVINGLICENCE_LIST,
	QUERY_ADD_DRIVINGLICENCE,
	QUERY_UPDATE_DRIVINGLICENCE,
	QUERY_DELETE_DRIVINGLICENCE,
	QUERY_INSPECTION,
	GET_CAR_INFORMATION,
	GET_CAR_CONDITION_LIST,
	QUERY_CAR_LIST_ONLY,
	GET_AD,
} from './actionsTypes'

//请求车辆列表
export const queryCarList = (param, callback) => ({
	type: REQUEST_CAR_LIST,
	param,
	callback
})

//请求车辆列表数据（不包含违章）
export const queryCarListOnly = (param, callback) => ({
	type: QUERY_CAR_LIST_ONLY,
	param,
	callback
})

//请求广告
export const queryBanners = (param) => ({
	type: QUERY_BANNERS,
	param
})

//请求查违章约束条件
export const queryCarCondition = (param, callback) => ({
	type: QUERY_CAR_CONDITION_LIST,
	param,
	callback
})

//获取车牌前缀列表和查询违章条件列表 
export const getCarConditionList = (data, callback) => ({
    type: GET_CAR_CONDITION_LIST,
    data,
    callback
})

//请求添加车辆
export const queryAddCar = (param) => ({
	type: QUERY_ADD_CAR,
	param
})

//请求删除车辆
export const queryDeleteCar = (param) => ({
	type: QUERY_DELETE_CAR,
	param
})

//请求修改车辆信息
export const queryUpdateCar = (param) => ({
	type: QUERY_UPDATE_CAR,
	param
})

//请求查询车辆信息
export const queryCarInfo = (param) => ({
	type: QUERY_CAR_INFO,
	param
})

//请求车辆违章概要信息
export const queryViolationBrief = (param) => ({
	type: QUERY_VIOLATION_BRIEF,
	param
})

//请求车辆品牌数据
export const queryBrandList = (param) => ({
	type: QUERY_BRAND_LIST,
	param
})

// //请求车型数据
// export const queryModelList = ( param ) => ({
// 	type:QUERY_MODEL_LIST,
// 	param
// })

//请求单个驾照信息
export const queryDrivingLicence = (param) => ({
	type: QUERY_DRIVINGLICENCE,
	param
})

//请求驾照列表
export const queryDrivingLicenceList = (param) => ({
	type: QUERY_DRIVINGLICENCE_LIST,
	param
})

//添加驾照信息
export const queryAddDrivingLicence = (param) => ({
	type: QUERY_ADD_DRIVINGLICENCE,
	param
})

//修改驾照信息
export const queryUpdateDrivingLicence = (param) => ({
	type: QUERY_UPDATE_DRIVINGLICENCE,
	param
})

//删除驾照信息
export const queryDeleteDrivingLicence = (param) => ({
	type: QUERY_DELETE_DRIVINGLICENCE,
	param
})

//请求年检信息
export const queryInspection = (param) => ({
	type: QUERY_INSPECTION,
	param
})


//根据车牌号码获取相关信息
export const getCarInformation = (data, callback) => ({ type: GET_CAR_INFORMATION, data, callback })

//请求广告信息
export const getAd = (data,callback) => ({type: GET_AD, data, callback})
