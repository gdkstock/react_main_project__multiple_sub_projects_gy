/**
 * 违章相关action
 */

import {
    ADD_COUPON,
    ADD_COUPONS,
    DELETE_COUPON
} from './actionsTypes.js'

/**
 * 同步action
 */
export const addCoupon = data => ({ type: ADD_COUPON, data: data })
export const addCoupons = data => ({ type: ADD_COUPONS, data: data })
export const deleteCoupon = data => ({ type: DELETE_COUPON, data: data })