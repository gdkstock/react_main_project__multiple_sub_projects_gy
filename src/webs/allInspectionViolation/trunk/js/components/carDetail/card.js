/*
*	顶部车辆概要信息组件
	props = {
		carId:"",
		carNumber:"",
		completely:"",
		vipImg:"",//认证图标
		carLogoImg:"",//车标
		bgImg:"",//背景图
		carModelName:""//车型名称
		handleClick:()=>{}
	}
*/
import React from 'react'
import common from '../../utils/common'
import styles from './card.scss'
import WarningInfo from './warningInfo'

const defaultProps = {
	carId:"22",
	carNumber:"粤A12345",
	completely:1,
	carBrandLogoUrl:"dfd",//车标
	carModelName:"宝马X6 2014款",//车型名称
	handleClick:()=>{}
}

const openBrand= (props)=>{
	let carId = props.carId;
	let carNumber = props.carNumber;
	//点击选择车系埋点
	common.sendCxytj({
		eventId:"Violation_CarBrand"
	})
	window.open(common.getRootUrl()+"brand/"+carId+"/"+carNumber,'_self')
}

class Card extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			...this.props,
		}
	}

	componentWillReceiveProps(nextProps){
		console.log("nextProps",nextProps)
		this.setState(nextProps)
	}

	componentDidMount(){

		// this.createAnimation()
	}

	componentDidUpdate(){
		// if(sessionStorage.getItem("isQueryViolationDataDone")){
		// 	sessionStorage.removeItem("isQueryViolationDataDone")
		// 	this.createAnimation()
		// }
	}

	componentWillUnmount() {
		
    }

	//图片出错
	onLogoError(){
		this.setState({
			carBrandLogoUrl:""
		})
	}

	render(){
		const props = this.state
		let { untreatedNum , totalFine , totalDegree} = props;
		return (
			<div className={styles.wrap}>
                <div className={styles.carInfo}>
					<div className={styles.carNumberWrap} onClick={()=>props.editCar(props.carId,"edit")}></div>
                    <div className={styles.carNum+" "+styles.v_center}>
                        <div className={styles.carNumber}>
                            {props.carNumber?(props.carNumber.slice(0,2)+" "+props.carNumber.slice(2)):""}
                            {props.completely==1 && <i className={styles.vipIcon}></i>}
                        </div>
                        <div>
                            {
                                props.carModelName?
                                <div className={styles.authority}>{props.carModelName}</div>
                                :<div className={styles.unauthority}><span>认证车辆信息</span></div>
                            }
                        </div>
                    </div>
                    <div className={styles.bgImg}></div>
                    <div>
                        {
                            props.carBrandLogoUrl ? 
                            <div className={styles.carLogo+" "+styles.v_center}  onClick={()=>openBrand(props)}><img src={props.carBrandLogoUrl} onError={()=>this.onLogoError()}/></div>
                            :<div className={styles.carLogo_default+" "+styles.v_center}  onClick={()=>openBrand(props)}></div>
                        }
                    </div>
                </div>
				<div className={styles.violationWrap} onClick={props.untreatedNum=="?"?()=>props.editCar(props.carId):()=>props.violationDetail(props.carId)}>
					<div className={styles.untreatedNum}>
						{props.violationNew && <div className={styles.unread}></div>}
						<div id={"untreatedNum"+props.carId}>{untreatedNum}</div>
					</div>
					<div>
						<div className={styles.violationTitle}>未办理违章</div>
						<div>
							<div className={styles.commonItem}>
								<div id={"totalFine"+props.carId}>{totalFine}</div>
								<div>待罚款(元)</div>
							</div>
							<div className={styles.commonItem}>
								<div id={"totalDegree"+props.carId}>{totalDegree}</div>
								<div>待扣分</div>
							</div>
						</div>
					</div>
					<div className={styles.btnWrap}>
						<div className={styles.checkDetailBtn}>{props.rightText}</div>
					</div>
				</div>
				{props.violationWarningMsg && <WarningInfo text={props.violationWarningMsg} id={"violationWarningMsg"+props.carId}/>}
			</div>
		)
	}
}

export default Card