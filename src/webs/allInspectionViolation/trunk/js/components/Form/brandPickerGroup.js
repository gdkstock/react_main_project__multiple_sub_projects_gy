/*
	品牌选择组件
	props={
		groupId:"registerDate" ,//唯一编码
		label:'车辆注册日期',//标签文本
		placeholder:'请选择' ,//占位符文本
		type:"date" ,//日期选择器类型
		format:"", //日期格式
		handleChange:()=>{} ,//输入框文本改变操作
		value:"" //输入框值
	}
*/
import styles from './brandPickerGroup.scss'
const DateGroup = props => {
	return (
		<div className="wz_item" onClick={() => props.handleChange()}>
			<span className="label">{props.label}</span>
			<input
				className="txt hasRightIcon"
				placeholder="请选择"
				name="brandPicker"
				value={props.value}
				disabled
			/>
			<i className="rightIcon"></i>
		</div>
	)
}

export default DateGroup