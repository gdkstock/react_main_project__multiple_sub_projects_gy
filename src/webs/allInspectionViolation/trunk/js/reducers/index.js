import { combineReducers } from 'redux'
import cars from './cars'
import banners from './banners'
import provinces from './provinces'
import carConditionList from './carConditionList'
import violations from './violations'
import drivingLicence from './drivingLicence'
import coupons from './coupons'
import user from './users'
import carBrand from './carBrand'

const rootReducer = combineReducers({
    cars,
    violations,
    banners,
    provinces,
    carConditionList, //车牌前缀列表和查询违章条件列表
    drivingLicence,
    coupons,
    user, //用户信息表
    carBrand,
});

export default rootReducer;
