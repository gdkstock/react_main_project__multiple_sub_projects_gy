/*
	全局状态drivingLicence
*/
import {
    UPDATE_DRIVINGLICENCE_DATA,
    DELETE_DRIVINGLICENCE_DATA,
    ADD_DRIVINGLICENCE_LIST,
} from '../actions/actionsTypes'

const initState = {
	data:{

	},
	result:[]
}

//测试
// const initState = {
// 	data: {
// 		"1234":{
// 			drivingLicenseId:"1234",
// 			driverName:"李君",
// 			drivingLicenseNo:"432503199008035697",
// 			drivingFileNumber:"432503199008035697",
// 			drivingTelephone:"18738116546",
// 			score:"12",
// 			deductionScore:"6",
// 			remainScore:"6",
// 			cycleEndDate:"2017-12-30",
// 			drivingWarningMsg:"",
// 		},
// 		"1236":{
// 			drivingLicenseId:"1236",
// 			driverName:"李君",
// 			drivingLicenseNo:"432503199008035697",
// 			drivingFileNumber:"432503199008035697",
// 			drivingTelephone:"18738116546",
// 			score:"12",
// 			deductionScore:"6",
// 			remainScore:"6",
// 			cycleEndDate:"2017-12-30",
// 			drivingWarningMsg:"",
// 		}
// 	},
// 	result: [ '1234', '1236' ] 
// }

export default function drivingLicence( state = initState, action ){
	let object={};
	let data = action.data;
	switch(action.type){
		case UPDATE_DRIVINGLICENCE_DATA:
			object.data = {...state.data,[data.drivingLicenseId]:data};
			object.result = Array.from(new Set([...state.result,data.drivingLicenseId]));
			return object;
		case DELETE_DRIVINGLICENCE_DATA:
			object = { ...state }
			delete object.data[data.drivingLicenseId]
			let i = object.result.indexOf(data.drivingLicenseId)
			object.result.splice(i, 1);
			return object;
		case ADD_DRIVINGLICENCE_LIST:
			object.data = {}
			object.result = []
			for (let j = 0; j < data.length; j++) {
				let id = data[j].drivingLicenseId;
				object.data[id] = data[j];
				object.result.push(id);
			}
			return object;
		default:
			return state;
	}
}


