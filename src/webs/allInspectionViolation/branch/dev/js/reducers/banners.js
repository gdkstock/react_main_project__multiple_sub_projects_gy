import {
    ADD_TO_BANNERS
} from '../actions/actionsTypes'

const initState = {
	data:{

	},
	result:[]
}

//测试
// const initState = {
// 	data: {
// 		"WZ_CAR_SY_01":{
// 			positionCode:"WZ_CAR_SY_01",
// 			adList:[
// 				{
// 					eventId:"123",
// 					imageUrl:"./images/banner_lg.png",
// 					jumpUrl:"http://www.baidu.com"
// 				},
// 				{
// 					eventId:"134",
// 					imageUrl:"./images/banner_lg.png",
// 					jumpUrl:"http://www.baidu.com"
// 				},
// 				{
// 					eventId:"145",
// 					imageUrl:"./images/banner_lg.png",
// 					jumpUrl:"http://www.baidu.com"
// 				}
// 			]
// 		},
// 		"WZ_CAR_LIST_01":{
// 			positionCode:"WZ_CAR_LIST_01",
// 			adList:[
// 				{
// 					eventId:"234",
// 					imageUrl:"./images/banner_sm.png",
// 					jumpUrl:"http://www.baidu.com"
// 				},
// 				{
// 					eventId:"245",
// 					imageUrl:"./images/banner_sm.png",
// 					jumpUrl:"http://www.baidu.com"
// 				},
// 				{
// 					eventId:"256",
// 					imageUrl:"./images/banner_sm.png",
// 					jumpUrl:"http://www.baidu.com"
// 				}
// 			]
// 		}
// 	},
// 	result: [ 'WZ_CAR_SY_01', 'WZ_CAR_LIST_01' ] 
// }

export default function banners( state = initState, action ){
	switch(action.type){
		case ADD_TO_BANNERS:
				let positionCode = action.data.positionCode,
					object={};
					object.data = {...state.data,[positionCode]:action.data}
					object.result = Array.from(new Set([...state.result,action.data.positionCode]))
			return object;
		default:
			return state;
	}
}