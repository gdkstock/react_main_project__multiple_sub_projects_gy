/**
 * 优惠券相关接口
 */

import apiHelper from './apiHelper';
import config from '../config'

class CouponService {
    /**
     * 有可用优惠券
     */
    useful(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}coupon/useful`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}
// 实例化后再导出
export default new CouponService()