/**
 * 违章相关action
 */

import {
    ADD_COUPON,
    ADD_COUPONS,
    DELETE_COUPON,
    USEFUL_COUPON_ASYNC,
} from './actionsTypes.js'

/**
 * 同步action
 */
export const addCoupon = data => ({ type: ADD_COUPON, data })
export const addCoupons = data => ({ type: ADD_COUPONS, data })
export const deleteCoupon = data => ({ type: DELETE_COUPON, data })

/**
 * 异步action
 */
export const usefulCouponAsync = (data, callback) => ({ type: USEFUL_COUPON_ASYNC, data, callback })