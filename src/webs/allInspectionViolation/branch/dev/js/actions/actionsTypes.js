/*
* 这里定义所有的action类型
* */

/**
 * fetch数据请求相关
 */
export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时

//首页车辆列表页
export const REQUEST_CAR_LIST = 'REQUEST_CAR_LIST' // 请求车辆列表数据

/*=======公共state修改========*/

//cars
export const QUERY_ADD_CAR = 'QUERY_ADD_CAR' //请求添加车辆操作
export const ADD_CAR = 'ADD_CAR' //添加车辆信息
export const ADD_CAR_LIST = "ADD_CAR_LIST" //添加车辆列表信息

export const QUERY_CAR_LIST_ONLY = 'QUERY_CAR_LIST_ONLY'//新版请求车辆列表接口

export const QUERY_UPDATE_CAR = 'QUERY_UPDATE_CAR' //请求修改车辆操作
export const UPDATE_CAR = 'UPDATE_CAR' //修改车辆信息

export const QUERY_DELETE_CAR = 'QUERY_DELETE_CAR' //请求删除车辆操作
export const DELETE_CAR = 'DELETE_CAR' //删除车辆信息

export const QUERY_CAR_INFO = 'QUERY_CAR_INFO' //查询车辆信息

export const QUERY_VIOLATION_BRIEF = 'QUERY_VIOLATION_BRIEF' //查询违章概要信息

export const QUERY_BRAND_LIST = 'QUERY_BRAND_LIST' //请求车辆品牌列表
export const ADD_BRAND_LIST = 'ADD_BRAND_LIST' //添加到车辆品牌列表

export const GET_REQUIREMENT_ASYNC = 'GET_REQUIREMENT_ASYNC' //查询办理违章所需资料接口

export const GET_CAR_INFORMATION = 'GET_CAR_INFORMATION' //根据车牌获取车辆的相关信息

// export const QUERY_MODEL_LIST = 'QUERY_MODEL_LIST' //请求车型数据
// export const ADD_MODEL_LIST = 'ADD_MODEL_LIST' //添加到车型中

//banners
export const QUERY_BANNERS = 'QUERY_BANNERS' //请求广告
export const ADD_TO_BANNERS = 'ADD_TO_BANNERS' //添加到state中
export const GET_AD = 'GET_AD' //文案广告（违章详情页面和车辆详情页面）

//车辆违章查询约束条件
export const QUERY_CAR_CONDITION_LIST = 'QUERY_CAR_CONDITION_LIST' //请求车辆违章约束条件信息
export const ADD_TO_PROVINCES = 'ADD_TO_PROVINCES' //添加到state中
export const GET_CAR_CONDITION_LIST = 'GET_CAR_CONDITION_LIST' //引导注册 - 请求车辆违章约束条件信息
export const ADD_CAR_CONDITION_LIST = 'ADD_CAR_CONDITION_LIST' //添加车牌前缀和违章查询条件到全局state

//驾照相关
export const QUERY_DRIVINGLICENCE_LIST = 'QUERY_DRIVINGLICENCE_LIST' //请求驾照列表数据
export const QUERY_DRIVINGLICENCE = 'QUERY_DRIVINGLICENCE' //请求单个驾照信息
export const QUERY_ADD_DRIVINGLICENCE = 'QUERY_ADD_DRIVINGLICENCE' //请求添加驾照信息
export const QUERY_UPDATE_DRIVINGLICENCE = 'QUERY_UPDATE_DRIVINGLICENCE' // 请求修改驾照信息
export const QUERY_DELETE_DRIVINGLICENCE = 'QUERY_DELETE_DRIVINGLICENCE' //请求删除驾照信息

export const UPDATE_DRIVINGLICENCE_DATA = 'UPDATE_DRIVINGLICENCE_DATA' //更新全局状态中的驾照信息
export const DELETE_DRIVINGLICENCE_DATA = 'DELETE_DRIVINGLICENCE_DATA' //删除全局状态中的驾照信息
export const ADD_DRIVINGLICENCE_LIST = 'ADD_DRIVINGLICENCE_LIST' //添加驾照列表

export const QUERY_INSPECTION = "QUERY_INSPECTION" //请求年检信息


/**
 * 违章相关
 */
//同步请求
export const ADD_VIOLATION = 'ADD_VIOLATION' //添加违章信息
export const ADD_VIOLATIONS = 'ADD_VIOLATIONS' //添加违章数组信息（同时添加多条违章）
export const UPDATE_VIOLATION = 'UPDATE_VIOLATION' //修改违章信息
export const DELETE_VIOLATION = 'DELETE_VIOLATION' //删除违章信息
export const ADD_READ_VIOLATION = 'ADD_READ_VIOLATION' //添加已阅读违章
export const DELETE_PROXY_RULE_KEY = 'DELETE_PROXY_RULE_KEY' //删除所需补充的资料key
export const GET_CARS_VIOLATIONS = 'GET_CARS_VIOLATIONS' //获取车辆信息和违章信息
export const GET_COMMENT = 'GET_COMMENT' //获取违章热点评论

//异步请求
export const GET_VIOLATION_ASYNC = 'GET_VIOLATION_ASYNC' //获取违章信息-异步
export const GET_VIOLATION_LIST_ASYNC = 'GET_VIOLATION_LIST_ASYNC' //获取违章列表信息-异步
export const UPDATE_VIOLATION_ASYNC = 'UPDATE_VIOLATION_ASYNC' //修改违章-异步
export const GET_LOCATION_VIOLATION_NUMBER_ASYNC = 'GET_LOCATION_VIOLATION_NUMBER_ASYNC' //获取违章热点违章次数

/**
 * 优惠券相关
 */
export const ADD_COUPON = 'ADD_COUPON' //添加优惠券
export const ADD_COUPONS = 'ADD_COUPONS' //添加优惠券数组
export const DELETE_COUPON = 'DELETE_COUPON' //删除优惠券
export const USEFUL_COUPON_ASYNC = 'USEFUL_COUPON_ASYNC' // 可用优惠券


/**
 * 订单相关
 */
//同步请求
export const ADD_ORDER = 'ADD_ORDER'; //添加订单

//异步请求
export const ADD_ORDER_ASYNC = 'ADD_ORDER_ASYNC'; //添加订单
