/**
 * 订单支付成功页
 */

import React, { Component } from 'react';

//styles
import styles from './index.scss';

//server
import orderService from '../../services/orderService';
import carService from '../../services/carService';

//配置
import config from '../../config';

// jsApi
import jsApi from '../../utils/jsApi';

//常用工具类
import common from '../../utils/common';
import { ChMessage } from '../../utils/message.config'

class OrderPay extends Component {
    constructor(props) {
        super(props)

        this.state = {
            orderId: '',
            orderTitle: '', //订单标题
            carNumber: '',
            orderStatus: '', //订单状态
            showMove: false, //显示更多
            handleMsg: '',
            moveText: '',
            orderMsg: '',
            voliationCount: '', //违章条数
            voliationsAmountScore: '', //违章扣分
            voliationsFineAmount: '', //罚款金额
            orderVoliations: [], //违章列表
            orderAmount: '',
            showHelpBox: navigator.userAgent.indexOf('appname_cxycwz') > -1, //显示常见问题和联系客服模块
            accountList: [], //
            createTime: '', //下单时间
            payNo: '', //支付流水号 || 交易号
            payTime: '', //支付时间
            payType: '', //支付类型
            payTypeName: '', //支付名称
            orderPayAmount: '', //总总额
        }
    }

    componentWillMount() {
        common.setViewTitle('支付成功');
    }

    componentDidMount() {
        common.backToUrlAndCloseWebView(config.myOrderUrl);
        common.noCloseWebViewUrl(window.location.href); //直接关闭，不跳转到我的订单了

        this.getOrderDetail(); //获取订单详情数据
        this.getAdUrl(); //获取广告Url

        const { carId } = this.props.params
        if (carId) {
            // 发送更新违章数据广播
            jsApi.appUpdateViolation(carId);
        } else {
            // 获取不到carId时，直接更新所有车辆的违章信息
            jsApi.appUpdateCarMsg();
        }
    }

    componentWillUnmount() {

    }

    getOrderDetail() {
        let { orderId } = this.props.params

        orderService.view({
            orderId
        }).then(res => {
            if (res.code == '1000') {
                this.setState(Object.assign({}, this.state, res.data));
            } else {
                Toast.info(res.msg || ChMessage.FETCH_FAILED, 3, () => window.history.back())
            }
        }, error => {
            Toast.info(ChMessage.FETCH_FAILED)
        })
    }

    getAdUrl() {
        //请求广告信息
        carService.getAd({ positionCode: "WZ_VIOLATION_PAY_SUCCESS" }).then(res => {
            if (res.code == 1000 && res.data.adList[0]) {
                this.setState({
                    adUrl: res.data.adList[0].targetUrl
                })
            }
        }, error => {
            //广告接口出错了
        })
    }

    toOrderDetail() {
        let { orderId } = this.props.params;

        this.props.router.push(`/orderDetail/${orderId}`);
    }

    render() {
        let { orderId, orderTitle, adUrl } = this.state

        return (
            <div className='box'>
                <div className='whiteBg'></div>
                <div className={styles.successBox} onClick={() => this.toOrderDetail()}>
                    <img src='./images/icon_wddhsdg.png' />
                    <h3>支付成功</h3>
                </div>
                <div className={styles.info}>
                    <div className={styles.top} onClick={() => this.toOrderDetail()}>
                        <div className={styles.title + ' fl'}>{orderTitle}</div>
                        <div className={styles.orderDetailBtn + ' fr'}>订单详情</div>
                    </div>
                    <ul className={styles.txt}>
                        <li>已下单违章请勿重复处理</li>
                        <li>办理周期2~5个工作日</li>
                        <li>处理失败的违章全额退款</li>
                    </ul>
                </div>
                { //广告位
                    adUrl ? <iframe className={styles.iframe} src={adUrl}></iframe> : ''
                }

            </div>
        )
    }
}

export default OrderPay