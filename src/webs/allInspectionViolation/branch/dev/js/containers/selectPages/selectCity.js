import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { ListCars, ListKey } from 'app/components/selectCar/'

//antd
import { Toast } from 'antd-mobile'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './index.scss'
import { TopCard } from '../home/carCard';

// 组件变量
let showToastId = -1; // 显示Toast的句柄Id

class SelectCity extends Component {

    constructor(props) {
        super(props)

        const { type } = this.props.location.query;

        this.state = {
            isShowCarBox: false,
            top: 0,
            keyName: '', //当前点击的key

            // 车系列表 第一层
            cityList: { list: [] },

            // 字母索引列表
            carListKey: { list: [] },
        }

        this.isValuation = type === 'valuation'; // type==='valuation' 表示为估值的车型信息
    }

    componentWillMount() {
        common.setViewTitle('选择城市');
    }

    componentDidMount() {
        //获取第一层车型信息
        this.getCityList();
    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    /**
     * 获取城市列表
     */
    getCityList() {
        const cache = window.api_getCityList;
        if (cache) {
            this.setBrandData(cache)
        } else {
            Toast.loading('', 0);
            //获取城市列表
            this.props.carsActions.getCityList({}, res => {
                console.log("拿到的数据:", res)
                Toast.hide();
                this.setBrandData(res);
            });
        }
    }

    /**
     * 保存第一层点击的数据
     * @param {object} res 
     */
    setBrandData(res) {
        if (res.code == '1000' && res.data) {

            window.api_getCityList = res; // 保存数据

            this.setState({
                cityList: {
                    list: res.data,
                },
                carListKey: {
                    list: res.data.map(item => item.level), // 更新列表
                }
            })
        }
    }

    //获取点击的序号
    getKeyName(keyName, i) {
        try {
            const { cityList, showKeyName } = this.refs;
            cityList.scrollTop = document.getElementById(keyName).offsetTop;
            showKeyName.style.display = 'block';
            showKeyName.innerHTML = keyName;
            clearTimeout(showToastId); // 先移除，避免重复
            showToastId = setTimeout(() => {
                showKeyName.style.display = 'none';
                showKeyName.innerHTML = '';
            }, 300);
        } catch (e) {
            // 可能出错
            console.log('出错了：', e)
        }
    }


    /**
     * 选中城市
     */
    clickCity(data) {
        window.cache_selectCity = data;
        this.back();
    }

    /**
     * 页面后退
     */
    back() {
        // 普通后退操作
        if (sessionStorage.getItem('goBackType') === 'h5Back') {
            return window.history.back();
        }
        // 需要关闭webview
        if (this.props.location.query.closeWebView != '0') {
            jsApi.back();
        } else {
            window.history.back();
        }
    }

    render() {
        const {
            cityList,
            carListKey,
            keyName,
        } = this.state;

        return (
            <div className={Style.car_fiexd100}>
                <div className={Style.car_container}>
                    {/* 品牌列表 */}
                    <div ref='cityList' className={Style.car_left}>
                        {cityList.list.length > 0 ?
                            <ListCars
                                keyNameKey={'level'}
                                listKey={'citys'}
                                titleKey='name'
                                list={cityList.list}
                                onClick={(data) => this.clickCity(data)} />
                            : ''
                        }
                    </div>

                    {/* 字母索引列表 */}
                    {
                        carListKey.list.length > 0 ?
                            <div className={Style.car_right}>
                                <ListKey list={carListKey.list} touchStart={(keyName) => this.getKeyName(keyName)} />
                            </div>
                            : ''
                    }

                    {/* Toast字母 */}
                    <div className={Style.car_showKeyName} ref='showKeyName' style={{ display: 'none' }}></div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    carsActions: bindActionCreators(carsActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SelectCity);