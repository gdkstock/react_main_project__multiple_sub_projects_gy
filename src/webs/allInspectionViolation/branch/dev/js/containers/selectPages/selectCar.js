import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { ListCars, ListKey, Others } from 'app/components/selectCar/'

//antd
import { Toast } from 'antd-mobile'

//servers
import carService from '../../services/carService'

// action
import * as carListActions from '../../actions/carListActions';

//通用类
import common from '../../utils/common'

//样式
import styles from './index.scss'
import jsApi from '../../utils/jsApi';

// 组件变量
let showToastId = -1; // 显示Toast的句柄Id

class SelectCar extends Component {

    constructor(props) {
        super(props)

        const { type } = this.props.location.query;

        this.state = {
            isShowCarBox: false,
            top: 0,
            keyName: '', //当前点击的key

            // 车系列表 第一层
            listCars: { list: [] },

            // 车型列表 第二层
            cars: { list: [] },

            // 第三层
            carsItem: { list: [] },

            // 字母索引列表
            carListKey: { list: [] },

            transmissionType: [],

            engineExhaust: [],
        }
    }

    componentWillMount() {
        common.setViewTitle('选择车型');
    }

    componentDidMount() {
        //获取第一层车型信息
        this.getRecallBrandList();
    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    /**
     * 获取车型列表（第一层）
     */
    getRecallBrandList() {

        const cache = window.api_getRecallBrandList;
        if (cache && cache.data) {
            this.setBrandData(cache)
        } else {
            Toast.loading('', 0);
            carService.getRecallBrandList({}).then(res => {
                Toast.hide();
                this.setBrandData(res);
            });
        }
    }

    /**
     * 保存第一层点击的数据
     * @param {object} res 
     */
    setBrandData(res) {
        if (res.code == '1000' && res.data) {

            window.api_getRecallBrandList = res; // 保存数据

            this.setState({
                listCars: res.data,
                carListKey: {
                    list: res.data.map(item => item.level), // 更新列表
                }
            })
        }
    }


	/**
     * 获取车型列表（第二层）
     */
    getRecallModelList(id) {
        // 对象不存在时 创建对象
        if (!window.api_getRecallModelList) {
            window.api_getRecallModelList = {};
        }

        const cache = window.api_getRecallModelList[id]; // 保存数据
        if (cache) {
            this.setModelData(id, cache)
        } else {
            Toast.loading('', 0);
            carService.getRecallModelList({ 'brandId': id })
                .then(res => {
                    Toast.hide();
                    this.setModelData(id, res)
                });
        }
    }

    /**
     * 保存第二层点击的数据
     * @param {object} res 
     */
    setModelData(id, res) {
        if (res.code == '1000' && res.data) {

            window.api_getRecallModelList[id] = res; // 保存数据

            if (res.data.carModels.length > 0) {
                this.setState({
                    cars: res.data.carModels
                })
            } else {
                // 无数据 直接返回
                this.back();
            }
        }
    }

	/**
     * 获取车型列表（第三层）
     */
    getRecallCarList(id) {

        if (!window.api_getRecallCarList) {
            window.api_getRecallCarList = {}
        }

        const cache = window.api_getRecallCarList[id];

        if (cache) {
            this.setCarData(id, cache)
        } else {
            Toast.loading('', 0);
            carService.getRecallCarList({ 'modelId': id })
                .then(res => {
                    Toast.hide();
                    this.setCarData(id, res);
                });
        }
    }

    /**
     * 保存第三层点击的数据
     * @param {object} res 
     */
    setCarData(id, res) {
        if (res.code == '1000' && res.data && res.data.cars) {

            window.api_getRecallCarList[id] = res; // 保存数据

            this.setState({
                carsItem: res.data.cars,
                transmissionType: res.data.transmissionType,
                engineExhaust: res.data.engineExhaust,
            })
        } else {
            this.back();
        }
    }

    //获取点击的序号
    getKeyName(keyName, i) {
        try {
            const { listCars, showKeyName } = this.refs;
            listCars.scrollTop = document.getElementById(keyName).offsetTop;
            showKeyName.style.display = 'block';
            showKeyName.innerHTML = keyName;
            clearTimeout(showToastId); // 先移除，避免重复
            showToastId = setTimeout(() => {
                showKeyName.style.display = 'none';
                showKeyName.innerHTML = '';
            }, 300);
        } catch (e) {
            // 可能出错
            console.log('出错了：', e)
        }
    }

    //隐藏第二层车型列表
    hideCars() {
        this.refs.Cars.style.display = 'none';
    }

    //隐藏第三层车型列表
    hideCarsItem() {
        //this.refs.CarsItem.style.display = 'none';
    }

    //显示第二层车型列表
    showCars(data) {
        console.log('品牌信息', data);

        const { carId } = this.props.params;
        if (!window.cache_selectCar || !window.cache_selectCar[carId]) window.cache_selectCar = { [carId]: {} };
        window.cache_selectCar[carId].brand = data;

        this.getRecallModelList(data.data.id);
        this.refs.Cars.style.display = 'block';

    }

    //显示第三层车型列表
    showCarsItem(data) {
        console.log('车系信息', data);

        const { carId } = this.props.params;
        window.cache_selectCar[carId].model = data;

        this.getRecallCarList(data.data.modelId);
        this.refs.CarsItem.style.display = 'block';
    }

    //获取车辆信息
    getCar(data) {
        console.log('车辆信息', data);

        const { carId } = this.props.params;
        window.cache_selectCar[carId].car = data;
        this.back();
    }

    /**
     * 页面后退
     */
    back() {
        const { update } = this.props.location.query;

        // 自动更新车辆信息
        if (update) {
            // 初始化选择车型的数据
            this.selectCarInit();
        } else {
            window.history.back();
        }
    }

	/**
     * 初始化选择的车型信息
     */
    selectCarInit() {
        const { carId } = this.props.params;

        // 存在车型信息
        if (window.cache_selectCar && window.cache_selectCar[carId]) {
            const obj = window.cache_selectCar[carId];
            const styleId = obj.car && obj.car.data ? obj.car.data.id : ''; // 车型ID

            // 车型id不存在
            if (!styleId) {
                // 清除临时变量
                delete (window.cache_selectCar[carId]);
                return false;
            }
            const selectCar = Object.assign({}, this.state.selectCar, window.cache_selectCar[carId]); // 合并数据
            const {
                brand: {
                    data: { id: carBrandId, imgUrl: carBrandLogoUrl, name: carBrandName, },
                },
                model: {
                    data: { model: carModelName, imgUrl: styleImgUrl, },
                },
                car: {
                    data: { name: carStyleName },
                },
			} = selectCar;
            // 额外提交的数据
            const other = {
                carBrandId,
                carBrandLogoUrl,
                carBrandName,
                carModelName,
                styleImgUrl,
                carStyleName,
            }

            const { cars } = this.props;
            const { carNumber, carCode, engineNumber } = cars.data[carId]; // 从原数据中获取更新车辆必要的数据

            this.props.actions.queryUpdateCar({
                carId,
                carNumber,
                styleId,
                carCode,
                engineNumber,
                ...other,
            });

            // 清除临时变量
            delete (window.cache_selectCar[carId]);
        } else {
            Toast.info('车型异常，请稍后再试', 1, () => jsApi.back());
        }
    }

    render() {
        const {
            listCars,
            carId,
            carNumber,
            cars,
            carsItem,
            transmissionType,
            engineExhaust,
            carListKey,
            keyName,
        } = this.state;

        return (
            <div className={styles.car_fiexd100}>
                <div className={styles.car_container}>
                    {/* 品牌列表 */}
                    <div ref='listCars' className={styles.car_left}>
                        {listCars.length > 0 ?
                            <ListCars
                                keyNameKey={'level'}
                                listKey={'carBrands'}
                                imgKey='imgUrl'
                                titleKey='name'
                                list={listCars}
                                onClick={(data) => this.showCars(data)} />
                            : ''
                        }
                    </div>

                    {/* 字母索引列表 */}
                    {
                        carListKey.list.length > 0 ?
                            <div className={styles.car_right}>
                                <ListKey list={carListKey.list} touchStart={(keyName) => this.getKeyName(keyName)} />
                            </div>
                            : ''
                    }

                    {/* Toast字母 */}
                    <div className={styles.car_showKeyName} ref='showKeyName' style={{ display: 'none' }}></div>

                    {/* 第二层 */}
                    <div ref='Cars' className={styles.car_left + ' ' + styles.cars} onClick={() => this.hideCars()}>
                        {cars.length > 0 ?
                            <ListCars
                                keyNameKey={'brandType'}
                                listKey={'modelList'}
                                imgKey='imgUrl'
                                titleKey='model'
                                list={cars}
                                onClick={(data) => this.showCarsItem(data)} />
                            : ''
                        }
                    </div>

                    {/* 第三层 */}
                    <div ref='CarsItem' className={styles.car_left + ' ' + styles.carsItem} onClick={() => this.hideCarsItem()}>
                        {carsItem.length > 0 ?
                            <Others
                                keyNameKey={'year'}
                                listKey={'carList'}
                                titleKey='name'
                                list={carsItem}
                                transmissionTypeKey={transmissionType}
                                engineExhaustKey={engineExhaust}
                                onClick={(data) => this.getCar(data)}
                            />
                            : ''
                        }
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cars: state.cars,
    carBrand: state.carBrand
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(carListActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectCar)
