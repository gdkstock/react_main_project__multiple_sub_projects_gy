/**
 * 优惠券列表
 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch } from 'antd-mobile'
import moment from 'moment';

//组件


//action


//server


//style
import Style from './index.scss'

//常用工具类
import common from '../../utils/common'

class Coupon extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isUrgent: false
        }
    }

    componentWillMount() {
        common.setViewTitle('选择优惠券');
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        sessionStorage.setItem("prevPageInfo", document.title)
    }

    /**
     * 选择优惠券
     */
    selectCoupon(couponId) {
        sessionStorage.setItem('couponId', couponId)
        window.history.back();
    }

    render() {
        let { couponIds, couponId } = this.props.params
        let { coupons } = this.props

        return (
            <div className='box'>
                <div className={Style.top} onClick={() => this.selectCoupon('')}>
                    <span>不使用优惠券</span>
                    <img src={!couponId || couponIds.indexOf(couponId) === -1 ? './images/checkBox-true.png' : './images/checkBox-false.png'} />
                </div>
                <div className={Style.subTitle}>有 <span>{couponIds.split(',').length}</span> 张优惠券可用</div>
                <div className={Style.couponList}>
                    {
                        couponIds.split(',').map((item, i) =>
                            <div key={item} className={Style.coupon} onClick={() => this.selectCoupon(item)}>
                                {
                                    coupons.data[item].description
                                        ? <div className={Style.couponMoney}>
                                            <span style={{fontSize:'.44rem'}}>{coupons.data[item].description}</span>
                                        </div>
                                        : <div className={Style.couponMoney}>

                                            <span>{coupons.data[item].couponAmount}</span><i>元</i>
                                        </div>
                                }

                                <div className={Style.couponText}>
                                    <span>{coupons.data[item].couponTitle}</span>
                                    <p>有效期至 {moment(coupons.data[item].invalidDate).format('YYYY年M月DD日')}</p>
                                </div>
                                <img className={Style.couponSelectImg} src={couponId == item ? './images/checkBox-true.png' : './images/checkBox-false.png'} />
                            </div>)
                    }
                </div>
                <div className={Style.footerText}>
                    <p>使用说明： </p>
                    <p>1、只能使用车行易通用优惠券和违章优惠券，其他类型的优惠券不能在本订单中使用； </p>
                    <p>2、每次下单只能使用一张优惠券； </p>
                </div>
            </div>
        );
    }
}

//使用context
Coupon.contextTypes = {
    router: React.PropTypes.object.isRequired
}

Coupon.propTypes = {

};

const mapStateToProps = state => ({
    coupons: state.coupons
})

const mapDispatchToProps = dispatch => ({

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Coupon);