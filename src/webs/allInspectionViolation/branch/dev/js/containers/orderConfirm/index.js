/**
 * 订单确认
 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, Toast } from 'antd-mobile';
import { normalize, schema } from 'normalizr'; //范式化库

//组件
import WAlert from '../../components/WAlert';
import { Notice } from '../../components/notice';

//action
import * as couponActions from '../../actions/couponActions'

//server
import orderService from '../../services/orderService'

//style
import Style from './index.scss'

//常用工具类
import common from '../../utils/common'
import jsApi from '../../utils/jsApi'
import { ChMessage } from '../../utils/message.config'

//配置
import config from '../../config';

// 为了配置钱包测试，在所有环境全部使用收银台的支付方式。（搜索“ && false”即可看到改动的地方），后端也做了对应的修改，切记！切记！
class OrderConfirm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            carId: '',
            carNumber: '',
            isUrgent: false, //加急
            buyVipCard: false, //购买会员
            coupon: {},
            couponId: '', //使用的优惠券ID
            autoSelectCouponId: true, //自动选择优惠券
            showMengCeng: undefined, //显示违章列表蒙层
            showViolationBox: false, //显示违章列表
            orderConfirmDatas: sessionStorage.getItem('orderConfirmDatas') ? JSON.parse(sessionStorage.getItem('orderConfirmDatas')) : {}, //订单试算的结果 用于保存历史结果 减少重复请求服务器的次数
            firstGetData: true, // 首次进入
            showVipAlert: false, //显示VIP弹窗说明
            showUrgentAlert: false, //显示加急弹窗说明
            showPayType: common.isCXYApp(),//显示支付方式选择
            curPayType: localStorage.getItem("payType") || "", //支付方式
        }
    }

    componentWillMount() {
        common.setViewTitle('订单确认');
    }

    componentDidMount() {
        if (sessionStorage.getItem('orderConfirmPage')) {
            //跳转前的数据
            let orderConfirmPage = JSON.parse(sessionStorage.getItem('orderConfirmPage'))
            this.setState(Object.assign({}, orderConfirmPage.state))
            document.body.scrollTop = orderConfirmPage.scrollTop
            sessionStorage.removeItem('orderConfirmPage')
        }
        if (sessionStorage.getItem('couponId') || sessionStorage.getItem('couponId') === '') {
            this.setState({
                couponId: sessionStorage.getItem('couponId')
            })
            sessionStorage.removeItem('couponId')
        }

        setTimeout(() => this.orderConfirm(), 10) //异步请求，确保state已经更新了

        common.sendCxytj({
            eventId: 'Violation_EnterOrderConfirm'
        })

        // 加载支付JSDK
        if (common.isCXYApp()) {
            this.requireCxyPays();
        }
    }

    componentWillUnmount() {
        sessionStorage.setItem("prevPageInfo", document.title)

        this.setState = () => false; // 避免组件被卸载时出错
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    requireCxyPays() {
        const userId = sessionStorage.getItem('userId');
        // 加载支付JS相关的js
        if (!window.cxyPays) {
            // 加载网络请求的js
            common.requireJs(config.cxyPaysJS.reqwest, () => {
                // 加载网络请求的js成功
                // 加载支付JS
                common.requireJs(config.cxyPaysJS.cxyPays, () => {
                    // 加载支付JS成功
                    window.cxyPays.showAppPayList(userId);
                }, () => {
                    // 加载支付JS失败
                    return Toast.info('获取支付信息失败：cxyPays');
                });
            }, () => {
                // 加载网络请求的js失败
                return Toast.info('获取支付请求失败：reqwest');
            });
        } else {
            // 支付JS已经存在
            window.cxyPays.showAppPayList(userId);
        }
    }

    /**
     * 订单确认 || 订单试算
     */
    orderConfirm() {
        let violationIds = this.props.params.violationIds || localStorage.getItem("orderConfirm_violationIds");
        let { violations, cars } = this.props
        let { isUrgent, buyVipCard, couponId, orderConfirmDatas, autoSelectCouponId, firstGetData } = this.state
        let violationIdArr = violationIds.split(',');
        let { carId } = violations.data[violationIdArr[0]]; //直接拿第一条违章的carId

        let postData = {
            carId: carId,
            carNumber: cars.data[carId].carNumber,
            urgentFlag: isUrgent ? '1' : '2', //订单加急标记1：加急2：不加急
            buyVipCard: buyVipCard ? '1' : '2', //订单是否购买会员，1：购买 2：不购买
            couponId: buyVipCard ? '' : couponId, //优惠券ID
            violationInfos: [] //违章信息
        }

        let violationInfo = {}
        violationIdArr.map(violationId => {
            let { backendId, locationId, violationCode, realDegree, fine, cooperPoundage } = violations.data[violationId];
            violationInfo = {
                violationId: violationId,
                backendId: backendId,
                locationId: locationId,
                violationCode: violationCode,
                realDegree: realDegree,
                fine: fine,
                cooperPoundage: cooperPoundage
            }
            postData.violationInfos.push(JSON.stringify(violationInfo))
        })
        postData.violationInfos = '[' + postData.violationInfos.toString() + ']' //转为json字符串

        let postDataStr = JSON.stringify(postData);

        //数据已经存在 并且 非首次进入（首次进入需要自动选择优惠券）
        if (orderConfirmDatas[postDataStr] && !firstGetData) {
            let result = orderConfirmDatas[postDataStr] //直接读取之前的试算数据

            this.setState(Object.assign({}, this.state, result.data, {
                //这里需要key的转换以及转为对应的数据类型
                carId: postData.carId,
                carNumber: postData.carNumber,
                buyVipCard: result.data.vip.isSwitchStatus == '1' ? true : false,
                orderConfirmDatas: Object.assign({}, orderConfirmDatas, {
                    [postDataStr]: result
                }) //保存试算数据
            }))
        } else {
            //从未试算过
            Toast.loading('', 0)
            orderService.confirm(postData).then(result => {
                Toast.hide();
                if (result.code == '1000') {
                    //添加优惠券到全局start
                    this.addCoupons(result.data.coupon);

                    //更新支付方式
                    // this.updatePayType(result.data);

                    //更新状态
                    this.setState(Object.assign({}, this.state, result.data, {
                        //这里需要key的转换以及转为对应的数据类型
                        carId: postData.carId,
                        carNumber: postData.carNumber,
                        buyVipCard: result.data.vip.isSwitchStatus == '1' ? true : false,
                        orderConfirmDatas: Object.assign({}, orderConfirmDatas, {
                            [postDataStr]: result
                        }), //保存试算数据
                        firstGetData: false, // 非首次进入
                    }), () => {
                        //自动选择优惠券
                        let { couponList, couponCount } = result.data.coupon;
                        if (autoSelectCouponId && couponCount > 0) {
                            let autoList = couponList.filter(coupon => coupon.choose === 1);
                            if (autoList[0]) {
                                let autoCouponId = autoList[0].couponId; //默认选中的优惠券id
                                this.autoSelectCouponId(autoCouponId);
                            }
                        }
                        sessionStorage.setItem('orderConfirmDatas', JSON.stringify(this.state.orderConfirmDatas)) //保存试算数据到临时缓存
                    })
                } else if (result.code != "4222" && result.code != "2222") {
                    // 清除缓存数据 避免违章 试算等数据还是旧的
                    this.clearCache();

                    if (result.msg) {
                        Toast.info(result.msg, 3, () => window.history.back()); //试算失败后 页面后退
                    }
                }
            }, error => {
                Toast.hide();
                // 清除缓存数据 避免违章 试算等数据还是旧的
                this.clearCache();
                Toast.info(ChMessage.FETCH_FAILED)
            })
        }
    }

    /**
     * 自动选中优惠券
     * @param couponId 优惠券ID
     */
    autoSelectCouponId(couponId) {
        this.setState({
            couponId,
            autoSelectCouponId: false
        }, () => {
            this.orderConfirm();
        })
    }

    /**
     * 加急开关
     * @param checked 选择状态
     * @param canUrgent 可否加急
     */
    urgentSwitch(checked, canUrgent = true) {
        if (canUrgent == 'true') {
            //可加急
            this.setState({
                isUrgent: canUrgent ? checked : false
            }, () => {
                this.orderConfirm()
            })
        } else {
            //不可加急
            if (this.state.urgent.urgentErrorMsg) {
                Toast.info(this.state.urgent.urgentErrorMsg, 2)
            } else {
                Toast.info('此订单不可加急', 2)
            }

        }
    }

    /**
     * 会员开关
     */
    vipSwitch(checked) {
        this.setState({
            buyVipCard: checked
        }, () => this.orderConfirm())
    }

    /**
     * 跳转到优惠券页面
     */
    toCoupons() {

        common.sendCxytj({
            eventId: 'h5_e_wz_wzdetail_selectcoupon'
        })

        let { couponId } = this.state
        let { coupons } = this.props

        let couponIds = coupons.result.toString();
        this.toUrl(`/coupons/${couponIds}/${couponId}`)
        this.sessionStorage()
    }

    /**
     * 保存优惠券列表到全局state
     */
    addCoupons(coupon) {
        const couponEntity = new schema.Entity('coupon', {}, { idAttribute: 'couponId' });
        const couponArray = new schema.Array(couponEntity)
        const normalizedData = normalize(coupon.couponList, couponArray)
        if (normalizedData.result.length > 0) {
            this.props.couponActions.addCoupons({
                data: normalizedData.entities.coupon,
                result: normalizedData.result
            })
        }
    }

    /**
     * 保存页面状态以及数据
     */
    sessionStorage() {
        let data = {
            state: this.state,
            scrollTop: document.body.scrollTop
        }
        sessionStorage.setItem('orderConfirmPage', JSON.stringify(data))
    }

    /**
     * 获取实际扣分
     */
    getRealDegree(violationIds) {
        let { violations } = this.props
        let realDegree = 0; //实际扣分
        violationIds.split(',').map(violationId => {
            realDegree += violations.data[violationId].realDegree * 1
        })
        return realDegree || 0
    }

    /**
     * 清除缓存
     */
    clearCache() {
        window.myCache_getViolationList = undefined; // 清除违章列表缓存
        window.myCache_fetchGetAd = undefined; // 清除广告
        sessionStorage.removeItem('orderConfirmDatas'); // 清除订单数据
    }

    /**
     * 支付
     */
    pay() {
        let violationIds = this.props.params.violationIds || localStorage.getItem("orderConfirm_violationIds");
        let { violations, cars } = this.props
        let { isUrgent, buyVipCard, couponId, showPayType, curPayType } = this.state
        let violationIdArr = violationIds.split(',');
        let { carId } = violations.data[violationIdArr[0]]; //直接拿第一条违章的carId

        // 提交埋点
        common.sendCxytj({
            eventId: 'Violation_PayOrder'
        })

        // 提交支付方式的埋点
        // if (showPayType) {
        //     const payEventId = this.getPayEventId(curPayType);
        //     if (payEventId) {
        //         common.sendCxytj({
        //             eventId: this.getPayEventId(curPayType)
        //         })
        //     }
        // }

        let postData = {
            carId: carId,
            carNumber: cars.data[carId].carNumber,
            urgentFlag: isUrgent ? '1' : '2', //订单加急标记1：加急2：不加急
            buyVipCard: buyVipCard ? '1' : '2', //订单是否购买会员，1：购买 2：不购买
            couponId: buyVipCard ? '' : couponId, //优惠券ID
            violationInfos: violationIds, //违章信息
            version: 'v1.3', //版本标识符，支付成功后 跳转到支付成功页
        }

        Toast.loading('', 0)
        orderService.pay(postData).then(result => {
            Toast.hide();

            // 清除缓存 避免跳转出去的URL变得不可控
            this.clearCache();

            if (result.code == '1000' && result.data && result.data.resultFlag == '1') {
                // 新版收银台
                this.showPayUI(result.data.result, carId);

            } else {
                Toast.info(result.msg || ChMessage.FETCH_FAILED)
            }
        }, error => {
            Toast.hide()
            Toast.info(ChMessage.FETCH_FAILED)
        })
    }

    /**
     * 显示支付界面
     * @param {object} result 支付接口返回的数据
     * @param {string} carId 车辆Id 用于广播给APP更新违章数据
     */
    showPayUI(result, carId) {
        try {
            const { orderId, pay_id, orderType, pay_url } = result;
            if (common.isCXYApp()) {
                window.cxyPays.appPay({
                    pay_id,
                    orderType,
                    callback: (payRes) => {
                        if (payRes.data && payRes.data.tradeStatus && payRes.data.tradeStatus == "1") {
                            // 跳转到支付成功页面
                            this.props.router.replace(`/orderPaySuccess/${orderId}/${carId}`);
                        } else {
                            // 支付失败
                        }
                    }
                });
            } else {
                // 非APP环境直接跳转到收银台
                window.location.href = pay_url;
            }
        } catch (e) {
            // 出错了，可能是缓存导致的 所以刷新下页面
            Toast.info('支付异常，请稍后再试', 3, () => window.location.reload());
        }
    }

    /**
     * 显示违章概要
     */
    showViolationBox() {
        common.sendCxytj({
            eventId: 'Violation_ViewViolationSummary'
        })
        this.setState({ showViolationBox: false })
        let that = this;
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            that.setState({ showMengCeng: false })
        }, 300);
    }

    /**
     * 支付方式变化
    */
    payTypeChange(payType) {
        let { curPayType } = this.state
        if (curPayType != payType) {
            this.setState({
                curPayType: payType,
            })
        }
    }

    render() {
        let violationIds = this.props.params.violationIds || localStorage.getItem("orderConfirm_violationIds");
        let { carId,
            carNumber,
            orderTitle,
            isUrgent,
            buyVipCard,
            couponId,
            showMengCeng,
            showViolationBox,
            drivingLicense,
            urgent,
            activity,
            coupon,
            vip,
            accountList,
            totalFee,
            discountAmount,
            showVipAlert,
            showUrgentAlert,
            showPayType,
            curPayType,
            payTypes,
            notice, // 提醒信息
        } = this.state
        let { violations, cars, coupons } = this.props
        let realDegree = this.getRealDegree(violationIds) //实际扣分

        return (
            <div className='box'>
                {notice && <Notice title={notice} />}
                {/*违章列表 start*/}
                <div className={showMengCeng ? Style.mengceng : showMengCeng !== undefined ? Style.mengceng_leave : 'hide'}>
                    <div className={showViolationBox ? Style.violationBox : Style.violationBox_leave}>
                        <span className={showViolationBox ? Style.violationTop : 'hide'}>已选择违章</span>
                        <div className={showViolationBox ? Style.violationList : Style.violationList_leave}>
                            {
                                violationIds.split(',').map((item, i) =>
                                    violations.data[item] ? <div key={item} className={Style.violationItem}>
                                        <div className={Style.violationTitle}>{violations.data[item].locationName + violations.data[item].location}</div>
                                        <div className={Style.violationTime}>{violations.data[item].occurTime}</div>
                                        <div className={Style.violationReason}>{violations.data[item].reason}</div>
                                        <div className={Style.violationFine}>
                                            <span>罚款{violations.data[item].fine}元</span>
                                            <span>扣{violations.data[item].degree}分</span>
                                            {violations.data[item].lateFine ? <span>滞纳金{violations.data[item].lateFine}元</span> : ''}
                                            <span>
                                                {
                                                    violations.data[item].poundage == violations.data[item].cooperPoundage
                                                        ? `服务费${violations.data[item].poundage}元`
                                                        : `服务费原价${violations.data[item].poundage}元，活动价${violations.data[item].cooperPoundage}元`
                                                }

                                            </span>
                                        </div>
                                    </div> : ''
                                )
                            }
                        </div>
                        <div className={showViolationBox ? Style.mo : Style.mo_leave}></div>
                        <div className={Style.closeViolation}
                            onClick={() => this.showViolationBox()}
                            style={{ backgroundImage: 'url("./images/icon_close.png")' }}></div>
                    </div>
                </div>
                {/*违章列表 end*/}

                {/*订单标题 start*/}
                <div className={Style.orderTitle} onClick={() => this.setState({ showViolationBox: true, showMengCeng: true })}>
                    <div className='absolute-vertical-center'>
                        <span>{carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}{orderTitle}</span>
                        <p>共办理{violationIds.split(',').length}条，驾驶证扣除{realDegree}分{drivingLicense ? '，扣分驾驶证号' + drivingLicense : ''}</p>
                    </div>
                    <i className={Style.iconRight}></i>
                </div>
                {/*订单标题 end*/}

                {/*会员、加急、优惠券、活动减免 start*/}
                <div className={Style.whiteBoxList}>

                    {/*会员 start*/}
                    {
                        !vip ? '' :
                            <div className={vip.showVip == '1' ? Style.vipBox : 'hide'}>
                                <div className={Style.vipTop}>
                                    <span>开通会员</span>
                                    <i className="icon-prompt" onClick={() => this.setState({ showVipAlert: true })}></i>
                                    <div className={Style.infomation}>违章立享5折，全年免违章服务费</div>
                                </div>
                                <div className={Style.vipSwitchBox}>
                                    <Switch
                                        checked={buyVipCard}
                                        onChange={(checked) => this.vipSwitch(checked)}
                                    />
                                </div>
                            </div>
                    }
                    {/*会员 end*/}

                    {//优惠券
                        !coupon ? '' :
                            !buyVipCard ? coupon.couponCount > 0
                                ? <div className={coupon.showCoupon == '1' ? Style.couponBox : 'hide'} onClick={() => this.toCoupons()}>
                                    <div className={Style.couponTitle}>
                                        <span>优惠券</span>
                                    </div>
                                    <div className={Style.couponSelect}>
                                        {
                                            couponId && coupons.data[couponId]
                                                ? <span className={Style.couponMoney}>{(coupons.data[couponId].description || coupons.data[couponId].couponAmount + '元') + coupons.data[couponId].couponTitle}</span>
                                                : <span className={Style.couponNum}>{coupon.couponCount}张可用</span>
                                        }
                                        <i className={Style.iconRight}></i>
                                    </div>
                                </div>
                                : <div className={coupon.showCoupon == '1' ? Style.couponBox : 'hide'}>
                                    <div className={Style.couponTitle}>
                                        <span>优惠券</span>
                                    </div>
                                    <div className={Style.noCoupons}>
                                        <span>暂无可用优惠券</span>
                                    </div>
                                </div>
                                :
                                <div className={coupon.showCoupon == '1' ? Style.couponBox + ' ' + Style.notSelectCoupon : 'hide'}>
                                    <div className={Style.couponTitle}>
                                        <span>优惠券</span><i>不与会员权益同时使用</i>
                                    </div>
                                    {
                                        coupon.couponCount > 0
                                            ? <div className={Style.couponSelect}>
                                                <span className={Style.couponNum}>{coupon.couponCount}张可用</span>
                                                <i className={Style.iconRight}></i>
                                            </div>
                                            : <div className={Style.noCoupons}>
                                                <span>暂无可用优惠券</span>
                                            </div>
                                    }
                                </div>
                    }
                    <div className="line"></div>

                    { //加急
                        !urgent ? '' :
                            <div className={urgent.showUrgent == '1' ? Style.urgentBox : 'hide'}>
                                <div className={Style.urgentTitle}>
                                    <div className={Style.urgentTitleText}>
                                        <span>{urgent.title}{urgent.describe ? `(${urgent.describe})` : ''}</span>
                                        <i className="icon-prompt"
                                            style={{ position: 'absolute', top: '-4px' }}
                                            onClick={() => this.setState({ showUrgentAlert: true })}></i>
                                    </div>
                                </div>
                                <div className={Style.urgentSwitchBox}>
                                    <Switch
                                        checked={isUrgent}
                                        onChange={(checked) => this.urgentSwitch(checked, urgent.canUrgent)}
                                    />
                                </div>
                            </div>
                    }

                    { //活动
                        !activity ? '' :
                            <div className={activity.showActivity == '1' ? Style.activityBox : 'hide'}>
                                <span>{activity.title || "活动减免"}</span>
                                <i>{activity.subTitle}</i>
                            </div>
                    }

                </div>
                {/*加急、活动减免、优惠券 end*/}

                {/*订单详情 start*/}
                <div className={Style.orderInfo}>
                    {
                        accountList && accountList.map((item, i) => item && item.name && item.account
                            ? <p key={'account' + i}><span>{item.name}</span><i>{item.account < 0 ? `- ¥${item.account * -1}` : `¥${item.account}`}</i></p>
                            : ''
                        )
                    }
                </div>
                {/*订单详情 end*/}

                {/*支付方式选择 start*/}
                {showPayType && <div className="h24"></div>}
                {showPayType && <div id='cxyPayList'></div>}
                {/*支付方式选择 end*/}

                {/*合计和支付按钮 start*/}
                <div style={{ height: '1.4rem' }}></div>
                <div className={Style.footerBox}>
                    <div className={Style.footerText}>
                        <span>合计￥{totalFee}</span><i className={discountAmount > 0 ? '' : 'hide'}>(已优惠￥{discountAmount})</i>
                    </div>
                    <div className={Style.footerBtn} onClick={() => this.pay()}>去支付</div>
                </div>
                {/*合计和支付按钮 end*/}

                {/*说明弹窗 start*/}
                {showVipAlert ? <WAlert num={0} onClose={() => this.setState({ showVipAlert: false })} /> : ''}
                {showUrgentAlert ? <WAlert num={1} onClose={() => this.setState({ showUrgentAlert: false })} /> : ''}
                {/*说明弹窗 end*/}
            </div >
        );
    }
}

//使用context
OrderConfirm.contextTypes = {
    router: React.PropTypes.object.isRequired
}

OrderConfirm.propTypes = {

};

const mapStateToProps = state => ({
    cars: state.cars,
    violations: state.violations,
    coupons: state.coupons
})

const mapDispatchToProps = dispatch => ({
    couponActions: bindActionCreators(couponActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderConfirm);