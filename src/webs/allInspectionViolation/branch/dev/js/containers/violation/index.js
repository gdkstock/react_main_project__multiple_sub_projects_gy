/**
 * 违章列表
 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Tabs, Toast } from 'antd-mobile'

//组件
import { Card, EmptyViolation } from '../../components/violation'
import Comfirm from '../../components/common/Confirm'
import Modal from '../../components/common/Modal'
import WAlert from '../../components/WAlert'
import { Notice } from '../../components/notice'

const TabPane = Tabs.TabPane;

//action
import * as violationActions from '../../actions/violationActions'
import * as carActions from '../../actions/carListActions'
import * as couponActions from '../../actions/couponActions'

//style
import '../../../styles/violation.scss'
import Style from './index.scss'

//jsdk
import jsApi from '../../utils/jsApi';

//常用工具类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config'
import config from '../../config'

//资源
const bannerSrc = './images/violationList-banner.png'

//变量
var checkedInit = true; //初始化默认选择

class ViolationIndex extends Component {
    constructor(props) {
        super(props);

        const getViolationStatus = () => {
            let violationStatus = '0';
            switch (sessionStorage.getItem("violationTabKey")) {
                case 'tabsTitle0': violationStatus = 'all'; break;
                case 'tabsTitle1': violationStatus = '0'; break;
                case 'tabsTitle2': violationStatus = '2'; break;
                case 'tabsTitle3': violationStatus = '1'; break;
            }
            return violationStatus;
        }
        this.state = {
            isLoading: false, //加载中
            tabsTitle: ['全部', '未办理', '办理中', '已完成'],
            tabsKey: '1', //当前tabs的key
            checkeds: [], //已选违章ID列表
            chooses: [], //必选违章ID列表
            checkAlled: false, //全选
            total: {
                totalNum: 0, //可办理违章总数
                totalDegree: 0, //可办理总扣分
                totalFine: 0, //可办理总罚款
                totalCheckeds: [], //可办理违章列表
                checkedNum: 0, //勾选违章总数
                checkedDegree: 0, //勾选总扣分
                checkedFine: 0, //勾选总罚款
                isOk: false //已经计算过
            },
            confirmInfo: {
                show: false, //显示
                content: '根据交通管理局的在线违章处理要求，您需要补充本车相关信息才能办理，是否继续办理？',
                leftBtn: {
                    name: '取消办理',
                    onClick: () => this.hideConfirm()
                },
                rightBtn: {
                    name: '继续办理',
                    onClick: () => this.confirmRightBtn()
                },
                onClose: () => this.hideConfirm()
            },
            showFooter: sessionStorage.getItem("violationTabKey") == 'tabsTitle2' || sessionStorage.getItem("violationTabKey") == 'tabsTitle3' ? false : true, //办理中和已完成不显示底部按钮
            violationStatus: getViolationStatus(), //违章的处理状态 -1：不可办理；0：待处理；1：已处理；2：处理中；all:所有
            ready: false, //已加载完毕
            isShowHistory: false,//是否展示历史违章
            showRealDegreeAlert: false, //显示扣分不执行弹窗说明
            getAdIsReady: false, // 获取广告成功 默认两秒后成功
            hasCoupon: 0, // 存在可用优惠券
        }
    }

    componentWillMount() {
        // common.setViewTitle('违章列表');
    }
    componentWillUpdate() {
        let { carId } = this.props.params
        const { cars } = this.props
        if (cars && cars.data && cars.data[carId] && cars.data[carId].carNumber) {
            common.setViewTitle(cars.data[carId].carNumber);
        }
    }

    componentDidUpdate() {
        setTimeout(() => {
            const { getAdIsReady } = this.state;
            if (!getAdIsReady) {
                // 避免重复渲染 所以这里先判断
                this.setState({
                    getAdIsReady: true
                });
            }
        }, 2000);
    }

    componentDidMount() {
        //进入违章列表
        common.sendCxytj({
            eventId: 'Violation_EnterViolationList',
        })

        if (common.isCXYApp() && !sessionStorage.getItem('city')) {
            // app环境需要在获取城市后再请求违章列表
            jsApi.getSymbol(res => {
                sessionStorage.setItem('city', res.data.city || '');
                this.getDataFuns();
                return true;
            })
        } else {
            this.getDataFuns();
        }


    }

    componentWillUnmount() {
        sessionStorage.setItem("prevPageInfo", document.title);
        this.setState = () => false;
    }

    /**
     * 支付宝城市服务
     */
    isAlipayCarService() {
        const userType = sessionStorage.getItem('userType') || '';
        return userType.toLocaleLowerCase() === 'alipaycarservice';
    }

    /**
     * 跳转到我的订单
     */
    toMyOrder() {
        window.location.href = config.myOrderUrl;
    }

    getDataFuns() {
        const { carId } = this.props.params
        const { cars } = this.props

        if (cars && cars.data && cars.data[carId] && cars.data[carId].carNumber) {
            //存在全局state || 缓存
            this.setState({
                ready: true
            })

            this.getData() //获取数据
        } else {
            //不存在全局state和缓存
            this.props.violationActions.getCarsViolations({
                carId: carId
            }, result => {
                if (result) {
                    this.setState({
                        ready: true
                    })

                    this.showData(result, carId);
                } else {
                    Toast.info(ChMessage.FETCH_FAILED, 1, () => {
                        window.location.replace(config.indexPageUrl) //重定向到首页
                    })
                }
            })
        }

        // 获取可用优惠券信息
        this.props.couponActions.usefulCouponAsync({ carId }, res => {
            if (res.code == 1000) {
                this.setState({
                    hasCoupon: 1,
                })
            }
        })
    }

    //动画
    animation() {
        //设置文字滚动效果
        let textContainer = document.getElementById("pmdContainer");
        let textContent = document.getElementById("pmdContent");
        if (textContainer && textContent) {
            let totalTime = textContent.clientWidth / 40 //动画总时长
            if (textContent.clientWidth > textContainer.clientWidth) {
                textContent.innerHTML = textContent.innerHTML + "。 " + textContent.innerHTML + "。 ";
                textContent.className = "text";
                textContent.style.animation = `txtAnimation ${totalTime}s linear infinite`
            }
        }
    }

    //无违章引导跳转链接
    noViolationGuidance(item) {
        if (item.targetUrl) {
            window.location.href = item.targetUrl;
        }
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    //扣分单跳转到办理
    toDeal(url) {
        common.sendCxytj({
            eventId: 'h5_e_wz_wzlist_xc_surenow',
        })
        let token = sessionStorage.getItem("token")
        //app渠道解决未设置userType的cookie导致违章办理不了的问题
        if (common.isCXYApp()) {
            url += "&userType=app_cxy&token=" + token;
        }
        if (url) window.location.href = url;
    }

    jumptoUrl(url) {
        if (url.indexOf("http") > -1 || url.indexOf("https") > -1) window.location.href = url;
    }

    //跳转到去咨询办理
    toConsult(url) {
        common.sendCxytj({
            eventId: 'h5_e_wz_wzlist_shopwork',
        })
        if (url) window.location.href = url;
    }

    /**
     * 获取数据
     * @param result 违章列表数据
     */
    getData(result = '') {
        let { carId } = this.props.params
        const { violations, violationActions } = this.props

        let showLoading = true; //显示加载中

        //判断缓存中是否有数据
        for (let i = 0; i < violations.result.length; i++) {
            let violationId = violations.result[i];
            if (violations.data[violationId].carId == carId) {
                showLoading = false;
                break;
            }
        }

        this.setState({
            isLoading: showLoading
        }, () => {
            //请求数据
            let postData = {
                type: '3', //全部
                carId: carId
            }

            violationActions.getViolationListAsync(postData, true, (result) => {
                this.showData(result, carId);
            }); //请求数据

        })
    }

    /**
     * 显示接口返回的数据内容
     * @param {object} result 接口返回的数据
     * @param {string} carId 车辆id 用于做缓存对象的key值
     */
    showData(result, carId) {
        if (result.code == '1000') {
            //接口请求成功
            checkedInit = true;
            this.setState({
                total: Object.assign({}, this.state.total, {
                    isOk: false //设置为未计算过
                })
            }, () => {
                let violations = this.getList()
                //判断是否有未处理的违章，根据结果显示无违章引导文案
                if (violations["0"].length == 0 && violations["99"].length == 0 && violations["-1"].length == 0) {
                    this.props.carActions.getAd({ positionCode: "WZ_CAR_GUIDE_DOCUMENT" }, (result) => {
                        this.setState({
                            noViolation: result.data.adList.length > 0 ? result.data.adList[0] : undefined,
                            getAdIsReady: true, // 获取广告成功
                        })
                    })
                    //无违章埋点
                    common.sendCxytj({
                        eventId: 'h5_p_wz_wzlist_checkwznull',
                    })
                }
            })
            this.props.carActions.getAd({ positionCode: "WZ_CAR_VIOLATION_LIST" }, (result) => {
                this.setState({
                    brodcast: result.data.adList.length > 0 ? result.data.adList[0] : undefined,
                }, () => {
                    this.animation()
                })
            })
        }
        if (this.state.isLoading) {
            this.setState({
                isLoading: false
            })
        }
    }

    tabsCallback(key, violations) {
        sessionStorage.setItem("violationTabKey", key)
        let showFooter = true, violationStatus = -1;
        switch (key) {
            case 'tabsTitle0': //全部
                violationStatus = 'all'
                showFooter = violations['all'] && violations['all'].length > 0;
                break;
            case 'tabsTitle1': //未办理
                violationStatus = '0'
                showFooter = (violations['0'] && violations['0'].length > 0) || (violations['-1'] && violations['-1'].length > 0);
                break;
            case 'tabsTitle2': //办理中
                violationStatus = '2'
                showFooter = false;
                break;
            case 'tabsTitle3': //已完成
                violationStatus = '1'
                showFooter = false;
                break;
        }

        this.setState({
            showFooter: !!showFooter,
            tabsKey: key,
            violationStatus: violationStatus //违章的处理状态
        })

    }

    handleTabClick(key) {
    }

    /**
     * 卡片滑动监听 （禁止最左和最右的卡片继续滑动）
     */
    tabsBoxTouchMove() {
        let translateX = 0;
        try {
            let dom = document.getElementsByClassName('am-tabs-content-animated')[0]
            translateX = dom.style.transform.match(/translateX\((-?\d+)/i)[1];
            if (translateX > 0) {
                //最左边的tab不允许再往右滑动
                dom.style.transform = `translateX(0) translateZ(0)`
            } else if (Math.abs(translateX) > 300) {
                //最右边的tab不徐云再往左滑动
                dom.style.transform = `translateX(-300%) translateZ(0)`
            }
        } catch (error) {
            //样式不存在
        }
    }

    /**
     * 获取违章列表
     */
    getList() {
        let { violations } = this.props
        let { carId } = this.props.params
        let violation = {};
        let statusViolations = {
            '-1': [], //不可办理
            '0': [], //待处理
            '1': [], //已处理
            '2': [], //处理中
            "99": [], //现场单
            'all': [], //所有
        } //违章列表、 根据status来做下标

        let checkeds = []; //勾选
        let chooses = []; //必选违章ID列表
        let needAddInfo = []; // 需要补充资料的ID列表

        if (violations.result.length > 0) {
            violations.result.map(i => {
                if (violations.data[i].carId == carId) {
                    violation = this.transformViolationList(violations.data[i])
                    statusViolations['all'].push(violation) //将转换后的数据保存进list数据

                    if (!statusViolations[violation.violationStatus]) {
                        statusViolations[violation.violationStatus] = []; //下标不存在时自动创建
                    }


                    if (violation.canProcess == '0' && violation.violationStatus == '0') {
                        //如果是现场单统一归类到99中
                        if (violation.isLocale == "1") {
                            statusViolations["99"].push(violation)//现场单
                        } else {
                            statusViolations['-1'].push(violation) //不可办理
                        }
                    } else {
                        statusViolations[violation.violationStatus].push(violation); //对应状态的违章列表

                        if (checkedInit) { //初始化默认选中可办理的违章
                            if (violation.violationStatus == '0' && violation.canProcess == '1') {
                                //可代办的未办理违章
                                if (violation.choose == '1' || violation.extendData) {
                                    //必选 || （待处理违章 && 可办理 && 已补充所需资料）
                                    checkeds.push(violation.violationId)
                                    if (violation.choose == '1') {
                                        chooses.push(violation.violationId) //必选违章ID
                                    }
                                }
                                if (!violation.extendData) {
                                    needAddInfo.push(violation.violationId)
                                }
                            }
                        }
                    }
                }
            })

            if (checkedInit) { //初始化默认选中可办理的违章
                checkedInit = false; //不再重复初始化
                setTimeout(() => {
                    this.setState({
                        checkeds: Array.from(new Set([...checkeds, ...chooses])),
                        chooses: chooses
                    }, () => {
                        let { checkeds, total } = this.state;
                        checkeds.map(item => {
                            this.getTotal(item)
                        })
                    })
                }, 10)
            }
        }

        //如果没有不需补充资料的可办理违章，则选中需要补充资料违章
        if (checkeds.length == 0 && needAddInfo.length > 0) {
            checkeds = Array.from(new Set([...checkeds, ...needAddInfo]))
        }

        return statusViolations
    }

    /**
     * 数据格式转换 - 列表
     */
    transformViolationList(data) {
        let { cars } = this.props
        let { carId, violationId, locationName, location, degree, canProcessMsg, proxyRule, violationStatus, isLocale, forwardUrl } = data
        let isNew = violationStatus == '0' //待处理的违章才显示new
        let readViolationIds = common.getUnreadList();
        if (cars.data[carId] && readViolationIds) {
            if (readViolationIds.indexOf(violationId) !== -1) {
                isNew = false
            }
        }

        return Object.assign({}, data, {
            clickCheckbox: () => this.selectViolation(violationId), //点击了勾选框
            clickRight: () => this.isAlipayCarService() ? false : this.toUrl(`/violationDetail/${violationId}`), //点击了右边区域
            clickFooterText: () => this.isAlipayCarService() ? false : this.toUrl(`/violationDetail/${violationId}`), //点击了提示信息区域
            clickDealBtn: () => this.toDeal(forwardUrl),//跳转到罚款代缴
            clickConsult: (url) => this.toConsult(url),//跳转商家咨询
            clickRealDegree: () => this.setState({ showRealDegreeAlert: true }), // 点击扣分不执行说明icon
            extendData: !proxyRule || Object.keys(proxyRule).length == 0, //已补充资料
            isNew: isNew,
            title: locationName + location, //标题
            footerText: canProcessMsg, //底部提示文案
            checked: false
        })
    }

    /**
     * 获取车辆信息
     */
    getCarInfo() {
        let { cars } = this.props;
        let { carId } = this.props.params;

        // 默认对象
        const obj = {
            carId,
            carNumber: '',
        }
        try {
            return cars.data[carId] || obj;
        } catch (e) {
            return obj
        }
    }

    /**
     * 选中|取消某一条
     */
    selectViolation(violationId) {
        let { checkeds, chooses } = this.state
        let { violations } = this.props
        if (violations.data[violationId].choose == '1' && checkeds.indexOf(violationId) !== -1) {
            return; //必选违章不允许取消
        }
        checkeds = checkeds.indexOf(violationId) === -1 ? [...checkeds, violationId] : checkeds.filter(item => item != violationId);

        this.setState({
            checkeds: Array.from(new Set([...checkeds, ...chooses]))
        }, () => this.getTotal(violationId))
    }

    /**
     * 获取总数
     * @param {*string} violationId 违章id 不传时 为全选
     */
    getTotal(violationId = '') {
        let { carId } = this.props.params
        let { violations } = this.props
        let { total, checkeds, chooses, checkAlled } = this.state

        let checkedNum = 0, checkedDegree = 0, checkedFine = 0; //勾选违章总数、勾选总扣分、勾选总罚款
        if (!total.isOk) { //未计算过总数
            let newTotal = {
                isOk: true, //已经计算
                totalNum: 0, //可办理违章总数
                totalDegree: 0, //可办理总扣分
                totalFine: 0, //可办理总罚款
                totalCheckeds: [], //可办理违章id列表
                checkedNum: 0, //勾选违章总数
                checkedDegree: 0, //勾选总扣分
                checkedFine: 0 //勾选总罚款
            }

            if (violations.result.length > 0) {
                let violation = {};
                for (let i in violations.data) {
                    if (violations.data[i].carId == carId && violations.data[i].canProcess != '0' && violations.data[i].violationStatus == '0') {
                        violation = violations.data[i]
                        newTotal.totalNum++;
                        newTotal.totalDegree += violation.degree * 1
                        newTotal.totalFine += violation.fine * 1
                        newTotal.totalCheckeds.push(violations.data[i].violationId)

                        if (violationId) {
                            //单选
                            if (checkeds.indexOf(violations.data[i].violationId) !== -1) {
                                //已选
                                newTotal.checkedNum++;
                                newTotal.checkedDegree += violation.degree * 1
                                newTotal.checkedFine += violation.fine * 1
                            }
                        } else {
                            //全选
                            newTotal.checkedNum++;
                            newTotal.checkedDegree += violation.degree * 1
                            newTotal.checkedFine += violation.fine * 1
                        }
                    }
                }

                if (violationId) {
                    //单选
                    this.setState({
                        total: Object.assign({}, total, newTotal),
                        checkAlled: newTotal.totalNum === newTotal.checkedNum
                    })
                } else {
                    //全选
                    this.setState({
                        total: Object.assign({}, total, newTotal),
                        checkAlled: !checkAlled,
                        // checkeds: newTotal.totalCheckeds
                        checkeds: Array.from(new Set([...newTotal.totalCheckeds, ...chooses]))
                    })
                }

            }

        } else {
            //已经计算过总数
            if (violationId) {
                //单选
                let isAdd = 1;
                if (checkeds.indexOf(violationId) !== -1) {
                    //已选
                    isAdd = 1;
                } else {
                    //反选
                    isAdd = -1;
                }
                checkedNum = total.checkedNum + isAdd; //勾选违章总数
                if (checkedNum !== checkeds.length) {
                    //长度不一致，说明有必选的违章，这里需要遍历所有已选违章，拿到正确的勾选信息
                    checkedNum = checkeds.length
                    checkeds.map(item => {
                        checkedDegree += violations.data[item].degree * isAdd
                        checkedFine += violations.data[item].fine * isAdd
                    })
                } else {
                    checkedDegree = total.checkedDegree + (violations.data[violationId].degree * isAdd); //勾选总扣分
                    checkedFine = total.checkedFine + (violations.data[violationId].fine * isAdd) //勾选总罚款
                }

                this.setState({
                    total: Object.assign({}, total, {
                        checkedNum: checkedNum,
                        checkedDegree: checkedDegree,
                        checkedFine: checkedFine
                    }),
                    checkAlled: checkedNum === total.totalNum && total.totalNum !== 0
                })

            } else {
                //全选
                if (!checkAlled) {
                    //勾选
                    checkedNum = total.totalNum //勾选违章总数
                    checkedDegree = total.totalDegree //勾选总扣分
                    checkedFine = total.totalFine //勾选总罚款

                } else {
                    //取消勾选
                    checkedNum = 0 //勾选违章总数
                    checkedDegree = 0 //勾选总扣分
                    checkedFine = 0 //勾选总罚款
                }

                this.setState({
                    total: Object.assign({}, total, {
                        checkedNum: checkedNum, //勾选违章总数
                        checkedDegree: checkedDegree, //勾选总扣分
                        checkedFine: checkedFine //勾选总罚款
                    }),
                    checkAlled: !checkAlled,
                    // checkeds: !checkAlled ? total.totalCheckeds : [] //已勾选列表
                    checkeds: !checkAlled ? Array.from(new Set([...total.totalCheckeds, ...chooses])) : [] //已勾选列表
                })
            }
        }

    }

    /**
     * 点击全选按钮
     */
    selectAll() {
        common.sendCxytj({
            eventId: 'Violation_SelectAll',
        })
        this.getTotal();
    }

    /**
     * 点击补充资料-单条
     */
    clickCardFooterText(violationId) {
        let { violations } = this.props
        this.toUpdateCarRequirement(violations.data[violationId].proxyRule, violationId);
    }

    /**
     * confirm继续办理按钮
     */
    confirmRightBtn() {
        // this.hideConfirm()
        common.sendCxytj({
            eventId: 'Violation_Popup_Continue',
        })
        this.toUpdateCarRequirement(this.getProxyRule());
    }

    /**
     * 跳转到补充资料页面
     * @param proxyRule
     * @param violationId 违章ID，存在时，优先使用
     */
    toUpdateCarRequirement(proxyRule, violationId = '') {

        let { checkeds } = this.state
        let { carId } = this.props.params
        let search = '';
        for (let i in proxyRule) {
            search += '&' + i + '=' + proxyRule[i]
        }
        search = '?' + search.substr(1)
        let violationIds = violationId || checkeds.toString() //判断单条违章还是多条违章
        setTimeout(() => {
            this.toUrl(`/updateCarRequirement/${carId}/${violationIds}${search}`)
        }, 0) //不知道为什么异步跳转才不弹出错误信息
    }

    /**
     * 跳转到补充资料页面--保存后返回来源页面
     */
    toUpdateCarRequirement2() {
        common.sendCxytj({
            eventId: 'Violation_AddHandingData_List',
        })

        let { carId } = this.props.params
        let { originalRule, violationIds } = this.getOriginalRuleAndViolationIds()
        let search = '';
        for (let i in originalRule) {
            search += '&' + i + '=' + originalRule[i]
        }
        search = '?' + search.substr(1);

        this.props.violationActions.getRequirementAsync(
            { carId: carId },
            () => this.toUrl(`/updateCarRequirement/${carId}${search}`)
        ) //获取已经补充过的资料 && 跳转到补充资料页面
    }

    /**
     * 跳转到订单确认页面
     */
    toOrderConfirm() {
        let { checkeds } = this.state
        // this.toUrl(`/orderConfirm/${checkeds.toString()}`)
        localStorage.setItem("orderConfirm_violationIds", checkeds.toString());//缓存下单界面的违章数据
        this.toUrl(`/orderConfirm`)
    }

    /**
     * 隐藏confirm确认对话框
     */
    hideConfirm() {
        let { confirmInfo } = this.state
        common.sendCxytj({
            eventId: 'Violation_Popup_Cancel',
        })
        this.setState({
            confirmInfo: Object.assign({}, confirmInfo, {
                show: false
            })
        })

    }

    /**
     * 显示confirm确认对话框
     * @param {object} props 确认对话框的props值
     */
    showConfirm(props = {}) {
        let { confirmInfo } = this.state
        let proxyRule = this.getProxyRule();
        common.sendCxytj({
            eventId: 'h5_e_wz_wzlist_surenow',
        })
        if (Object.keys(proxyRule).length > 0) {
            common.sendCxytj({
                eventId: 'Violation_HandletheViolation_Popup',
            })
            // props.show = true //显示确认框
            // this.setState({
            //     confirmInfo: Object.assign({}, confirmInfo, props)
            // })
            this.toUpdateCarRequirement(this.getProxyRule()); //不显示确认框，直接跳转到补充资料页面
            return;
        }

        common.sendCxytj({
            eventId: 'Violation_HandletheViolation_OrderConfirm',
        })
        this.toOrderConfirm()
    }

    /**
     * 获取需要补充的资料
     */
    getProxyRule() {
        let { violations } = this.props
        let { checkeds } = this.state
        let proxyRule = {}
        checkeds.map(violationId => {
            proxyRule = Object.assign(proxyRule, violations.data[violationId].proxyRule)
        })
        return proxyRule
    }

    /**
     * 获取代办所需资料和违章IDs
     */
    getOriginalRuleAndViolationIds() {
        let { violations } = this.props
        let { total } = this.state
        let carInfo = this.getCarInfo()
        let carId = carInfo.carId

        let originalRule = {}
        let violationIds = []

        if (total.totalNum > 0) {
            //已经获取过违章的总数以及拿到可办理的违章列表
            total.totalCheckeds.map(violationId => {
                originalRule = Object.assign(originalRule, violations.data[violationId].originalRule)
            })
            violationIds = total.totalCheckeds
        } else {
            //未获取过违章的总数
            violations.result.map(violationId => {
                if (violations.data[violationId].carId == carId && violations.data[violationId].canProcess != '0' && violations.data[violationId].violationStatus == '0') {
                    originalRule = Object.assign(originalRule, violations.data[violationId].originalRule);
                    violationIds.push(violationId); //保存可办理的违章ID
                }
            })
        }

        return {
            originalRule: originalRule,
            violationIds: violationIds
        }
    }

    /**
     * 渲染违章列表
     * @param {*object} violations 根据status来排序的违章列表
     * @param {*string} key 当前tabs的key
     */
    showViolations(violations, key) {
        let { checkeds, noViolation, brodcast, getAdIsReady } = this.state
        let index = 0; //默认展示未办理违章
        let texts = {
            '0': '该车辆暂无违章',
            '1': '恭喜你，没有未办理违章',
            '2': '该车辆暂无办理中违章',
            '3': '该车辆暂无已办完违章'
        }

        let carInfo = this.getCarInfo(); //车辆信息
        let carNumber = carInfo.carNumber.substr(0, 2) + ' ' + carInfo.carNumber.substr(2)
        let showUpdateBox = this.isShowupdateBox();

        let { untreatedNum, totalDegree, totalFine } = this.getBreif(violations)// 获取违章概要信息

        if (key == '0') {
            if (violations['all'].length > 0) {
                //有违章
                return (
                    <div>
                        {/*占位图片 start*/}
                        {/*<div style={{ lineHeight: '0' }}>
                            <img src={bannerSrc} style={{ width: '100%', height: 'auto' }} />
                        </div>*/}
                        {/*占位图片 end*/}
                        {/*车牌和修改资料 start*/}
                        <div className={Style.tabsTop}>
                            <span className={Style.carNumBox}>{carNumber}</span>
                            <span className={showUpdateBox ? Style.updateBox : 'hide'} onClick={() => this.toUpdateCarRequirement2()}>资料</span>
                        </div>
                        {/*车牌和修改资料 end*/}
                        {/*未办理*/}
                        {violations['0'] && violations['0'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                        {violations['99'] && violations['99'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                        {violations['0'] && violations['0'].length > 0 || (violations['99'] && violations['99'].length > 0) ? <div style={{ height: '.4rem' }}></div> : ''}
                        {/*办理中*/}
                        {violations['2'] && violations['2'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                        {violations['2'] && violations['2'].length > 0 ? <div style={{ height: '.4rem' }}></div> : ''}
                        {/*已完毕*/}
                        {violations['1'] && violations['1'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                        {violations['1'] && violations['1'].length > 0 ? <div style={{ height: '.4rem' }}></div> : ''}
                        {/*不可办理*/}
                        {violations['-1'] && violations['-1'].length > 0 ? <div className={Style.listSubTitle}>以下违章不可代办，请自行前往车管所办理</div> : ''}
                        {violations['-1'] && violations['-1'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                        {violations['-1'] && violations['-1'].length > 0 ? <div style={{ height: '.4rem' }}></div> : ''}
                    </div>
                )
            } else {
                //当前无任何违章
                return <EmptyViolation text={texts[key]} />
            }

        } else {
            //未办理、办理中、已完毕
            switch (key + '') {
                case '1': index = 0; break; //未办理
                case '2': index = 2; break; //办理中
                case '3': index = 1; break; //已完毕
            }

            if (index === 0) {
                //未办理
                if ((violations['0'] && Object.keys(violations['0']).length > 0) || (violations['99'] && Object.keys(violations['99']).length > 0) || (violations['-1'] && Object.keys(violations['-1']).length > 0)) {

                    return (
                        <div>
                            {/*车牌和修改资料 start*/}
                            {
                                brodcast && <div className={Style.brodcastContainer} onClick={() => this.jumptoUrl(brodcast.targetUrl)}>
                                    <div className={Style.brodcastIcon}></div>
                                    <div className={Style.textView} id="pmdContainer">
                                        <div className={Style.textWrap}>
                                            <div id="pmdContent">{brodcast.title}</div>
                                        </div>
                                    </div>
                                </div>
                            }
                            {
                                this.isAlipayCarService()
                                    ?
                                    <div className={Style.violationBreif}>
                                        <span className={showUpdateBox ? Style.updateBox : 'hide'} onClick={() => this.toUpdateCarRequirement2()}>资料</span>
                                        <span className={Style.myOrder} onClick={() => this.toMyOrder()}>我的订单</span></div>
                                    :
                                    <div className={Style.violationBreif}>
                                        待处理{untreatedNum}条<i></i>罚款{totalFine}元<i></i>扣{totalDegree}分
                                        <span className={showUpdateBox ? Style.updateBox : 'hide'} onClick={() => this.toUpdateCarRequirement2()}>资料</span>
                                    </div>
                            }
                            {/*车牌和修改资料 end*/}
                            {/*未办理*/}
                            {violations['0'] && violations['0'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                            {violations['99'] && violations['99'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                            {/*不可办理*/}
                            {((violations['0'] && violations['0'].length > 0) || (violations['99'] && violations['99'].length > 0)) && (violations['-1'] && violations['-1'].length > 0) ? <div style={{ height: '.4rem' }}></div> : ''}
                            {violations['-1'] && violations['-1'].length > 0 ? <div className={Style.listSubTitle}>以下违章不可代办，请自行前往车管所办理</div> : ''}
                            {violations['-1'] && violations['-1'].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                        </div>
                    )
                } else {
                    return (
                        <div style={{ width: "100%", height: "5.58rem" }}>
                            {
                                this.isAlipayCarService()
                                    ?
                                    <div className={Style.violationBreif} style={{ background: '#f1f1f1' }}>
                                        <span className={Style.myOrder} onClick={() => this.toMyOrder()}>我的订单</span>
                                    </div>
                                    :
                                    ''
                            }
                            {
                                noViolation && noViolation.targetUrl
                                    ? // 存在广告
                                    <iframe style={{ width: "100%", height: "5.58rem" }} src={noViolation.targetUrl}></iframe>
                                    : // 不存在广告
                                    <div className={Style.noViolationWrap}>
                                        {
                                            getAdIsReady ?
                                                <div>
                                                    <img src='./images/bitmap.png' />
                                                    <p>爱车无违章为自己点个赞</p>
                                                </div>
                                                : ''
                                        }
                                    </div>
                            }
                        </div>
                    )
                }
            } else {
                //办理中、已完毕
                if (violations[index] && Object.keys(violations[index]).length > 0) {
                    let hasuntreated = (violations['0'] && violations['0'].length > 0) || (violations['99'] && violations['99'].length > 0) || (violations['-1'] && violations['-1'].length > 0)
                    return (
                        <div>
                            {violations[index].map((item2, k) => <Card key={item2.violationId} {...item2} checked={checkeds.indexOf(item2.violationId) != -1} />)}
                        </div>
                    )

                } else {
                    return ""
                }
            }

        }
    }

    /**
     * 显示白色背景
     */
    showWhiteBg(violationList) {
        let { violationStatus } = this.state
        //不存在数据时，背景为白色
        let showWhiteBg = !violationList[violationStatus] || (violationList[violationStatus] && violationList[violationStatus].length == 0)
        //未办理页面显示不可办理的违章 因此还需要判断不可办理
        if (violationStatus == '0' && ((violationList['-1'] && violationList['-1'].length > 0) || (violationList['99'] && violationList['99'].length > 0))) {
            showWhiteBg = false
        }

        return showWhiteBg;
    }

    //显示历史违章
    showHistory() {
        this.setState({
            isShowHistory: true,
        })
    }

    //获取违章概要信息
    getBreif(violations) {
        let { carId } = this.props.params;
        let { cars } = this.props;
        let [untreatedNum, totalDegree, totalFine] = [0, 0, 0];
        if (cars && cars.data && cars.data[carId]) {
            violations["all"].map(item => {
                if (item.violationStatus == "0") {
                    untreatedNum += 1;
                    totalDegree += item.degree * 1;
                    totalFine += item.fine * 1;
                }
            })
        }
        return {
            untreatedNum,
            totalDegree,
            totalFine
        }
    }

    //是否显示补充资料按钮
    isShowupdateBox() {
        let showUpdateBox = false;
        try {
            let { originalRule } = this.getOriginalRuleAndViolationIds()
            if (Object.keys(originalRule).length > 0) {
                showUpdateBox = true
            }
        } catch (error) {

        }
        return showUpdateBox;
    }

    render() {
        let { isLoading, tabsKey, confirmInfo, checkeds, checkAlled, total, showRealDegreeAlert,
            tabsTitle, violationStatus, showFooter, ready, isShowHistory, hasCoupon } = this.state

        let { violations } = this.props
        let violationList = this.getList() //违章列表 没有时返回false
        let historyBtnClass = (violationList['0'].length == 0 && violationList['-1'].length == 0 && violationList['99'].length == 0) ? Style.checkHistoryBorderBtn : Style.checkHistoryBtn;
        showFooter = violationList['0'].length > 0 ? true : false;
        let showWhiteBg = violationList['0'].length == 0 && violationList['-1'].length == 0 && violationList['99'].length == 0

        // 获取车辆信息
        const carInfo = this.getCarInfo();

        return (
            <div className="box">
                {showWhiteBg ? <div className='whiteBg'></div> : <div className='grayBg'></div>}
                {
                    !ready ? '' :
                        <div className={isLoading ? 'hide' : 'box'} style={{ paddingBottom: showFooter ? "1rem" : 0 }}>
                            <Comfirm {...confirmInfo} />

                            {
                                // 车主服务 京牌车的提示信息
                                this.isAlipayCarService() && carInfo.carNumber.substr(0, 1) === '京' ?
                                    <Notice title='北京地区违章数据有延迟，正在加紧修复中，不影响正常下单' />
                                    : ''
                            }

                            <div style={{ position: 'relative', zIndex: '1' }}>
                                {/*未办理违章 start*/}
                                {this.showViolations(violationList, 1)}
                                {/*未办理违章 end*/}
                            </div>
                            {isShowHistory && <div>
                                {/*历史违章 start*/}
                                {this.showViolations(violationList, 2)/*办理中*/}
                                {this.showViolations(violationList, 3)/*已完成*/}
                                {/*历史违章 end*/}
                            </div>}
                            {(!isShowHistory && (violationList['2'].length > 0 || violationList['1'].length > 0)) ? <div className={historyBtnClass} onClick={() => this.showHistory()}>点击查看历史违章</div> : <div style={{ height: "0.48rem" }}></div>}

                            {
                                this.isAlipayCarService() ?
                                    <div>
                                        {/* 支付宝关注组件 start*/}
                                        <iframe src="https://publicexprod.alipay.com/recommend/queryRecommend.htm?publicId=2014052100006070&sourceId=AlipayCarService" style={{ width: '100%', height: '72PX' }} frameborder="0"></iframe>
                                        {/* 支付宝关注组件 end*/}
                                        <div className={Style.kefuPhone}>
                                            <a href='tel:020-62936789'>车行易客服电话：020-62936789</a>
                                        </div>
                                    </div>
                                    : ''
                            }

                            {/* 有可用优惠券 */}
                            <div className={hasCoupon ? Style.hasCoupon : 'hide'}>
                                <span>有可用优惠券</span>
                            </div>

                            <div className={showFooter && violationList['all'].length > 0 ? Style.footerBox : 'hide'}>
                                <div className={Style.checkBox + (checkAlled ? ' checkBoxTrue' : ' checkBoxFalse')} onClick={() => this.selectAll()}></div>
                                <div className={Style.footerText}>
                                    <span>{total.checkedNum}笔</span><span>共{total.checkedFine}元</span><span>扣{total.checkedDegree}分</span>
                                </div>
                                {
                                    checkeds.length > 0 && (!this.isAlipayCarService() || carInfo.carNumber.substr(0, 1) !== '闽') ?
                                        <div className={Style.footerBtn} onClick={() => this.showConfirm()}>立即办理</div>
                                        :
                                        <div className={Style.footerBtn + ' ' + Style.notClickBtn}>立即办理</div>
                                }

                            </div>

                        </div>
                }

                {/*说明弹窗 start*/}
                {showRealDegreeAlert ? <WAlert num={2} onClose={() => this.setState({ showRealDegreeAlert: false })} /> : ''}
                {/*说明弹窗 end*/}
            </div >
        );
    }
}

//使用context
ViolationIndex.contextTypes = {
    router: React.PropTypes.object.isRequired
}

ViolationIndex.propTypes = {

};

const mapStateToProps = state => ({
    cars: state.cars,
    violations: state.violations
})

const mapDispatchToProps = dispatch => ({
    violationActions: bindActionCreators(violationActions, dispatch),
    carActions: bindActionCreators(carActions, dispatch),
    couponActions: bindActionCreators(couponActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViolationIndex);