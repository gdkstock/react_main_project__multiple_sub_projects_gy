/*
*	车辆违章概要信息组件
	props = {
		"carId": "12345",
        "carNumber": "粤A12345",
        "untreatedNum": "3",
        "totalFine": "550",
        "totalDegree": "3"

	}
*/
import WarningMsg from './WarningMsg'
import styles from './ViolationBrief.scss'

const defaultProps = {
	"carId": "12345",
    "carNumber": "粤A12345",
    "untreatedNum": "3",
    "totalFine": "550",
    "totalDegree": "3",
    "violationWarningMsg":"未处理违章已达5条以上，车辆上路被查将强制扣车，请尽快办理违章",
    "violationNew":true,
    "rightText":"",//右侧文本
    "handleClick":()=>{},
}

const ViolationBrief = props => {
	return (
		<div>
			<div className={styles.wrap} onClick={()=>props.handleClick(props.carId)}>
				<div className={styles.carNumberWrap}>
					<div className={styles.carNumber}>
						违章办理
						{props.violationNew && <i className={styles.unread}></i>}
					</div>
					<span className={styles.treatedNow}>{props.rightText}</span>
					<div className={styles.rightIcon}></div>
				</div>
				<div className={styles.hr}></div>
				<div className={styles.violationDetail}>
					<div className={styles.commonItem}>
						<div>{props.untreatedNum}</div>
						<div>违章数</div>
					</div>
					<div className={styles.commonItem}>
						<div>{props.totalFine}</div>
						<div>待罚款(元)</div>
					</div>
					<div className={styles.commonItem}>
						<div>{props.totalDegree}</div>
						<div>待扣分</div>
					</div>
				</div>
			</div>
			<WarningMsg text={props.violationWarningMsg} id="violationWarningMsg"/>
		</div>
	)
}

export default ViolationBrief