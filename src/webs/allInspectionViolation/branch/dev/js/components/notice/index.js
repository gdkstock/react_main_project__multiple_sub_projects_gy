/**
 * 公告相关
 */

import styles from './index.scss';

export const Notice = ({ title }) => {

    return (
        <div className={styles.notice}>
            <p>{title}</p>
        </div>
    )
}