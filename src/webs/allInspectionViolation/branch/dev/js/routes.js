import React from 'react'
import { Route, IndexRoute } from 'react-router'

//antd UI
import { Toast } from 'antd-mobile'

import {
  App,
  Home,
  NotFoundPage,
} from './containers'


//显示加载中
const showLoading = () => {
  Toast.loading('', 30, () => {
    window.networkError('./images/networkError-icon.png')
  })
}

export default (
  <Route path="/" component={App}>
    {/*<IndexRoute component={CarList} />*/}
    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}

    {/*车辆相关 start*/}
    <IndexRoute component={Home} />

    {/* 专门给外部跳转过来的URL使用，避免出现自动关闭webView的情况 */}
    <Route path="index" component={Home} />

    <Route path="addCar" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/addCar').default)
      }, 'car')
    }} />
    {/*<Route path="carDetail/:id(/:getDatas)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carDetail/index').default)
      }, 'car')
    }} />*/}
    <Route path="carDetail(/:id(/:getDatas))" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carDetail/index_v1.3').default)
      }, 'car')
    }} />
    {/*共内部使用的编辑车辆页面*/}
    <Route path="editCar/inner/:id" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/editCar').default)
      }, 'car')
    }} />
    {/*共外部使用的编辑车辆页面*/}
    <Route path="editCar/:id" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/editCar').default)
      }, 'car')
    }} />
    <Route path="replenishInfo/:id" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/replenishInfo').default)
      }, 'car')
    }} />
    <Route path="annualInfo/:carId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./components/calculatorOfYearlyCheck/demo').default)
      }, 'car')
    }} />

    {/* 车型选择页面 this.props.location.query.update为true时 自动更新车辆信息 */}
    <Route path="brand(/:carId)(/:carNumber)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        // cb(null, require('./components/Form/carbrand').default)
        cb(null, require('./containers/selectPages/selectCar').default)
      }, 'car')
    }} />
    <Route path="drivingLicenceList/:carId/(:licenceId)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/drivingLicence/list').default)
      }, 'car')
    }} />
    <Route path="addDrivingLicence/:carId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/drivingLicence/addLicence').default)
      }, 'car')
    }} />
    <Route path="editDrivingLicence/:carId/:licenceId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/drivingLicence/editLicence').default)
      }, 'car')
    }} />
    {/*车辆相关 end*/}

    {/*违章相关 start*/}
    <Route path="/updateCarRequirement/:carId(/:violationIds)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/updateCarRequirement').default)
      }, 'violation')
    }} />
    <Route path="/violationList/:carId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/violation/index').default)
      }, 'violation')
    }} />
    <Route path="/violationDetail/:violationId(/:carId)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/violation/detail').default)
      }, 'violation')
    }} />
    <Route path="/violationHandle" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/violation/handleList').default)
      }, 'violation')
    }} />
    <Route path="/violationHandle" component={require('./containers/violation/handleList').default} />
    {/*违章相关 end*/}

    {/*订单相关 start*/}
    <Route path="/orderConfirm(/:violationIds)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/orderConfirm').default)
      }, 'order')
    }} />
    <Route path="/orderDetail/:orderId(/:userId/:token/:userType/:authType)(/:from)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/orderDetail/index').default)
      }, 'order')
    }} />
    <Route path="/orderPaySuccess/:orderId(/:carId)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/orderPaySuccess').default)
      }, 'order')
    }} />
    <Route path="/orderPaySuccessAd" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/orderPaySuccess/defaultAd').default)
      }, 'order')
    }} />
    <Route path="/coupons/:couponIds(/:couponId)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/coupon').default)
      }, 'order')
    }} />

    <Route path="*" component={NotFoundPage} />
  </Route>
);