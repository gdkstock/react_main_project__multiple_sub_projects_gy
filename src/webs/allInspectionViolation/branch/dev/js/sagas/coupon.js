/**
 * 优惠券相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    USEFUL_COUPON_ASYNC,
} from '../actions/actionsTypes'

//service
import couponService from '../services/couponService';

import { ChMessage } from '../utils/message.config' //提示信息

import { Toast } from 'antd-mobile' //UI

import { normalize, schema } from 'normalizr'; //范式化库


/** 可用优惠券 */
function* usefulCouponAsync(action) {
    Toast.loading('', 0)
    try {
        let { result, timeout } = yield race({
            result: call(couponService.useful, action.data),
            timeout: call(delay, 30000)
        })
        Toast.hide();
        if (timeout) {
            // 超时
        } else {
            if (result.code == "1000") {
                if (action.callback) {
                    action.callback(result)
                }
            }
        }
    } catch (error) {
        Toast.hide();
    }
}

function* watchUsefulCouponAsync() {
    yield takeLatest(USEFUL_COUPON_ASYNC, usefulCouponAsync)
}

export function* watchCoupons() {
    yield [
        fork(watchUsefulCouponAsync)
    ]
}