# 违章项目 全渠道

## 基于全局变量的临时缓存（刷新页面或重新进入webView会被清空）
* `window.myCache_getViolationList[carId]` 违章列表接口的缓存数据
* `window.myCache_violationHotComment[violationId]` 违章评论接口的缓存数据
* `window.myCache_getLocationViolationNumber[violationId]` 地点热点违章人数的缓存数据
* `window.myCache_fetchGetAd[positionCode]` 广告接口的缓存数据
* `window.api_getRecallBrandList` 用于存放车辆品牌列表
* `window.api_getRecallModelList[id]` 车辆品牌下的车系
* `window.api_getRecallCarList[id]` 车辆车系下的具体车型
* `window.cache_selectCar[carId]` 选择的车型信息

## sessionStorage缓存
* `orderConfirmDatas` 订单试算数据


## 项目全局state 概况

users //用户列表
cars //车辆车辆
drivingLicences //驾驶证列表
violations //违章列表
banners //banner列表
inspections //年检列表

banners = {
	data:{
		positionCode:{
			"advertisement": [
                {
                    "id": 20055,
                    "adJson": {
                        "imageAddress": {
					        "img": "http://oss.cx580.com/v5picture/2017gd_new_rule.png",
					        "backImg": ""
					    },
					    "imageUrl": "http://m.cx580.com/sqwei",
					    "showTime": "5",
					    "clickEvent": "0",
					    "iosCheckShowFlag": "1",
					    "umengEvent": ""
                    },
                    "updateTime": "2017-01-19 10:53:46",
                    "sort": 4
                }
            ],
            "updateTime": "2017-01-19 10:53:46",
            "positionCode": "activity_splash"
		}
	}
	result:[]
}

cars = {
	data: {
		carId:{
			carNumber:'粤A12345',
            //其他
			untreatedNum:"",
			totalFine: "",
			totalDegree: "",
			violationWarningMsg:"",
			drivingLicenceWarningMsg:"",
			violationNew:false, //有未读违章
			readViolationIds:[violationId], //已阅读的违章Id列表
			violations:[1,2,3]
		}
	}
	result: [ '123', '456' ] 
}

cars.result.map(item=>{
	cars.data[item].carNumber
})


action.js
const addCar = data => ({type:'ADD_car', data:data})


reducer.js  -- cars
case 'ADD_car': 
	return {
		data:Object.assign({},state.data,action.data),
		result: Array.from(new State([...state.result,action.data.carId]))
	}