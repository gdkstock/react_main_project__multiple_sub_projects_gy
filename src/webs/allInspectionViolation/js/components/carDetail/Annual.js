/*
*	年检卡片信息组件
	props = {
		days:"",//年检天数
	}
*/

import styles from './index.scss'

const defaultProps = {
	days:"",	//距离年检天数
	state:"", 	//年检状态
	content:{__html:"只需一步，年检标志寄到家"},
	rightText:"立即办理",
}

const Annual = props => {
	return (
		<div className={styles.wrap} onClick={props.handleClick}>
			<div className={styles.leftText}>
				<div className={styles.title}>车辆年检</div>
				<div className={styles.content} dangerouslySetInnerHTML={props.content}></div>
			</div>
			<div>
				<span className={styles.rightText}>{props.rightText}</span><i className={styles.rightIcon}></i>
			</div>
		</div>
	)
}

export default Annual