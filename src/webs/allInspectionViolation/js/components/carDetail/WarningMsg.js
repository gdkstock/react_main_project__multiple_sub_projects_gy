/*
*	警告轮播信息组件
	props = {
		text:"",//轮播文本
	}
*/
import React from 'react'
import styles from './WarningMsg.scss'

class WarningMsg extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		this.state = {
			id:this.props.id
		}
	}

	componentDidMount(){
	}

	componentDidUpdate(){
		//获得父元素
		let parentId = this.state.id 
		let wrap = document.getElementById(parentId);
		//执行滚动动画
		if(wrap){
			setTimeout(()=>this.animation(parentId),100)
		}
	}

	componentWillUnmount(){
		// alert("组件卸载了")
		// alert(window.timer)
	}

	animation(parentId){
		try{
			let wrap = document.getElementById(parentId);
			if(wrap){
				let text = wrap.firstElementChild
				let wrapWidth = wrap.clientWidth;
				let textWidth = text.clientWidth;
				let distance = 1 //每次移动的距离
				let time = 40 //每次移动的时间间隔
				let diff = textWidth - wrapWidth
				if(diff>0){
					let html = `<div class="text">${text.innerHTML+text.innerHTML}</div>`;
					wrap.innerHTML = html;
					let i = 0;
					text = wrap.firstElementChild
					if(window['timer'+parentId]){
						clearInterval(window['timer'+parentId]);
					}
					window['timer'+parentId]=setInterval(()=>{
						let left = text.offsetLeft
						if(document.getElementById(parentId)){
							if(Math.abs(left)<textWidth){
								text.style.left = `-${i}px`;
								i+=distance;
							}else{
								text.style.left = 0;
								i=0;
							}
						}else{
							clearInterval(window['timer'+parentId])
						}
						
					},time)
				}
			}
		}catch(e){

		}
	}

	render(){
		return (
			<div>
				{
					this.props.text && <div className={styles.wrap}>
						{ this.props.hasBrodcastIcon && <i className={styles.brodcastIcon}></i> }
						<div className={styles.textWrap} id={this.props.id}>
							<div className="text">{this.props.text+"。 "}</div>
						</div>
					</div>
				}
			</div>
		)
	}
}

export default WarningMsg

// <marquee 
// 	align="middle" 
// 	behavior="scroll" 
// 	direction="left"  
// 	hspace="50" 
// 	vspace="20" 
// 	loop="-1" 
// 	scrollamount="1" 
// 	scrolldelay="100" 
// 	className={styles.marquee}
// >
// 	<div>
// 		{props.children}
// 	</div>
// </marquee>	