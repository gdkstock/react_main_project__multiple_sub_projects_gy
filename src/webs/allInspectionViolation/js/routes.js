import React from 'react'
import { Route, IndexRoute } from 'react-router'

//antd UI
import { Toast } from 'antd-mobile'

import {
  App,
  // Home,
  NotFoundPage,
  // CarList,
  // AddCar,
  // EditCar,
  // ReplenishInfo,
} from './containers'

// import ViolationList from './containers/violation/index' //违章列表
// import ViolationDetail from './containers/violation/detail' //违章详情
// import UpdateCarRequirement from './containers/updateCarRequirement' //补充资料
// import OrderConfirm from './containers/orderConfirm' //订单确认
// import OrderDetail from './containers/orderDetail' //订单详情
// import Coupons from './containers/coupon' //优惠券

// import { default as CarDetail } from './containers/carDetail'

// import Brand from './components/Form/carbrand'
// import DrivingLicenceList from './containers/drivingLicence/list'
// import AddLicence from './containers/drivingLicence/addLicence'
// import EditLicence from './containers/drivingLicence/editLicence'
// import AnnualInfo from './components/calculatorOfYearlyCheck/demo'

//显示加载中
const showLoading = () => {
  Toast.loading('', 30, () => {
    window.networkError('./images/networkError-icon.png')
  })
}

export default (
  <Route path="/" component={App}>
    {/*<IndexRoute component={CarList} />*/}
    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}

    {/*车辆相关 start*/}
    <IndexRoute getComponents={(nextState, cb) => {
      require.ensure([], (require) => {
        cb(null, require('./containers/carList/index').default)
      }, 'index')
    }} />

    {/* 专门给外部跳转过来的URL使用，避免出现自动关闭webView的情况 */}
    <Route path="index" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carList/index').default)
      }, 'index')
    }} />

    <Route path="addCar" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/addCar').default)
      }, 'car')
    }} />
    <Route path="carDetail/:id(/:getDatas)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carDetail/index').default)
      }, 'car')
    }} />
    <Route path="editCar/:id" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/editCar').default)
      }, 'car')
    }} />
    <Route path="replenishInfo/:id" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/carInfo/replenishInfo').default)
      }, 'car')
    }} />
    <Route path="annualInfo/:carId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./components/calculatorOfYearlyCheck/demo').default)
      }, 'car')
    }} />
    <Route path="brand(/:carId/:carNumber)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./components/Form/carbrand').default)
      }, 'car')
    }} />
    <Route path="drivingLicenceList/:carId/(:licenceId)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/drivingLicence/list').default)
      }, 'car')
    }} />
    <Route path="addDrivingLicence/:carId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/drivingLicence/addLicence').default)
      }, 'car')
    }} />
    <Route path="editDrivingLicence/:carId/:licenceId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/drivingLicence/editLicence').default)
      }, 'car')
    }} />
    {/*车辆相关 end*/}

    {/*违章相关 start*/}
    <Route path="/updateCarRequirement/:carId(/:violationIds)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/updateCarRequirement').default)
      }, 'violation')
    }} />
    <Route path="/violationList/:carId" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/violation/index').default)
      }, 'violation')
    }} />
    <Route path="/violationDetail/:violationId(/:carId)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/violation/detail').default)
      }, 'violation')
    }} />
    {/*违章相关 end*/}

    {/*订单相关 start*/}
    <Route path="/orderConfirm/:violationIds" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/orderConfirm').default)
      }, 'order')
    }} />
    <Route path="/orderDetail/:orderId(/:userId/:token/:userType/:authType)(/:from)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/orderDetail/index').default)
      }, 'order')
    }} />
    <Route path="/coupons/:couponIds(/:couponId)" getComponents={(nextState, cb) => {
      showLoading()
      require.ensure([], (require) => {
        Toast.hide()
        cb(null, require('./containers/coupon').default)
      }, 'order')
    }} />
    {/*订单相关 end*/}

    {/*<Route path="addCar" component={AddCar} />*/}
    {/*<Route path="carDetail/:id" component={CarDetail} />*/}
    {/*<Route path="editCar/:id" component={EditCar} />*/}
    {/*<Route path="replenishInfo/:id" component={ReplenishInfo} />*/}
    {/*<Route path="annualInfo/:carId" component={AnnualInfo} />*/}

    {/*<Route path="brand(/:carId/:carNumber)" component={Brand} />*/}
    {/*<Route path="drivingLicenceList/:carId/(:licenceId)" component={DrivingLicenceList} />*/}

    {/*补充违章代办资料 start*/}
    {/*<Route path="/updateCarRequirement/:carId/:violationIds" component={UpdateCarRequirement} />*/}
    {/*补充违章代办资料 end*/}
    {/*<Route path="addDrivingLicence/:carId" component={AddLicence} />*/}
    {/*<Route path="editDrivingLicence/:carId/:licenceId" component={EditLicence} />*/}


    {/*违章相关路由 start*/}
    {/*<Route path="/violationList/:carId" component={ViolationList} />*/}
    {/*<Route path="/violationDetail/:violationId" component={ViolationDetail} />*/}
    {/*违章相关路由 end*/}

    {/*订单相关 start*/}
    {/*<Route path="/orderConfirm/:violationIds" component={OrderConfirm} />*/}
    {/*<Route path="/orderDetail/:orderId(/:userId/:token/:userType/:authType)(/:from)" component={OrderDetail} />*/}
    {/*<Route path="/coupons/:couponIds(/:couponId)" component={Coupons} />*/}
    {/*订单相关 end*/}
    <Route path="*" component={NotFoundPage} />
  </Route>
);