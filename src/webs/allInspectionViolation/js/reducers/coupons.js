import {
	ADD_COUPON,
	ADD_COUPONS,
	DELETE_COUPON
} from '../actions/actionsTypes'

let initState = {
	data: {},
	result: []
}
export default function coupons(state = initState, action) {
	switch (action.type) {
		case ADD_COUPON:
		//添加优惠券

		case ADD_COUPONS:
			//添加优惠券列表
			return {
				// data: { ...state.data, ...action.data.data },
				data: Object.assign({}, state.data, action.data.data),
				result: Array.from(new Set([...state.result, ...action.data.result]))
			}
		default:
			return state;
	}
}