export {default as App} from './App'

// 简单
// export { default as Home } from './Home'

// 404
export { default as NotFoundPage } from './404.js'

// carList
// export { default as CarList } from './carList'

// AddCar
// export { default as AddCar } from './carInfo/addCar'

// EditCar
// export { default as EditCar } from './carInfo/editCar'

// export { default as ReplenishInfo } from './carInfo/replenishInfo'

