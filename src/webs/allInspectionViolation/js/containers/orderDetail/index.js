/**
 * 订单详情
 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, Toast } from 'antd-mobile'

//组件


//action
import * as violationActions from '../../actions/violationActions'

//server
import orderService from '../../services/orderService'

//style
import Style from './index.scss'

//常用工具类
import { ChMessage } from '../../utils/message.config'
import userHelper from '../../utils/userHelper'
import common from '../../utils/common'
import config from '../../config'

class OrderDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            orderId: '',
            carNumber: '',
            orderStatus: '', //订单状态
            showMove: false, //显示更多
            handleMsg: '',
            moveText: '',
            orderMsg: '',
            voliationCount: '', //违章条数
            voliationsAmountScore: '', //违章扣分
            voliationsFineAmount: '', //罚款金额
            orderVoliations: [], //违章列表
            orderAmount: '',
            showHelpBox: navigator.userAgent.indexOf('appname_cxycwz') > -1, //显示常见问题和联系客服模块
            accountList: [], //
            createTime: '', //下单时间
            payNo: '', //支付流水号 || 交易号
            payTime: '', //支付时间
            payType: '', //支付类型
            payTypeName: '', //支付名称
            orderPayAmount: '', //总总额
        }
    }

    componentWillMount() {
        common.setViewTitle('订单详情');

        createJsApi(); //加载对应平台的JS
    }

    componentDidMount() {
        let { orderId, userId, token, userType, authType, from } = this.props.params;
        if (from && from == 'Pay') { //Pay：收银台；MyOrderList：订单中心
            common.backToUrlAndCloseWebView(config.myOrderUrl);
            common.noCloseWebViewUrl(window.location.href); //直接关闭，不跳转到我的订单了
        }
        if (userId && token && userType && authType) {
            //存在用户信息
            sessionStorage.setItem('userId', userId);
            sessionStorage.setItem('token', token);
            sessionStorage.setItem('userType', userType);
            sessionStorage.setItem('authType', authType);
            sessionStorage.setItem('windowLocationHref', window.location.href); //保存所有信息
        }
        if (sessionStorage.getItem('userId') && sessionStorage.getItem('token') && sessionStorage.getItem('userType') && sessionStorage.getItem('authType')) {
            this.getOrderDetail() //获取订单详情数据
        } else {
            userHelper.Login();
        }
    }

    componentWillUnmount() {
        sessionStorage.setItem("prevPageInfo", document.title)
    }

    getOrderDetail() {
        let { orderId } = this.props.params
        let postData = {
            // orderId: '1630644546964',
            // userId: 'CXY_DC69095B03B64A6793F7FA0F3D1D8625',
            // userType: 'app'
            orderId: orderId
        }
        orderService.view(postData).then(result => {
            if (result.code == '1000') {
                this.setState(Object.assign({}, this.state, result.data))
            } else {
                Toast.info(result.msg || ChMessage.FETCH_FAILED, 3, () => window.history.back())
            }
        }, error => {
            Toast.info(ChMessage.FETCH_FAILED)
        })
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    handleMove() {
        let { showMove } = this.state
        this.setState({
            showMove: !showMove //显示更多按钮
        })
    }

    render() {
        let { orderId, carNumber, showMove, orderStatus, moveText, handleMsg, orderMsg, isShowVip, voliationCount, voliationsAmountScore, voliationsFineAmount, orderVoliations, orderAmount, showHelpBox, accountList, createTime, payNo, payTime, payType, payTypeName, sweetTips, orderPayAmount } = this.state
        return (
            <div className='box'>
                {/*办理状态 start*/}
                <div className={Style.top + (orderStatus == '4' ? ' ' + Style.gray : '')}>
                    <div className={Style.topText}>
                        <span>{handleMsg}</span>
                        {orderMsg ? <p>{orderMsg}</p> : ''}
                    </div>
                    <img src={orderStatus == '4' ? './images/icon-book2.png' : './images/icon-book.png'} className={Style.topImg} />
                </div>
                {/*办理状态 end*/}

                {/*违章相关 start*/}
                <div className={Style.violations}>
                    <div className={Style.violationTitle}>
                        <div className={Style.verticalAlignBox}>
                            <span>{carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}的违章</span>
                            <p>共办理{voliationCount}条，扣{voliationsAmountScore}分，罚款金额￥{voliationsFineAmount}元</p>
                        </div>
                    </div>
                    <div className={Style.violationList}>
                        {
                            orderVoliations.map((item, i) => (
                                showMove
                                    ? <div key={i} className={Style.violationItem}>
                                        <div className={Style.verticalAlignBox}>
                                            <span className='text-overflow-2'>{item.locaitonName + item.violationLocation}</span>
                                            <p><i className={Style.violationTime}>{item.occurTime}</i><i>罚款{item.fineAmount}元</i><i>扣{item.realDegree}分</i></p>
                                        </div>
                                    </div>
                                    : i < 3
                                        ? <div key={i} className={Style.violationItem}>
                                            <div className={Style.verticalAlignBox}>
                                                <span className='text-overflow-2'>{item.locaitonName + item.violationLocation}</span>
                                                <p><i className={Style.violationTime}>{item.occurTime}</i><i>罚款{item.fineAmount}元</i><i>扣{item.realDegree}分</i></p>
                                            </div>
                                        </div>
                                        : ''
                            ))
                        }
                    </div>
                </div>
                <div className={orderVoliations && orderVoliations.length > 3 && showMove ? Style.moveBtn : 'hide'}><span onClick={() => this.handleMove()}>查看更多</span></div>
                {/*违章相关 end*/}

                {/*会员 start*/}
                <div className={isShowVip == '1' ? Style.vipBox : 'hide'} onClick={() => window.location.href = config.vipInfoUrl}>
                    <div className={Style.vipRight}>
                        <span>已办理</span>
                    </div>
                    <div className={Style.vipLeft}>
                        <span>会员</span>
                        <p>一站式车务管家</p>
                    </div>
                </div>
                {/*会员 end*/}

                {/*金额详情 start*/}
                <div className={Style.moneyInfo}>
                    {
                        accountList && accountList.map((item, i) =>
                            <p key={'account' + i}><span>{item.name}</span><i>{item.account < 0 ? `- ¥${item.account * -1}` : `¥${item.account}`}</i></p>
                        )
                    }
                    <div className={Style.totalMoney}>
                        <span>合计金额</span><i>¥{orderPayAmount}</i>
                    </div>
                </div>
                {/*金额详情 end*/}

                {/*帮助相关 start*/}
                <div className={Style.helpBox}>
                    {/*只在APP中显示 start*/}
                    <div className={showHelpBox ? Style.helpBtns : 'hide'}>
                        <div className={Style.question}><span>常见问题</span></div>
                        <div className={Style.telPhone}><span>联系客服</span></div>
                    </div>
                    {/*只在APP中显示 end*/}
                    <div className={Style.helpText}>
                        <p>{sweetTips || '已下单用户请勿私下重复处理违章，否则因此导致的重复缴款损失将自行承担；异地差异罚款及产生滞纳金的罚单由客户通知补齐。'}</p>
                    </div>
                </div>
                {/*帮助相关 end*/}

                {/*订单详情 start*/}
                <div className={Style.orderInfo}>
                    <p>订单编号：{orderId}</p>
                    <p>支付方式：{payTypeName}</p>
                    <p>交易号：{payNo}</p>
                    <p>创建时间：{createTime}</p>
                    <p>支付时间：{payTime}</p>
                </div>
            </div>
        );
    }
}

//使用context
OrderDetail.contextTypes = {
    router: React.PropTypes.object.isRequired
}

OrderDetail.propTypes = {

};

const mapStateToProps = state => ({
    cars: state.cars,
    violations: state.violations
})

const mapDispatchToProps = dispatch => ({
    violationActions: bindActionCreators(violationActions, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderDetail);