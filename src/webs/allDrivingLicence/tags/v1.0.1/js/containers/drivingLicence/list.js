/**
 * 驾照列表页面
 */
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn } from '../../components'
import { LicenceCard } from '../../components/drivingLicence'
import * as drivingLicenceActions from '../../actions/drivingLicenceActions'
import common from '../../utils/common'
import styles from './list.scss'
import { Toast } from 'antd-mobile'
import jsApi from '../../utils/cx580.jsApi'

class DrivingLicenceList extends React.Component {
	constructor(props) {
		super(props);
		//设置初始状态
		this.state = {
			isReady: false, // 加载完成标识符
		}
	}

	componentWillMount() {
		//设置标题
		common.setViewTitle('驾照查分');

		//设置菜单栏右侧按钮
		if (common.isCXYApp()) {
			try {
				jsApi.call({
					"commandId": "",
					"command": "customizeTopRightButton",
					"data": {
						"reset": "false",
						"list": []
					}
				},
					function (data) {

					})
			} catch (e) {

			}
		}

		let list = this.props.drivingLicence.result

		//请求驾照列表数据
		this.props.actions.queryDrivingLicenceList({}, res => {
			if (res.code == 1000) {
				this.setState({
					isReady: true, // 标识为加载完成
				});

				if (res.data.length === 0) {
					if (!sessionStorage.getItem('W_toLicence')) {
						sessionStorage.setItem('W_toLicence', 1); // 定义一个标识符，避免出现死循环
						this.toLicence(); // 只自动触发一次
					}
				}
				sessionStorage.setItem('W_toLicence', 1); // 定义一个标识符，只在首次会自动跳转
			}
		})
	}

	componentWillReceiveProps(nextProps) {

	}

	componentDidMount() {
		//进入驾照列表页埋点
		common.sendCxytj({
			eventId: "Violation_EnterDrivingLicence"
		})
		this.createCircle();
	}

	componentWillUnmount() {
		//离开驾照列表页记录页面信息
		sessionStorage.setItem("prevPageInfo", document.title)
	}

	componentDidUpdate() {
		//组件更新完成
		this.createCircle();
	}

	//生成圆环
	createCircle() {
		let circleList = document.querySelectorAll(".circle");
		let scoreList = document.querySelectorAll(".score")
		for (var j = 0; j < circleList.length; j++) {
			let circle = circleList[j]
			let child = circle.firstElementChild
			if (circle) {
				let score = 12 - scoreList[j].innerHTML;//获取扣分数
				child.innerHTML = scoreList[j].innerHTML;
				let num = score * 30;
				let D = circle.clientWidth;//外圆直径
				let d = child.clientWidth;//内圆直径
				let r = d / 2 + (D - d) / 4;//圆环半径
				let html = "";
				for (let i = 0; i < num; i++) {
					html += `<div class="unit" style="-webkit-transform:rotate(${i}deg) translateY(-${r}px);transform:rotate(${i}deg) translateY(-${r}px)"></div>`
				}
				circle.innerHTML = circle.innerHTML + html;
			}
		}

	}

	//打开编辑驾照
	openEditLicence(props) {
		let licenceId = props.drivingLicenseId
		window.open(common.getRootUrl() + "editDrivingLicence/" + licenceId, '_self')
	}

	//生成驾照列表数据
	createLicenceList() {
		let { drivingLicence } = this.props;
		let list = drivingLicence.result;
		let data = drivingLicence.data;
		let licenceId;
		let outPutList = []
		outPutList = list.map(item => {
			let tmpObj = {}
			tmpObj = {
				...data[item],
				isOwner: false,
			}
			return tmpObj
		})

		return outPutList
	}

	//添加驾照
	addLicence() {

		//点击添加驾照埋点
		common.sendCxytj({
			eventId: "Violation_AddDrivingLicence_List"
		})

		this.toLicence();
	}

	/**
	 * 跳转到添加驾照页面
	 */
	toLicence() {
		const url = common.getRootUrl() + "addDrivingLicence/";
		window.open(url, '_self');
	}

	render() {
		const { isReady } = this.state;

		//驾照列表数据
		const drivingLiceceList = this.createLicenceList()

		const btnProps = {
			text: "添加驾照",
			disabled: drivingLiceceList.length >= 5 ? true : false,
			handleClick: () => this.addLicence()
		}

		return (
			<div className={styles.wrap}>
				{
					drivingLiceceList.length > 0 ? drivingLiceceList.map((item, i) =>
						<div key={i}>
							<div className='wz_whiteSpace_30'></div>
							<LicenceCard {...item} handleClick={(o) => this.openEditLicence(o)} />
						</div>
					) : isReady && <div className={styles.noDrivingList}></div>
				}
				<div className={styles.btnWrap}>
					<Btn {...btnProps} />
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	drivingLicence: state.drivingLicence,
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(drivingLicenceActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(DrivingLicenceList)