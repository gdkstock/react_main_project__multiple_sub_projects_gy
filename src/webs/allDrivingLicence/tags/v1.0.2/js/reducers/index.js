import { combineReducers } from 'redux'
import drivingLicence from './drivingLicence'

const rootReducer = combineReducers({
    drivingLicence,
});

export default rootReducer;
