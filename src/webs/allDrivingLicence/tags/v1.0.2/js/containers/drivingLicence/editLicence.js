/*
* 编辑驾照
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn ,Confirm ,Modal} from '../../components'
import { TxtGroup ,DateGroup} from '../../components/Form'
import styles from './addLicence.scss'
import * as drivingLicenceActions from '../../actions/drivingLicenceActions'
import { Toast } from 'antd-mobile'
import moment from 'moment'
import common from '../../utils/common'

class EditLicence extends React.Component{
	constructor(props){
		super(props);
		//设置初始状态
		console.log(this.props)
		//初始化状态	
		this.state = this.getNewState({},this.props);

	}

	getNewState(state,props){
		let carNumber;
		let carId = props.params.carId || "";
		let licenceId = props.params.licenceId || "";

		let	driverName = '',
			drivingLicenseNo = '',
			drivingFileNumber = '',
			firstLicensingDate = '',
			drivingTelephone = '';
		console.log('props',props)
		if(licenceId && props.drivingLicence.data[licenceId]){
			driverName = props.drivingLicence.data[licenceId].driverName || '';
			drivingLicenseNo = props.drivingLicence.data[licenceId].drivingLicenseNo || '';
			drivingFileNumber = props.drivingLicence.data[licenceId].drivingFileNumber || '';
			drivingTelephone = props.drivingLicence.data[licenceId].drivingTelephone || '';
			firstLicensingDate = props.drivingLicence.data[licenceId].firstLicensingDate || '';
			//转化date
			if(firstLicensingDate){
				firstLicensingDate=new Date(firstLicensingDate)
				firstLicensingDate = moment(firstLicensingDate).format("YYYY-MM-DD")
			}
		}
		if(carId && props.cars.data[carId]){
			carNumber = props.cars.data[carId].carNumber || "";
		}
		return {
			...state,
			carId,
			drivingLicenseId:licenceId,
			driverName,
			drivingLicenseNo,
			drivingFileNumber,
			firstLicensingDate,
			drivingTelephone,
			isBtnDisabled:true,
		}
	}


	componentWillMount(){
		//设置标题
		common.setViewTitle('编辑驾照');

		//请求车辆信息
		let { drivingLicence } = this.props;
		let licenceId = this.props.params.licenceId || "";
		if(licenceId && !drivingLicence.data[licenceId]){
			this.props.actions.queryDrivingLicence({drivingLicenseId:licenceId})
		}	
	}

	componentWillReceiveProps(nextProps){
		console.log("nextProps",nextProps)
		let nextState = this.getNewState(this.state,nextProps)
		console.log("nextState",nextState)
		this.setState(nextState)
	}

	componentDidMount(){
		//组件加载完成

		//后退时显示确认对话框
		common.backShowConfirm(() => {
			let obj = {
				show: true, //显示对话框
				content: '编辑的信息尚未保存，确定退出？', //内容
				leftBtn: {
					name: '离开', //左按钮文字
					onClick: () => {sessionStorage.removeItem("showConfirm");history.back()}  //点击左按钮
				},
				rightBtn: {
					name: '取消',//右按钮文字
					onClick: () => history.forward() //点击右按钮
				},
				onClose: () => history.forward() //点击关闭按钮
			}
			Modal.confirm(obj);
		})
	}

	componentDidUpdate(){
		//如果编辑驾照信息
		if(!this.state.isBtnDisabled){
			sessionStorage.setItem("showConfirm",1);
		}
	}

	componentWillUnmount() {
		//离开编辑驾照页记录页面信息
        sessionStorage.setItem("prevPageInfo", document.title)
    }

	//输入改变时验证数据
	preValiDate(e){
		console.log(e.target.name)
		let target = e.target,
			name = target.name, 
			len,
			value,
			isBtnDisabled = this.state.isBtnDisabled,
			currValue = target.value.trim();
		switch(name){
			case "driverName":
				len = 32;
				break;
			case "drivingLicenseNo":
				currValue = currValue.toUpperCase();
				len = 18;
				break;
			case "drivingFileNumber":
				currValue = currValue.toUpperCase();
				len = 12;
				break;
			case "drivingTelephone":
				len = 11;
				currValue = currValue.replace(/[^\d]/,'')
				break;
		}
		value = this.cutInput(currValue,len);
		if(value != this.state[name]){
			isBtnDisabled = false
		}
		this.setState({
			[name]:value,
			isBtnDisabled,
		})
	}

	//截取输入框输入文本长度
	cutInput(value ,len){
		let output = value;
		if(value.length>len){
			output = value.slice(0,len)
		}
		return output
	}

	//初次领证日期选择变化
	firstLicensingDateChange(date){
		let isBtnDisabled = this.state.isBtnDisabled;
		if(date.format("YYYY-MM-DD")!=this.state.firstLicensingDate){
			isBtnDisabled = false;
		}
		this.setState({
			firstLicensingDate:date.format("YYYY-MM-DD"),
			isBtnDisabled
		})
	}

	//提交数据
	editInfo(){
		console.log("保存")
		if(this.valiDate()){
			let param = {
				drivingLicenseId :this.state.drivingLicenseId,
				driverName :this.state.driverName,
				drivingLicenseNo :this.state.drivingLicenseNo,
				drivingFileNumber:this.state.drivingFileNumber,
			}
			this.state.firstLicensingDate && (param.firstLicensingDate = this.state.firstLicensingDate)
			this.state.drivingTelephone && (param.drivingTelephone = this.state.drivingTelephone)
			// alert(JSON.stringify(param))
			this.props.actions.queryUpdateDrivingLicence(param)
		}
	}

	//点击提交时验证数据
	valiDate(){
		let {
			carId,
			driverName,
			drivingLicenseNo,
			drivingFileNumber,
			firstLicensingDate,
			drivingTelephone,
		} = this.state

		//验证姓名
		if(driverName.length == 0){
			Toast.info("请输入姓名",2)
			return false
		}

		//验证驾驶证号
		if(drivingLicenseNo.length == 0){
			Toast.info("请输入驾驶证号",2)
			return false
		}else if(!(drivingLicenseNo.length == 15 || drivingLicenseNo.length == 18)){
			Toast.info("请输入正确驾驶证号",2)
			return false
		}

		//验证档案编号
		if(drivingFileNumber.length == 0){
			Toast.info("请输入档案编号",2)
			return false
		}else if( drivingFileNumber.length != 12 ){
			Toast.info("请输入正确的档案编号",2)
			return false
		}

		//验证手机号码
		if(drivingTelephone.length > 0 && !/^\d{11}$/.test(drivingTelephone)){
			Toast.info("请输入正确的手机号",2)
			return false
		}

		return true
	}

	//删除驾照按钮点击
	deleteBtnClick(){
		this.setState({
			confirm:{
			    show: true, //显示对话框
			    content: '确定要删除此驾照吗？', //内容
			    leftBtn: {
			        name: '删除', //左按钮文字
			        onClick: () => this.confirmOk()  //点击左按钮
			    },
			    rightBtn: {
			        name: '取消',//右按钮文字
			        onClick: () => this.confirmCancel() //点击右按钮
			    },
			    onClose: () => this.confirmClose() //点击关闭按钮
			}
		})
	}

	//confirm确认事件
	confirmOk(){
		console.log('点击了确认按钮')
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
		this.deleteLicence()
	}

	//confirm取消事件
	confirmCancel(){
		console.log('点击了取消按钮')
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
	}

	//confirm关闭事件
	confirmClose(){
		console.log('点击了关闭按钮')
		this.setState({
			confirm:{...this.state.confirm,show:false}
		})
	}

	//删除驾照
	deleteLicence(){
		let drivingLicenseId = this.state.drivingLicenseId
		this.props.actions.queryDeleteDrivingLicence({drivingLicenseId})
	}

	render(){
		console.log("render_______________________________")
		console.log(this.state)

		//车主姓名
		const driverName={
			groupId:"driverName" ,//唯一编码
			label:'车主姓名',//标签文本
			placeholder:"车主姓名" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			value:this.state.driverName,//输入框值
		}

		//驾驶证号
		const drivingLicenseNo={
			groupId:"drivingLicenseNo" ,//唯一编码
			label:'驾驶证号',//标签文本
			placeholder:"驾驶证号完整15位或18位" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			hasIcon:true,//是否有提示icon
			iconImg:"./images/demo-jashiZhengHaoLen.png",//icon弹出图片
			value:this.state.drivingLicenseNo,//输入框值
		}

		//档案编号
		const drivingFileNumber={
			groupId:"drivingFileNumber" ,//唯一编码
			label:'档案编号',//标签文本
			placeholder:"档案编号完整12位数" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			hasIcon:true,//是否有提示icon
			iconImg:"./images/demo-danganBianHaoLen.png",//icon弹出图片
			value:this.state.drivingFileNumber,//输入框值
		}
		
		//初次领证日期选择组件props
		const firstLicensingDateProps = {
			groupId:"firstLicensingDate" ,//唯一编码
			label:'初次领证日期',//标签文本
			placeholder:'请选择' ,//占位符文本
			type:"date" ,//日期选择器类型
			format:"YYYY年MM月DD日", //日期格式
			maxDate:moment(),//最大值
			handleChange:(v)=>{this.firstLicensingDateChange(v)} ,//输入框文本改变操作
			value:this.state.firstLicensingDate?moment(this.state.firstLicensingDate,'YYYY年MM月DD日'):"" //输入框值
		}

		//驾驶证绑定手机号
		const drivingTelephone={
			groupId:"drivingTelephone" ,//唯一编码
			label:'驾驶证绑定手机号',//标签文本
			placeholder:"输入手机号码" ,//占位符文本
			handleChange:(e)=>this.preValiDate(e) ,//输入框文本改变操作
			type:"text", //输入框type
			value:this.state.drivingTelephone,//输入框值
		}

		//保存按钮
		const btnProps = {
			text:"保存",
			handleClick:()=>this.editInfo(),
			disabled:this.state.isBtnDisabled,
		}

		return (
			<div>
				<div className="wz_whiteSpace_30"></div>
				<div className="wz_headMsg">
					驾照信息
					<div className={styles.deleteBtn} onClick={()=>this.deleteBtnClick()}><i className={styles.icon}></i>删除</div>
				</div>
				<div className="wz_list">
					<TxtGroup {...driverName}/>
					<TxtGroup {...drivingLicenseNo}/>
					<TxtGroup {...drivingFileNumber}/>
					<DateGroup {...firstLicensingDateProps}/>
					<TxtGroup {...drivingTelephone}/>
				</div>
				<div className="wz_whiteSpace_50"></div>
				<div className={styles.remaindInfo}>支持全国驾照查分，数据来源当地交管局，当天扣分情况可能会有延迟</div>
				<div className="wz_whiteSpace_50"></div>
				<Btn {...btnProps}/>
				<Confirm {...this.state.confirm}/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	drivingLicence:state.drivingLicence
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(drivingLicenceActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditLicence)

