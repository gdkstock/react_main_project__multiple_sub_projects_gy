/*
	日期选择组件
	props={
		groupId:"registerDate" ,//唯一编码
		label:'车辆注册日期',//标签文本
		placeholder:'请选择' ,//占位符文本
		type:"date" ,//日期选择器类型
		format:"", //日期格式
		handleChange:()=>{} ,//输入框文本改变操作
		value:"" //输入框值
	}
*/
import { DatePicker ,List } from 'antd-mobile'
import styles from './datePickerGroup.scss'

const CustomChildren = props =>(
	<div onClick={props.onClick}>
		{props.children}
		{props.extra ? <div className="txt hasRightIcon">{props.extra}</div>:<div className={styles.defalut+" txt hasRightIcon"}>请选择</div>}
		<i className="rightIcon"></i>
	</div>
)


const DateGroup = props =>{
	return (
		<div className="wz_item">
			<DatePicker
	          mode = {props.type}
	          format={val => val.format(props.format)}
	          title = "选择日期"
	          value = {props.value}
	          onChange = {v => props.handleChange(v)}
	          extra = {props.value}
	          maxDate={props.maxDate}
	        >
	          <CustomChildren><span className="label">{props.label}</span></CustomChildren>
	        </DatePicker>
		</div>
	)
}

export default DateGroup

// <input 
// 	className="txt" 
// 	placeholder="请选择" 
// 	value = {props.extra}
// 	disabled
// />
