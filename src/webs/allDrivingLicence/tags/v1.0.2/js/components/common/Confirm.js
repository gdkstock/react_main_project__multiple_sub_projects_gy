/**
 * 确认|取消对话框
 */
import React, { Component } from 'react';
import Style from './Confirm.scss'

class Confirm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showConfirm: false
        }
    }

    /**
     * 点击了按钮
     * @param {*function} callback 回调函数
     */
    onClickBtn(callback) {
        let { leftBtn, rightBtn, onClose } = this.props
        this.refs.confirm.className = Style.confirm + ' ' + Style.hideConfirm
        setTimeout(() => {
            try {
                callback && callback()
                this.refs.confirm.className = Style.confirm + ' ' + Style.showConfirm
            } catch (error) {
                console.error(error)
            }
        }, 200)
    }

    render() {
        let { show, content, leftBtn, rightBtn, onClose } = this.props
        let { showConfirm } = this.state
        return (
            <div className={show ? Style.fixedBox : Style.hide}>
                <div ref='confirm' className={Style.confirm + ' ' + Style.showConfirm}>
                    <i className={Style.closeIcon} onClick={() => this.onClickBtn(() => onClose())}></i>
                    <div className={Style.content}>{content}</div>
                    <div className={Style.btns}>
                        <div className={Style.leftBtn} onClick={() => this.onClickBtn(() => leftBtn.onClick())}>{leftBtn.name}</div>
                        <div className={Style.rightBtn} onClick={() => this.onClickBtn(() => rightBtn.onClick())}>{rightBtn.name}</div>
                    </div>
                </div>
            </div>
        );
    }
}

Confirm.defaultProps = {
    show: false, //显示对话框
    content: '', //内容
    leftBtn: {
        name: '取消', //左按钮文字
        onClick: () => console.log('点击了取消按钮') //点击左按钮
    },
    rightBtn: {
        name: '确认',//右按钮文字
        onClick: () => console.log('点击了确认按钮') //点击右按钮
    },
    onClose: () => console.log("点击了关闭按钮") //点击关闭按钮
}

export default Confirm;