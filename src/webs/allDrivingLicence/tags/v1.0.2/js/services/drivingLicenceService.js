/**
 * 车辆相关接口
 */

import apiHelper from './apiHelper';

class drivingLicenceService {

  /**
   * 查询单个驾照信息
   * @param {*object} data
   */
  getInfo(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}drivingLicence/view`,
      data: {
        method: 'post',
        body: data
      }
    };
    
    return apiHelper.fetch(requestParam);
  }

  /**
   * 查询驾照列表信息
   * @param {*object} data
   */
  getList(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}drivingLicence/list`,
      data: {
        method: 'post',
        body: data
      }
    };
    
    return apiHelper.fetch(requestParam);
  }

  /**
   * 添加驾照
   * @param {*object} data
   */
  add(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}drivingLicence/save`,
      data: {
        method: 'post',
        body: data
      }
    };
    
    return apiHelper.fetch(requestParam);
  }

  /**
   * 修改驾照
   * @param {*object} data
   */
  update(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}drivingLicence/update`,
      data: {
        method: 'post',
        body: data
      }
    };
    
    return apiHelper.fetch(requestParam);
  }

  /**
   * 删除驾照
   * @param {*object} data
   */
  delete(data) {
    let requestParam = {
      url: `${apiHelper.baseApiUrl}drivingLicence/delete`,
      data: {
        method: 'post',
        body: data
      }
    };
    
    return apiHelper.fetch(requestParam);
  }
   

}

export default new drivingLicenceService()