import React from 'react'
import { Route, IndexRoute } from 'react-router'

import {
  App,
  NotFoundPage,
} from './containers'

export default (
  <Route path="/" component={App}>
    {/*<IndexRoute component={CarList} />*/}
    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}

    {/*驾照相关 start*/}
    <IndexRoute getComponents={(nextState, cb) => {
      require.ensure([], (require) => {
        cb(null, require('./containers/drivingLicence/list').default)
      }, 'index')
    }} />
    <Route path="addDrivingLicence" getComponents={(nextState, cb) => {
      require.ensure([], (require) => {
        cb(null, require('./containers/drivingLicence/addLicence').default)
      }, 'index')
    }} />
    <Route path="editDrivingLicence/:licenceId" getComponents={(nextState, cb) => {
      require.ensure([], (require) => {
        cb(null, require('./containers/drivingLicence/editLicence').default)
      }, 'index')
    }} />
    {/*驾照相关 end*/}
    <Route path="*" component={NotFoundPage} />
  </Route>
);