/*
	驾照卡片组件
*/

import styles from './LicenceCard.scss'

const defaultProps = {
	isOwner:true,//是否本人驾照
	drivingLicenseId:"1234",//驾照id
	driverName:"lijun",//车主姓名
	drivingLicenseNo:"123456789012345678",//驾驶证号
	drivingFileNumber:"123456789012345678",//档案编号
	score:"12",//总分
	remainScore:"4",//剩余分数
	handleClick:()=>{},//编辑驾照
}

const LicenceCard = props => {
	//props = defaultProps
	return (
		<div className={styles.wrap}>
			<div className={styles.container}>
				<div className={styles.head}>
					<span className={styles.title}>中华人民共和国驾驶证</span>
				</div>
				<div className={styles.editBtn} onClick={()=>props.handleClick(props)}></div>
				<div className={styles.content}>
					<div className={styles.driverName}>驾驶人：{props.driverName}</div>
					<div>驾驶证号：{props.drivingLicenseNo.slice(0,4)+"**********"+props.drivingLicenseNo.slice(props.drivingLicenseNo.length-4)}</div>
					<div>档案编号：{props.drivingFileNumber.slice(0,4)+"****"+props.drivingFileNumber.slice(props.drivingFileNumber.length-4)}</div>
				</div>
				<div className={styles.scoreWrap}>
					<div className={styles.scoreContainer+" circle"}><div className={styles.score}>{props.remainScore}</div></div>
					<div className={styles.remainText}></div>
					<div className="score" style={{display:"none"}}>{props.remainScore}</div>
				</div>
			</div>
		</div>
	)
}

export default LicenceCard