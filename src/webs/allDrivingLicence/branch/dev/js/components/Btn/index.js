/*
* 按钮组件
* props={
	handleClick:()=>{}, //按钮点击执行的方法
	text:"添加车辆", //按钮显示文本
	disabled:true //是否可以点击
  }
*/

import styles from './index.scss'

const Btn = props => {
	let className = props.disabled?styles.btn_disabled:styles.btn
	return (
		<div 
			onClick={props.disabled?()=>{}:props.handleClick} 
			className={className}
		>
			{props.text}
		</div>
	)
}

export default Btn