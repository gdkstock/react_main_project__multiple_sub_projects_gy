/*
	车辆信息相关请求
*/
import {
  takeEvery,
  delay,
  takeLatest,
  buffers,
  channel,
  eventChannel,
  END
} from 'redux-saga';

import {
  put,
  call,
  take,
  fork,
} from 'redux-saga/effects'

import moment from 'moment'
import apiHelper from '../services/apiHelper';
import fetch from 'isomorphic-fetch'
import { Toast } from 'antd-mobile'
import { ChMessage } from '../utils/message.config'
import drivingLicenceService from '../services/drivingLicenceService'
import common from '../utils/common'

import {
  QUERY_DRIVINGLICENCE,
  QUERY_DRIVINGLICENCE_LIST,
  QUERY_ADD_DRIVINGLICENCE,
  QUERY_UPDATE_DRIVINGLICENCE,
  QUERY_DELETE_DRIVINGLICENCE,
  UPDATE_DRIVINGLICENCE_DATA,
  ADD_DRIVINGLICENCE_LIST,
  DELETE_DRIVINGLICENCE_DATA
} from '../actions/actionsTypes'

/*==========================查询单个驾照信息============================*/
function* fetchDrivingLicence(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(drivingLicenceService.getInfo, data)
    Toast.hide()
    if (result.code == "1000") {
      yield put({ type: UPDATE_DRIVINGLICENCE_DATA, data: result.data })
    } else {
      if (result.code != "4222") {
        Toast.info(result.msg || ChMessage.FETCH_FAILED, 2);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 2);
  }

}

function* watchDrivingLicenceInfo() {
  while (true) {
    let { param } = yield take(QUERY_DRIVINGLICENCE)
    yield fork(fetchDrivingLicence, param)
  }
}

/*==========================查询驾照列表信息============================*/
function* fetchDrivingLicenceList(action) {
  Toast.loading("", 0)
  try {
    const result = yield call(drivingLicenceService.getList, action.param)
    Toast.hide()
    if (result.code == "1000") {
      yield put({ type: ADD_DRIVINGLICENCE_LIST, data: result.data })
    } else {
      if (result.code != "4222") {
        Toast.info(result.msg || ChMessage.FETCH_FAILED, 2);
      }
    }
    if (action.callback) action.callback(result);
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 2);
    if (action.callback) action.callback(error);
  }

}

function* watchDrivingLicenceList() {
  yield takeLatest(QUERY_DRIVINGLICENCE_LIST, fetchDrivingLicenceList)
}

/*==========================添加驾照信息============================*/
function* fetchAddDrivingLicence(action) {
  Toast.loading("", 0);
  try {
    const result = yield call(drivingLicenceService.add, action.param)
    Toast.hide()
    if (result.code == "1000") {
      sessionStorage.removeItem("showConfirm")
      if (action.callback) action.callback(result);
    } else {
      if (result.code != "4222") {
        Toast.info(result.msg || ChMessage.FETCH_FAILED, 2);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 2);
    if (action.callback) action.callback(error);
  }
}

function* watchAddDrivingLicence() {
  yield takeLatest(QUERY_ADD_DRIVINGLICENCE, fetchAddDrivingLicence)
}

/*==========================修改驾照信息============================*/
function* fetchUpdateDrivingLicence(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(drivingLicenceService.update, data)
    Toast.hide()
    if (result.code == "1000") {
      //yield put({type:UPDATE_DRIVINGLICENCE_DATA,data:result.data}) 
      sessionStorage.removeItem("showConfirm");
      window.history.go(-1)
    } else {
      if (result.code != "4222") {
        Toast.info(result.msg || ChMessage.FETCH_FAILED, 2);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 2);
  }
}

function* watchUpdateDrivingLicence() {
  while (true) {
    let { param } = yield take(QUERY_UPDATE_DRIVINGLICENCE)
    yield fork(fetchUpdateDrivingLicence, param)
  }
}

/*==========================删除驾照信息============================*/
function* fetchDeleteDrivingLicence(data) {
  Toast.loading("", 0)
  try {
    const result = yield call(drivingLicenceService.delete, data)
    Toast.hide()
    if (result.code == "1000") {
      //删除驾照列表中数据
      yield put({ type: DELETE_DRIVINGLICENCE_DATA, data: { drivingLicenseId: data.drivingLicenseId } })
      sessionStorage.removeItem("showConfirm")
      window.history.go(-1)
    } else {
      if (result.code != "4222") {
        Toast.info(result.msg || ChMessage.FETCH_FAILED, 2);
      }
    }
  } catch (error) {
    Toast.hide()
    Toast.info(ChMessage.FETCH_FAILED, 2);
  }
}

function* watchDeleteDrivingLicence() {
  while (true) {
    let { param } = yield take(QUERY_DELETE_DRIVINGLICENCE)
    yield fork(fetchDeleteDrivingLicence, param)
  }
}


/*********监听所有驾照相关请求！*********/
export function* watchDrivingLicence() {
  yield [
    fork(watchDrivingLicenceInfo),
    fork(watchDrivingLicenceList),
    fork(watchAddDrivingLicence),
    fork(watchUpdateDrivingLicence),
    fork(watchDeleteDrivingLicence),
  ]
}