/*
* 添加驾照
*/
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Btn, Modal } from '../../components'
import { TxtGroup, DateGroup } from '../../components/Form'
import styles from './addLicence.scss'
import * as drivingLicenceActions from '../../actions/drivingLicenceActions'
import { Toast } from 'antd-mobile'
import moment from 'moment'
import common from '../../utils/common'

class AddLicence extends React.Component {
	constructor(props) {
		super(props);
		//设置初始状态

		//初始化状态	
		// this.state = this.getNewState({},this.props);
		this.state = {
			driverName: "",
			drivingLicenseNo: "",
			drivingFileNumber: "",
		}
	}


	componentWillMount() {
		//设置标题
		common.setViewTitle('添加驾照');
	}

	componentDidMount() {
		//组件加载完成

		//后退时显示确认对话框
		common.backShowConfirm(() => {
			let obj = {
				show: true, //显示对话框
				content: '编辑的信息尚未保存，确定退出？', //内容
				leftBtn: {
					name: '离开', //左按钮文字
					onClick: () => { sessionStorage.removeItem("showConfirm"); history.back() }  //点击左按钮
				},
				rightBtn: {
					name: '取消',//右按钮文字
					onClick: () => history.forward() //点击右按钮
				},
				onClose: () => history.forward() //点击关闭按钮
			}
			Modal.confirm(obj);
		})
	}

	componentDidUpdate() {
		//如果编辑驾照信息
		if (this.state.isDirty) {
			sessionStorage.setItem("showConfirm", 1);
		}
	}

	componentWillUnmount() {
		//离开添加驾照页记录页面信息
		sessionStorage.setItem("prevPageInfo", document.title)
	}

	//输入改变时验证数据
	preValiDate(e) {
		let target = e.target,
			name = target.name,
			len,
			value,
			isDirty = this.state.Dirty,
			currValue = target.value.trim();
		switch (name) {
			case "driverName":
				len = 32;
				break;
			case "drivingLicenseNo":
				currValue = currValue.toUpperCase();
				len = 18;
				break;
			case "drivingFileNumber":
				currValue = currValue.toUpperCase();
				len = 12;
				break;
		}
		value = this.cutInput(currValue, len);
		if (value != this.state[name]) {
			isDirty = true;
		}
		this.setState({
			[name]: value,
			isDirty
		})
	}

	//截取输入框输入文本长度
	cutInput(value, len) {
		let output = value;
		if (value.length > len) {
			output = value.slice(0, len)
		}
		return output
	}

	//提交数据
	submitInfo() {
		if (this.valiDate()) {
			const param = this.state
			this.props.actions.queryAddDrivingLicence(param, res => {
				if (res.code == 1000) {
					const { backNum } = this.props.params;
					if (backNum * -1 < 0) {
						// 后退指定的步数
						window.history.go(backNum * -1);
					} else {
						// 默认添加成功后 页面后退
						window.history.go(-1);
					}
				}
			})
		}
	}

	//点击提交时验证数据
	valiDate() {
		let {
			driverName,
			drivingLicenseNo,
			drivingFileNumber,
		} = this.state

		//验证姓名
		if (driverName.length == 0) {
			Toast.info("请输入姓名", 2)
			return false
		}

		//验证驾驶证号
		if (drivingLicenseNo.length == 0) {
			Toast.info("请输入驾驶证号", 2)
			return false
		} else if (!(drivingLicenseNo.length == 15 || drivingLicenseNo.length == 18)) {
			Toast.info("请输入正确驾驶证号", 2)
			return false
		}

		//验证档案编号
		if (drivingFileNumber.length == 0) {
			Toast.info("请输入档案编号", 2)
			return false
		} else if (drivingFileNumber.length != 12) {
			Toast.info("请输入正确的档案编号", 2)
			return false
		}

		return true
	}

	render() {
		//车主姓名
		const driverName = {
			groupId: "driverName",//唯一编码
			label: '车主姓名',//标签文本
			placeholder: "车主姓名",//占位符文本
			handleChange: (e) => this.preValiDate(e),//输入框文本改变操作
			type: "text", //输入框type
			value: this.state.driverName,//输入框值
		}

		//驾驶证号
		const drivingLicenseNo = {
			groupId: "drivingLicenseNo",//唯一编码
			label: '驾驶证号',//标签文本
			placeholder: "驾驶证号完整15位或18位",//占位符文本
			handleChange: (e) => this.preValiDate(e),//输入框文本改变操作
			type: "text", //输入框type
			hasIcon: true,//是否有提示icon
			iconImg: "./images/demo-jashiZhengHaoLen.png",//icon弹出图片
			value: this.state.drivingLicenseNo,//输入框值
		}

		//档案编号
		const drivingFileNumber = {
			groupId: "drivingFileNumber",//唯一编码
			label: '档案编号',//标签文本
			placeholder: "档案编号完整12位数",//占位符文本
			handleChange: (e) => this.preValiDate(e),//输入框文本改变操作
			type: "text", //输入框type
			hasIcon: true,//是否有提示icon
			iconImg: "./images/demo-danganBianHaoLen.png",//icon弹出图片
			value: this.state.drivingFileNumber,//输入框值
		}

		return (
			<div>
				<div className="wz_whiteSpace_30"></div>
				<div className="wz_list">
					<TxtGroup {...driverName} />
					<TxtGroup {...drivingLicenseNo} />
					<TxtGroup {...drivingFileNumber} />
				</div>
				<div className="wz_whiteSpace_50"></div>
				<div className={styles.remaindInfo}>支持全国驾照查分，数据来源当地交管局，当天扣分情况可能会有延迟</div>
				<div className="wz_whiteSpace_50"></div>
				<Btn text="保存并查询" handleClick={() => this.submitInfo()} />
			</div>
		)
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(drivingLicenceActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddLicence)

