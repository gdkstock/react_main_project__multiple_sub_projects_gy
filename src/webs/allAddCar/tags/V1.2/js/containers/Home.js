import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//antd
import { Toast } from 'antd-mobile'

//组件
import CarNumInput from '../components/inputs/CarNumInput'

//actons
import * as carsActions from '../actions/carsActions'

//通用工具类
import common from '../utils/common'
import config from '../config'
import { ChMessage } from '../utils/message.config'

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			carNumber: '' //车牌号码
		}
	}

	componentWillMount() {
		common.setViewTitle('违章查询')

		let { provinces } = this.props
		//请求车辆查询约束条件
		if (!provinces || provinces.length === 0) {
			//约束条件不存在
			Toast.loading('', 0)
			this.props.carsActions.queryCarCondition({}, result => {
				Toast.hide()
			})
		}

	}

	componentDidMount() {
		common.sendCxytj({
			eventId: 'h5_p_violation_EnterViolationlist'
		})
	}

	componentWillReceiveProps(nextProps) {
		//props发生改变

	}

	componentWillUnmount() {

	}

	toUrl(url) {
		window.location.href = common.getRootUrl() + url
	}

	/**
	* 重定向到违章办理首页
	*/
	toViolation() {
		window.location.replace(this.violationIndexUrl()) //重定向到违章页面
	}

	/**
	 * 违章办理首页
	 */
	violationIndexUrl(){
		return config.violationUrl + '?' + common.getUserInfoString();
	}

	/**
	 * H5首页
	 */
	H5IndexUrl(){
		return config.h5IndexUrl + '?' + common.getUserInfoString();
	}

	/**
	* 重定向到违章列表
	*/
	toViolationList(carId) {
		common.backToUrl(config.violationUrl) //后退时跳转到指定的页面
		let url = config.violationUrl + '#/violationList/' + carId
		window.location.replace(url) //重定向到违章页面
	}

	/**
	 * 检测输入的合法性
	 */
	checkInputs() {
		let carNumber = CarNumInput.carNum()
		let checkAZ = /[A-Z]/g.exec(carNumber.substr(1, 1)); //检查车牌号码是否以字母开头

		if (carNumber.length < 2) {
			Toast.info("车牌号码不能为空", 1);
		} else if (carNumber.length < 7 || carNumber.length > 8 || !checkAZ) {
			Toast.info("您输入的车牌号码有误", 1);
		} else {
			return true
		}
		return false
	}

	/**
	 * 点击查询按钮
	 */
	send() {
		common.sendCxytj({
			eventId: 'h5_e_violation_ClickQuery'
		})
		if (this.checkInputs()) {
			//输入合法，这里请求数据
			let carNumber = CarNumInput.carNum()
			this.props.carsActions.getCarInformation({ carNumber: carNumber }, result => {
				if (result && result.code == '1000') {
					let { carId, count, degree, money, tel, flag } = result.data
					switch (flag + '') {
						case '0': //无车辆信息
							if (carNumber.substr(0, 1) == '粤') {
								//粤牌车可以进行身份验证
								this.toUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							} else {
								this.toUrl(`addCar/${encodeURIComponent(carNumber)}`)
							}
							break;
						case '1': //存在车辆信息
							if (carNumber.substr(0, 1) != '粤' && count == -1 && !tel) {
								//非粤牌车 && 无违章 && 无手机号码
								this.toUrl(`addCar/${encodeURIComponent(carNumber)}`)
							} else {
								this.toUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							}

							break;
						case '2': //车辆已经存在车辆列表中
							this.toViolationList(carId)
							break;
						default:
							this.toUrl(`addCar/${encodeURIComponent(carNumber)}`)
					}

				} else {
					console.log("出错了")
					if (result.code == '4443') {
						//满三辆车
						Toast.info('违章查询最多支持绑定3辆车辆,请前往车辆列表删除后再继续查询', 3, () => this.toViolation())
					} else {
						Toast.info(result.msg || ChMessage.FETCH_FAILED)
					}
				}
			})
		}
	}

	render() {
		let { provinces } = this.props

		return (
			<div className='box whiteBg'>
				<div style={{ position: 'relative', zIndex: '2', padding: '.6rem .3rem', }}>
					<CarNumInput list={provinces} />
				</div>
				<div className='plr30'>
					<div className='btn' onClick={() => this.send()}>查询</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	provinces: state.provinces.result
})

const mapDispatchToProps = dispatch => ({
	carsActions: bindActionCreators(carsActions, dispatch)
})
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);