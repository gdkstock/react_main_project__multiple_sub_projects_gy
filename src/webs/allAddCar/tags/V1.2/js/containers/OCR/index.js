import React, { Component } from 'react';

//antd
import { Toast } from 'antd-mobile'

//style
import Style from './index.scss'

//servers
import imageService from '../../services/imageService'

//actions


//通用工具类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config'
require("lrz")

class OCR extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {
        common.setViewTitle('身份验证')
    }

    componentDidMount() {
        common.sendCxytj({
			eventId: 'h5_h_violation_InterPictureExplanation'
		})
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    /**
	 * 图片上传
	 */
    uploadImg(target, name) {
        common.sendCxytj({
			eventId: 'h5_e_violation_ClickPicture'
		})
        this._lrz(target.files[0], name);
    }

    /**
	 * 图片压缩上传
	 * @param e 图片资源
	 */
    _lrz(files, name) {
        Toast.loading('识别中', 0)
        try {
            let quality = 1;
            if (files.size > 1024 * 1024 * 5) {
                quality = .5;
            }
            else if (files.size > 1024 * 1024 * 2) {
                quality = .5;
            }
            else if (files.size > 1024 * 1024) {
                quality = .5;
            }
            else if (files.size > 1024 * 500) {
                quality = .4;
            }
            else if (files.size > 1024 * 100) {
                quality = .5;
            } else {
                quality = .7;
            }

            lrz(files, {
                width: 1024,
                quality: quality
            }).then((rst) => {
                // 处理成功会执行
                this.postImgToUploadImg(rst.base64, name);

            }).catch((err) => {
                // 处理失败会执行
                Toast.hide()
                Toast.info(ChMessage.UPLOAD_ERROR);
            }).always(() => {
                // 不管是成功失败，都会执行
                this.refs.imageInput.value = ''; //清空文件上传 避免上传同一张照片或是拍照时出现的图片无法展示的bug
            });
        } catch (e) {
            Toast.hide()
            Toast.info(ChMessage.UPLOAD_ERROR)
        }

    }

    /**
	 * 上传图片到服务器
	 * @param base64 base64图片
	 */
    postImgToUploadImg(base64, name) {
        //上传图片到服务器
        let uploadimgData = {
            content: base64.substr((base64.indexOf('base64,') + 7)), //只上传图片流
            format: 'jpg',
            channel: 'app',
            thumbnail: '100x100' //缩略图尺寸
            // watermark: true //加水印
        }
        imageService.uploadImg(uploadimgData).then(data => {
            if (data.code === 0 && data.data.url) {
                this.carCheckLicense(data.data.url) //识别车身架号
            } else {
                Toast.hide()
                Toast.info(data.msg || ChMessage.FETCH_FAILED);
            }
        }, () => {
            Toast.hide()
            Toast.info(ChMessage.FETCH_FAILED, 2);
        })
    }

    /**
	 * 行驶证图片识别接口
	 * @param {string} imgUrl 图片URL
	 */
    carCheckLicense(imgUrl) {
        sessionStorage.setItem('allAddCar_ORC', 'error') //默认返回上传错误

        imageService.carCheckLicense({
            imgUrl: imgUrl
        }).then(data => {
            Toast.hide()
            if (data.code == '1000' && data.data) {
                sessionStorage.setItem('allAddCar_ORC', JSON.stringify(data.data))
                Toast.success(ChMessage.OCR_SUCCESS, 1)
            } else {
                Toast.fail(ChMessage.OCR_FAILED, 2);
            }
        }, () => {
            Toast.hide()
            Toast.info(ChMessage.FETCH_FAILED, 2);
        }).then(() => window.history.back())
    }

    render() {
        return (
            <div className='box whiteBg'>
                <div className={Style.content}>
                    <h3>拍照识别说明</h3>
                    <p>请对准行驶证正面拍照上传，拍照时确保图像清晰，避免反光影响，以提高识别的正确率。</p>
                    <img src="./images/demo-ocr.png" alt="拍照识别示例" />
                </div>
                <div className={Style.btn + ' box'}>
                    <div className='btn'>拍照识别</div>
                    <input
                        ref='imageInput'
                        className="txt"
                        type="file"
                        accept="image/*"
                        name='imageInput'
                        onChange={(e) => this.uploadImg(e.target, name)}
                        style={{ opacity: '0' }}
                    />
                </div>
            </div>
        );
    }
}

export default OCR;