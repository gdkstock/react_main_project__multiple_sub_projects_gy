import React, { Component, PropTypes } from 'react';
import Style from './index.scss'
import CarPrefixList from 'app/components/common/carPrefixList'

//常用工具类
import common from '../../utils/common'

class CarNumInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            carPrefix: '粤', //默认车牌前缀
            showCarPrefixList: false, //显示车牌前缀列表
        }
        CarNumInput.carNum = () => this.getCarNum();
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    /**
     * 获取车牌前缀
     */
    getCarPrefix(name) {
        if (name) {
            this.setState({
                carPrefix: name,
                showCarPrefixList: false
            })
        } else {
            this.setState({
                showCarPrefixList: false
            })
        }
    }

    /**
     * 获取完整的车牌号码
     */
    getCarNum() {
        return this.state.carPrefix + this.refs.carNum.value.toUpperCase()
    }

    /**
     * 车牌号码 输入框 用户输入事件
     */
    inputOnInput(e) {
        // let value = e.target.value.replace(/[^A-Za-z0-9]*/g, '').toUpperCase().substr(0, 7);

        // setTimeout(() => {
        //     this.refs.carNum.value = value //只能输入七位的字母和数字
        // }, 0) //避免IOS中文输入法下的bug

        let value = e.target.value;
        // if (value.length > 7) {
        //     this.refs.carNum.value = e.target.value.toUpperCase().substr(0, 7) //最多只能输入七位
        // } //这里不做处理，避免中文输入法下最后一个中文无法输入

        if (value.length === 1) {
            //输入车牌
            common.sendCxytj({
                eventId: 'h5_e_violation_InputLicencenumber'
            })
        }
    }

    /**
     * 车牌号码 输入框 失去焦点
     */
    inputOnBlur(e) {
        this.refs.carNum.value = e.target.value.toUpperCase().substr(0, 7) //最多只能输入七位
    }

    componentWillMount() {

    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
        let { list } = nextProps
        let carPrefix = list && list.length > 0 ? list[0] : '粤'
        this.setState({
            carPrefix: carPrefix
        })
    }

    componentWillUnmount() {

    }

    render() {
        let _props = this.props

        return (
            <div className={Style.inputBox}>
                <div className={Style.carPrefix} onClick={() => this.setState({ showCarPrefixList: true })}>
                    <span>{this.state.carPrefix}<i></i></span>
                </div>
                <input
                    type='text'
                    style={{ minWidth: '5.4rem' }}
                    ref='carNum'
                    placeholder="请输入车牌号码"
                    onInput={e => this.inputOnInput(e)}
                    onBlur={e => this.inputOnBlur(e)}
                    maxLength={7}
                />
                <CarPrefixList {..._props} getName={(name) => this.getCarPrefix(name)} show={this.state.showCarPrefixList} />
            </div>
        );
    }
}

//使用context
CarNumInput.contextTypes = {
    router: React.PropTypes.object.isRequired
}

CarNumInput.defaultProps = {
    // list:[] //车牌前缀
};

export default CarNumInput;