/**
 * 弹窗六位数字的输入框
 */
import React, { Component } from 'react';

//样式
import Style from './index.scss'

//通用工具类
import common from '../../utils/common'

class AlertNumberInput extends Component {

    constructor(props) {
        super(props);

        this.state = {
            number: '', //用户输入的数字
        }
    }

    handleInput(input, inputLen) {
        input.value = input.value.replace(/[^A-Za-z0-9]*/g, '').toUpperCase().substr(0, inputLen) //只能输入8位字母或数字

        this.setState({
            number: input.value
        })

        if (input.value.length === 1) {
            let { title } = this.props
            if (title == '短信验证码') {
                common.sendCxytj({
                    eventId: 'h5_e_violation_InputVerificationcode'
                })
            } else if (title == '身份证验证') {
                common.sendCxytj({
                    eventId: 'h5_e_violation_InputId'
                })
            }
        } else if (input.value.length === inputLen) {
            //自动提交数据
            this.send()
        }
    }

    /**
     * 显示用户输入的数字
     * @param {*number} number 
     * @param {*number} inputLen 输入框长度
     */
    showNumber(number, inputLen) {
        number = number.substr(0, inputLen) //只显示前8位数字
        let numArr = [];
        for (let i = 0; i < inputLen; i++) {
            let num = ''
            if (number[i]) {
                num = number[i]//存在数字
            }
            numArr.push(num);
        }
        return (
            <ul style={inputLen === 6 ? { left: '.94rem' } : {}}>
                {
                    numArr.map((num, i) =>
                        <li key={`number-${i}`} className={i === number.length ? Style.curr : ''}>{num}</li>
                    )
                }
            </ul>
        )
    }

    close() {
        let { title } = this.props
        if (title == '短信验证码') {
            common.sendCxytj({
                eventId: 'h5_e_violation_CloseCodePopups'
            })
        } else if (title == '身份证验证') {
            common.sendCxytj({
                eventId: 'h5_e_violation_CloseIdPopups'
            })
        }
        let number = this.refs.number.value
        this.props.close(number) //返回输入的Number给父组件
    }

    send() {
        let number = this.refs.number.value
        this.props.onClick(number) //返回输入的Number给父组件
    }

    render() {
        let { number } = this.state
        let { title, subTitle, show, inputType } = this.props

        let inputLen = title === '身份证验证' ? 8 : 6; //输入框长度

        return (
            <div className='alert-bg' style={show ? { display: 'inherit' } : {}}>
                <div className="abs-center">
                    <div className={Style.alertBox}>
                        <div className={Style.closeIcon} onClick={() => this.close()}></div>
                        <h3>{title}</h3>
                        <p>{subTitle}</p>
                        <div className={Style.numIcons}>
                            <input ref="number" type={inputType || "url"} onInput={(e) => this.handleInput(e.target, inputLen)} autoFocus={true} />
                            {this.showNumber(number, inputLen)}
                        </div>
                        {/*{
                            number.length === 6
                                ? <div className="btn" onClick={() => this.send()}>提交</div>
                                : <div className="btn notClickBtn">提交</div>
                        }*/}
                    </div>
                </div>
            </div>
        )
    }
}

AlertNumberInput.defaultProps = {
    title: '身份证验证',
    subTitle: '请输入身份证号后8位',
    onClick: num => console.log('输入了：' + num),
    close: () => console.log('关闭'),
    inputType: 'url', //input标签的类型 默认url
    show: true,
}

export default AlertNumberInput