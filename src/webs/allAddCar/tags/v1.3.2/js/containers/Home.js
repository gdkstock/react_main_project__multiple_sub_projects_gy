import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//antd
import { Toast } from 'antd-mobile'

//组件
import CarNumInput from '../components/inputs/CarNumInput'
import OCR from './OCR/alertDemo'

//styles
import styles from './home/index.scss'

//actons
import * as carsActions from '../actions/carsActions'

//通用工具类
import common from '../utils/common'
import config from '../config'
import { ChMessage } from '../utils/message.config'

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			carNumber: '', //车牌号码
			showOCR: false, //显示OCR说明页
		}
	}

	componentWillMount() {
		common.setViewTitle('添加车辆')

		let { provinces } = this.props
		//请求车辆查询约束条件
		if (!provinces || provinces.length === 0) {
			//约束条件不存在
			Toast.loading('', 0)
			this.props.carsActions.queryCarCondition({}, result => {
				Toast.hide()
			})
		}

	}

	componentDidMount() {
		common.sendCxytj({
			eventId: 'h5_p_violation_EnterViolationlist'
		})
	}

	componentWillReceiveProps(nextProps) {
		//props发生改变

	}

	componentWillUnmount() {

	}

	toUrl(url) {
		this.props.router.push(url);
	}

	/**
	 * 违章办理首页
	 */
	violationIndexUrl() {
		return config.violationUrl + '?' + common.getUserInfoString();
	}

	/**
	 * H5首页
	 */
	H5IndexUrl() {
		return config.h5IndexUrl + '?' + common.getUserInfoString();
	}

	/**
	* 重定向到违章列表
	*/
	toViolationList(carId) {
		let url = config.violationUrl + '#/violationList/' + carId
		window.location.replace(url) //重定向到违章页面
	}

	/**
	 * 检测输入的合法性
	 */
	checkInputs() {
		let carNumber = CarNumInput.carNum()
		let checkAZ = /[A-Z]/g.exec(carNumber.substr(1, 1)); //检查车牌号码是否以字母开头

		if (carNumber.length < 2) {
			Toast.info("车牌号码不能为空", 1);
		} else if (carNumber.length < 7 || carNumber.length > 8 || !checkAZ) {
			Toast.info("您输入的车牌号码有误", 1);
		} else {
			return true
		}
		return false
	}

	/**
	 * 点击查询按钮
	 */
	send() {
		common.sendCxytj({
			eventId: 'h5_e_violation_ClickQuery'
		})
		if (this.checkInputs()) {
			//输入合法，这里请求数据
			let carNumber = CarNumInput.carNum()
			this.props.carsActions.getCarInformation({ carNumber: carNumber }, result => {
				if (result && result.code == '1000') {
					let { carId, count, degree, money, tel, flag } = result.data
					switch (flag + '') {
						case '0': //无车辆信息
							if (carNumber.substr(0, 1) == '粤') {
								//粤牌车可以进行身份验证
								this.toUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							} else {
								this.toUrl(`addCar/${encodeURIComponent(carNumber)}`)
							}
							break;
						case '1': //存在车辆信息
							if (carNumber.substr(0, 1) != '粤' && count == -1 && !tel) {
								//非粤牌车 && 无违章 && 无手机号码
								this.toUrl(`addCar/${encodeURIComponent(carNumber)}`)
							} else {
								this.toUrl(`authPage/${encodeURIComponent(carNumber)}/${count}/${degree}/${money}/${tel || ''}`)
							}

							break;
						case '2': //车辆已经存在车辆列表中
							this.toViolationList(carId)
							break;
						default:
							this.toUrl(`addCar/${encodeURIComponent(carNumber)}`)
					}

				} else {
					console.log("出错了")
					if (result.code == '4443') {
						//满三辆车
						Toast.info('违章查询最多支持绑定3辆车辆')
					} else {
						Toast.info(result.msg || ChMessage.FETCH_FAILED)
					}
				}
			})
		}
	}

	/**
	 * 显示OCR说明页
	 */
	showOCR() {
		localStorage.setItem('ocrMsg', 1) //不再显示提示文案

		this.setState({
			showOCR: true
		})

		common.sendCxytj({
			eventId: 'h5_e_violation_ClickPicture'
		})
	}

	/**
	 * 隐藏OCR说明页
	 */
	hideOCR() {
		this.setState({
			showOCR: false
		})
	}

	/**
	 * 获取OCR识别的内容
	 */
	getOCR() {
		this.hideOCR(); //隐藏OCR
		let carNumber = '';
		let allAddCar_ORC = sessionStorage.getItem('allAddCar_ORC')
		if (allAddCar_ORC) {
			try {
				allAddCar_ORC = JSON.parse(allAddCar_ORC) //这里可能出错
				carNumber = allAddCar_ORC.carNumber
			} catch (error) {
			}
		}
		this.props.router.push(`/addCar2${carNumber ? '/' + encodeURIComponent(carNumber) : ''}`);
	}

	/**
	 * 隐藏OCR提示信息
	 */
	hideOcrMsg() {
		localStorage.setItem('ocrMsg', 1) //不再显示提示文案
		this.refs.ocrMsg.className = 'hide'
	}

	render() {
		let { provinces } = this.props
		let { showOCR } = this.state

		return (
			<div className='box'>
				<div className='whiteBg'></div>
				<div className={styles.ocrBox}>
					<div ref='ocrMsg' className={!localStorage.getItem('ocrMsg') ? styles.ocrMsg : 'hide'}>
						<p>拍照自动识别行驶证</p>
						<i className={styles.ocrMsgClose} onClick={() => this.hideOcrMsg()}></i>
					</div>
					<div className={styles.ocrBtn} onClick={() => this.showOCR()}></div>
				</div>
				<div className={styles.homeImgBox}>
					<img src='./images/home-img.png' />
				</div>
				<div style={{ position: 'relative', zIndex: '2', padding: '.7rem .44rem .62rem', }}>
					<CarNumInput list={provinces} />
				</div>
				<div className='plr30 ptb30'>
					<div className='btn' onClick={() => this.send()}>立即添加</div>
				</div>

				{/*弹窗demo start*/}
				<div className={showOCR ? '' : 'hide'}>
					<OCR onClick={() => this.getOCR()} close={() => this.hideOCR()} />
				</div>
				{/*弹窗demo end*/}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	provinces: state.provinces.result
})

const mapDispatchToProps = dispatch => ({
	carsActions: bindActionCreators(carsActions, dispatch)
})
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);