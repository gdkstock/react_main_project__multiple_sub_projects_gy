import { combineReducers } from 'redux'
import user from './users'

import provinces from './provinces' //省份约束条件

const rootReducer = combineReducers({
    user, //用户信息表
    provinces,
});

export default rootReducer;
