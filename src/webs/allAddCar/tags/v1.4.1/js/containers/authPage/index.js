/**
 * 身份验证
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'

//样式
import Style from './index.scss'

//组件
import AuthList from './AuthList'
import AlertNumberInput from '../../components/authPage/AlertNumberInput'

//antd
import { Toast } from 'antd-mobile'

//actions
import * as carsActions from '../../actions/carsActions'

//通用工具类
import common from '../../utils/common'
import config from '../../config'
import { ChMessage } from '../../utils/message.config'

class AuthPage extends Component {
    constructor(props) {
        super(props);

        let { carNumber, count, degree, money, tel } = this.props.params
        carNumber = decodeURIComponent(carNumber) //url解码

        let authList = [{
            show: !!tel, //存在手机号码则显示
            icon: './images/card-phone.png',
            title: '手机验证码验证',
            subTitle: tel,
            onClick: () => this.getCaptcha(tel) //获取验证码
        }, {
            show: carNumber.substr(0, 1) == '粤', //只有粤牌车才支持身份证验证
            icon: './images/card-user.png',
            title: '身份证信息验证',
            subTitle: '输入身份证后8位验证',
            onClick: () => {
                common.sendCxytj({
                    eventId: 'h5_e_violation_ClickIdVerity'
                })
                this.updateKeyProps('alertNumberInputProps', {
                    title: '身份证验证',
                    subTitle: '请输入身份证号后8位',
                    onClick: num => this.checkCardId(num),
                    show: true,
                    inputType: 'url',
                })
            }
        }, {
            show: true,
            icon: './images/card-car.png',
            title: '车辆信息验证',
            subTitle: '输入或拍照获取车辆信息验证',
            onClick: () => {
                common.sendCxytj({
                    eventId: 'h5_e_violation_clicklicenceverity'
                })
                this.toUrl(`addCar/${encodeURIComponent(carNumber)}/-2`)
            }
        }]

        this.state = {
            carNumber: carNumber,
            userCode: '', //身份证后8位
            violation: {
                count, degree, money
            },
            tel,
            authList: authList,
            alertNumberInputProps: {
                title: '',
                subTitle: '',
                onClick: num => console.log('输入了：' + num),
                close: () => this.showAlertNumberInput('0'),
                show: false
            }
        }
    }

    componentWillMount() {
        common.setViewTitle('身份验证')
    }

    componentDidMount() {
        common.sendCxytj({
            eventId: 'h5_p_violation_EnterAuthentication'
        })
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {

    }

    toUrl(url) {
        this.props.router.push(url);
    }

    /**
     * 显示|隐藏弹窗数字输入框
     * @param {*string} type 0:隐藏（默认） 1：显示
     */
    showAlertNumberInput(type = '0') {
        let obj = {
            show: false
        }
        if (type == '0') {
            //隐藏
            obj = {
                show: false
            }
        } else if (type == '1') {
            //显示
            obj = {
                show: true
            }
        }

        this.updateKeyProps('alertNumberInputProps', obj)
    }

    /**
     * 更新state对应key值得对象
     * @param {*string} key state的key值 
     * @param {*object} obj key值对应的对象 
     */
    updateKeyProps(key, obj) {
        let oldData = this.state[key]
        this.setState({
            [key]: Object.assign({}, oldData, obj)
        })
    }

    /**
     * 获取验证码
     * @param {*string} tel 手机号码
     */
    getCaptcha(tel) {
        common.sendCxytj({
            eventId: 'h5_e_violation_ClickPhoneVerity'
        })

        let { carNumber } = this.state
        if (tel && tel.length == 11) {
            this.props.dispatch(carsActions.getCaptcha({ carNumber: carNumber }, result => {
                if (result.code == '1000') {
                    //短信发送成功
                } else {
                    Toast.info(result.msg || ChMessage.TEL_ERROR, 2, () => this.updateKeyProps('alertNumberInputProps', {
                        show: false
                    }))
                }
            }))
            this.updateKeyProps('alertNumberInputProps', {
                title: '短信验证码',
                subTitle: '已向' + tel + '的手机发送验证码',
                onClick: num => this.checkCaptcha(num),
                show: true,
                inputType: 'number',
            })
        } else {
            Toast.info(ChMessage.TEL_ERROR)
        }
    }

    /**
     * 手机号码验证
     */
    checkCaptcha(phoneCode) {
        common.sendCxytj({
            eventId: 'h5_e_violation_SubmitVerificationcode'
        })

        let { carNumber } = this.state
        this.props.dispatch(carsActions.checkCaptcha({
            "phoneCode": phoneCode,
            "carNumber": carNumber
        }, result => {
            if (result.code == '1000' && result.data.carId) {
                common.sendCxytj({
                    eventId: 'h5_e_violation_PhoneVeritySuccess'
                })
                Toast.info('验证成功', 2, () => this.toViolationList(result.data.carId));
            } else {
                common.sendCxytj({
                    eventId: 'h5_e_violation_PhoneVerityFail'
                })
                if (result.code == '4443') {
                    //满三辆车
                    Toast.info('违章查询最多支持绑定3辆车辆');
                } else {
                    Toast.info(result.msg || ChMessage.FETCH_FAILED);
                    this.showAlertNumberInput('0');
                    this.showAlertNumberInput('1');
                }
            }
        }))
    }

    /**
     * 身份证验证
     */
    checkCardId(userCode) {
        common.sendCxytj({
            eventId: 'h5_e_violation_SubmitId'
        })

        let { carNumber } = this.state
        this.props.dispatch(carsActions.checkCardId({
            "userCode": userCode,
            "carNumber": carNumber
        }, result => {
            if (result.code == '1000' && result.data.carId) {
                common.sendCxytj({
                    eventId: 'h5_e_violation_IdVeritySuccess'
                })
                Toast.info('验证成功', 2, () => this.toViolationList(result.data.carId));
            } else {
                common.sendCxytj({
                    eventId: 'h5_e_violation_IdVerityFail'
                })
                if (result.code == '4443') {
                    //满三辆车
                    Toast.info('违章查询最多支持绑定3辆车辆');
                } else {
                    Toast.info(result.msg || ChMessage.FETCH_FAILED);
                    this.showAlertNumberInput('0');
                    this.showAlertNumberInput('1');
                }
            }
        }))
    }

    /**
     * 重定向到违章列表
     */
    toViolationList(carId) {
        let url = this.violationIndexUrl() + '#/violationList/' + carId
        window.location.replace(url) //重定向到违章页面
    }

	/**
	 * 违章办理首页
	 */
    violationIndexUrl() {
        return config.violationUrl + '?' + common.getUserInfoString();
    }

	/**
	 * H5首页
	 */
    H5IndexUrl() {
        return config.h5IndexUrl + '?' + common.getUserInfoString();
    }

    render() {
        let { carNumber, violation, authList, alertNumberInputProps } = this.state
        let authNum = 0; //验证方式的数量
        authList.map(item => {
            if (item.show) {
                authNum++
            }
        })

        return (
            <div className='box'>
                <div className='whiteBg'></div>
                <div className={Style.carInfo}>
                    <div className={Style.carPre}><span>{carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}</span></div>
                    {
                        violation.count && violation.count > 0
                            ? <div className={Style.violation}>
                                <div>
                                    <h3>违章</h3>
                                    <p>{violation.count}</p>
                                </div>
                                <div>
                                    <h3>罚款</h3>
                                    <p>{violation.money}</p>
                                </div>
                                <div>
                                    <h3>扣分</h3>
                                    <p>{violation.degree}</p>
                                </div>
                            </div>
                            : ''
                    }
                </div>
                <div className='h12' style={{ background: '#f1f1f1' }}></div>
                <div className={Style.authTitle}>
                    <h3>验证车主身份成功后，即可查看违章详情</h3>
                    <p>请选择一种车辆验证方式：</p>
                </div>
                <AuthList list={authList} />

                {/*弹窗 state*/}
                {alertNumberInputProps.show
                    ? <AlertNumberInput {...alertNumberInputProps} />
                    : ''
                }
                {/*弹窗 end*/}
            </div>
        );
    }
}

AuthPage.propTypes = {

};

export default connect()(AuthPage);