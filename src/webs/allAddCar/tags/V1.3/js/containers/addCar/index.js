import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//andt
import { Toast, Switch } from 'antd-mobile'

//样式
import Style from './index.scss'

//组件
import OCR from '../OCR/alertDemo'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用组件类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'

class AddCar extends Component {
	constructor(props) {
		super(props);

		let { carNumber } = this.props.params
		this.state = {
			cities: {
				carCodeLen: 0,
				carEngineLen: 0
			}, //规则
			postData: {
				carNumber: carNumber,
				isSubscribe: 1,//是否开启消息通知 0:否，1:是
				userTelephone: ''
			}, //需要提交的数据
			showOCR: false, //显示OCR说明页
		}
	}

	componentWillMount() {
		common.setViewTitle('身份验证')

		let { provinces, params } = this.props
		let { carNumber } = params
		//设置约束条件的state
		this.setCitiesState(provinces, carNumber)

		//请求车辆查询约束条件
		if (!provinces || Object.keys(provinces).length === 0) {
			//约束条件不存在
			this.props.carsActions.queryCarCondition({}, result => { })
		}

	}

	componentDidMount() {

		this.inputDefaultValue() //存在驾驶证信息，则自动填充

		common.sendCxytj({
			eventId: 'h5_p_violation_EnterLicenceVerity'
		})
	}

	componentWillReceiveProps(nextProps) {
		//props发生改变

		let { provinces, params } = nextProps
		let { carNumber } = params

		this.setCitiesState(provinces, carNumber) //设置约束条件的state
	}

	componentWillUnmount() {

	}

	toUrl(url) {
		window.location.href = common.getRootUrl() + url
	}

	/**
	 * 设置约束条件的state
	 * @param {*object} provinces 
	 * @param {*string} carNumber 
	 */
	setCitiesState(provinces, carNumber) {
		let { cities } = this.state

		let newData = {}
		if (provinces) { //这里需要判断一下，避免从外部项目直接进来时出现错误
			if (provinces[carNumber.substr(0, 2)]) {
				//存在约束条件
				newData = {
					carCodeLen: provinces[carNumber.substr(0, 2)].carCodeLen,
					carEngineLen: provinces[carNumber.substr(0, 2)].carEngineLen
				}
			} else if (provinces[carNumber.substr(0, 1)]) {
				//直辖市等特殊城市
				newData = {
					carCodeLen: provinces[carNumber.substr(0, 1)].carCodeLen,
					carEngineLen: provinces[carNumber.substr(0, 1)].carEngineLen
				}
			} else {
				//不存在约束条件
				newData = {
					carCodeLen: 99,
					carEngineLen: 99
				}
			}
		}
		this.setState({
			cities: Object.assign({}, cities, newData)
		})
	}

	/**
	 * 输入框的默认值
	 */
	inputDefaultValue() {
		let allAddCar_ORC = sessionStorage.getItem('allAddCar_ORC')
		if (allAddCar_ORC) {
			sessionStorage.removeItem('allAddCar_ORC') //清除默认值
			try {
				allAddCar_ORC = JSON.parse(allAddCar_ORC) //这里可能出错
				let { carCodeLen, carEngineLen } = this.state.cities
				let { carNumber, carCode, engineNo } = allAddCar_ORC
				if (carNumber == this.state.postData.carNumber) {
					carCode = carCode ? carCode.substr(carCodeLen * -1) : '' //截取所需要的长度
					engineNo = engineNo ? engineNo.substr(carEngineLen * -1) : '' //截取所需要的长度

					this.setState({
						postData: Object.assign({}, this.state.postData, {
							carCode: carCode,
							engineNumber: engineNo
						})
					})

					this.refs.carCode.value = carCode
					this.refs.engineNumber.value = engineNo

					Toast.success(ChMessage.OCR_SUCCESS, 1)
					common.sendCxytj({
						eventId: 'h5_e_violation_PictureVeritySuccess'
					})
				} else {
					Toast.fail(ChMessage.OCR_FAILED, 1)
					common.sendCxytj({
						eventId: 'h5_e_violation_PictureVerityFail'
					})
				}

			} catch (error) {
				Toast.fail(ChMessage.OCR_FAILED, 1)
				common.sendCxytj({
					eventId: 'h5_e_violation_PictureVerityFail'
				})
			}
		}
	}

	handleInput(input, name, maxLength = 99) {
		let { postData } = this.state
		let setTime = false; //是否延迟
		let value = ''

		//只能输入字母和数字
		if (['carCode', 'engineNumber'].indexOf(name) !== -1) {
			value = input.value.replace(/[^A-Za-z0-9]*/g, '');
			setTime = true;
		}

		//转为大写字母
		if (['carCode', 'engineNumber'].indexOf(name) !== -1) {
			value = input.value.toLocaleUpperCase();
			setTime = true;
		}

		//只能输入数字
		if (['userTelephone'].indexOf(name) !== -1) {
			value = input.value.replace(/[^0-9]*/g, '');
			setTime = true;
		}

		if (value.length >= maxLength) {
			value = value.substr(0, maxLength) //输入框的最大长度
			input.value = value
		}

		//更新输入框的内容
		if (setTime) {
			setTimeout(() => {
				this.setState({
					postData: Object.assign({}, postData, {
						[name]: value
					})
				})
			}, 0)
		} else {
			this.setState({
				postData: Object.assign({}, postData, {
					[name]: value
				})
			})
		}

		if (input.value.length === 1) {
			if (name == 'carCode') {
				common.sendCxytj({
					eventId: 'h5_e_violation_InputVinNumber'
				})
			} else if (name == 'engineNumber') {
				common.sendCxytj({
					eventId: 'h5_e_violation_InputEngineNumber'
				})
			} else if (name == 'userTelephone') {
				common.sendCxytj({
					eventId: 'h5_e_violation_InputPhoneNumber'
				})
			}
		}
	}

	/**
	 * switch选择改变
	 */
	checkboxChange(checked) {
		let { postData } = this.state
		this.setState({
			postData: Object.assign({}, postData, {
				isSubscribe: checked ? 1 : 0
			})
		})
		if (!checked) {
			common.sendCxytj({
				eventId: 'h5_e_violation_CloseReminder'
			})
		}
	}

	/**
     * 显示demo图片
     */
	showDeomImg(name) {
		this.setState({
			demoImg: `./images/${name}`
		})
	}

    /**
     * 隐藏demo图片
     */
	hideDemoBox() {
		this.setState({
			demoImg: ''
		})
	}

	/**
	 * 根据cities的key值获取对应的名称
	 */
	getCitiesKeyName(key) {
		let names = {
			carCodeLen: '车身架号',
			carEngineLen: '发动机号',
			userTelephone: '手机号码'
		}

		return names[key] || key
	}

	/**
	 * 根据cities的key值获取保存资料的key值
	 */
	getCitiesKeytoPostKey(key) {
		let names = {
			carCodeLen: 'carCode',
			carEngineLen: 'engineNumber',
		}

		return names[key] || key
	}

	/**
	 * 检查提交的数据是否合法
	 */
	checkDatas() {
		let { postData, cities } = this.state

		if (Object.keys(cities).length > 0) {
			for (let key in cities) {
				if (cities[key] > 0) {
					//需要输入
					if (!postData[this.getCitiesKeytoPostKey(key)]) {
						Toast.info(`请输入${this.getCitiesKeyName(key)}`, 1)
						return false; //直接返回
					}
					if (postData[this.getCitiesKeytoPostKey(key)].length < cities[key] && cities[key] != 99) {
						//输入的长度不合法
						Toast.info(`请输入${this.getCitiesKeyName(key)}后${cities[key]}位`, 1)
						return false; //直接返回
					}
				}
			}
		}

		if (postData.isSubscribe == 1) {
			//开通提醒
			if (!postData.userTelephone) {
				Toast.info(`请输入手机号码`, 1)
				return false;
			}

			if (postData.userTelephone.length !== 11) {
				Toast.info(`请输入手机号码（11位数）`, 1)
				return false;
			}
		}

		return true
	}

    /**
     * 重定向到违章办理首页
     */
	toViolation() {
		window.location.replace(this.violationIndexUrl()) //重定向到违章页面
	}

	/**
	 * 违章办理首页
	 */
	violationIndexUrl() {
		return config.violationUrl + '?' + common.getUserInfoString();
	}

	/**
	 * H5首页
	 */
	H5IndexUrl() {
		return config.h5IndexUrl + '?' + common.getUserInfoString();
	}

	/**
	 * 显示OCR说明页
	 */
	showOCR() {
		localStorage.setItem('ocrMsg', 1) //不再显示提示文案

		this.setState({
			showOCR: true
		})

		common.sendCxytj({
			eventId: 'h5_e_violation_ClickPicture'
		})
	}

	/**
	 * 隐藏OCR说明页
	 */
	hideOCR() {
		this.setState({
			showOCR: false
		})
	}

	/**
	 * 获取OCR识别的内容
	 */
	getOCR() {
		this.hideOCR(); //隐藏OCR
		this.inputDefaultValue() //存在驾驶证信息，则自动填充
	}

	/**
	 * 提交数据
	 */
	send() {
		let { postData } = this.state

		if (postData.isSubscribe) {
			common.sendCxytj({
				eventId: 'h5_e_violation_Save_OpenReminder'
			})
		} else {
			common.sendCxytj({
				eventId: 'h5_e_violation_Save_CloseReminder'
			})
		}

		if (this.checkDatas()) {



			Toast.loading('', 0)
			carService.addCar(postData).then(result => {
				Toast.hide()
				if (result.code == '1000' && result.data.carId) {
					common.sendCxytj({
						eventId: 'h5_e_violation_LicenceVeritySuccess'
					})
					setTimeout(() => {
						//后退 默认后退一页 如果路由指定了后退的页数，则根据路由指定的页数后退
						let historyGo = this.props.params.historyGo || '-1';
						common.historyGo(historyGo);

						//重定向到违章页面
						let url = this.violationIndexUrl() + '#/violationList/' + result.data.carId
						window.location.replace(url)
					}, 200) //延迟跳转，避免埋点没有成功
				} else {
					common.sendCxytj({
						eventId: 'h5_e_violation_LicenceVerityFail'
					})
					if (result.code == '4443') {
						//满三辆车
						Toast.info('违章查询最多支持绑定3辆车辆,请前往车辆列表删除后再继续查询', 3, () => this.toViolation())
					} else {
						Toast.info(result.msg || ChMessage.FETCH_FAILED)
					}
				}
			}, () => {
				Toast.hide()
				Toast.info(ChMessage.FETCH_FAILED)
				common.sendCxytj({
					eventId: 'h5_e_violation_LicenceVerityFail'
				})
			})
		}
	}

	render() {
		let { carNumber } = this.props.params
		carNumber = decodeURIComponent(carNumber) //解码
		let { demoImg, cities, postData, showOCR } = this.state

		return (
			<div className='box whiteBg'>
				{/*表单 start*/}
				<div className='wz_headMsg'>以下信息仅供交管局查询，我们将严格保密</div>
				<div className='wz_list'>
					<div className="wz_item">
						<div className={!localStorage.getItem('ocrMsg') ? Style.ocrMsg : 'hide'}>
							<img src='./images/ocr-msg.png' />
						</div>
						<div className={Style.orcBtn} onClick={() => this.showOCR()}>
							<img src='./images/icon_scan.png' />
						</div>
						<div style={{ width: '6.04rem', position: 'relative' }}>
							<span className="label">车牌号</span>
							<input
								className="txt"
								type='text'
								value={carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}
								disabled
								style={{ background: 'rgba(255,255,255,0)' }}
							/>
						</div>
					</div>

					<div className={cities.carCodeLen ? "wz_item" : 'hide'}>
						<span className="label">车身架号<i className='icon-prompt' onClick={() => this.showDeomImg('alertDemoImg.png')}></i></span>
						<input
							ref='carCode'
							className={'txt ' + Style.input}
							placeholder={cities.carCodeLen == 99 ? '请输入完整车身架号' : `请输入车身架号后${cities.carCodeLen}位`}
							onChange={(e) => this.handleInput(e.target, 'carCode', cities.carCodeLen)}
							type='url'
						/>
					</div>

					<div className={cities.carEngineLen ? "wz_item" : 'hide'}>
						<span className="label">发动机号<i className='icon-prompt' onClick={() => this.showDeomImg('alertDemoImg.png')}></i></span>
						<input
							ref='engineNumber'
							className={'txt ' + Style.input}
							placeholder={cities.carEngineLen == 99 ? '请输入完整发动机号' : `请输入发动机号后${cities.carEngineLen}位`}
							onChange={(e) => this.handleInput(e.target, 'engineNumber', cities.carEngineLen)}
							type='url'
						/>
					</div>
				</div>
				<div className={Style.h24}></div>
				<div className="wz_list">
					<div className="wz_item">
						<span className="label">违章通知及年检提醒</span>
						<span className={Style.switch}>
							<Switch checked={postData.isSubscribe} onChange={(checked) => this.checkboxChange(checked)} />
						</span>
					</div>

					<div className="wz_item wz_item_after">
						<span className="label">手机号码</span>
						<input
							ref='userTelephone'
							className="txt"
							placeholder="请输入手机号码"
							onChange={(e) => this.handleInput(e.target, 'userTelephone', 11)}
							type="tel"
						/>
					</div>
				</div>
				<div className="plr30 pt50">
					<div className="btn" onClick={() => this.send()}>保存并查询</div>
				</div>
				<div className={Style.ftxt}>
					<p>点击保存并查询即表示同意<span onClick={() => window.location.href = '//daiban.cx580.com/agree/AgreeMent.html'}>用户服务协议</span></p>
				</div>
				{/*表单 end*/}

				{/*弹窗demo start*/}
				<div className={demoImg ? Style.demoBox : 'hide'} onClick={() => this.hideDemoBox()}>
					<div className="abs-center" style={{ padding: '0 .3rem' }}>
						<div className={Style.demoImgBox}>
							<img src={demoImg} />
						</div>
					</div>
				</div>

				<div className={showOCR ? '' : 'hide'}>
					<OCR onClick={() => this.getOCR()} close={() => this.hideOCR()} />
				</div>
				{/*弹窗demo end*/}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	provinces: state.provinces.data
})

const mapDispatchToProps = dispatch => ({
	carsActions: bindActionCreators(carsActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddCar)