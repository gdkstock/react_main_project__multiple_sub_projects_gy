import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//andt
import { Toast, Switch } from 'antd-mobile'

//样式
import Style from './index.scss'

//组件
import OCR from '../OCR/alertDemo'
import Popup from '../Popup/popup' //行驶证不在身边 弹窗组件引入

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

class AddCar extends Component {
	constructor(props) {
		super(props);

		let { carNumber } = this.props.params
		carNumber = carNumber ? decodeURIComponent(carNumber) : '' //解码
		this.state = {
			carNumber: carNumber,
			cities: {
				carCodeLen: 0,
				carEngineLen: 0
			}, //规则
			postData: {
				isSubscribe: 1,//是否开启消息通知 0:否，1:是
				userTelephone: sessionStorage.getItem('phone') || '',
			}, //需要提交的数据
			showOCR: false, //显示OCR说明页
			showBottom: false, //显示底部文字  “行驶证不在身边”
			showPopup: false, //显示行驶证不在身边 弹窗
			canUpdateCarNumber: this.props.location.pathname.indexOf('/addCar2') === 0, //判断是否允许修改车牌号码
		}
	}

	componentWillMount() {
		common.setViewTitle('身份验证');

		let { provinces, params } = this.props;

		//设置约束条件的state
		this.setCitiesState(provinces);

		//请求车辆查询约束条件
		if (!provinces || Object.keys(provinces).length === 0) {
			//约束条件不存在
			this.props.carsActions.queryCarCondition({}, result => {
				setTimeout(() => {
					// 键盘初始化
					this.keyboardInit();
				}, 100); // 延迟处理，因为setState是异步的
			})
		}

		//还原页面跳转协议之前的state
		let addCar_start = sessionStorage.getItem('addCar_start');
		if (addCar_start) {
			sessionStorage.removeItem('addCar_start'); //删除session 避免重复使用
			this.setState(Object.assign({}, this.state, JSON.parse(addCar_start))); //还原state
		}

		//请求添加闹钟约束
		carService.addClockConstraint().then(res => {
			if (res.code == '1000') {
				this.setState({
					showBottom: true
				})
			}
		});
	}

	componentDidMount() {

		this.inputDefaultValue() //存在驾驶证信息或手机号码，则自动填充

		common.sendCxytj({
			eventId: 'h5_p_violation_EnterLicenceVerity'
		})

		// 键盘初始化
		this.keyboardInit();
	}

	componentWillReceiveProps(nextProps) {
		//props发生改变

		let { provinces } = nextProps

		this.setCitiesState(provinces) //设置约束条件的state
	}

	componentWillUnmount() {
	}

	toUrl(url) {
		window.location.href = common.getRootUrl() + url
	}

	/**
	 * 设置约束条件的state
	 * @param {*object} provinces 约束条件不存在时默认从props中获取
	 */
	setCitiesState(provinces) {
		let { cities, carNumber } = this.state
		if (!provinces) {
			provinces = this.props.provinces;
		}

		let newData = {}

		//车牌号码不存在
		if (!carNumber || carNumber == 'undefined') {
			newData = {
				carCodeLen: 99,
				carEngineLen: 99
			}
			this.setState({
				cities: Object.assign({}, cities, newData)
			})

			return;
		}

		//这里需要判断一下，避免从外部项目直接进来时出现错误
		if (provinces && carNumber.length > 1) {
			if (provinces[carNumber.substr(0, 2)]) {
				//存在约束条件
				newData = {
					carCodeLen: provinces[carNumber.substr(0, 2)].carCodeLen,
					carEngineLen: provinces[carNumber.substr(0, 2)].carEngineLen
				}
			} else if (provinces[carNumber.substr(0, 1)]) {
				//直辖市等特殊城市
				newData = {
					carCodeLen: provinces[carNumber.substr(0, 1)].carCodeLen,
					carEngineLen: provinces[carNumber.substr(0, 1)].carEngineLen
				}
			} else {
				//不存在约束条件
				newData = {
					carCodeLen: 99,
					carEngineLen: 99
				}
			}
		}
		this.setState({
			cities: Object.assign({}, cities, newData)
		})
	}

	/**
	 * 修改车牌号码
	 */
	handleCarNumberInput(input) {
		const val = input.value.toLocaleUpperCase(); // 转为大写
		this.setState({
			carNumber: val.length > 8 ? val.substr(0, 8) : val
		}, () => {
			if (val.length > 1) {
				this.setCitiesState() //设置约束条件的state
			}
		})

	}

	/**
	 * 输入框的默认值
	 */
	inputDefaultValue() {
		let { cities, postData, canUpdateCarNumber } = this.state;

		let allAddCar_ORC = sessionStorage.getItem('allAddCar_ORC')
		if (allAddCar_ORC) {
			sessionStorage.removeItem('allAddCar_ORC') //清除默认值
			try {
				allAddCar_ORC = JSON.parse(allAddCar_ORC) //这里可能出错
				let { carCodeLen, carEngineLen } = cities
				let { carNumber, carCode, engineNo } = allAddCar_ORC

				//车牌号码一致或是允许修改车牌号码的情况下 允许自动填充input标签
				if (carNumber == this.state.carNumber || canUpdateCarNumber) {
					carCode = carCode ? carCode.substr(carCodeLen * -1) : '' //截取所需要的长度
					engineNo = engineNo ? engineNo.substr(carEngineLen * -1) : '' //截取所需要的长度

					//数据合并
					postData = Object.assign({}, postData, {
						carCode: carCode,
						engineNumber: engineNo
					})
					this.setState({
						carNumber,
						postData
					}, () => {
						// 重新初始化键盘
						this.keyboardInit();
					})

					Toast.success(ChMessage.OCR_SUCCESS, 1)
					common.sendCxytj({
						eventId: 'h5_e_violation_PictureVeritySuccess'
					})
				} else {
					Toast.fail(ChMessage.OCR_FAILED, 1)
					common.sendCxytj({
						eventId: 'h5_e_violation_PictureVerityFail'
					})
				}

			} catch (error) {
				Toast.fail(ChMessage.OCR_FAILED, 1)
				common.sendCxytj({
					eventId: 'h5_e_violation_PictureVerityFail'
				})
			}
		}
	}

	/**
	 * switch选择改变
	 */
	checkboxChange(checked) {
		let { postData } = this.state
		this.setState({
			postData: Object.assign({}, postData, {
				isSubscribe: checked ? 1 : 0
			})
		})
		if (!checked) {
			common.sendCxytj({
				eventId: 'h5_e_violation_CloseReminder'
			})
		}
	}

	/**
     * 显示demo图片
     */
	showDeomImg(name) {
		this.setState({
			demoImg: `./images/${name}`
		})
	}

    /**
     * 隐藏demo图片
     */
	hideDemoBox() {
		this.setState({
			demoImg: ''
		})
	}

	/**
	 * 根据cities的key值获取对应的名称
	 */
	getCitiesKeyName(key) {
		let names = {
			carCodeLen: '车身架号',
			carEngineLen: '发动机号',
			userTelephone: '手机号码'
		}

		return names[key] || key
	}

	/**
	 * 根据cities的key值获取保存资料的key值
	 */
	getCitiesKeytoPostKey(key) {
		let names = {
			carCodeLen: 'carCode',
			carEngineLen: 'engineNumber',
		}

		return names[key] || key
	}

	/**
	 * 检查提交的数据是否合法
	 */
	checkDatas() {
		const { postData, cities } = this.state;
		const { provincesResult } = this.props;
		const { carNumber } = postData;
		const checkAZ = /[A-Z]/g.exec(carNumber.substr(1, 1)); //检查车牌号码是否以字母开头
		const carNumberPre = carNumber.substr(0, 1); // 车牌前缀

		// 校验车牌号码
		if (carNumber == '') {
			Toast.info('请输入车牌号码', 1);
			return false;
		} else if (provincesResult && provincesResult.length > 0 && provincesResult.indexOf(carNumberPre) === -1) {
			// 存在约束条件 并且 不支持当前省份
			Toast.info("暂不提供该城市违章查询请求", 1);
			return false;
		} else if (carNumber.length < 7 || carNumber.length > 8 || !checkAZ) {
			Toast.info("您输入的车牌号码有误", 1);
			return false;
		}

		// 校验需要补充的资料资讯
		if (Object.keys(cities).length > 0) {
			for (let key in cities) {
				if (cities[key] > 0) {
					//需要输入
					if (!postData[this.getCitiesKeytoPostKey(key)]) {
						Toast.info(`请输入${this.getCitiesKeyName(key)}`, 1)
						return false; //直接返回
					}
					if (postData[this.getCitiesKeytoPostKey(key)].length < cities[key] && cities[key] != 99) {
						//输入的长度不合法
						Toast.info(`请输入${this.getCitiesKeyName(key)}后${cities[key]}位`, 1)
						return false; //直接返回
					}
				}
			}
		}

		if (postData.isSubscribe == 1) {
			//开通提醒
			if (!postData.userTelephone) {
				Toast.info(`请输入手机号码`, 1);
				return false;
			}

			if (postData.userTelephone.length !== 11) {
				Toast.info(`请输入手机号码（11位数）`, 1);
				return false;
			}

			if (!common.checkTelephone(postData.userTelephone)) {
				Toast.info('请输入正确的手机号码', 2);
				return false;
			}
		}

		return true
	}

	/**
	 * 跳转到协议页面
	 */
	toXieyi() {
		sessionStorage.setItem("addCar_start", JSON.stringify(this.state)); //保存当前start
		jsApi.pushWindow('https://daiban.cx580.com/agree/AgreeMent.html')
	}

	/**
	 * 违章办理首页
	 */
	violationIndexUrl() {
		return config.violationUrl;
	}

	/**
	 * H5首页
	 */
	H5IndexUrl() {
		return config.h5IndexUrl;
	}

	/**
	 * 显示OCR说明页
	 */
	showOCR() {
		localStorage.setItem('ocrMsg', 1) //不再显示提示文案

		this.setState({
			showOCR: true
		})

		common.sendCxytj({
			eventId: 'h5_e_violation_ClickPicture'
		})
	}

	/**
	 * 隐藏OCR说明页
	 */
	hideOCR() {
		this.setState({
			showOCR: false
		})
	}

	/**
	 * 获取OCR识别的内容
	 */
	getOCR() {
		this.hideOCR(); //隐藏OCR
		this.inputDefaultValue() //存在驾驶证信息，则自动填充
	}

	/**
	 * 提交数据
	 */
	send() {
		let { carNumber, postData } = this.state

		postData.carNumber = carNumber;

		if (postData.isSubscribe) {
			common.sendCxytj({
				eventId: 'h5_e_violation_Save_OpenReminder'
			})
		} else {
			common.sendCxytj({
				eventId: 'h5_e_violation_Save_CloseReminder'
			})
		}

		if (this.checkDatas()) {
			Toast.loading('', 0);
			// 避免点击过快时，键盘没有收起
			if (cxyKeyboard.isShow) {
				cxyKeyboard.hide();
			}
			carService.addCar(postData).then(result => {
				Toast.hide()
				if (result.code == '1000' && result.data.carId) {
					common.sendCxytj({
						eventId: 'h5_e_violation_LicenceVeritySuccess'
					})

					try {
						window.cx580.jsApi.call({
							"commandId": "",
							"command": "sendBroadcast",
							"data": {
								'eventId': 'H5_updateCar'
							}
						}, function (data) {
						});
					} catch (error) {
						//广播发送失败
					}

					Toast.info('验证成功', 2, () => {
						//后退 默认后退一页 如果路由指定了后退的页数，则根据路由指定的页数后退
						let historyGo = this.props.params.historyGo || '-1';
						common.historyGo(historyGo);

						//重定向到违章页面
						let url = this.violationIndexUrl() + '#/violationList/' + result.data.carId
						window.location.replace(url);
					});
				} else {
					common.sendCxytj({
						eventId: 'h5_e_violation_LicenceVerityFail'
					})
					if (result.code == '4443') {
						//满三辆车
						Toast.info('违章查询最多支持绑定3辆车辆');
					} else {
						Toast.info(result.msg || ChMessage.FETCH_FAILED)
					}
				}
			}, () => {
				Toast.hide()
				Toast.info(ChMessage.FETCH_FAILED)
				common.sendCxytj({
					eventId: 'h5_e_violation_LicenceVerityFail'
				})
			})
		}
	}

	showPopup() {
		this.setState({
			showPopup: true,
		})
	}

	hidePopup(data) {
		const param = {
			showPopup: false
		};
		if (data && data.showBottom !== undefined) {
			param.showBottom = data.showBottom;
		}
		this.setState(param);
	}

    /**
     * 键盘实例初始化
     */
	keyboardInit() {
		const { carNumber, cities, postData } = this.state;
		const inputs = [
			{
				selectors: '#carCodeInput',
				type: 'URL',
				maxLength: cities.carCodeLen * 1 || 99,
				placeholder: cities.carCodeLen == 99 ? '请输入完整车身架号' : `请输入车身架号后${cities.carCodeLen}位`,
				value: postData.carCode || '',
			},
			{
				selectors: '#engineNumberInput',
				type: 'URL',
				maxLength: cities.carEngineLen * 1 || 99,
				placeholder: cities.carEngineLen == 99 ? '请输入完整发动机号' : `请输入发动机号后${cities.carEngineLen}位`,
				value: postData.engineNumber || '',
			},
			{
				selectors: '#userTelephoneInput',
				type: 'number',
				maxLength: 11,
				placeholder: '请输入手机号码',
				value: postData.userTelephone || '',
			},
			{
				selectors: '#carNumberInput',
				type: 'carNumberPre',
				maxLength: 8,
				placeholder: '输入车牌号码',
				value: carNumber || '',
			},
		];
		cxyKeyboard.init({ inputs });

		// 内容发生改变
		cxyKeyboard.onChange = (value, keyboardId) => this.keyboardChange(value, keyboardId);
	}

    /**
     * 显示键盘
     * @param {string} id 
     */
	showKeyboard(id) {
		const param = {
			selectors: id,
			animation: !cxyKeyboard.isShow, // 键盘不存在时则显示动画
		}

		// 键盘如果存在，则切换键盘。不存在则显示键盘
		cxyKeyboard.show(param, cxyKeyboard.isShow);
	}

	/**
	 * 键盘按键被点击
	 * @param {string} value 
	 * @param {string} keyboardId 
	 */
	keyboardChange(value, keyboardId) {
		const { postData } = this.state;

		// 车身架号或发动机号
		if (keyboardId === '#carCodeInput' || keyboardId === '#engineNumberInput') {
			// 获取key
			const key = keyboardId === '#carCodeInput' ? 'carCode' : 'engineNumber';

			// 保存数据
			this.setState({
				postData: Object.assign({}, postData, {
					[key]: value.toLocaleUpperCase() // 转为大写
				})
			})
		} else if (keyboardId === '#userTelephoneInput') {
			// 手机号码
			this.setState({
				postData: Object.assign({}, postData, {
					userTelephone: value
				})
			})
		} else if (keyboardId === '#carNumberInput') {

			if (value.length === 1 && !window.addCar_authSwitchKeyboard) {
				cxyKeyboard.switchKeyboard('ABC'); // 仅在首次自动切换键盘
			}

			window.addCar_authSwitchKeyboard = 1;

			// 车牌号码
			this.setState({
				carNumber: value.toLocaleUpperCase() // 转为大写
			}, () => {
				if (value.length > 1) {
					this.setCitiesState(); // 设置约束条件的state
					setTimeout(() => {
						this.keyboardInit(); // 异步重新初始化输入框
					}, 10);
				}
			})
		}
	}

	render() {
		let { carNumber, demoImg, cities, postData, showOCR, showPopup, canUpdateCarNumber } = this.state

		return (
			<div className='box' key={canUpdateCarNumber ? 'addcar2' : 'addcar'}>
				{/*表单 start*/}
				<div className='wz_headMsg'>以下信息仅供交管局查询，我们将严格保密</div>
				<div className='wz_list'>
					<div className="wz_item">
						<div className={!localStorage.getItem('ocrMsg') ? Style.ocrMsg : 'hide'}>
							<img src='./images/ocr-msg.png' />
						</div>
						<div className={Style.orcBtn} onClick={() => this.showOCR()}>
							<img src='./images/icon_scan.png' />
						</div>
						<div style={{ width: '6.04rem', position: 'relative' }}>
							<span className="label">车牌号</span>
							{
								canUpdateCarNumber ?
									<div className={'txt ' + Style.cxyInput} id='carNumberInput' onClick={() => this.showKeyboard('#carNumberInput')}></div>
									: <input
										className={'txt ' + Style.input}
										type='text'
										value={canUpdateCarNumber ? carNumber : carNumber.substr(0, 2) + ' ' + carNumber.substr(2)}
										placeholder='输入车牌号码'
										disabled={canUpdateCarNumber ? false : true}
										style={{ background: 'rgba(255,255,255,0)' }}
										onChange={e => canUpdateCarNumber ? this.handleCarNumberInput(e.target) : false}
									/>
							}
						</div>
					</div>

					{/* 自定义键盘 */}
					<div className={cities.carCodeLen ? "wz_item" : 'hide'}>
						<span className="label">车身架号<i className='icon-prompt' onClick={() => this.showDeomImg('alertDemoImg.png')}></i></span>
						<div className={'txt ' + Style.cxyInput} id='carCodeInput' onClick={() => this.showKeyboard('#carCodeInput')}></div>
					</div>

					<div className={cities.carEngineLen ? "wz_item" : 'hide'}>
						<span className="label">发动机号<i className='icon-prompt' onClick={() => this.showDeomImg('alertDemoImg.png')}></i></span>
						<div className={'txt ' + Style.cxyInput} id='engineNumberInput' onClick={() => this.showKeyboard('#engineNumberInput')}></div>
					</div>
				</div>
				<div className={Style.h24}></div>
				<div className="wz_list">
					<div className="wz_item">
						<span className="label">违章通知及年检提醒</span>
						<span className={Style.switch}>
							<Switch checked={postData.isSubscribe} onChange={(checked) => this.checkboxChange(checked)} />
						</span>
					</div>

					{/* 自定义键盘 */}
					<div className="wz_item wz_item_after">
						<span className="label">手机号码</span>
						<div className={'txt ' + Style.cxyInput} id='userTelephoneInput' onClick={() => this.showKeyboard('#userTelephoneInput')}></div>
					</div>
				</div>
				<div className="plr30 pt50">
					<div className="btn" onClick={() => this.send()}>保存并查询</div>
				</div>
				<div className={Style.ftxt}>
					<p>点击保存并查询即表示同意<span onClick={() => this.toXieyi()}>用户服务协议</span></p>
				</div>
				{/*表单 end*/}

				{/*弹窗demo start*/}
				<div className={demoImg ? Style.demoBox : 'hide'} onClick={() => this.hideDemoBox()}>
					<div className="abs-center" style={{ padding: '0 .3rem' }}>
						<div className={Style.demoImgBox}>
							<img src={demoImg} />
						</div>
					</div>
				</div>

				<div className={showOCR ? '' : 'hide'}>
					<OCR onClick={() => this.getOCR()} close={() => this.hideOCR()} />
				</div>
				{/*弹窗demo end*/}

				{/* 底部文字 行驶证不在身边 */}
				<div onClick={() => this.showPopup()} className={this.state.showBottom ? Style.bottomMessage : 'hide'}>行驶证不在身边？</div>

				{/* 行驶证不在身边 弹窗start */}
				{showPopup && <Popup closePopup={(data) => this.hidePopup(data)} />}
				{/* 行驶证不在身边 弹窗end */}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	provinces: state.provinces.data,
	provincesResult: state.provinces.result,
})

const mapDispatchToProps = dispatch => ({
	carsActions: bindActionCreators(carsActions, dispatch)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddCar)