/**
 * 配置
 */
import common from './utils/common'

const userType = sessionStorage.getItem('userType') || common.getUrlKeyValue('userType') || common.getJsApiUserType() || 'alipay'
const authType = sessionStorage.getItem('authType') || common.getUrlKeyValue('authType') || common.getJsApiUserType() || 'alipay'
const isPre = window.location.host.indexOf('pre.cx580.com') > -1; //预发布
const production = window.location.host.indexOf('daiban.cx580.com') > -1 || isPre; //是否为生产环境或预发布环境
const authUrl = (production ? 'https://auth.cx580.com/Auth.aspx' : 'http://testauth.cx580.com/Auth.aspx') + `?userType=${userType}&authType=${authType}&clientId=CheWu&redirect_uri=`;

export default {
    production: production, //是否为生产环境
    baseApiUrl: production ? window.location.protocol + "//" + window.location.host + "/" : "http://webtest.cx580.com:9021/", //接口地址
    authUrl: authUrl,//单点登录地址 回调地址需自行补全

    //违章首页路径
    violationUrl: production
        ? `${window.location.protocol + "//" + window.location.host}/Violation1.3/index.html`
        : `http://webtest.cx580.com:9021/Violation/index.html`,

    //H5首页路径
    h5IndexUrl: production
        ? (isPre
            ? `http://pre.cx580.com:59026/H5Index_v1.3/index.html`
            : window.location.protocol + '//h5index.cx580.com:9027/H5Index_v1.3/index.html'
        )
        : `http://webtest.cx580.com:9021/H5Index/index.html`,
}