import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//antd
import { Toast } from 'antd-mobile'
import { DatePickerView } from 'ys-antd-mobile2'

//actions
import * as carsActions from '../../actions/carsActions'

//servers
import carService from '../../services/carService'

//通用类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config.js'
import config from '../../config'
import jsApi from '../../utils/jsApi'

//样式
import Style from './popup.scss'

/*
 * 时间格式化 format() 方法
 */
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(), //day
        "h+": this.getHours(), //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
        "S": this.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
}

/**
 * 获取最大和最小时间（日期格式）
 */
const getDates = () => {
    const date = new Date();
    let m = date.getMinutes();
    let lastNum = (m + '').substr(-1);
    let k = 0;
    if (lastNum < 5) {
        k = 5 - (lastNum * 1);
    } else if (lastNum < 10) {
        k = 10 - (lastNum * 1);
    }
    m += k;

    let minDate = new Date(date.setMinutes(m))
    let maxDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 30, date.getHours(), m)

    return {
        minDate,
        maxDate
    }
}

class Popup extends Component {

    constructor(props) {
        super(props)

        const { minDate, maxDate } = getDates();
        this.state = {
            // 手机号码
            phoneNumber: sessionStorage.getItem('phone') || '',

            // 当前选中的日期(Date类型)
            pickerValue: minDate,

            // 最小可选择的日期
            minDate,

            // 最大可选择的日期
            maxDate,

            clockDate: '',

            //弹框出现时，弹框向上移动避免键盘遮挡按钮
            popupFloat: false,
        }
    }

    componentWillMount() {

        this._addListener();

    }

    componentDidMount() {
        // 键盘初始化
        this.keyboardInit();
    }

    componentWillUnmount() {
        this._removeListener();
    }

    componentWillReceiveProps(nextProps) {

    }

    
    /**
    * 添加popupFloat记录是否需添加样式
    * 键盘显示时，弹框向上移动避免遮挡按钮
    * 键盘不显示时，弹框回到原来的位置
    */

    _addListener() {
        //键盘隐藏监听
        window.Popup_cxyKeyboard_hide = () => this.setState({ popupFloat: false, });
        window.addEventListener('cxyKeyboard_hide', window.Popup_cxyKeyboard_hide);
    }

    _removeListener() {
        //清除键盘隐藏监听
        window.removeEventListener('cxyKeyboard_hide', window.Popup_cxyKeyboard_hide);
    }

    /*
    * 点击保存闹钟按钮
    */
    saveClock(e) {
        e.stopPropagation();
        const { pickerValue, phoneNumber } = this.state;

        /*
         *  闹钟时间格式化 'yyyy-MM-dd hh:mm'
         */
        const DPV_value = pickerValue.format('yyyy-MM-dd hh:mm')

        /*
         *  输入框  手机号验证
         */
        if (!common.checkTelephone(phoneNumber)) {
            return Toast.info('请输入正确的手机号码', 2);
        } else {
            const postData = {
                'phone': phoneNumber,
                'warnTime': DPV_value
            }

            this.addClock(postData);
        }
    }

    /**
     * 发送闹钟请求
     * @param {object} postData 提交的参数
     */
    addClock(postData) {
        Toast.loading('', 0);
        carService.addClock(postData).then(res => {
            Toast.hide()
            if (res.code == '1000') {
                Toast.success(res.msg, 2);
                const { closePopup } = this.props;
                closePopup && closePopup({
                    showBottom: false
                }); // 提醒成功后才关闭弹窗
            } else {
                Toast.fail(res.msg, 2);
            }
        }, () => {
            Toast.hide()
            Toast.info(ChMessage.FETCH_FAILED)
        })
    }

    /*
    * DatePickerView 组件开始
    */

    DPV_OnChange = (pickerValue) => {
        this.setState({
            pickerValue,
        });
    }

    /*
    * DatePickerView 组件结束
    */


    /**
     * 键盘实例初始化
     */
    keyboardInit() {
        const { phoneNumber } = this.state;
        const inputs = [
            {
                selectors: '#phoneNumber',
                type: 'number',
                maxLength: 11,
                placeholder: '请输入您的手机号码',
                value: phoneNumber || '',
            }
        ];
        cxyKeyboard.init({ inputs });

        // 内容发生改变
        cxyKeyboard.onChange = (value, keyboardId) => {
            this.setState({
                phoneNumber: value
            })
        };
    }

    /**
     * 显示键盘
     * @param {string} id 
     */
    showKeyboard(id) {
        const param = {
            selectors: id,
            animation: !cxyKeyboard.isShow, // 键盘不存在时则显示动画
        }

        // 键盘如果存在，则切换键盘。不存在则显示键盘
        cxyKeyboard.show(param, cxyKeyboard.isShow);

        // 异步显示，避免因为CSS上移后点击到底部的按钮
        setTimeout(() => {
            this.setState({ popupFloat: true, });
        }, 0);
    }

    render() {
        const { closePopup } = this.props;
        const { pickerValue, phoneNumber, popupFloat, minDate, maxDate } = this.state;

        return (

            <div className={Style.box}>
                <div id='popup' className={popupFloat ? Style.popupUp : Style.popup}>
                    <p className={Style.title}>行驶证不在身边</p>
                    <p className={Style.content}>您可设置车辆添加提醒，我们将以短信的方式提醒您添加爱车，不错过限行、违章信息</p>
                    {/*表单 start*/}
                    <div className={Style.pvBox}>
                        {/* DatePickerView组件 */}
                        <DatePickerView
                            name='datepickerview'
                            value={pickerValue}
                            onChange={this.DPV_OnChange}
                            mode='datetime'
                            minuteStep={5}
                            minDate={minDate}
                            maxDate={maxDate}
                            prefixCls={'am-picker control-col ' + Style.myAmpicker}
                        />
                    </div>
                    <div className={Style.inputGroups} onClick={() => this.showKeyboard('#phoneNumber')}>
                        <span className={phoneNumber ? Style.label : 'hide'}>手机号：</span>
                        <div id='phoneNumber' className={phoneNumber ? Style.input : Style.inputNoValue}></div>
                    </div>
                    <div className={Style.btnGroups}>
                        <button className={Style.noBtn} onClick={() => closePopup && closePopup()}>暂不添加</button>
                        <button className={Style.yesBtn} onClick={(e) => this.saveClock(e)}>保存闹钟</button>
                    </div>
                    {/*表单 end*/}
                </div>
            </div>
        )
    }
}

Popup.defaultProps = {
    closePopup: () => console.log('点击了关闭按钮'),
}

export default Popup
