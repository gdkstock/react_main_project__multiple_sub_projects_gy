import React, { Component, PropTypes } from 'react';
import Style from './index.scss'

//常用工具类
import common from '../../utils/common'

class CarNumInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            carPrefix: '粤', // 默认车牌前缀
            showCarPrefixList: false, // 显示车牌前缀列表
        }
        CarNumInput.carNum = () => this.getCarNum();
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.keyboardInit(); // 键盘初始化
    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount() {
        if (cxyKeyboard.isShow) {
            cxyKeyboard.hide();
        }
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    /**
     * 获取车牌前缀
     */
    getCarPrefix(name) {
        if (name) {
            this.setState({
                carPrefix: name,
                showCarPrefixList: false
            })
        } else {
            this.setState({
                showCarPrefixList: false
            })
        }
    }

    /**
     * 获取完整的车牌号码
     */
    getCarNum() {
        return this.state.carPrefix + cxyKeyboard.inputs['#carNumberKeyboard'].value;
    }

    /**
     * 车牌号码 输入框 用户输入事件
     */
    inputOnInput(e) {
        let value = e.target.value;

        if (value.length === 1) {
            //输入车牌
            common.sendCxytj({
                eventId: 'h5_e_violation_InputLicencenumber'
            })
        }
    }

    /**
     * 车牌号码 输入框 失去焦点
     */
    inputOnBlur(e) {
        this.refs.carNum.value = e.target.value.toUpperCase().substr(0, 7) //最多只能输入七位
    }

    /**
     * 键盘实例初始化
     */
    keyboardInit() {
        const inputs = [
            {
                selectors: '#carPrefix',
                type: 'carNumberPre',
                maxLength: 1,
                value: '',
                showDoneBtn: true,
            }, {
                selectors: '#carNumberKeyboard',
                placeholder: '输入车牌号码',
                type: 'ABC',
                maxLength: 7,
                value: '',
                showDoneBtn: true,
            },
        ];
        cxyKeyboard.init({ inputs });

        // 内容发生改变
        cxyKeyboard.onChange = (value, keyboardId) => {
            if (keyboardId === '#carPrefix' && value) {
                this.setState({
                    carPrefix: value
                });

                // 修改光标位置
                cxyKeyboard.cursorIndex = cxyKeyboard.inputs['#carNumberKeyboard'] ? cxyKeyboard.inputs['#carNumberKeyboard'].length : 0;
                // 切换键盘
                cxyKeyboard.show({
                    selectors: '#carNumberKeyboard',
                    animation: !cxyKeyboard.isShow,
                    type: 'ABC',
                }, cxyKeyboard.isShow);
            }
        };
    }

    /**
     * 显示键盘
     * @param {string} id 
     */
    showKeyboard(id) {
        const param = {
            selectors: id,
            animation: !cxyKeyboard.isShow, // 键盘不存在时则显示动画
        }

        if (id === '#carPrefix') {
            param.value = ''; // 每次选择都清空
        }

        // 键盘如果存在，则切换键盘。不存在则显示键盘
        cxyKeyboard.show(param, cxyKeyboard.isShow);
    }

    render() {
        let _props = this.props

        return (
            <div>
                <div className={Style.inputBox}>
                    <div className={Style.carPrefix} onClick={() => this.showKeyboard('#carPrefix')}>
                        <span><p>{this.state.carPrefix}</p><i></i></span>
                    </div>
                    <label>车牌号码</label>
                    <div className={Style.carNumberInput} id="carNumberKeyboard" onClick={() => this.showKeyboard('#carNumberKeyboard')}></div>
                </div>
            </div>
        );
    }
}

//使用context
CarNumInput.contextTypes = {
    router: React.PropTypes.object.isRequired
}

CarNumInput.defaultProps = {
    // list:[] //车牌前缀
};

export default CarNumInput;