//样式
import Style from './index.scss'

const AuthItem = props => {
    let { title, subTitle, icon, onClick } = props

    return (
        <div className={Style.authItem} onClick={onClick ? props => onClick(props) : ''}>
            <div className="abs-v-center">
                <img src={icon} className={Style.authImg} />
                <div className={Style.authText}>
                    <h3>{title}</h3>
                    <p>{subTitle}</p>
                </div>
            </div>
        </div>
    )
}
const AuthList = props => (
    <div className={Style.authList}>
        {
            props.list.map((item, i) => item.show ? <AuthItem key={`authItem-${i}`} {...item} /> : '')
        }
    </div>
)

export default AuthList