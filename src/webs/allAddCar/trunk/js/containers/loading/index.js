/**
 * 加载中
 */

//styles
import styles from './index.scss'

export const Loading = props =>{

    return (
        <div className='box'>
            <div className='whiteBg'></div>
            <div className={styles.loading}>玩命加载中，请稍后...</div>
        </div>
    )
}
