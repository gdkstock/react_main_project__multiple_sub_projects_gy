import { combineReducers } from 'redux'
import user from './users'
import order from './order'

const rootReducer = combineReducers({
    user, //用户信息表
    order, //我的订单
});

export default rootReducer;
