import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//actions
import * as orderActions from '../../actions/orderActions'

//组件
import { Toast, Tabs, Badge } from 'antd-mobile';
import Card from '../../components/myOrder/card'
const TabPane = Tabs.TabPane;

//样式
import styles from './list.scss'

//工具类
import common from '../../utils/common'
import { ChMessage } from '../../utils/message.config'
import jsApi from '../../utils/jsApi'
import config from '../../config'

class CouponList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //选中的tab
            activeKey: '',

            //判断数据是否请求完毕  值为false时，不显示“无订单”等字样
            ready: false
        }
    }

    componentWillMount() {
        //设置标题
        common.setViewTitle("我的订单")

        try {
            let { order } = this.props;
            //默认选中
            let index = this.props.params.index || 0;
            if ([0, 1, 2, 3, 4].indexOf(index * 1) === -1) {
                index = 0;
            }

            this.setState({
                activeKey: sessionStorage.getItem('MyOrder_activeKey') || order['tab-' + index].key
            })
        } catch (error) {
            //非路由加载
            this.setState({
                activeKey: sessionStorage.getItem('MyOrder_activeKey')
            })
        }
    }

    componentDidMount() {
        let { orderLength } = this.props;

        if (orderLength === 0) {
            Toast.loading('', 0);
        } else {
            this.setState({
                ready: true //存在缓存数据时 标识加载完成
            })
        }

        //请求数据
        this.props.orderActions.getOrderList({}, () => {
            if (orderLength === 0) {
                Toast.hide();
                this.setState({
                    ready: true //标识加载完成
                })
            }
        });

        //接收广播
        window.cxyBroadcast_H5_addEvaluation = data => {
            let { orderId } = data;
            this.props.orderActions.updateEvaluationStatus({ orderId }); //更新评价的状态和跳转URL
            return "true";
        }

    }

    //跳转到订单详情
    toDetail(order) {
        if (order.orderDetailUrl) {
            if (!common.isCXYApp() && (order.orderType === 210 || order.orderType === 220)) {
                Toast.info('请下载APP查看此订单');
            } else {
                window.location.href = order.orderDetailUrl;
            }

        } else {
            Toast.info(ChMessage.FETCH_FAILED)
        }
    }

    //点击tab
    handleTabClick() {
        // 
    }

    //tab发生改变
    handleTabChange(key) {
        let { order } = this.props;

        document.body.scrollTop = 0; //滚动到顶部
        sessionStorage.setItem('MyOrder_activeKey', key);
        this.setState({
            activeKey: key
        });

        // 提交埋点
        common.sendCxytj({ eventId: order[key].cxytj });
    }

    //点击卡片按钮
    clickCardBtn(orderId, btn) {
        if (btn.operationType == 2) {
            //催单
            this.props.orderActions.orderReminder({ orderId }, res => Toast.info(res.msg));
        } else {
            //评论
            if (btn.pointUrl) {
                //APP 需要打开新view 避免页面评论成功后 自动关闭
                if (common.isCXYApp()) {
                    jsApi.pushWindow(btn.pointUrl);
                } else {
                    window.location.href = btn.pointUrl;
                }
            } else {
                Toast.info(ChMessage.FETCH_FAILED);
            }
        }
    }

    render() {
        let { order } = this.props;
        let { activeKey, ready } = this.state;

        return (
            <div className='orderList'>
                <Tabs defaultActiveKey={activeKey || 'tab-0'} onChange={(key) => this.handleTabChange(key)} onTabClick={() => this.handleTabClick()} swipeable={false}>
                    {
                        Object.keys(order).map(key =>
                            <TabPane tab={<Badge>{order[key].title}</Badge>} key={key}>
                                {
                                    order[key].list.length > 0
                                        ? order[key].list.map((item) =>
                                            <Card {...item} key={item.orderId}
                                                onClick={() => this.toDetail(item)}
                                                clickBtn={btn => this.clickCardBtn(item.orderId, btn)}
                                            />
                                        )
                                        : <div className={ready ? styles.noOrder : 'hide'}>
                                            <img src='./images/NoOrder.png' />
                                            {
                                                order[activeKey].title !== '全部'
                                                    ? <p>您无{order[activeKey].title}的订单</p>
                                                    : <p>您暂无订单</p>
                                            }
                                        </div>
                                }
                            </TabPane>
                        )
                    }
                </Tabs>
            </div >
        );
    }
}

/**
 * 我的订单列表
 * @param {*object} state orderState
 * return Object
 */
const orders = state => ({
    'tab-0': {
        key: 'tab-0',
        title: '全部',
        list: getOrderList(state, [0, 1, 2, 3, 4]),
        cxytj: 'cxyapp_e_orderlist_allTab'
    },
    'tab-1': {
        key: 'tab-1',
        title: '待支付',
        list: getOrderList(state, [1]),
        cxytj: 'cxyapp_e_orderlist_payingTab'
    },
    'tab-2': {
        key: 'tab-2',
        title: '办理中',
        list: getOrderList(state, [2]),
        cxytj: 'cxyapp_e_orderlist_processedTab'
    },
    'tab-3': {
        key: 'tab-3',
        title: '待消费',
        list: getOrderList(state, [3]),
        cxytj: 'cxyapp_e_orderlist_notUesdTab'
    },
    'tab-4': {
        key: 'tab-4',
        title: '待确认',
        list: getOrderList(state, [4]),
        cxytj: 'cxyapp_e_orderlist_notConfirmTab'
    }
})

const getOrderList = (state, numArr) => {
    return state.result.filter(orderId => numArr.indexOf(state.data[orderId].type) > -1).map(orderId => state.data[orderId]);
}

const mapStateToProps = state => ({
    order: orders(state.order),
    orderLength: state.order.result.length
})

const mapDispatchToProps = dispatch => ({
    orderActions: bindActionCreators(orderActions, dispatch),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CouponList);