import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//组件
import MyOrder from './myOrder/list'

class Home extends Component {
	constructor(props) {
		super(props);
	}

	render() {

		return (
			<div className='box'>
				<MyOrder />
			</div>
		);
	}
}

const mapStateToProps = state => ({

})

export default connect(
	mapStateToProps
)(Home);