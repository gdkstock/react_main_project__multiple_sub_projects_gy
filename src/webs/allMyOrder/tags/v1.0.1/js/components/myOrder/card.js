import styles from './card.scss'

import common from '../../utils/common'

const Card = props => {
    let { orderId, onClick, iconUrl, orderTitle, amount, createTime, statusName, orderType, operations, clickBtn } = props

    //非APP环境 过滤评论按钮
    if(!common.isCXYApp()){
        operations = operations.filter(item => item.operationType != '1');
    }
    
    return (
        <div className={styles.box}>
            <div className={styles.card} onClick={() => onClick && onClick()}>
                <div className={styles.iconBox}>
                    <img src={iconUrl} />
                </div>
                <div className={styles.txt}>
                    <div className={styles.txtTop}>
                        <span className={styles.title + ' text-overflow-1'}>{orderTitle}</span>
                        <span className={styles.amount + ' fr'}>￥{amount}</span>
                    </div>
                    <div className={styles.txtfooter}>
                        <span className='fl'>下单时间：{createTime}</span>
                        <span className='fr'>{statusName}</span>
                    </div>
                </div>
            </div>
            {
                orderType === 1 && operations.length > 0
                    ? <div className={styles.footer}>
                        {
                            operations.map((btn, i) => btn.status
                                ? <p>{btn.name}</p>
                                : <span key={orderId + '-operations-' + i} onClick={() => clickBtn ? clickBtn(btn) : false}>{btn.name}</span>
                            )
                        }
                    </div>
                    : ''
            }
        </div >
    )
}

export default Card;