/**
 * 我的订单
 */
import {
    ADD_ORDER_LIST,
    UPDATE_ORDER_REMINDER,
    UPDATE_EVALUATION_STATUS
} from '../actions/actionsTypes.js'

const init = {
    data: {},
    result: []
}

export default function myOrder(state = init, action) {
    switch (action.type) {
        case ADD_ORDER_LIST:
            return {
                data: action.data,
                result: action.result
            };

        //更新催单状态
        case UPDATE_ORDER_REMINDER:
            {
                let _data = JSON.parse(JSON.stringify(state.data));
                _data[action.data.orderId].operations[0].status = 1;
                _data[action.data.orderId].operations[0].name = action.data.name;
                return {
                    data: _data,
                    result: state.result
                };
            }

        //更新评价的状态
        case UPDATE_EVALUATION_STATUS:
            if (action.data && action.data.orderId) {
                let _data = JSON.parse(JSON.stringify(state.data));
                _data[action.data.orderId].operations[0].name = '已评价';
                _data[action.data.orderId].operations[0].pointUrl += '/1';
                
                return {
                    data: _data,
                    result: state.result
                };
            }
        default:
            return state;
    }
}