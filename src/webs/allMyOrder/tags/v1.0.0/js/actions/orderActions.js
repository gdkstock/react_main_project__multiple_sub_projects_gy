/*
* 订单列表actions
*/

import {
    GET_ORDER_LIST,
    ORDER_REMINDER,
    UPDATE_EVALUATION_STATUS
} from './actionsTypes'

//获取订单列表
export const getOrderList = (data, callback) => ({
    type: GET_ORDER_LIST,
    data,
    callback
})

//催单
export const orderReminder = (data, callback) => ({
    type: ORDER_REMINDER,
    data,
    callback
})

//更新评价的状态
export const updateEvaluationStatus = data => ({
    type: UPDATE_EVALUATION_STATUS,
    data
})