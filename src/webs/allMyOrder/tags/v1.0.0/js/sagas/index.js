// saga 模块化引入
import { fork } from 'redux-saga/effects'

//订单相关
import { watchOrder } from './order'

// 单一进入点，一次启动所有 Saga
export default function* rootSaga() {
  yield [
    fork(watchOrder)
  ]
}