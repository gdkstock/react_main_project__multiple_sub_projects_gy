/**
 * 订单相关
 */
import {
    takeEvery,
    delay,
    takeLatest,
    buffers,
    channel,
    eventChannel,
    END
} from 'redux-saga'
import {
    race,
    put,
    call,
    take,
    fork,
    select,
    actionChannel,
    cancel,
    cancelled
} from 'redux-saga/effects'

import {
    GET_ORDER_LIST,
    ADD_ORDER_LIST,
    UPDATE_ORDER_REMINDER,
    ORDER_REMINDER
} from '../actions/actionsTypes.js'

//antd
import { Toast } from 'antd-mobile'

//server
import orderService from '../services/orderService'

//常用工具类
import { ChMessage } from '../utils/message.config'

import { normalize, schema } from 'normalizr'; //范式化库

/**
 * 获取订单列表
 */
function* getOrderList(action) {
    try {
        const { result, timeout } = yield race({
            result: call(orderService.getOrderList, action.data),
            timeout: call(delay, 30000)
        })

        if (timeout) {
            Toast.info(ChMessage.FETCH_FAILED);
            return;
        } else {
            if (result.code == 1000) {
                const order = new schema.Entity('order', {}, { idAttribute: 'orderId' });
                const orders = new schema.Array(order);
                const normalizedData = normalize(result.data, orders);
                yield put({
                    type: ADD_ORDER_LIST,
                    data: normalizedData.entities.order,
                    result: normalizedData.result
                })
            }

            if (action.callback) {
                action.callback(result)
            }
        }
    } catch (error) {
        if (action.callback) {
            action.callback(error)
        }
    }
}

function* watchGetOrderList() {
    yield takeLatest(GET_ORDER_LIST, getOrderList)
}

/**
 * 订单催单
 */
function* orderReminder(action) {
    try {
        Toast.loading('', 0);
        const { result, timeout } = yield race({
            result: call(orderService.reminder, action.data),
            timeout: call(delay, 30000)
        })
        Toast.hide();
        if (timeout) {
            Toast.info(ChMessage.FETCH_FAILED);
            return;
        } else {
            if (result.code == 1000) {
                yield put({
                    type: UPDATE_ORDER_REMINDER,
                    data: {
                        orderId: action.data.orderId,
                        name: result.msg || '违章正常处理流程需3-7天。已为您催单，请耐心等待'
                    }
                })
            } else {
                Toast.info(result.msg || ChMessage.FETCH_FAILED);
            }

            if (action.callback) {
                action.callback(result)
            }
        }
    } catch (error) {
        Toast.hide();
        Toast.info(ChMessage.FETCH_FAILED);
        if (action.callback) {
            action.callback(error)
        }
    }
}

function* watchOrderReminder() {
    yield takeLatest(ORDER_REMINDER, orderReminder)
}

export function* watchOrder() {
    yield [
        fork(watchGetOrderList),
        fork(watchOrderReminder),
    ]
}