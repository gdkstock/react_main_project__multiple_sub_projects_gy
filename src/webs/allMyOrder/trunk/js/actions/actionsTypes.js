/*
* 这里定义所有的action类型
* */

/**
 * fetch数据请求相关
 */
export const FETCH_FAILED = 'FETCH_FAILED' // fetch请求出错
export const FETCH_TIMEOUT = 'FETCH_TIMEOUT' // fetch请求超时

//订单列表
export const GET_ORDER_LIST = 'GET_ORDER_LIST' //获取订单列表
export const ADD_ORDER_LIST = 'ADD_ORDER_LIST' //添加订单列表
export const UPDATE_ORDER_REMINDER = 'UPDATE_ORDER_REMINDER' //更新催单后的订单信息
export const ORDER_REMINDER = 'ORDER_REMINDER' //催单
export const UPDATE_EVALUATION_STATUS = 'UPDATE_EVALUATION_STATUS'; //更新评价的状态

