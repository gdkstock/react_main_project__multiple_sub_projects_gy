/**
 * 订单相关接口
 */

import apiHelper from './apiHelper';

class OrderService {

    /**
     * 获取订单列表
     * @param {*object} data
     */
    getOrderList(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/list`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 订单催单
     * @param {*object} data
     */
    reminder(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/reminder`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new OrderService()