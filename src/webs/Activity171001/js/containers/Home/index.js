import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// 加载样式
import Style from './index.scss'

// 加载组件
import auth from '../../utils/auth'
import common from '../../utils/common'
import config from '../../config'
import jsApi from '../../utils/cx580.jsApi'

class Home extends Component {
	constructor(props) {
		super(props);

	}

	/**
   * 生命周期
   */
	componentWillMount() {
		//执行一次，在初始化render之前执行，如果在这个方法内调用setState，render()知道state发生变化，并且只执行一次
		document.querySelector("title").innerHTML = '国庆88折活动';
	}
	componentDidMount() {
		//在初始化render之后只执行一次，在这个方法内，可以访问任何组件，componentDidMount()方法中的子组件在父组件之前执行
		this.sendCxytjFUN();
	}

	/**
* 事件
*/

	sendCxytjFUN() {
		// 保存用户信息
		//	alert('sendCxytjFUN= ' + common.getJsApiUserType())
		if (common.getJsApiUserType() == 'app') {
			jsApi.call({
				"commandId": "",
				"command": "getSymbol",
				"data": {
					"deviceId": "",
					"userid": "",
					"lng": "",
					"version": "",
					"channel": "",
					"phone": "",
					"name": "",
					"type|orderNum": "",
					"city": "",
					"accountId": "",
					"token": "",
					"carId": "",
					"carNumber": ""
				}
			}, (data) => {

				sessionStorage.setItem('userId', data.data.accountId);
				sessionStorage.setItem('token', data.data.token);
				sessionStorage.setItem('userType', 'app');
				sessionStorage.setItem('authType', 'app');
				sessionStorage.setItem('productUserId', data.data.userid || data.data.deviceId);
				setTimeout(function () {
					//	common.sendCxytj({ eventId: 'h5_p_mo' })//  埋点
				}, 1000); // 活动请求：没有用户ID，也请求数据
			});
		}

	}
	// 点击获取优惠券
	getGift(url) {
alert("aa")
		//	common.sendCxytj({ eventId: 'h5_e_mot_ClickQuery' })//  埋点


	}
	
	// --------
	render() {

		return (
			<div>
				{window.location.href}
				------------
				{sessionStorage.getItem('token')}
				<div className='relative'>
					<div className={Style.btn} onClick={() => this.getGift()} />
					<img src='./images/index/1_05.png' width='100%' height='auto' />
				</div>
				
				<ul>
					<li>1、 活动时间：9月18日-9月24日</li>
					<li>2、 用户在活动期间通过查询年审时间有机会获取年审优惠券，可在车行易办理年审时下单直接抵扣现金</li>
					<li>3、 此活动新老用户均可参与，每个用户只能领取一次</li>
					<li>4、 优惠券有效期为领取后7天内有效</li>
					<li>5、 此优惠券不可与平台其他优惠券同时使用</li>
					<li>6、 车行易公司享有对上述规则的最终解释权</li>
				</ul>
				<br /><br />
			</div>
		);
	}
}

//使用context
Home.contextTypes = {
	router: React.PropTypes.object.isRequired
}

export default connect()(Home)