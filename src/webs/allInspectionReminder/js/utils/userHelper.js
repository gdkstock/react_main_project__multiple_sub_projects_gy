import jsApi from './cx580.jsApi';
import common from './common';

let g_userType = '';

let g_userId = ''; //CXY_51374EB0DCDE4373BE4FA95C226B4116
let g_userToken = ''; //8030D163A2ACE546797F814D4F521E54

let g_city = "";

/**
 * 用户帮助类
 */
class UserHelper {
    //userContainer = "";
    // userId = "";
    // userToken = "";
    //city = "";


    constructor() {
        this._initialize();
    }

    _initialize() {
        // setTimeout(() => {
        //     this._getContainer();
        // }, 500);
        if (common.isCXYApp()) {
            //执行到这里，说明是在 app 中运行
            g_userType = "App";
            this._getUserInfoFromApp();
            // setTimeout(() => {
            //     this._getUserInfoFromApp();
            // }, 150);
        } else {
            //执行到这里，说明不在 app 中运行
            g_userType = "";
            //alert("请使用车行易APP访问！");
        }
    }

    _getContainer() {
        try {
            jsApi.call({
                "commandId": "",
                "command": "netstat"
            }, (data) => {
                //执行到这里，说明是在 app 中运行
                g_userType = "App";
                this._getUserInfoFromApp();
            });
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            g_userType = "";
        }
    }


    /**
     * 从 app 中获取用户 token
     */
    _getUserInfoFromApp() {
        try {
            jsApi.call({
                "commandId": "",
                "command": "getSymbol",
                "data": {
                    "city": "",
                    "accountId": "",
                    "token": ""
                }
            }, (data) => {
                g_userId = data.data.accountId;
                g_userToken = data.data.token;
                g_city = data.data.city;
            });
        } catch (error) {
            //执行到这里，说明不在 app 中运行
            // alert("调试信息：调用APP JS SDK出错了 获取APP信息出错了");
        }
    }

    /**
     * 获取getSymbol的值
     * @param callback 回调函数
     */
    getSymbol(callback) {
        if (g_userType == "App") {
            jsApi.call({
                "commandId": "",
                "command": "getSymbol",
                "data": {
                    "userid": "",
                    "lng": "",
                    "lat": "",
                    "version": "",
                    "channel": "",
                    "cars": "",
                    "phone": "",
                    "name": "",
                    "type|orderNum": "",
                    "city": "",
                    "accountId": "",
                    "token": "",
                    "carId": "",
                    "carNumber": "",
                    "headImage": "",
                    "nickName": ""
                }
            }, (data) => {
                g_userId = data.data.accountId;
                g_userToken = data.data.token;
                g_city = data.data.city;
                callback(data.data)
            });
        } else {
            let data = {
                "userid": "",
                "lng": "",
                "lat": "",
                "version": "",
                "channel": "",
                "cars": "",
                "phone": "",
                "name": "",
                "type|orderNum": "",
                "city": "",
                "accountId": localStorage.getItem('userId') || "",
                "token": localStorage.getItem('token') || "",
                "carId": localStorage.getItem('carId') || "",
                "carNumber": localStorage.getItem('carNumber') || "",
                "headImage": "",
                "nickName": ""
            }
            callback(data)
        }
    }

    /**
     * app 登陆
     */
    _appLogin(callback) {
        jsApi.call({
            "commandId": "",
            "command": "login"
        }, (data) => {
            localStorage.setItem("upLoginState", "1"); //用户登录状态发生变化
            if (data.data.accountId) {
                g_userId = data.data.accountId;
                this._getUserInfoFromApp();
                if (callback) {
                    callback();
                }
            } else {
                //登录失败
                // common.closeAppView(); //关闭APP视图
                callback();
            }
        });
    }

    /**
     * 获取 userId 和 token
     */
    getUserIdAndToken() {
        if (g_userType == "App") {
            this._getUserInfoFromApp();
            return {
                userId: g_userId,
                token: g_userToken,
                city: g_city,
                userType: 'APP_cxy',
                authType: 'App'
            }
        } else {
            //非APP
            return {
                userId: localStorage.getItem('userId'),
                token: localStorage.getItem('token'),
                userType: localStorage.getItem('userType'),
                authType: localStorage.getItem('authType')
            }
        }
    }

    /**
     * 登陆
     * @param callback function 登陆成功之后的回调
     */
    Login(callback) {
        console.log(g_userType)
        if (g_userType == "App") {
            this._appLogin(callback) //APP年检提醒 不需要用户登录
        } else {
            //跳转到单点登录
            var authIstest = window.location.host.indexOf('annualcheck.cx580.com') > -1 ? 'https://auth.cx580.com/' : 'http://testauth.cx580.com/'; //正式||测试 单点登录
            var search = window.location.search || '?userType=AliPay&authType=AliPay'
            window.location.replace(authIstest + "Auth.aspx" + search + '&clientId=CheWu&redirect_uri=' + window.location.href.split('?')[0]);
        }
    }
};

// 实例化后再导出
export default new UserHelper()