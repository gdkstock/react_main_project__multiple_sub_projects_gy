/**
 * 车辆相关
 */
import {
    ADD_CAR_INFO,
    UPDATE_CAR_INFO
} from './actionsTypes.js'

export const addCarInfo = data => ({ type: ADD_CAR_INFO, data: data }) //添加车辆信息
export const updateCarInfo = data => ({ type: UPDATE_CAR_INFO, data: data }) //更新车辆信息