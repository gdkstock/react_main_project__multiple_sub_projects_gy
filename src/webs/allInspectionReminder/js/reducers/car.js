/**
 * 车辆信息
 */
import {
    ADD_CAR_INFO, //添加车辆信息
    UPDATE_CAR_INFO, //更新车辆信息
} from '../actions/actionsTypes.js'

export default function car(state = {}, action) {
    switch (action.type) {
        case ADD_CAR_INFO:
            return action.data
        case UPDATE_CAR_INFO:
            return Object.assign({}, state, action.data)
        default:
            return state
    }
}