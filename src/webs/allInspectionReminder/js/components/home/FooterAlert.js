import React, { Component, PropTypes } from 'react';
import Style from './footerAlert.scss'

class FooterAlert extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        let { days, type, close, clickBtn, isRemind } = this.props
        return (
            <div className={Style.footerBox}>
                <div className={Style.top}>
                    <div className={Style.close} onClick={() => close()}></div>
                </div>

                {/*未到年检周期 start*/}
                <div style={type == '1' ? {} : { display: 'none' }}>
                    <div className={Style.body}>
                        <p>该车辆尚未到年检周期，下单后暂时无法<br />为您立即代办，建议您 <span>{days}</span> 天后再行预约办理 </p>
                    </div>
                    <div className={Style.btns + ' box'}>
                        <div onClick={(btn) => clickBtn('leftBtn')}>继续办理</div>
                        <div onClick={(btn) => clickBtn('rightBtn')}>{isRemind ? '暂不办理' : '预约到时提醒'}</div>
                    </div>
                </div>
                {/*未到年检周期 end*/}

                {/*预约成功 start*/}
                <div style={type == '2' ? {} : { display: 'none' }}>
                    <div className={Style.body}>
                        <h3>预约成功</h3>
                        <p>届时我们将短信提醒您办理年检！</p>
                    </div>
                    <div className={Style.btn}>
                        <div className='btn' onClick={() => clickBtn()}>立即办理</div>
                    </div>
                </div>
                {/*预约成功 end*/}

            </div>
        );
    }
}

FooterAlert.defaultProps = {
    days: '', //天数
    type: '1', //底部弹窗类型：1、未到年检周期。2、预约成功
    close: () => console.log("点击关闭按钮"),
    clickBtn: (btn) => console.log("点击按钮：", btn) //返回按钮标识符：leftBtn、rightBtn、只有一个按钮时不返回
};

export default FooterAlert;