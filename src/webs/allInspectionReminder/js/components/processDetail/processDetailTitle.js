/**
 * Created by 小敏哥 on 2017/3/23.
 */
import React, {Component} from 'react';
import style from './processDetailTitle.scss';

class ProcessDetailTitle extends Component {

    render() {
        let innerStyle= {
                backgroundImage: 'url(' +this.props.titleBackground+')',
            }
        ;
        return <div className={style.container}>
            <img src={this.props.titleImg}/>
            <div style={innerStyle}>{this.props.title}</div>
        </div>
    }
}

export default ProcessDetailTitle