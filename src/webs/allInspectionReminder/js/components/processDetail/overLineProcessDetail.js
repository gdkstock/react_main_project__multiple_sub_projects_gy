/**
 * Created by 小敏哥 on 2017/6/16.
 */
import React, {Component} from 'react';
import style from './index.scss';
import ProcessDetailTitle from './processDetailTitle';
class OverLineProcessDetail extends Component {
    constructor(props) {
        super(props);
        this.state={
            loaded:false,
            containerHeight:'100%'
        }
    }
    //返回主页
    goBack(){
        history.go(-1);
    }
    componentDidMount() {
        //首页隐藏标题栏右键
        /* AlipayJSOperation.setRightButtonStatus(false);
         AlipayJSOperation.setTitle('年检代办详细流程');
         //设置标题颜色
         AlipayJSOperation.setBarColor('#2FB3FE');
         setTimeout(()=>{
         this.setState({
         loaded:true,
         })
         },50)*/
    }

    setMainClassName(){
        //  return this.state.loaded?style.container+' '+style.sliderUp:style.container;
        return style.container;
    }

    goHomeBySlider(endCallBack){
        this.setState({
            loaded:false
        });
        setTimeout(()=>{this.goBack();},500)
    }

    render() {
        let containerStyle={
            height:this.state.containerHeight
        };
        return  <div style={containerStyle} className={this.setMainClassName()}>
            {/*<ScrollContainer width="100%" background="white" height={(window.innerHeight- 73)+'px'} noScaleTop={false}
             noScaleBottom={true} maxMove={50}
             topText={{ before: "下拉返回首页", middle: "释放返回首页", after: "正在跳转" }}
             topReleaseEvent={(endCallBack) => {
             this.goHomeBySlider(endCallBack)
             }}>*/}
            <div className={style.contentContainer}>
                <ProcessDetailTitle title="准备材料" titleImg="./images/procedure_pic_A.png"/>
                <div className={style.contentDiv}>
                    <div className={style.left}></div>
                    <div className={style.content}>
                        <p>请将以下年检代办所需材料备齐：</p>
                        <p>1、车辆行驶证原件正副本；</p>
                        <p>2、有效期内交强险副本原件；</p>
                        <p>3、有效期内车船税发票原件</p>
                        <p>（如交强险含车船税，则无需提供）；</p>

                    </div>
                </div>

                <ProcessDetailTitle title="选择检测方式" titleImg="./images/procedure_pic_B.png"/>
                <div className={style.contentDiv+ ' ' + style.imgContent}>
                    <div className={style.left}></div>
                    <div className={style.content}>
                        <p>您可选择车行易管家代驾前往检测站或自驾前往</p>
                    </div>
                </div>
                <ProcessDetailTitle title="年审办理" titleImg="./images/procedure_pic_C.png"/>
                <div className={style.contentDiv}>
                    <div className={style.left}></div>
                    <p className={style.content}>车行易管家将现场为您代办年检手续。</p>
                </div>
                <ProcessDetailTitle title="办理完成" titleImg="./images/procedure_pic_D.png"/>
                <div className={style.contentDiv}>
                    <div className={style.lastLeft}></div>
                    <p className={style.content}>年检通过之后我们将第一时间归还车辆及相关资料。</p>
                </div>
            </div>
            {/*  </ScrollContainer>*/}
            {/* <div className={style.buttonDiv}>
             <button onClick={this.goBack}>返回</button>
             </div>*/}
        </div>
    }
}

export default OverLineProcessDetail;