/**
 * Created by 小敏哥 on 2017/6/16.
 */
import React, {Component} from 'react';
import ReTab from 'app/components/common/reTab/reTab';
import OverLineProcessDetail from './overLineProcessDetail';
import ProcessDetail from './index';
import style from './allProcessDetail.scss'
class AllProcessDetail extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className={style.allDetailContainer}>
            <ReTab>
                <ProcessDetail reTabTitle="免检办理流程"/>
                <OverLineProcessDetail reTabTitle="上线检办理流程"/>
            </ReTab>
        </div>
    }
}

export default AllProcessDetail