import apiHelper from './apiHelper';

/**
 * 车辆相关接口
 */
class CarsServiece {

    /**
    * 获取车辆列表
    */
    list() {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/list`,
            data: {
                method: 'get'
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
    * 车辆查询
    * @param data object
    * {
    * carId:'', //车辆id
    * carNumber:'', //车牌号码
    * }
    */
    view(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/view`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 车辆修改
     * @param data object
     * {
     * carId:'', //车辆id
     * carNumber:'', //车牌号码
     * registerDate:'', //车辆注册日期
     * checkDate:'', //车辆检验日期
     * carCode:'', //车身架号
     * engineNumber:'', //发动机号
     * }
     */
    update(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}cars/update`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    //获取订单详情url
    getOrderDetail(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}order/viewUri`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new CarsServiece()