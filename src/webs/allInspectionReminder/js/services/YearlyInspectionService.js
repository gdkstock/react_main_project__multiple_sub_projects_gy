import apiHelper from './apiHelper';

/**
 * 年检相关接口
 */
class YearlyInspectionService{

    /**
     * 联动获取检验有效时间列表
     * @param data object
     * {
     * registerDate:'', //车辆注册时间
     * }
     */
    inspectionCheckList(data){
        let requestParam = {
            url: `${apiHelper.baseApiUrl}inspection/checkList`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 保存预约记录接口
     * @param data object
     * {
     * carId:'', //车辆id
     * carNumber:'', //车牌号
     * check_date:'', //检验有效期
     * appointment_date:'', //预约日期
     * }
     */
    recordAddRecord(data){
        let requestParam = {
            url: `${apiHelper.baseApiUrl}record/addRecord`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 车牌是否支持年检验证
     * @param data object
     * {
     * carNumber:'', //车牌号
     * }
     */
    inspectionValidate(data){
        let requestParam = {
            url: `${apiHelper.baseApiUrl}inspection/validate`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new YearlyInspectionService()