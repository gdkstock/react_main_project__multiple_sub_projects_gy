import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'
import CalculatorOfYearlyCheck from 'app/components/calculatorOfYearlyCheck'
import YearlyCheckDetail from 'app/components/calculatorOfYearlyCheck/YearlyCheckDetail'
import urlOperation from '../../utils/urlOperation';

//UI组件
import { Toast } from 'antd-mobile'

//数据请求
import YearlyInspectionService from '../../services/YearlyInspectionService'
import CarsServiece from '../../services/CarsServiece'

//常用类
import common from '../../utils/common'
import apiHelper from '../../services/apiHelper'

//actions
import * as carsActions from '../../actions/carsActions';

class Index extends Component {
    constructor(props) {
        super(props);

        let { type } = this.props.params;
        let { carNumber, registerDate, checkDate } = this.props.carInfo
        this.state = {
            data: { //年检计算器的props数据
                carNum: carNumber, //车牌号码
                dateList: [], //检验有效期列表
                registerDate: registerDate || '', //车辆注册日期
                inspectDate: checkDate || '', //检验有效期
                getRegisterDate: d => this.getRegisterDate(d), //获取车辆注册日期
                getInspectDate: d => this.setInspectDate(d), //获取检验有效期
                onClick: (r, i) => this.showYearlyCheckDetail(r, i),
                btnText: type == '1' ? '保存' : '保存并计算'
            },
            showYearlyCheckDetail: false, //显示年检状态
            yearlyCheckDetailProps: { //年检状态props数据
                carNum: carNumber,
                state: '', //年检状态；0：正常;1：可预约；2：逾期（仍可免检）；3：逾期；4：严重逾期；5：报废
                agentFlag: true, //年检类型：true为6年新车年检；false为上线年检
                registerDate: '', //注册时间
                inspectDate: '', //检验有效期
                days: '', //文案提醒中的天数 (有效天数、逾期天数等)
                clickTopBtn: () => this.setState({ showYearlyCheckDetail: false }),
                clickLeftBtn: () => this.toUrl('/'),
                clickRightBtn: () => this.toProcessDetail() //跳转到办理说明页
            },
        }
    }

    componentWillMount() {
        let { type } = this.props.params
        if (type == '1') { //补充资料
            common.setViewTitle('补充年检资料')
        } else if (type == '2') { //计算器
            common.setViewTitle('年检计算器')
        }
    }

    componentDidMount() {
        if (!this.props.carInfo || !this.props.carInfo.carId) {
            //全局状态不存在时 页面后退
            window.history.back()
        } else {
            let { registerDate, checkDate } = this.props.carInfo
            if (registerDate) {
                this.getRegisterDate(registerDate, checkDate)
            }
        }
    }

    componentWillUnmount() {

    }

    toUrl(url) {
        this.context.router.push(url);
    }


    //构建跳转地址
    getInspectionUrl() {
        let {carId, carNumber,  bizFlag} = this.props.carInfo;
        let detailUserType = '';
        let url = '';
        let host = apiHelper.production ? 'https://annualcheck.cx580.com' : 'http://192.168.1.165:7083';
        carNumber = encodeURIComponent(carNumber); //url编码
        let t = new Date().getTime();
        if (navigator.userAgent.indexOf("appname_cxycwz") > -1) {
            detailUserType = 'app';
            url = `${host}/inspection/index.html?t=${t}&detailUserType=${detailUserType}#/confirmOrder/${carId}/${carNumber}/${bizFlag}`;
        }
        else {
            detailUserType = urlOperation.getParameters().userType;
            let userId = urlOperation.getParameters().userId;
            let userType = urlOperation.getParameters().userType;
            let token = urlOperation.getParameters().token;
            let authType = urlOperation.getParameters().authType;
            url = `${host}/inspection/index.html?t=${t}&detailUserType=${detailUserType}&userId=${userId}&userType=${userType}&token=${token}&authType=${authType}#/confirmOrder/${carId}/${carNumber}/${bizFlag}`
        }
        return url;
    }

    /**
	 * 跳转到办理说明页面
	 * @param {*string} url 
	 */
    toProcessDetail() {
        let { carId, carNumber, validateFlag, validateFlagDone, inspectionValidateMsg ,bizFlag} = this.props.carInfo

        //判断车牌是否支持年检办理
       // if (!validateFlagDone) {
            this.inspectionValidate(()=>{
                let url = this.getInspectionUrl();
                url = encodeURIComponent(url); //url编码
                common.openNewBrowserWithURL(`processDetail/${url}`);
            });
           /* return;
        } else {
            if (validateFlag != 1) {
                Toast.info(inspectionValidateMsg)
                return;
            }
        }*/


        // this.toUrl(`/processDetail/${url}`)
    }

    /**
	 * 判断车牌是否支持年检验证
	 */
    inspectionValidate(callBack) {
        let { carId, carNumber, inspectionValidateMsg } = this.props.carInfo
        let postData = {
            carId: carId,
            carNumber: carNumber
        }
        Toast.loading('', 0)
        YearlyInspectionService.inspectionValidate(postData).then(data => {
            Toast.hide()
            if (data.code == '1000') {
                data = data.data
                inspectionValidateMsg = data.msg || '很抱歉，我们尚未开通您车牌所在地的年检业务！'
                this.props.dispatch(carsActions.updateCarInfo({
                    validateFlag: data.validateFlag, //车牌是否支持年检验证 1：支持年检；0：不支持年检
                    validateFlagDone: true, //已经请求过数据
                    inspectionValidateMsg: inspectionValidateMsg, //不支持年检办理的提示语
                    bizFlag:data.bizFlag,//年检办理标识
                })); //提交数据到全局state
                if (data.validateFlag == 1) {
                    //跳转到办理说明页面
                    setTimeout(() => {callBack()}, 10) //延迟跳转
                } else {
                    Toast.info(inspectionValidateMsg)
                }
            } else {
                Toast.info(data.msg)
            }
        }, () => {
            Toast.hide()
            Toast.info(errorMsg)
        })
    }

    /**
     * 设置检验有效期
     * @param {*string} d 日期
     */
    setInspectDate(d) {
        this.setState({
            data: Object.assign({}, this.state.data, {
                inspectDate: d
            })
        })
    }

    /**
     * 联动获取检验有效时间列表
     * @param registerDate 注册时间
     * @param inspectDate 检验有效期 不传时使用接口返回的默认值
     */
    getRegisterDate(registerDate, inspectDate = '') {
        //这里触发接口请求
        let postData = {
            registerDate: registerDate
        }

        YearlyInspectionService.inspectionCheckList(postData).then(data => {
            if (data.code == 1000) {
                let dateList = data.data.dateList
                // let { inspectDate } = this.state.data //检验有效期

                // if (!inspectDate) {
                //     //不存在检验有效期时，自动为用户选择最接近当前时间的有效期
                //     if (dateList.length > 1) {
                //         //存在两条或以上检验有效期
                //         let d = new Date()
                //         let nowDate = new Date(d.getFullYear(), d.getMonth()).toJSON().substr(0, 7) //当前时间减30天 （规则如此）
                //         // console.log(dateList[dateList.length - 2].checkDate >= nowDate, dateList[dateList.length - 2].checkDate, nowDate)
                //         if (dateList[dateList.length - 2].checkDate >= nowDate) {
                //             //倒数第二条日期大于当前时间
                //             inspectDate = dateList[dateList.length - 2].checkDate //取列表倒数第二条数据的日期
                //         } else {
                //             inspectDate = dateList[dateList.length - 1].checkDate //取列表最后一条数据的日期
                //         }
                //     } else {
                //         inspectDate = dateList[dateList.length - 1].checkDate //取列表最后一条数据的日期
                //     }
                // }

                // let stateDataList = data.data.dateList.map(item => item.checkDate);
                // this.setState({
                //     data: Object.assign({}, this.state.data, {
                //         inspectDate: inspectDate,
                //         dateList: stateDataList.reverse()
                //     })
                // })


                let stateDataList = data.data.dateList.map(item => {
                    if (!inspectDate && item.default == '1') {
                        inspectDate = item.checkDate
                    }
                    return item.checkDate
                });
                this.setState({
                    data: Object.assign({}, this.state.data, {
                        inspectDate: inspectDate,
                        dateList: data.data.dateList.map(item => item.checkDate)
                    })
                })
            } else {
                Toast.info(data.msg);
            }
        })
    }

    /**
     * 跳转到年检状态页面 或 保存车辆并后退
     */
    showYearlyCheckDetail(r, i) {
        console.log("父组件：点击了保存按钮!", "注册时间：" + r, "检验有效期：" + i)
        if (!r) {
            Toast.info("请选择车辆注册日期", 1)
        } else if (!i) {
            Toast.info("请选择检验有效期", 1)
        } else {
            //这里应该去请求数据
            Toast.loading('', 0)
            let { carId, carNumber, carCode, engineNumber } = this.props.carInfo
            let postData = {
                carId: carId, //车辆id
                carNumber: carNumber, //车牌号码
                registerDate: r, //车辆注册日期
                checkDate: i, //车辆检验日期
                carCode: carCode, //车身架号
                engineNumber: engineNumber, //发动机号
            }
            CarsServiece.update(postData).then(data => {
                Toast.hide()
                if (data.code == '1000') {
                    if (this.props.params.type == '1') {
                        window.history.back() //成功保存车辆信息后 页面后退
                        return
                    }
                    data = data.data
                    this.props.dispatch(carsActions.updateCarInfo(data)); //提交数据到全局state
                    this.setState({
                        showYearlyCheckDetail: true,
                        data: Object.assign({}, this.state.data, {
                            registerDate: r, //注册时间
                            inspectDate: i, //检验有效期
                        }),
                        yearlyCheckDetailProps: Object.assign({}, this.state.yearlyCheckDetailProps, {
                            carNum: carNumber,
                            state: data.state,
                            agentFlag: data.agentFlag, //年检类型 true为六年内新车年检；false为上线年检
                            bizFlag:data.bizFlag,//1:免检；2:上线检；3:不可办理
                            registerDate: r, //注册时间
                            inspectDate: i, //检验有效期
                            days: data.days
                        })
                    })
                } else {
                    Toast.info(data.msg)
                }
            }, () => {
                Toast.hide()
                Toast.info('系统繁忙，请稍后再试')
            })
        }
    }

    render() {
        let calculatorOfYearlyCheckProps = this.state.data;
        let yearlyCheckDetailProps = this.state.yearlyCheckDetailProps
        console.log(this.state, '--------------------')
        return (
            <div>
                {this.state.showYearlyCheckDetail ?
                    <YearlyCheckDetail {...yearlyCheckDetailProps} />
                    :
                    <CalculatorOfYearlyCheck {...calculatorOfYearlyCheckProps} />
                }
            </div>
        );
    }
}

Index.propTypes = {

};

//使用context
Index.contextTypes = {
    router: React.PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    carInfo: state.car
})

export default connect(mapStateToProps)(Index);