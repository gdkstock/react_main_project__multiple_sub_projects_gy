import React, { Component } from 'react';
import Style from './index.scss';
import ProcessDetail from '../../components/processDetail/allProcessDetail';

//常用类
import common from '../../utils/common'
import userHelper from '../../utils/userHelper'
import jsApi from '../../utils/cx580.jsApi'

var startTime = window.cxytj.getNowFormatDate(); //开始时间
class Index extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        common.setViewTitle('办理说明')
    }

    componentDidMount() {
        startTime = window.cxytj.getNowFormatDate(); //开始时间

        setTimeout(() => {
            userHelper.getSymbol(data => {
                window.cxytj.init({ //以下为初始化示例，可新增或删减字段
                    productId: data.productId || 'allInspectionReminder', //产品ID
                    productUserId: data.userid, //APP端 （设备标记ID）
                    channel: data.channel || 'App', //推广渠道
                    userId: data.accountId, //用户ID
                    sessionId: data.sessionId, //会话ID
                    longitude: data.lng, //经度
                    latitude: data.lat //纬度
                });
                window.cxytj.recordUserBehavior({
                    eventId: 'enterProcessDetailPage', //事件标记
                    attr1: '进入办理说明页面' //属性1
                });
            })
        }, 100)
    }

    /**
	 * 显示日期 年月||年月日
	 * @param {*date} date 日期 格式：YYYY-MM-DD
	 * @param {*string} type 日期类型 1、年月入；2、年月； 	默认等于年月日
	 */
    showDate(date, type = '1') {
        console.log(date)
        let arr = date.split('-')
        if (arr.constructor !== Array || arr.length < 3) {
            return date; //格式不合法 返回原数据
        }
        switch (type) {
            case '1':
                date = arr[0] + '年' + arr[1] + '月' + arr[2] + '日';
                break
            case '2':
                date = arr[0] + '年' + arr[1] + '月'
                break
            default:
                date = arr[0] + '年' + arr[1] + '月' + arr[2] + '日';
        }
        return date
    }

    openNewBrowserWithURL(url) {
        window.cxytj.recordUserBehavior({
            eventId: 'TimeOnProcessDetailPage', //事件标记
            attr1: '',
            eventType: '1',
            startTime: startTime
        });
        userHelper.getSymbol(data => {
            window.cxytj.init({ //以下为初始化示例，可新增或删减字段
                productId: data.productId || 'allInspectionReminder', //产品ID
                productUserId: data.userid, //APP端 （设备标记ID）
                channel: data.channel || 'App', //推广渠道
                userId: data.accountId, //用户ID
                sessionId: data.sessionId, //会话ID
                longitude: data.lng, //经度
                latitude: data.lat //纬度
            });

            window.cxytj.recordUserBehavior({
                eventId: 'allInspectionReminder_event6', //事件标记
                startTime: startTime
            });

            common.openNewBrowserWithURL(url)
        })
    }

    render() {
        let { url } = this.props.params
        url = decodeURIComponent(url)

        //时间规则 +20天-5年
        let date = new Date()
        date.setDate(date.getDate() + 20) //当前时间+20天
        date.setFullYear(date.getFullYear() - 5) //减5年
        date = date.toJSON().substr(0, 10) //转为YYYY-MM-DD
        date = this.showDate(date) //转为yyyy年mm月dd日

        return (
            <div className={Style.condition + ' box'}>
                <h3>免检办理条件<span className={Style.tips}>（不满足条件车辆需上线检测）</span></h3>
                <div className={Style.conditionBox}>
                    <p>1、7座以下（不含7座）的蓝牌车，即普通家用小轿车；<br />2、{date}后购买的车辆；<br />3、免检期内未发生过致人伤亡事故的车辆；</p>

                </div>
                <h3>办理流程</h3>
                <ProcessDetail />
                <div style={{ height: '1.64rem', width: '100%' }}></div>
                <div className={Style.footerBtn}>
                    <div className='btn' onClick={() => this.openNewBrowserWithURL(url)}>立即办理</div>
                </div>
            </div>
        )
    }
}

export default Index;