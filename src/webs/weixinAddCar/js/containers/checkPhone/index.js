import React, { Component } from 'react'
import { Link } from 'react-router'
import { Button, Toast } from 'antd-mobile';
import VerificationCode from '../../components/inputs/VerificationCode'
import Style from './index.scss'
import apiHelper from '../../services/apiHelper'
import illegalService from '../../services/illegalService'
import common from '../../utils/common'

export default class CheckPhone extends Component {

    toUrl(url) {
        this.context.router.push(url);
    }

    getCodeBtn() {
        let btn = this.refs.getCodeBtn;
        if (btn.innerHTML === "获取验证码" || btn.innerHTML === "重新发送") {
            console.log("获取验证码", this.refs.getCodeBtn.innerHTML)
            btn.innerHTML = '<span class="' + Style.black + '">' + '59s</span>';
            let second = 59; //用来记录秒,因为需求是从1开始的。
            let SEND = setInterval(function () {
                second--;
                btn.innerHTML = '<span class="' + Style.black + '">' + second + 's</span>'; //显示时间
                if (second == 0) {
                    btn.innerHTML = "重新发送";
                    clearInterval(SEND);
                } //到过60秒时停止
            }, 1000); //每1000毫秒即1秒执行一次此函数

            let { carNum } = this.props.params;
            carNum = decodeURI(carNum); //转码
            let postData = {
                carnumber: carNum
            }
            illegalService.getSmsCode(postData).then(data => console.log(data), error => console.log(error)); //验证码发送 不需要提示错误信息
        }
    }

    /**
     * 手机不在身边
     */
    noPhone(url) {
        window.cxytj.recordUserBehavior({
            userId: sessionStorage.getItem('userId'),
            eventId: 'noPhoneBtn',
            attr1: '手机不在身边'
        })
        this.toUrl(url);
    }

    /**
     * 不是您的手机号？
     */
    notYourPhoneNumber(url) {
        window.cxytj.recordUserBehavior({
            userId: sessionStorage.getItem('userId'),
            eventId: 'notYourPhoneNumber',
            attr1: '不是您的手机号'
        })
        this.toUrl(url);
    }

    /**
     * 提交数据
     */
    send() {
        let code = VerificationCode.code();
        if (!code) {
            Toast.info("请输入验证码", 1)
        } else if (code.length !== 6) {
            Toast.info("请输入6位验证码", 1)
        } else {
            let { carNum } = this.props.params;
            let postData = {
                phoneCode: code,
                carnumber: carNum
            }
            Toast.loading("", 0)
            window.cxytj.recordUserBehavior({
                userId: sessionStorage.getItem('userId'),
                eventId: 'verifyTelphoneBtn',
                attr1: '手机验证页查询按钮'
            })
            illegalService.verifyTelphone(postData).then(data => {
                Toast.hide()
                if (data.code == 1000) {
                    //跳转到违章办理页面
                    window.cxytj.recordUserBehavior({
                        userId: sessionStorage.getItem('userId'),
                        eventId: 'toIllegalPage',
                        attr1: '跳转到违章页面'
                    })
                    let url = `http://webtest.cx580.com:5555/carInfo/queryList.aspx?carNumber=${carNum}&userType=WeiXin`;
                    if (apiHelper.production) {
                        url = `https://banli.cx580.com/carInfo/queryList.aspx?carNumber=${carNum}&userType=WeiXin`;
                    }

                    window.location.href = url;
                } else {
                    Toast.info(data.msg);
                }
            }, () => {
                Toast.hide()
                Toast.info("系统繁忙，请稍后再试");
            })
        }


    }

    componentWillMount() {
        common.setViewTitle('验证手机号');
    }

    componentDidMount() {
        window.cxytj.recordUserBehavior({
            userId: sessionStorage.getItem('userId'),
            eventId: 'inGetSmsCodePage',
            attr1: '进入手机验证页'
        })
    }

    render() {
        let { carNum, phone, CarCodeLen, CarEngineLen, count, money, degree, dateTime } = this.props.params;
        carNum = encodeURI(carNum); //url编码
        let toAddCarUrl = dateTime ? `/addCar/${carNum}/2/${CarCodeLen}/${CarEngineLen}/${count}/${money}/${degree}/${dateTime}` : `/addCar/${carNum}/2/${CarCodeLen}/${CarEngineLen}`;

        return (
            <div className={"whiteBg box"}>
                <div className='checkPhoneTitle'>为了保障您的车辆信息安全，此次查询需要进行手机验证，通过后下次无需验证</div>
                <div className='checkPhoneSubTitle'>用户绑定手机号：<b>{phone}</b></div>
                <div className='ver-center plr30'>
                    <VerificationCode />
                    <div ref='getCodeBtn' className={Style.getCodeBtn} onClick={() => this.getCodeBtn()}>获取验证码</div>
                </div>
                <div className='plr30' style={{ marginTop: '.6rem', marginBottom: '.4rem' }} >
                    <Button className="btn" type="primary" onClick={() => this.send()}>验证并查询</Button>
                </div>
                <div className={Style.url}>
                    <span className='fl' onClick={(url) => this.notYourPhoneNumber(toAddCarUrl)}>不是您的手机号？</span><span className='fr' onClick={(url) => this.noPhone(toAddCarUrl)}>手机不在身边？</span>
                </div>
            </div>
        )
    }
}

//使用context
CheckPhone.contextTypes = {
    router: React.PropTypes.object.isRequired
}