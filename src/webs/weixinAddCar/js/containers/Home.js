import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Button, Toast } from 'antd-mobile';
import CarNumInput from '../components/inputs/CarNumInput'
import apiHelper from '../services/apiHelper'
import geeTestService from '../services/geeTestService'
import illegalService from '../services/illegalService'
import common from '../utils/common'

class Home extends Component {

	constructor(props) {
		super(props)
	}

	toUrl(url) {
		this.context.router.push(url);
	}

	/**
	 * 检查输入的合法性
	 */
	checkInputs() {
		let result = captchaObj.getValidate(); //验证信息
		let carNum = CarNumInput.carNum();
		let checkCarNum = /[A-Z]/g.exec(carNum.substr(1, 1)); //检查车牌号码是否以字母开头

		if (carNum.length < 2) {
			Toast.info("车牌号码不能为空", 1);
		} else if (carNum.length < 7 || carNum.length > 8 || !checkCarNum) {
			Toast.info("您输入的车牌号码有误", 1);
		} else if (!result) {
			Toast.info("请先完成验证", 1);
			return;
		} else {
			return true;
		}
		return false;
	}

	send(a, b) {
		if (this.checkInputs()) {
			let carnumber = CarNumInput.carNum(); //车牌号码
			let result = captchaObj.getValidate(); //验证信息
			let postData = {
				carnumber: carnumber,
				geetest_challenge: result.geetest_challenge,
				geetest_validate: result.geetest_validate,
				geetest_seccode: result.geetest_seccode
			}

			Toast.loading('', 0)
			window.cxytj.recordUserBehavior({
				userId: sessionStorage.getItem('userId'),
				eventId: 'carNumPageQueryBtn',
				attr1: '输入车牌页查询按钮'
			})
			illegalService.query(postData).then(data => {
				Toast.hide()
				if (data.code == 1000) {
					let res = data.data.columns;
					let { CarCodeLen, CarEngineLen } = res.city; //车架号和发动机号所需要填充的位数
					CarCodeLen = CarCodeLen || 99;
					CarEngineLen = CarEngineLen || 99;
					let url;
					carnumber = encodeURI(carnumber); //url编码

					if (CarCodeLen == '0' && CarEngineLen == '0') {
						//不需要输入车架号和发动机号的时候 直接跳转到违章办理页面 or 车辆列表
						if (data.data.flag != '6') {
							//未超过3辆 跳转到违章办理页面
							window.cxytj.recordUserBehavior({
								userId: sessionStorage.getItem('userId'),
								eventId: 'toIllegalPage',
								attr1: '跳转到违章页面'
							})

							url = `http://webtest.cx580.com:5555/carInfo/queryList.aspx?carNumber=${carnumber}&userType=WeiXin`;
							if (apiHelper.production) {
								url = `https://banli.cx580.com/carInfo/queryList.aspx?carNumber=${carnumber}&userType=WeiXin`;
							}
						} else {
							//超过3辆 跳转到车辆列表
							window.cxytj.recordUserBehavior({
								userId: sessionStorage.getItem('userId'),
								eventId: 'toCarsPage',
								attr1: '跳转到车辆列表页面'
							})

							url = `http://webtest.cx580.com:5555/carInfo/queryList.aspx?userType=WeiXin`;
							if (apiHelper.production) {
								url = `https://banli.cx580.com/carInfo/queryList.aspx?userType=WeiXin`;
							}
						}

						setTimeout(() => window.location.href = url, 30) //等待埋点
					} else {
						switch (data.data.flag) {
							case '1': //表示有手机号码
								if (res.count) {
									url = `checkPhone/${carnumber}/${res.tel}/${CarCodeLen}/${CarEngineLen}/${res.count}/${res.money}/${res.degree}/${res.dateTime}`
								} else {
									url = `checkPhone/${carnumber}/${res.tel}/${CarCodeLen}/${CarEngineLen}`
								}
								break;
							case '2': //表示无手机号码，有违章
								url = `addCar/${carnumber}/2/${CarCodeLen}/${CarEngineLen}/${res.count}/${res.money}/${res.degree}/${res.dateTime}`
								break;
							case '3': //表示无手机号码，无违章
								url = `addCar/${carnumber}/2/${CarCodeLen}/${CarEngineLen}`
								break;
							case '4': //表示无车辆信息
								// url = `addCar/${carnumber}/1/${CarCodeLen}/${CarEngineLen}`
								url = `http://webtest.cx580.com:5555/carInfo/RegistCar.aspx?carNumber=${carnumber}&userType=WeiXin`;
								if (apiHelper.production) {
									url = `https://banli.cx580.com/carInfo/RegistCar.aspx?carNumber=${carnumber}&userType=WeiXin`;
								}
								window.cxytj.recordUserBehavior({
									userId: sessionStorage.getItem('userId'),
									eventId: 'toAddCarPage',
									attr1: '跳转到添加车辆页面'
								})
								break;
							case '5': //已经添加过车辆（不超过3辆） 跳转到违章办理页面
								//未超过3辆 跳转到违章办理页面
								window.cxytj.recordUserBehavior({
									userId: sessionStorage.getItem('userId'),
									eventId: 'toIllegalPage',
									attr1: '跳转到违章页面'
								})

								url = `http://webtest.cx580.com:5555/carInfo/queryList.aspx?carNumber=${carnumber}&userType=WeiXin`;
								if (apiHelper.production) {
									url = `https://banli.cx580.com/carInfo/queryList.aspx?carNumber=${carnumber}&userType=WeiXin`;
								}
								break;
							case '6':
								//超过3辆 跳转到车辆列表
								window.cxytj.recordUserBehavior({
									userId: sessionStorage.getItem('userId'),
									eventId: 'toCarsPage',
									attr1: '跳转到车辆列表页面'
								})
								url = `http://webtest.cx580.com:5555/carInfo/queryList.aspx?userType=WeiXin`;
								if (apiHelper.production) {
									url = `https://banli.cx580.com/carInfo/queryList.aspx?userType=WeiXin`;
								}
								Toast.info('违章查询最多支持绑定3辆车辆,请前往车辆列表删除后再继续查询')
								break;
							default:
								url = `addCar/${carnumber}/1/${CarCodeLen}/${CarEngineLen}`
								break;
						}
						if (data.data.flag == '4' || data.data.flag == '5') {
							//跳转到外部链接
							setTimeout(() => window.location.href = url, 30) //等待埋点
						} else if (data.data.flag == '6') {
							//延迟1.5秒 跳转到外部链接
							setTimeout(() => window.location.href = url, 1500)
						} else if (sessionStorage.getItem('userId')) {
							//路由跳转
							this.toUrl(url);
						} else {
							//需要跳转到单点登录获取用户信息
							url = (common.getRootUrl() + url).replace("#", '%23'); //避免#号不被认
							let authIstest = apiHelper.production ? 'https://auth.cx580.com/' : 'http://testauth.cx580.com/'; //正式||测试 单点登录
							window.location.href = authIstest + "Auth.aspx?authType=weixin&userType=weixin&clientId=CheWu&redirect_uri=" + url;
							// window.location.href = url;
						}
					}

				} else {
					Toast.info(data.msg);
				}
				// this.toUrl('checkPhone/' + carNum); //验证成功后跳转到对应的页面
			}, error => {
				Toast.hide()
				Toast.info("系统繁忙，请稍后再试")
			})
		}

	}

	componentWillMount() {
		common.setViewTitle('违章查询');
	}

	componentDidMount() {
		this.showGeeTest();

		if (window.cxytjIsReady) { //统计JS已加载完毕
			console.log("统计JS已加载完毕")
			window.cxytj.recordUserBehavior({
				userId: sessionStorage.getItem('userId'),
				eventId: 'inIllegalQueryPage',
				attr1: '进入违章查询页'
			})
		} else { //统计JS还未加载完毕 但是home组件已经加载完毕
			console.log("统计JS还未加载完毕 但是home组件已经加载完毕")
			window.homeReady = () => window.cxytj.recordUserBehavior({
				userId: sessionStorage.getItem('userId'),
				eventId: 'inIllegalQueryPage',
				attr1: '进入违章查询页'
			})
		}

	}

	/**
	 * 显示geeTest验证
	 */
	showGeeTest() {
		// 将验证码加到id为captcha的元素里，同时会有三个input的值用于表单提交
		let handler1 = captchaObj => {
			window.captchaObj = captchaObj; //保存captchaObj
			let result = captchaObj.getValidate();
			console.log("geetest：", result)

			// 将验证码加到id为captcha的元素里，同时会有三个input的值用于表单提交
			captchaObj.appendTo("#captcha1");

			this.refs.captcha1Code.style.display = 'none';

			// 更多接口参考：http://www.geetest.com/install/sections/idx-client-sdk.html
		};

		geeTestService.initGeetest({}).then(data => {
			console.log(data)
			initGeetest({
				gt: data.gt,
				challenge: data.challenge,
				// new_captcha: data.new_captcha, // 用于宕机时表示是新验证码的宕机
				new_captcha: false, // 用于宕机时表示是新验证码的宕机
				offline: !data.success, // 表示用户后台检测极验服务器是否宕机，一般不需要关注
				product: "embed", // 产品形式，包括：float，popup
				width: "100%"

				// 更多配置参数请参见：http://www.geetest.com/install/sections/idx-client-sdk.html#config
			}, handler1);
		}, error => console.log(error))
	}

	render() {
		return (
			<div className={"whiteBg box"}>
				<div style={{ lineHeight: 0, marginBottom: '.4rem' }} >
					<img src='./images/banner.png' style={{ width: '100%' }} alt='轻松查违章，极速又准确' />
				</div>
				<div className='plr30' style={{position:'relative',zIndex:'2', paddingBottom: '.24rem',}}>
					<CarNumInput />
				</div>
				<div id="captcha1" className='plr30' style={{position:'relative',zIndex:'2', paddingBottom: '.6rem' }} >
					<p ref='captcha1Code'>正在加载验证码......</p>
				</div>
				<div className='plr30'>
					<Button ref="sendBtn" className="btn" type="primary" onClick={() => this.send()}>查询</Button>
				</div>
			</div>
		)
	}
}

//使用context
Home.contextTypes = {
	router: React.PropTypes.object.isRequired
}

export default connect()(Home)