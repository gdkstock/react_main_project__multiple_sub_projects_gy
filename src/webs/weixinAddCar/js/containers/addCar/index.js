import React, { Component, PropTypes } from 'react';
import { Button, Toast } from 'antd-mobile';
import AlertDemo from '../../../../../js/components/common/alertDemo'
import Style from './index.scss'
import apiHelper from '../../services/apiHelper'
import illegalService from '../../services/illegalService'
import common from '../../utils/common'

class AddCar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            AlertDemo: { //弹窗demo
                show: false, //显示或隐藏弹窗
                props: { //弹窗demo 的props
                    demo: '暂无demo', //需要展示的demo
                    close: () => console.log('点击关闭按钮'), //点击关闭按钮
                }
            }
        }
    }

    /**
     * 
     * @param {*} e
     * @param {*} length //限制长度
     */
    inputLength(e, length = 6) {
        let { type } = this.props.params;
        let len = type == '1' ? length : 2; //补充资料只需要输入后两位
        e.target.value = e.target.value.substr(0, len).toUpperCase(); //最大允许输入的长度
    }

    send() {
        let { carNum, type, CarCodeLen, CarEngineLen, count } = this.props.params;
        carNum = decodeURI(carNum); //转码
        let isAddCar = type == '1' ? true : false; //判断是否为添加车辆
        let carCode = this.refs.carCode.value;
        let carDriveNumber = this.refs.carDriveNumber.value;

        if (CarCodeLen != '0') {
            CarCodeLen = isAddCar ? parseInt(CarCodeLen) : 2; //车架号长度
            if (!carCode) {
                Toast.info("车架号不能为空", 1)
                return;
            } else if (carCode.length !== CarCodeLen && CarCodeLen !== 99) {
                console.log(carCode.length)
                Toast.info(`请输入车架号后${CarCodeLen}位`, 1)
                return;
            }
        }

        if (CarEngineLen != '0') {
            CarEngineLen = isAddCar ? parseInt(CarEngineLen) : 2; //发动机号长度
            if (!carDriveNumber) {
                Toast.info("发动机号不能为空", 1)
                return;
            } else if (carDriveNumber.length !== CarEngineLen && CarEngineLen !== 99) {
                Toast.info(`请输入发动机号后${CarEngineLen}位`, 1)
                return;
            }
        }

        //输入合法
        let postData = {
            "carnumber": carNum,
            "carcode": carCode,
            "cardrivenumber": carDriveNumber,
            "type": type
        }
        Toast.loading("", 0)

        //用户行为埋点start
        let eventId, attr2;
        if (count) {
            eventId = 'carInfoPage1'
            attr2 = '有违章'
        } else if (!isAddCar) {
            eventId = 'carInfoPage2'
            attr2 = '缩略'
        } else {
            eventId = 'carInfoPage3'
            attr2 = '完整'
        }
        window.cxytj.recordUserBehavior({
            userId: sessionStorage.getItem('userId'),
            eventId: eventId,
            attr1: '查看违章详情',
            attr2: attr2
        })
        //用户行为埋点end

        illegalService.addCar(postData).then(data => {
            Toast.hide()
            if (data.code == 1000) {
                //跳转到违章办理页面
                window.cxytj.recordUserBehavior({
                    userId: sessionStorage.getItem('userId'),
                    eventId: 'toIllegalPage',
                    attr1: '跳转到违章页面'
                })

                let url = `http://webtest.cx580.com:5555/carInfo/queryList.aspx?carNumber=${carNum}&userType=WeiXin`;
                if (apiHelper.production) {
                    url = `https://banli.cx580.com/carInfo/queryList.aspx?carNumber=${carNum}&userType=WeiXin`;
                }

                window.location.href = url;
            } else {
                Toast.info(data.msg);
            }
        }, () => {
            Toast.hide()
            Toast.info("系统繁忙，请稍后再试");
        })
    }

    /**
     * 显示弹窗说明
     * @param type 内容的类型
     */
    showAlertBox(type) {
        console.log(type);
        let demo = <img src='./images/alertDemoImg.png' />; //发动机号和车架号共用同一张demo图

        this.setState({
            AlertDemo: { //弹窗demo
                show: true, //显示
                props: {
                    demo: demo, //需要展示的demo
                    close: () => this.hideAlertBox(), //点击关闭按钮
                }
            }
        })
    }

    /**
     * 隐藏弹窗说明
     */
    hideAlertBox() {
        this.setState({
            AlertDemo: Object.assign({}, this.state.AlertDemo, {
                show: false //隐藏
            })
        })
    }

    componentWillMount() {
        common.setViewTitle('补充车辆信息');
    }

    componentDidMount() {
        let { type, count } = this.props.params;

        let len = type == '1' ? 6 : 2;
        //用户行为埋点start
        let eventId, attr2;
        if (count) {
            eventId = 'inCarInfoPage1'
            attr2 = '有违章'
        } else if (len === 2) {
            eventId = 'inCarInfoPage2'
            attr2 = '缩略'
        } else if (len === 6) {
            eventId = 'inCarInfoPage3'
            attr2 = '完整'
        }
        window.cxytj.recordUserBehavior({
            userId: sessionStorage.getItem('userId'),
            eventId: eventId,
            attr1: '查看违章详情',
            attr2: attr2
        })
        //用户行为埋点end
    }

    componentWillUnmount() {

    }

    render() {
        let { carNum, type, CarCodeLen, CarEngineLen, count, money, degree, dateTime } = this.props.params;
        carNum = decodeURI(carNum); //转码

        let carCodePlaceholder = type == '1' ? CarCodeLen == '99' ? '请输入完整车架号' : '请输入车架号后' + CarCodeLen + '位' : '请输入车架号后2位';
        let carDriveNumberPlaceholder = type == '1' ? CarEngineLen == '99' ? '请输入完整发动机号' : '请输入发动机号' + CarEngineLen + '位' : '请输入发动机号2位';
        return (
            <div className='box'>
                {!count ? '' :
                    <div style={{ background: '#fff', padding: '0 .3rem' }}>
                        <div className={Style.violationTitle}>违章信息</div>
                        <div className={Style.violationInfo}>
                            <div><i>违章</i><span>{count}次</span></div>
                            <div><i>罚款</i><span>{money}元</span></div>
                            <div><i>扣分</i><span>{degree}分</span></div>
                            {/*<div>时间<span>{dateTime}</span></div>*/}
                        </div>
                    </div>
                }
                <div className='subTitle'>完善您的车辆信息就可以查看违章详情哦！</div>
                <div className={Style.inputBox + ' ver-center'}>
                    <span className={Style.title}>车牌号</span>
                    <span className={Style.carNum}>{carNum.substr(0, 2) + ' ' + carNum.substr(2)}</span>
                </div>
                <div className={Style.inputBox + ' ver-center'} style={CarCodeLen == '0' ? { display: 'none' } : {}} >
                    <div className={Style.title}>
                        <span>车身架号</span><i>*</i>
                        <i className={Style.prompt} onClick={() => this.showAlertBox('carCode')}></i>
                    </div>
                    <input ref='carCode' type='url' placeholder={carCodePlaceholder} onInput={(e) => this.inputLength(e, CarCodeLen)} />
                </div>
                <div className={Style.inputBox + ' ver-center'} style={CarEngineLen == '0' ? { display: 'none' } : {}}>
                    <div className={Style.title}>
                        <span>发动机号</span><i>*</i>
                        <i className={Style.prompt} onClick={() => this.showAlertBox('carDriveNumber')}></i>
                    </div>
                    <input ref='carDriveNumber' type='url' placeholder={carDriveNumberPlaceholder} onInput={(e) => this.inputLength(e, CarEngineLen)} />
                </div>
                <div className='plr30' style={{ paddingTop: '.24rem' }}>
                    <Button className="btn" type="primary" onClick={() => this.send()}>查看违章详情</Button>
                </div>
                {/*弹窗说明 start*/}
                {this.state.AlertDemo.show ? <AlertDemo {...this.state.AlertDemo.props} /> : ''}
                {/*弹窗说明 end*/}
            </div>
        );
    }
}

AddCar.propTypes = {

};

export default AddCar;