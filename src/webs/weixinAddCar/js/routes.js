import React from 'react'
import { Route, IndexRoute } from 'react-router'

import {
  App,
  Home,
  NotFoundPage,
  CheckPhone, //验证手机号
} from './containers'

import AddCar from './containers/addCar'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home}/>
    {/*<Route path="路由地址" getComponents={(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./组件路径/按需加载demo').default)
        }, 'chunkName')
      }} />*/}
    <Route path="addCar/:carNum/:type/:CarCodeLen/:CarEngineLen(/:count/:money/:degree)(/:dateTime)" component={AddCar}/>
    <Route path="checkPhone/:carNum/:phone/:CarCodeLen/:CarEngineLen(/:count/:money/:degree)(/:dateTime)" component={CheckPhone}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);