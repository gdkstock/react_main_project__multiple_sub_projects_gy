import apiHelper from '../services/apiHelper';

/**
 * 引导注册相关接口
 */
class illegalService {
    /**
      * 根据车牌号码获取违章信息
      * @param data object
      * {
      * carnumber:'', //车牌号码
      * }
      */
    query(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}illegal/query`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
      * 添加或补充车辆接口
      * @param data object
      * {
      * carcode:'', //车牌号码
      * carnumber:'', //车架号
      * cardrivenumber:'', //发动机号
      * type:'', //1：添加车辆；2：补充资料
      * }
      */
    addCar(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}illegal/addCar`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
      * 验证手机号接口
      * @param data object
      * {
      * tel:'', //手机号码
      * phoneCode:'', //手机号验证码
      * carnumber:'', //车牌号码
      * }
      */
    verifyTelphone(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}illegal/verifyTelphone`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
      * 发送手机号验证码
      * @param data object
      * {
      * carnumber:'', //车牌号码
      * }
      */
    getSmsCode(data) {
        let requestParam = {
            url: `${apiHelper.baseApiUrl}illegal/get_sms_code`,
            data: {
                method: 'post',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new illegalService()