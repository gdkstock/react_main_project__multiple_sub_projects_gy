import apiHelper from '../services/apiHelper';

/**
 * Test接口
 */
class TestService {
    /**
     * geeTest初始化
     */
    initGeetest(data) {
        let requestParam = {
            url: apiHelper.baseApiUrl + 'gt/register1?t' + (new Date()).getTime(),
            data: {
                method: 'get',
                body: {}
            }
        };
        return apiHelper.fetch(requestParam);
    }

    /**
     * 获取车辆信息
     */
    getCarInfo(data) {
        let requestParam = {
            url: apiHelper.baseApiUrl + 'gt/register1',
            data: {
                method: 'get',
                body: {}
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new TestService()