import common from '../utils/common';

require('isomorphic-fetch');

class ApiHelper {

    production = window.location.href.indexOf('weixin.cx580.com') > -1 ? true : false; //是否为生产环境

    /**
     * 切换生产或测试环境
     * "http://192.168.2.48:9977/"; //本地测试地址
     * "http://192.168.1.235:18066/"; //测试环境地址
     * "http://comments.cx580.com:6050/";//生产
     */
    baseApiUrl = this.production ? window.location.protocol + "//" + window.location.host + "/" : "http://192.168.1.235:18066/";
    // baseApiUrl = this.production ? window.location.protocol + "//" + window.location.host + "/" : "http://192.168.2.48:9977/";
    

    /**
     * 获取 HTTP 头
     */
    _getHeaders() {
        return {
            "Accept": "*/*",
        }
    }

    /**
     * 封装fetch
     */
    fetch(requestParam) {
        let setTimeNum = 0; //不需要延迟

        let promise = new Promise((resolve, reject) => {
            //用于支付宝 不需要延迟请求
            setTimeout(() => {
                // console.log("requestParam", requestParam);
                requestParam.data.method = requestParam.data.method || "get";
                requestParam.data.headers = requestParam.data.headers || {};
                Object.assign(requestParam.data.headers, this._getHeaders());
                if (requestParam.data.method.trim().toLowerCase() == "post") {
                    requestParam.data.headers["Content-Type"] = "application/x-www-form-urlencoded";
                }
                requestParam.data.body = requestParam.data.body || {};
                //设置用户
                if (!requestParam.data.body.userId) {
                    requestParam.data.body["userId"] = sessionStorage.getItem('userId');
                }
                if (!requestParam.data.body.userOpenId) {
                    requestParam.data.body["userOpenId"] = sessionStorage.getItem('userOpenId');
                }
                if (!requestParam.data.body.token) {
                    requestParam.data.body["token"] = sessionStorage.getItem('token');
                }

                requestParam.data.body = common.toQueryString(requestParam.data.body);


                requestParam.data.mode = "cors";
                if (requestParam.data.method.trim().toLowerCase() == "get") {
                    var request = new Request(requestParam.url + '?' + requestParam.data.body); //get请求不能有body,否则会报错
                } else {
                    var request = new Request(requestParam.url, requestParam.data);
                }
                // console.debug("request", request);
                let result = window.fetch(request, { headers: requestParam.data.headers })
                    .then(function (response) {
                        let resp = response.json();
                        resp.then(function (data) {
                            if (data.code == "2222") {
                                // userHelper.Login(); //微信端 不需要登录
                            }
                        });
                        return resp;
                    })
                    .catch(function (e) {
                        console.error("fetch 请求出错了");
                        console.dir(e);
                        throw e;
                    });
                console.log("result", result);
                resolve(result)
            }, setTimeNum);

            // 网络超时
            setTimeout(() => reject('网络错误'), 30000);
        });
        return promise;
    }
}

// 实例化后再导出
export default new ApiHelper()