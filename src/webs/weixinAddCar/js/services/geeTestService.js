import apiHelper from '../services/apiHelper';

/**
 * Test接口
 */
class geeTestService {
    /**
     * geeTest初始化
     */
    initGeetest(data) {
        let requestParam = {
            url: apiHelper.baseApiUrl + 'geetest/startCaptcha?t' + (new Date()).getTime(),
            data: {
                method: 'get',
                body: {}
            }
        };
        return apiHelper.fetch(requestParam);
    }

}

export default new geeTestService()