class Common {

    /**
     * 获取当前网址的根路径 返回的数据为：http.../#/
     */
    getRootUrl() {
        let this_url = window.location.href; //当前网址
        let substrNum = this_url.indexOf('#') + 1; //获取到哈希路由当前的位置 + 1
        let url = this_url.substr(0, substrNum) + '/';
        return url;
    }

    /**
     * 将键值对转为URL参数
     */
    _toQueryPair(key, value) {
        ///<summary>将键值对转为URL参数</summary>
        if (typeof value == 'undefined') {
            return key;
        }
        return key + '=' + encodeURIComponent(value === null ? '' : String(value));
        //return key + '=' + (value == null ? '' : String(value));
    }

    /**
     * 将对象转为URL参数
     */
    toQueryString(obj) {
        var ret = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                key = encodeURIComponent(key);
                var values = obj[key];
                if (values && values.constructor === Array) { //数组 
                    var queryValues = [];
                    for (var i = 0, len = values.length, value; i < len; i++) {
                        value = values[i];
                        queryValues.push(this._toQueryPair(key + '[' + i + ']', value));
                    }
                    ret = ret.concat(queryValues);
                } else { //字符串 
                    ret.push(this._toQueryPair(key, values));
                }
            }
        }
        return ret.join('&');
    }

    /**
     * 设置浏览器标题 兼容IOS 后退title不修改的bug
     */
    setViewTitle(title) {
        let body = document.getElementsByTagName('body')[0];
        document.title = title;
        let iframe = document.createElement("iframe");
        iframe.setAttribute("src", "/favicon.ico");
        iframe.style.display='none';
        iframe.addEventListener('load', function () {
            setTimeout(() => {
                document.body.removeChild(iframe);
            }, 10);
        });
        document.body.appendChild(iframe);

        this.getWeixinInfo();
    }

    /**
     * 获取微信信息 用户信息等
     */
    getWeixinInfo() {
        (function () {
            if (!sessionStorage.getItem('userId')) {
                var userId = '';
                var userOpenId = '';
                var token = '';

                userId = /[\?|&]userId=([^&]+)/g;
                userId = userId.exec(window.location.href);
                userId = userId ? userId[1] : "";

                userOpenId = /[\?|&]userOpenId=([^&]+)/g;
                userOpenId = userOpenId.exec(window.location.href);
                userOpenId = userOpenId ? userOpenId[1] : "";

                token = /[\?|&]token=([^&]+)/g;
                token = token.exec(window.location.href);
                token = token ? token[1] : "";

                sessionStorage.setItem("userId", userId); //保存openId
                sessionStorage.setItem("userOpenId", userOpenId); //保存userOpenId
                sessionStorage.setItem("token", token); //保存token
            }
        })();
    }

    /**
     * 获取安卓系统的版本号 非安卓手机则返回false
     */
    getAndroidVersion() {
        let re = /Android\s([^;]+)/ig;
        let _version = re.exec(navigator.userAgent);
        if (_version) {
            _version = _version[1];
        } else {
            _version = false;
        }
        return _version;
    }

    browser = function () {
        let u = navigator.userAgent;
        //app = navigator.appVersion;
        return {
            versions: { //移动终端浏览器版本信息 
                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端 
                iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器 
                iPad: u.indexOf('iPad') > -1, //是否iPad 
                android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1 //android终端或uc浏览器 
            }
        }
    }

    /**
     * 检测当前浏览器是否为iPhone(Safari)
     */
    isIPhone = function () {
        let browser = this.browser();
        if (browser.versions.iPhone || browser.versions.iPad || browser.versions.ios) {
            return true;
        }
        return false;
    };

    /**
     * 检测当前浏览器是否为Android(Chrome)
     */
    isAndroid = function () {
        let browser = this.browser();
        if (browser.versions.android) {
            return true;
        }
        return false;
    };

};

// 实例化后再导出
export default new Common()