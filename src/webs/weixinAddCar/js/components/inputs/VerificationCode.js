import React, { Component, PropTypes } from 'react';
import Style from './index.scss'

class VerificationCode extends Component {
    constructor(props) {
        super(props);

        this.state = {
            carPrefix: '粤', //默认车牌前缀
            showCarPrefixList: false, //显示车牌前缀列表
        }

        VerificationCode.code = () => this.getCode();
    }

    toUrl(url) {
        this.context.router.push(url);
    }

    /**
     * 获取验证码
     */
    getCode() {
        return this.refs.code.value
    }

    /**
     * 内容发生改变
     */
    handleCodeInput(e) {
        let value = e.target.value.substr(0, this.props.maxLength);
        
        setTimeout(()=>{
            this.refs.code.value = value
        },0) //避免IOS中文输入法下的bug
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div className={Style.inputBox + ' ver-center'}>
                <img className={Style.duipai} src='./images/icon-dunpai.png' />
                <input ref='code' type='url' placeholder="请输入验证码" onInput={(e) => this.handleCodeInput(e)} style={{width:"5.8rem"}} />
            </div>
        );
    }
}

//使用context
VerificationCode.contextTypes = {
    router: React.PropTypes.object.isRequired
}

VerificationCode.propTypes = {

};

VerificationCode.defaultProps = {
    maxLength: '6' //允许最大的输入值
}

export default VerificationCode;