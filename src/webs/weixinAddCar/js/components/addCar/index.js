import React, { Component, PropTypes } from 'react';
import { Button } from 'antd-mobile';
import Style from './index.scss'

class AddCar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showViolationInfo: false, //显示违章信息
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        //模拟拿到车辆信息数据
        if(this.props.params.carNum === '粤A12345'){
            this.setState({
                showViolationInfo: true
            })
        }
    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div>
                {!this.state.showViolationInfo ? '' :
                <div>
                    <div className='plr30 ptb20'>违章信息</div>
                    <div className={Style.violationInfo}>
                        <div>违章<span>3次</span></div>
                        <div>罚款<span>5000元</span></div>
                        <div>扣分<span>300分</span></div>
                        <div>时间<span>2017-3-28</span></div>
                    </div>
                </div>
                }
                <div className='plr30 ptb20'>补充车辆信息查看违章详情</div>
                <div className={Style.inputBox + ' ver-center'}>
                    <span className={Style.title}>车牌号</span>
                    <span>{this.props.params.carNum}</span>
                </div>
                <div className={Style.inputBox + ' ver-center'}>
                    <span className={Style.title}>发动机号</span>
                    <input type='url' placeholder='请输入发动机号2位' />
                </div>
                <div className={Style.inputBox + ' ver-center'}>
                    <span className={Style.title}>车身架号</span>
                    <input type='url' placeholder='请输入车架号后2位' />
                </div>
                <div className='plr30 ptb30'>
                    <Button className="btn" type="primary">查看违章详情</Button>
                </div>
            </div>
        );
    }
}

AddCar.propTypes = {

};

export default AddCar;