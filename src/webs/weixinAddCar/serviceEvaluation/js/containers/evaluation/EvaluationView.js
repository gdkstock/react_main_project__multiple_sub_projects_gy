import React, { Component, PropTypes } from 'react';
import { Toast } from 'antd-mobile'
import Style from './index.scss'
import UserInfo from './UserInfo'
import EvaluationService from '../../services/EvaluationService'

//common
import common from '../../utils/common'
//userHelper
import userHelper from '../../utils/userHelper'

class EvaluationView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                "score": '1', //分数
                "content": "", //内容
                "userAvatar": "", //用户头像
                "userName": "", //用户名称
                "telephone": "", //手机号码
                "createTime": "" //时间
            },
            isFetching: true, //加载中
        }
    }

    componentWillMount() {
        common.setViewTitle('评价详情')
    }

    componentDidMount() {
        let { orderId } = this.props
        let postData = {
            orderId: orderId
        }
        Toast.loading('', 0)
        EvaluationService.view(postData).then(data => {
            Toast.hide()

            if (data.code == 1000) {
                this.setState({
                    data: data.data
                })
            } else {
                Toast.info(data.msg || '系统繁忙，请稍后再试')
            }
        }, () => {
            Toast.hide()
            Toast.info('系统繁忙，请稍后再试')
        }).then(() => {
            userHelper.getSymbol(data => {
                let { nickName, phone, headImage } = data;
                if (!nickName && phone) {
                    nickName = phone.substr(0, 2) + '**' + phone.substr(-1)
                }
                this.setState({
                    data: Object.assign({}, this.state.data, {
                        userName: nickName || this.state.data.userName, //名称
                        userAvatar: headImage//头像
                    }),
                    isFetching: false
                })
            })
        })
    }

    componentWillUnmount() {

    }

    /**
     * 渲染分数
     */
    showScore() {
        let score = parseInt(this.state.data.score); //分数
        let htm = [];
        for (let i = 0; i < 5; i++) {
            htm.push(<i key={'score' + i} className={i < score ? Style.blue : {}}></i>)
        }
        return htm;
    }

    /**
     * 高度自动
     */
    autoHeight() {
        setTimeout(() => {
            let obj = this.refs.textarea
            obj.style.height = obj.scrollHeight + 'px'
        }, 10)
    }

    render() {
        console.log("this.refs", this.refs)
        return (
            <div className='box'>
                {this.state.isFetching ? '' :
                    this.state.data.content &&
                    <div className={Style.whiteBg}>
                        <UserInfo {...this.state.data} />
                        <div className={Style.stars} style={{ padding: '0 .3rem' }}>
                            {this.showScore()}
                        </div>
                        <div className={Style.textareaBox}>
                            <div className={Style.disabled}></div>
                            <textarea ref='textarea' height={this.autoHeight()} defaultValue={this.state.data.content} readOnly='readOnly'></textarea>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

EvaluationView.propTypes = {

};

export default EvaluationView;