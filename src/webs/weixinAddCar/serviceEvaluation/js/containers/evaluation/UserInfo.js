import React, { Component } from 'react';
import Style from './index.scss'

export default class UserInfo extends Component {
    render() {
        let data = this.props
        return (
            <div className={Style.userBox}>
                <div className={Style.userPic}>
                    <img src={data.userAvatar} />
                </div>
                <div className={Style.userInfo}>
                    <span>{data.userName}</span>
                    <i>{data.createTime.substr(0,10)}</i>
                </div>
            </div>
        );
    }
}
