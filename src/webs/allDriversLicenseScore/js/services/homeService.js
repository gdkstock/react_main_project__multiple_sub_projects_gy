/**
 * Created by 小敏哥on 2017/3/16.
 */

import apiHelper from './apiHelper';

class HomeService{

    //查分
	  checkPoints(data){
        let requestParam = {
            url: `${apiHelper.baseApiUrl}drivingLicense/queryScore`,
            data: {
                method: 'get',
                body: data
            }
        };
        return apiHelper.fetch(requestParam);
    }

    //更新驾照接口
	  updateLicense(data){
      let requestParam = {
        url: `${apiHelper.baseApiUrl}drivingLicense/updateLicense`,
        data: {
          method: 'get',
          body: data
        }
      };
      return apiHelper.fetch(requestParam);
    }

    //查询驾照接口
	  queryLicense(data){
      let requestParam = {
        url: `${apiHelper.baseApiUrl}drivingLicense/queryLicense`,
        data: {
          method: 'get',
          body: data
        }
      };
      return apiHelper.fetch(requestParam);
    }

    //分享
  	share(data){
		  let production = window.location.href.indexOf('https://webservice.cx580.com/') > -1 ? true : false; //是否为生产环境
		  let baseApiUrl = production ? "https://webservice.cx580.com/" : "http://192.168.1.165:9071/";
      let requestParam = {
        url: `${baseApiUrl}share/url`,
        data: {
          method: 'post',
          body: data
        }
      };
      return apiHelper.fetch(requestParam);
    }

}

export default new HomeService();