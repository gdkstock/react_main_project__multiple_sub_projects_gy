import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Popup, Toast, Modal } from 'antd-mobile'
// import FooterAlert from '../components/home/FooterAlert' //底部弹窗

// //actions
// import * as carsActions from '../actions/carsActions';
//
// //接口
// import CarsServiece from '../services/CarsServiece'
// import YearlyInspectionService from '../services/YearlyInspectionService'

//常用工具类
// import userHelper from '../utils/userHelper'
// import common from '../utils/common'
// import apiHelper from '../services/apiHelper'

//资源
let progresBoxBg = './images/progresBoxBgo.png' //圆形进度条背景图
let errorMsg = '系统繁忙，请稍后再试' //错误提示信息

class Demo extends Component {
	constructor(props) {
		super(props);

		this.state = {
			carId: '', //车辆ID
			carNumber: '', //车牌
			registerDate: '', //车辆注册日期（yyyy-MM-dd）
			checkDate: '', //车辆检验日期（yyyy-MM）
			carCode: '', //车身架号
			engineNumber: '', //发动机号
			state: '-1', //年检状态；-1:未知；0：未进入预约期；1：可预约；2：逾期但不足一年；3：逾期且超过一年；4：严重逾期（上线年检）；5报废
			agentFlag: true, //年检类型：true为6年新车年检；false为上线年检
			days: 0, //天数
			orderId: '', //订单id，app年检提醒需要（跟据userType区分渠道）
			orderDetailUrl: '', //订单详情URL（跟据userType区分渠道）
			checkDateText: '', //车辆检验日期（YYYY年MM月DD日）
			beginCheckDateText: '', //开始办理年检的日期（YYYY年MM月DD日）
			isRemind: false, //已预约提醒
			validateFlag: 1, //车牌是否支持年检验证 1：支持年检；0：不支持年检
			validateFlagDone: false, //已经请求过数据
			inspectionValidateMsg: '很抱歉，我们尚未开通您车牌所在地的年检业务！', //不支持年检办理的提示语
		}
	}

	componentWillMount() {

	}

	componentDidMount() {

		//获取用户信息
		setTimeout(() => {
			try {
					let postData = {
						carId: '22',
						carNumber: '223'
					}
					this.getData(postData); //获取数据
			} catch (error) {

			}

		}, 250) //延迟 避免JSDK还未初始化
	}

	componentWillUnmount() {

	}
	
	getData(postData) {
		//模拟拿到数据
		if (window.location.host.indexOf('localhost') !== -1) { //本地模拟数据调试
			console.log(postData)
			console.log(this.refs.days)
			let data = {
				carId: '7887', //车辆ID
				carNumber: '辽AV3Y88', //车牌
				registerDate: '2015-06-09', //车辆注册日期（yyyy-MM-dd）
				checkDate: '2017-06', //车辆检验日期（yyyy-MM）
				carCode: '', //车身架号
				engineNumber: '', //发动机号
				state: '0', //年检状态；-1:未知；0：未进入预约期；1：可预约；2：逾期但不足一年；3：逾期且超过一年；4：严重逾期（上线年检）；5报废；6办理中
				agentFlag: true, //年检类型：true为6年新车年检；false为上线年检
				days: 2, //天数
				orderId: '', //订单id，app年检提醒需要（跟据userType区分渠道）
				orderDetailUrl: 'http://m.baidu.com', //订单详情URL（跟据userType区分渠道）
				isRemind: false,
			}
			this.setState(Object.assign({}, this.state, data), () => {
				if (['0', '1', '2', '3'].indexOf(this.state.state) !== -1) {
					this.showNumber(this.refs.days, 12, this.state.days, 500) //动态显示数据
					this.showProgres(100, Math.ceil((this.state.days / 12) * 100), 500) //动态显示进度条
				}
			})
			return;
		}
			this.setState(Object.assign({}, this.state, postData, data), () => {
				if (['0', '1', '2', '3'].indexOf(this.state.state) !== -1) {
					this.clearProgres(); //先重置圆环
					this.showNumber(this.refs.days, 12, this.state.days, 1000) //动态显示数据
					this.showProgres(100, Math.ceil((this.state.days / 630) * 100), 1500) //动态显示进度条
				}
			})
	}

	/**
	 * 动态显示数字
	 * @param {*object} e dom对象
	 * @param {*number} start 开始时数字
	 * @param {*number} end 结束时数字
	 * @param {*number} 速度 多少毫秒完成 默认1000毫秒
	 */
	showNumber(e, start, end, ms = 1000) {
		ms = Math.floor(ms / Math.abs(end - start))
		let delNum = 1 //每次应该减的数字
		if (ms < 4) {
			delNum = 4 / ms
		}
		console.log("ms:", ms, " delNum:", delNum)
		let timeId = setInterval(() => {
			try {
				if (start < end) {
					start += delNum; //由小变大
					if (start >= end) {
						clearInterval(timeId)
					}
				} else {
					start -= delNum; //由大变小
					if (start <= end) {
						clearInterval(timeId)
					}
				}

				e.innerHTML = parseInt(start)
			} catch (error) {
				clearInterval(timeId)
			}

		}, ms)
	}

	/**
	 * 动态显示进度条
	 * @param {*number} start 开始时进度 0-100
	 * @param {*number} end 结束时进度 0-100
	 * @param {*number} 速度 多少毫秒完成 默认1000毫秒
	 */
	showProgres(start, end, ms = 1000) {
		ms = Math.floor(ms / Math.abs(end - start))
		let clientWidth = document.documentElement.clientWidth
		console.log(clientWidth)
		clientWidth = clientWidth > 540 ? 540 : clientWidth
		let bl = clientWidth / 375

		if (start < end) {
			//从小到大
		} else {
			//从大到小
			let iLeft = 0; //控制左边 占比35%
			let iTop = 0; //控制上边 占比30%
			let iRight = 0; //控制右边 占比35%

			let leftBottom = 0; //左边的bottom

			let timeId = setInterval(() => {
				try {
					--start;
					if (start >= 64) {
						//大于60%
						++iRight;
						this.refs.grayCircularBoxRight.style.top = (172 - iRight * Math.floor(143 / 35)) + 'px'
						if (start === 64) {
							this.refs.grayCircularBoxRight.style.top = '0px' //避免还存在多余的进度
						}
					} else if (start >= 34) {
						//大于60%
						++iTop;
						this.refs.grayCircularBoxTop.style.width = (iTop * Math.ceil(112 / 30)) + 'px'
					} else {
						++iLeft;
						leftBottom = (143 - (iLeft * Math.ceil(143 / 35)))
						leftBottom = leftBottom < 6 ? 6 : leftBottom //
						this.refs.grayCircularBoxLeft.style.bottom = leftBottom + 'px'
					}
					if (start <= end) {
						clearInterval(timeId)
					}
				} catch (error) {
					clearInterval(timeId)
				}
			}, ms)
		}
	}

	/**
	 * 还原进度条
	 */
	clearProgres() {
		this.refs.grayCircularBoxRight.style.top = ''
		this.refs.grayCircularBoxTop.style.width = ''
		this.refs.grayCircularBoxLeft.style.bottom = ''
	}


	/**
	 * 获取页面色系
	 * @param {*number} state 状态
	 */
	getColor(state) {
		let color = 'homeRed'

		if (state == '-1') {
			color = 'homeGray'
		} else if (state == '0') {
			color = '' //默认颜色 所以不需要修改
		} else if (state == '1') {
			color = 'homeBlue'
		} else if (state == '6') {
			color = 'homeBlue'
		}

		return color
	}

	render() {
		console.log("this.state", this.state)
		let { state, checkDateText, beginCheckDateText } = this.state

		let color = this.getColor(state) //获取颜色


		let daysClassName = ['0', '1', '2', '3'].indexOf(state) !== -1 ? '' : 'font60'
		let texts = { //年检状态；-1:未知；0：未进入预约期；1：可预约；2：逾期但不足一年；3：逾期且超过一年；4：严重逾期（上线年检）；5报废；6年审订单办理中
			'-1': <i>小心年检逾期</i>,
			'0': <i>驾照剩余分值</i>,
			'1': <i>距年检有效期截止还剩天数</i>,
			'2': <i className='iconPromptI' onClick={() => this.showIconPromptToast(state)}>该车辆已逾期天数</i>,
			'3': <i className='iconPromptI' onClick={() => this.showIconPromptToast(state)}>该车辆已逾期天数</i>,
			'4': <i className='iconPromptI' onClick={() => this.showIconPromptToast(state)}>该车辆存在报废风险</i>,
			'5': <i className='iconPromptI' onClick={() => this.showIconPromptToast(state)}>请勿上路驾驶</i>,
			'6': <i><span style={{ fontWeight: 'normal' }}>请您耐心等待</span></i>
		}
		let daysTexts = {
			'-1': '状态未知',
			'0': '',
			'1': '',
			'2': '',
			'3': '',
			'4': '严重逾期',
			'5': '车辆已报废',
			'6': '年检办理中'
		}
		return (
			<div className={"box homeBox " + color}>
				<div className='homeTop'>

				</div>
				<div className='progresBox'>

					{/*显示天数或对应的年检状态icon start*/}
					<div className='progres' style={{ backgroundImage: `url(${progresBoxBg})` }}>
						<div className='progresDaysBox'>
							<span ref='days' className={daysClassName}></span>{texts[state]}
						</div>
						{/*<span className='progresIssue' onClick={() => this.clickProgresIssue()}>常见问题</span>*/}
					</div>
					{/*显示天数或对应的年检状态icon end*/}

					{/*渐变圆环 start*/}
					<div className="circularBox">
						<div className="circular1Box">
							<div className="circular circular1">
								<div className='hideCircular'></div>
							</div>
						</div>
						<div className="circular circular2">
							<div className='hideCircular'></div>
						</div>
						<div className="whiteCircular"></div>
						<div className='grayCircularBox'>
							<div ref='grayCircularBoxLeft' className='grayCircularBoxLeft'></div>
							<div ref='grayCircularBoxTop' className='grayCircularBoxTop'></div>
							<div ref='grayCircularBoxRight' className='grayCircularBoxRight'></div>
						</div>
					</div>
					{/*渐变圆环 start*/}

				</div>


				{/*立即办理按钮 || 查看订单详情按钮 end*/}

				{/*已办理过年检 更新按钮 start*/}
				{
					['1', '2', '3', '4', '5'].indexOf(state) != -1 ?
						<div className='homeBtnBottom'>已办理过本次年检？ <span onClick={() => this.clickDateMsg()}>点击更新信息</span></div>
						:
						''
				}
				{/*已办理过年检 更新按钮 end*/}

			

			</div>
		)
	}
}

//使用context
Demo.contextTypes = {
	router: React.PropTypes.object.isRequired
}

export default connect()(Demo);