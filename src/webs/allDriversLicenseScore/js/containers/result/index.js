
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Toast,DatePicker} from 'antd-mobile';
import AlipayJSOperation from '../../utils/alipayJSOperation';
import PageBar from '../../components/common/pageBar';
import urlOperation from '../../utils/urlOperation';
import homeService from '../../services/homeService';
import style from './index.scss';
import common from '../../utils/common'
import 'moment/locale/zh-cn';

// 如果不是使用 List.Item 作为 children
const CustomChildren = props => (
	<div
		onClick={props.onClick}
		style={{ backgroundColor: '#fff', height: '0.9rem', lineHeight: '0.9rem', padding: '0 0.3rem' }}
	>
		{
			props.status ?<span style={{color: '#1a1a1a',fontSize: '0.26rem'}}>{props.children+props.maturityDate}	<span style={{color: 'red',fontSize: '0.4rem',position: 'relative',top: '.15rem'}}>*</span></span>:
				<span style={{color: '#1a1a1a',fontSize: '0.26rem'}}>
					距扣分清零还剩 <span style={{color: '#FF8707',fontSize: '0.28rem'}}> {props.maturityDate} </span>天
				</span>
		}
				<span style={{ float: 'right', color: '#888' }}>{props.extra}
					<img src="./images/icon_rg.png" alt=""
	        style={{width: '0.18rem',height: '0.26rem',position:'relative',top: '0.04rem',left: '0.02rem'}}/>
				</span>
	</div>
);

class Result extends Component {
    constructor(props) {
        super(props);
        let {drivingLicenseNo,drivingFileNumber,score} = this.props.carInfo
	      let result = this.props.params.result;
        let drivingLicense = this.props.params.drivingLicense;
        let drivingFile = this.props.params.drivingFile;

        this.state = {
	        dpValue: null,
	        maturityDate: '', //剩余的天数
	        status: true,
	        score: score || result, //分数
	        drivingLicenseNo: drivingLicenseNo || drivingLicense,  //编号
	        drivingFileNumber: drivingFileNumber || drivingFile, //档案号
	        drivingLicenseScoreId: '', //ID
        };
	     this.getDays = this.getDays.bind(this);
    }

    //发送埋点
    sendMessage(type) {
        window.cxytj.recordUserBehavior({
            eventId: type,
            eventType: '2', //事件类型
            eventTime: '',  //触发时间
        });
    }
		 getDaysInMonth(year,month){
			month = parseInt(month,10); //parseInt(number,type)这个函数后面如果不跟第2个参数来表示进制的话，默认是10进制。
			var temp = new Date(year,month,0);
			return temp.getDate();
	   }

	   isLeapYear(year) {  ///true为闰年
				return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
     }

	  getDays(strDateStart){  //strDateStart和strDateEnd是2002-12-18格式
		  let d = new Date();
		  let isLeap = this.isLeapYear(d.getFullYear());
		  let days=isLeap? 366 : 355;
		  /////选择的时间总天数
		  let choice=[];
		  for(let y=1;y<strDateStart.split('-')[1];y++){
			  let data_s = this.getDaysInMonth(strDateStart.split('-')[0],y);
			  choice.push(data_s)
		  }
		  let strs='';
		  for (let i in choice){
			  strs+=choice[i];
			  strs+='+';

		  }
		  strs=strs.substring(0, strs.length - 1);
		  let strz=eval(strs)+Number(strDateStart.split('-')[2]);

			/////本地的时间总天数
			let local=[];
			for(let i=1;i<Number(d.getMonth()+1);i++){
				let data_a = this.getDaysInMonth(d.getFullYear(),i);
				local.push(data_a)
		  }

			let stra='';
			for (let i in local){
			  stra+=local[i];
			  stra+='+';
			}

		  stra=stra.substring(0, stra.length - 1);
			let strb=eval(stra)+d.getDate();

		  let total='';
		  if(strz>=strb){
			  total=days-(strz-strb);
		  }else if(strz<=strb){
			  total=strb-strz;
		  }

		  let res = days-total;

			this.setState({
				strDateStart:strDateStart.split('-')[0],
				getFullYear: d.getFullYear()
			});
			return res==0?days:res ;
	  }

		componentWillMount(){
			//this.closeAliModal();
			///查日期接口，跟查分接口参数一样
			let getData = {
				drivingLicenseNo: this.state.drivingLicenseNo,
				drivingFileNumber: this.state.drivingFileNumber,
			};
			Toast.hide(); //加载前 先关闭其他加载中的提示框 避免提示框一直存在的bug
			//Toast.loading("", 30, () => Toast.info("网络错误", 2));
			homeService.queryLicense(getData).then(data => {
				let date_Time = data.data.firstLicensingDate.split(' ');
				Toast.hide();
				if (data.code == 1000) {
					if(data.data.firstLicensingDate){
						this.setState({
							drivingLicenseScoreId: data.data.drivingLicenseScoreId,
						});
						this.changeCheckDate(date_Time[0].toString()); //如果有日期，就传值
					}else{
						this.setState({
							drivingLicenseScoreId: data.data.drivingLicenseScoreId,
						})
					}
				} else {
					Toast.info(data.msg);
				}
			});
    }

    componentWillUnmount(){

    }

    componentDidMount() {
	    var userType = urlOperation.getParameters().detailUserType;
	    var shareTile = "查询驾照分，让你心里有个底";
	    var shareUrl = location.href.split('#')[0];
	    var index = shareUrl.lastIndexOf('/');
	    var shareImg = shareUrl.substring(0, index) + "/images/share.png";
	    var urlShare = 'https://webservice.cx580.com/cweb/activity/2017/05/driving_license_check_score/index.html';
	    var shareDesc = '还在担心你的驾照分不翼而飞吗？马上查询看看吧~';
	    var postData = {
		    signUrl: encodeURIComponent(shareUrl)
	    };
	    Toast.hide(); //加载前 先关闭其他加载中的提示框 避免提示框一直存在的bug
	    Toast.loading("", 30, () => Toast.info("网络错误", 2));
	    homeService.share(postData).then(data => {
		    Toast.hide(); //加载前 先关闭其他加载中的提示框 避免提示框一直存在的bug
		    wx.config({
			    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			    appId: data.data.appId, // 必填，公众号的唯一标识
			    timestamp: data.data.timestamp, // 必填，生成签名的时间戳
			    nonceStr: data.data.nonceStr, // 必填，生成签名的随机串
			    signature: data.data.signature,// 必填，签名，见附录1
			    jsApiList: [
				    'checkJsApi',
				    'onMenuShareTimeline',
				    'onMenuShareAppMessage',
				    'onMenuShareQQ',
				    'onMenuShareWeibo',
				    'onMenuShareQZone',
			    ]
		    });

		    wx.ready(function () {
			    /// wx.hideOptionMenu();

			    //获取“分享到朋友圈”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareTimeline({
				    title: shareTile,
				    link: urlShare+'?detailUserType='+userType,
				    imgUrl: shareImg,
				    trigger: function (res) {
					    // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
					    //alert('用户点击分享到朋友圈');
				    },
				    success: function (res) {

				    },
				    cancel: function (res) {

				    }
			    });

			    //获取“分享给朋友”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareAppMessage({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType, // 分享链接
				    imgUrl: shareImg, // 分享图标
				    type: '', // 分享类型,music、video或link，不填默认为link
				    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
				    trigger: function (res) {
					    // alert(JSON.stringify(res))
					    // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
					    //alert('分享给朋友2');
				    },
				    success: function () {
					    //alert('分享给朋友3');
				    },
				    cancel: function () {
				    }
			    });
			    //获取“分享到QQ”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareQQ({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType, // 分享链接
				    imgUrl: shareImg, // 分享图标
				    trigger: function (res) {
					    // alert(JSON.stringify(res))
					    // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
					    //alert('分享给朋友2');
				    },
				    success: function () {
					    //alert('分享给朋友3');
				    },
				    cancel: function () {
				    }
			    });

			    //获取“分享到腾讯微博”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareWeibo({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType, // 分享链接
				    imgUrl: shareImg, // 分享图标
				    success: function () {
					    // 用户确认分享后执行的回调函数
				    },
				    cancel: function () {
					    // 用户取消分享后执行的回调函数
				    }
			    });

			    //获取“分享到QQ空间”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareQZone({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType,// 分享链接
				    imgUrl: shareImg, // 分享图标
				    success: function () {
					    // 用户确认分享后执行的回调函数
				    },
				    cancel: function () {
					    // 用户取消分享后执行的回调函数
				    }
			    });

			    wx.error(function(res){
				    // alert(JSON.stringify(res));
				    // alert('res');
			    });
		    });

	    });

	    common.setViewTitle('查询结果');
	    //首页隐藏标题栏右键
	    AlipayJSOperation.setRightButtonStatus(false);
	    //设置标题颜色
	    AlipayJSOperation.setBarColor('#2FB3FE');
	    AlipayJSOperation.setTitle('查询结果');
    }

    //离开首页时缓存变量方便其他页面调用
    componentWillUnmount() {
        //当前绑定的事件仅限首页使用，卸载时解绑
        AlipayJSOperation.removeLeftButtonEvent();
    }

		//选择的时间
		changeCheckDate(registerDate) {
    	// registerDate ='2018-3-3'
			let d = new Date();
			if(registerDate.split('-')[0]>d.getFullYear()){   ///判断年份
				Toast.info('选择的时间不能大于当前时间~',1);
				return false;
			}

			if(registerDate.split('-')[0]==d.getFullYear()){
				if(registerDate.split('-')[1].substr(0,1)==0){  //判断月份

					if(registerDate.split('-')[1].substr(1,1)>d.getMonth()+1){
						Toast.info('选择的时间不能大于当前时间~',1);
						return false;
					}

					if(registerDate.split('-')[1].substr(1,1)==d.getMonth()+1){

						if(registerDate.split('-')[2].substr(0,1)==0){   ///判断日
							if(registerDate.split('-')[2].substr(1,1)>d.getDate()){
								Toast.info('选择的时间不能大于当前时间~',1);
								return false;
							}
						}else if(registerDate.split('-')[2].substr(0,1)!=0){
							if(registerDate.split('-')[2]>d.getDate()){
								Toast.info('选择的时间不能大于当前时间~',1);
								return false;
							}
						}

					}


				}

				// else if(registerDate.split('-')[1].substr(0,1)!=0){
				//
				// 	if(registerDate.split('-')[1]>d.getMonth()+1){
				// 		Toast.info('选择的时间不能大于当前时间2~',1);
				// 		return false;
				// 	}
				//
				// 	if(registerDate.split('-')[1].substr(1,1)==d.getMonth()+1){
				//
				// 		if(registerDate.split('-')[2].substr(0,1)==0){   ///判断日
				// 			console.log(registerDate.split('-')[2].substr(0,1)==0);
				// 			console.log(registerDate.split('-')[2]);
				// 			console.log(registerDate.split('-')[2].substr(1,1))
				// 			console.log(d.getDate())
				// 			if(registerDate.split('-')[2].substr(1,1)>d.getDate()){
				// 				Toast.info('选择的时间不能大于当前时间3~',1);
				// 				return false;
				// 			}
				// 		}else if(registerDate.split('-')[2].substr(0,1)!=0){
				// 			if(registerDate.split('-')[2]>d.getDate()){
				// 				Toast.info('选择的时间不能大于当前时间4~',1);
				// 				return false;
				// 			}
				// 		}
				//
				// 	}
				//
				// }



				// if(registerDate.split('-')[2].substr(0,1)==0){   ///判断日
				// 	console.log(registerDate.split('-')[2].substr(0,1)==0);
				// 	console.log(registerDate.split('-')[2]);
				// 	console.log(registerDate.split('-')[2].substr(1,1))
				// 	console.log(d.getDate())
				// 	if(registerDate.split('-')[2].substr(1,1)>d.getDate()){
				// 		Toast.info('选择的时间不能大于当前时间3~',1);
				// 		return false;
				// 	}
				// }else if(registerDate.split('-')[2].substr(0,1)!=0){
				// 	if(registerDate.split('-')[2]>d.getDate()){
				// 		Toast.info('选择的时间不能大于当前时间4~',1);
				// 		return false;
				// 	}
				// }

			}

			this.setState({
				maturityDate: this.getDays(registerDate),
				status: false
			});

			let getData = {
				firstLicensingDate: registerDate,
				drivingLicenseNo: this.state.drivingLicenseNo,
				drivingFileNumber: this.state.drivingFileNumber,
				drivingLicenseScoreId: this.state.drivingLicenseScoreId,
			};
			homeService.updateLicense(getData).then(data => {
				if (data.code == 1000) {

				} else {
					Toast.info(data.msg);
				}
			},(error)=>{

			});
		}
    render() {
	      let drivingLicenseNo = this.state.drivingLicenseNo.toString();
	      let drivingFileNumber = this.state.drivingFileNumber.toString();

        return (
            <div>
                {/*<PageBar title="查询结果" isHome={true} />*/}
	              <div className={style.result}>
		              <div className={style.fraction}>
			              <p className={style.branch}>
				              <span className={style.circle}>{this.state.score}</span>
			              </p>
			              <p className={style.branch_text}>驾照剩余分值</p>
			              <DatePicker
				              mode="date"
				              title="选择日期"
				              extra={this.state.status? '选择驾驶证领取日期':'修改'}
				             // value={this.state.dpValue}
				              onChange={(data) => {
					              this.setState({ dpValue: data});
					              this.changeCheckDate(data.format('YYYY-MM-DD'));
					              this.sendMessage('btn_drivingLicense_clear_time');
				              }}
			              >
			              <CustomChildren
					              maturityDate={this.state.maturityDate}
					              status={this.state.status}
				              >扣分清零提醒</CustomChildren>
			              </DatePicker>

										<div style={{borderBottom:'1PX solid #d7d7d7',margin: '0 0.3rem 0.3rem 0.3rem'}}></div>
			              <div className={style.archives}>
				              <p>
					              <span>驾驶证号：</span>
					              <span>{drivingLicenseNo.substr(0,4)+'**********'+drivingLicenseNo.substr(drivingLicenseNo.length-4,drivingLicenseNo.length)}</span>
				              </p>
				              <p>
					              <span>档案编号：</span>
					              <span>{drivingFileNumber.substr(0,4)+'****'+drivingFileNumber.substr(drivingFileNumber.length-4,drivingFileNumber.length)}</span>
				              </p>
			              </div>
		              </div>
	              </div>


                {/*<div onClick={()=>{ this.context.router.push('demo');}}>圆圈</div>*/}
                {/*<div className={style.result}>*/}

	                {/*<div>{this.state.maturityDate}</div>*/}
                {/*</div>*/}
            </div>
        )
    }
}

//使用context
Result.contextTypes = {
    router: React.PropTypes.object.isRequired
};

Result.defaultProps = {

};

const mapStateToProps = state => ({
	carInfo: state.mark
})


export default connect(mapStateToProps)(Result)
