export {default as App} from './App'

// 简单
export { default as Home } from './Home'

export {default as Result} from './Result'//查分结果页
export {default as Demo} from './Demo'//园圈

// 404
export { default as NotFoundPage } from './404.js'