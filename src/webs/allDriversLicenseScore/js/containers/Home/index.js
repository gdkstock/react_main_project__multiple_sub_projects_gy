
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Toast,Popup} from 'antd-mobile';
import userHelper from '../../utils/userHelper';
import AlipayJSOperation from '../../utils/alipayJSOperation';
import urlOperation from '../../utils/urlOperation';
import login from '../../utils/loginOperation';
import PageBar from '../../components/common/pageBar';
import InputItem from '../../components/home/inputItem';
import homeService from '../../services/homeService';
import style from './index.scss';

import common from '../../utils/common'
import * as homeActions from '../../actions/homeActions';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
	        isApp: navigator.userAgent.indexOf("appname_cxycwz") > -1 ? true:false,
	        isShow: false,  //输入框蒙层首先隐藏
	        isPrompt_a: false, //驾驶证蒙层提示_1
	        isPrompt_b: false, //驾驶证蒙层提示_2
	        drivingLicense: '',  //驾驶证号
	        drivingFile: '',  //档案号
	        phone: '',  //手机号
	        a: '',
        };
	     this.subMit = this.subMit.bind(this);
	    //尽可能快的屏蔽微信分享
	    // this.WeXinOperation();
	    // this.backOperation();
    }

    //发送埋点
    sendMessage(type) {
        window.cxytj.recordUserBehavior({
            eventId: type,
            eventType: '2', //事件类型
            eventTime: '',  //触发时间
        });
    }

    componentWillMount(){
    }

    componentWillUnmount(){
    }

		closeAll() {
			window.cxytj.recordUserBehavior({
				eventId: 'checkPoints_close',
				eventType: '2', //事件类型
				eventTime: '',  //触发时间
			});
			AlipayJSOperation.closeView();
		}

    componentDidMount() {
    	this.sendMessage('view_drivingLicense');
	    var userType = urlOperation.getParameters().detailUserType;
	    var shareTile = "查询驾照分，让你心里有个底";
	    var shareUrl = location.href.split('#')[0];
	    var index = shareUrl.lastIndexOf('/');
	    var urlShare = 'https://webservice.cx580.com/cweb/activity/2017/05/driving_license_check_score/index.html';
	    var shareImg = shareUrl.substring(0, index) + "/images/share.png";
	    var shareDesc = '还在担心你的驾照分不翼而飞吗？马上查询看看吧~';
	    var postData = {
		    signUrl: encodeURIComponent(shareUrl)
	    };
	    Toast.hide(); //加载前 先关闭其他加载中的提示框 避免提示框一直存在的bug
	    Toast.loading("", 30, () => Toast.info("网络错误", 2));
	    homeService.share(postData).then(data => {
		    Toast.hide(); //加载前 先关闭其他加载中的提示框 避免提示框一直存在的bug
		    wx.config({
			    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			    appId: data.data.appId, // 必填，公众号的唯一标识
			    timestamp: data.data.timestamp, // 必填，生成签名的时间戳
			    nonceStr: data.data.nonceStr, // 必填，生成签名的随机串
			    signature: data.data.signature,// 必填，签名，见附录1
			    jsApiList: [
				    'checkJsApi',
				    'onMenuShareTimeline',
				    'onMenuShareAppMessage',
				    'onMenuShareQQ',
				    'onMenuShareWeibo',
				    'onMenuShareQZone',
			    ]
		    });

		    wx.ready(function () {
			    /// wx.hideOptionMenu();

			    //获取“分享到朋友圈”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareTimeline({
				    title: shareTile,
				    link: urlShare+'?detailUserType='+userType,
				    imgUrl: shareImg,
				    trigger: function (res) {
					    // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
					    //alert('用户点击分享到朋友圈');
				    },
				    success: function (res) {

				    },
				    cancel: function (res) {

				    }
			    });

			    //获取“分享给朋友”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareAppMessage({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType, // 分享链接
				    imgUrl: shareImg, // 分享图标
				    type: '', // 分享类型,music、video或link，不填默认为link
				    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
				    trigger: function (res) {
					    // alert(JSON.stringify(res))
					    // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
					    //alert('分享给朋友2');
				    },
				    success: function () {
					    //alert('分享给朋友3');
				    },
				    cancel: function () {
				    }
			    });
			    //获取“分享到QQ”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareQQ({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType, // 分享链接
				    imgUrl: shareImg, // 分享图标
				    trigger: function (res) {
					    // alert(JSON.stringify(res))
					    // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
					    //alert('分享给朋友2');
				    },
				    success: function () {
					    //alert('分享给朋友3');
				    },
				    cancel: function () {
				    }
			    });

			    //获取“分享到腾讯微博”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareWeibo({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType, // 分享链接
				    imgUrl: shareImg, // 分享图标
				    success: function () {
					    // 用户确认分享后执行的回调函数
				    },
				    cancel: function () {
					    // 用户取消分享后执行的回调函数
				    }
			    });

			    //获取“分享到QQ空间”按钮点击状态及自定义分享内容接口
			    wx.onMenuShareQZone({
				    title: shareTile, // 分享标题
				    desc: shareDesc, // 分享描述
				    link: urlShare+'?detailUserType='+userType,// 分享链接
				    imgUrl: shareImg, // 分享图标
				    success: function () {
					    // 用户确认分享后执行的回调函数
				    },
				    cancel: function () {
					    // 用户取消分享后执行的回调函数
				    }
			    });

			    wx.error(function(res){
				    // alert(JSON.stringify(res));
				    // alert('res');
			    });
		    });

	    });


    	common.setViewTitle('驾照查分');
	    //首页隐藏标题栏右键
	    AlipayJSOperation.setRightButtonStatus(false);
	    //设置标题颜色
	    AlipayJSOperation.setBarColor('#2FB3FE');
	    AlipayJSOperation.setTitle('驾照查分');
	    AlipayJSOperation.setLeftButtonStatus(this.closeAll);

    }

    //离开首页时缓存变量方便其他页面调用
    componentWillUnmount() {
        //当前绑定的事件仅限首页使用，卸载时解绑
        AlipayJSOperation.removeLeftButtonEvent();
    }

    subMit() {//提交表单
      this.sendMessage('btn_drivingLicense_check_points');

      if(!this.state.drivingLicense||this.state.drivingLicense.length==0){
	      Toast.info('驾驶证号不能为空',1);
	      return false;
      }else if(!this.state.drivingFile||this.state.drivingFile.length==0){
	      Toast.info('档案编号不能为空',1);
	      return false;
      }

	      let getData = {
		      // userId: "CXY_FEA04126DA2D439290E24E01D0A66014",
		      // token: "A855FA0D2CC7FB6867531A1DF0589A2A",
		      // driverName: "粤GLP218",
		      // drivingLicenseNo: "440821196812130316",
		      // drivingFileNumber: "440810019299",
		      // phone: "13702681456"
		      drivingLicenseNo: this.state.drivingLicense,
		      drivingFileNumber: this.state.drivingFile,
		     // phone: this.state.phone
	      };
	      Toast.hide(); //加载前 先关闭其他加载中的提示框 避免提示框一直存在的bug
	      Toast.loading("", 30, () => Toast.info("网络错误", 2));
	      homeService.checkPoints(getData).then(data => {
		      Toast.hide();
		      if (data.code == 1000) {
			      this.props.dispatch(homeActions.fraction({
				      score: data.data.score,
				      drivingLicenseNo: this.state.drivingLicense,
				      drivingFileNumber: this.state.drivingFile,
			      }));
			       setTimeout( this.context.router.push(`result/${data.data.score}/${this.state.drivingLicense}/${this.state.drivingFile}`), 500);
		      } else {
			      Toast.info(data.msg);
		      }
	      });

    }

    maskLayer(){
	    login.isAppOperation(() => {
		    this.setState({
			    isShow: true,
		    });
    	});

	    this.sendMessage('btn_drivingLicense_check_points');
    }

		isHide(){
    	this.setState({
		    isShow: false,
	    })
		}

		maskPrompt(i){
			if(i=='1'){
				this.setState({
					isPrompt_b: true,
				})
			}else if(i == '2'){
				this.setState({
					isPrompt_a: true,
				})
			}

		}

    render() {

				let top = this.state.isApp?'0rem':'0rem';
				let maskShow = this.state.isShow?'block':'none';
				let prompt_a = this.state.isPrompt_a?'block':'none';
				let prompt_b = this.state.isPrompt_b?'block':'none';

        return (
            <div>
                {/*<PageBar title="驾照查分" isHome={true} />*/}
                <div className={style.home}>
									{/*首页图片*/}
	                <div className={style.container}>
		                <img className={style.home_box} src="./images/home_box_01.png" alt=""/>

		                <div className={style.shrink_box}>
			                <img className={style.home_box_02} src="./images/home_box_02.png" alt=""/>
			                <img ref="shake" onClick={()=>{this.maskLayer()}} className='shrink' src="./images/btn_s.png" alt=""/>
		                </div>
	                </div>
	                {/*首页图片*/}

									{/*蒙层输入框*/}
                  <div className={style.floatLayer} style={{top:`${top}`,display: `${maskShow}`}}>
                    <div className={style.follow_step}>
	                  <img className={style.del} src="./images/icon_del.png" alt="" onClick={()=>{this.isHide()}}/>
                      <InputItem type="text" index='1' itemText="驾驶证号" maxLength="18" placeHolder="驾驶证号完整15位或18位数"
                                 value={this.state.drivingLicense} showUnderLine={true}
                                 onPrompt={(i)=>{
                                 	  this.maskPrompt(i);
                                 }}
                                 onChange={(e) => {
                        this.setState({drivingLicense: e.target.value})
                      }}/>
                      <InputItem type="text" index='2' itemText="档案编号" maxLength="12" placeHolder="档案编号完整12位数"   //32位
                                 value={this.state.drivingFile} showUnderLine={true}
                                 onPrompt={(i)=>{
	                                 this.maskPrompt(i);
                                 }}
                                 onChange={(e) => {
                        this.setState({drivingFile: e.target.value})
                      }}/>

                        <button type="button" className={style.btn_s} onClick={(e)=>{this.subMit()}}>点击查询</button>
                    </div>
                  </div>

	                {/*档案编号图片提示*/}
									<div className={style.floatLayer} style={{top:`${top}`,display:`${prompt_a}`}}>
											<div>
												<img className={style.del_a} src="./images/icon_del.png" alt="" onClick={()=>{this.setState({isPrompt_a:false,})}}/>
												<img src="./images/file_recv.png" alt=""/>
											</div>
									</div>

	                {/*证件编号提示*/}
	                <div className={style.floatLayer} style={{top:`${top}`,display:`${prompt_b}`}}>
		                <div>
			                <img className={style.del_a} src="./images/icon_del.png" alt="" onClick={()=>{this.setState({isPrompt_b:false,})}}/>
			                <img src="./images/file_recv_b.png" alt=""/>
		                </div>
	                </div>

                </div>
            </div>
        )
    }
}

//使用context
Home.contextTypes = {
    router: React.PropTypes.object.isRequired
};

Home.defaultProps = {
	list:['0','1'],
};

const mapStateToProps = state => ({
	carInfo: state.mark
})

export default connect(mapStateToProps)(Home)
