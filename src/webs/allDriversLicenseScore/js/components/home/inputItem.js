/**
 * Created by 小敏哥 on 2017/3/22.
 */
import React, {Component} from 'react';
import style from './inputItem.scss';

class InputItem extends Component {
    constructor(props) {
        super(props);
        this.onBlurCallBack=this.onBlurCallBack.bind(this);
        this.onIconCallBack=this.onIconCallBack.bind(this);
    }

    //焦点离开回调
    onBlurCallBack(event){
        this.props.onBlur&&this.props.onBlur(event.target.value)
    }

	  onIconCallBack(i){
        this.props.onPrompt&&this.props.onPrompt(i)
    }

    render() {
        return <div className={this.props.showUnderLine ? style.inputItem + ' ' + style.underLine : style.inputItem}>
            <div><span>{this.props.itemText}</span></div>
            <img className={style.icon_prompt} src="./images/icon_bule.png" alt="" onClick={()=>{this.onIconCallBack(this.props.index)}}/>
            <input defaultValue={this.props.defaultValue} value={this.props.value} placeholder={this.props.placeHolder} onChange={this.props.onChange} onBlur={this.onBlurCallBack} maxLength={this.props.maxLength} type={this.props.type}/>
        </div>
    }
}

export default InputItem