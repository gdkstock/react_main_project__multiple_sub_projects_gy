/**
 * 车辆信息
 */
import {
  	FRACTION, //更新车辆信息
} from '../actions/actionsTypes.js'

export default function car(state = {}, action) {
    switch (action.type) {
        case FRACTION:
            return Object.assign({}, state, action.data)
        default:
            return state
    }
}