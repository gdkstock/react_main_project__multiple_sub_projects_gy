import { combineReducers } from 'redux'
import mark from './mark'

const rootReducer = combineReducers({
	mark
});

export default rootReducer;
