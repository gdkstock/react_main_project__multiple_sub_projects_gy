import React, { Component } from 'react'
import common from '../utils/common'
import apiHelper from '../services/apiHelper'
import styles from './index.scss'
import jsApi from '../utils/cx580.jsApi'
import Loading from './Loading'
import End from './End'

export default class App extends Component {
	constructor(props) {
        super(props);

        const defaultState={
			list:[
				{
					eventId:"123",
					imageUrl:"./images/banner01.png",
					jumpUrl:"http://www.baidu.com"

				},
				{
					eventId:"233",
					imageUrl:"./images/banner02.png",
					jumpUrl:"http://www.baidu.com"

				},
				{
					eventId:"455",
					imageUrl:"./images/banner03.png",
					jumpUrl:"http://www.baidu.com"

				},
				{
					eventId:"565",
					imageUrl:"./images/banner04.png",
					jumpUrl:"http://www.baidu.com"

				}
			]
		}

        this.state = {
        	list:[],
        	isFetching:false,
        	isEndShow:false
        }
    }

	componentWillMount(){

		//http://mt.cx580.com:6002/api/mp/cxy.media.recommend.ashx

		document.querySelector('title').innerHTML = "车主福利"

		let me = this

		// alert(JSON.stringify(window.cx580.jsApi))

		// if(common.isCXYApp()){
		// 	jsApi.call({
		// 		"commandId":"",
		// 	    "command":"getSymbol",
		// 	    "data": {
		// 	        "userid":"",
		// 	        "lat":"",
		// 	        "lng":"",
		// 	        "city":"",
		// 	        "channel":"",
		// 	        "version":"",
		// 	        "sessionId":"",
		// 	        "productId":"",
		// 	        "accountId":"",
		// 	    }
		// 	},function(data){
		// 		//请求数据
		// 		// alert(JSON.stringify(data))
		// 		let requestParam = {
		// 			url:apiHelper.baseApiUrl+"advert/chezhufuli",
		// 			data:{
		// 				method:"post",
		// 				body:{
		// 					userId:data.data.userid,
		// 					latitude:data.data.latitude,
		// 					longitude:data.data.longitude,
		// 					channel:data.data.channel,
		// 					appServierVersion:data.data.version,
		// 					city:data.data.city
		// 				}
		// 			}
		// 		};
		// 		me.setState({
		// 			isFetching:true
		// 		})
		// 		apiHelper.fetch(requestParam).then(data=>{
		// 			//将数据转换成我们需要的格式
		// 			//alert("成功"+JSON.stringify(data))
		// 			let list=[]
		// 			if(data.code=="1000"){
		// 				if(data.data && Object.prototype.toString.call(data.data.AdList)=="[object Array]"){
		// 					list = data.data.AdList.map((item)=>{
		// 						let targetItem={
		// 							imageUrl:item.imgUrl
		// 						}
		// 						return {...item,...targetItem}
		// 					})
		// 				}
		// 			}
		// 			me.setState({
		// 				list,
		// 				isFetching:false
		// 			})
		// 		}).catch((e)=>{
		// 			window.networkError('./images/networkError-icon.png')
		// 		})

		// 		//初始化统计
		// 		window.cxytj.init({ //以下为初始化示例，可新增或删减字段
		// 	        productId: data.data.productId, //产品ID
		// 	        productVersion: data.data.version, //产品版本
		// 	        productUserId: data.data.userid, //APP端 （设备标记ID）
		// 	        channel: data.data.channel, //推广渠道
		// 	        isProduction: apiHelper.production, //生产环境or测试环境 默认测试环境
		// 	        userId: data.data.accountId, //用户ID
		// 	        sessionId: data.data.sessionId, //会话ID
		// 	        ip: '', //IP地址
		// 	        longitude: data.data.lng, //经度
		// 	        latitude: data.data.lat //纬度
		// 	    });

		// 	})
		// }

		cxyJsdkReady((me)=>{
			if(common.isCXYApp()){
				window.cx580.jsApi.call({
					"commandId":"",
				    "command":"getSymbol",
				    "data": {
				        "deviceId":"",
				        "lat":"",
				        "lng":"",
				        "city":"",
				        "channel":"",
				        "version":"",
				        "sessionId":"",
				        "productId":"",
				        "accountId":"",
				    }
				},function(data){
					//请求数据
					alert(JSON.stringify(data))
					let requestParam = {
						url:apiHelper.baseApiUrl+"advert/chezhufuli",
						data:{
							method:"post",
							body:{
								userId:data.data.deviceId,
								latitude:data.data.latitude,
								longitude:data.data.longitude,
								channel:data.data.channel,
								appServierVersion:data.data.version,
								city:data.data.city
							}
						}
					};
					me.setState({
						isFetching:true
					})
					apiHelper.fetch(requestParam).then(data=>{
						//将数据转换成我们需要的格式
						//alert("成功"+JSON.stringify(data))
						let list=[]
						if(data.code=="1000"){
							if(data.data && Object.prototype.toString.call(data.data.AdList)=="[object Array]"){
								list = data.data.AdList.map((item)=>{
									let targetItem={
										imageUrl:item.imgUrl
									}
									return {...item,...targetItem}
								})
							}
						}
						me.setState({
							list,
							isFetching:false
						})
					}).catch((e)=>{
						window.networkError('./images/networkError-icon.png')
					})

					//初始化统计
					window.cxytj.init({ //以下为初始化示例，可新增或删减字段
				        productId: data.data.productId, //产品ID
				        productVersion: data.data.version, //产品版本
				        productUserId: data.data.userid, //APP端 （设备标记ID）
				        channel: data.data.channel, //推广渠道
				        isProduction: apiHelper.production, //生产环境or测试环境 默认测试环境
				        userId: data.data.accountId, //用户ID
				        sessionId: data.data.sessionId, //会话ID
				        ip: '', //IP地址
				        longitude: data.data.lng, //经度
				        latitude: data.data.lat //纬度
				    });

				})
			}
		})
	}


  
  	jumpTo(item){
  		//提交用户行为数据
		window.cxytj.recordUserBehavior({
			eventId: item.eventId,
			eventType: '2',
			attr1: '车主福利页点击统计',
			curPageInfo: '车主福利页',
		});

  		common.openNewBrowserWithURL(item.jumpUrl);
  	}

  	componentDidMount(){
  		// this.isEndShow();
  	}
  	componentDidUpdate(){
  		// this.isEndShow();
  	}

  	//控制end是否显示
  	// isEndShow(){
  	// 	if(!window.timer){
  	// 		window.timer = setTimeout(()=>{
	  // 			let bodyHeight = document.body.clientHeight
		 //  		let windowHeight = window.innerHeight
		 //  		alert(bodyHeight+" "+windowHeight)
		 //  		if(bodyHeight>windowHeight){
		 //  			this.setState({
		 //  				isEndShow:true,
		 //  			})
		 //  			window.timer = null;
		 //  		}
	  // 		},1000)
  	// 	}
  	// }

	render() {
		console.log(this.state)
		return (
		  <div>
		  	{
			  	!this.state.isFetching?
			  	(this.state.list && 
			  		<div>
				  		{
				  			this.state.list.map((item,index)=>
						  		<div key={index}>
						  			<div className={styles.whiteSpace}></div>
						  			<div onClick={()=>this.jumpTo(item)}>
						  				<img src={item.imageUrl} style={{verticalAlign:"bottom",width:"100%"}}/>
						  			</div>	
						  		</div>
					  		)
				  		}
				  		{ this.state.list.length>4 && <End/> }
			  		</div>
			  	):<Loading/>
		  	}
		  </div>
		)
	}
}