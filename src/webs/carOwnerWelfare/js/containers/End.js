/*
* end组件
*/
import styles from './index.scss'

const End = props =>{
	return (
		<div className={styles.End}>
			<div className={styles.txt}>END</div>
			<div className={styles.line}></div>
		</div>
	)
}

export default End