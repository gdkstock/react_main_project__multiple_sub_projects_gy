/*
* 加载中组件
*/
import styles from './index.scss'

const Loading = props =>{
	return (
		<div className={styles.loading}>
			<img src="./images/loading.gif"/>
		</div>
	)
}

export default Loading