import '../styles/normalize.scss'
import '../styles/app.scss'
import '../styles/font.scss'

import {AppContainer} from 'react-hot-loader'
import React from 'react'
import 'babel-polyfill' //兼容低版本浏览器不支持ES6语法
import {render} from 'react-dom'
import App from './containers/App'
import Redbox from 'redbox-react'
const rootEl = document.getElementById('app');

render(
  <AppContainer errorReporter={Redbox}>
    <App />
  </AppContainer>,
  rootEl
)


if (module.hot) {
  module.hot.accept('./containers/App', () => {
    // If you use Webpack 2 in ES modules mode, you can
    // use <App /> here rather than require() a <NextApp />.
    const NextApp = require('./containers/App').default;
    render(
      <AppContainer errorReporter={Redbox}>
        <NextApp />
      </AppContainer>,
      rootEl
    )
  });
}