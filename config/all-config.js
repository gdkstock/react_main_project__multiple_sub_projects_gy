/**
 * 混合项目配置文件
 * 如果项目同时在两个或以上平台运行的话，那么则将项目的配置信息写在本配置文件中
 */

var allConfig = {

	//全渠道默认项目
	allDefault: {
		name: 'allDefault',
		title: ' ',
		port: 7000,
		outputPath: 'dist',
		description: '多渠道||APP、微信、支付宝、QQ',
		publicPath: '',
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//年检信息页
	allInspectionReminder: {
		name: 'allInspectionReminder',
		title: ' ',
		port: 7010,
		outputPath: 'inspection-reminder',
		description: '年检提醒||多渠道||APP、微信、支付宝、QQ'
	},

	//违章优化
	allInspectionViolation: {
		name: 'allInspectionViolation',
		title: ' ',
		port: 7011,
		outputPath: 'Violation',
		// outputPath: '../builds/Violation1.3',
		description: '适用于多平台的违章改版业务H5',
		// publicPath: '//oss.cx580.com/Violation/',
		publicPath: '//oss.cx580.com/Violation1.3/', //1.3版本
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//驾照查分
	allDriversLicenseScore: {
		name: 'allDriversLicenseScore',
		title: ' ',
		port: 7012,
		outputPath: 'LicenseScore',
		description: '页面名称：驾照查分；页面目的：收集用户驾照信息的活动；'
	},

	//驾照查分
	allDrivingLicence: {
		name: 'allDrivingLicence',
		title: ' ',
		port: 7013,
		outputPath: 'drivingLicence',
		description: '适用于多平台的驾照查分业务'
	},

	//类APP的H5首页
	allH5Index: {
		name: 'allH5Index',
		title: ' ',
		port: 7014,
		// outputPath: 'H5Index', //打包输出路径（相对应于项目目录）
		outputPath: './H5Index_v1.3', //打包输出路径（相对应于项目目录） 1.3版本
		description: '模仿APP首页||迷你版首页',
		// publicPath: '//oss.cx580.com/H5Index/', //oss、CDN路径 
		publicPath: '//oss.cx580.com/H5Index_v1.3/', //1.3版本
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//引导注册-添加车辆
	allAddCar: {
		name: 'allAddCar',
		title: ' ',
		port: 7016,
		// outputPath: 'AddCar',
		outputPath: 'AddCar_v1.3',
		description: '引导注册，添加车辆',
		// publicPath: '//oss.cx580.com/AddCar/',
		publicPath: '//oss.cx580.com/AddCar_v1.3/', //1.3版本
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//全渠道适配2
	allDefault2: {
		name: 'allDefault2',
		title: ' ',
		port: 7018,
		outputPath: 'Default2',
		description: '全渠道适配2；优化',
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//车展直播宣传页
	allCheZhanZhiBo: {
		name: 'allCheZhanZhiBo',
		title: ' ',
		port: 7018,
		outputPath: 'CheZhanZhiBo',
		description: '车展直播宣传页',
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//港澳续签H5对接
	allGangAoQianZhuH5: {
		name: 'allGangAoQianZhuH5',
		title: ' ',
		port: 7020,
		outputPath: 'GangAoQianZhuH5',
		description: '港澳续签H5对接',
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	// 年审提醒活动：全渠道，不分享
	Activity170912: {
		name: 'Activity170912',
		title: '年审活动',
		port: 7022,
		outputPath: 'activity170912-regist',
		description: '新年审活动'
	},
	//全渠道优惠券列表
	allCoupon: {
		name: 'allCoupon',
		title: ' ',
		port: 7024,
		outputPath: './Coupon',
		description: '全渠道优惠券列表页面',
		// publicPath: '//oss.cx580.com/Coupon/',
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//我的订单
	allMyOrder: {
		name: 'allMyOrder',
		title: ' ',
		port: 7026,
		outputPath: './MyOrder',
		description: '全渠道我的订单',
		publicPath: '//oss.cx580.com/MyOrder/',
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	// 双11活动：全渠道，不分享
	Activity171001: {
		name: 'Activity171001',
		title: '活动',
		port: 7028,
		outputPath: 'activity171001-regist',
		description: '活动'
	},

	//社区
	allCommunity: {
		name: 'allCommunity',
		title: ' ',
		port: 7050,
		outputPath: 'Community', //测试打包
		// outputPath: '../../builds/Community', //CD部署打包目录
		description: '全渠道社区',
		// publicPath: '//oss.cx580.com/Community/', //OSS CDN加速
		script: {
			onerror: "networkError('./images/networkError-icon.png')",
		}
	},

	//车辆信息
	carInfo: {
		name: 'carInfo', //项目名称 需要和allConfig的key一样。也是webs下的文件夹名称
		title: ' ', //index.html的title 建议设置为空 避免直接进入路由组件时，出现title切换的bug
		port: 7051, //启动的server 端口
		outputPath: 'H5Index', //打包输出路径（相对应于项目目录）
		description: '车辆信息', //项目描述
		script: {
			onerror: "networkError('./images/networkError-icon.png')", //打包后的JS 自动增加onerror属性
		}
	},
	
	// 12月微众钱包活动：全渠道（暂时微信和APP），分享
	Activity171218Wallet: {
		name: 'Activity171218Wallet',
		title: '活动',
		port: 7030,
		outputPath: 'Activity171218Wallet-regist',
		description: '活动'
	},

}

module.exports = allConfig